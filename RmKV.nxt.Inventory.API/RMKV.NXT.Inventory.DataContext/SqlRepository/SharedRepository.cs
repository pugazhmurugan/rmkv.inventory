﻿using Dapper;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using RmKV.NXT.Inventory.DataContext.InterfaceRepository;
using RmKV.NXT.Inventory.DataModel.Model.Shared;
using RmKV.NXT.Inventory.DataModel.Model.User;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace RmKV.NXT.Inventory.DataContext.SqlRepository
{
    public class SharedRepository : RepositoryBase, ISharedRepository
    {
        public SharedRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public Task<List<string>> GetCompanySelect(int User_Id, int Sales_Location_Id)
        {
            throw new NotImplementedException();
        }  
        public Task<string> GetHRDocumentRootPathAndUrl(int salesLocationId)
        {
            throw new NotImplementedException();
        }
        public Task<List<string>> GetSalesLocation(int User_Id)
        {
            throw new NotImplementedException();
        }
        public async Task<string> GetWarehouse(string warehouse)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", warehouse);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_saleslocation_based_warehouse_select",
                 datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetSection(string section)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", section);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_warehouse_based_company_section_select",
                 datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<Users> CheckLockPassword(UserDTO objEntry)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_user_name", objEntry.User_Name);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_Authenticate_UsernameAndPassword",
                 datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            List<Users> result = JsonConvert.DeserializeObject<List<Users>>(response.Records);
            return result[0];
        }
        public long EncryptKey(string key)
        {
            long lngHold = 0;

            for (int intLoop = 1; intLoop <= Strings.Len(key); intLoop++)
            {
                switch ((Strings.Asc(Strings.Left(key, 1)) * intLoop) % 4)
                {
                    case 0:
                        {
                            lngHold = (lngHold + (Strings.Asc(Strings.Mid(key, intLoop, 1)) * intLoop));
                            break;
                        }
                    case 1:
                        {
                            lngHold = (lngHold - (Strings.Asc(Strings.Mid(key, intLoop, 1)) * intLoop));
                            break;
                        }
                    case 2:
                        {
                            lngHold = (lngHold + (Strings.Asc(Strings.Mid(key, intLoop, 1)) * (intLoop - Strings.Asc(Strings.Mid(key, intLoop, 1)))));
                            break;
                        }
                    case 3:
                        {
                            lngHold = (lngHold - (Strings.Asc(Strings.Mid(key, intLoop, 1)) * (intLoop + Strings.Len(key))));
                            break;
                        }
                }
            }
            return lngHold;
        }
        public async Task<DashboardTable> GetDashboardDetails(int warehouseId)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_wh_id", warehouseId);
            var response = await Connection.QueryFirstOrDefaultAsync<DashboardTable>($"nxtgrn.fn_grn_dashboard_details", values,
                commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
        public async Task<string> GetByNoDetails(string ByNoData)
        {
           // List<BynoDetails> result = null;
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ByNoData);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_getbynodetails",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            //if (response != null && response.Records != null)
            //    result = JsonConvert.DeserializeObject<List<BynoDetails>>(response.Records);
            return response.Records;
        }
        public async Task<string> GetWarehouseBasedCompanySection(string objSave)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objSave);
            var result = await Connection.ExecuteScalarAsync<string>($"nxtcommon.fn_warehouse_based_company_section_select", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }
        public async Task<string> GetMaterialLookup(string lookup)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", lookup);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_material_select",
                 datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetMaterialProductLookup(string lookup)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", lookup);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_material_product_select",
                 datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}
