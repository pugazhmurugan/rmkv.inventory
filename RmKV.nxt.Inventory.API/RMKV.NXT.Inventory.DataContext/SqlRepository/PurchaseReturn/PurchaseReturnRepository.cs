﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseReturn;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.PurchaseReturn
{
   public class PurchaseReturnRepository : RepositoryBase, IPurchaseReturnRepository
    {
        public PurchaseReturnRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<int> AddPurchaseReturn(string PurchaseReturn)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", PurchaseReturn);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_purchase_return_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<bool> ApprovePurchaseReturn(string PurchaseReturn)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", PurchaseReturn);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_cg_transfer_approve", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<bool> CancelPurchaseReturn(string PurchaseReturn)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", PurchaseReturn);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_purchase_return_delete", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<string> FetchPurchaseReturn(string PurchaseReturn)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", PurchaseReturn);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_cg_transfer_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetPurchaseReturn(string PurchaseReturn)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", PurchaseReturn);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_purchase_return_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}
