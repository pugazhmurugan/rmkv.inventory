﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseReturn;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.PurchaseReturn
{
    public class PurchaseReturnDCRepository : RepositoryBase, IPurchaseReturnDCRepository
    {
        public PurchaseReturnDCRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<string> LoadPurchaseReturnDCDetails(string obj)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", obj);
            var result = await Connection.ExecuteScalarAsync<string>($"nxtwarehouse.fn_vapas_dc_list", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<string> FetchPurchaseReturnDCDetails(string obj)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", obj);
            var result = await Connection.ExecuteScalarAsync<string>($"nxtwarehouse.fn_vapas_dc_fetch", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<int> GeneratePurchaseReturnDCNo(string obj)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", obj);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_vapas_dc_no_generate", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }
    }
}
