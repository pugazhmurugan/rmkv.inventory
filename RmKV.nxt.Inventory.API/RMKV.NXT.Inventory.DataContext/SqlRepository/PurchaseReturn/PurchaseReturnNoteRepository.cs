﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseReturn;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.PurchaseReturn
{
    public class PurchaseReturnNoteRepository : RepositoryBase,IPurchaseReturnNoteRepository
    {
        public PurchaseReturnNoteRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<string> GetPurchaseReturnNote(string returnNote)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", returnNote);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_trading_notes_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetPurchaseReturnByNos(string returnNote)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", returnNote);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_vapas_note_byno_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetPurchaseReturnByNoDetails(string returnNote)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", returnNote);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_vapas_note_byno_details",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> AddPurchaseReturnNote(string returnNote)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", returnNote);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<string> FetchPurchaseReturnNote(string returnNote)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", returnNote);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}
