﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseReturn;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.PurchaseReturn
{
   public class ReturnedGoodsNotesRepository : RepositoryBase, IReturnedGoodsNotesRepository
    {
        public ReturnedGoodsNotesRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<string> GetRGNWiseDCDetails(string objPRApprove)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_rgn_dcno_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> AddRGNWiseDetails(string objPRApprove)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<int>($"nxtwarehouse.fn_rgn_save", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }
        public async Task<string> GetRGNWiseDetails(string objPRApprove)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_rgn_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> FetchRGNWiseDetails(string objPRApprove)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_rgn_fetch", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetDCDetails(string objPRApprove)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_rgn_dc_details_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetTransporter(string objPRApprove)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtgrn.fn_transporters_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}
