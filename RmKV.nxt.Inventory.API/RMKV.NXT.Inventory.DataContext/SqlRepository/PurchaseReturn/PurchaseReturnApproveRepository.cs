﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseReturn;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.PurchaseReturn
{
    public class PurchaseReturnApproveRepository : RepositoryBase, IPurchaseReturnApproveRepository
    {
        public PurchaseReturnApproveRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<string> GetApprovedSupplier(string objPRApprove)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_purchase_return_approved_supplier_name_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetPurchaseRetunApprovedList(string objPRApprove)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_purchase_return_approval_supplier_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> RemovPurchaseReturn(string objPRApprove)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPRApprove);
            var response = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_purchase_return_approval_delete", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }
        public async Task<string> GetSupplierCityForApprove(string objPRApprove)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_purchase_return_approval_supplier_city_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetSupplierGroupNameForApprove(string objPRApprove)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_purchase_return_approval_supplier_group_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetSupplierNameForApprove(string objPRApprove)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_purchase_return_approval_supplier_name_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GeSupplierDetailsForApprove(string objPRApprove)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_purchase_return_approval_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> ApprovePurchaseReturnList(string objPRApprove)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", objPRApprove);
            var response = await Connection.QueryFirstOrDefaultAsync<int>($"nxtwarehouse.fn_purchase_return_approval_save",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }
    }
}
