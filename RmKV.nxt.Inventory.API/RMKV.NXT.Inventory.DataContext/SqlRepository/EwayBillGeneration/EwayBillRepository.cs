﻿using Microsoft.AspNetCore.Http;
using Nancy.Json;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.EwayBillGeneration;
using RMKV.NXT.Inventory.DataModel.Model.EwayBill;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.EwayBillGeneration
{
    public class EwayBillRepository : IEwayBillRepository
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        public EwayBillRepository(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public async Task<string> EwayBillLogin(EwayBillEntity obj)
        {
            Uri BaseAddress = new Uri("https://compuewaybill.azurewebsites.net/");
            var client = new HttpClient { BaseAddress = BaseAddress };

            //var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            //if (authorizationRequestHeader.Any())
            //{
            //    client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            //}

            client.DefaultRequestHeaders.Add(obj.client, obj.value);

            using (var response = await client.GetAsync($"ewbreqproc.aspx?action={obj.action}&userid={obj.userid}&password={obj.password}&App={obj.App}&prod={obj.prod}"))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                var result = content.ToString();
                return result;
            }
        }

        public async Task<string> EwayBillValidateUser(EwayBillEntity obj)
        {
            Uri BaseAddress = new Uri("https://compuewaybill.azurewebsites.net/");
            var client = new HttpClient { BaseAddress = BaseAddress };

            //var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            //if (authorizationRequestHeader.Any())
            //{
            //    client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            //}

            client.DefaultRequestHeaders.Add(obj.client, obj.value);

            using (var response = await client.GetAsync($"ewbreqproc.aspx?action={obj.action}&userid={obj.userid}&gstin={obj.gstin}&uid={obj.uid}&password={obj.password}&token={obj.token}&App={obj.App}&prod={obj.prod}"))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                var result = content.ToString();
                return result;
            }
        }

        public async Task<string> EwayBillDetails(EwayBillEntity obj)
        {
            Uri BaseAddress = new Uri("https://compuewaybill.azurewebsites.net/");
            var client = new HttpClient { BaseAddress = BaseAddress };

            //var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            //if (authorizationRequestHeader.Any())
            //{
            //    client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            //}

            client.DefaultRequestHeaders.Add(obj.client, obj.value);

            using (var response = await client.GetAsync($"ewbreqproc.aspx?action={obj.action}&userid={obj.userid}&gstin={obj.gstin}&action2={obj.action2}&irnno={obj.irnno}&token={obj.token}&App={obj.App}&prod={obj.prod}"))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                var content = await response.Content.ReadAsStringAsync();

                EwayBillDetailsEntity ewayBillDetailsEntity = new EwayBillDetailsEntity();

                EwayBillDetailsEntity result1 = JsonConvert.DeserializeObject<EwayBillDetailsEntity>(content);
                var stream = result1.SignedInvoice;

                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(stream);
                var tokenS = handler.ReadToken(stream) as JwtSecurityToken;

                result1.detailsData = tokenS.Payload.Values.ToArray()[0].ToString();

                var result = JsonConvert.SerializeObject(result1);
                return result;
            }
        }

        public async Task<string> GenerateEwayBillandEInvoice(EwayBillEntity obj)
        {
            Uri BaseAddress = new Uri("https://compuewaybill.azurewebsites.net/");
            var client = new HttpClient { BaseAddress = BaseAddress };

            //var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            //if (authorizationRequestHeader.Any())
            //{
            //    client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            //}

            client.DefaultRequestHeaders.Add(obj.client, obj.value);

            var stringContent = new StringContent(obj.parameter.ToString(), UnicodeEncoding.UTF8, "application/json");

            using (var response = await client.PostAsync($"ewbreqproc.aspx?action={obj.action}&userid={obj.userid}&gstin={obj.gstin}&action2={obj.action2}&token={obj.token}&App={obj.App}&prod={obj.prod}",stringContent))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                var result = content.ToString();
                return result;
            }
        }

        public async Task<string> GenerateEwayBill(EwayBillEntity obj)
        {
            Uri BaseAddress = new Uri("https://compuewaybill.azurewebsites.net/");
            var client = new HttpClient { BaseAddress = BaseAddress };

            //var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            //if (authorizationRequestHeader.Any())
            //{
            //    client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            //}

            client.DefaultRequestHeaders.Add(obj.client, obj.value);

            var stringContent = new StringContent(obj.parameter.ToString(), UnicodeEncoding.UTF8, "application/json");

            using (var response = await client.PostAsync($"ewbreqproc.aspx?action={obj.action}&userid={obj.userid}&gstin={obj.gstin}&action2={obj.action2}&token={obj.token}&App={obj.App}&prod={obj.prod}", stringContent))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                var result = content.ToString();
                return result;
            }
        }

        public async Task<string> CancelEwayBillandEInvoice(EwayBillEntity obj)
        {
            Uri BaseAddress = new Uri("https://compuewaybill.azurewebsites.net/");
            var client = new HttpClient { BaseAddress = BaseAddress };

            //var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            //if (authorizationRequestHeader.Any())
            //{
            //    client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            //}

            client.DefaultRequestHeaders.Add(obj.client, obj.value);

            var stringContent = new StringContent(obj.parameter.ToString(), UnicodeEncoding.UTF8, "application/json");

            using (var response = await client.PostAsync($"ewbreqproc.aspx?action={obj.action}&userid={obj.userid}&gstin={obj.gstin}&action2={obj.action2}&token={obj.token}&App={obj.App}&prod={obj.prod}", stringContent))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                var result = content.ToString();
                return result;
            }
        }

        public async Task<string> CancelEwayBill(EwayBillEntity obj)
        {
            Uri BaseAddress = new Uri("https://compuewaybill.azurewebsites.net/");
            var client = new HttpClient { BaseAddress = BaseAddress };

            //var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            //if (authorizationRequestHeader.Any())
            //{
            //    client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            //}

            client.DefaultRequestHeaders.Add(obj.client, obj.value);

            var stringContent = new StringContent(obj.parameter.ToString(), UnicodeEncoding.UTF8, "application/json");

            using (var response = await client.PostAsync($"ewbreqproc.aspx?action={obj.action}&userid={obj.userid}&gstin={obj.gstin}&action2={obj.action2}&token={obj.token}&App={obj.App}&prod={obj.prod}", stringContent))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                var result = content.ToString();
                return result;
            }
        }
    }

    class Test
    {

        String test;

        String getTest() { return test; }
        void setTest(String test) { this.test = test; }

    }
}
