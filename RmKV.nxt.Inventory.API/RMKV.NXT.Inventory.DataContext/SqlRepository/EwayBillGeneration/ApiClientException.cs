﻿using System;
using System.Net.Http;
using System.Runtime.Serialization;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.EwayBillGeneration
{
    [Serializable]
    internal class ApiClientException : Exception
    {
        private HttpResponseMessage response;

        public ApiClientException()
        {
        }

        public ApiClientException(HttpResponseMessage response)
        {
            this.response = response;
        }

        public ApiClientException(string message) : base(message)
        {
        }

        public ApiClientException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ApiClientException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}