﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement
{
   public class WarehouseGatepassRepository : RepositoryBase, IWarehouseGatepassRepository
    {
        public WarehouseGatepassRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<string> GetWarehouseGatePassType(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_transfer_type_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetTransferDetails(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_gatepass_get_transfers_by_transfer_type",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetGatepassTowarehouse(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_gatepass_get_destination_names",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GatepassNo(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_gatespass_no_generation",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GatepassToLocation(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_gatepass_get_to_location",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> AddWarehouseGatePass(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<int>($"nxtwarehouse.fn_gatepass_save",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }
        public async Task<Table2> FetchWarehouseGatePass(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table2>($"nxtwarehouse.fn_gatepass_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
        public async Task<string> GetWarehouseGatePass(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_gatepass_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> CancelWarehouseGatePass(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<int>($"nxtwarehouse.fn_gatepass_cancel",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }
    }
}
