﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.ReturnableTransferReports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableTransferReports;
using RMKV.NXT.Inventory.DataModel.Model;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.ReturnableTransferReports
{ 
    public class ReturnableTransferReportsRepository : RepositoryBase, IReturnableTransferReportsRepository
    {
        public ReturnableTransferReportsRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<DataSet> GetWorkOrderIssueListReport(GetWorkOrderIssueListEntity objWoi)
        {
            var dataset = new DataSet();
            var dataTable = new DataTable();

            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objWoi.WorkOrderIssueListReportView);

            var result = await Connection.ExecuteReaderAsync($"", values,
             commandType: CommandType.StoredProcedure, transaction: Transaction);
            dataTable.Load(result);
            dataset.Tables.Add(dataTable);
            return dataset;
        }

        public async Task<DataSet> GetPolishPendingReport(string objPolish)
        {
            var dataset = new DataSet();
            var dataTable = new DataTable();

            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPolish);

            var result = await Connection.ExecuteReaderAsync($"nxtwarehouse.fn_frx_pending_polish_issue_list", values,
             commandType: CommandType.StoredProcedure, transaction: Transaction);
            dataTable.Load(result);
            dataset.Tables.Add(dataTable);
            return dataset;
        }
    }
}