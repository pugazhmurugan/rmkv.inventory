﻿using Dapper;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.WarehouseGatepassAcknowledgment;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.WarehouseGatepassAcknowledgement;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.WarehouseGatepassAcknowledgment
{
    public class WarehouseGatePassAckRepository : RepositoryBase, IWarehouseGatepassAckRepository
    {
        public WarehouseGatePassAckRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<List<WarehouseGatePassEntity>> GetWarehouseGatePass(string objInwardGatePass)
        {
            List<WarehouseGatePassEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objInwardGatePass);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtgrn.fn_inward_gatepass_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response.Records != null)
                result = JsonConvert.DeserializeObject<List<WarehouseGatePassEntity>>(response.Records);
            return result;
        }

        public async Task<List<GatePassEntity>> GetGatePass(string objInwardGatePass)
        {
            List<GatePassEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objInwardGatePass);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtgrn.fn_inward_gatepass_no_details_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response.Records != null)
                result = JsonConvert.DeserializeObject<List<GatePassEntity>>(response.Records);
            return result;
        }

        public async Task<List<GetWarehouseTransferDetailEntity>> GetWarehouseTransferDetail(string objInwardGatePass)
        {
            List<GetWarehouseTransferDetailEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objInwardGatePass);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtgrn.fn_inward_gatepass_transfer_details_fetch", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response.Records != null)
                result = JsonConvert.DeserializeObject<List<GetWarehouseTransferDetailEntity>>(response.Records);
            return result;
        }
        public async Task<List<GetSameWarehouseEntity>> GetStoreLocation(string objInwardGatePass)
        {
            List<GetSameWarehouseEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objInwardGatePass);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtgrn.fn_same_hr_location_store_warehouse_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response.Records != null)
                result = JsonConvert.DeserializeObject<List<GetSameWarehouseEntity>>(response.Records);
            return result;
        }

        public async Task<string> GetTransferTypeDetails(string company)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", company);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_transfer_type_list_for_store_inward",
                 datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetWarehouseGatePassDetails(string company)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", company);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtgrn.fn_inward_gatepass_list_gatepass_details",
                 datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }



        public async Task<string> LoadWarehouseGatePassNo(string objInwardGatePass)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objInwardGatePass);
            var result = await Connection.ExecuteScalarAsync<string>($"nxtgrn.fn_inward_gatepass_list_gatepass_no", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        //public async Task<List<GetSameWarehouseEntity>> GetStoreLocation(string objGatepass)
        //{
        //    List<GetSameWarehouseEntity> result = null;
        //    DynamicParameters values = new DynamicParameters();
        //    values.Add("@v_txt", objGatepass);
        //    var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtgrn.fn_same_hr_location_store_warehouse_list", values,
        //    commandType: CommandType.StoredProcedure, transaction: Transaction);
        //    if (response.Records != null)
        //        result = JsonConvert.DeserializeObject<List<GetSameWarehouseEntity>>(response.Records);
        //    return result;
        //}

        public async Task<List<GetSameWarehouseEntity>> GetSameWarehouse(string objGatepass)
        {
            List<GetSameWarehouseEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objGatepass);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtgrn.fn_same_hr_location_warehouse_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response.Records != null)
                result = JsonConvert.DeserializeObject<List<GetSameWarehouseEntity>>(response.Records);
            return result;
        }

        public async Task<string> LoadWarehouseAckGRNNoList(string obj)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", obj);
            var result = await Connection.ExecuteScalarAsync<string>($"nxtgrn.fn_warehouse_ack_grn_no_list", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<string> LoadWarehouseAckGatePassDetails(string obj)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", obj);
            var result = await Connection.ExecuteScalarAsync<string>($"nxtgrn.fn_warehouse_ack_gatepass_details_fetch", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<int> SaveWarehouseGatePass(string objInwardGatePass)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objInwardGatePass);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtgrn.fn_warehouse_ack_gatepass_details_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }
    }

}
