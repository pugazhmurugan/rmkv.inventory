﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfer;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.GoodsTransfer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.GoodsTransfer
{
    public class GoodsTransferRepository : RepositoryBase, IGoodsTransferRepository
    {
        public GoodsTransferRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<string> GetWithincityVirtualToStore(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_withincity_virtual_tostore_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetGoodsTransferType(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_get_goods_transfer_types",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetWithinStateToLocation(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_withinstate_tolocation_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetOtherStateToLocation(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_otherstate_tolocation_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;

        }
        public async Task<string> GetWithinStateVirtualStoreToStore(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_withinstate_virtual_storetostore_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GoodsTransferFromStore(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_within_state_store_to_warehouse_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        ////////////////////////////////////////Goods Transfer (with In City)/////////////////////////////////////////
        public async Task<int> AddWithincity(string GoodsTransfer)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", GoodsTransfer);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_gt_transfer_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }
        public async Task<bool> ApproveWithincity(string GoodsTransfer)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", GoodsTransfer);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_goods_transfer_approve", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<int> GetTransferNoWithincity(string GoodsTransfer)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", GoodsTransfer);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_get_transfer_no", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<bool> CancelWithincity(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_goods_transfer_cancel",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result); ;
        }
        public async Task<Table1> FetchWithincity(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table1>($"nxtwarehouse.fn_goods_transfer_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
        public async Task<string> GetWithincity(string GoodsTransfer)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GoodsTransfer);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_goods_transfer_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        /// <summary>
        /// ////////////////  Goods Transfer Report
        /// </summary>
        /// 
        //public async Task<int> CheckGoodsTransferReport(string GoodsTransfer)
        //{
        //    DynamicParameters datas = new DynamicParameters();
        //    datas.Add("@v_txt", GoodsTransfer);
        //    var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_frx_check_all_goods_transfer_report",
        //        datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
        //    return response.Records;
        //}

        public async Task<int> CheckGoodsTransferReport(string GoodsTransfer)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", GoodsTransfer);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_frx_check_all_goods_transfer_report", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<DataSet> GoodsTransferReport(GoodsTransferEntity obj)
        {
            var dataset = new DataSet();
            var dataTable = new DataTable();
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", obj.GoodsTransferReport);
            if (obj.Type == true)
            {
                var result = await Connection.ExecuteReaderAsync($"nxtwarehouse.fn_frx_all_goods_transfer_report", values,
                commandType: CommandType.StoredProcedure, transaction: Transaction);
                dataTable.Load(result);
                dataset.Tables.Add(dataTable);
                return dataset;
            }
            else
            {
                var result = await Connection.ExecuteReaderAsync($"nxtwarehouse.fn_frx_all_goods_transfer_report_with_supplier", values,
                commandType: CommandType.StoredProcedure, transaction: Transaction);
                dataTable.Load(result);
                dataset.Tables.Add(dataTable);
                return dataset;
            }
           
        }

    }
}