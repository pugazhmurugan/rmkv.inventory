﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.ReturnableGoodsTransfer
{
    public class ProductionIssueRepository : RepositoryBase, IProductionIssueRepository
    {
        public ProductionIssueRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<string> GetProductionIssue(string objPI)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", objPI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_issue_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<int> AddProductionIssue(string objPI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPI);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_production_issue_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToInt32(result);
        }
        public async Task<Table2> FetchProductionIssue(string objPI)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", objPI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table2>($"nxtwarehouse.fn_production_issue_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
        public async Task<bool> CancelProductionIssue(string objPI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPI);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_production_issue_cancel", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<string> GetProductionIssueReconciliation(string objPI)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", objPI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_issue_reconciliation_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<Table2> GetProductionIssueEstimatedVsReceived(string objPI)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", objPI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table2>($"nxtwarehouse.fn_production_issue_reconciliation_view",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
        public async Task<int> AddProductionIssueReconciliation(string objPI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objPI);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_production_issue_reconciliation_update", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToInt32(result);
        }
    }
}
