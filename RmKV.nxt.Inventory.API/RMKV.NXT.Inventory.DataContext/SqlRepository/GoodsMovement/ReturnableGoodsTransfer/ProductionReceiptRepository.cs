﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.ReturnableGoodsTransfer
{
    public class ProductionReceiptRepository : RepositoryBase, IProductionReceiptRepository
    {
        public ProductionReceiptRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<string> GetProductionReceipts(string productionReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", productionReceipts);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_receipts_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetProductionIssueNos(string productionReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", productionReceipts);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_receipt_pending_issue_no",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetProductionIssueDetails(string productionReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", productionReceipts);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_receipt_pending_issue_details",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetProductionReceiptProductLookup(string productionReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", productionReceipts);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_receipt_pending_issue_details_lookup",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> AddProductionReceipt(string productionReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", productionReceipts);
             var response = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_production_receipt_save",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }
        public async Task<Table2> FetchProductionReceipt(string productionReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", productionReceipts);
            var response = await Connection.QueryFirstOrDefaultAsync<Table2>($"nxtwarehouse.fn_production_receipt_fetch",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
        public async Task<bool> RemoveProductionReceipt(string productionReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", productionReceipts);
            var response = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_production_receipt_cancel",
               values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }
    }
}
