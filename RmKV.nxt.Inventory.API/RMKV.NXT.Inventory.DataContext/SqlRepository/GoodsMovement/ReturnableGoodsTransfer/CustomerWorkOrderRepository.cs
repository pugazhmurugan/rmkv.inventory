﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.ReturnableGoodsTransfer
{
    public class CustomerWorkOrderRepository : RepositoryBase, ICustomerWorkOrderRepository
    {
        public CustomerWorkOrderRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<string> GetCustomerWorkOrder(string objCWO)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", objCWO);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_work_order_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<int> AddCustomerWorkOrder(string objCWO)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objCWO);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_work_orders_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToInt32(result);
        }
        public async Task<string> FetchCustomerWorkOrder(string objCWO)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", objCWO);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_work_order_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> CancelCustomerWorkOrder(string objCWO)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objCWO);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_work_order_cancel", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
    }
}
