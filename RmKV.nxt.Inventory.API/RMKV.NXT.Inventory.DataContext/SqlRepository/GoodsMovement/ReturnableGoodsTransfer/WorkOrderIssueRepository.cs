﻿using Dapper;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.ReturnableGoodsTransfer.WorkOrderIssue;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.ReturnableGoodsTransfer
{
    public class WorkOrderIssueRepository : RepositoryBase, IWorkOrderIssueRepository
    {
        public WorkOrderIssueRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<bool> AddWorkOrderIssue(string objWOI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objWOI);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_wo_issues_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<Table4> FetchWorkOrderIssue(string objWOI)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", objWOI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table4>($"nxtwarehouse.fn_wo_issue_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }

        //public async Task<List<GetWorkOrderIssuetransferTypeEntity>> GetTransferTypeList()
        //{
        //    var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_work_order_transfer_type_list",
        //          commandType: CommandType.StoredProcedure, transaction: Transaction);
        //    var result = JsonConvert.DeserializeObject<List<GetWorkOrderIssuetransferTypeEntity>>(response.Records);
        //    return result;
        //}

        public async Task<List<GetWorkOrderIssuetransferTypeEntity>> GetTransferTypeList()
        {
            List<GetWorkOrderIssuetransferTypeEntity> result = null;
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_work_order_transfer_type_list",
                  commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response != null && response.Records != null)
                result = JsonConvert.DeserializeObject<List<GetWorkOrderIssuetransferTypeEntity>>(response.Records);
            return result;
        }

        public async Task<string> GetWorkOrderIssue(string objWOI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objWOI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_wo_issue_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetWorkOrderIssueDetails(string objWOI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objWOI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_wo_issue_fetch_details",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetWorkOrderIssueEntryGridList(string objWOI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objWOI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_work_order_issue_list_pending_wo",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        //workorderPendindlist//

        public async Task<string> GetWorkOrderpendingList(string objWOI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objWOI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_work_order_receipt_list_pending_wo",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetWorkOrderReceiptList(string objWOI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objWOI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_work_receipt_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> AddWorkOrderpendingList(string objWOI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objWOI);
            var response = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_wo_receipt_save",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.ToString();
        }

        public async Task<string> GetWorkOrderPendindSupplier(string objWOI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objWOI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_wo_receipt_get_pending_supplier",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetWorkOrderPendindListDetails(string objWOI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objWOI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_work_order_receipt_list_pending_wo_issue_details",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> FetchWorkOrderReceipt(string objWOI)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objWOI);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_work_order_receipt_fetch_details",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}
