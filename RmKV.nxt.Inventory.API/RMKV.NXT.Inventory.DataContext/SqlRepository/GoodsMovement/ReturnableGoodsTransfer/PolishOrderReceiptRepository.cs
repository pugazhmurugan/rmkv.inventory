﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.ReturnableGoodsTransfer
{
    public class PolishOrderReceiptRepository : RepositoryBase, IPolishOrderReceiptRepository
    {
        public PolishOrderReceiptRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<string> GetPolishOrderReceipts(string polishReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", polishReceipts);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_polish_receipts_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetPendingPolishIssueNos(string polishReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", polishReceipts);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_polish_receipt_pending_issue_no",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetPendingPolishIssueDetails(string polishReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", polishReceipts);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_polish_receipt_pending_issue_details",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> AddPolishOrderReceipt(string polishReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", polishReceipts);
            var response = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_polish_receipt_save",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }
        public async Task<bool> RemovePolishOrderReceipt(string polishReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", polishReceipts);
            var response = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_polish_receipt_cancel",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }
        public async Task<Table1> FetchPolishOrderReceiptDetails(string polishReceipts)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", polishReceipts);
            var response = await Connection.QueryFirstOrDefaultAsync<Table1>($"nxtwarehouse.fn_polish_receipt_fetch",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
    }
}
