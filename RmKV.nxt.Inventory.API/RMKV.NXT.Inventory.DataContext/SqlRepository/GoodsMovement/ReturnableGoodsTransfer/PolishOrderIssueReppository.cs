﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.ReturnableGoodsTransfer
{
    public class PolishOrderIssueReppository : RepositoryBase, IPolishOrderIssueRepository
    {

        public PolishOrderIssueReppository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<bool> AddPolishOrder(string PolishOrder)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", PolishOrder);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_polish_issue_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<bool> DeletePolishOrder(string PolishOrder)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", PolishOrder);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_polish_issue_cancel", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<Table4> FetchPolishOrder(string PolishOrder)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", PolishOrder);
            var response = await Connection.QueryFirstOrDefaultAsync<Table4>($"nxtwarehouse.fn_polish_issue_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }

        public async Task<string> GetPolishOrderList(string PolishOrder)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", PolishOrder);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_polish_issue_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}
