﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsReceipts;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.Goods_Receipts
{

    public class GoodsTransferReceiptsRepository : RepositoryBase, IGoodsTransferReceiptsRepository
    {
        public GoodsTransferReceiptsRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<string> LoadGoodsReceiptDetails(string objAck)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAck);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_cg_transfer_ack_list", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> LoadGoodsReceiptsTransferNo(string objAck)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAck);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_goods_transfer_receipts_fetch_transfers", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> FetchGoodsTransferReceipts(string objAck)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAck);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_cg_transfer_ack_fetch", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<int> SaveGoodsTransferReceiptsDetails(string objSave)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objSave);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_gt_transfer_receipt_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<bool> CheckGoodsReceiptsTransferNo(string objSave)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objSave);
            var result = await Connection.ExecuteScalarAsync<bool>($"nxtwarehouse.fn_check_goods_transfer_in_bales", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<string> LoadGoodsReceiptsProductDetails(string objSave)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objSave);
            var result = await Connection.ExecuteScalarAsync<string>($"nxtwarehouse.fn_goods_transfer_receipts_fetch_product_details", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<string> GetAllWarehouseList(string obj)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", obj);
            var result = await Connection.ExecuteScalarAsync<string>($"nxtwarehouse.fn_all_warehouse_by_sales_location_id_with_virtual", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

    }
}
