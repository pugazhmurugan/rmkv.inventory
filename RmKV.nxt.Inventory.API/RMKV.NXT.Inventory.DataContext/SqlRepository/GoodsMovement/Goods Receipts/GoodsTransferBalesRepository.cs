﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsReceipts;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.Goods_Receipts
{
    public class GoodsTransferBalesRepository : RepositoryBase, IGoodsTransferBalesRepository
    {
        public GoodsTransferBalesRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<string> LoadGoodsTransferBales(string objAck)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAck);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_goods_transfer_in_bales_load_transfer", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<int> SaveGoodsTransferBales(string objSave)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objSave);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_goods_transfer_in_bales_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }
        public async Task<string> GetGoodsBalesSectionGRN(string objAck)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAck);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_goods_transfer_in_bales_fetch_section_grn", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}
