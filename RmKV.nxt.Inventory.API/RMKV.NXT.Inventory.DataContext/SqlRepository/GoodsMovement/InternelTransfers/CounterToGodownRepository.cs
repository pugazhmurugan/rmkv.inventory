﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfers;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.GoodsTransfers
{
   public class CounterToGodownRepository : RepositoryBase, ICounterToGodownRepository
    {
        public CounterToGodownRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<int> AddCounterToGodown(string ProductMaster)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProductMaster);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_cg_transfer_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }
        public async Task<bool> ApproveCounterToGodown(string ProductMaster)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProductMaster);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_cg_transfer_approve", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<bool> CancelCounterToGodown(string ProductMaster)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProductMaster);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_cg_transfer_cancel", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<string> GetToWarehouse(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_get_warehouse_by_company_section",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetCounterToGodown(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_cg_transfer_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<int> GetTransferNo(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_cg_transfer_get_max_no",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.RETURN_VALUE;
        }
        public async Task<string> FetchCounterToGodown(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_cg_transfer_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<int> CheckCounterToGodownReport(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<int>($"nxtwarehouse.fn_frx_check_all_goods_transfer_report",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
        public async Task<DataSet> GetCounterToGodownReport(string ProductMaster)
        {
            var dataset = new DataSet();
            var dataTable = new DataTable();

            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProductMaster);

            var result = await Connection.ExecuteReaderAsync($"nxtwarehouse.fn_frx_all_goods_transfer_report", values,
             commandType: CommandType.StoredProcedure, transaction: Transaction);
            dataTable.Load(result);
            dataset.Tables.Add(dataTable);
            return dataset;
        }
    }
}
