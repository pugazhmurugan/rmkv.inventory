﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfers;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.GoodsTransfers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.GoodsTransfers
{
   public class GodownToCounterRepository : RepositoryBase, IGodownToCounterRepository
    {
        public GodownToCounterRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<int> AddGodownToCounter(string GodownToCounter)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", GodownToCounter);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_gc_transfer_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToInt32(result);
        }
        public async Task<bool> ApproveGodownToCounter(string GodownToCounter)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", GodownToCounter);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_gc_transfer_approve", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<bool> CancelGodownToCounter(string GodownToCounter)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", GodownToCounter);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_gc_transfer_cancel", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<string> GetToLocation(string GodownToCounter)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GodownToCounter);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_gc_from_warehouse_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetGodownToCounter(string GodownToCounter)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GodownToCounter);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_gc_transfer_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        } 
        public async Task<int> GetTransferNo(string GodownToCounter)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GodownToCounter);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_gc_transfer_get_max_no",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.RETURN_VALUE;
        }
        public async Task<string> FetchGodownToCounter(string GodownToCounter)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GodownToCounter);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_gc_transfer_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<DataSet> GetGodownToCounterReport(string obj)
        {
            var dataset = new DataSet();
            var dataTable = new DataTable();

            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", obj);

            var result = await Connection.ExecuteReaderAsync($"nxtwarehouse.fn_frx_all_goods_transfer_report", values,
             commandType: CommandType.StoredProcedure, transaction: Transaction);
            dataTable.Load(result);
            dataset.Tables.Add(dataTable);
            return dataset;
        }
        public async Task<bool> CheckGodownToCounterReport(string GodownToCounter)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", GodownToCounter);
            var response = await Connection.QueryFirstOrDefaultAsync<int>($"nxtwarehouse.fn_frx_check_all_goods_transfer_report",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }

    }
}

