﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfers;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.GoodsTransfers
{
   
    public class CountertoGodownAcknowledgementRepository : RepositoryBase, ICountertoGodownAcknowledgementRepository
    {
        public CountertoGodownAcknowledgementRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<string> LoadCtoGDetails(string objAck)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAck);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_cg_transfer_ack_list", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> LoadTransferNoBasedData(string objAck)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAck);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_get_cg_transfer_ack_byno_details", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> FetchCtoGAcknowledgement(string objAck)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAck);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_cg_transfer_ack_fetch", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<int> SaveCtoGAcknowledgementDetails(string objSave)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objSave);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_cg_transfer_ack_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<bool> CheckCtoGAcknowledgementTransferNo(string objSave)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objSave);
            var result = await Connection.ExecuteScalarAsync<bool>($"nxtwarehouse.fn_cg_transfer_check_transfer_no", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }
    }
}
