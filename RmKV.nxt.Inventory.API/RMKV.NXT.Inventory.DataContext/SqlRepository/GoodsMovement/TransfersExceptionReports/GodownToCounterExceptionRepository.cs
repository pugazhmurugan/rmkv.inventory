﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.TransfersExceptionReports;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.TransfersExceptionReports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.TransfersExceptionReports
{
    public class GodownToCounterExceptionRepository : RepositoryBase, IGodownToCounterExceptionRepository
    {
        public GodownToCounterExceptionRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<DataSet> GodownToCounterExceptionReport(GodownToCounterExceptionEntity objGodownToCounter)
        {
            var dataset = new DataSet();
            var dataTable = new DataTable();
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objGodownToCounter.GodownToCounterException_List);
            if (objGodownToCounter.Type)
            {
                var result = await Connection.ExecuteReaderAsync($"nxtwarehouse.fn_frx_gc_exception_report_with_details", values,
                commandType: CommandType.StoredProcedure, transaction: Transaction);
                dataTable.Load(result);
                dataset.Tables.Add(dataTable);
                //return dataset;
            }
            else
            {
                var result = await Connection.ExecuteReaderAsync($"nxtwarehouse.fn_frx_gc_exception_report_without_details", values,
                commandType: CommandType.StoredProcedure, transaction: Transaction);
                dataTable.Load(result);
                dataset.Tables.Add(dataTable);
                //return dataset;
            }
            return dataset;
        }
    }
}
