﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Purchases;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.Purchases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Purchases
{
    public class InvoicesRepository : RepositoryBase, IInvoicesRepository
    {
        public InvoicesRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<string> GetSupplierDescription(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_productdescription_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> CheckPriceCode(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_pricecode_check", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<string> InvoiceEntryConfig(int groupSectionId)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_group_section_id", groupSectionId);
            var result = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_load_invoice_entry_config", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result.Records;
        }
        public async Task<string> GetGstByHSN(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_find_supplier_tax_by_hsn",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetInvoiceNos(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_pending_supplier_invoices_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetMasterDiscount(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_supplier_discount",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetInvoicePoNos(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_invoice_po_number_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> AddInvoices(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_invoice_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<InvoicesTable> FetchInvoices(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<InvoicesTable>($"nxtwarehouse.fn_invoice_fetch", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
        public async Task<string> GetInvoices(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_invoices_list", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> ApproveInvoices(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_invoice_approval", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<bool> PostInvoices(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_invoice_posting", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<bool> EditCostPrice(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_invoice_cost_price_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<DataSet> GetInvoiceCheckListReport(string objInvoice)
        {
            var dataset = new DataSet();
            var dataTable = new DataTable();

            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objInvoice);

            var result = await Connection.ExecuteReaderAsync($"nxtwarehouse.fn_frx_invoice_checklist_report", values,
             commandType: CommandType.StoredProcedure, transaction: Transaction);
            dataTable.Load(result);
            dataset.Tables.Add(dataTable);
            return dataset;
        }
    }
}
