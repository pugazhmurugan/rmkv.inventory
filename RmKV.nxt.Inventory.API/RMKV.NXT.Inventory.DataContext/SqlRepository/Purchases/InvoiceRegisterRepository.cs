﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Purchases;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Purchases
{
   public class InvoiceRegisterRepository : RepositoryBase, IInvoiceRegisterRepository
    {

        public InvoiceRegisterRepository(IDbTransaction transaction) : base(transaction)
        {

        }

        public async Task<bool> AddInvoiceRepository(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_invoice_register_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<bool> ApproveInvoiceRepository(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_invoice_register_approve", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<bool> DeleteInvoiceRepository(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_invoice_register_cancel", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<string> FetchProductGroups(string invoice)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_invoice_register_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetInvoiceRepository(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_invoice_register_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}
