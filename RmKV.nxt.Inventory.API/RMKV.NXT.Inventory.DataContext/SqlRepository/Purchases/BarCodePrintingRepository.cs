﻿using Dapper;
using Newtonsoft.Json;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Purchases;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Purchases
{
   public class BarCodePrintingRepository : RepositoryBase, IBarCodePrintingRepository
    {

        public BarCodePrintingRepository(IDbTransaction transaction) : base(transaction)
        {

        }

        public async Task<string> GetBarCodePrintingRepository(string BarCode)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", BarCode);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_barcode_printing_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GenerateBarcodePrintRepository(string BynoData)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", BynoData);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_byno_barcode_print_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            var result = JsonConvert.DeserializeObject<List<BarcodePrintEntity>>(response.Records);
            if(result != null)
            {
                var return_value = Utilities.GenerateBarCode(result);
                if(return_value == 1)
                {
                    return response.Records;
                }
                else
                {
                    return "Failed";
                }
            }
            else
            {
                return "No Data";
            }
        }

        public async Task<string> ImageBytes()
        {
            var imagePath = @"\\192.9.206.75\d$\5001.jpg";
            string path = imagePath;
            var photo = File.ReadAllBytes(path);
            var value = imagePath.Split(@"\");
            var contentType = MimeTypes.GetContentType(value[value.Length - 1]);
            var ext = imagePath.Split(@"\");
            var extension = ext[ext.Length - 1];
            var filenameArr = extension.Split(@".");
            var filename = filenameArr[0];

            var byteArray = new List<ImageByteArray>();

            var obj = new ImageByteArray
            {
                photo = photo,
                Type = contentType,
                extension = extension,
                FileName = filename
            };
            byteArray.Add(obj);

            var data = JsonConvert.SerializeObject(byteArray);

            return data;
        }

    }
}
