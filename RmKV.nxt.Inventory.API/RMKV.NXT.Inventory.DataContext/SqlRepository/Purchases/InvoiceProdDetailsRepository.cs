﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Purchases;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Purchases
{
   public class InvoiceProdDetailsRepository : RepositoryBase, IInvoiceProdDetailsRepository
    {
        public InvoiceProdDetailsRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<Table1> GetInvoiceProdDetails(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table1>($"nxtwarehouse.fn_get_bynoserial_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
        public async Task<bool> AddInvoiceProdDetails(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<int>($"nxtwarehouse.fn_ipd_save",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }
        public async Task<Table> GetInvoiceProductLookup(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_ipd_products_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
        public async Task<string> GetInvoiceProdAttributes(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table3>($"nxtwarehouse.fn_ipd_attributes_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Attributes;
        }
        public async Task<string> GetProductLookupDetails(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_ipd_product_details",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetCounterDetails(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_counters_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> FetchInvoiceProductDetails(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_ipd_fetch_prod_details_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> SectionWiseAttribute(int invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_company_section_id", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_load_section_attributes",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetAttributesValuesDetails(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_ipd_attributes_value_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetProductDetailsByBrandType(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_product_details_by_type_brand",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}
