﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Purchases;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.Purchases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Purchases
{
    public class InvoiceInBaleRepository : RepositoryBase, IInvoiceInBaleRepository
    {
        public InvoiceInBaleRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<string> GetInvoiceInBales(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_grn_invoices_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> AddInvoiceInBales(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_grn_invoices_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<ReturnTable> FetchInvoiceInBales(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<ReturnTable>($"nxtwarehouse.fn_grn_invoices_fetch",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
        public async Task<bool> DeleteInvoiceInBales(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_grn_invoices_delete", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<string> GetSectionGrn(string invoice)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", invoice);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_grn_invoices_section_grn_no_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}
