﻿using Dapper;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Pricing.SellingPriceChange;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.Pricing.SellingPriceChange;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Pricing.SellingPriceChange
{
    public class SingleProductRepository : RepositoryBase, ISingleProductRepository
    {
        public SingleProductRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<string> GetSingleProduct(string singleProduct)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", singleProduct);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_byno_selling_price_by_byno_list",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> AddSingleProduct(string singleProduct)
        {
            List<SingleProductEntity> singleProductList = JsonConvert.DeserializeObject<List<SingleProductEntity>>(singleProduct);
            bool[] allUpdated = new bool[singleProductList.Count];
            bool errorFound = true;
            for (var i = 0; i < singleProductList.Count; i++)
            {
                List<SingleProductEntity> singleProductArr = new List<SingleProductEntity>();
                singleProductArr.Add(singleProductList[i]);
                DynamicParameters values = new DynamicParameters();
                values.Add("@v_txt", JsonConvert.SerializeObject(singleProductArr));
                var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_wh_merge_selling_price_change", values,
                     commandType: CommandType.StoredProcedure, transaction: Transaction);
                allUpdated[i] = Convert.ToBoolean(result);
            }
            for(var i = 0; i < allUpdated.Length; i++)
            {
                if (allUpdated[i] == false)
                {
                    errorFound = false;
                    break;
                }
            }
            return errorFound;
        }

        public async Task<DataSet> GetPriceChangeHistoryReport(string SingleProduct)
        {
            var dataset = new DataSet();
            var dataTable = new DataTable();

            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", SingleProduct);

            var result = await Connection.ExecuteReaderAsync($"nxtwarehouse.fn_frx_price_change_history", values,
             commandType: CommandType.StoredProcedure, transaction: Transaction);
            dataTable.Load(result);
            dataset.Tables.Add(dataTable);
            return dataset;
        }
    }
}
