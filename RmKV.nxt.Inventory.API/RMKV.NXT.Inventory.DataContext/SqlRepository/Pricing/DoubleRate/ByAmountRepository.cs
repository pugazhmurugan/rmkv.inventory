﻿using Dapper;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Pricing.DoubleRate;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.Pricing.DoubleRate;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Pricing.DoubleRate
{
    public class ByAmountRepository : RepositoryBase, IByAmountRepository
    {
        public ByAmountRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<bool> AddByAmount(string byAmount)
        {
            List<ByAmountEntity> byAmountList = JsonConvert.DeserializeObject<List<ByAmountEntity>>(byAmount);
            bool[] allUpdated = new bool[byAmountList.Count];
            bool errorFound = true;
            for (var i = 0; i < byAmountList.Count; i++)
            {
                List<ByAmountEntity> byAmountArr = new List<ByAmountEntity>();
                byAmountArr.Add(byAmountList[i]);
                DynamicParameters values = new DynamicParameters();
                values.Add("@v_txt", JsonConvert.SerializeObject(byAmountArr));
                var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_wh_merge_price_change", values,
                     commandType: CommandType.StoredProcedure, transaction: Transaction);
                allUpdated[i] = Convert.ToBoolean(result);
            }
            for (var i = 0; i < allUpdated.Length; i++)
            {
                if (allUpdated[i] == false)
                {
                    errorFound = false;
                    break;
                }
            }
            return errorFound;
        }
        public async Task<bool> AddByPercentage(string byPercentage)
        {
            List<ByPercentageEntity> byPercentageList = JsonConvert.DeserializeObject<List<ByPercentageEntity>>(byPercentage);
            bool[] allUpdated = new bool[byPercentageList.Count];
            bool errorFound = true;
            for (var i = 0; i < byPercentageList.Count; i++)
            {
                List<ByPercentageEntity> byPercentagetArr = new List<ByPercentageEntity>();
                byPercentagetArr.Add(byPercentageList[i]);
                DynamicParameters values = new DynamicParameters();
                values.Add("@v_txt", JsonConvert.SerializeObject(byPercentagetArr));
                var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_wh_merge_price_change_for_doubleratepercent", values,
                     commandType: CommandType.StoredProcedure, transaction: Transaction);
                allUpdated[i] = Convert.ToBoolean(result);
            }
            for (var i = 0; i < allUpdated.Length; i++)
            {
                if (allUpdated[i] == false)
                {
                    errorFound = false;
                    break;
                }
            }
            return errorFound;
        }
        public async Task<bool> CheckMargin(string margin)
        {
                DynamicParameters values = new DynamicParameters();
                values.Add("@v_txt", margin);
                var response = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_double_margin_check", values,
                     commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }
        public async Task<string> GetDoubleRateMarginCalculation(string margin)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", margin);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_double_rate_margin_calculate",
                values, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}
