﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Pricing;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Pricing
{
    public class MRPChangeRepository : RepositoryBase, IMRPChangeRepository
    {
        public MRPChangeRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<bool> AddMrpChanges(string mrpChange)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", mrpChange);
            var response = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_mrp_change_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(response);
        }
    }
}
