﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Tools;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Tools
{
    public class ProductDescriptionChangeRepository : RepositoryBase, ISupplierDescriptionChangeRepository
    {
        public ProductDescriptionChangeRepository(IDbTransaction transaction) : base(transaction)
        {

        }

        public async Task<bool> addsupplierDescriptionChange(string SupplierDescription)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", SupplierDescription);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_supplier_desc_change_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
    }
}
