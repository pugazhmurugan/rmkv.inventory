﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Tools;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Tools
{
    public class ProductChangeRepository : RepositoryBase, IProductChangeRepository
    {
        public ProductChangeRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<string> GetProductChanges(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_product_code_lookup",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetStyleChanges(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_style_code_lookup",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetCounterChanges(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_counter_code_lookup",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<bool> counterchangesave(string ProductMaster)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProductMaster);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_counter_change_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<bool> productchangesave(string ProductMaster)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProductMaster);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_product_change_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<bool> stylecodechangesave(string ProductMaster)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProductMaster);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_style_code_change_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        ////Report

        public async Task<DataSet> GetProductChangeHistoryReport(string obj)
        {
            var dataset = new DataSet();
            var dataTable = new DataTable();

            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", obj);

            var result = await Connection.ExecuteReaderAsync($"nxtwarehouse.fn_frx_product_change_history", values,
             commandType: CommandType.StoredProcedure, transaction: Transaction);
            dataTable.Load(result);
            dataset.Tables.Add(dataTable);
            return dataset;
        }
        public async Task<DataSet> GetCounterChangeHistoryReport(string obj)
        {
            var dataset = new DataSet();
            var dataTable = new DataTable();

            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", obj);

            var result = await Connection.ExecuteReaderAsync($"nxtwarehouse.fn_frx_counter_change_history", values,
             commandType: CommandType.StoredProcedure, transaction: Transaction);
            dataTable.Load(result);
            dataset.Tables.Add(dataTable);
            return dataset;
        }
    }
}
