﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Masters
{
    public class ProductionMaterialsRepository : RepositoryBase, IProductionMaterialsRepository
    {
        public ProductionMaterialsRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<int> AddProductionMaterials(string ProductMaster)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProductMaster);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_production_materials_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<string> FetchProductionMaterials(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_materials_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetProductionMaterials()
        {
            DynamicParameters datas = new DynamicParameters();
            //datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_materials_list",
               commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        
    }
}