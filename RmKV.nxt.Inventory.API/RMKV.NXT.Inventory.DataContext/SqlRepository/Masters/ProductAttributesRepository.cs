﻿using Dapper;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Masters
{
    public class ProductAttributesRepository : RepositoryBase, IProductAttributesRepository
    {
        public ProductAttributesRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<int> AddProductAttributes(AddProductAttributesEntity objProduct)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objProduct.Product_Attribute_List);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_attributes_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<List<GetProductAttributesEntity>> ModifyProductAttributes(AddProductAttributesEntity objProduct)
        {
            List<GetProductAttributesEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objProduct.Product_Attribute_List);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_attributes_fetch", values,
              commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response != null && response.Records != null)
                result = JsonConvert.DeserializeObject<List<GetProductAttributesEntity>>(response.Records);
            return result;
        }

        public async Task<List<GetProductAttributesEntity>> GetProductAttributes(GetProductAttributesParams objProduct)
        {
            List<GetProductAttributesEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_group_section_id", objProduct.Group_Section_Id);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_attributes_list", values,
              commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response != null && response.Records != null)
                result = JsonConvert.DeserializeObject<List<GetProductAttributesEntity>>(response.Records);
            return result;
        }
        public async Task<int> AddProductAttributeValues(AddProductAttributesEntity objProduct)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objProduct.Product_Attribute_List);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_attribute_values_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }
        public async Task<List<GetProductAttributeValuesEntity>> ModifyProductAttributeValues(AddProductAttributesEntity objProduct)
        {
            List<GetProductAttributeValuesEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objProduct.Product_Attribute_List);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_attributes_value_fetch", values,
              commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response != null && response.Records != null)
                result = JsonConvert.DeserializeObject<List<GetProductAttributeValuesEntity>>(response.Records);
            return result;
        }
        public async Task<List<GetProductAttributeValuesEntity>> GetProductAttributeValues(AddProductAttributesEntity objProduct)
        {
            List<GetProductAttributeValuesEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objProduct.Product_Attribute_List);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_attributes_value_list", values,
              commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response != null && response.Records != null)
                result = JsonConvert.DeserializeObject<List<GetProductAttributeValuesEntity>>(response.Records);
            return result;
        }
    }
}
