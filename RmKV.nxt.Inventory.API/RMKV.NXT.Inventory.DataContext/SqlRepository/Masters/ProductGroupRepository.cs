﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Masters
{
    public class ProductGroupRepository :RepositoryBase,IProductGroupRepository
    {
        public ProductGroupRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<string> GetProductGroups(string productGroup)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", productGroup);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_product_groups_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> AddProductGroups(string productGroup)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", productGroup);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_product_groups_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<string> FetchProductGroups(string productGroup)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", productGroup);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_product_groups_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> DeleteProductGroups(string productGroup)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", productGroup);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_product_groups_active", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
    }
}
