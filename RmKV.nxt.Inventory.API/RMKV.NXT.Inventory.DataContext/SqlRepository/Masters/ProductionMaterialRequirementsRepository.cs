﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Masters
{
    public class ProductionMaterialRequirementsRepository : RepositoryBase, IProductionMaterialRequirementsRepository
    {
        public ProductionMaterialRequirementsRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<int> AddProdMaterialRequirement(string ProdMaterialRequirement)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProdMaterialRequirement);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_production_material_requirements_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }
        public async Task<string> GetProdMaterialRequirement()
        {
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_material_requirements_list",
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            var result = response.Records;
            return result;
        }
        public async Task<string> FetchProdMaterialRequirement(string ProdMaterialRequirement)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProdMaterialRequirement);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_material_requirements_fetch", datas,
             commandType: CommandType.StoredProcedure, transaction: Transaction) ;
            var result = response.Records;
            return result;
        }
    }
}
