﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Masters
{
   public class ProductMasterRepository : RepositoryBase, IProductMasterRepository
    {
        public ProductMasterRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<string> GetProductMasters(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_products_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetProductCounter(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_product_counter_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetProductAttributes(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_product_group_attributes_new_product",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<bool> AddProductMasters(string ProductMaster)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProductMaster);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_products_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<bool> DeleteProductMasters(string ProductMaster)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProductMaster);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_products_delete", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }
        public async Task<string> FetchProductMasters(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_products_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }




        public async Task<bool> addGoods(string ProductMaster)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProductMaster);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_gc_transfer_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return Convert.ToBoolean(result);
        }

        public async Task<string> GetToLocation(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_virtual_warehouse_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetGoods(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_gc_transfer_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<int> GetTransferNo(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_gc_transfer_get_max_no",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.RETURN_VALUE;
        }
        public async Task<string> FetchGoods(string ProductMaster)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProductMaster);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_gc_transfer_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}

