﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Masters
{
    public class ProductionProductRepository : RepositoryBase, IProductionProductsRepository
    {
        public ProductionProductRepository(IDbTransaction transaction) : base(transaction)
        {

        }
        public async Task<int> AddProductionProduct(string ProdProduct)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ProdProduct);
            var response = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_production_products_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
        public async Task<string> GetProductionProduct()
        {
            DynamicParameters values = new DynamicParameters();
           // values.Add("@v_txt", ProdProduct);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_products_list",
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetProductionProductDetails(string ProdProduct)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", ProdProduct);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_products_fetch",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
        public async Task<string> GetProductionProductLookup()
        {
            DynamicParameters values = new DynamicParameters();
            // values.Add("@v_txt", ProdProduct);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_production_products_lookup_list",
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }
    }
}
