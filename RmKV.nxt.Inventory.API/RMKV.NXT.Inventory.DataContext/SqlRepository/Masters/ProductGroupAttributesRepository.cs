﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Masters
{
    public class ProductGroupAttributesRepository : RepositoryBase, IProductGroupAttributesRepository
    {
        public ProductGroupAttributesRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<string> GetAttributeLookup(string objAttribute)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAttribute);
            var result = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_attributes_lookup", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result.Records;
        }

        public async Task<string> GetProductGroupAttributeList(string objAttribute)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAttribute);
            var result = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_product_group_attributes_list", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result.Records;
        }

        public async Task<string> ProductGroupAttributeFetch(string objAttribute)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAttribute);
            var result = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_product_group_attributes_fetch", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result.Records;
        }

        public async Task<string> GetProductGroupLookup(string objAttribute)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAttribute);
            var result = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_product_groups_list_lookup", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result.Records;
        }

        public async Task<int> RemoveProductGroupAttributes(string objAttribute)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAttribute);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_product_group_attributes_delete", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<int> SaveProductGroupAttributes(string objAttribute)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objAttribute);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_product_group_attributes_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }
    }
    }
