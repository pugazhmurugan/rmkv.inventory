﻿using Dapper;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Masters
{
   public class ProductCategoryRepository : RepositoryBase, IProductCategoryRepository
    {
        public ProductCategoryRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<List<FetchProductCategoryListEntity>> FetchCategory(FetchProductCategoryParmsEntity objCategory)
        {
            List<FetchProductCategoryListEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objCategory.FetchCategory);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_product_categories_fetch", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response.Records != null)
                result = JsonConvert.DeserializeObject<List<FetchProductCategoryListEntity>>(response.Records);
            return result;
        }

        public async Task<List<GetProductCategoryListEntity>> GetCategory(GetProductCategoryParmsEntity objCategory)
        {
            List<GetProductCategoryListEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_group_section_id", objCategory.Group_Section_Id);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_product_categories_list", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response.Records != null)
                result = JsonConvert.DeserializeObject<List<GetProductCategoryListEntity>>(response.Records);
            return result;
        }

        public async Task<int> RemoveCategory(RemoveProductCategoryListEntity objCategory)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objCategory.CancelCategory);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_product_categories_delete", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<int> SaveCategory(SaveProductCategoryEntity objCategory)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objCategory.SaveCategories);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_product_categories_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }


    }
}
