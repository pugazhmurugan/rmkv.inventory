﻿using Dapper;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.Masters
{
   public class HSNMasterRepository : RepositoryBase, IHSNMasterRepository
    {
        public HSNMasterRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<List<HSNMasterListEntity>> GetHSNMasterList()
        {
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_hsn_master_list",
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            var result = JsonConvert.DeserializeObject<List<HSNMasterListEntity>>(response.Records);
            return result;
        }

        public async Task<List<FetchHSNMasterListEntity>> HSNMasterFetch(FetchHSNMasterParamsEntity objHSN)
        {
            List<FetchHSNMasterListEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objHSN.FetchHSN);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_hsn_master_fetch", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response.Records != null)
                result = JsonConvert.DeserializeObject<List<FetchHSNMasterListEntity>>(response.Records);
            return result;
        }

        public async Task<List<FetchHSNTaxRateIndividualListEntity>> HSNMasterFetchIndividual(FetchHSNTaxRateIndividualParamsEntity objHSN)
        {
            List<FetchHSNTaxRateIndividualListEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objHSN.FetchHSNTaxRateIndividual);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_hsn_tax_rate_individual_fetch", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response.Records != null)
                result = JsonConvert.DeserializeObject<List<FetchHSNTaxRateIndividualListEntity>>(response.Records);
            return result;
        }

        public async Task<List<FetchHSNTaxRateListEntity>> HSNTaxRateFetch(FetchHSNTaxRateParamsEntity objHSN)
        {
            List<FetchHSNTaxRateListEntity> result = null;
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objHSN.FetchHSNTaxRate);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_hsn_tax_rate_fetch", values,
            commandType: CommandType.StoredProcedure, transaction: Transaction);
            if (response.Records != null)
                result = JsonConvert.DeserializeObject<List<FetchHSNTaxRateListEntity>>(response.Records);
            return result;
        }

        public async Task<int> RemoveHsnMaster(RemoveHSNMasterEntity objHSN)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objHSN.CancelHSN);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_hsn_master_delete", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<int> RemoveHsnTaxRate(RemoveHSNTaxRateEntity objHSN)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objHSN.RemoveHSNTaxRate);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_hsn_tax_rates_delete", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<int> SaveHSN(SaveHSNMasterEntity objHSN)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objHSN.SaveHSN);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_hsn_master_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<int> SaveHSNTax(SaveHSNTaxEntity objHSN)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objHSN.SaveHSNTax);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtcommon.fn_hsn_tax_rates_save", values,
                 commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }
    }
}
