﻿using Dapper;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseOrders;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.PurchaseOrders
{
    public class PurchaseOrdersRepository : RepositoryBase, IPurchaseOrdersRepository
    {
        public PurchaseOrdersRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        public async Task<string> GetPOAnalysis(string objPO)
        {
            DynamicParameters datas = new DynamicParameters();
            datas.Add("@v_txt", objPO);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_po_analysis_list",
                datas, commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetSupplierCityDetails()
        {
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_po_suppliercity_list",
              commandType: CommandType.StoredProcedure, transaction: Transaction);
            var result = response.Records;
            return result;
        }

        public async Task<string> GetSupplierGroupDetails(string objSupplier)
      {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objSupplier);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_po_supplier_group_list",values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetSupplierByGroupId(string objSupplier)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objSupplier);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_po_supplier_list_by_group_id",values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetModeofDispatchMasterList(string objSupplier)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", objSupplier);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtwarehouse.fn_mode_of_dispatch_master_list",values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetUOMList(int section_id)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_section_id", section_id);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtcommon.fn_get_uom_name_list", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetPurchaseOrderList(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_purchase_order_list", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<int> CancelPurchaseOrderList(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtpo.fn_purchase_order_cancel", values,
                commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<int> PurchaseOrderList(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtwarehouse.fn_purchase_order_cancel", values,
                commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<int> SavePurchaseOrderList(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtpo.fn_purchase_order_save", values,
                commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<string> FetchPurchaseOrderList(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_purchase_order_fetch", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetPoDispatchLocationList(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_po_saleslocation_select", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetPurchaseOrderSizeName(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_get_purchase_order_size_name", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetPurchaseOrderSizeNameBasedSizes(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_get_purchase_order_size_name_sizes", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetPurchaseOrderNo(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<string>($"nxtpo.fn_get_purchase_order_no", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }

        public async Task<string> GetPurchaseOrderEmail(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<string>($"nxtpo.fn_purchase_order_email", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }
    }
    }
