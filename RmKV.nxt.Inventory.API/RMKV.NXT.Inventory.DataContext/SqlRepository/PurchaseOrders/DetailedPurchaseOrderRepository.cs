﻿using Dapper;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseOrders;
using RMKV.NXT.Inventory.DataContext.RepositoryBases;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.SqlRepository.PurchaseOrders
{
    public class DetailedPurchaseOrderRepository : RepositoryBase, IDetailedPurchaseOrderRepository
    {
        public DetailedPurchaseOrderRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<int> SaveDetailedPurchaseOrder(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtpo.fn_detail_purchase_order_save", values,
                commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<int> CancelDetailedPurchaseOrder(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var result = await Connection.ExecuteScalarAsync<int>($"nxtpo.fn_detail_purchase_order_cancel", values,
                commandType: CommandType.StoredProcedure, transaction: Transaction);
            return result;
        }

        public async Task<string> FetchDetailedPurchaseOrder(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_detail_purchase_order_fetch", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetDetailedPurchaseOrderList(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_detail_purchase_order_list", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetBrandListForDetailedPO(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_get_brand_list", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetProductGroupListForDetailedPO(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_get_productgroup_list", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetProductListForDetailedPO(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_get_product_list", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetDesignCodeListForDetailedPO(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_get_style_code_list", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetColorListForDetailedPO(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_get_color_list", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetQualityListForDetailedPO(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_get_quality_list", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetProductSizeListForDetailedPO(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_get_size_list", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetPurchaseOrderLocations(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<Table>($"nxtpo.fn_po_saleslocation_select", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response.Records;
        }

        public async Task<string> GetDetailedPurchaseOrderNo(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<string>($"nxtpo.fn_get_detail_purchase_order_no", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }

        public async Task<string> GetDetailedPurchaseOrderEmail(string ObjList)
        {
            DynamicParameters values = new DynamicParameters();
            values.Add("@v_txt", ObjList);
            var response = await Connection.QueryFirstOrDefaultAsync<string>($"nxtpo.fn_detail_purchase_order_email", values,
           commandType: CommandType.StoredProcedure, transaction: Transaction);
            return response;
        }

    }
   }
