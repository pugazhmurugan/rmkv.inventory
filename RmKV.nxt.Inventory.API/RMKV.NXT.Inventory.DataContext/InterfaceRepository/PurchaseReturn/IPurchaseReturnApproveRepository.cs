﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseReturn
{
    public interface IPurchaseReturnApproveRepository
    {
        Task<string> GetApprovedSupplier(string objPRApprove);
        Task<string> GetPurchaseRetunApprovedList(string objPRApprove);
        Task<bool> RemovPurchaseReturn(string objPRApprove);
        Task<string> GetSupplierCityForApprove(string objPRApprove);
        Task<string> GetSupplierGroupNameForApprove(string objPRApprove);
        Task<string> GetSupplierNameForApprove(string objPRApprove);
        Task<string> GeSupplierDetailsForApprove(string objPRApprove);
        Task<bool> ApprovePurchaseReturnList(string objPRApprove);
    }
}
