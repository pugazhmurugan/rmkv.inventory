﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseReturn
{
   public interface IPurchaseReturnRepository
    {

        Task<int> AddPurchaseReturn(string PurchaseReturn);
        Task<bool> ApprovePurchaseReturn(string PurchaseReturn);
        Task<bool> CancelPurchaseReturn(string PurchaseReturn);
        Task<string> GetPurchaseReturn(string PurchaseReturn);
        Task<string> FetchPurchaseReturn(string PurchaseReturn);
    }
}
