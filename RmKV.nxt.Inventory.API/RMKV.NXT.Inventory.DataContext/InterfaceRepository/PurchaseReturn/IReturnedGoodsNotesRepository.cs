﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseReturn
{
   public interface IReturnedGoodsNotesRepository
    {
        Task<string> GetRGNWiseDCDetails(string objPRApprove);
        Task<bool> AddRGNWiseDetails(string objPRApprove);
        Task<string> GetRGNWiseDetails(string objPRApprove);
        Task<string> FetchRGNWiseDetails(string objPRApprove);
        Task<string> GetDCDetails(string objPRApprove);
        Task<string> GetTransporter(string objPRApprove);
    }
}
