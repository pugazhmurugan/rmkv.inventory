﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseReturn
{
    public interface IPurchaseReturnDCRepository
    {
        Task<string> LoadPurchaseReturnDCDetails(string obj);
        Task<string> FetchPurchaseReturnDCDetails(string obj);
        Task<int> GeneratePurchaseReturnDCNo(string obj);
    }
}
