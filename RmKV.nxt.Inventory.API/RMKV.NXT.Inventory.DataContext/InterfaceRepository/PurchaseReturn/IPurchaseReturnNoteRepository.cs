﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseReturn
{
    public interface IPurchaseReturnNoteRepository
    {
        Task<string> GetPurchaseReturnNote(string returnNote);
        Task<string> GetPurchaseReturnByNos(string returnNote);
        Task<string> GetPurchaseReturnByNoDetails(string returnNote);
        Task<bool> AddPurchaseReturnNote(string returnNote);
        Task<string> FetchPurchaseReturnNote(string returnNote);
    }
}
