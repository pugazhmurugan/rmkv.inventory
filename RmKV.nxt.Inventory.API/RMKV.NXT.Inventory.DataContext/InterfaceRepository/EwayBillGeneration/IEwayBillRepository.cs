﻿using RMKV.NXT.Inventory.DataModel.Model.EwayBill;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.EwayBillGeneration
{
    public interface IEwayBillRepository
    {
        Task<string> EwayBillLogin(EwayBillEntity obj);
        Task<string> EwayBillValidateUser(EwayBillEntity obj);
        Task<string> EwayBillDetails(EwayBillEntity obj);
        Task<string> GenerateEwayBillandEInvoice(EwayBillEntity obj);
        Task<string> GenerateEwayBill(EwayBillEntity obj);
        Task<string> CancelEwayBillandEInvoice(EwayBillEntity obj);
        Task<string> CancelEwayBill(EwayBillEntity obj);
    }
}
