﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Pricing.SellingPriceChange
{
    public interface ISingleProductRepository
    {
        Task<string> GetSingleProduct(string singleProduct);
        Task<bool> AddSingleProduct(string singleProduct);

        Task<DataSet> GetPriceChangeHistoryReport(string SingleProduct); 
    }
}
