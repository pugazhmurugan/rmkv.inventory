﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Pricing.DoubleRate
{
    public interface IByAmountRepository
    {
        Task<bool> AddByAmount(string byAmount);
        Task<bool> AddByPercentage(string byPercentage);
        Task<bool> CheckMargin(string margin);
        Task<string> GetDoubleRateMarginCalculation(string margin);
    }
}
