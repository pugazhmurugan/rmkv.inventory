﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Pricing
{
    public interface IMRPChangeRepository
    {
        Task<bool> AddMrpChanges(string mrpChange);
    }
}
