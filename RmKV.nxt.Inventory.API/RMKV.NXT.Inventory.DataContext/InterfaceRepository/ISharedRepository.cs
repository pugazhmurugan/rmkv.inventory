﻿using RmKV.NXT.Inventory.DataModel.Model.Shared;
using RmKV.NXT.Inventory.DataModel.Model.User;
using RMKV.NXT.Inventory.DataModel.Model.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RmKV.NXT.Inventory.DataContext.InterfaceRepository
{
    public interface ISharedRepository
    {
        Task<List<string>> GetCompanySelect(int User_Id, int Sales_Location_Id);
        Task<string> GetHRDocumentRootPathAndUrl(int salesLocationId);
        long EncryptKey(string key);
        Task<DashboardTable> GetDashboardDetails(int warehouseId);
        Task<Users> CheckLockPassword(UserDTO objEntry);
        Task<string> GetWarehouse(string warehouse);
        Task<string> GetSection(string section);
        Task<string> GetByNoDetails(string fieldValue);
        Task<string> GetWarehouseBasedCompanySection(string objSave);
        Task<string> GetMaterialLookup(string lookup);
        Task<string> GetMaterialProductLookup(string lookup);
    }
}
