﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Tools
{
   public interface IProductChangeRepository
    {
        Task<string> GetProductChanges(string ProductMaster); 
        Task<string> GetStyleChanges(string ProductMaster); 
        Task<string> GetCounterChanges(string ProductMaster);
        Task<bool> counterchangesave(string ProductMaster);
        Task<bool> productchangesave(string ProductMaster);
        Task<bool> stylecodechangesave(string ProductMaster);
        ////Report
        Task<DataSet> GetProductChangeHistoryReport(string obj);
        Task<DataSet> GetCounterChangeHistoryReport(string obj);
    }
}
