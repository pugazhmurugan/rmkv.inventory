﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Tools
{
   public interface ISupplierDescriptionChangeRepository
    {
        Task<bool> addsupplierDescriptionChange(string SupplierDescription);
    }
}
