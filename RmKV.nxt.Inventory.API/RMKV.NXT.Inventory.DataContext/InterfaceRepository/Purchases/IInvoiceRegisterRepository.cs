﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Purchases
{
   public interface IInvoiceRegisterRepository
    {
        Task<string> GetInvoiceRepository(string invoice);
        Task<bool> AddInvoiceRepository(string invoice);
        Task<bool> DeleteInvoiceRepository(string invoice);
        Task<bool> ApproveInvoiceRepository(string invoice);
        Task<string> FetchProductGroups(string invoice);
    }
}
