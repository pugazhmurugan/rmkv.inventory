﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Purchases
{
   public interface IBarCodePrintingRepository
    {
        Task<string> GetBarCodePrintingRepository(string BarCode);
        Task<string> GenerateBarcodePrintRepository(string BynoData);
        Task<string> ImageBytes();
    }
}
