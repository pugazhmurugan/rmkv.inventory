﻿using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Purchases
{
   public interface IInvoiceProdDetailsRepository
    {
        Task<Table1> GetInvoiceProdDetails(string invoice);
        Task<bool> AddInvoiceProdDetails(string invoice);
        Task<Table> GetInvoiceProductLookup(string invoice);
        Task<string> GetInvoiceProdAttributes(string invoice);
        Task<string> GetProductLookupDetails(string invoice);
        Task<string> GetCounterDetails(string invoice);
        Task<string> FetchInvoiceProductDetails(string invoice);
        Task<string> SectionWiseAttribute(int invoice);
        Task<string> GetAttributesValuesDetails(string invoice);
        Task<string> GetProductDetailsByBrandType(string invoice);
    }
}
