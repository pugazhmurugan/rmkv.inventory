﻿using RMKV.NXT.Inventory.DataModel.Model.Purchases;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Purchases
{
    public interface IInvoiceInBaleRepository
    {
        Task<string> GetInvoiceInBales(string invoice);
        Task<bool> AddInvoiceInBales(string invoice);
        Task<bool> DeleteInvoiceInBales(string invoice);
        Task<string> GetSectionGrn(string invoice);
        Task<ReturnTable> FetchInvoiceInBales(string invoice);
    }
}
