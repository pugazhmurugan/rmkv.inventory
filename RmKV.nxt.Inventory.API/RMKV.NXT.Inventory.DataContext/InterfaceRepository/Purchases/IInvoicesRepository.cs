﻿using RMKV.NXT.Inventory.DataModel.Model.Purchases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Purchases
{
    public interface IInvoicesRepository
    {
        Task<string> GetSupplierDescription(string invoice);
        Task<bool> CheckPriceCode(string invoice);
        Task<string> InvoiceEntryConfig(int groupSectionId);
        Task<string> GetGstByHSN(string invoice);
        Task<string> GetInvoiceNos(string invoice);
        Task<string> GetMasterDiscount(string invoice);
        Task<string> GetInvoicePoNos(string invoice);
        Task<string> AddInvoices(string invoice);
        Task<InvoicesTable> FetchInvoices(string invoice);
        Task<string> GetInvoices(string invoice);
        Task<bool> ApproveInvoices(string invoice);
        Task<bool> PostInvoices(string invoice);
        Task<bool> EditCostPrice(string invoice);
        Task<DataSet> GetInvoiceCheckListReport(string objInvoice);
    }
}
