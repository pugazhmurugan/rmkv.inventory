﻿using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement
{
   public interface IWarehouseGatepassRepository
    {
        Task<string> GetWarehouseGatePassType(string GoodsTransfer);
        Task<bool> AddWarehouseGatePass(string GoodsTransfer);
        Task<Table2> FetchWarehouseGatePass(string GoodsTransfer);
        Task<string> GetWarehouseGatePass(string GoodsTransfer);
        Task<bool> CancelWarehouseGatePass(string GoodsTransfer);
        Task<string> GetTransferDetails(string GoodsTransfer);
        Task<string> GetGatepassTowarehouse(string GoodsTransfer);
        Task<string> GatepassNo(string GoodsTransfer);
        Task<string> GatepassToLocation(string GoodsTransfer);
    }
}
