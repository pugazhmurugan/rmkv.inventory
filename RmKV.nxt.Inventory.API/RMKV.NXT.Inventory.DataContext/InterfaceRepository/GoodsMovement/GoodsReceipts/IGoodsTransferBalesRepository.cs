﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsReceipts
{
    public interface IGoodsTransferBalesRepository
    {
        Task<string> LoadGoodsTransferBales(string objAck);
        Task<int> SaveGoodsTransferBales(string objSave);
        Task<string> GetGoodsBalesSectionGRN(string objAck);
    }
}
