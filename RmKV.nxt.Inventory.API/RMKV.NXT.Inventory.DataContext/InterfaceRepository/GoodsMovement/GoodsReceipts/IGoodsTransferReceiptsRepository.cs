﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsReceipts
{
    public interface IGoodsTransferReceiptsRepository
    {
        Task<string> LoadGoodsReceiptDetails(string objAck);
        Task<string> LoadGoodsReceiptsTransferNo(string objAck); 
        Task<string> FetchGoodsTransferReceipts(string objAck);
        Task<int> SaveGoodsTransferReceiptsDetails(string objSave);
        Task<bool> CheckGoodsReceiptsTransferNo(string objSave);
        Task<string> LoadGoodsReceiptsProductDetails(string objSave);
        Task<string> GetAllWarehouseList(string obj);
    }
}
