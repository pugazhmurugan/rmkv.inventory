﻿using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer
{
    public interface IPolishOrderReceiptRepository
    {
        Task<string> GetPolishOrderReceipts(string polishReceipts);
        Task<string> GetPendingPolishIssueNos(string polishReceipts);
        Task<string> GetPendingPolishIssueDetails(string polishReceipts);
        Task<bool> AddPolishOrderReceipt(string polishReceipts);
        Task<bool> RemovePolishOrderReceipt(string polishReceipts);
        Task<Table1> FetchPolishOrderReceiptDetails(string polishReceipts);
    }
}
