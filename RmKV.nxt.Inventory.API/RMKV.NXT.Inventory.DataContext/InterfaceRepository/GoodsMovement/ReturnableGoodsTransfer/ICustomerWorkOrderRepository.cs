﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer
{
    public interface ICustomerWorkOrderRepository
    {
        Task<string> GetCustomerWorkOrder(string objCWO);
        Task<int> AddCustomerWorkOrder(string objCWO);
        Task<string> FetchCustomerWorkOrder(string objCWO);
        Task<bool> CancelCustomerWorkOrder(string objCWO);
    }
}
