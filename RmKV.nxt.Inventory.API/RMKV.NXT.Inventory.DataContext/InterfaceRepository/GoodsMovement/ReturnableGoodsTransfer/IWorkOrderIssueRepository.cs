﻿using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.ReturnableGoodsTransfer.WorkOrderIssue;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer
{
    public interface IWorkOrderIssueRepository
    {
        Task<string> GetWorkOrderIssue(string objWOI);
       
        Task<bool> AddWorkOrderIssue(string objWOI);
        Task<Table4> FetchWorkOrderIssue(string objWOI);
        Task<string> GetWorkOrderIssueEntryGridList(string objWOI);
        Task<string> GetWorkOrderIssueDetails(string objWOI);
        Task<List<GetWorkOrderIssuetransferTypeEntity>> GetTransferTypeList();
        //workorder receipt List//
        Task<string> GetWorkOrderPendindListDetails(string objWOI);
        Task<string> GetWorkOrderPendindSupplier(string objWOI);
        Task<string> GetWorkOrderReceiptList(string objWOI);
        Task<string> GetWorkOrderpendingList(string objWOI);
        Task<string> AddWorkOrderpendingList(string objWOI);
        Task<string> FetchWorkOrderReceipt(string objWOI);
    }
}
