﻿using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer
{
    public interface IPolishOrderIssueRepository
    {
        Task<string> GetPolishOrderList(string PolishOrder);
        Task<bool> AddPolishOrder(string PolishOrder);
        Task<bool> DeletePolishOrder(string PolishOrder);
        Task<Table4> FetchPolishOrder(string PolishOrder);
    }
}
