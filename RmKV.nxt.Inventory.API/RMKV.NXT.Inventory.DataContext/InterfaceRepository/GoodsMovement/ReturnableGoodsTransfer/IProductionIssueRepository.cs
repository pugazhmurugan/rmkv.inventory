﻿using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer
{
    public interface IProductionIssueRepository
    {
        Task<string> GetProductionIssue(string objPI);
        Task<int> AddProductionIssue(string objPI);
        Task<Table2> FetchProductionIssue(string objPI);
        Task<bool> CancelProductionIssue(string objPI);
        Task<string> GetProductionIssueReconciliation(string objPI);
        Task<Table2> GetProductionIssueEstimatedVsReceived(string objPI);
        Task<int> AddProductionIssueReconciliation(string objPI);
    }
}
