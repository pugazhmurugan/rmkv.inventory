﻿using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer
{
    public interface IProductionReceiptRepository
    {
        Task<string> GetProductionReceipts(string productionReceipts);
        Task<string> GetProductionIssueNos(string productionReceipts);
        Task<string> GetProductionIssueDetails(string productionReceipts);
        Task<string> GetProductionReceiptProductLookup(string productionReceipts);
        Task<bool> AddProductionReceipt(string productionReceipts);
        Task<Table2> FetchProductionReceipt(string productionReceipts);
        Task<bool> RemoveProductionReceipt(string productionReceipts);
    }
}
