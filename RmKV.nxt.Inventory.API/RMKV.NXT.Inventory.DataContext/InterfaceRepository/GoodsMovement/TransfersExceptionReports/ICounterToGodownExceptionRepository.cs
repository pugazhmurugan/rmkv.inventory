﻿using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.TransfersExceptionReports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.TransfersExceptionReports
{
    public interface ICounterToGodownExceptionRepository
    {
        Task<DataSet> CounterToGodownExceptionReport(CounterToGodownExceptionEntity objCounterToGodown);
    }
}
