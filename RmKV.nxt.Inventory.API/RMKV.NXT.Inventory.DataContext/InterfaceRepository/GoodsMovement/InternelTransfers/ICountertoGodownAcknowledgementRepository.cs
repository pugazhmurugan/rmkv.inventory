﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfers
{
    public interface ICountertoGodownAcknowledgementRepository
    {
        Task<string> LoadCtoGDetails(string objAck);
        Task<string> LoadTransferNoBasedData(string objAck);
        Task<string> FetchCtoGAcknowledgement(string objAck);
        Task<int> SaveCtoGAcknowledgementDetails(string objSave);
        Task<bool> CheckCtoGAcknowledgementTransferNo(string objSave);
    }
}
