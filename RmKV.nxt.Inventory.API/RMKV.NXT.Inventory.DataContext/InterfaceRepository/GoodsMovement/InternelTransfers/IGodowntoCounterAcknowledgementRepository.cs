﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfers
{
    public interface IGodowntoCounterAcknowledgementRepository
    {
        Task<string> LoadGtoCDetails(string objAck);
        Task<string> LoadTransferNoBasedData(string objAck);
        Task<int> SaveGtoCAcknowledgementDetails(string objSave);
        Task<string> FetchGtoCAcknowledgement(string objAck);
        Task<bool> CheckGtoCAcknowledgementTransferNo(string objSave);
    }
}
