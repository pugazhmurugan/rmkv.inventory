﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfers
{
  public  interface ICounterToGodownRepository
    {
        Task<int> AddCounterToGodown(string CounterToGodown);
        Task<bool> ApproveCounterToGodown(string CounterToGodown);
        Task<bool> CancelCounterToGodown(string CounterToGodown);
        Task<string> GetToWarehouse(string CounterToGodown);
        Task<string> GetCounterToGodown(string CounterToGodown);
        Task<int> GetTransferNo(string CounterToGodown);
        Task<string> FetchCounterToGodown(string CounterToGodown);

        Task<DataSet> GetCounterToGodownReport(string GetCounterToGodownReport);
        Task<int> CheckCounterToGodownReport(string CheckCounterToGodownReport);

    }
}
