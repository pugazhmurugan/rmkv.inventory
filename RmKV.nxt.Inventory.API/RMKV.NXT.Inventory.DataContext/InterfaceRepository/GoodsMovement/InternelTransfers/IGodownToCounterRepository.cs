﻿using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.GoodsTransfers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfers
{
   public interface IGodownToCounterRepository
    {
        Task<int> AddGodownToCounter(string GodownToCounter);
        Task<bool> ApproveGodownToCounter(string GodownToCounter);
        Task<bool> CancelGodownToCounter(string GodownToCounter);
        Task<string> GetToLocation(string GodownToCounter);
        Task<string> GetGodownToCounter(string GodownToCounter);
        Task<int> GetTransferNo(string GodownToCounter);
        Task<string> FetchGodownToCounter(string GodownToCounter);

        ///report
        Task<DataSet> GetGodownToCounterReport(string obj);
        Task<bool> CheckGodownToCounterReport(string GodownToCounter);
    }
}
