﻿using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.ReturnableTransferReports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableTransferReports
{
    public interface IReturnableTransferReportsRepository
    {
        Task<DataSet> GetWorkOrderIssueListReport(GetWorkOrderIssueListEntity objWoi);
        Task<DataSet> GetPolishPendingReport(string objPolish);
    }

}