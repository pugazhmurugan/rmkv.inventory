﻿using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.WarehouseGatepassAcknowledgement;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.WarehouseGatepassAcknowledgment
{
   public interface IWarehouseGatepassAckRepository
    {
        Task<List<GetSameWarehouseEntity>> GetSameWarehouse(string objGatepass);
        //Task<List<GetSameWarehouseEntity>> GetStoreLocation(string objGatepass);
        //Task<string> GetGatePassType(string company);

        Task<List<WarehouseGatePassEntity>> GetWarehouseGatePass(string objInwardGatePass);
        Task<List<GatePassEntity>> GetGatePass(string objInwardGatePass);
        Task<List<GetWarehouseTransferDetailEntity>> GetWarehouseTransferDetail(string objInwardGatePass);
        Task<List<GetSameWarehouseEntity>> GetStoreLocation(string objInwardGatePass);
        Task<string> GetTransferTypeDetails(string company);
        Task<string> GetWarehouseGatePassDetails(string company);
        Task<int> SaveWarehouseGatePass(string objInwardGatePass);
        Task<string> LoadWarehouseGatePassNo(string objInwardGatePass);
        Task<string> LoadWarehouseAckGRNNoList(string obj);
        Task<string> LoadWarehouseAckGatePassDetails(string obj);
    }
}
