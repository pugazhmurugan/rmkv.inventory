﻿using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.GoodsTransfer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfer
{
    public interface IGoodsTransferRepository
    {
        Task<string> GetWithincityVirtualToStore(string GoodsTransfer);
        Task<string> GetGoodsTransferType(string GoodsTransfer);
        Task<string> GetWithinStateToLocation(string GoodsTransfer);
        Task<string> GetOtherStateToLocation(string GoodsTransfer);
        Task<string> GetWithinStateVirtualStoreToStore(string GoodsTransfer);
        Task<string> GoodsTransferFromStore(string GoodsTransfer);

        ////////////////////////////////////////Goods Transfer (with In City)/////////////////////////////////////////
        Task<int> AddWithincity(string GoodsTransfer);
        Task<bool> CancelWithincity(string GoodsTransfer);
        Task<Table1> FetchWithincity(string GoodsTransfer);
        Task<string> GetWithincity(string GoodsTransfer);
        Task<int> GetTransferNoWithincity(string GoodsTransfer);
        Task<bool> ApproveWithincity(string GoodsTransfer);


        ///// Goods Transfer Report
        Task<int> CheckGoodsTransferReport(string GoodsTransfer);
            Task<DataSet> GoodsTransferReport(GoodsTransferEntity obj);


    } 
}