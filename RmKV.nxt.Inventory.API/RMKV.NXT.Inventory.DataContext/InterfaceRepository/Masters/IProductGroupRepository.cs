﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters
{
    public interface IProductGroupRepository
    {
        Task<string> GetProductGroups(string productGroup);
        Task<bool> AddProductGroups(string productGroup);
        Task<string> FetchProductGroups(string productGroup);
        Task<bool> DeleteProductGroups(string productGroup);
    }
}
