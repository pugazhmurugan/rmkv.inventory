﻿using System;
using System.Threading.Tasks;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters
{
    public interface IProductionMaterialsRepository
    {
        Task<int> AddProductionMaterials(string ProductMaterial);
        Task<string> GetProductionMaterials();

        Task<string> FetchProductionMaterials(string ProductMaterial);

    }
}
