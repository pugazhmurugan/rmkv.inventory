﻿using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters
{
   public interface IHSNMasterRepository
    {
        Task<int> SaveHSN(SaveHSNMasterEntity objHSN);
        Task<int> SaveHSNTax(SaveHSNTaxEntity objHSN);
        Task<List<FetchHSNMasterListEntity>> HSNMasterFetch(FetchHSNMasterParamsEntity objHSN);
        Task<List<FetchHSNTaxRateIndividualListEntity>> HSNMasterFetchIndividual(FetchHSNTaxRateIndividualParamsEntity objHSN);
        Task<List<FetchHSNTaxRateListEntity>> HSNTaxRateFetch(FetchHSNTaxRateParamsEntity objHSN);
        
        Task<int> RemoveHsnMaster(RemoveHSNMasterEntity objHSN);
        Task<int> RemoveHsnTaxRate(RemoveHSNTaxRateEntity objHSN);

        Task<List<HSNMasterListEntity>> GetHSNMasterList();
    }
}
