﻿using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters
{
    public interface IProductionMaterialRequirementsRepository
    {
        Task<int> AddProdMaterialRequirement(string ProdMaterialRequirement);
        Task<string> GetProdMaterialRequirement();
        Task<string> FetchProdMaterialRequirement(string ProdMaterialRequirement);
    }
}
