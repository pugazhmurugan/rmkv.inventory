﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters
{
   public interface IProductMasterRepository
    {
        Task<string> GetProductMasters(string ProductMaster);
        Task<bool> AddProductMasters(string ProductMaster);
        Task<bool> DeleteProductMasters(string ProductMaster);
        Task<string> GetProductCounter(string ProductMaster);
        Task<string> GetProductAttributes(string ProductMaster);
        Task<string> FetchProductMasters(string ProductMaster);


        Task<bool> addGoods(string ProductMaster);
        Task<string> GetToLocation(string ProductMaster);
        Task<string> GetGoods(string ProductMaster);
        Task<int> GetTransferNo(string ProductMaster); 
        Task<string> FetchGoods(string ProductMaster);
    }
}
