﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters
{
   public interface IProductGroupAttributesRepository
    {
        Task<string> GetAttributeLookup(string objAttribute);
        Task<string> GetProductGroupLookup(string objAttribute);
        Task<int> RemoveProductGroupAttributes(string objAttribute);
        Task<int> SaveProductGroupAttributes(string objAttribute);
        Task<string> GetProductGroupAttributeList(string objAttribute);
        Task<string> ProductGroupAttributeFetch(string objAttribute);
    }
}
