﻿using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters
{
   public interface IProductionProductsRepository
    {
        Task<int> AddProductionProduct(string ProdProduct);
        Task<string> GetProductionProduct();
        Task<string> GetProductionProductDetails(string fetchPhotoShoot);
        Task<string> GetProductionProductLookup();


    }
}
