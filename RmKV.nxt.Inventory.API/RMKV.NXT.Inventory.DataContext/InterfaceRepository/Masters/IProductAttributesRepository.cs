﻿using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters
{
    public interface IProductAttributesRepository
    {
        Task<int> AddProductAttributes(AddProductAttributesEntity objProduct);
        Task<List<GetProductAttributesEntity>> GetProductAttributes(GetProductAttributesParams objProduct);
        Task<int> AddProductAttributeValues(AddProductAttributesEntity objProduct);
        Task<List<GetProductAttributeValuesEntity>> GetProductAttributeValues(AddProductAttributesEntity objProduct);
        Task<List<GetProductAttributesEntity>> ModifyProductAttributes(AddProductAttributesEntity objProduct);
        Task<List<GetProductAttributeValuesEntity>> ModifyProductAttributeValues(AddProductAttributesEntity objProduct);
    }
}
