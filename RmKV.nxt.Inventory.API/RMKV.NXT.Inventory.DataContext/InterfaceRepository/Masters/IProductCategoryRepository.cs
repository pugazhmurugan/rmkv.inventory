﻿using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters
{
   public interface IProductCategoryRepository
    {
        Task<int> SaveCategory(SaveProductCategoryEntity objCategory);
        Task<List<GetProductCategoryListEntity>> GetCategory(GetProductCategoryParmsEntity objCategory);
        Task<List<FetchProductCategoryListEntity>> FetchCategory(FetchProductCategoryParmsEntity objCategory);
        Task<int> RemoveCategory(RemoveProductCategoryListEntity objCategory);
    }
}
