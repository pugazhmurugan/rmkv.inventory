﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseOrders
{
    public interface IPurchaseOrdersRepository
    {
        Task<string> GetPOAnalysis(string objPO);
        Task<string> GetSupplierCityDetails();
        Task<string> GetSupplierGroupDetails(string objSupplier);
        Task<string> GetSupplierByGroupId(string objSupplier);
        Task<string> GetModeofDispatchMasterList(string objSupplier);
        Task<string> GetUOMList(int section_id);
        Task<string> GetPurchaseOrderList(string ObjList);
        Task<int> CancelPurchaseOrderList(string ObjList);
        Task<int> SavePurchaseOrderList(string ObjList);
        Task<string> FetchPurchaseOrderList(string ObjList);
        Task<string> GetPoDispatchLocationList(string ObjList);
        Task<string> GetPurchaseOrderSizeName(string ObjList);
        Task<string> GetPurchaseOrderSizeNameBasedSizes(string ObjList);
        Task<string> GetPurchaseOrderNo(string ObjList);
        Task<string> GetPurchaseOrderEmail(string ObjList);

    }
}
