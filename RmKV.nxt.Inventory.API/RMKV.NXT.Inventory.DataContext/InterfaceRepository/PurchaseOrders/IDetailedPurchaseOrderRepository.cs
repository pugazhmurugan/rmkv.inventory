﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseOrders
{
    public interface IDetailedPurchaseOrderRepository
    {
        Task<int> SaveDetailedPurchaseOrder(string ObjList);
        Task<int> CancelDetailedPurchaseOrder(string ObjList);
        Task<string> FetchDetailedPurchaseOrder(string ObjList);
        Task<string> GetDetailedPurchaseOrderList(string ObjList);
        Task<string> GetBrandListForDetailedPO(string ObjList);
        Task<string> GetProductGroupListForDetailedPO(string ObjList);
        Task<string> GetProductListForDetailedPO(string ObjList);
        Task<string> GetDesignCodeListForDetailedPO(string ObjList);
        Task<string> GetColorListForDetailedPO(string ObjList);
        Task<string> GetQualityListForDetailedPO(string ObjList);
        Task<string> GetProductSizeListForDetailedPO(string ObjList);
        Task<string> GetPurchaseOrderLocations(string ObjList);
        Task<string> GetDetailedPurchaseOrderNo(string ObjList);
        Task<string> GetDetailedPurchaseOrderEmail(string ObjList);
    }
}
