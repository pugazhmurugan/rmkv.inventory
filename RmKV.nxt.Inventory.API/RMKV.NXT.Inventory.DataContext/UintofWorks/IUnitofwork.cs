﻿using RmKV.NXT.Inventory.DataContext.InterfaceRepository;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsReceipts;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfer;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfers;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseOrders;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.TransfersExceptionReports;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Purchases;
using System;
using System.Collections.Generic;
using System.Text;
using RMKV.NXT.Inventory.DataContext.SqlRepository.Purchases;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.WarehouseGatepassAcknowledgment;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseReturn;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Tools;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Pricing.SellingPriceChange;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Pricing.DoubleRate;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Pricing;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableTransferReports;

namespace RMKV.NXT.Inventory.DataContext.UintofWorks
{
   public interface IUnitofwork: IDisposable
    {
        ISharedRepository SharedRepository { get; }
        IProductAttributesRepository ProductAttributesRepository { get; }
        IProductCategoryRepository ProductCategoryRepository { get; }
        IHSNMasterRepository HSNMasterRepository { get; }
        IProductGroupAttributesRepository ProductGroupAttributesRepository { get; }
        IProductGroupRepository ProductGroupRepository { get; }
        IProductMasterRepository ProductMasterRepository { get; }
        ICounterToGodownRepository CounterToGodownRepository { get; }
        IGodownToCounterRepository GodownToCounterRepository { get; }
        IPurchaseOrdersRepository PurchaseOrdersRepository { get; }
        IGodowntoCounterAcknowledgementRepository GodowntoCounterAcknowledgementRepository { get; }
        ICountertoGodownAcknowledgementRepository CountertoGodownAcknowledgementRepository { get; }
        IGoodsTransferRepository GoodsTransferRepository { get; }
        IInvoiceInBaleRepository InvoiceInBaleRepository { get; }
        IGoodsTransferReceiptsRepository GoodsTransferReceiptsRepository { get; }
        IWarehouseGatepassRepository WarehouseGatepassRepository { get; }
        IInvoiceRegisterRepository InvoiceRegisterRepository { get; }
        IBarCodePrintingRepository BarCodePrintingRepository { get; }
        IGoodsTransferBalesRepository GoodsTransferBalesRepository { get; }
        IInvoiceProdDetailsRepository InvoiceProdDetailsRepository { get; }
        IInvoicesRepository InvoicesRepository { get; }
        IWarehouseGatepassAckRepository WarehouseGatePassAckRepository { get; }
        IPurchaseReturnRepository PurchaseReturnRepository { get; }
        
        IPurchaseReturnApproveRepository PurchaseReturnApproveRepository { get; }
        IPurchaseReturnNoteRepository PurchaseReturnNoteRepository { get; }
        IPurchaseReturnDCRepository PurchaseReturnDCRepository { get; }
        IProductChangeRepository ProductChangeRepository { get; }
        ISingleProductRepository SingleProductRepository { get; }
        ICustomerWorkOrderRepository CustomerWorkOrderRepository { get; }
        IByAmountRepository ByAmountRepository { get; }
        IMRPChangeRepository MRPChangeRepository { get; }
        IWorkOrderIssueRepository WorkOrderIssueRepository { get; }
        IReturnedGoodsNotesRepository ReturnedGoodsNotesRepository { get; }
        IDetailedPurchaseOrderRepository DetailedPurchaseOrderRepository { get; }
        ICounterToGodownExceptionRepository CounterToGodownExceptionRepository { get; }

        IGoodsTransferExceptionRepository GoodsTransferExceptionRepository { get; }
        IGodownToCounterExceptionRepository GodownToCounterExceptionRepository { get; }
        ISupplierDescriptionChangeRepository ProductDescriptionChangeRepository { get; }
        IProductionIssueRepository ProductionIssueRepository { get; }
        IProductionMaterialsRepository ProductionMaterialsRepository { get; }
        IProductionProductsRepository ProductionProductRepository { get; }
        IPolishOrderIssueRepository PolishOrderIssueReppository { get; }
        IPolishOrderReceiptRepository PolishOrderReceiptRepository { get; }
        IProductionMaterialRequirementsRepository ProductionMaterialRequirementsRepository { get; }
        IReturnableTransferReportsRepository ReturnableTransferReportsRepository { get; }
        IProductionReceiptRepository ProductionReceiptRepository { get; }
        void Commit();
    }
}
