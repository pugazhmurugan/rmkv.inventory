﻿using Npgsql;
using RmKV.NXT.Inventory.DataContext.InterfaceRepository;
using RmKV.NXT.Inventory.DataContext.SqlRepository;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsReceipts;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfer;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.GoodsTransfers;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Masters;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseOrders;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Purchases;
using RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.Goods_Receipts;
using RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement;
using RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.GoodsTransfer;
using RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.GoodsTransfers;
using RMKV.NXT.Inventory.DataContext.SqlRepository.Masters;
using RMKV.NXT.Inventory.DataContext.SqlRepository.PurchaseOrders;
using RMKV.NXT.Inventory.DataContext.SqlRepository.Purchases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.WarehouseGatepassAcknowledgment;
using RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.WarehouseGatepassAcknowledgment;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.PurchaseReturn;
using RMKV.NXT.Inventory.DataContext.SqlRepository.PurchaseReturn;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Tools;
using RMKV.NXT.Inventory.DataContext.SqlRepository.Tools;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Pricing.SellingPriceChange;
using RMKV.NXT.Inventory.DataContext.SqlRepository.Pricing.SellingPriceChange;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableGoodsTransfer;
using RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.ReturnableGoodsTransfer;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Pricing.DoubleRate;
using RMKV.NXT.Inventory.DataContext.SqlRepository.Pricing.DoubleRate;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.Pricing;
using RMKV.NXT.Inventory.DataContext.SqlRepository.Pricing;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.TransfersExceptionReports;
using RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.TransfersExceptionReports;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.ReturnableTransferReports;
using RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.ReturnableTransferReports;

namespace RMKV.NXT.Inventory.DataContext.UintofWorks
{
    public class Unitofwork : IUnitofwork
    {
        private IDbConnection connection;
        private IDbTransaction transaction;
        private bool disposed;
        private ISharedRepository sharedRepository;
        private IProductAttributesRepository productAttributesRepository;
        private IProductCategoryRepository productCategoryRepository;
        private IHSNMasterRepository hSNMasterRepository;
        private IProductGroupAttributesRepository productGroupAttributesRepository;
        private IProductGroupRepository productGroupRepository;
        private IProductMasterRepository productMasterRepository;
        private ICounterToGodownRepository counterToGodownRepository;
        private IGodownToCounterRepository godownToCounterRepository;
        private IPurchaseOrdersRepository purchaseOrdersRepository;
        private IGodowntoCounterAcknowledgementRepository godowntoCounterAcknowledgementRepository;
        private ICountertoGodownAcknowledgementRepository countertoGodownAcknowledgementRepository;
        private IGoodsTransferRepository goodsTransferRepository;
        private IInvoiceInBaleRepository invoiceInBaleRepository;
        private IGoodsTransferReceiptsRepository goodsTransferReceiptsRepository;
        private IWarehouseGatepassRepository warehouseGatepassRepository;
        private IInvoiceRegisterRepository invoiceRegisterRepository;
        private IBarCodePrintingRepository barCodePrintingRepository;
        private IGoodsTransferBalesRepository goodsTransferBalesRepository;
        private IInvoiceProdDetailsRepository invoiceProdDetailsRepository;
        private IInvoicesRepository invoicesRepository;
        private IWarehouseGatepassAckRepository warehouseGatepassAckRepository;
        private IPurchaseReturnRepository purchaseReturnRepository;
        private IPurchaseReturnApproveRepository purchaseReturnApproveRepository;
        private IPurchaseReturnNoteRepository purchaseReturnNoteRepository;
        private IPurchaseReturnDCRepository purchaseReturnDCRepository;
        private IProductChangeRepository productChangeRepository;
        private ISingleProductRepository singleProductRepository;
        private ICustomerWorkOrderRepository customerWorkOrderRepository;
        private IByAmountRepository byAmountRepository;
        private IMRPChangeRepository mrpChangeRepository;
        private IWorkOrderIssueRepository workOrderIssueRepository;
        private IReturnedGoodsNotesRepository returnedGoodsNotesRepository;
        private IDetailedPurchaseOrderRepository detailedPurchaseOrderRepository;
        private ICounterToGodownExceptionRepository counterToGodownExceptionRepository;
        private IGodownToCounterExceptionRepository godownToCounterExceptionRepository;
        private ISupplierDescriptionChangeRepository supplierDescriptionChangeRepository;
        private IPolishOrderIssueRepository polishOrderIssueRepository;
        private IGoodsTransferExceptionRepository goodsTransferExceptionRepository;
        private IProductionIssueRepository productionIssueRepository;
        private IProductionMaterialsRepository productionMaterialsRepository;
        private IProductionProductsRepository productionProductRepository;
        private IPolishOrderReceiptRepository polishOrderReceiptRepository;
        private IProductionMaterialRequirementsRepository productionMaterialRequirementsRepository;
        private IReturnableTransferReportsRepository returnableTransferReportsRepository;
        private IProductionReceiptRepository productionReceiptRepository;
        public Unitofwork(string ConnectionString)
        {
            connection = new NpgsqlConnection(ConnectionString);
            connection.Open();
            transaction = connection.BeginTransaction();
        }

        public ISharedRepository SharedRepository
        {
            get { return sharedRepository ?? (sharedRepository = new SharedRepository(transaction)); }
        }
        public IProductAttributesRepository ProductAttributesRepository
        {
            get { return productAttributesRepository ?? (productAttributesRepository = new ProductAttributesRepository(transaction)); }
        }

        public IProductCategoryRepository ProductCategoryRepository
        {
            get { return productCategoryRepository ?? (productCategoryRepository = new ProductCategoryRepository(transaction)); }
        }

        public IHSNMasterRepository HSNMasterRepository
        {
            get { return hSNMasterRepository ?? (hSNMasterRepository = new HSNMasterRepository(transaction)); }
        }

        public IProductGroupAttributesRepository ProductGroupAttributesRepository
        {
            get { return productGroupAttributesRepository ?? (productGroupAttributesRepository = new ProductGroupAttributesRepository(transaction)); }
        }

        public IProductGroupRepository ProductGroupRepository
        {
            get { return productGroupRepository ?? (productGroupRepository = new ProductGroupRepository(transaction)); }
        }
        public IProductMasterRepository ProductMasterRepository
        {
            get { return productMasterRepository ?? (productMasterRepository = new ProductMasterRepository(transaction)); }
        }
        public ICounterToGodownRepository CounterToGodownRepository
        {
            get { return counterToGodownRepository ?? (counterToGodownRepository = new CounterToGodownRepository(transaction)); }
        }
        public IGodownToCounterRepository GodownToCounterRepository
        {
            get { return godownToCounterRepository ?? (godownToCounterRepository = new GodownToCounterRepository(transaction)); }
        }
        public IPurchaseOrdersRepository PurchaseOrdersRepository
        {
            get { return purchaseOrdersRepository ?? (purchaseOrdersRepository = new PurchaseOrdersRepository(transaction)); }
        }
        public IGodowntoCounterAcknowledgementRepository GodowntoCounterAcknowledgementRepository
        {
            get { return godowntoCounterAcknowledgementRepository ?? (godowntoCounterAcknowledgementRepository = new GodowntoCounterAcknowledgementRepository(transaction)); }
        }
        public ICountertoGodownAcknowledgementRepository CountertoGodownAcknowledgementRepository
        {
            get { return countertoGodownAcknowledgementRepository ?? (countertoGodownAcknowledgementRepository = new CountertoGodownAcknowledgementRepository(transaction)); }
        }
        public IGoodsTransferRepository GoodsTransferRepository
        {
            get { return goodsTransferRepository ?? (goodsTransferRepository = new GoodsTransferRepository(transaction)); }
        }

        public IInvoiceInBaleRepository InvoiceInBaleRepository
        {
            get { return invoiceInBaleRepository ?? (invoiceInBaleRepository = new InvoiceInBaleRepository(transaction)); }
        }

        public IGoodsTransferReceiptsRepository GoodsTransferReceiptsRepository
        {
            get { return goodsTransferReceiptsRepository ?? (goodsTransferReceiptsRepository = new GoodsTransferReceiptsRepository(transaction)); }
        }

        public IWarehouseGatepassRepository WarehouseGatepassRepository
        {
            get { return warehouseGatepassRepository ?? (warehouseGatepassRepository = new WarehouseGatepassRepository(transaction)); }
        }

        public IInvoiceRegisterRepository InvoiceRegisterRepository
        {
            get { return invoiceRegisterRepository ?? (invoiceRegisterRepository = new InvoiceRegisterRepository(transaction)); }
        }

        public IBarCodePrintingRepository BarCodePrintingRepository
        {
            get { return barCodePrintingRepository ?? (barCodePrintingRepository = new BarCodePrintingRepository(transaction)); }
        }

        public IGoodsTransferBalesRepository GoodsTransferBalesRepository
        {
            get { return goodsTransferBalesRepository ?? (goodsTransferBalesRepository = new GoodsTransferBalesRepository(transaction)); }
        }
        public IInvoiceProdDetailsRepository InvoiceProdDetailsRepository
        {
            get { return invoiceProdDetailsRepository ?? (invoiceProdDetailsRepository = new InvoiceProdDetailsRepository(transaction)); }
        }
        public IInvoicesRepository InvoicesRepository
        {
            get { return invoicesRepository ?? (invoicesRepository = new InvoicesRepository(transaction)); }
        }

        public IWarehouseGatepassAckRepository WarehouseGatePassAckRepository
        {
            get { return warehouseGatepassAckRepository ?? (warehouseGatepassAckRepository = new WarehouseGatePassAckRepository(transaction)); }
        }

        public IPurchaseReturnRepository PurchaseReturnRepository
        {
            get { return purchaseReturnRepository ?? (purchaseReturnRepository = new PurchaseReturnRepository(transaction)); }
        }
        public IPurchaseReturnApproveRepository PurchaseReturnApproveRepository
        {
            get { return purchaseReturnApproveRepository ?? (purchaseReturnApproveRepository = new PurchaseReturnApproveRepository(transaction)); }
        }
        public IPurchaseReturnNoteRepository PurchaseReturnNoteRepository
        {
            get { return purchaseReturnNoteRepository ?? (purchaseReturnNoteRepository = new PurchaseReturnNoteRepository(transaction)); }
        }
        public IPurchaseReturnDCRepository PurchaseReturnDCRepository
        {
            get { return purchaseReturnDCRepository ?? (purchaseReturnDCRepository = new PurchaseReturnDCRepository(transaction)); }
        }
        public IProductChangeRepository ProductChangeRepository
        {
            get { return productChangeRepository ?? (productChangeRepository = new ProductChangeRepository(transaction)); }
        }
        public ISingleProductRepository SingleProductRepository
        {
            get { return singleProductRepository ?? (singleProductRepository = new SingleProductRepository(transaction)); }
        }
        public ICustomerWorkOrderRepository CustomerWorkOrderRepository
        {
            get { return customerWorkOrderRepository ?? (customerWorkOrderRepository = new CustomerWorkOrderRepository(transaction)); }
        }
        public IByAmountRepository ByAmountRepository
        {
            get { return byAmountRepository ?? (byAmountRepository = new ByAmountRepository(transaction)); }
        }
        public IMRPChangeRepository MRPChangeRepository
        {
            get { return mrpChangeRepository ?? (mrpChangeRepository = new MRPChangeRepository(transaction)); }
        }
        public IWorkOrderIssueRepository WorkOrderIssueRepository
        {
            get { return workOrderIssueRepository ?? (workOrderIssueRepository = new WorkOrderIssueRepository(transaction)); }
        }
        public IReturnedGoodsNotesRepository ReturnedGoodsNotesRepository
        {
            get { return returnedGoodsNotesRepository ?? (returnedGoodsNotesRepository = new ReturnedGoodsNotesRepository(transaction)); }
        }
        public IDetailedPurchaseOrderRepository DetailedPurchaseOrderRepository
        {
            get { return detailedPurchaseOrderRepository ?? (detailedPurchaseOrderRepository = new DetailedPurchaseOrderRepository(transaction)); }
        }
        public IGoodsTransferExceptionRepository GoodsTransferExceptionRepository
        {
            get { return goodsTransferExceptionRepository ?? (goodsTransferExceptionRepository = new GoodsTransferExceptionRepository(transaction)); }
        }
        public IGodownToCounterExceptionRepository GodownToCounterExceptionRepository
        {
            get { return godownToCounterExceptionRepository ?? (godownToCounterExceptionRepository = new GodownToCounterExceptionRepository(transaction)); }
        }
        public ICounterToGodownExceptionRepository CounterToGodownExceptionRepository
        {
            get { return counterToGodownExceptionRepository ?? (counterToGodownExceptionRepository = new CounterToGodownExceptionRepository(transaction)); }
        }

        public ISupplierDescriptionChangeRepository ProductDescriptionChangeRepository
        {
            get { return supplierDescriptionChangeRepository ?? (supplierDescriptionChangeRepository = new ProductDescriptionChangeRepository(transaction)); }
        }

        public IReturnableTransferReportsRepository ReturnableTransferReportsRepository
        {
            get { return returnableTransferReportsRepository ?? (returnableTransferReportsRepository = new ReturnableTransferReportsRepository(transaction)); }
        }


        public IProductionIssueRepository ProductionIssueRepository
        {
            get { return productionIssueRepository ?? (productionIssueRepository = new ProductionIssueRepository(transaction)); }
        }
        public IProductionMaterialsRepository ProductionMaterialsRepository
        {
            get { return productionMaterialsRepository ?? (productionMaterialsRepository = new ProductionMaterialsRepository(transaction)); }
        }
        public IProductionProductsRepository ProductionProductRepository
        {
            get { return productionProductRepository ?? (productionProductRepository = new ProductionProductRepository(transaction)); }
        }
        public IPolishOrderIssueRepository PolishOrderIssueReppository
        {
            get { return polishOrderIssueRepository ?? (polishOrderIssueRepository = new PolishOrderIssueReppository(transaction)); }
        }
        public IPolishOrderReceiptRepository PolishOrderReceiptRepository
        {
            get { return polishOrderReceiptRepository ?? (polishOrderReceiptRepository = new PolishOrderReceiptRepository(transaction)); }
        }
        public IProductionMaterialRequirementsRepository ProductionMaterialRequirementsRepository
        {
            get { return productionMaterialRequirementsRepository ?? (productionMaterialRequirementsRepository = new ProductionMaterialRequirementsRepository(transaction)); }
        }
        public IProductionReceiptRepository ProductionReceiptRepository
        {
            get { return productionReceiptRepository ?? (productionReceiptRepository = new ProductionReceiptRepository(transaction)); }
        }
        public void Commit()
        {
            try
            {
                transaction.Commit();
            }
            catch (Exception Ex)
            {
                transaction.Rollback();
                throw Ex;
            }
            finally
            {
                transaction.Dispose();
                transaction = connection.BeginTransaction();
                resetRepositories();
            }
        }

        private void resetRepositories()
        {
            sharedRepository = null;
            productAttributesRepository = null;
            productCategoryRepository = null;
            hSNMasterRepository = null;
            productGroupAttributesRepository = null;
            productGroupRepository = null;
            purchaseOrdersRepository = null;
            godowntoCounterAcknowledgementRepository = null;
            countertoGodownAcknowledgementRepository = null;
            invoiceInBaleRepository = null;
            goodsTransferReceiptsRepository = null;
            goodsTransferBalesRepository = null;
            invoicesRepository = null;
            purchaseReturnApproveRepository = null;
            purchaseReturnNoteRepository = null;
            purchaseReturnDCRepository = null;
            singleProductRepository = null;
            customerWorkOrderRepository = null;
            byAmountRepository = null;
            mrpChangeRepository = null;
            detailedPurchaseOrderRepository = null;
            godownToCounterExceptionRepository = null;
            counterToGodownExceptionRepository = null;
            productionIssueRepository = null;
            productionMaterialsRepository = null;
            productionProductRepository = null;
            polishOrderReceiptRepository = null;
            productionMaterialRequirementsRepository = null;
            returnableTransferReportsRepository = null;
            productionReceiptRepository = null;
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }
        private void dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                        transaction = null;
                    }
                    if (connection != null)
                    {
                        connection.Dispose();
                        connection = null;
                    }
                }
            }
            disposed = true;
        }
        ~Unitofwork()
        {
            dispose(false);
        }
    }
}
