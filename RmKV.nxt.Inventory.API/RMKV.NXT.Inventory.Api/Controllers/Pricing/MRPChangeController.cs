﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.Pricing;

namespace RMKV.NXT.Inventory.Api.Controllers.Pricing
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class MRPChangeController : ControllerBase
    {
        private IMediator mediator;

        public MRPChangeController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("AddMRPChanges")]
        public async Task<IActionResult> AddMRPChanges([FromBody] AddMRPChangeService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

    }
}