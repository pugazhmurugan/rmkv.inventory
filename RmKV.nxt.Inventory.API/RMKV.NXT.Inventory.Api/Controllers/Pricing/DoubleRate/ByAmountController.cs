﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.Pricing.DoubleRate;

namespace RMKV.NXT.Inventory.Api.Controllers.Pricing.DoubleRate
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ByAmountController : ControllerBase
    {
        private IMediator mediator;

        public ByAmountController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("AddByAmount")]
        public async Task<IActionResult> AddByAmount([FromBody] AddByAmountService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddByPercentage")]
        public async Task<IActionResult> AddByPercentage([FromBody] AddByPercentageService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("CheckMargin")]
        public async Task<IActionResult> CheckMargin([FromBody] CheckMarginService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetMarginCalculate")]
        public async Task<IActionResult> GetMarginCalculate([FromBody] GetMarginCalculateService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}