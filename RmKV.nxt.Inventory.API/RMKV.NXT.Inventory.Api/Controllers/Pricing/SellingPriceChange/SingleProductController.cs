﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.Business.Pricing.SellingPriceChange;

namespace RMKV.NXT.Inventory.Api.Controllers.Pricing.SellingPriceChange
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class SingleProductController : ControllerBase
    {
        private IMediator mediator;

        public SingleProductController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("GetSingleProduct")]
        public async Task<IActionResult> GetSingleProduct([FromBody] GetSingleProductService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddSingleProduct")]
        public async Task<IActionResult> AddSingleProduct([FromBody] AddSingleProductService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetPriceChangeHistoryReport")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPriceChangeHistoryReport(GetPriceChangeHistoryReportService values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                var webRootPath = Path.GetFullPath(Path.Combine("Reports/PriceChangeHistoryReport.frx"));
                //string webRootPath = "Reports/Form_S_Report.frx";

                FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                return stream;
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}