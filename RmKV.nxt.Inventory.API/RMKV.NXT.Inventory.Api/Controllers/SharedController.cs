﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RmKV.NXT.Inventory.Api.Shared;
using RmKV.NXT.Inventory.DataModel.Model.Shared;
using RMKV.NXT.Inventory.DataModel.Model.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RmKV.NXT.Inventory.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class SharedController : ControllerBase
    {
        private IMediator mediator;

        public SharedController(IMediator mediator)
        {
            this.mediator = mediator;
        }


        ////////////////////////////////////////////////////   NxtCommon Database ////////////////////////////////////////////////////////////

        [HttpPost]
        [Route("GetSalesLocations")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetSalesLocations([FromBody] GetSalesLocationServices values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetAllSalesLocations")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllSalesLocations([FromBody] GetAllSalesLocationServices values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetCompanies")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCompanies([FromBody] GetCompanyServices values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }


        [HttpPost]
        [Route("GetWarehouse")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetWarehouse([FromBody] GetWarehouseService command)
        {
            var response = await mediator.Send(command);
            return Ok(JsonConvert.SerializeObject(response));
        }


        [HttpGet]
        [Route("GetGRNDocumentRootPathAndUrl/{salesLocationId}")]
        public async Task<IActionResult> GetGRNDocumentRootPathAndUrl([FromRoute] int salesLocationId)
        {
            var response = await mediator.Send(new GetGRNDocumentRootPathAndUrlServices() { SalesLocationId = salesLocationId });
            return Ok(JsonConvert.SerializeObject(response));
        }

        ////////////////////////////////////////////////////   NxtHR Database ////////////////////////////////////////////////////////////
        ///
        [HttpPost]
        [Route("GetEmployeeLookup")]
        public async Task<IActionResult> GetEmployeeLookup([FromBody] GetEmployeeLookupServices values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetUserProfile")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUserProfile([FromBody] GetUserProfileService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("CheckLockPassword")]
        public async Task<IActionResult> CheckLockPassword([FromBody] CheckLockPasswordService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetCompanySectionDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCompanySectionDetails([FromBody] GetWarehouseBasedCompanySectionService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetGRNSectionsByWarehouseId")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetGRNSectionsByWarehouseId([FromBody] GetSectionService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        ////////////////////////////////////////////////////////Notification/////////////////////////////////////////////////////////////

        //[HttpPost]
        //[Route("FetchNotification")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //public async Task<IActionResult> FetchNotification([FromBody] FetchNotificationService values)
        //{
        //    var response = await mediator.Send(values);
        //    return Ok(JsonConvert.SerializeObject(response));
        //}

        //[HttpPost]
        //[Route("SaveNotification")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //public async Task<IActionResult> SaveNotification([FromBody] SaveNotificationService values)
        //{
        //    var response = await mediator.Send(values);
        //    return Ok(JsonConvert.SerializeObject(response));
        //}

        //[HttpPost]
        //[Route("ViewNotification")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //public async Task<IActionResult> ViewNotification([FromBody] ViewNotificationService values)
        //{
        //    var response = await mediator.Send(values);
        //    return Ok(JsonConvert.SerializeObject(response));
        //}


        //[HttpPost]
        //[Route("CheckPayRollMonthEntryExist")]
        //public async Task<IActionResult> CheckPayRollMonthEntryExist([FromBody] CheckPayrollEntryExistService Values)
        //{
        //    var response = await mediator.Send(Values);
        //    return Ok(JsonConvert.SerializeObject(response));
        //}

        [HttpPost]
        [Route("UploadGRNTempFile"), DisableRequestSizeLimit]
        public async Task<IActionResult> UploadGRNTempFile(IFormFile file)
        {
            var fileRootPath = Request.Form["fileRootPath"];
            DocumentPath document = new DocumentPath();
            string oldDocumentPath = Request.Form["oldDocument"];
            //var file = Request.Form.Files[0];
            if (file != null && file.Length > 0)
            {
                string extension = Path.GetExtension(file.FileName);
                var fileName = "Temp\\" + DateTime.Now.ToString("yyyyMMddhmmss") + extension;
                var fileSavePath = fileRootPath + "\\" + fileName;
                if (!Directory.Exists(fileRootPath + "\\Temp\\"))
                {
                    Directory.CreateDirectory(fileRootPath + "\\Temp\\");
                }
                DirectoryInfo directoryInfo = new DirectoryInfo(fileRootPath + "\\Temp\\");
                DateTime dateForButton = DateTime.Now.AddDays(-1);
                FileInfo[] existFiles = directoryInfo.GetFiles("*" + DateTime.Now.AddDays(-1).ToString("yyyyMMdd") + "*" + ".*");
                if (existFiles.Length > 0)
                {
                    foreach (FileInfo files in existFiles)
                    {
                        files.Delete();
                    }
                }
                using (var stream = new FileStream(fileSavePath, FileMode.Create))
                {
                    file.CopyTo(stream);
                    document.Document_Path = fileName;
                }
            }
            else if (file == null)
            {
                if (oldDocumentPath != "" && oldDocumentPath != null && oldDocumentPath != "null")
                {
                    DirectoryInfo directoryInfo = new DirectoryInfo(fileRootPath + "\\Temp\\");
                    FileInfo[] OldexistFiles = directoryInfo.GetFiles(oldDocumentPath.Split("\\")[1].Split(".")[0] + ".*");
                    if (OldexistFiles.Length > 0)
                    {
                        foreach (FileInfo files in OldexistFiles)
                        {
                            files.Delete();
                        }
                    }
                }
            }
            return Ok(document);
        }

        [HttpPost]
        [Route("GetDashboardDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetDashboardDetails([FromBody] GetDashboardService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetSection")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetSection([FromBody] GetSectionService command)
        {
            var response = await mediator.Send(command);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpGet]
        [Route("GetGroupSections")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetGroupSections()
        {
            var response = await mediator.Send(new GetGroupSectionService());
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetByNoDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByNoDetails([FromBody] GetByNoDetailsService command)
        {
            var response = await mediator.Send(command);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetGlobalParams")]
        public async Task<IActionResult> GetGlobalParams([FromBody] GetGlobalParamsServices values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetGRNAccountsLookup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetGRNAccountsLookup([FromBody] GetGRNAccountsLookupService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetCommonSectionsDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCommonSectionsDetails([FromBody] GetCommonSectionService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetDocumentPath")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilePath([FromBody] GetFilePathService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetDocumentPathView")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetDocumentPathView([FromBody] GetDocumentPathViewService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetMaterialLookup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetMaterialLookup([FromBody] GetMaterialLookupService command)
        {
            var response = await mediator.Send(command);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetMaterialProductLookup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetMaterialProductLookup([FromBody] GetMaterialProductLookupService command)
        {
            var response = await mediator.Send(command);
            return Ok(JsonConvert.SerializeObject(response));
        }

        //[HttpPost]
        //[Route("GetDocumentAsFileStream")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //public async Task<IActionResult> GetDocumentAsFileStream()
        //{
        //    var filePaths = Request.Form["filePaths"];
        //    var fileNames = Request.Form["fileNames"];
        //    List<GetFileStream> test = JsonConvert.DeserializeObject<List<GetFileStream>>(filePaths);
        //    List<GetFileNames> files = JsonConvert.DeserializeObject<List<GetFileNames>>(fileNames);
        //    return Ok(JsonConvert.SerializeObject(""));
        //}
    }
}
