﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.PurchaseReturn;

namespace RMKV.NXT.Inventory.Api.Controllers.PurchaseReturn
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class PurchaseReturnNoteController : ControllerBase
    {
        private IMediator mediator;

        public PurchaseReturnNoteController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("GetPurchaseReturnNote")]
        public async Task<IActionResult> GetPurchaseReturnNote([FromBody] GetPurchaseReturnNoteService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetPurchaseReturnByNo")]
        public async Task<IActionResult> GetPurchaseReturnByNo([FromBody] GetPurchaseReturnByNoService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetPurchaseReturnByNoDetails")]
        public async Task<IActionResult> GetPurchaseReturnByNoDetails([FromBody] GetPurchaseReturnByNoDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddPurchaseReturnNote")]
        public async Task<IActionResult> AddPurchaseReturnNote([FromBody] AddPurchaseReturnNoteService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("FetchPurchaseReturnNote")]
        public async Task<IActionResult> FetchPurchaseReturnNote([FromBody] FetchPurchaseReturnNoteService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}