﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnEntry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Api.Controllers.PurchaseReturn
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class PurchaseReturnEntryController : ControllerBase
    {
        private IMediator mediator;

        public PurchaseReturnEntryController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("AddPurchaseReturn")]
        public async Task<IActionResult> AddPurchaseReturn([FromBody] AddPurchaseReturnEntryService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("ApprovePurchaseReturn")]
        public async Task<IActionResult> ApprovePurchaseReturn([FromBody] ApprovePurchaseReturnEntryService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("CancelPurchaseReturn")]
        public async Task<IActionResult> CancelPurchaseReturn([FromBody] CancelPurchaseReturnEntryService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetPurchaseReturn")]
        public async Task<IActionResult> GetPurchaseReturn([FromBody] GetPurchaseReturnEntryService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("FetchPurchaseReturn")]
        public async Task<IActionResult> FetchPurchaseReturn([FromBody] FetchPurchaseReturnEntryService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}
