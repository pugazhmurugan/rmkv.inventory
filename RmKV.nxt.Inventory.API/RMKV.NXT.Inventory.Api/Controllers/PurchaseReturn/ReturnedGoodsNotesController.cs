﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.Business.PurchaseReturn.ReturnedGoodsNotes;
using RMKV.NXT.Inventory.DataModel;
using RMKV.NXT.Inventory.DataModel.Model.PurchaseReturn;

namespace RMKV.NXT.Inventory.Api.Controllers.PurchaseReturn
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ReturnedGoodsNotesController : ControllerBase
    {
        private IMediator mediator;
        public ReturnedGoodsNotesController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("GetRGNWiseDCDetails")]
        public async Task<IActionResult> GetRGNWiseDCDetails([FromBody] GetRGNWiseDCDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        //[HttpPost]
        //[Route("AddRGNWiseDetails")]
        //public async Task<IActionResult> AddRGNWiseDetails([FromBody] AddRGNWiseDetailsService values)
        //{
        //    var response = await mediator.Send(values);
        //    return Ok(response);
        //}
        [HttpPost]
        [Route("GetRGNWiseDetails")]
        public async Task<IActionResult> GetRGNWiseDetails([FromBody] GetRGNWiseDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("FetchRGNWiseDetails")]
        public async Task<IActionResult> FetchRGNWiseDetails([FromBody] FetchRGNWiseDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetDCDetails")]
        public async Task<IActionResult> GetDCDetails([FromBody] GetDCDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetTransporter")]
        public async Task<IActionResult> GetTransporter([FromBody] GetTransporterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }


        [HttpPost]
        [Route("AddRGNWiseDetails")]
        public async Task<IActionResult> AddRGNWiseDetails(IFormFile[] files)
        {
            var data = Request.Form["objRGNDetails"];
            AddRGNEntity objRGN = JsonConvert.DeserializeObject<AddRGNEntity>(data);
            var FileAttachPath = Request.Form["fileRootPath"];
            List<int> index = JsonConvert.DeserializeObject<List<int>>(Request.Form["fileIndex"]);
            List<int> removedIndex = JsonConvert.DeserializeObject<List<int>>(Request.Form["removedFileIndex"]);
            List<OldFiles> oldFiles = JsonConvert.DeserializeObject<List<OldFiles>>(Request.Form["removedFiles"]);
            if (removedIndex.Count > 0)
            {
                for (int i = 0; i < removedIndex.Count; i++)
                {
                    for (int j = 0; j < oldFiles.Count; j++)
                    {
                        int dataIndex = Convert.ToInt32(removedIndex[i]);
                        if (oldFiles[j] != null)
                        {
                            if (dataIndex == oldFiles[j].index)
                            {
                                var directory = FileAttachPath + "\\" + oldFiles[j].name.Split("\\")[0];
                                string fileName = oldFiles[j].name.Split("\\")[1];
                                DirectoryInfo directoryInfo = new DirectoryInfo(directory);
                                FileInfo[] OldexistFiles = directoryInfo.GetFiles(fileName.Split(".")[0] + ".*");
                                if (OldexistFiles.Length > 0)
                                {
                                    foreach (FileInfo file in OldexistFiles)
                                    {
                                        file.Delete();
                                        if (dataIndex == 0)
                                            objRGN.file_path[i].file_path1 = "";
                                        else if (dataIndex == 1)
                                            objRGN.file_path[i].file_path2 = "";
                                        else if (dataIndex == 2)
                                            objRGN.file_path[i].file_path3 = "";
                                        else if (dataIndex == 3)
                                            objRGN.file_path[i].file_path4 = "";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (index.Count > 0)
            {
                if (files != null && files.Length > 0)
                {
                    for (int i = 0; i < index.Count; i++)
                    {
                        if (files[i].FileName != null && FileAttachPath.ToString() != null)
                        {
                            int dataIndex = Convert.ToInt32(index[i]);
                            FileConfig fileConfig = Utilities.GetFileNameAndPath(FileAttachPath, "GRNDocuments", files[i].FileName);
                            var directory = FileAttachPath + "\\GRNDocuments\\";
                            if (!Directory.Exists(directory))
                            {
                                Directory.CreateDirectory(directory);
                            }
                            using (var stream = new FileStream(fileConfig.fileSavePath, FileMode.Create))
                            {
                                files[i].CopyTo(stream);
                                if (dataIndex == 0)
                                    objRGN.file_path[i].file_path1 = fileConfig.fileName.ToString();
                                else if (dataIndex == 1)
                                    objRGN.file_path[i].file_path2 = fileConfig.fileName.ToString();
                                else if (dataIndex == 2)
                                    objRGN.file_path[i].file_path3 = fileConfig.fileName.ToString();
                                else if (dataIndex == 3)
                                    objRGN.file_path[i].file_path4 = fileConfig.fileName.ToString();
                            }
                        }
                    }
                }
            }
            List<AddRGNEntity> rgnArr = new List<AddRGNEntity>();
            rgnArr.Add(objRGN);

            var response = await mediator.Send(new AddRGNWiseDetailsService()
            {
                RGNDetails = JsonConvert.SerializeObject(rgnArr)
            });
            return Ok(response);
        }

    }
}