﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnApprove;

namespace RMKV.NXT.Inventory.Api.Controllers.PurchaseReturn
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class PurchaseReturnApproveController : ControllerBase
    {
        private IMediator mediator;
        public PurchaseReturnApproveController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("GetApprovedSupplier")]
        public async Task<IActionResult> GetApprovedSupplier([FromBody] GetApprovedSupplierService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetPurchaseRetunApprovedList")]
        public async Task<IActionResult> GetPurchaseRetunApprovedList([FromBody] GetPurchaseRetunApprovedListService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("RemovPurchaseReturn")]
        public async Task<IActionResult> RemovPurchaseReturn([FromBody] RemovPurchaseReturnService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetSupplierCityForApprove")]
        public async Task<IActionResult> GetSupplierCityForApprove([FromBody] GetSupplierCityForApproveService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetSupplierGroupNameForApprove")]
        public async Task<IActionResult> GetSupplierGroupNameForApprove([FromBody] GetSupplierGroupNameForApproveService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetSupplierNameForApprove")]
        public async Task<IActionResult> GetSupplierNameForApprove([FromBody] GetSupplierNameForApproveService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GeSupplierDetailsForApprove")]
        public async Task<IActionResult> GeSupplierDetailsForApprove([FromBody] GeSupplierDetailsForApproveService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("ApprovePurchaseReturnList")]
        public async Task<IActionResult> ApprovePurchaseReturnList([FromBody] ApprovePurchaseReturnListService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}