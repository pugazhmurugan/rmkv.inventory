﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnDC;

namespace RMKV.NXT.Inventory.Api.Controllers.PurchaseReturn
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class PurchaseReturnDCController : ControllerBase
    {
        private IMediator mediator;

        public PurchaseReturnDCController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("LoadPurchaseReturnDCDetails")]
        public async Task<IActionResult> LoadPurchaseReturnDCDetails([FromBody] LoadPurchaseReturnDCService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("FetchPurchaseReturnDCList")]
        public async Task<IActionResult> FetchPurchaseReturnDCList([FromBody] FetchPurchaseReturnDCDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GeneratePurchaseReturnDCNoList")]
        public async Task<IActionResult> GeneratePurchaseReturnDCNoList([FromBody] GeneratePurchaseReturnDCNoService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}