﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.Masters.ProductionMaterialRequirements;

namespace RMKV.NXT.Inventory.Api.Controllers.Masters
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ProductionMaterialRequirementsController : ControllerBase
    {
        private IMediator mediator;

        public ProductionMaterialRequirementsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("SaveProdMaterialRequirements")]
        public async Task<IActionResult> SaveProdMaterialRequirements([FromBody] SaveProductionMaterialRequirementsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpGet]
        [Route("GetProdMaterialRequirements")]
        public async Task<IActionResult> GetProdMaterialRequirements()
        {
            var response = await mediator.Send(new GetProductionMaterialRequirementsService());
            return Ok(response);
        }
        [HttpPost]
        [Route("FetchProdMaterialRequirements")]
        public async Task<IActionResult> FetchProdMaterialRequirements([FromBody] FetchProductionMaterialRequirementsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
    }
}
