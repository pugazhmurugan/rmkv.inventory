﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.Masters.ProductAttributes;

namespace RMKV.NXT.Inventory.Api.Controllers.Masters
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ProductAttributesController : ControllerBase
    {

        private IMediator mediator;

        public ProductAttributesController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("AddProductAttributes")]
        public async Task<IActionResult> AddProductAttributes([FromBody] AddProductAttributesService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddProductAttributeValues")]
        public async Task<IActionResult> AddProductAttributeValues([FromBody] AddProductAttributeValuesService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetProductAttributes")]
        public async Task<IActionResult> GetProductAttributes([FromBody] GetProductAttributesService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetProductAttributeValues")]
        public async Task<IActionResult> GetProductAttributeValues([FromBody] GetProductAttributeValuesService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
        [HttpPost]
        [Route("ModifyProductAttributes")]
        public async Task<IActionResult> ModifyProductAttributes([FromBody] ModifyProductAttributesService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
        [HttpPost]
        [Route("ModifyProductAttributeValues")]
        public async Task<IActionResult> ModifyProductAttributeValues([FromBody] ModifyProductAttributeValuesService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
    }
}