﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.Masters.ProductionProducts;

namespace RMKV.NXT.Inventory.Api.Controllers.Masters
{

    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ProductionProductController : ControllerBase
    {
        private IMediator mediator;

        public ProductionProductController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("AddProdProduct")]
        public async Task<IActionResult> AddProdProduct([FromBody] ProductionProductService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpGet]
        [Route("GetProdProduct")]
        public async Task<IActionResult> GetProdProduct()
        {
            var response = await mediator.Send(new GetProductionProductService());
            return Ok(response);
        }
        [HttpPost]
        [Route("FetchProdProduct")]
        public async Task<IActionResult> FetchProdProduct([FromBody] FetchProductionProductService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpGet]
        [Route("GetProdProductLookup")]
        public async Task<IActionResult> GetProdProductLookup()
        {
            var response = await mediator.Send(new GetProductionProductLookupService());
            return Ok(response);
        }
    }
}