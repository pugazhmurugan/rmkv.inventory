﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.Masters;
using RMKV.NXT.Inventory.Business.Masters.ProductionMaterials;

namespace RMKV.NXT.Inventory.Api.Controllers.Masters
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ProductionMaterialsController : ControllerBase
    {
        private IMediator mediator;

        public ProductionMaterialsController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpGet]
        [Route("GetProductionMaterials")]
        public async Task<IActionResult> GetProductionMaterials()
        {
            var response = await mediator.Send(new GetProductionMaterialsService());
            return Ok(response);
        }
        [HttpPost]
        [Route("AddProductionMaterials")]
        public async Task<IActionResult> AddProductionMaterials([FromBody]AddProductionMaterialsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("FetchProductionMaterials")]
        public async Task<IActionResult> FetchGood([FromBody] FetchProductionMaterialsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}