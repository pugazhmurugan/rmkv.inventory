﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.Masters.HSNMaster;

namespace RMKV.NXT.Inventory.Api.Controllers.Masters
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class HSNMasterController : ControllerBase
    {
        private IMediator mediator;

        public HSNMasterController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("SaveHSNMaster")]
        public async Task<IActionResult> SaveHSNMaster([FromBody] SaveHSNMasterService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("SaveHSNTax")]
        public async Task<IActionResult> SaveHSNTax([FromBody] SaveHSNTaxService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("FetchHSNMaster")]
        public async Task<IActionResult> FetchHSNMaster([FromBody] FetchHSNMasterService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("FetchHSNTaxRate")]
        public async Task<IActionResult> FetchHSNTaxRate([FromBody] FetchHSNTaxService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }


        [HttpPost]
        [Route("FetchHSNTaxRateIndividual")]
        public async Task<IActionResult> FetchHSNTaxRateIndividual([FromBody] FetchHSNTaxIndividualService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("RemoveHSNMaster")]
        public async Task<IActionResult> RemoveHSNMaster([FromBody] RemoveHSNMasterService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("RemoveHSNTaxRate")]
        public async Task<IActionResult> RemoveHSNTaxRate([FromBody] RemoveHSNTaxRateService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }


        [HttpGet]
        [Route("GetHSNMAsterMaster")]
        public async Task<IActionResult> GetHSNMAsterMaster()
        {
            var response = await mediator.Send(new GetHSNMasterService());
            return Ok(JsonConvert.SerializeObject(response));
        }
    }
}
