﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.Masters.ProductGroupAttributes;

namespace RMKV.NXT.Inventory.Api.Controllers.Masters
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ProductGroupAttributesController : ControllerBase
    {
        private IMediator mediator;

        public ProductGroupAttributesController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("GetAtttributeLookupData")]
        public async Task<IActionResult> GetAtttributeLookupData([FromBody] GetAttributeLookupService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetProductGroupLookupData")]
        public async Task<IActionResult> GetProductGroupLookupData([FromBody] GetProductGroupLookupService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetProductGroupAttributeList")]
        public async Task<IActionResult> GetProductGroupAttributeList([FromBody] GetProductGroupAttributeListService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("FetchProductGroupAttributeData")]
        public async Task<IActionResult> FetchProductGroupAttributeData([FromBody] FetchProductGroupAttributeService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("RemoveProductGroupDetails")]
        public async Task<IActionResult> RemoveProductGroupDetails([FromBody] RemoveProductGroupAttributesService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("AddProductGroupDetails")]
        public async Task<IActionResult> AddProductGroupDetails([FromBody] AddProductGroupAttributesService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
    }
}
