﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.Masters.ProductMaster;

namespace RMKV.NXT.Inventory.Api.Controllers.Masters
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ProductMasterController : ControllerBase
    {
        private IMediator mediator;

        public ProductMasterController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("GetProductMasters")]
        public async Task<IActionResult> GetProductMasters([FromBody] GetProductMasterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetProductCounter")]
        public async Task<IActionResult> GetProductCounter([FromBody] GetProductCounterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetProductAttributes")]
        public async Task<IActionResult> GetProductAttributes([FromBody] GetProductAttributesServices values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("AddProductMasters")]
        public async Task<IActionResult> AddProductMasters([FromBody] AddProductMasterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("FetchProductMasters")]
        public async Task<IActionResult> FetchProductMasters([FromBody] FetchProductMasterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("DeleteProductMasters")]
        public async Task<IActionResult> DeleteProductMasters([FromBody] DeleteProductMasterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
                
        
                //////////////////////////////////////////////////////////////////////////////////////////////////////
        [HttpPost]
        [Route("addGoods")]
        public async Task<IActionResult> addGoods([FromBody] addGoodsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetToLocation")]
        public async Task<IActionResult> GetToLocation([FromBody] GetToLocationsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetGoods")]
        public async Task<IActionResult> GetGoods([FromBody] GetGoodsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetTransferNo")]
        public async Task<IActionResult> GetTransferNo([FromBody] GetTransferNoService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("FetchGood")]
        public async Task<IActionResult> FetchGood([FromBody] FetchGoodsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}
