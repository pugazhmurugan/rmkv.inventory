﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.Masters.ProductGroup;

namespace RMKV.NXT.Inventory.Api.Controllers.Masters
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ProductGroupController : ControllerBase
    {
        private IMediator mediator;

        public ProductGroupController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("GetProductGroups")]
        public async Task<IActionResult> GetProductGroups([FromBody] GetProductGroupService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddProductGroups")]
        public async Task<IActionResult> AddProductGroups([FromBody] AddProductGroupService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("FetchProductGroups")]
        public async Task<IActionResult> FetchProductGroups([FromBody] FetchProductGroupService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("DeleteProductGroups")]
        public async Task<IActionResult> DeleteProductGroups([FromBody] DeleteProductGroupService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}
