﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.ProductionIssue;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.ReturnableGoodsTransfer
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ProductionIssueController : ControllerBase
    {
        private IMediator mediator;

        public ProductionIssueController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("AddProductionIssue")]
        public async Task<IActionResult> AddProductionIssue([FromBody] AddProductionIssueService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetProductionIssue")]
        public async Task<IActionResult> GetProductionIssue([FromBody] GetProductionIssueService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("FetchProductionIssue")]
        public async Task<IActionResult> FetchProductionIssue([FromBody] FetchProductionIssueService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("CancelProductionIssue")]
        public async Task<IActionResult> CancelProductionIssue([FromBody] CancelProductionIssueService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetProductionIssueReconciliation")]
        public async Task<IActionResult> GetProductionIssueReconciliation([FromBody] GetProductionIssueReconciliationService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetProductionIssueEstimatedVsReceived")]
        public async Task<IActionResult> GetProductionIssueEstimatedVsReceived([FromBody] GetProductionIssueEstimatedVsReceivedService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddProductionIssueReconciliation")]
        public async Task<IActionResult> AddProductionIssueReconciliation([FromBody] AddProductionIssueReconciliationService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

    }
}