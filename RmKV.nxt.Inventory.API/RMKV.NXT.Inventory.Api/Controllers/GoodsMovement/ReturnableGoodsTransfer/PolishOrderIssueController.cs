﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.PolishOrderIssue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.ReturnableGoodsTransfer
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class PolishOrderIssueController : ControllerBase
    {
        private IMediator mediator;

        public PolishOrderIssueController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("GetPolishOrderIssue")]
        public async Task<IActionResult> GetPolishOrderIssue([FromBody] GetPolishOrderIssueService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddPolishOrderIssue")]
        public async Task<IActionResult> AddPolishOrderIssue([FromBody] AddPolishOrderIssueService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("DeletePolishOrderIssue")]
        public async Task<IActionResult> DeletePolishOrderIssue([FromBody] DeletePolishOrderIssueService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }


        [HttpPost]
        [Route("FetchPolishOrderIssue")]
        public async Task<IActionResult> FetchPolishOrderIssue([FromBody] FetchPolishOrderIssueService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}
