﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.PolishOrderReceipt;
using RMKV.NXT.Inventory.DataModel;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.ReturnableGoodsTransfer
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class PolishOrderReceiptController : ControllerBase
    {
        private IMediator mediator;

        public PolishOrderReceiptController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("GetPolishOrderReceipts")]
        public async Task<IActionResult> GetPolishOrderReceipts([FromBody] GetPolishOrderReceiptService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetPendingPolishIssueNos")]
        public async Task<IActionResult> GetPendingPolishIssueNos([FromBody] GetPendingPolishIssueNoService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetPendingPolishIssueDetails")]
        public async Task<IActionResult> GetPendingPolishIssueDetails([FromBody] GetPendingPolishIssueDetailService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddPolishOrderReceipts")]
        public async Task<IActionResult> AddPolishOrderReceipts([FromBody] AddPolishOrderReceiptService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        //[HttpPost]
        //[Route("AddPolishOrderReceipts")]
        //public async Task<IActionResult> AddPolishOrderReceipts(IFormFile[] files)
        //{
        //    var data = Request.Form["SavePolishOrder"];
        //    AddRGNEntity objPolish = JsonConvert.DeserializeObject<AddRGNEntity>(data);
        //    var FileAttachPath = Request.Form["fileRootPath"];
        //    List<int> index = JsonConvert.DeserializeObject<List<int>>(Request.Form["fileIndex"]);
        //    List<int> removedIndex = JsonConvert.DeserializeObject<List<int>>(Request.Form["removedFileIndex"]);
        //    List<OldFiles> oldFiles = JsonConvert.DeserializeObject<List<OldFiles>>(Request.Form["removedFiles"]);
        //    if (removedIndex.Count > 0)
        //    {
        //        for (int i = 0; i < removedIndex.Count; i++)
        //        {
        //            for (int j = 0; j < oldFiles.Count; j++)
        //            {
        //                int dataIndex = Convert.ToInt32(removedIndex[i]);
        //                if (oldFiles[j] != null)
        //                {
        //                    if (dataIndex == oldFiles[j].index)
        //                    {
        //                        var directory = FileAttachPath + "\\" + oldFiles[j].name.Split("\\")[0];
        //                        string fileName = oldFiles[j].name.Split("\\")[1];
        //                        DirectoryInfo directoryInfo = new DirectoryInfo(directory);
        //                        FileInfo[] OldexistFiles = directoryInfo.GetFiles(fileName.Split(".")[0] + ".*");
        //                        if (OldexistFiles.Length > 0)
        //                        {
        //                            foreach (FileInfo file in OldexistFiles)
        //                            {
        //                                file.Delete();
        //                                if (dataIndex == 0)
        //                                    objPolish.file_path[i].file_path1 = "";
        //                                else if (dataIndex == 1)
        //                                    objPolish.file_path[i].file_path2 = "";
        //                                else if (dataIndex == 2)
        //                                    objPolish.file_path[i].file_path3 = "";
        //                                else if (dataIndex == 3)
        //                                    objPolish.file_path[i].file_path4 = "";
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    if (index.Count > 0)
        //    {
        //        if (files != null && files.Length > 0)
        //        {
        //            for (int i = 0; i < index.Count; i++)
        //            {
        //                if (files[i].FileName != null && FileAttachPath.ToString() != null)
        //                {
        //                    int dataIndex = Convert.ToInt32(index[i]);
        //                    FileConfig fileConfig = Utilities.GetFileNameAndPath(FileAttachPath, "PolishOrderReceiptDocuments", files[i].FileName);
        //                    var directory = FileAttachPath + "\\PolishOrderReceiptDocuments\\";
        //                    if (!Directory.Exists(directory))
        //                    {
        //                        Directory.CreateDirectory(directory);
        //                    }
        //                    using (var stream = new FileStream(fileConfig.fileSavePath, FileMode.Create))
        //                    {
        //                        files[i].CopyTo(stream);
        //                        if (dataIndex == 0)
        //                            objPolish.file_path[i].file_path1 = fileConfig.fileName.ToString();
        //                        else if (dataIndex == 1)
        //                            objPolish.file_path[i].file_path2 = fileConfig.fileName.ToString();
        //                        else if (dataIndex == 2)
        //                            objPolish.file_path[i].file_path3 = fileConfig.fileName.ToString();
        //                        else if (dataIndex == 3)
        //                            objPolish.file_path[i].file_path4 = fileConfig.fileName.ToString();
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    List<AddRGNEntity> orderArr = new List<AddRGNEntity>();
        //    orderArr.Add(objPolish);

        //    var response = await mediator.Send(new AddPolishOrderReceiptService()
        //    {
        //        PolishOrderReceipt = JsonConvert.SerializeObject(orderArr)
        //    });
        //    return Ok(response);
        //}

        [HttpPost]
        [Route("RemovePolishOrderReceipts")]
        public async Task<IActionResult> RemovePolishOrderReceipts([FromBody] RemovePolishOrderReceiptService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("FetchPolishOrderReceiptDetails")]
        public async Task<IActionResult> FetchPolishOrderReceiptDetails([FromBody] FetchPolishOrderReceiptDetailService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}