﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.WorkOrderReceipt;
using RMKV.NXT.Inventory.DataModel;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.ReturnableGoodsTransfer;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.ReturnableGoodsTransfer
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class WorkOrderReceiptController : ControllerBase
    {
        private IMediator mediator;

        public WorkOrderReceiptController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("GetWorkOrderReceiptPendindIsssueDetails")]
        public async Task<IActionResult> GetWorkOrderReceiptPendindIsssueDetails([FromBody] GetWorkOrderReceiptPendindIsssueDetailsServices values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetWorkOrderReceiptPendindSupplierList")]
        public async Task<IActionResult> GetWorkOrderReceiptPendindSupplierList([FromBody] GetWorkOrderReceiptPendindSupplierServices values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetWorkOrderReceiptList")]
        public async Task<IActionResult> GetWorkOrderReceiptList([FromBody] GetWorkOrderReceiptListServices values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetWorkOrderReceiptListPending")]
        public async Task<IActionResult> GetWorkOrderReceiptListPending([FromBody] GetWorkOrderReceiptListPendingServices values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("AddWorkOrderReceiptList")]
        public async Task<IActionResult> AddWorkOrderReceiptList([FromBody] AddWorkOrderReceiptListPendingServices values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        //[HttpPost]
        //[Route("AddWorkOrderReceiptList")]
        //public async Task<IActionResult> AddWorkOrderReceiptList(IFormFile[] files)
        //{
        //    var data = Request.Form["SaveWorkOrderInvoice"];
        //    WorkOrderInvoiceEntity objWork = JsonConvert.DeserializeObject<WorkOrderInvoiceEntity>(data);
        //    var FileAttachPath = Request.Form["fileRootPath"];
        //    List<int> index = JsonConvert.DeserializeObject<List<int>>(Request.Form["fileIndex"]);
        //    List<int> removedIndex = JsonConvert.DeserializeObject<List<int>>(Request.Form["removedFileIndex"]);
        //    List<OldFiles> oldFiles = JsonConvert.DeserializeObject<List<OldFiles>>(Request.Form["removedFiles"]);
        //    if (removedIndex.Count > 0)
        //    {
        //        for (int i = 0; i < removedIndex.Count; i++)
        //        {
        //            for (int j = 0; j < oldFiles.Count; j++)
        //            {
        //                int dataIndex = Convert.ToInt32(removedIndex[i]);
        //                if (oldFiles[j] != null)
        //                {
        //                    if (dataIndex == oldFiles[j].index)
        //                    {
        //                        var directory = FileAttachPath + "\\" + oldFiles[j].name.Split("\\")[0];
        //                        string fileName = oldFiles[j].name.Split("\\")[1];
        //                        DirectoryInfo directoryInfo = new DirectoryInfo(directory);
        //                        FileInfo[] OldexistFiles = directoryInfo.GetFiles(fileName.Split(".")[0] + ".*");
        //                        if (OldexistFiles.Length > 0)
        //                        {
        //                            foreach (FileInfo file in OldexistFiles)
        //                            {
        //                                file.Delete();
        //                                if (dataIndex == 0)
        //                                    objWork.file_path[i].file_path1 = "";
        //                                else if (dataIndex == 1)
        //                                    objWork.file_path[i].file_path2 = "";
        //                                else if (dataIndex == 2)
        //                                    objWork.file_path[i].file_path3 = "";
        //                                else if (dataIndex == 3)
        //                                    objWork.file_path[i].file_path4 = "";
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    if (index.Count > 0)
        //    {
        //        if (files != null && files.Length > 0)
        //        {
        //            for (int i = 0; i < index.Count; i++)
        //            {
        //                if (files[i].FileName != null && FileAttachPath.ToString() != null)
        //                {
        //                    int dataIndex = Convert.ToInt32(index[i]);
        //                    FileConfig fileConfig = Utilities.GetFileNameAndPath(FileAttachPath, "WorkOrderInvoiceDocuments", files[i].FileName);
        //                    var directory = FileAttachPath + "\\WorkOrderInvoiceDocuments\\";
        //                    if (!Directory.Exists(directory))
        //                    {
        //                        Directory.CreateDirectory(directory);
        //                    }
        //                    using (var stream = new FileStream(fileConfig.fileSavePath, FileMode.Create))
        //                    {
        //                        files[i].CopyTo(stream);
        //                        if (dataIndex == 0)
        //                            objWork.file_path[i].file_path1 = fileConfig.fileName.ToString();
        //                        else if (dataIndex == 1)
        //                            objWork.file_path[i].file_path2 = fileConfig.fileName.ToString();
        //                        else if (dataIndex == 2)
        //                            objWork.file_path[i].file_path3 = fileConfig.fileName.ToString();
        //                        else if (dataIndex == 3)
        //                            objWork.file_path[i].file_path4 = fileConfig.fileName.ToString();
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    List<WorkOrderInvoiceEntity> orderArr = new List<WorkOrderInvoiceEntity>();
        //    orderArr.Add(objWork);

        //    var response = await mediator.Send(new AddWorkOrderReceiptListPendingServices()
        //    {
        //        WorkOrderIssue = JsonConvert.SerializeObject(orderArr)
        //    });
        //    return Ok(response);
        //}
        [HttpPost]
        [Route("FetchWorkOrderReceipt")]
        public async Task<IActionResult> FetchWorkOrderReceipt([FromBody] FetchWorkOrderReceiptServices values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}