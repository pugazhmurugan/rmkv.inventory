﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.ReturnableGoodsTransfer
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ProductionReceiptController : ControllerBase
    {
        private IMediator mediator;

        public ProductionReceiptController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("GetProductionReceipts")]
        public async Task<IActionResult> GetProductionReceipts([FromBody] GetProductionReceiptService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetProductionIssueNos")]
        public async Task<IActionResult> GetProductionIssueNos([FromBody] GetProductionIssueNoService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetProductionIssueDetails")]
        public async Task<IActionResult> GetProductionIssueDetails([FromBody] GetProductionIssueDetailService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetProductionReceiptProductLookup")]
        public async Task<IActionResult> GetProductionReceiptProductLookup([FromBody] GetProductionReceiptProductLookupService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddProductionReceipt")]
        public async Task<IActionResult> AddProductionReceipt([FromBody] AddProductionReceiptService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("FetchProductionReceipt")]
        public async Task<IActionResult> FetchProductionReceipt([FromBody] FetchProductionReceiptService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("RemoveProductionReceipt")]
        public async Task<IActionResult> RemoveProductionReceipt([FromBody] RemoveProductionReceiptService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

    }
}