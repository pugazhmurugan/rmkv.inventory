﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.CustomerWorkOrder;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.ReturnableGoodsTransfer
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class CustomerWorkOrderController : ControllerBase
    {
        private IMediator mediator;

        public CustomerWorkOrderController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("AddCustomerWorkOrder")]
        public async Task<IActionResult> addCustomerWorkOrder([FromBody] AddCustomerWorkOrderService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetCustomerWorkOrder")]
        public async Task<IActionResult> GetCustomerWorkOrder([FromBody] GetCustomerWorkOrderService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("FetchCustomerWorkOrder")]
        public async Task<IActionResult> FetchCustomerWorkOrder([FromBody] FetchCustomerWorkOrderService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("CancelCustomerWorkOrder")]
        public async Task<IActionResult> CancelCustomerWorkOrder([FromBody] CancelCustomerWorkOrderService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

    }
}