﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.WorkOrderIssue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.ReturnableGoodsTransfer
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class WorkOrderIssueController : ControllerBase
    {
        private IMediator mediator;

        public WorkOrderIssueController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("GetWorkOrderIssue")]
        public async Task<IActionResult> GetWorkOrderIssue([FromBody] GetWorkOrderIssueService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetWorkOrderIssueEntryGridList")]
        public async Task<IActionResult> GetWorkOrderIssueEntryGridList([FromBody] GetWorkOrderIssueEntryGridlistService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpGet]
        [Route("GetWorkOrderIssueTransferType")]
        public async Task<IActionResult> GetWorkOrderIssueTransferType()
        {
            var response = await mediator.Send(new GetWorkorderIssueTransferTypeService());
            return Ok(JsonConvert.SerializeObject(response));
        }

     
        [HttpPost]
        [Route("GetWorkOrderIssueDetails")]
        public async Task<IActionResult> GetWorkOrderIssueDetails([FromBody] GetWorkOrderIssueDetailsListService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddWorkOrderIssue")]
        public async Task<IActionResult> AddWorkOrderIssue([FromBody] AddWorkOrderIssueService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("FetchWorkOrderIssue")]
        public async Task<IActionResult> FetchWorkOrderIssue([FromBody] FetchWorkOrderIssueService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}
