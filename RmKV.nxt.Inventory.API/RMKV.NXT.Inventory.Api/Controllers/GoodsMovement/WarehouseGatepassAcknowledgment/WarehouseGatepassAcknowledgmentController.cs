﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.GoodsMovement.WarehouseGatePassAcknowledgment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.WarehouseGatepassAcknowledgment
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class WarehouseGatepassAcknowledgmentController : ControllerBase
    {
        private IMediator mediator;

        public WarehouseGatepassAcknowledgmentController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("SaveWarehouseGatePassAck")]
        public async Task<IActionResult> SaveWarehouseGatePassAck([FromBody] SaveWarehouseGatePassServices values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("LoadWarehouseGatePassAck")]
        public async Task<IActionResult> LoadWarehouseGatePassAck([FromBody] LoadWarehouseGatePassServices values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetSameWarehouse")]
        public async Task<IActionResult> GetSameWarehouse([FromBody] GetSameWarehouseService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetStoreLocation")]
        public async Task<IActionResult> GetStoreLocation([FromBody] GetStoreLocationService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetWarehouseTransferDetailAck")]
        public async Task<IActionResult> GetWarehouseTransferDetailAck([FromBody] GetWarehouseTransferDetailService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetTransferTypeDetails")]
        public async Task<IActionResult> GetTransferTypeDetails([FromBody] GetTransferTypeService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        //[HttpPost]
        //[Route("GetWarehouseGatePassListDetails")]
        //public async Task<IActionResult> GetWarehouseGatePassListDetails([FromBody] GetWarehouseGatePassService values)
        //{
        //    var response = await mediator.Send(values);
        //    return Ok(JsonConvert.SerializeObject(response));
        //}

        [HttpPost]
        [Route("GetWarehouseGatePassNoListAck")]
        public async Task<IActionResult> GetWarehouseGatePassNoListAck([FromBody] GetWarehouseGatePassNoDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetWarehouseGRNNoListAck")]
        public async Task<IActionResult> GetWarehouseGRNNoListAck([FromBody] GetWarehouseGRNNoListService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetWarehouseAckGatePassDetails")]
        public async Task<IActionResult> GetWarehouseAckGatePassDetails([FromBody] GetWarehouseGatePassDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
    }
}

    //[HttpPost]
    //[Route("GetSameWarehouse")]
    //public async Task<IActionResult> GetSameWarehouse([FromBody] GetSameWarehouseService values)
    //{
    //    var response = await mediator.Send(values);
    //    return Ok(JsonConvert.SerializeObject(response));
    //}

//[HttpPost]
//[Route("GetStoreLocation")]
//public async Task<IActionResult> GetStoreLocation([FromBody] GetStoreLocationService values)
//{
//    var response = await mediator.Send(values);
//    return Ok(JsonConvert.SerializeObject(response));
//}



