﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.Business.GoodsMovement.TransfersExceptionReports.GoodsTransferException;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.TransfersExceptionReports
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class GoodsTransferExceptionController : ControllerBase
    {
        private IMediator mediator;

        public GoodsTransferExceptionController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("GetGoodsTransferExceptionReport")]
        public async Task<IActionResult> GetGoodsTransferExceptionReport([FromBody] GoodsTransferExceptionReportService values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                if (values.Type)
                {
                    string webRootPath = "Reports/Goods_Transfer_Exception_Details_Report.frx";
                    FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                    return stream;
                }
                else
                {
                    string webRootPath = "Reports/Goods_Transfer_Exception_Report.frx";
                    FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                    return stream;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}