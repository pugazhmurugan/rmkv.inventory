﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.IO;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.Business.GoodsMovement.TransfersExceptionReports.CounterToGodownException;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.TransfersExceptionReports
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class CounterToGodownExceptionController : ControllerBase
    {
        private IMediator mediator;

        public CounterToGodownExceptionController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("GetCounterToGodownException")]
        public async Task<IActionResult> CounterToGodownExceptionReportDetails([FromBody] CounterToGodownExceptionService values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                if (values.Type)
                {
                    var webRootPath = Path.GetFullPath(Path.Combine("Reports/Counter_To_Godown_Acknowledgement_Report.frx"));
                    FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                    return stream;
                }
                else
                {
                    var webRootPath = Path.GetFullPath(Path.Combine("Reports/CG_Counter_To_Godown_Acknowledgement_Report2.frx"));
                    FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                    return stream;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }
}