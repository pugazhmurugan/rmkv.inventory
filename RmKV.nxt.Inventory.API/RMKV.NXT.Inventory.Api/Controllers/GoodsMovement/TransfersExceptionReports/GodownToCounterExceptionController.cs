﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.Business.GoodsMovement.TransfersExceptionReports.GodownToCounterException;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.TransfersExceptionReports
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class GodownToCounterExceptionController : ControllerBase
    {
        private IMediator mediator;

        public GodownToCounterExceptionController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("GetGodownToCounterException")]
        public async Task<IActionResult> GodownToCounterExceptionReportDetails([FromBody] GodownToCounterExceptionService values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                if (values.Type)
                {
                    var webRootPath = Path.GetFullPath(Path.Combine("Reports/GodownToCounterExceptionReportWithDetails.frx"));
                    FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                    return stream;
                }
                else
                {
                    var webRootPath = Path.GetFullPath(Path.Combine("Reports/GodownToCounterExceptionReportwithOutDetails.frx"));
                    FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                    return stream;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }
}