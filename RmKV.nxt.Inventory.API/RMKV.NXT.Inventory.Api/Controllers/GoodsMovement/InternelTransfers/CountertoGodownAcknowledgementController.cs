﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.CountertoGodownAcknowledgement;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.GoodsTransfers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class CountertoGodownAcknowledgementController : ControllerBase
    {
        private IMediator mediator;

        public CountertoGodownAcknowledgementController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("LoadCtoGAcknowledgementDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> LoadGtoCAcknowledgementDetails([FromBody] LoadCtoGService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("LoadCtoGTransferNoBasedDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> LoadTransferNoBasedDetails([FromBody] LoadCtoGTransferNoBasedDataService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("AddCtoGAcknowledgementDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> AddGtoCAcknowledgementDetails([FromBody] AddCtoGAcknowledgementService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("FetchCtoGAcknowledgementDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> FetchCtoGAcknowledgementDetails([FromBody] FetchCtoGAcknowledgementService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("CheckCtoGAckTransferNoDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckCtoGAckTransferNoDetails([FromBody] CheckCtoGAckTransferNoService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
    }
}