﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.IO;
using RmKV.NXT.Inventory.DataModel;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.CounterToGodown;
using RmKV.NXT.Inventory.DataModel.utilities;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.GoodsTransfers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class CounterToGodownController : ControllerBase
    {
        private IMediator mediator;

        public CounterToGodownController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("AddCounterToGodown")]
        public async Task<IActionResult> AddCounterToGodown([FromBody] AddCounterToGodownService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("ApproveCounterToGodown")]
        public async Task<IActionResult> ApproveCounterToGodown([FromBody] ApproveCounterToGodownService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("CancelCounterToGodown")]
        public async Task<IActionResult> CancelCounterToGodown([FromBody] CancelCounterToGodownService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetToWarehouse")]
        public async Task<IActionResult> GetToLocation([FromBody] GetToWarehouseService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetCounterToGodown")]
        public async Task<IActionResult> GetCounterToGodown([FromBody] GetCounterToGodownService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetCounterTransferNo")]
        public async Task<IActionResult> GetTransferNo([FromBody] GetCounterTransferNoService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("FetchCounterToGodown")]
        public async Task<IActionResult> FetchGood([FromBody] FetchCounterToGodownService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("CheckCounterToGodownReport")]
        public async Task<IActionResult> CheckCounterToGodownReport([FromBody] CheckCounterToGodownReportService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetCounterToGodownReport")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCounterToGodownReport(GetCounterToGodownReportService values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                var webRootPath = Path.GetFullPath(Path.Combine("Reports/Counter_To_Godown_Transfer_Report.frx"));
                //string webRootPath = "Reports/Form_S_Report.frx";

                FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                return stream;
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
