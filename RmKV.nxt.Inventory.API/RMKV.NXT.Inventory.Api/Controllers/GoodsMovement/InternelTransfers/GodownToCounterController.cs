﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.GoodsTransfers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class GodownToCounterController : ControllerBase
    {
        private IMediator mediator;

        public GodownToCounterController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("AddGodownToCounter")]
        public async Task<IActionResult> addGodownToCounter([FromBody] AddGodownToCounterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("ApproveGodownToCounter")]
        public async Task<IActionResult> ApproveGodownToCounter([FromBody] ApproveGodownToCounterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("CancelGodownToCounter")]
        public async Task<IActionResult> CancelGodownToCounter([FromBody] CancelGodownToCounterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetToLocation")]
        public async Task<IActionResult> GetToLocation([FromBody] GetGodownToLocationsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetGodownToCounter")]
        public async Task<IActionResult> GetGodownToCounter([FromBody] GetGodownToCounterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetTransferNo")]
        public async Task<IActionResult> GetTransferNo([FromBody] GetGodownTransferNoService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("FetchGodownToCounter")]
        public async Task<IActionResult> FetchGodownToCounter([FromBody] FetchGodownToCounterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetGodownToCounterReport")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetGodownToCounterReport(GetGodownToCounterReportServices values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                var webRootPath = Path.GetFullPath(Path.Combine("Reports/GodownToCounterReports.frx"));
                //string webRootPath = "Reports/Bonus_Slip_Report.frx";

                FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                return stream;
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("CheckGodownToCounterReportDetails")]
        public async Task<IActionResult> CheckGodownToCounterReportDetails([FromBody] CheckGodownToCounterReportServices values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

    }
}