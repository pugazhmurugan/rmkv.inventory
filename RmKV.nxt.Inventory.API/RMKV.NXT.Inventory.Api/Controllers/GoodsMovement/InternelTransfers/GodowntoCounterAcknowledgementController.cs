﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.GodowntoCounterAcknowledgement;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.GoodsTransfers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class GodowntoCounterAcknowledgementController : ControllerBase
    {
        private IMediator mediator;

        public GodowntoCounterAcknowledgementController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("LoadGtoCAcknowledgementDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> LoadGtoCAcknowledgementDetails([FromBody] LoadGtoCACKService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("LoadTransferNoBasedDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> LoadTransferNoBasedDetails([FromBody] LoadTransferNoBasedDataService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("AddGtoCAcknowledgementDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> AddGtoCAcknowledgementDetails([FromBody] AddGtoCAcknowledgementService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("FetchGtoCAcknowledgementDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> FetchGtoCAcknowledgementDetails([FromBody] FetchGtoCAcknowledgementService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("CheckGtoCAckTransferNoDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckGtoCAckTransferNoDetails([FromBody] CheckGtoCAckTransferNoService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
    }
}