﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsReceipts.GoodsTransferReceipts;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.GoodsReceipts
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class GoodsTransferReceiptsController : ControllerBase
    {
        private IMediator mediator;

        public GoodsTransferReceiptsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("LoadGoodsTransferReceiptsDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> LoadGoodsTransferReceiptsDetails([FromBody] LoadGoodsTransferReceiptsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("LoadGoodsReceiptsTransferNo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> LoadGoodsReceiptsTransferNo([FromBody] LoadGoodsReceiptsTransferNoService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("AddGoodsTransferReceiptsDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> AddGoodsTransferReceiptsDetails([FromBody] AddGoodsTransferReceiptsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("FetchGoodsTransferReceiptsDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> FetchGoodsTransferReceiptsDetails([FromBody] FetchGoodsTransferReceiptsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("CheckGoodsTrandferReceiptsTransferNoDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckGoodsTrandferReceiptsTransferNoDetails([FromBody] CheckReceiptsAckTransferNoService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("LoadGoodsReceiptsProductDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> LoadGoodsReceiptsProductDetails([FromBody] LoadGoodsReceiptsProductDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetAllWarehouseListDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllWarehouseListDetails([FromBody] GetAllWarehouseListService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
    }
}