﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsReceipts.GoodsTransferBales;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.GoodsReceipts
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class GoodsTransferBalesController : ControllerBase
    {
        private IMediator mediator;

        public GoodsTransferBalesController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("LoadGoodsTransferBalesDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> LoadGoodsTransferBalesDetails([FromBody] LoadGoodsTransferBalesService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("AddGoodsTransferBalesDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> AddGoodsTransferBalesDetails([FromBody] AddGoodsTransferBalesService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetBalesSectionGRNList")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetBalesSectionGRNList([FromBody] GetGoodsBalesSectionGRNService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
    }
}