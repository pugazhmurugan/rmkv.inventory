﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using RmKV.NXT.Inventory.DataModel.utilities;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableTransferReports;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.ReturnableTransferReports
{
    [Route("api/[controller]")]
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class ReturnableTransferReportsController : ControllerBase
    {
        private IMediator mediator;
        public ReturnableTransferReportsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("GetWorkOrderIssueListReport")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetWorkOrderIssueListReport(GetWorkOrderIssueListService values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                var webRootPath = Path.GetFullPath(Path.Combine("Reports/.frx"));
                //string webRootPath = "Reports/Payslip_Report.frx";

                FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                return stream;
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("GetWorkOrderReceiptListReport")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetWorkOrderReceiptListReport(GetWorkOrderReceiptListService values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                var webRootPath = Path.GetFullPath(Path.Combine("Reports/.frx"));
                //string webRootPath = "Reports/Payslip_Report.frx";

                FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                return stream;
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("GetMismatchStoneQtyReport")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetMismatchStoneQtyReport(GetMismatchStoneQtyService values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                var webRootPath = Path.GetFullPath(Path.Combine("Reports/.frx"));
                //string webRootPath = "Reports/Payslip_Report.frx";

                FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                return stream;
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("GetPolishPendingIssueReport")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPolishPendingIssueReport(GetPolishPendingReportService values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                var webRootPath = Path.GetFullPath(Path.Combine("Reports/Pending_Polish_Issue_List_Report.frx"));
                FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                return stream;
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }
}