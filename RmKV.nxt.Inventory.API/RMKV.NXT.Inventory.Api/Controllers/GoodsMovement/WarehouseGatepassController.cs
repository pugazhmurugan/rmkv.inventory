﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.WarehouseGatepass;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]

    public class WarehouseGatepassController : ControllerBase
    {
        private IMediator mediator;
        public WarehouseGatepassController(IMediator mediator)
        {
            this.mediator = mediator;
        }


        [HttpPost]
        [Route("GetWarehouseGatePassType")]
        public async Task<IActionResult> GetGoodsTransferType([FromBody] GetWarehouseGatePassTypeService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }


        [HttpPost]
        [Route("AddWarehouseGatePass")]
        public async Task<IActionResult> AddWarehouseGatePass([FromBody] AddWarehouseGatePassService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("CancelWarehouseGatePass")]
        public async Task<IActionResult> CancelWarehouseGatePass([FromBody] CancelWarehouseGatePassService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("FetchWarehouseGatePass")]
        public async Task<IActionResult> FetchWarehouseGatePass([FromBody] FetchWarehouseGatePassService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetWarehouseGatePass")]
        public async Task<IActionResult> GetWarehouseGatePass([FromBody] GetWarehouseGatePassService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetTransferDetails")]
        public async Task<IActionResult> GetTransferDetails([FromBody] GetTransferDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetGatepassTowarehouse")]
        public async Task<IActionResult> GetGatepassTowarehouse([FromBody] GetGatepassTowarehouseService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GatepassNo")]
        public async Task<IActionResult> GatepassNo([FromBody] GatepassNoService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GatepassToLocation")]
        public async Task<IActionResult> GatepassNo([FromBody] GatepassToLocationService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}