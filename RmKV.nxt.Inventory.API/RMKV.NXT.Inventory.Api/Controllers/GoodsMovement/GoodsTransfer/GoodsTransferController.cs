﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfer;

namespace RMKV.NXT.Inventory.Api.Controllers.GoodsMovement.GoodsTransfer
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]

    public class GoodsTransferController : ControllerBase
    {
        private IMediator mediator;
        public GoodsTransferController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        /////////////////////////////////////////////////////////Common////////////////////////////////////////////////////////////
        [HttpPost]
        [Route("GetGoodsTransferType")]
        public async Task<IActionResult> GetGoodsTransferType([FromBody] GetGoodsTransferTypeService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }


        [HttpPost]
        [Route("GetWithincityVirtualToStore")]
        public async Task<IActionResult> GetWithincityVirtualToStore([FromBody] GetWithincityVirtualToStoreService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetWithinStateToLocation")]
        public async Task<IActionResult> GetWithinStateToLocation([FromBody] GetWithinStateToLocationService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetOtherStateToLocation")]
        public async Task<IActionResult> GetOtherStateToLocation([FromBody] GetOtherStateToLocationService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetWithinStateVirtualStoreToStore")]
        public async Task<IActionResult> GetWithinStateVirtualStoreToStore([FromBody] GetWithinStateVirtualStoreToStoreService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetGoodsTransferFromStore")]
        public async Task<IActionResult> GoodsTransferFromStore([FromBody] GoodsTransferFromStoreService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        ////////////////////////////////////////Goods Transfer (with In City)/////////////////////////////////////////
        [HttpPost]
        [Route("AddWithincity")]
        public async Task<IActionResult> AddWithincity([FromBody] AddWithincityService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("ApproveWithincity")]
        public async Task<IActionResult> ApproveWithincity([FromBody] ApproveWithincityService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetTransferNoWithincity")]
        public async Task<IActionResult> GetTransferNoWithincity([FromBody] GetTransferNoWithincityService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("CancelWithincity")]
        public async Task<IActionResult> CancelWithincity([FromBody] CancelWithincityService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("FetchWithincity")]
        public async Task<IActionResult> FetchWithincity([FromBody] FetchWithincityService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetWithincity")]
        public async Task<IActionResult> GetWithincity([FromBody] GetWithincityService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }


        [HttpPost]
        [Route("GoodsTransferReportDetails")]
        public async Task<IActionResult> GoodsTransferReportDetails([FromBody] GoodsTransferReportService values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                if (values.Type)
                {
                    var webRootPath = Path.GetFullPath(Path.Combine("Reports/Goods_Transfer_Report.frx"));
                    //string webRootPath = "Reports/Salary_Statement_Bank_Report.frx";
                    FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                    return stream;
                }
                else
                {
                    var webRootPath = Path.GetFullPath(Path.Combine("Reports/Goods_Transfer_Invoice_List_Report.frx"));
                    //string webRootPath = "Reports/Salary_Statement_Cash_Report.frx";
                    FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                    return stream;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }


        [HttpPost]
        [Route("CheckGoodsTransferReportDetails")]
        public async Task<IActionResult> CheckGoodsTransferReportDetails([FromBody] CheckGoodsTransferReportService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

    }
}