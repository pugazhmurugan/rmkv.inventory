﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.Business.Tools.ProductChange;

namespace RMKV.NXT.Inventory.Api.Controllers.Tools
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]

    public class ProductChangeController : ControllerBase
    {
        private IMediator mediator;

        public ProductChangeController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("GetProductChangeLookup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetProductChangeLookup([FromBody] GetProductChangeService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetStyleChangeLookup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetStyleChangeLookup([FromBody] GetStyleChangeService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetCounterChangeLookup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCounterChangeLookup([FromBody] GetCounterChangeService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("Addcounterchange")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Addcounterchange([FromBody] counterchangesaveService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("Addproductchange")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Addproductchange([FromBody] productchangesaveService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }


        [HttpPost]
        [Route("Addstylecodechange")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Addstylecodechange([FromBody] stylecodechangesaveService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        ////Report
        [HttpPost]
        [Route("GetCounterChangeHistoryReport")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCounterChangeHistoryReport(GetCounterChangeHistoryReportServices values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                var webRootPath = Path.GetFullPath(Path.Combine("Reports/Counter_Change_History_Report.frx"));
                //string webRootPath = "Reports/Bonus_Slip_Report.frx";

                FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                return stream;
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpPost]
        [Route("GetProductChangeHistoryReport")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetProductChangeHistoryReport(GetProductChangeHistoryReportServices values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                var webRootPath = Path.GetFullPath(Path.Combine("Reports/Product_Change_History_Report.frx"));
                //string webRootPath = "Reports/Bonus_Slip_Report.frx";

                FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                return stream;
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}