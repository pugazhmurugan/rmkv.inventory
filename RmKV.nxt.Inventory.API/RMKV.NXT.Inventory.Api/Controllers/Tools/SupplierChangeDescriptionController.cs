﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.Tools.SupplierDescriptionChange;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Api.Controllers.Tools
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class SupplierChangeDescriptionController : ControllerBase
    {
        private IMediator mediator;

        public SupplierChangeDescriptionController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("AddSupplierDescriptionChange")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> AddProductDescriptionChange([FromBody] AddSupplierDescriptionService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}
