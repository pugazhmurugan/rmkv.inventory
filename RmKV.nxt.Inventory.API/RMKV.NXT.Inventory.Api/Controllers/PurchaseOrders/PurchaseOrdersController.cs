﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.Business.PurchaseOrders.PurchaseOrders;
using RMKV.NXT.Inventory.DataModel;
using RMKV.NXT.Inventory.DataModel.Model.PurchaseOrders;

namespace RMKV.NXT.Inventory.Api.Controllers.PurchaseOrders
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class PurchaseOrdersController : ControllerBase
    {
        private IMediator mediator;

        public PurchaseOrdersController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("GetPOAnalysis")]
        public async Task<IActionResult> GetPOAnalysis([FromBody] GetPOAnalysisService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpGet]
        [Route("GetSupplierCityDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetSupplierCityDetails()
        {
            var response = await mediator.Send(new GetSupplierCityDetailsService());
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetSupplierGroupDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetSupplierGroupDetails([FromBody] GetSupplierGroupDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetSupplierByGroupIdList")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetSupplierByGroupIdList([FromBody] GetSupplierByGroupIdService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetModeOfDispatchDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetModeOfDispatchDetails([FromBody] GetModeofDispatchMasterService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetUOMMasterDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUOMMasterDetails([FromBody] GetUOMDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetPurchaseOrderDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPurchaseOrderDetails([FromBody] GetPurchaseOrderListService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("CancelPurchaseOrderDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CancelPurchaseOrderDetails([FromBody] CancelPurchaseOrderListService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        //[HttpPost]
        //[Route("SavePurchaseOrderDetails")]
        //public async Task<IActionResult> SavePurchaseOrderDetails(IFormFile files)
        //{
        //    var data = Request.Form["SavePurchaseOrder"];
        //    AddPurchaseOrdersList poList = JsonConvert.DeserializeObject<AddPurchaseOrdersList>(data);
        //    var FileAttachPath = Request.Form["fileRootPath"];

        //    if (poList.OldDocumentPathForPO != null && poList.OldDocumentPathForPO != "" && poList.OldDocumentPathForPO != "null")
        //    {
        //        var directory = FileAttachPath + "\\" + poList.OldDocumentPathForPO.Split("\\")[0];
        //        string fileName = poList.OldDocumentPathForPO.Split("\\")[1];
        //        DirectoryInfo directoryInfo = new DirectoryInfo(directory);
        //        FileInfo[] OldexistFiles = directoryInfo.GetFiles(fileName.Split(".")[0] + ".*");
        //        if (OldexistFiles.Length > 0)
        //        {
        //            foreach (FileInfo file in OldexistFiles)
        //            {
        //                file.Delete();
        //                poList.file_path = "";
        //            }
        //        }
        //    }

        //    if (oldDocumentPath2 != null && oldDocumentPath2 != "" && oldDocumentPath2 != "null")
        //    {
        //        var directory = FileAttachPath + "\\" + oldDocumentPath2.Split("\\")[0];
        //        string fileName = oldDocumentPath2.Split("\\")[1];
        //        DirectoryInfo directoryInfo = new DirectoryInfo(directory);
        //        FileInfo[] OldexistFiles = directoryInfo.GetFiles(fileName.Split(".")[0] + ".*");
        //        if (OldexistFiles.Length > 0)
        //        {
        //            foreach (FileInfo file in OldexistFiles)
        //            {
        //                file.Delete();
        //                poList.bank_file_path = "";
        //            }
        //        }
        //    }

        //    if (files != null && files.Length > 0)
        //    {
        //        if (files.FileName != null && FileAttachPath.ToString() != null)
        //        {
        //            FileConfig fileConfig = Utilities.GetFileNameAndPath(FileAttachPath, "PurchaseOrderDocuments", files.FileName);
        //            using (var stream = new FileStream(fileConfig.fileSavePath, FileMode.Create))
        //            {
        //                files.CopyTo(stream);
        //                poList.file_path = fileConfig.fileName.ToString();
        //            }
        //        }
        //    }

        //    List<AddPurchaseOrdersList> ListArr = new List<AddPurchaseOrdersList>();
        //    ListArr.Add(poList);

        //    var response = await mediator.Send(new SavePurchaseOrderListService()
        //    {
        //        SavePurchaseOrder = JsonConvert.SerializeObject(ListArr)
        //    });
        //    return Ok(response);
        //}

        [HttpPost]
        [Route("SavePurchaseOrderDetails")]
        public async Task<IActionResult> SavePurchaseOrderDetails(IFormFile[] files)
        {
            var data = Request.Form["SavePurchaseOrder"];
            AddPurchaseOrdersList poList = JsonConvert.DeserializeObject<AddPurchaseOrdersList>(data);
            var fileRootPath = Request.Form["fileRootPath"];
            List<int> index = JsonConvert.DeserializeObject<List<int>>(Request.Form["fileIndex"]);
            if (index.Count > 0)
            {
                if (files.Length > 0)
                {

                    for (int i = 0; i < index.Count; i++)
                    {
                        int dataIndex = Convert.ToInt32(index[i]);
                        if (files[i].FileName != null && fileRootPath.ToString() != null)
                        {

                            FileConfig fileConfig = Utilities.GetFileNameAndPath(fileRootPath, "PurchaseOrderDocuments", files[i].FileName);
                            var directory = fileRootPath + "\\PurchaseOrderDocuments\\";
                            if (!Directory.Exists(directory))
                            {
                                Directory.CreateDirectory(directory);
                            }
                            using (var stream = new FileStream(fileConfig.fileSavePath, FileMode.Create))
                            {
                                files[i].CopyTo(stream);
                                poList.details[dataIndex].image_path = fileConfig.fileName;
                            }
                        }
                    }
                }
            }
            if (poList.isBankFile == true)
            {
                if (files.Length > index.Count)
                {
                    int fileIndex = 0;
                    if (poList.isCommonFile == true)
                        fileIndex = index.Count - 1;
                    else fileIndex = index.Count;
                    if (files.Length > 0)
                    {
                        if (poList.OldDocumentPathForBank != null && poList.OldDocumentPathForBank != "" && poList.OldDocumentPathForBank != "null")
                        {
                            var directory = fileRootPath + "\\" + poList.OldDocumentPathForBank.Split("\\")[0];
                            string fileName = poList.OldDocumentPathForBank.Split("\\")[1];
                            DirectoryInfo directoryInfo = new DirectoryInfo(directory);
                            FileInfo[] OldexistFiles = directoryInfo.GetFiles(fileName.Split(".")[0] + ".*");
                            if (OldexistFiles.Length > 0)
                            {
                                foreach (FileInfo file in OldexistFiles)
                                {
                                    file.Delete();
                                    poList.bank_file_path[0].bank_file_path = "";
                                }
                            }
                        }
                        if (files[fileIndex].FileName != null && fileRootPath.ToString() != null)
                        {
                            FileConfig fileConfig = Utilities.GetFileNameAndPath(fileRootPath, "PurchaseOrderDocuments", files[fileIndex].FileName);
                            using (var stream = new FileStream(fileConfig.fileSavePath, FileMode.Create))
                            {
                                files[fileIndex].CopyTo(stream);
                                poList.bank_file_path[0].bank_file_path = fileConfig.fileName.ToString();
                            }
                        }
                    }
                }
            }
            if (poList.isCommonFile == true)
            {
                if (files.Length > index.Count)
                {
                    int fileIndex = 0;
                    if (poList.isBankFile == true)
                        fileIndex = index.Count;
                    else fileIndex = index.Count + 1;
                    if (files.Length > 0)
                    {
                        if (poList.OldDocumentPathForPO != null && poList.OldDocumentPathForPO != "" && poList.OldDocumentPathForPO != "null")
                        {
                            var directory = fileRootPath + "\\" + poList.OldDocumentPathForPO.Split("\\")[0];
                            string fileName = poList.OldDocumentPathForPO.Split("\\")[1];
                            DirectoryInfo directoryInfo = new DirectoryInfo(directory);
                            FileInfo[] OldexistFiles = directoryInfo.GetFiles(fileName.Split(".")[0] + ".*");
                            if (OldexistFiles.Length > 0)
                            {
                                foreach (FileInfo file in OldexistFiles)
                                {
                                    file.Delete();
                                    poList.file_path[0].file_path = "";
                                }
                            }
                        }
                        if (files[fileIndex].FileName != null && fileRootPath.ToString() != null)
                        {
                            FileConfig fileConfig = Utilities.GetFileNameAndPath(fileRootPath, "PurchaseOrderDocuments", files[fileIndex].FileName);
                            using (var stream = new FileStream(fileConfig.fileSavePath, FileMode.Create))
                            {
                                files[fileIndex].CopyTo(stream);
                                poList.file_path[0].file_path = fileConfig.fileName.ToString();
                            }
                        }
                    }
                }
            }

            List<AddPurchaseOrdersList> ListArr = new List<AddPurchaseOrdersList>();
            ListArr.Add(poList);

            var response = await mediator.Send(new SavePurchaseOrderListService()
            {
                SavePurchaseOrder = JsonConvert.SerializeObject(ListArr)
            });
            return Ok(response);
        }

        //public HttpResponseMessage ManualEmail(ManualEmailDTO POobj)
        //{
        //    HttpResponseMessage message;
        //    try
        //    {
        //        Purchase_OrderDAL dal = new Purchase_OrderDAL();
        //        var dynObj = new { result = dal.ManualEmailDAL(POobj) };
        //        bool mailStatus = SendEmail(dynObj.result, dynObj.result.Company_Name, POobj.Section_Name);
        //        message = Request.CreateResponse(HttpStatusCode.OK, mailStatus);
        //        //if(mailStatus == true)
        //        // message = Request.CreateResponse(HttpStatusCode.OK, dynObj);
        //        //else
        //        // message = Request.CreateResponse(HttpStatusCode.OK, mailStatus);
        //    }
        //    catch (Exception ex)
        //    {
        //        message = Request.CreateResponse(HttpStatusCode.BadRequest, new { msgText = "Something wrong. Try Again!" });
        //    }
        //    return message;
        //}

        [HttpPost]
        [Route("ManualEmail")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> ManualEmail([FromBody] FetchPurchaseOrderListService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("FetchPurchaseOrderDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> FetchPurchaseOrderDetails([FromBody] FetchPurchaseOrderListService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetDispatchNameListDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetDispatchNameListDetails([FromBody] GetDispatchNameListService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetPurchaseOrderSizeName")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPurchaseOrderSizeName([FromBody] GetPurchaseOrderSizeNameService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetPOSizeNameBasedSizes")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPOSizeNameBasedSizes([FromBody] GetPOSizeNameBasedSizesService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetPONoDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPONoDetails([FromBody] GetPONoService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetEmailPurchaseOrderDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetEmailPurchaseOrderDetails([FromBody] GetPurchaseOrderEmailService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
    }
}