﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.PurchaseOrders.DetailedPurchaseOrder;

namespace RMKV.NXT.Inventory.Api.Controllers.PurchaseOrders
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class DetailedPurchaseOrderController : ControllerBase
    {
        private IMediator mediator;

        public DetailedPurchaseOrderController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("SaveDetailedPODetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> SaveDetailedPODetails([FromBody] SaveDetailedPODetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("CancelDetailedPODetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CancelDetailedPODetails([FromBody] CancelDetailedPODetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetDetailedPODetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetDetailedPODetails([FromBody] LoadDetailedPODetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("FetchDetailedPODetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> FetchDetailedPODetails([FromBody] FetchDetailedPODetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetProductGroupDetailedPODetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetProductGroupDetailedPODetails([FromBody] GetProductGroupListForDetailedPOService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetProductDetailedPODetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetProductDetailedPODetails([FromBody] GetProductListForDetailedPOService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetDesignCodeDetailedPODetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetDesignCodeDetailedPODetails([FromBody] GetDesignCodeListForDetailedPOService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetBrandDetailedPODetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetBrandDetailedPODetails([FromBody] GetBrandListForDetailedPOService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetColorListForDetailedPODetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetColorListForDetailedPODetails([FromBody] GetColorListForDetailedPOService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetQualityListForDetailedPODetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetQualityListForDetailedPODetails([FromBody] GetQualityListForDetailedPOService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetSizeListForDetailedPODetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetSizeListForDetailedPODetails([FromBody] GetSizeForDetailedPOService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetLocationsDetailedPODetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetLocationsDetailedPODetails([FromBody] GetLocationsForDetailedPOService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetDetailedPONoDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetDetailedPONoDetails([FromBody] GetDetailedPONoService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("GetEmailDetailedPurchaseOrderDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetEmailDetailedPurchaseOrderDetails([FromBody] GetDetailedPurchaseOrderEmailService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
    }
}