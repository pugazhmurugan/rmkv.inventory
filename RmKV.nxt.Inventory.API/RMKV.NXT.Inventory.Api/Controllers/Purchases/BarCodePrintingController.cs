﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.Purchases.BarCodePrinting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Api.Controllers.Purchases
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class BarCodePrintingController : ControllerBase
    {
        private IMediator mediator;

        public BarCodePrintingController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("GetBarCodePrinting")]
        public async Task<IActionResult> GetBarCodePrinting([FromBody] BarCodePrintingService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GenerateBarcodePrint")]
        public async Task<IActionResult> GenerateBarcodePrint([FromBody] GenerateBarcodePrintService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("SelectBarCodePrinting")]
        public async Task<IActionResult> SelectBarCodePrinting([FromBody] SelectBarCodePrintingService values)
        {
            var response = await mediator.Send(values);
            foreach (var data in response)
            {
                if (data.Caption == "")
                {
                    StringFormat format1 = new StringFormat(StringFormatFlags.NoClip);
                    StringFormat format2 = new StringFormat(format1);
                    format1.LineAlignment = StringAlignment.Far;
                    format1.Alignment = StringAlignment.Far;
                    var rupees = "RS:" + data.selling_price + "." + "00";
                    //var product = "LLMF026X-C12A";
                    PrintDocument pd = new PrintDocument();
                    pd.OriginAtMargins = true;
                    pd.DefaultPageSettings.Landscape = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
                    pd.DefaultPageSettings.PrinterResolution = new PrinterResolution();
                    pd.PrintPage += (sender, ev) =>
                    {
                        //ev.Graphics.DrawImage(data.images, new RectangleF(5, 5, 95, 95));
                        //ev.Graphics.DrawString(data.byno.ToString(), new Font("Swiss 721 BT Bold", 10, FontStyle.Bold), Brushes.Black, new RectangleF(93, 13, 260, 50), new StringFormat(format2));
                        //ev.Graphics.DrawString(data.product_code + " -" + data.counter_code, new Font("Swiss 721 BT Bold", 9, FontStyle.Bold), Brushes.Black, new RectangleF(93, 30, 250, 65), new StringFormat(format2));
                        //ev.Graphics.DrawString(rupees, new Font("Swiss 721 BT Bold", 12, FontStyle.Bold), Brushes.Black, new RectangleF(93, 45, 210, 65), new StringFormat(format2));


                       // ev.Graphics.DrawImage(data.images, new RectangleF(3, 1, 90, 90));
                        ev.Graphics.DrawString(data.byno.ToString(), new Font("Swiss 721 BT Bold", 11, FontStyle.Bold), Brushes.Black, new RectangleF(87, 7, 260, 50), new StringFormat(format2));
                        ev.Graphics.DrawString(data.product_code + " -" + data.counter_code, new Font("Swiss 721 BT Bold", 9, FontStyle.Bold), Brushes.Black, new RectangleF(87, 25, 250, 65), new StringFormat(format2));
                        ev.Graphics.DrawString(rupees, new Font("Swiss 721 BT Bold", 12, FontStyle.Bold), Brushes.Black, new RectangleF(87, 40, 210, 65), new StringFormat(format2));


                    };
                    pd.Print();
                    pd.Dispose();
                }
                else
                {
                    StringFormat format1 = new StringFormat(StringFormatFlags.NoClip);
                    StringFormat format2 = new StringFormat(format1);
                    format1.LineAlignment = StringAlignment.Far;
                    format1.Alignment = StringAlignment.Far;
                    var rupees = "RS:" + data.selling_price + "." + "00";
                    //var product = "LLMF026X-C12A";
                    PrintDocument pd = new PrintDocument();
                    pd.OriginAtMargins = true;
                    pd.DefaultPageSettings.Landscape = false;
                    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
                    pd.DefaultPageSettings.PrinterResolution = new PrinterResolution();
                    pd.PrintPage += (sender, ev) =>
                    {
                        //ev.Graphics.DrawImage(data.images, new RectangleF(5, 5, 95, 95));
                       // ev.Graphics.DrawImage(data.images, new RectangleF(3, -1, 90, 90));
                        //ev.Graphics.DrawString(data.byno.ToString(), new Font("Swiss 721 BT Bold", 10, FontStyle.Bold), Brushes.Black, new RectangleF(95, 5, 260, 50), new StringFormat(format2));
                        ev.Graphics.DrawString(data.byno.ToString(), new Font("Swiss 721 BT Bold", 11, FontStyle.Bold), Brushes.Black, new RectangleF(87, 7, 260, 50), new StringFormat(format2));
                        ev.Graphics.DrawString(data.product_code +"-"+ data.counter_code +"-"+ data.Caption, new Font("Swiss 721 BT Bold", 9, FontStyle.Bold), Brushes.Black, new RectangleF(87, 25, 250, 65), new StringFormat(format2));
                        ev.Graphics.DrawString(rupees, new Font("Swiss 721 BT Bold", 12, FontStyle.Bold), Brushes.Black, new RectangleF(87, 40, 230, 65), new StringFormat(format2));
                        ev.Graphics.DrawString(rupees, new Font("Swiss 721 BT Bold", 12, FontStyle.Bold), Brushes.Black, new RectangleF(190, 40, 150, 65), new StringFormat(format2));
                        //ev.Graphics.DrawString(rupees, new Font("Swiss 721 BT Bold", 12, FontStyle.Bold), Brushes.Black, new RectangleF(93, 60, 210, 65), new StringFormat(format2));
                    };
                    pd.Print();
                    pd.Dispose();
                }
            }              
            return Ok(response);
    }

        [HttpPost]
        [Route("DoubleRateBarCodePrinting")]
        public async Task<IActionResult> DoubleRateBarCodePrinting([FromBody] DoublerateBarCodePrintingService values)
        {
            var response = await mediator.Send(values);
            //foreach (var data in response)
            //{
            //    StringFormat format3 = new StringFormat(StringFormatFlags.DirectionVertical);
            //    StringFormat format4 = new StringFormat();
            //    format4.Alignment = StringAlignment.Center;
            //    StringFormat format1 = new StringFormat(StringFormatFlags.NoWrap);
            //    StringFormat format2 = new StringFormat(format1);
            //    format1.LineAlignment = StringAlignment.Far;
            //    format1.Alignment = StringAlignment.Far;                
            //    var rupees = "RS:" + data.selling_price ;
            //    //var rupees = "RS:" + data.selling_price + "." + "00";
            //    var product = "LLMF026X-C12A";                
            //    PrintDocument pd = new PrintDocument();
            //    pd.OriginAtMargins = true;
            //    pd.DefaultPageSettings.Landscape = false;
            //    pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
            //    pd.DefaultPageSettings.PrinterResolution = new PrinterResolution();
            //    pd.PrintPage += (sender, ev) =>
            //    {
            //        ev.Graphics.DrawImage(data.images, new RectangleF(5, 5, 95, 95));
            //        ev.Graphics.DrawString(data.byno.ToString(), new Font("Swiss 721 BT Bold", 11, FontStyle.Bold), Brushes.Black, new RectangleF(93, 13, 260, 50), new StringFormat(format2));
            //        ev.Graphics.DrawString(product, new Font("Swiss 721 BT Bold", 9, FontStyle.Bold), Brushes.Black, new RectangleF(93, 30, 250, 65), new StringFormat(format2));
            //        ev.Graphics.DrawString(rupees, new Font("Swiss 721 BT Bold", 12, FontStyle.Bold), Brushes.Black, new RectangleF(93, 45, 240, 65), new StringFormat(format2));
            //        ev.Graphics.DrawString(rupees, new Font("Swiss 721 BT Bold", 12, FontStyle.Bold), Brushes.Black, new RectangleF(190, 45, 150, 65), new StringFormat(format2));
            //       // ev.Graphics.DrawString("TEXT", new Font("Swiss 721 BT Bold", 8, FontStyle.Bold), Brushes.Black, new RectangleF(270, 45, 20, 35), new StringFormat(format3));
            //    };
            //    pd.Print();
            //    pd.Dispose();

            //}
            return Ok(response);
        }

        [HttpPost]
        [Route("ImageBytes")]
        public async Task<IActionResult> ImageBytes([FromBody] ImageBytesService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}
