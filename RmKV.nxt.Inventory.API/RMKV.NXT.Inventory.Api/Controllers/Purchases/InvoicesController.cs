﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RmKV.NXT.Inventory.DataModel.utilities;
using RMKV.NXT.Inventory.Business.Purchases.Invoices;

namespace RMKV.NXT.Inventory.Api.Controllers.Purchases
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class InvoicesController : ControllerBase
    {
        private IMediator mediator;

        public InvoicesController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("GetSupplierDescription")]
        public async Task<IActionResult> GetSupplierDescription([FromBody] GetSupplierDescService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("CheckPriceCode")]
        public async Task<IActionResult> CheckPriceCode([FromBody] CheckPriceCodeService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("CheckDiscountAmount")]
        public async Task<IActionResult> CheckDiscountAmount([FromBody] InvoiceEntryConfigService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetGstByHSN")]
        public async Task<IActionResult> GetGstByHSN([FromBody] GetGSTByHSNService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetInvoiceNos")]
        public async Task<IActionResult> GetInvoiceNos([FromBody] GetInvoiceNosService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetMasterDiscount")]
        public async Task<IActionResult> GetMasterDiscount([FromBody] GetMasterDiscountService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetInvoicePoNos")]
        public async Task<IActionResult> GetInvoicePoNos([FromBody] GetInvoicePoNosService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddInvoices")]
        public async Task<IActionResult> AddInvoices([FromBody] AddInvoiceService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("FetchInvoices")]
        public async Task<IActionResult> FetchInvoices([FromBody] FetchInvoiceService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetInvoices")]
        public async Task<IActionResult> GetInvoices([FromBody] GetInvoiceService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("ApproveInvoices")]
        public async Task<IActionResult> ApproveInvoices([FromBody] ApproveInvoiceService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("PostInvoices")]
        public async Task<IActionResult> PostInvoices([FromBody] PostInvoiceService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("EditCostPrice")]
        public async Task<IActionResult> EditCostPrice([FromBody] EditCostPriceService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetInvoiceCheckListReport")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetIncentiveSlipReport(GetInvoiceCheckListReportService values)
        {
            DataSet response = await mediator.Send(values);
            try
            {
                string webRootPath = "Reports/Invoice_Entry_Check_List_Report.frx";

                FileStreamResult stream = Utilities.ReturnStreamReport(response, webRootPath);
                return stream;
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}