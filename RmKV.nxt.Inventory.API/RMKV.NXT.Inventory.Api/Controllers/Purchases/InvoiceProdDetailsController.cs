﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.Purchases.InvoiceProdDetails;

namespace RMKV.NXT.Inventory.Api.Controllers.Purchases
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class InvoiceProdDetailsController : ControllerBase
    {
        private IMediator mediator;

        public InvoiceProdDetailsController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("GetInvoiceProdDetails")]
        public async Task<IActionResult> GetInvoiceProdDetails([FromBody] GetInvoiceProdDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("AddInvoiceProdDetails")]
        public async Task<IActionResult> AddInvoiceProdDetails([FromBody] AddInvoiceProdDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetInvoiceProductLookup")]
        public async Task<IActionResult> GetInvoiceProductLookup([FromBody] GetInvoiceProductLookupService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetInvoiceProdAttributes")]
        public async Task<IActionResult> GetInvoiceProdAttributes([FromBody] GetInvoiceProdAttributesService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetProductLookupDetails")]
        public async Task<IActionResult> GetProductLookupDetails([FromBody] GetProductLookupDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetCounterDetails")]
        public async Task<IActionResult> GetCounterDetails([FromBody] GetCounterDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("FetchInvoiceProductDetails")]
        public async Task<IActionResult> FetchInvoiceProductDetails([FromBody] FetchInvoiceProductDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("SectionWiseAttribute")]
        public async Task<IActionResult> SectionWiseAttribute([FromBody] SectionWiseAttributeService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetAttributesValuesDetails")]
        public async Task<IActionResult> GetAttributesValuesDetails([FromBody] GetAttributesValuesDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
        [HttpPost]
        [Route("GetProductDetailsByBrandType")]
        public async Task<IActionResult> GetProductDetailsByBrandType([FromBody] GetProductDetailsByBrandTypeService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}