﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.Purchases.InvoiceRegister;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Api.Controllers.Purchases
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class InvoiceRegisterController : ControllerBase
    {
        private IMediator mediator;

        public InvoiceRegisterController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("GetInvoiceRegister")]
        public async Task<IActionResult> GetInvoiceRegister([FromBody] GetInvoiceRegisterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddInvoiceRegister")]
        public async Task<IActionResult> AddInvoiceInBales([FromBody] AddInvoiceRegisterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("DeleteInvoiceRegister")]
        public async Task<IActionResult> DeleteInvoiceInBales([FromBody] DeleteInvoiceRegisterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("ApproveInvoiceRegister")]
        public async Task<IActionResult> ApproveInvoiceRegister([FromBody] ApproveInvoiceRegisterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }


        [HttpPost]
        [Route("FetchInvoiceRegister")]
        public async Task<IActionResult> FetchInvoiceRegister([FromBody] FetchInvoiceRegisterService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}
