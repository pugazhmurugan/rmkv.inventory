﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMKV.NXT.Inventory.Business.Purchases;

namespace RMKV.NXT.Inventory.Api.Controllers.Purchases
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class InvoiceInBaleController : ControllerBase
    {
        private IMediator mediator;

        public InvoiceInBaleController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost]
        [Route("GetInvoiceInBales")]
        public async Task<IActionResult> GetInvoiceInBales([FromBody] GetInvoiceInBaleService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("AddInvoiceInBales")]
        public async Task<IActionResult> AddInvoiceInBales([FromBody] AddInvoiceInBaleService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("FetchInvoiceInBales")]
        public async Task<IActionResult> FetchInvoiceInBales([FromBody] FetchInvoiceInBaleService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("DeleteInvoiceInBales")]
        public async Task<IActionResult> DeleteInvoiceInBales([FromBody] DeleteInvoiceInBaleService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetSectionGRNNos")]
        public async Task<IActionResult> GetSectionGRNNos([FromBody] GetSectionGRNService values)
        {
            var response = await mediator.Send(values);
            return Ok(response);
        }
    }
}