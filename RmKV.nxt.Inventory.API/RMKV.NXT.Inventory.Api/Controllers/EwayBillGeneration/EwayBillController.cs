﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.Business.EwayBillGeneration;

namespace RMKV.NXT.Inventory.Api.Controllers.EwayBillGenration
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class EwayBillController : ControllerBase
    {
        private IMediator mediator;

        public EwayBillController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [Route("EwayBillLogin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> EwayBillLogin([FromBody] EwayBillLoginService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }

        [HttpPost]
        [Route("EwayBillValidate")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> EwayBillValidateUser([FromBody] EwayBillValidateUserService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
        [HttpPost]
        [Route("EwayBillDetails")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> EwayBillDetails([FromBody] EwayBillDetailsService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
        [HttpPost]
        [Route("GenerateEwayBillandEInvoice")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GenerateEwayBillandEInvoice([FromBody] GenerateEwayBillandEInvoiceService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
        [HttpPost]
        [Route("GenerateEwayBill")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GenerateEwayBill([FromBody] GenerateEwayBillService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
        [HttpPost]
        [Route("CancelEwayBillandEInvoice")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CancelEwayBillandEInvoice([FromBody] CancelEwayBillandEInvoiceService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
        [HttpPost]
        [Route("CancelEwayBill")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CancelEwayBill([FromBody] CancelEwayBillService values)
        {
            var response = await mediator.Send(values);
            return Ok(JsonConvert.SerializeObject(response));
        }
    }
}