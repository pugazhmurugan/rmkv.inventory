using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PartialResponse.Extensions.DependencyInjection;
using RmKV.NXT.Inventory.Api.Shared;
using RMKV.NXT.Inventory.Api.Filters;
using RMKV.NXT.Inventory.Api.MediatR;
using RMKV.NXT.Inventory.Api.Middlewares;
using RMKV.NXT.Inventory.Api.Validators;
using RMKV.NXT.Inventory.Business;
using RMKV.NXT.Inventory.Business.ApiClients.CommonApi;
using RMKV.NXT.Inventory.Business.ApiClients.ControlPanelApi;
using RMKV.NXT.Inventory.Business.ApiClients.GRNApi;
using RMKV.NXT.Inventory.Business.ApiClients.HrmsApi;
using RMKV.NXT.Inventory.Business.ApiClients.LaunchingApi;
using RMKV.NXT.Inventory.Business.Masters.ProductAttributes;
using RMKV.NXT.Inventory.Business.Masters.HSNMaster;
using RMKV.NXT.Inventory.Business.Masters.ProductCategory;
using RMKV.NXT.Inventory.Business.Menus;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Net;
using RMKV.NXT.Inventory.Business.Masters.ProductGroupAttributes;
using RMKV.NXT.Inventory.Business.Masters.ProductGroup;
using RMKV.NXT.Inventory.Business.Masters.ProductMaster;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.CounterToGodown;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers;
using RMKV.NXT.Inventory.Business.PurchaseOrders.PurchaseOrders;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.GodowntoCounterAcknowledgement;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.CountertoGodownAcknowledgement;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfer;
using RMKV.NXT.Inventory.Business.Purchases;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsReceipts.GoodsTransferReceipts;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.WarehouseGatepass;
using RMKV.NXT.Inventory.Business.Purchases.InvoiceRegister;
using RMKV.NXT.Inventory.Business.Purchases.BarCodePrinting;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsReceipts.GoodsTransferBales;
using RMKV.NXT.Inventory.Business.Purchases.Invoices;
using RMKV.NXT.Inventory.Business.GoodsMovement.WarehouseGatePassAcknowledgment;
using RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnEntry;
using RMKV.NXT.Inventory.Business.Purchases.InvoiceProdDetails;
using RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnApprove;
using RMKV.NXT.Inventory.Business.PurchaseReturn;
using RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnDC;
using RMKV.NXT.Inventory.Business.Tools.ProductChange;
using RMKV.NXT.Inventory.Business.Pricing.SellingPriceChange;
using RMKV.NXT.Inventory.Business.Pricing.DoubleRate;
using RMKV.NXT.Inventory.Business.Pricing;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.WorkOrderReceipt;
using RMKV.NXT.Inventory.Business.EwayBillGeneration;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.EwayBillGeneration;
using RMKV.NXT.Inventory.DataContext.SqlRepository.EwayBillGeneration;
using RMKV.NXT.Inventory.Business.PurchaseReturn.ReturnedGoodsNotes;
using RMKV.NXT.Inventory.Business.PurchaseOrders.DetailedPurchaseOrder;
using RMKV.NXT.Inventory.Business.GoodsMovement.TransfersExceptionReports.CounterToGodownException;
using RMKV.NXT.Inventory.Business.GoodsMovement.TransfersExceptionReports.GodownToCounterException;
using RMKV.NXT.Inventory.Business.GoodsMovement.TransfersExceptionReports.GoodsTransferException;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.WorkOrderIssue;
using RMKV.NXT.Inventory.Business.Tools.SupplierDescriptionChange;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.ProductionIssue;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.CustomerWorkOrder;
using RMKV.NXT.Inventory.Business.Masters.ProductionMaterials;
using RMKV.NXT.Inventory.Business.Masters.ProductionProducts;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.PolishOrderIssue;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.PolishOrderReceipt;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableTransferReports;
using RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer;

namespace RMKV.NXT.Inventory.Api
{
    public class Startup
    {

        readonly string CorsOnly = "CorsPolicy";
        readonly string AllowedOrg = "AllowedOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.KnownProxies.Add(IPAddress.Parse("0.0.0.0"));
            });
            services.AddControllers();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //additional filters
            services
             .AddMvc(options =>
             {
                 options.Filters.Add(typeof(ApiClientValidationExceptionAttribute));
                 options.Filters.Add(typeof(FluentValidationExceptionAttribute));
                 options.Filters.Add(typeof(ValidateModelAttribute));
                 options.Filters.Add(typeof(GlobalExceptionFilter));
                 //options.Filters.Add(typeof(ApiIActionFilter));
                 options.OutputFormatters.RemoveType<SystemTextJsonOutputFormatter>();
             }).AddPartialJsonFormatters().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            // Configure MediatR design pattern
            services.AddMediatR(typeof(AssemblyMaker));
            services.AddScoped<IMediator, SynchronousMediator>();

            //Validation token keys
            string jwtPublicKey = Configuration.GetValue<string>("JwtPublicKey");
            string authTokenIssuer = Configuration.GetValue<string>("AuthTokenIssuer");
            services.AddTransient<IJwtTokenValidator>(jwtTokenValidator => new JwtTokenValidator(jwtPublicKey, authTokenIssuer));
            //pgsql database connected
            // services.AddDbContext<RMKVCommonContext>(options => options.UseNpgsql(Configuration["DatabaseConfig:PostgresSQL"]));
            services.AddTransient<IUnitofwork, Unitofwork>(ctx => new Unitofwork(Configuration["DatabaseConfig:PostgresSQL"]));

            //Menu
            services.AddScoped<IValidator<MenusService>, MenusValidations>();

            // Master
            services.AddScoped<IValidator<SaveProductCategoryService>, SaveProductCategoryValidation>();
            services.AddScoped<IValidator<FerchProductCategoryService>, FetchProductCategoryValidation>();
            services.AddScoped<IValidator<LoadProductCategoryService>, GetProductCategoryValidation>();
            services.AddScoped<IValidator<RemoveProductCategoryService>, RemoveProductCategoryValidation>();

            services.AddScoped<IValidator<SaveHSNTaxService>, SaveHSNTaxValidation>();
            services.AddScoped<IValidator<SaveHSNMasterService>, SaveHSNMasterValidation>();
            services.AddScoped<IValidator<FetchHSNMasterService>, FetchHSNMasterValidation>();
            services.AddScoped<IValidator<FetchHSNTaxService>, FetchHSNTaxRateValidation>();
            services.AddScoped<IValidator<FetchHSNTaxIndividualService>, FetchHSNTaxRateIndividualValidation>();
            services.AddScoped<IValidator<RemoveHSNMasterService>, RemoveHSNMasteValidation>();
            services.AddScoped<IValidator<RemoveHSNTaxRateService>, RemoveHSNTaxRateValidation>();
            services.AddScoped<IValidator<GetHSNMasterService>, GetHSNMasterValidation>();

            //shared
            services.AddScoped<IValidator<GetEmployeeLookupServices>, GetEmployeeLookupValidations>();
            services.AddScoped<IValidator<GetSalesLocationServices>, GetSalesLocationValidations>();
            services.AddScoped<IValidator<GetCompanyServices>, GetCompanyServiceValidations>();
            services.AddScoped<IValidator<GetEmployeeLookupServices>, GetEmployeeLookupValidations>();
            services.AddScoped<IValidator<GetAllSalesLocationServices>, GetAllSalesLocationValidations>();
            services.AddScoped<IValidator<GetDashboardService>, GetDashboardValidation>();
            services.AddScoped<IValidator<GetUserProfileService>, GetUserProfileValidation>();
            services.AddScoped<IValidator<GetGRNDocumentRootPathAndUrlServices>, GetGRNDocumentRootPathAndUrlValidations>();
            services.AddScoped<IValidator<CheckLockPasswordService>, CheckLockPasswordValidation>();
            services.AddScoped<IValidator<GetWarehouseService>, GetWarehouseValidation>();
            services.AddScoped<IValidator<GetSectionService>, GetSectionValidation>();
            services.AddScoped<IValidator<GetGroupSectionService>, GetGroupSectionValidation>();
            services.AddScoped<IValidator<GetByNoDetailsService>, GetByNoDetailsValidation>();
            services.AddScoped<IValidator<GetGlobalParamsServices>, GetGlobalParamsValidations>();
            services.AddScoped<IValidator<GetGRNAccountsLookupService>, GetGRNAccountsLookupValidation>();
            services.AddScoped<IValidator<GetCommonSectionService>, GetCommonSectionsValidation>();
            services.AddScoped<IValidator<GetMaterialLookupService>, GetMaterialLookupValidation>();
            services.AddScoped<IValidator<GetMaterialProductLookupService>, GetMaterialProductLookupValidation>();

            //Masters
            services.AddScoped<IValidator<GetProductAttributesService>, GetProductAttributesValidation>();
            services.AddScoped<IValidator<AddProductAttributesService>, AddProductAttributesValidation>();
            services.AddScoped<IValidator<ModifyProductAttributesService>, ModifyProductAttributesValidation>();
            services.AddScoped<IValidator<GetProductAttributeValuesService>, GetProductAttributeValuesValidation>();
            services.AddScoped<IValidator<AddProductAttributeValuesService>, AddProductAttributeValuesValidation>();
            services.AddScoped<IValidator<ModifyProductAttributeValuesService>, ModifyProductAttributeValuesValidation>();
            services.AddScoped<IValidator<AddProductionMaterialsService>, AddProductionMaterialsValidation>();
            services.AddScoped<IValidator<GetProductionMaterialsService>, GetProductionMaterialsValidations>();
            services.AddScoped<IValidator<FetchProductionMaterialsService>, FetchProductionMaterialsValidations>();
            services.AddScoped<IValidator<GetProductionProductLookupService>, GetProductionProductLookupValidation>();

            // Product Group Attributes
            services.AddScoped<IValidator<GetAttributeLookupService>, GetAttributeLookupValidation>();
            services.AddScoped<IValidator<GetProductGroupLookupService>, GetProductGroupLookupValidation>();
            services.AddScoped<IValidator<AddProductGroupAttributesService>, AddProductGroupAttributesValidation>();
            services.AddScoped<IValidator<RemoveProductGroupAttributesService>, RemoveProductGroupAttributesValidation>();
            services.AddScoped<IValidator<FetchProductGroupAttributeService>, FetchProductGroupAttributesValidation>();
            services.AddScoped<IValidator<GetProductGroupAttributeListService>, GetProductGroupAttributesValidation>();

            //Product Groups
            services.AddScoped<IValidator<GetProductGroupService>, GetProductGroupValidation>();
            services.AddScoped<IValidator<AddProductGroupService>, AddProductGroupValidation>();
            services.AddScoped<IValidator<FetchProductGroupService>, FetchProductGroupValidation>();
            services.AddScoped<IValidator<DeleteProductGroupService>, DeleteProductGroupValidation>();

            //Product Master
            services.AddScoped<IValidator<GetProductMasterService>, GetProductMasterValidation>();
            services.AddScoped<IValidator<AddProductMasterService>, AddProductMasterValidation>();
            services.AddScoped<IValidator<DeleteProductMasterService>, DeleteProductMasterValidation>();
            services.AddScoped<IValidator<FetchProductMasterService>, FetchProductMasterValidation>();
            services.AddScoped<IValidator<GetProductCounterService>, GetProductCounterValidation>();
            services.AddScoped<IValidator<GetProductAttributesServices>, GetProductAttributesValidations>();
            services.AddScoped<IValidator<addGoodsService>, addGoodsValidation>();
            services.AddScoped<IValidator<GetToLocationsService>, GetToLocationsValidation>();
            services.AddScoped<IValidator<GetGoodsService>, GetGoodsValidations>();
            services.AddScoped<IValidator<GetTransferNoService>, GetTransferNoValidations>();
            services.AddScoped<IValidator<FetchGoodsService>, FetchGoodsValidations>();

            //CounterToGodown
            services.AddScoped<IValidator<AddCounterToGodownService>, AddCounterToGodownValidation>();
            services.AddScoped<IValidator<ApproveCounterToGodownService>, ApproveCounterToGodownValidation>();
            services.AddScoped<IValidator<CancelCounterToGodownService>, CancelCounterToGodownValidation>();
            services.AddScoped<IValidator<GetToWarehouseService>, GetToWarehouseValidation>();
            services.AddScoped<IValidator<GetCounterToGodownService>, GetCounterToGodownValidations>();
            services.AddScoped<IValidator<GetCounterTransferNoService>, GetCounterTransferNoValidations>();
            services.AddScoped<IValidator<FetchCounterToGodownService>, FetchCounterToGodownValidations>();

            services.AddScoped<IValidator<GetCounterToGodownReportService>, GetCounterToGodownReportValidations>();
            services.AddScoped<IValidator<CheckCounterToGodownReportService>, CheckCounterToGodownReportValidations>();

            services.AddScoped<IValidator<CounterToGodownExceptionService>, CounterToGodownExceptionValidation>();

            //GodownToCounter
            services.AddScoped<IValidator<AddGodownToCounterService>, AddGodownToCounterValidation>();
            services.AddScoped<IValidator<ApproveGodownToCounterService>, ApproveGodownToCounterValidation>();
            services.AddScoped<IValidator<CancelGodownToCounterService>, CancelGodownToCounterValidation>();
            services.AddScoped<IValidator<GetGodownToLocationsService>, GetGodownToLocationsValidation>();
            services.AddScoped<IValidator<GetGodownToCounterService>, GetGodownToCounterValidations>();
            services.AddScoped<IValidator<GetGodownTransferNoService>, GetGodownTransferNoValidations>();
            services.AddScoped<IValidator<FetchGodownToCounterService>, FetchGodownToCounterValidations>();
            services.AddScoped<IValidator<GetGodownToCounterReportServices>, GetGodownToCounterReportValidations>();
            services.AddScoped<IValidator<CheckGodownToCounterReportServices>, CheckGodownToCounterReportValidations>();

            // PO Analysis
            services.AddScoped<IValidator<GetPOAnalysisService>, GetPOAnalysisValidation>();

            // Product Orders
            services.AddScoped<IValidator<GetSupplierCityDetailsService>, GetSupplierCityDetailsValidation>();
            services.AddScoped<IValidator<GetSupplierGroupDetailsService>, GetSupplierGroupDetailsValidation>();
            services.AddScoped<IValidator<GetSupplierByGroupIdService>, GetSupplierByGroupIdValidation>();
            services.AddScoped<IValidator<GetModeofDispatchMasterService>, GetModeOfDispatchMasterValidation>();
            services.AddScoped<IValidator<GetUOMDetailsService>, GetUOMDetailsValidation>();
            services.AddScoped<IValidator<GetPurchaseOrderListService>, GetPurchaseOrderValidation>();
            services.AddScoped<IValidator<CancelPurchaseOrderListService>, CancelPurchaseOrderValidation>();
            services.AddScoped<IValidator<SavePurchaseOrderListService>, SavePurchaseOrderValidation>();
            services.AddScoped<IValidator<FetchPurchaseOrderListService>, FetchPurchaseOrderValidation>();
            services.AddScoped<IValidator<GetDispatchNameListService>, GetDispatchNameListValidation>();
            services.AddScoped<IValidator<GetPurchaseOrderSizeNameService>, GetPOSizeNameValidation>();
            services.AddScoped<IValidator<GetPONoService>, GetPONoValidation>();
            services.AddScoped<IValidator<GetPurchaseOrderEmailService>, GetEmailPurchaseOrderValidation>();

            //Godown to Counter Acknowledgement
            services.AddScoped<IValidator<LoadGtoCACKService>, LoadAcknowledgementValidation>();
            services.AddScoped<IValidator<LoadTransferNoBasedDataService>, LoadTransferNoBasedDataValidation>();
            services.AddScoped<IValidator<AddGtoCAcknowledgementService>, AddGtoCAcknowledgementValidation>();
            services.AddScoped<IValidator<FetchGtoCAcknowledgementService>, FetchGtoCAcknowledgementValidation>();
            services.AddScoped<IValidator<CheckGtoCAckTransferNoService>, CheckGtoCAckTransferValidation>();
            services.AddScoped<IValidator<GodownToCounterExceptionService>, GodownToCounterExceptionValidation>();

            //Counter to Godown Acknowledgement
            services.AddScoped<IValidator<LoadCtoGService>, LoadCtoGAcknowledgementValidation>();
            services.AddScoped<IValidator<LoadCtoGTransferNoBasedDataService>, LoadCtoGTransferNoBasedDataValidation>();
            services.AddScoped<IValidator<AddCtoGAcknowledgementService>, AddCtoGAcknowledgementValidation>();
            services.AddScoped<IValidator<FetchCtoGAcknowledgementService>, FetchCtoGAcknowledgementValidation>();
            services.AddScoped<IValidator<CheckCtoGAckTransferNoService>, CheckCtoGAckTransferValidation>();

            //GoodsTransfer
            services.AddScoped<IValidator<GetWithincityVirtualToStoreService>, GetWithincityVirtualToStoreValidation>();
            services.AddScoped<IValidator<GetWithinStateToLocationService>, GetWithinStateToLocationValidation>();
            services.AddScoped<IValidator<GetOtherStateToLocationService>, GetOtherStateToLocationValidation>();
            services.AddScoped<IValidator<GetWithinStateVirtualStoreToStoreService>, GetWithinStateVirtualStoreToStoreValidation>();
            services.AddScoped<IValidator<GoodsTransferReportService>, GoodsTransferReportValidation>();
            services.AddScoped<IValidator<CheckGoodsTransferReportService>, CheckGoodsTransferReportValidation>();
            services.AddScoped<IValidator<GoodsTransferExceptionReportService>, GoodsTransferExceptionReportValidation>();



            //GoodsTransfer(WithInCity)
            services.AddScoped<IValidator<GetTransferNoWithincityService>, GetTransferNoWithincityValidation>();
            services.AddScoped<IValidator<AddWithincityService>, AddWithincityValidation>();
            services.AddScoped<IValidator<CancelWithincityService>, CancelWithincityValidation>();
            services.AddScoped<IValidator<FetchWithincityService>, FetchWithincityValidation>();
            services.AddScoped<IValidator<GetWithincityService>, GetWithincityValidation>();
            services.AddScoped<IValidator<ApproveWithincityService>, ApproveWithincityValidation>();
            services.AddScoped<IValidator<GetGoodsTransferTypeService>, GetGoodsTransferTypeValidation>();
            services.AddScoped<IValidator<GoodsTransferFromStoreService>, GoodsTransferFromStoreValidation>();

            //Invoice In Bales
            services.AddScoped<IValidator<GetInvoiceInBaleService>, GetInvoiceInBaleValidation>();
            services.AddScoped<IValidator<AddInvoiceInBaleService>, AddInvoiceInBaleValidation>();
            services.AddScoped<IValidator<DeleteInvoiceInBaleService>, DeleteInvoiceInBaleValidation>();
            services.AddScoped<IValidator<GetSectionGRNService>, GetSectionGRNValidation>();
            services.AddScoped<IValidator<FetchInvoiceInBaleService>, FetchInvoiceInBaleValidation>();

            //Invoice Register
            services.AddScoped<IValidator<GetInvoiceRegisterService>, GetInvoiceRegisterValidation>();
            services.AddScoped<IValidator<AddInvoiceRegisterService>, AddInvoiceRegisterValidation>();
            services.AddScoped<IValidator<FetchInvoiceRegisterService>, FetchInvoiceRegisterValidation>();
            services.AddScoped<IValidator<DeleteInvoiceRegisterService>, DeleteInvoiceRegisterValidation>();
            services.AddScoped<IValidator<ApproveInvoiceRegisterService>, ApproveInvoiceRegisterValidation>();

            //Goods Transfer Receipts
            services.AddScoped<IValidator<LoadGoodsTransferReceiptsService>, LoadGoodsTransferReceiptsValidation>();
            services.AddScoped<IValidator<LoadGoodsReceiptsTransferNoService>, LoadGoodsReceiptsTransferNoValidation>();
            services.AddScoped<IValidator<AddGoodsTransferReceiptsService>, AddGoodsTransferReceiptsValidation>();
            services.AddScoped<IValidator<FetchGoodsTransferReceiptsService>, FetchGoodsTransferReceiptsValidation>();
            services.AddScoped<IValidator<CheckReceiptsAckTransferNoService>, CheckGoodsReceiptsTransferNoValidation>();
            services.AddScoped<IValidator<LoadGoodsReceiptsProductDetailsService>, LoadGoodsReceiptsProductDetailsValidation>();
            services.AddScoped<IValidator<GetAllWarehouseListService>, GetAllWarehouseListValidation>();

            //Warehouse Gatepass
            services.AddScoped<IValidator<SaveWarehouseGatePassServices>, SaveWarehouseGatePassValidation>();
            services.AddScoped<IValidator<LoadWarehouseGatePassServices>, LoadWarehouseGatePassValidation>();
            services.AddScoped<IValidator<GetGatePassService>, GetGatePassValidation>();
            services.AddScoped<IValidator<GetSameWarehouseService>, GetSameWarehouseValidation>();
            services.AddScoped<IValidator<GetStoreLocationService>, GetStoreLocationValidation>();
            services.AddScoped<IValidator<GetDesignationService>, GetDesignationValidation>();
            services.AddScoped<IValidator<GetTransferTypeService>, GetTransferTypeValidation>();
            services.AddScoped<IValidator<GetWarehouseGatePassNoDetailsService>, GetWarehouseGatePassNoListValidation>();
            services.AddScoped<IValidator<GetWarehouseGRNNoListService>, GetWarehouseGRNNoListValidation>();

            //BarCodePrinting
            services.AddScoped<IValidator<BarCodePrintingService>, GetBarCodePrintingValidation>();
            services.AddScoped<IValidator<GenerateBarcodePrintService>, GenerateBarcodePrintValidation>();
            services.AddScoped<IValidator<SelectBarCodePrintingService>, SelectBarCodePrintingValidation>();
            services.AddScoped<IValidator<DoublerateBarCodePrintingService>, DoubleRateBarCodePrintingValidation>();
            services.AddScoped<IValidator<ImageBytesService>, ImageBytesValidation>();


            //Goods Transfer Bales 
            services.AddScoped<IValidator<LoadGoodsTransferBalesService>, LoadGoodsTransferBalesValidation>();
            services.AddScoped<IValidator<AddGoodsTransferBalesService>, AddGoodsTransferBalesValidation>();
            services.AddScoped<IValidator<GetGoodsBalesSectionGRNService>, GetBalesSectionGRNValidation>();


            //Invoices
            services.AddScoped<IValidator<CheckPriceCodeService>, CheckPriceCodeValidation>();
            services.AddScoped<IValidator<GetSupplierDescService>, GetSupplierDescValidation>();
            services.AddScoped<IValidator<InvoiceEntryConfigService>, InvoiceEntryConfigValidation>();
            services.AddScoped<IValidator<GetGSTByHSNService>, GetGSTByHSNValidation>();
            services.AddScoped<IValidator<GetInvoiceNosService>, GetInvoiceNosValidation>();
            services.AddScoped<IValidator<GetMasterDiscountService>, GetMasterDiscountValidation>();
            services.AddScoped<IValidator<GetInvoicePoNosService>, GetInvoicePoNosValidation>();
            services.AddScoped<IValidator<AddInvoiceService>, AddInvoiceValidation>();
            services.AddScoped<IValidator<FetchInvoiceService>, FetchInvoiceValidation>();
            services.AddScoped<IValidator<GetInvoiceService>, GetInvoiceValidation>();
            services.AddScoped<IValidator<ApproveInvoiceService>, ApproveInvoiceValidation>();
            services.AddScoped<IValidator<PostInvoiceService>, PostInvoiceValidation>();
            services.AddScoped<IValidator<EditCostPriceService>, EditCostPriceValidation>();

            //Invoices Prod
            services.AddScoped<IValidator<GetInvoiceProdDetailsService>, GetInvoiceProdDetailsValidation>();
            services.AddScoped<IValidator<AddInvoiceProdDetailsService>, AddInvoiceProdDetailsValidation>();
            services.AddScoped<IValidator<GetInvoiceProductLookupService>, GetInvoiceProductLookupValidation>();
            services.AddScoped<IValidator<GetInvoiceProdAttributesService>, GetInvoiceProdAttributesValidation>();
            services.AddScoped<IValidator<GetProductLookupDetailsService>, GetProductLookupDetailsValidation>();
            services.AddScoped<IValidator<GetCounterDetailsService>, GetCounterDetailsValidation>();
            services.AddScoped<IValidator<FetchInvoiceProductDetailsService>, FetchInvoiceProductDetailsValidation>();
            services.AddScoped<IValidator<SectionWiseAttributeService>, SectionWiseAttributeValidation>();
            services.AddScoped<IValidator<GetAttributesValuesDetailsService>, GetAttributesValuesDetailsValidation>();
            services.AddScoped<IValidator<GetProductDetailsByBrandTypeService>, GetProductDetailsByBrandTypeValidation>();

            ////Warehouse GatePass Acknowledgement
            //services.AddScoped<IValidator<GetSameWarehouseService>, GetSameWarehouseValidation>();
            //services.AddScoped<IValidator<GetStoreLocationService>, GetStoreLocationValidation>();
            //services.AddScoped<IValidator<GetWarehouseBasedCompanySectionService>, GetCompanySectionValidation>();
            ////services.AddScoped<IValidator<GetGatePassTypeService>, GetGatePassTypeValidation>();

            //PurchaseReturnEntry
            services.AddScoped<IValidator<AddPurchaseReturnEntryService>, AddPurchaseReturnEntryValidation>();
            services.AddScoped<IValidator<CancelPurchaseReturnEntryService>, CancelPurchaseReturnEntryValidation>();
            services.AddScoped<IValidator<FetchPurchaseReturnEntryService>, FetchPurchaseReturnEntryValidation>();
            services.AddScoped<IValidator<GetPurchaseReturnEntryService>, GetPurchaseReturnEntryValidation>();
            services.AddScoped<IValidator<ApprovePurchaseReturnEntryService>, ApprovePurchaseReturnEntryValidation>();

            //PurchaseReturnApprove
            services.AddScoped<IValidator<GetApprovedSupplierService>, GetApprovedSupplierValidation>();
            services.AddScoped<IValidator<GetPurchaseRetunApprovedListService>, GetPurchaseRetunApprovedValidation>();
            services.AddScoped<IValidator<RemovPurchaseReturnService>, RemovPurchaseReturnValidation>();
            services.AddScoped<IValidator<GetSupplierCityForApproveService>, GetSupplierCityForApproveValidation>();
            services.AddScoped<IValidator<GetSupplierGroupNameForApproveService>, GetSupplierGroupNameForApproveValidation>();
            services.AddScoped<IValidator<GetSupplierNameForApproveService>, GetSupplierNameForApproveValidation>();
            services.AddScoped<IValidator<GeSupplierDetailsForApproveService>, GeSupplierDetailsForApproveValidation>();
            services.AddScoped<IValidator<ApprovePurchaseReturnListService>, ApprovePurchaseReturnListValidation>();


            //PurchaseReturnNote
            services.AddScoped<IValidator<GetPurchaseReturnNoteService>, GetPurchaseReturnNoteValidation>();
            services.AddScoped<IValidator<GetPurchaseReturnByNoService>, GetPurchaseReturnByNoValidation>();
            services.AddScoped<IValidator<GetPurchaseReturnByNoDetailsService>, GetPurchaseReturnByNoDetailsValidation>();
            services.AddScoped<IValidator<AddPurchaseReturnNoteService>, AddPurchaseReturnNoteValidation>();
            services.AddScoped<IValidator<FetchPurchaseReturnNoteService>, FetchPurchaseReturnNoteValidation>();

            //PurchaseReturnDC
            services.AddScoped<IValidator<LoadPurchaseReturnDCService>, LoadPurchaseReturnValidation>();
            services.AddScoped<IValidator<GeneratePurchaseReturnDCNoService>, GeneratePurchaseReturnDCNoValidation>();

            //RGN
            services.AddScoped<IValidator<GetRGNWiseDCDetailsService>, GetRGNWiseDCDetailsValidation>();
            services.AddScoped<IValidator<AddRGNWiseDetailsService>, AddRGNWiseDetailsValidation>();
            services.AddScoped<IValidator<GetRGNWiseDetailsService>, GetRGNWiseDetailsValidation>();
            services.AddScoped<IValidator<GetDCDetailsService>, GetDCDetailsValidation>();

            //Change
            services.AddScoped<IValidator<GetProductChangeService>, GetProductChangeValidation>();
            services.AddScoped<IValidator<GetCounterChangeService>, GetCounterChangeValidation>();
            services.AddScoped<IValidator<GetStyleChangeService>, GetStyleChangeValidation>();
            //services.AddScoped<IValidator<AddPurchaseReturnNoteService>, AddPurchaseReturnNoteValidation>();
            //services.AddScoped<IValidator<FetchPurchaseReturnNoteService>, FetchPurchaseReturnNoteValidation>();

            //Tools
            services.AddScoped<IValidator<counterchangesaveService>, counterchangesaveValidation>();
            services.AddScoped<IValidator<productchangesaveService>, productchangesaveValidation>();
            services.AddScoped<IValidator<stylecodechangesaveService>, stylecodechangesaveValidation>();
            services.AddScoped<IValidator<GetCounterChangeHistoryReportServices>, GetCounterChangeHistoryReportValidations>();
            services.AddScoped<IValidator<GetProductChangeHistoryReportServices>, GetProductChangeHistoryReportValidations>();
            services.AddScoped<IValidator<FetchPurchaseReturnDCDetailsService>, FetchPurchaseReturnDCDetailsValidation>();



            //Pricing

            //Selling Price Change

            //Single Product Price Change
            services.AddScoped<IValidator<GetSingleProductService>, GetSingleProductValidation>();
            services.AddScoped<IValidator<AddSingleProductService>, AddSingleProductValidation>();

            //Double Rate
            //By Amount
            services.AddScoped<IValidator<AddByAmountService>, AddByAmountValidation>();
            services.AddScoped<IValidator<AddByPercentageService>, AddByPercentageValidation>();
            services.AddScoped<IValidator<CheckMarginService>, CheckMarginValidation>();
            services.AddScoped<IValidator<GetMarginCalculateService>, GetMarginCalculateValidation>();

            //Customer WorkOrder
            services.AddScoped<IValidator<AddCustomerWorkOrderService>, AddCustomerWorkOrderValidation>();
            services.AddScoped<IValidator<CancelCustomerWorkOrderService>, CancelCustomerWorkOrderValidation>();
            services.AddScoped<IValidator<FetchCustomerWorkOrderService>, FetchCustomerWorkOrderValidations>();
            services.AddScoped<IValidator<GetCustomerWorkOrderService>, GetCustomerWorkOrderValidations>();

            //WorkOrderReceiptList
            services.AddScoped<IValidator<GetWorkOrderReceiptListServices>, WorkOrderReceiptValidation>();
            services.AddScoped<IValidator<GetWorkOrderReceiptListPendingServices>, WorkOrderReceiptListPendingValidation>();
            services.AddScoped<IValidator<AddWorkOrderReceiptListPendingServices>, AddWorkOrderReceiptListValidation>();
            services.AddScoped<IValidator<FetchWorkOrderReceiptServices>, FetchWorkOrderReceiptValidation>();

            //WorkorderIssue
            services.AddScoped<IValidator<FetchWorkOrderIssueService>, FetchWorkOrderIssueValidation>();
            services.AddScoped<IValidator<GetWorkOrderIssueDetailsListService>, GetWorkOrderIssueDetailsListValidation>();
            services.AddScoped<IValidator<AddWorkOrderIssueService>, AddWorkOrderIssueValidation>();
            services.AddScoped<IValidator<GetWorkorderIssueTransferTypeService>, GetWorkorderIssueTransferTypeValidation>();

            //MRP Change
            services.AddScoped<IValidator<AddMRPChangeService>, AddMRPChangeValidation>();
            services.AddScoped<IValidator<AddMRPChangeService>, AddMRPChangeValidation>();

            //Detailed Purchase Order 
            services.AddScoped<IValidator<SaveDetailedPODetailsService>, SaveDetailedPOValidation>();
            services.AddScoped<IValidator<CancelDetailedPODetailsService>, CancelDetailedPOValidation>();
            services.AddScoped<IValidator<FetchDetailedPODetailsService>, FetchDetailedPOValidation>();
            services.AddScoped<IValidator<LoadDetailedPODetailsService>, GetDetailedPOValidation>();
            services.AddScoped<IValidator<GetBrandListForDetailedPOService>, GetBrandListDetailedPOValidation>();
            services.AddScoped<IValidator<GetProductGroupListForDetailedPOService>, GetProductGroupDetailedPOValidation>();
            services.AddScoped<IValidator<GetProductListForDetailedPOService>, GetProductDetailedPOValidation>();
            services.AddScoped<IValidator<GetColorListForDetailedPOService>, GetColorDetailedPOValidation>();
            services.AddScoped<IValidator<GetQualityListForDetailedPOService>, GetQualityDetailedPOValidation>();
            services.AddScoped<IValidator<GetSizeForDetailedPOService>, GetSizeDetailedPOValidation>();
            services.AddScoped<IValidator<GetLocationsForDetailedPOService>, GetLocationsDetailedPOValidation>();
            services.AddScoped<IValidator<GetDesignCodeListForDetailedPOService>, GetDesignCodeDetailedPOValidation>();
            services.AddScoped<IValidator<GetDetailedPONoService>, GetDetailedPONoValidation>();
            services.AddScoped<IValidator<GetDetailedPurchaseOrderEmailService>, GetDetailedEmailPurchaseOrderValidation>();

            //Production Issue
            services.AddScoped<IValidator<AddProductionIssueService>, AddProductionIssueValidation>();
            services.AddScoped<IValidator<CancelProductionIssueService>, CancelProductionIssueValidation>();
            services.AddScoped<IValidator<FetchProductionIssueService>, FetchProductionIssueValidations>();
            services.AddScoped<IValidator<GetProductionIssueService>, GetProductionIssueValidations>();
            services.AddScoped<IValidator<GetProductionIssueReconciliationService>, GetProductionIssueReconciliationValidations>();
            services.AddScoped<IValidator<GetProductionIssueEstimatedVsReceivedService>, GetProductionIssueEstimatedVsReceivedValidations>();
            services.AddScoped<IValidator<AddProductionIssueReconciliationService>, AddProductionIssueReconciliationValidation>();

            //Polish Order Receipts
            services.AddScoped<IValidator<GetPolishOrderReceiptService>, GetPolishOrderReceiptValidation>();
            services.AddScoped<IValidator<GetPendingPolishIssueNoService>, GetPendingPolishIssueNoValidation>();
            services.AddScoped<IValidator<GetPendingPolishIssueDetailService>, GetPendingPolishIssueDetailValidation>();
            services.AddScoped<IValidator<AddPolishOrderReceiptService>, AddPolishOrderReceiptValidation>();
            services.AddScoped<IValidator<RemovePolishOrderReceiptService>, RemovePolishOrderReceiptValidation>();
            services.AddScoped<IValidator<FetchPolishOrderReceiptDetailService>, FetchPolishOrderReceiptValidation>();


            ////ProductChangeDescription
            services.AddScoped<IValidator<AddSupplierDescriptionService>, AddSupplierDescriptionValidaions>();

            // PolishOrderIssue
            services.AddScoped<IValidator<GetPolishOrderIssueService>, GetPolishOrderIssueValidation>();
            services.AddScoped<IValidator<AddPolishOrderIssueService>, AddPolishOrderIssueValidation>();
            services.AddScoped<IValidator<FetchPolishOrderIssueService>, FetchPolishOrderIssueValidation>();
            services.AddScoped<IValidator<DeletePolishOrderIssueService>, DeletePolishOrderIssueValidation>();

            ///

            //ProductionProduct
            services.AddScoped<IValidator<ProductionProductService>, ProductionProductValidation>();
            services.AddScoped<IValidator<GetProductionProductService>, GetProductionProductValidation>();
            services.AddScoped<IValidator<FetchProductionProductService>, FetchProductionProductValidation>();


            services.AddScoped<IValidator<GetPolishPendingReportService>, GetPolishPendingReportValidations>();
            //Production Receipt
            services.AddScoped<IValidator<GetProductionReceiptService>, GetProductionReceiptValidation>();
            services.AddScoped<IValidator<GetProductionIssueNoService>, GetProductionIssueNoValidation>();
            services.AddScoped<IValidator<GetProductionIssueDetailService>, GetProductionIssueDetailValidation>();
            services.AddScoped<IValidator<GetProductionReceiptProductLookupService>, GetProductionReceiptProductLookupValidation>();
            services.AddScoped<IValidator<AddProductionReceiptService>, AddProductionReceiptValidation>();
            services.AddScoped<IValidator<FetchProductionReceiptService>, FetchProductionReceiptValidation>();
            services.AddScoped<IValidator<RemoveProductionReceiptService>, RemoveProductionReceiptValidation>();

            //EwayBill Generation
            services.AddScoped<IEwayBillRepository, EwayBillRepository>(sp =>
               new EwayBillRepository(
                   sp.GetService<IHttpContextAccessor>()));



            services.AddScoped<IValidator<FetchPurchaseReturnDCDetailsService>, FetchPurchaseReturnDCDetailsValidation>();


            //ControlPanelApi
            services.AddScoped<IControlPanelApiClients, ControlPanelApiClients>(sp =>
              new ControlPanelApiClients(
                  new Uri(Configuration.GetValue<string>("ContolPanelApi:BaseAddress")),
                  sp.GetService<IHttpContextAccessor>()));

            //LaunchingApi
            services.AddScoped<ILauncherApiClients, LauncherApiClients>(sp =>
               new LauncherApiClients(
                   new Uri(Configuration.GetValue<string>("LaunchingApi:BaseAddress")),
                   sp.GetService<IHttpContextAccessor>()));

            ////HRMSApi
            services.AddScoped<IHrmsApiClients, HrmsApiClients>(sp =>
                       new HrmsApiClients(
                           new Uri(Configuration.GetValue<string>("HRMSApi:BaseAddress")),
                           sp.GetService<IHttpContextAccessor>()));

            ////CommonApi
            services.AddScoped<ICommonApiClients, CommonApiClients>(sp =>
                       new CommonApiClients(
                           new Uri(Configuration.GetValue<string>("CommonApi:BaseAddress")),
                           sp.GetService<IHttpContextAccessor>()));

            ////GRNApi
            services.AddScoped<IGRNApiClients, GRNApiClients>(sp =>
                       new GRNApiClients(
                           new Uri(Configuration.GetValue<string>("GRNApi:BaseAddress")),
                           sp.GetService<IHttpContextAccessor>()));


            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                //c.DescribeAllEnumsAsStrings();
                //c.DescribeStringEnumsInCamelCase();
                c.DescribeAllParametersInCamelCase();

                c.SwaggerDoc("v1", new OpenApiInfo { Title = "RMKV NXT Inventory API", Version = "v1" });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "Please enter into field the word 'Bearer' following by space and JWT",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                    {
                    {
                        new OpenApiSecurityScheme
                        {
                        Reference = new OpenApiReference
                            {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                            },
                            //Scheme = "oauth2",
                            Scheme = "ApiKeys",
                            Name = "Bearer",
                            In = ParameterLocation.Header,

                        },
                        new List<string>()
                        }
                    });

            });
            services.AddCors(options =>
            {
                options.AddPolicy(name: CorsOnly,
                 builder =>
                 {
                     builder.WithOrigins(Configuration.GetSection(AllowedOrg)
                                          .Get<string[]>())
                                         .AllowAnyHeader()
                                         .AllowAnyMethod().AllowCredentials();
                 });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMiddleware<AuthorizationMiddleware>();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "RMKV NXT Inventory V1");
            });
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(CorsOnly);
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
