﻿using MediatR;
using RmKV.NXT.Inventory.DataModel.Model.Shared;
using RmKV.NXT.Inventory.DataModel.Model.Shared.Company;
using RmKV.NXT.Inventory.DataModel.Model.Shared.HRDocumentRootPathAndUrl;
using RmKV.NXT.Inventory.DataModel.Model.Shared.SalesLocation;
using RmKV.NXT.Inventory.DataModel.Model.User;
using RmKV.NXT.Inventory.DataModel.Models.Shared;
using RMKV.NXT.Inventory.Business.ApiClients.CommonApi;
using RMKV.NXT.Inventory.Business.ApiClients.HrmsApi;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model.Shared;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RmKV.NXT.Inventory.Api.Shared
{
    public class SharedHandler : IRequestHandler<GetEmployeeLookupServices, List<GetEmployeeLookupEntity>>
    {
        private readonly IHrmsApiClients hrmsApi;

        public SharedHandler(IHrmsApiClients hrmsApi)
        {
            this.hrmsApi = hrmsApi;
        }
       
        public class GetGRNDocumentRootPathAndUrlHandler : IRequestHandler<GetGRNDocumentRootPathAndUrlServices, GetGRNDocumentRootPathAndUrlEntity>
        {
            private readonly ICommonApiClients lcommonApi;

            public GetGRNDocumentRootPathAndUrlHandler(ICommonApiClients lcommonApi)
            {
                this.lcommonApi = lcommonApi;
            }

            public async Task<GetGRNDocumentRootPathAndUrlEntity> Handle(GetGRNDocumentRootPathAndUrlServices request, CancellationToken cancellationToken)
            {
                var result = await lcommonApi.GetGRNDocumentRootPathAndUrl(request.SalesLocationId);
                return result;
            }
        }
     
        public async Task<List<GetEmployeeLookupEntity>> Handle(GetEmployeeLookupServices request, CancellationToken cancellationToken)
        {
            var data = new GetEmployeeLookupParams()
            {
                Sales_Location_Id = request.Sales_Location_Id,
                For = request.For,
                Resigned = request.Resigned
            };
            var result = await hrmsApi.GetEmployeeLookup(data);
            return result;
        }

        public class GetUserProfileHandler : IRequestHandler<GetUserProfileService, string>
        {
            private readonly ICommonApiClients lcommonApi;

            public GetUserProfileHandler(ICommonApiClients lcommonApi)
            {
                this.lcommonApi = lcommonApi;
            }

            public async Task<string> Handle(GetUserProfileService request, CancellationToken cancellationToken)
            {
                var result = await lcommonApi.GetUserProfile(request.UserProfile);
                return result;
            }
        }

    }

    public class CheckLockPasswordHandler : IRequestHandler<CheckLockPasswordService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CheckLockPasswordHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(CheckLockPasswordService request, CancellationToken cancellationToken)
        {
            var data = new UserDTO()
            {
                User_Name = request.User_Name,
                Password = request.Password
            };
            var user = await unitOfWork.SharedRepository.CheckLockPassword(data);
            if (user.Password != 0 && user.Password == unitOfWork.SharedRepository.EncryptKey(request.Password))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class GetSalesLocationHandler : IRequestHandler<GetSalesLocationServices, List<GetSalesLocationEntity>>
    {
        private readonly ICommonApiClients lcommonApi;

        public GetSalesLocationHandler(ICommonApiClients lcommonApi)
        {
            this.lcommonApi = lcommonApi;
        }

        public async Task<List<GetSalesLocationEntity>> Handle(GetSalesLocationServices request, CancellationToken cancellationToken)
        {
            var data = new SalesLocationParams()
            {
                Sales_Location_Id = request.Sales_Location_Id,
                User_Id = request.User_Id
            };
            var result = await lcommonApi.GetSalesLocation(data);
            return result;
        }
    }
    public class GetAllSalesLocationHandler : IRequestHandler<GetAllSalesLocationServices, List<GetSalesLocationEntity>>
    {
        private readonly ICommonApiClients lcommonApi;

        public GetAllSalesLocationHandler(ICommonApiClients lcommonApi)
        {
            this.lcommonApi = lcommonApi;
        }

        public async Task<List<GetSalesLocationEntity>> Handle(GetAllSalesLocationServices request, CancellationToken cancellationToken)
        {
           
            var result = await lcommonApi.GetAllSalesLocation(request.SalesLocation);
            return result;
        }
    }

    public class GetDashboardHandler : IRequestHandler<GetDashboardService, DashboardTable>
    {
        private readonly IUnitofwork unitOfWork;
        public GetDashboardHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<DashboardTable> Handle(GetDashboardService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.SharedRepository.GetDashboardDetails(request.Warehouse_Id);
            return result;
        }
    }

    public class GetWarehouseHandler : IRequestHandler<GetWarehouseService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetWarehouseHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetWarehouseService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.SharedRepository.GetWarehouse(request.Warehouse);
            return result;
        }
    }
    public class GetSectionHandler : IRequestHandler<GetSectionService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetSectionHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetSectionService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.SharedRepository.GetSection(request.Section);
            return result;
        }
    }
    public class GetGroupSectionHandler : IRequestHandler<GetGroupSectionService, List<GetGroupSectionsEntity>>
    {
        private readonly ICommonApiClients lcommonApi;
        public GetGroupSectionHandler(ICommonApiClients lcommonApi)
        {
            this.lcommonApi = lcommonApi;
        }
        public async Task<List<GetGroupSectionsEntity>> Handle(GetGroupSectionService request, CancellationToken cancellationToken)
        {
            var result = await lcommonApi.GetGroupSections();
            return result;
        }
    }
    //public class GetByNoDetailsHandler : IRequestHandler<GetByNoDetailsService, BynoDetails>
    //{
    //    private readonly ICommonApiClients lcommonApi;
    //    public GetByNoDetailsHandler(ICommonApiClients lcommonApi)
    //    {
    //        this.lcommonApi = lcommonApi;
    //    }
    //    public async Task<BynoDetails> Handle(GetByNoDetailsService request, CancellationToken cancellationToken)
    //    {
    //        var result = await lcommonApi.GetByNoDetails(request.ByNoData);
    //        return result;
    //    }
    //}

    public class GetByNoDetailsHandler : IRequestHandler<GetByNoDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetByNoDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string>  Handle(GetByNoDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.SharedRepository.GetByNoDetails(request.ByNoData);
            return result;
        }
    }


    public class GetGlobalParamsHandler : IRequestHandler<GetGlobalParamsServices, string>
    {
        private readonly ICommonApiClients lcommonApi;
        public GetGlobalParamsHandler(ICommonApiClients lcommonApi)
        {
            this.lcommonApi = lcommonApi;
        }
        public async Task<string> Handle(GetGlobalParamsServices request, CancellationToken cancellationToken)
        {
            var result = await lcommonApi.GetGlobalParams(request.globalParams);
            return result;
        }
    }
    public class GetGRNAccountsLookupHandler : IRequestHandler<GetGRNAccountsLookupService, string>
    {
        private readonly ICommonApiClients lcommonApi;

        public GetGRNAccountsLookupHandler(ICommonApiClients lcommonApi)
        {
            this.lcommonApi = lcommonApi;
        }

        public async Task<string> Handle(GetGRNAccountsLookupService request, CancellationToken cancellationToken)
        {
            //var data = new GetGRNAccountsLookupEntity()
            //{
            //    GetLookup = request.GetLookup
            //};
            var result = await lcommonApi.GetGRNAccountsLookup(request.GetLookup);
            return result;
        }
    }

    public class GetWarehouseBasedCompanySectionHandler : IRequestHandler<GetWarehouseBasedCompanySectionService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetWarehouseBasedCompanySectionHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWarehouseBasedCompanySectionService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.SharedRepository.GetWarehouseBasedCompanySection(request.GetAllCompanySection);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetCommonSectionsHandler : IRequestHandler<GetCommonSectionService, List<GetSectionsEntity>>
    {
        private readonly ICommonApiClients lcommonApi;

        public GetCommonSectionsHandler(ICommonApiClients lcommonApi)
        {
            this.lcommonApi = lcommonApi;
        }

        public async Task<List<GetSectionsEntity>> Handle(GetCommonSectionService request, CancellationToken cancellationToken)
        {
            var result = await lcommonApi.GetGRNSections(request.GetWarehouseSections);
            return result;
        }
    }
    public class GetFilePathHandler : IRequestHandler<GetFilePathService, string>
    {
        private readonly ICommonApiClients lcommonApi;

        public GetFilePathHandler(ICommonApiClients lcommonApi)
        {
            this.lcommonApi = lcommonApi;
        }

        public async Task<string> Handle(GetFilePathService request, CancellationToken cancellationToken)
        {
            var result = await lcommonApi.GetFilePath(request.GetFilePath);
            return result;
        }
    }
    public class GetDocumentPathViewHandler : IRequestHandler<GetDocumentPathViewService, string>
    {
        private readonly ICommonApiClients lcommonApi;

        public GetDocumentPathViewHandler(ICommonApiClients lcommonApi)
        {
            this.lcommonApi = lcommonApi;
        }

        public async Task<string> Handle(GetDocumentPathViewService request, CancellationToken cancellationToken)
        {
            var result = await lcommonApi.GetDocumentPathView(request.GetFilePath);
            return result;
        }
    }
    public class GetMaterialLookupHandler : IRequestHandler<GetMaterialLookupService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetMaterialLookupHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetMaterialLookupService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.SharedRepository.GetMaterialLookup(request.Lookup);
            return result;
        }
    }
    public class GetMaterialProductLookupHandler : IRequestHandler<GetMaterialProductLookupService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetMaterialProductLookupHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetMaterialProductLookupService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.SharedRepository.GetMaterialProductLookup(request.Lookup);
            return result;
        }
    }
}
