﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RmKV.NXT.Inventory.Api.Shared
{
    public class GetCompanyServiceValidations : AbstractValidator<GetCompanyServices>
    {
    }

    public class GetGRNDocumentRootPathAndUrlValidations : AbstractValidator<GetGRNDocumentRootPathAndUrlServices>
    {
    }

    public class GetAllSalesLocationValidations : AbstractValidator<GetAllSalesLocationServices>
    {

    }

    public class GetSalesLocationValidations : AbstractValidator<GetSalesLocationServices>
    {

    }

    public class GetEmployeeLookupValidations : AbstractValidator<GetEmployeeLookupServices>
    {
    }

    public class GetResignedEmployeeLookupValidations : AbstractValidator<GetResignedEmployeeLookupService>
    {

    }

    public class GetAllUserValidation : AbstractValidator<GetAllUserService>
    {

    }

    public class GetUserProfileValidation : AbstractValidator<GetUserProfileService>
    {

    }
    public class FetchNotificationValidation : AbstractValidator<FetchNotificationService>
    {

    }
    public class SaveNotificationValidation : AbstractValidator<SaveNotificationService>
    {

    }
    public class ViewNotificationValidation : AbstractValidator<ViewNotificationService>
    {

    }
    public class GetWarehouseValidation : AbstractValidator<GetWarehouseService>
    {
    }
    public class CheckLockPasswordValidation : AbstractValidator<CheckLockPasswordService>
    {
    }
    public class GetDashboardValidation : AbstractValidator<GetDashboardService>
    {

    }
    public class GetSectionValidation : AbstractValidator<GetSectionService>
    {
    }
    public class GetGroupSectionValidation : AbstractValidator<GetGroupSectionService>
    {

    }
    public class GetByNoDetailsValidation : AbstractValidator<GetByNoDetailsService>
    {

    }
    public class GetGlobalParamsValidations : AbstractValidator<GetGlobalParamsServices>
    {

    }
    public class GetGRNAccountsLookupValidation : AbstractValidator<GetGRNAccountsLookupService>
    {

    }

    public class GetCompanySectionValidation : AbstractValidator<GetWarehouseBasedCompanySectionService>
    {

    }

    public class GetCommonSectionsValidation : AbstractValidator<GetCommonSectionService>
    {

    }
    public class GetFilePathValidation : AbstractValidator<GetFilePathService>
    {

    }
    public class GetDocumentPathViewValidation : AbstractValidator<GetDocumentPathViewService>
    {

    }
    public class GetMaterialLookupValidation : AbstractValidator<GetMaterialLookupService>
    {

    }
    public class GetMaterialProductLookupValidation : AbstractValidator<GetMaterialProductLookupService>
    {

    }
}
