﻿using MediatR;
using RmKV.NXT.Inventory.DataModel.Model.Shared;
using RmKV.NXT.Inventory.DataModel.Model.Shared.Company;
using RmKV.NXT.Inventory.DataModel.Model.Shared.HRDocumentRootPathAndUrl;
using RmKV.NXT.Inventory.DataModel.Model.Shared.SalesLocation;
using  RmKV.NXT.Inventory.DataModel.Models.Shared;
using RMKV.NXT.Inventory.DataModel.Model.Shared;
using System.Collections.Generic;
using System.IO;

namespace RmKV.NXT.Inventory.Api.Shared
{
    public class GetAllSalesLocationServices : IRequest<List<GetSalesLocationEntity>>
    {
        public string SalesLocation { get; set; }
    }
    public class GetCompanyServices : IRequest<List<GetCompanyEntity>>
    {
        public int User_ID { get; set; }
        public int Sales_Location_ID { get; set; }
    }
    public class GetSalesLocationServices : IRequest<List<GetSalesLocationEntity>>
    {
        public int User_Id { get; set; }
        public int Sales_Location_Id { get; set; }
    }
    public class GetGRNDocumentRootPathAndUrlServices : IRequest<GetGRNDocumentRootPathAndUrlEntity>
    {
        public int SalesLocationId { get; set; }
    }
    public class GetEmployeeLookupServices : IRequest<List<GetEmployeeLookupEntity>>
    {
        public int Sales_Location_Id { get; set; }
        public string For { get; set; }
        public bool Resigned { get; set; }
    }
    public class GetResignedEmployeeLookupService : IRequest<List<GetEmployeeLookupEntity>>
    {
        public int Location_Id { get; set; }
    }
    public class GetAllUserService : IRequest<List<EmployeeAllUserLookUpEntity>>
    {

    }
    public class GetUserProfileService : IRequest<string>
    {
        public int UserProfile { get; set; }
    }
    public class FetchNotificationService : IRequest<string>
    {
        public string Notification { get; set; }
    }
    public class SaveNotificationService : IRequest<int>
    {
        public string Notification { get; set; }
    }
    public class ViewNotificationService : IRequest<int>
    {
        public string Notification { get; set; }
    }
    public class CheckLockPasswordService : IRequest<bool>
    {
        public string User_Name { get; set; }
        public string Password { get; set; }
    }
    public class GetDashboardService : IRequest<DashboardTable>
    {
        public int Warehouse_Id { get; set; }
    }
    public class GetWarehouseService : IRequest<string>
    {
        public string Warehouse { get; set; }
    }
    public class GetSectionService : IRequest<string>
    {
        public string Section { get; set; }
    }
    public class GetGroupSectionService : IRequest<List<GetGroupSectionsEntity>>
    {

    }   
    public class GetByNoDetailsService : IRequest<string>
    {
        public string ByNoData { get; set; }
    }
    public class GetGlobalParamsServices : IRequest<string>
    {
        public string globalParams { get; set; }
    }
    public class GetGRNAccountsLookupService : IRequest<string>
    {
        public string GetLookup { get; set; }
    }
    public class GetWarehouseBasedCompanySectionService : IRequest<string>
    {
        public string GetAllCompanySection { get; set; }
    }
    public class GetCommonSectionService : IRequest<List<GetSectionsEntity>>
    {
        public string GetWarehouseSections { get; set; }
    }
    public class GetFilePathService : IRequest<string>
    {
        public string GetFilePath { get; set; }
    }
    public class GetDocumentPathViewService : IRequest<string>
    {
        public string GetFilePath { get; set; }
    }
    public class GetMaterialLookupService : IRequest<string>
    {
        public string Lookup { get; set; }
    }
    public class GetMaterialProductLookupService : IRequest<string>
    {
        public string Lookup { get; set; }
    }
}

