﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.Purchases;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Purchases
{
    public class GetInvoiceInBaleHandler : IRequestHandler<GetInvoiceInBaleService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetInvoiceInBaleHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetInvoiceInBaleService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceInBaleRepository.GetInvoiceInBales(request.InvoiceInBale);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class AddInvoiceInBaleHandler : IRequestHandler<AddInvoiceInBaleService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddInvoiceInBaleHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddInvoiceInBaleService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceInBaleRepository.AddInvoiceInBales(request.InvoiceInBale);
            unitOfWork.Commit();
            return result;
        }
    }
    public class FetchInvoiceInBaleHandler : IRequestHandler<FetchInvoiceInBaleService, ReturnTable>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchInvoiceInBaleHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<ReturnTable> Handle(FetchInvoiceInBaleService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceInBaleRepository.FetchInvoiceInBales(request.InvoiceInBale);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class DeleteInvoiceInBaleHandler : IRequestHandler<DeleteInvoiceInBaleService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public DeleteInvoiceInBaleHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(DeleteInvoiceInBaleService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceInBaleRepository.DeleteInvoiceInBales(request.InvoiceInBale);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetSectionGRNHandler : IRequestHandler<GetSectionGRNService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetSectionGRNHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetSectionGRNService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceInBaleRepository.GetSectionGrn(request.SectionGRN);
            //unitOfWork.Commit();
            return result;
        }
    }
}
