﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.Purchases;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Purchases
{
    public class GetInvoiceInBaleService : IRequest<string>
    {
        public string InvoiceInBale { get; set; }
    }
    public class AddInvoiceInBaleService : IRequest<bool>
    {
        public string InvoiceInBale { get; set; }
    }
    public class FetchInvoiceInBaleService : IRequest<ReturnTable>
    {
        public string InvoiceInBale { get; set; }
    }
    public class DeleteInvoiceInBaleService : IRequest<bool>
    {
        public string InvoiceInBale { get; set; }
    }
    public class GetSectionGRNService : IRequest<string>
    {
        public string SectionGRN { get; set; }
    }
}
