﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Purchases
{
    public class GetInvoiceInBaleValidation : AbstractValidator<GetInvoiceInBaleService>
    {

    }
    public class AddInvoiceInBaleValidation : AbstractValidator<AddInvoiceInBaleService>
    {

    }
    public class FetchInvoiceInBaleValidation : AbstractValidator<FetchInvoiceInBaleService>
    {

    }
    public class DeleteInvoiceInBaleValidation : AbstractValidator<DeleteInvoiceInBaleService>
    {

    }
    public class GetSectionGRNValidation : AbstractValidator<GetSectionGRNService>
    {

    }
}
