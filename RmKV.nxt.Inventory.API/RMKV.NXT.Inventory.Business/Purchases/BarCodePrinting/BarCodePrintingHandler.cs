﻿using MediatR;
using Newtonsoft.Json;
using QRCoder;
using Rmkv.Nxt.GRN.Business.BItMapimages;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Purchases.BarCodePrinting
{
    public class BarCodePrintingHandler : IRequestHandler<BarCodePrintingService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public BarCodePrintingHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(BarCodePrintingService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.BarCodePrintingRepository.GetBarCodePrintingRepository(request.BarCodePrinting);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GenerateBarcodeHandler : IRequestHandler<GenerateBarcodePrintService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GenerateBarcodeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GenerateBarcodePrintService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.BarCodePrintingRepository.GenerateBarcodePrintRepository(request.BynoData);
            unitOfWork.Commit();
            return result;
        }

    }
    public class SelectBarCodePrintingHandler : IRequestHandler<SelectBarCodePrintingService, List<selectPrintingList>>
    {
        public async Task<List<selectPrintingList>> Handle(SelectBarCodePrintingService request, CancellationToken cancellationToken)
        {
            var strbynovalue = string.Empty;
            var strbynoserialvalue = string.Empty;
            var strbynoprodserialvalue = string.Empty;
            var jsonString = JsonConvert.DeserializeObject<List<selectPrintingList>>(request.BarCodePrinting);
            QRCodeGenerator _qrCode = new QRCodeGenerator();
            List<selectPrintingList> list = new List<selectPrintingList>();
            foreach (var data in jsonString)
            {
                var byno = data.byno;
                var bynoparts = byno.Split('/');
                var strbyno = bynoparts[0];
                var bynoserial = bynoparts[1];
                var bynoprodserial = bynoparts[2];
                var dblsellingpricevalue = string.Empty;
                var strbarcode = string.Empty;

                for (var i = 4; i > bynoserial.Length; i--)
                {
                    strbynovalue += ("0");
                }
                for (var j = 4; j > bynoprodserial.Length; j--)
                {
                    strbynoserialvalue += ("0");
                }
                for (var k = 5; k > data.selling_price.Length; k--)
                {
                    dblsellingpricevalue += ("0");
                }
                var bynumber = strbyno + strbynovalue + bynoserial + strbynoserialvalue + bynoprodserial;
                strbarcode = bynumber + dblsellingpricevalue + data.selling_price + "00" + dblsellingpricevalue + data.selling_price + "00";
                QRCodeData _qrCodeData = _qrCode.CreateQrCode(Convert.ToString(strbarcode), QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(_qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(20);
                list.Add(new selectPrintingList
                {
                    //images = qrCodeImage,
                    byno = byno,
                    product_code = data.product_code,
                    selling_price = data.selling_price,
                    product_name = data.product_name,
                    counter_code = data.counter_code,
                    isSelected = data.isSelected,
                    Caption = request.Caption,
                    NoOfCopies = request.NoOfCopies

                });
            }
            return list;


        }
    }
    public class DoublerateBarCodePrintingHandler : IRequestHandler<DoublerateBarCodePrintingService, List<selectPrintingList>>
    {
        public async Task<List<selectPrintingList>> Handle(DoublerateBarCodePrintingService request, CancellationToken cancellationToken)
        {
            var strbynovalue = string.Empty;
            var strbynoserialvalue = string.Empty;
            var strbynoprodserialvalue = string.Empty;
            var jsonString = JsonConvert.DeserializeObject<List<selectPrintingList>>(request.BarCodePrinting);
            QRCodeGenerator _qrCode = new QRCodeGenerator();
            List<selectPrintingList> list = new List<selectPrintingList>();
            foreach (var data in jsonString)
            {
                var byno = data.byno;
                var bynoparts = byno.Split('/');
                var strbyno = bynoparts[0];
                var bynoserial = bynoparts[1];
                var bynoprodserial = bynoparts[2];
                var dblsellingpricevalue = string.Empty;
                var strbarcode = string.Empty;

                for (var i = 4; i > bynoserial.Length; i--)
                {
                    strbynovalue += ("0");
                }
                for (var j = 4; j > bynoprodserial.Length; j--)
                {
                    strbynoserialvalue += ("0");
                }
                for (var k = 5; k > data.selling_price.Length; k--)
                {
                    dblsellingpricevalue += ("0");
                }
                var bynumber = strbyno + strbynovalue + bynoserial + strbynoserialvalue + bynoprodserial;
                strbarcode = bynumber + dblsellingpricevalue + data.selling_price + "00" + dblsellingpricevalue + data.selling_price + "00";
                QRCodeData _qrCodeData = _qrCode.CreateQrCode(Convert.ToString(strbarcode), QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(_qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(20);
                list.Add(new selectPrintingList
                {
                    images = BItMapimage.BitmapToBytesCodes(qrCodeImage),
                    byno = byno,
                    product_code = data.product_code,
                    selling_price = data.selling_price,
                    product_name = data.product_name,
                    counter_code = data.counter_code,
                    isSelected = data.isSelected,

                });
            }
            return list;

        }
    }

    public class ImageBytesHandler : IRequestHandler<ImageBytesService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public ImageBytesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(ImageBytesService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.BarCodePrintingRepository.ImageBytes();
            unitOfWork.Commit();
            return result;
        }
    }
}
