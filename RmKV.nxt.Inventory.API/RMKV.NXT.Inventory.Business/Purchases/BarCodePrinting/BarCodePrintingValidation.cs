﻿using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Purchases.BarCodePrinting
{
    public class GetBarCodePrintingValidation : AbstractValidator<BarCodePrintingService>
    {

    }
    public class GenerateBarcodePrintValidation : AbstractValidator<GenerateBarcodePrintService>
    {

    }
    public class SelectBarCodePrintingValidation : AbstractValidator<SelectBarCodePrintingService>
    {

    }
    public class DoubleRateBarCodePrintingValidation : AbstractValidator<DoublerateBarCodePrintingService>
    {

    }
    public class ImageBytesValidation : AbstractValidator<ImageBytesService>
    {

    }
}
