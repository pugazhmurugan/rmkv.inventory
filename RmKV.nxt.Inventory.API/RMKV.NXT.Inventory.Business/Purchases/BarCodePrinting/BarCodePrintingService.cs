﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Purchases.BarCodePrinting
{
   public class BarCodePrintingService : IRequest<string>
    {
        public string BarCodePrinting { get; set; }
    }
    public class GenerateBarcodePrintService : IRequest<string>
    {
        public string BynoData { get; set; }
    }
    public class SelectBarCodePrintingService : IRequest<List<selectPrintingList>>
    {
        public string BarCodePrinting { get; set; }
        public string Caption { get; set; }
        public int NoOfCopies { get; set; }


    }
    public class DoublerateBarCodePrintingService : IRequest<List<selectPrintingList>>
    {
        public string BarCodePrinting { get; set; }
        public string Caption { get; set; }
        public int NoOfCopies { get; set; }
    }

    public class ImageBytesService : IRequest<string>
    {
    }
}
