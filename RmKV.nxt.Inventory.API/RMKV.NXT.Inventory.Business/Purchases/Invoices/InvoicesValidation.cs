﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Purchases.Invoices
{
    public class CheckPriceCodeValidation : AbstractValidator<CheckPriceCodeService>
    {

    }
    public class GetSupplierDescValidation : AbstractValidator<GetSupplierDescService>
    {

    }
    public class InvoiceEntryConfigValidation : AbstractValidator<InvoiceEntryConfigService>
    {

    }
    public class GetGSTByHSNValidation : AbstractValidator<GetGSTByHSNService>
    {

    }
    public class GetInvoiceNosValidation: AbstractValidator<GetInvoiceNosService>
    {

    }
    public class GetMasterDiscountValidation : AbstractValidator<GetMasterDiscountService>
    {

    }
    public class GetInvoicePoNosValidation : AbstractValidator<GetInvoicePoNosService>
    {

    }
    public class AddInvoiceValidation : AbstractValidator<AddInvoiceService>
    {

    }
    public class FetchInvoiceValidation : AbstractValidator<FetchInvoiceService>
    {

    }
    public class GetInvoiceValidation : AbstractValidator<GetInvoiceService>
    {

    }
    public class ApproveInvoiceValidation: AbstractValidator<ApproveInvoiceService>
    {

    }
    public class PostInvoiceValidation : AbstractValidator<PostInvoiceService>
    {

    }
    public class EditCostPriceValidation : AbstractValidator<EditCostPriceService>
    {

    }
    public class GetInvoiceCheckListReportValidation : AbstractValidator<GetInvoiceCheckListReportService>
    {

    }
}
