﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model.Purchases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Purchases.Invoices
{
    public class GetSupplierDescHandler : IRequestHandler<GetSupplierDescService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetSupplierDescHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetSupplierDescService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.GetSupplierDescription(request.ProductDesc);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class CheckPriceCodeHandler : IRequestHandler<CheckPriceCodeService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CheckPriceCodeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(CheckPriceCodeService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.CheckPriceCode(request.PriceCode);
            unitOfWork.Commit();
            return result;
        }
    }
    public class InvoiceEntryConfigHandler : IRequestHandler<InvoiceEntryConfigService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public InvoiceEntryConfigHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(InvoiceEntryConfigService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.InvoiceEntryConfig(request.group_section_id);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetGstByHSNHandler : IRequestHandler<GetGSTByHSNService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetGstByHSNHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetGSTByHSNService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.GetGstByHSN(request.GstByHSN);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetInvoiceNosHandler : IRequestHandler<GetInvoiceNosService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetInvoiceNosHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetInvoiceNosService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.GetInvoiceNos(request.InvoiceNos);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetMasterDiscountHandler : IRequestHandler<GetMasterDiscountService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetMasterDiscountHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetMasterDiscountService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.GetMasterDiscount(request.Discount);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetInvoicePoNosHandler : IRequestHandler<GetInvoicePoNosService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetInvoicePoNosHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetInvoicePoNosService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.GetInvoicePoNos(request.PoNos);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class AddInvoiceHandler : IRequestHandler<AddInvoiceService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public AddInvoiceHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(AddInvoiceService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.AddInvoices(request.Invoices);
            unitOfWork.Commit();
            return result;
        }
    }
    public class FetchInvoiceHandler : IRequestHandler<FetchInvoiceService, InvoicesTable>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchInvoiceHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<InvoicesTable> Handle(FetchInvoiceService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.FetchInvoices(request.Invoices);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetInvoiceHandler : IRequestHandler<GetInvoiceService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetInvoiceHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetInvoiceService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.GetInvoices(request.Invoices);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class ApproveInvoiceHandler : IRequestHandler<ApproveInvoiceService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public ApproveInvoiceHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(ApproveInvoiceService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.ApproveInvoices(request.Invoices);
            unitOfWork.Commit();
            return result;
        }
    }
    public class PostInvoiceHandler : IRequestHandler<PostInvoiceService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public PostInvoiceHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(PostInvoiceService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.PostInvoices(request.Invoices);
            unitOfWork.Commit();
            return result;
        }
    }
    public class EditCostPriceHandler : IRequestHandler<EditCostPriceService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public EditCostPriceHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(EditCostPriceService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.EditCostPrice(request.Invoices);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetInvoiceCheckListReportHandler : IRequestHandler<GetInvoiceCheckListReportService, DataSet>
    {
        private readonly IUnitofwork unitOfWork;

        public GetInvoiceCheckListReportHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<DataSet> Handle(GetInvoiceCheckListReportService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoicesRepository.GetInvoiceCheckListReport(request.Invoice_Check_List);
            unitOfWork.Commit();
            return result;
        }
    }
}
