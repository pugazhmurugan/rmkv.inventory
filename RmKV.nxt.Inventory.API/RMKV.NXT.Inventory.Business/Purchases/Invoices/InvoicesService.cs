﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model.Purchases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Purchases.Invoices
{
    public class CheckPriceCodeService: IRequest<bool>
    {
        public string PriceCode { get; set; }
    }
    public class GetSupplierDescService: IRequest<string>
    {
        public string ProductDesc { get; set; }
    }
    public class InvoiceEntryConfigService : IRequest<string>
    {
        public int group_section_id { get; set; }
    }
    public class GetGSTByHSNService : IRequest<string>
    {
        public string GstByHSN { get; set; }
    }
    public class GetInvoiceNosService: IRequest<string>
    {
        public string InvoiceNos { get; set; }
    }
    public class GetMasterDiscountService: IRequest<string>
    {
        public string Discount { get; set; }
    }
    public class GetInvoicePoNosService : IRequest<string>
    {
        public string PoNos { get; set; }
    }
    public class AddInvoiceService: IRequest<string>
    {
        public string Invoices { get; set; }
    }
    public class FetchInvoiceService : IRequest<InvoicesTable>
    {
        public string Invoices { get; set; }
    }
    public class GetInvoiceService : IRequest<string>
    {
        public string Invoices { get; set; }
    }
    public class ApproveInvoiceService: IRequest<bool>
    {
        public string Invoices { get; set; }
    }
    public class PostInvoiceService: IRequest<bool>
    {
        public string Invoices { get; set; }
    }
    public class EditCostPriceService: IRequest<bool>
    {
        public string Invoices { get; set; }
    }

    public class GetInvoiceCheckListReportService : IRequest<DataSet>
    {
        public string Invoice_Check_List { get; set; }
    }
}
