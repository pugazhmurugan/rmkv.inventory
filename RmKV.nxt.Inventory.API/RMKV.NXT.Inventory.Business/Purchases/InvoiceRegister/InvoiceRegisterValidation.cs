﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Purchases.InvoiceRegister
{
    public class GetInvoiceRegisterValidation : AbstractValidator<GetInvoiceRegisterService>
    {
    }

    public class AddInvoiceRegisterValidation : AbstractValidator<AddInvoiceRegisterService>
    {

    }

    public class FetchInvoiceRegisterValidation : AbstractValidator<FetchInvoiceRegisterService>
    {

    }

    public class DeleteInvoiceRegisterValidation : AbstractValidator<DeleteInvoiceRegisterService>
    {

    }

    public class ApproveInvoiceRegisterValidation : AbstractValidator<ApproveInvoiceRegisterService>
    {

    }
}
