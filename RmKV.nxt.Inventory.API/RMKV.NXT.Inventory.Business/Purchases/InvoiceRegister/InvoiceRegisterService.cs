﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Purchases.InvoiceRegister
{
   
       public class GetInvoiceRegisterService : IRequest<string> 
        {
            public string InvoiceRegister { get; set; }
        }

        public class AddInvoiceRegisterService : IRequest<bool>
        {
            public string InvoiceRegister { get; set; }
        }

        public class FetchInvoiceRegisterService : IRequest<string>
        {
            public string InvoiceRegister { get; set; }
        }

        public class DeleteInvoiceRegisterService : IRequest<bool>
        {
            public string InvoiceRegister { get; set; }
        }

    public class ApproveInvoiceRegisterService : IRequest<bool>
    {
        public string InvoiceRegister { get; set; }
    }
}

