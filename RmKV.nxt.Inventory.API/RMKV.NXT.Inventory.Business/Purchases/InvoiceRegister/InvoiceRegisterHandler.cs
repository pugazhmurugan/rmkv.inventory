﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Purchases.InvoiceRegister
{
    public class GetInvoiceRegisterHandler : IRequestHandler<GetInvoiceRegisterService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetInvoiceRegisterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetInvoiceRegisterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceRegisterRepository.GetInvoiceRepository(request.InvoiceRegister);
            //unitOfWork.Commit();
            return result;
        }
    }

    public class AddInvoiceRegisterHandler : IRequestHandler<AddInvoiceRegisterService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddInvoiceRegisterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddInvoiceRegisterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceRegisterRepository.AddInvoiceRepository(request.InvoiceRegister);
            unitOfWork.Commit();
            return result;
        }
    }

    public class FetchInvoiceRegisterHandler : IRequestHandler<FetchInvoiceRegisterService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchInvoiceRegisterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(FetchInvoiceRegisterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceRegisterRepository.FetchProductGroups(request.InvoiceRegister);
            unitOfWork.Commit();
            return result;
        }
    }

    public class DeleteInvoiceRegisterHandler : IRequestHandler<DeleteInvoiceRegisterService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public DeleteInvoiceRegisterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(DeleteInvoiceRegisterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceRegisterRepository.DeleteInvoiceRepository(request.InvoiceRegister);
            unitOfWork.Commit();
            return result;
        }
    }

    public class ApproceInvoiceRegisterHandler : IRequestHandler<ApproveInvoiceRegisterService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public ApproceInvoiceRegisterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(ApproveInvoiceRegisterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceRegisterRepository.ApproveInvoiceRepository(request.InvoiceRegister);
            unitOfWork.Commit();
            return result;
        }
    }
}
