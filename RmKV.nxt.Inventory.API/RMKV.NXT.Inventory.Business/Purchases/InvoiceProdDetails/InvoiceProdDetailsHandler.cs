﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Purchases.InvoiceProdDetails
{
    public class GetInvoiceProdDetailsHandler : IRequestHandler<GetInvoiceProdDetailsService, Table1>
    {
        private readonly IUnitofwork unitOfWork;
        public GetInvoiceProdDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<Table1> Handle(GetInvoiceProdDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceProdDetailsRepository.GetInvoiceProdDetails(request.InvoiceProdDetails);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class AddInvoiceProdDetailsHandler : IRequestHandler<AddInvoiceProdDetailsService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddInvoiceProdDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddInvoiceProdDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceProdDetailsRepository.AddInvoiceProdDetails(request.InvoiceProdDetails);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetInvoiceProductLookupHandler : IRequestHandler<GetInvoiceProductLookupService, Table>
    {
        private readonly IUnitofwork unitOfWork;
        public GetInvoiceProductLookupHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<Table> Handle(GetInvoiceProductLookupService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceProdDetailsRepository.GetInvoiceProductLookup(request.InvoiceProdDetails);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetInvoiceProdAttributesHandler : IRequestHandler<GetInvoiceProdAttributesService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetInvoiceProdAttributesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetInvoiceProdAttributesService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceProdDetailsRepository.GetInvoiceProdAttributes(request.InvoiceProdDetails);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetProductLookupDetailsHandler : IRequestHandler<GetProductLookupDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductLookupDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetProductLookupDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceProdDetailsRepository.GetProductLookupDetails(request.InvoiceProdDetails);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetCounterDetailsHandler : IRequestHandler<GetCounterDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetCounterDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetCounterDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceProdDetailsRepository.GetCounterDetails(request.InvoiceProdDetails);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class FetchInvoiceProductDetailsHandler : IRequestHandler<FetchInvoiceProductDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchInvoiceProductDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(FetchInvoiceProductDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceProdDetailsRepository.FetchInvoiceProductDetails(request.InvoiceProdDetails);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class SectionWiseAttributeHandler : IRequestHandler<SectionWiseAttributeService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public SectionWiseAttributeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(SectionWiseAttributeService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceProdDetailsRepository.SectionWiseAttribute(request.InvoiceProdDetails);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetAttributesValuesDetailsHandler : IRequestHandler<GetAttributesValuesDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetAttributesValuesDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetAttributesValuesDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceProdDetailsRepository.GetAttributesValuesDetails(request.InvoiceProdDetails);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetProductDetailsByBrandTypeHandler : IRequestHandler<GetProductDetailsByBrandTypeService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductDetailsByBrandTypeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetProductDetailsByBrandTypeService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.InvoiceProdDetailsRepository.GetProductDetailsByBrandType(request.InvoiceProdDetails);
            //unitOfWork.Commit();
            return result;
        }
    }
}
