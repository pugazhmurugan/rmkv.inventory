﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Purchases.InvoiceProdDetails
{
    public class GetInvoiceProdDetailsValidation : AbstractValidator<GetInvoiceProdDetailsService>
    {

    }
    public class AddInvoiceProdDetailsValidation : AbstractValidator<AddInvoiceProdDetailsService>
    {

    }
    public class GetInvoiceProductLookupValidation : AbstractValidator<GetInvoiceProductLookupService>
    {

    }
    public class GetInvoiceProdAttributesValidation : AbstractValidator<GetInvoiceProdAttributesService>
    {

    }
    public class GetProductLookupDetailsValidation : AbstractValidator<GetProductLookupDetailsService>
    {

    }
    public class GetCounterDetailsValidation : AbstractValidator<GetCounterDetailsService>
    {

    }
    public class FetchInvoiceProductDetailsValidation : AbstractValidator<FetchInvoiceProductDetailsService>
    {

    }
    public class SectionWiseAttributeValidation : AbstractValidator<SectionWiseAttributeService>
    {

    }
    public class GetAttributesValuesDetailsValidation : AbstractValidator<GetAttributesValuesDetailsService>
    {

    }
    public class GetProductDetailsByBrandTypeValidation : AbstractValidator<GetProductDetailsByBrandTypeService>
    {

    }
}
