﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Purchases.InvoiceProdDetails
{
    public class GetInvoiceProdDetailsService : IRequest<Table1>
    {
        public string InvoiceProdDetails { get; set; }
    }
    public class AddInvoiceProdDetailsService : IRequest<bool>
    {
        public string InvoiceProdDetails { get; set; }
    }
    public class GetInvoiceProductLookupService : IRequest<Table>
    {
        public string InvoiceProdDetails { get; set; }
    }
    public class GetInvoiceProdAttributesService : IRequest<string>
    {
        public string InvoiceProdDetails { get; set; }
    }
    public class GetProductLookupDetailsService : IRequest<string>
    {
        public string InvoiceProdDetails { get; set; }
    }
    public class GetCounterDetailsService : IRequest<string>
    {
        public string InvoiceProdDetails { get; set; }
    }
    public class FetchInvoiceProductDetailsService : IRequest<string>
    {
        public string InvoiceProdDetails { get; set; }
    }
    public class SectionWiseAttributeService : IRequest<string>
    {
        public int InvoiceProdDetails { get; set; }
    }
    public class GetAttributesValuesDetailsService : IRequest<string>
    {
        public string InvoiceProdDetails { get; set; }
    }
    public class GetProductDetailsByBrandTypeService : IRequest<string>
    {
        public string InvoiceProdDetails { get; set; }
    }
}
