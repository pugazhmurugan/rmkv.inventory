﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.GoodsTransfer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfer
{
    public class GetWithincityVirtualToStoreHandler : IRequestHandler<GetWithincityVirtualToStoreService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetWithincityVirtualToStoreHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWithincityVirtualToStoreService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.GetWithincityVirtualToStore(request.GoodsTransfer);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetGoodsTransferTypeHandler : IRequestHandler<GetGoodsTransferTypeService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetGoodsTransferTypeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetGoodsTransferTypeService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.GetGoodsTransferType(request.GoodsTransfer);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetWithinStateToLocationHandler : IRequestHandler<GetWithinStateToLocationService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetWithinStateToLocationHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWithinStateToLocationService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.GetWithinStateToLocation(request.GoodsTransfer);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetOtherStateToLocationHandler : IRequestHandler<GetOtherStateToLocationService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetOtherStateToLocationHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetOtherStateToLocationService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.GetOtherStateToLocation(request.GoodsTransfer);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetWithinStateVirtualStoreToStoreHandler : IRequestHandler<GetWithinStateVirtualStoreToStoreService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetWithinStateVirtualStoreToStoreHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWithinStateVirtualStoreToStoreService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.GetWithinStateVirtualStoreToStore(request.GoodsTransfer);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GoodsTransferFromStoreHandler : IRequestHandler<GoodsTransferFromStoreService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GoodsTransferFromStoreHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GoodsTransferFromStoreService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.GoodsTransferFromStore(request.GoodsTransfer);
            unitOfWork.Commit();
            return result;
        }

    }

    ////////////////////////////////////////Goods Transfer (with In City)/////////////////////////////////////////


    public class AddWithincityHandler : IRequestHandler<AddWithincityService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddWithincityHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddWithincityService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.AddWithincity(request.GoodsTransfer);
            unitOfWork.Commit();
            return result;
        }
    }
    public class ApproveWithincityHandler : IRequestHandler<ApproveWithincityService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public ApproveWithincityHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(ApproveWithincityService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.ApproveWithincity(request.GoodsTransfer);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetTransferNoWithincityHandler : IRequestHandler<GetTransferNoWithincityService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public GetTransferNoWithincityHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(GetTransferNoWithincityService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.GetTransferNoWithincity(request.GoodsTransfer);
            unitOfWork.Commit();
            return result;
        }
    }
    public class CancelWithincityHandler : IRequestHandler<CancelWithincityService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CancelWithincityHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(CancelWithincityService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.CancelWithincity(request.GoodsTransfer);
            unitOfWork.Commit();
            return result;
        }
    }
    public class FetchWithincityHandler : IRequestHandler<FetchWithincityService, Table1>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchWithincityHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<Table1> Handle(FetchWithincityService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.FetchWithincity(request.GoodsTransfer);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetWithincityHandler : IRequestHandler<GetWithincityService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetWithincityHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWithincityService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.GetWithincity(request.GoodsTransfer);
            unitOfWork.Commit();
            return result;
        }
    }


    //////////// Goods Transfer Report 
    ///
    public class GoodsTransferReportHandler : IRequestHandler<GoodsTransferReportService, DataSet>
    {
        private readonly IUnitofwork unitOfWork;

        public GoodsTransferReportHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<DataSet> Handle(GoodsTransferReportService request, CancellationToken cancellationToken)
        {

            var data = new GoodsTransferEntity()
            {
                GoodsTransferReport = request.GoodsTransferReport,
                Type = request.Type
            };
            var result = await unitOfWork.GoodsTransferRepository.GoodsTransferReport(data);
            unitOfWork.Commit();
            return result;
        }
    }


    public class CheckGoodsTransferReportHandler : IRequestHandler<CheckGoodsTransferReportService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public CheckGoodsTransferReportHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(CheckGoodsTransferReportService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferRepository.CheckGoodsTransferReport(request.CheckGoodsTransfer);
            unitOfWork.Commit();
            return result;
        }

    }


}