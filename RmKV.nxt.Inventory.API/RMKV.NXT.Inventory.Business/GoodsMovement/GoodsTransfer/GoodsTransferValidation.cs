﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfer
{
    public class GetWithincityVirtualToStoreValidation : AbstractValidator<GetWithincityVirtualToStoreService>
    {

    }
    public class GetGoodsTransferTypeValidation : AbstractValidator<GetGoodsTransferTypeService>
    {

    }
    public class GetWithinStateToLocationValidation : AbstractValidator<GetWithinStateToLocationService>
    {

    }
    public class GetOtherStateToLocationValidation : AbstractValidator<GetOtherStateToLocationService>
    {

    }
    public class GetWithinStateVirtualStoreToStoreValidation : AbstractValidator<GetWithinStateVirtualStoreToStoreService>
    {

    }
    public class GoodsTransferFromStoreValidation : AbstractValidator<GoodsTransferFromStoreService>
    {

    }
    ////////////////////////////////////////Goods Transfer (with In City)/////////////////////////////////////////
    public class AddWithincityValidation : AbstractValidator<AddWithincityService>
    {

    }
    public class ApproveWithincityValidation : AbstractValidator<ApproveWithincityService>
    {

    }
    public class GetTransferNoWithincityValidation : AbstractValidator<GetTransferNoWithincityService>
    {

    }
    public class CancelWithincityValidation : AbstractValidator<CancelWithincityService>
    {

    }
    public class FetchWithincityValidation : AbstractValidator<FetchWithincityService>
    {

    }
    public class GetWithincityValidation : AbstractValidator<GetWithincityService>
    {

    }


    /// <summary>
    /// ///////////  Goods Transfer Report
    /// </summary>


    public class GoodsTransferReportValidation : AbstractValidator<GoodsTransferReportService>
    {

    }

    public class CheckGoodsTransferReportValidation : AbstractValidator<CheckGoodsTransferReportService>
    {

    }


}