﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfer
{
    public class GetWithincityVirtualToStoreService : IRequest<string>
    {
        public string GoodsTransfer { get; set; }
    }
    public class GetGoodsTransferTypeService : IRequest<string>
    {
        public string GoodsTransfer { get; set; }
    }
    public class GetWithinStateToLocationService : IRequest<string>
    {
        public string GoodsTransfer { get; set; }
    }
    public class GetOtherStateToLocationService : IRequest<string>
    {
        public string GoodsTransfer { get; set; }
    }
    public class GetWithinStateVirtualStoreToStoreService : IRequest<string>
    {
        public string GoodsTransfer { get; set; }
    }
    public class GoodsTransferFromStoreService : IRequest<string>
    {
        public string GoodsTransfer { get; set; }
    }

    ////////////////////////////////////////Goods Transfer (with In City)/////////////////////////////////////////
    public class AddWithincityService : IRequest<int>
    {
        public string GoodsTransfer { get; set; }
    }
    public class ApproveWithincityService : IRequest<bool>
    {
        public string GoodsTransfer { get; set; }
    }
    public class GetTransferNoWithincityService : IRequest<int>
    {
        public string GoodsTransfer { get; set; }
    }
    public class CancelWithincityService : IRequest<bool>
    {
        public string GoodsTransfer { get; set; }
    }
    public class FetchWithincityService : IRequest<Table1>
    {
        public string GoodsTransfer { get; set; }
    }
    public class GetWithincityService : IRequest<string>
    {
        public string GoodsTransfer { get; set; }
    }


    /// Goods Transfer Report 

    public class GoodsTransferReportService : IRequest<DataSet>
    {
        public string GoodsTransferReport { get; set; }
        public bool Type { get; set; }
    }

    public class CheckGoodsTransferReportService : IRequest<int>
    {
        public string CheckGoodsTransfer { get; set; }
    }

}