﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.TransfersExceptionReports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.TransfersExceptionReports.GodownToCounterException
{
    public class GodownToCounterExceptionHandler : IRequestHandler<GodownToCounterExceptionService, DataSet>
    {
        private readonly IUnitofwork unitOfWork;

        public GodownToCounterExceptionHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<DataSet> Handle(GodownToCounterExceptionService request, CancellationToken cancellationToken)
        {
            var data = new GodownToCounterExceptionEntity
            {
                GodownToCounterException_List = request.GodownToCounterException_List,
                Type = request.Type
            };

            var result = await unitOfWork.GodownToCounterExceptionRepository.GodownToCounterExceptionReport(data);
            unitOfWork.Commit();
            return result;
        }
    }
}
