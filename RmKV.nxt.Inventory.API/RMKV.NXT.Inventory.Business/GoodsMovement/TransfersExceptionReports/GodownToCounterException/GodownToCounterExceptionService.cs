﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.TransfersExceptionReports.GodownToCounterException
{
  public class GodownToCounterExceptionService : IRequest<DataSet>
    {
        public string GodownToCounterException_List { get; set; }
        public bool Type { get; set; }
    }
}
