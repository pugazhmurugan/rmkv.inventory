﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.TransfersExceptionReports.CounterToGodownException
{
    public class CounterToGodownExceptionService : IRequest<DataSet>
    {
        public string CounterToGodownException_List { get; set; }
        public bool Type { get; set; }
    }
}
