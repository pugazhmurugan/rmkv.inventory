﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.TransfersExceptionReports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.TransfersExceptionReports.CounterToGodownException
{

    public class CounterToGodownExceptionHandler : IRequestHandler<CounterToGodownExceptionService, DataSet>
    {
        private readonly IUnitofwork unitOfWork;

        public CounterToGodownExceptionHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<DataSet> Handle(CounterToGodownExceptionService request, CancellationToken cancellationToken)
        {
            var data = new CounterToGodownExceptionEntity
            {
                CounterToGodownException_List = request.CounterToGodownException_List,
                Type = request.Type
            };

            var result = await unitOfWork.CounterToGodownExceptionRepository.CounterToGodownExceptionReport(data);
            unitOfWork.Commit();
            return result;
        }
    }
}
