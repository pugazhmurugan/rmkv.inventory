﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.TransfersExceptionReports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.TransfersExceptionReports.GoodsTransferException
{
    public class GoodsTransferExceptionReportHandler : IRequestHandler<GoodsTransferExceptionReportService, DataSet>
    {
        private readonly IUnitofwork unitOfWork;

        public GoodsTransferExceptionReportHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<DataSet> Handle(GoodsTransferExceptionReportService request, CancellationToken cancellationToken)
        {
            var data = new GoodsTransferExceptionEntity()
            {
                Goods_Transfer_Exception_Report = request.Goods_Transfer_Exception_Report,
                Type = request.Type
            };
            var result = await unitOfWork.GoodsTransferExceptionRepository.GetGoodsTransferExceptionReport(data);
            unitOfWork.Commit();
            return result;
        }
    }
}
