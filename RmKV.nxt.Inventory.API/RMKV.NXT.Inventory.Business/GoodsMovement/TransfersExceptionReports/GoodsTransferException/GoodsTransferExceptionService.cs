﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.TransfersExceptionReports.GoodsTransferException
{
    public class GoodsTransferExceptionReportService : IRequest<DataSet>
    {
        public string Goods_Transfer_Exception_Report { get; set; }
        public bool Type { get; set; }
    }
}

