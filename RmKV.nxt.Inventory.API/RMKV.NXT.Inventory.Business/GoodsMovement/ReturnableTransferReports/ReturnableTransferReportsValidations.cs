﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableTransferReports
{
    public class WorkOrderIssueListValidations : AbstractValidator<GetWorkOrderIssueListService>
    {

    }
    public class WorkOrderReceiptListValidations : AbstractValidator<GetWorkOrderReceiptListService>
    {

    }
    public class MismatchStoneQtyValidations : AbstractValidator<GetMismatchStoneQtyService>
    {

    }
    public class GetPolishPendingReportValidations : AbstractValidator<GetPolishPendingReportService>
    {

    }
}
