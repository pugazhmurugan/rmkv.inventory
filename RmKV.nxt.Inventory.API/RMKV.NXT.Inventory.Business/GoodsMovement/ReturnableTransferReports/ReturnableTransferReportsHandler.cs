﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.ReturnableTransferReports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RMKV.NXT.Inventory.DataContext.SqlRepository.GoodsMovement.ReturnableTransferReports;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableTransferReports

{
    public class ReturnableTransferReportsHandler : IRequestHandler<GetWorkOrderIssueListService, DataSet>
    {
        private readonly IUnitofwork unitOfWork;
        public ReturnableTransferReportsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<DataSet> Handle(GetWorkOrderIssueListService request, CancellationToken cancellationToken)
        {
            var data = new GetWorkOrderIssueListEntity()
            {
                WorkOrderIssueListReportView = request.WorkOrderIssueListReportView
            };
            var result = await unitOfWork.ReturnableTransferReportsRepository.GetWorkOrderIssueListReport(data);
            unitOfWork.Commit();
            return result;
        }


    }
    public class GetPolishPendingReportHandler : IRequestHandler<GetPolishPendingReportService, DataSet>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPolishPendingReportHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<DataSet> Handle(GetPolishPendingReportService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ReturnableTransferReportsRepository.GetPolishPendingReport(request.PolishPendingIssueReport);
            return result;
        }
    }
}