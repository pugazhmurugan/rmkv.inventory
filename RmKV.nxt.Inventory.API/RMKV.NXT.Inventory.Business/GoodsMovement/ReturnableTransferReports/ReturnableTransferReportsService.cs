﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableTransferReports
{
    public class GetWorkOrderIssueListService : IRequest<DataSet>
    {
        public string WorkOrderIssueListReportView { get; set; }
    }                

    public class GetWorkOrderReceiptListService : IRequest<DataSet>
    {
        public string WorkOrderReceiptListReportView { get; set; }
    }

    public class GetMismatchStoneQtyService : IRequest<DataSet>
    {
        public string MismatchStoneQtyReportView { get; set; }
    }

    public class GetPolishPendingReportService : IRequest<DataSet>
    {
        public string PolishPendingIssueReport { get; set; }
    }

}

