﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.CustomerWorkOrder
{
    public class AddCustomerWorkOrderHandler : IRequestHandler<AddCustomerWorkOrderService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddCustomerWorkOrderHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddCustomerWorkOrderService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CustomerWorkOrderRepository.AddCustomerWorkOrder(request.CustomerWorkOrder);
            unitOfWork.Commit();
            return result;
        }
    }

    public class CancelCustomerWorkOrderHandler : IRequestHandler<CancelCustomerWorkOrderService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CancelCustomerWorkOrderHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(CancelCustomerWorkOrderService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CustomerWorkOrderRepository.CancelCustomerWorkOrder(request.CustomerWorkOrder);
            unitOfWork.Commit();
            return result;
        }

    }

    public class GetCustomerWorkOrderHandler : IRequestHandler<GetCustomerWorkOrderService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetCustomerWorkOrderHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetCustomerWorkOrderService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CustomerWorkOrderRepository.GetCustomerWorkOrder(request.CustomerWorkOrder);
            unitOfWork.Commit();
            return result;
        }

    }

    public class FetchCustomerWorkOrderHandler : IRequestHandler<FetchCustomerWorkOrderService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchCustomerWorkOrderHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(FetchCustomerWorkOrderService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CustomerWorkOrderRepository.FetchCustomerWorkOrder(request.CustomerWorkOrder);
            unitOfWork.Commit();
            return result;
        }

    }

}
