﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.CustomerWorkOrder
{
    public class AddCustomerWorkOrderValidation : AbstractValidator<AddCustomerWorkOrderService>
    {

    }   
    public class CancelCustomerWorkOrderValidation : AbstractValidator<CancelCustomerWorkOrderService>
    {

    }
   
    public class FetchCustomerWorkOrderValidations : AbstractValidator<FetchCustomerWorkOrderService>
    {

    }
    public class GetCustomerWorkOrderValidations : AbstractValidator<GetCustomerWorkOrderService>
    {

    }
}
