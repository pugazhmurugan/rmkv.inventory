﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.CustomerWorkOrder
{
    public class AddCustomerWorkOrderService : IRequest<int>
    {
        public string CustomerWorkOrder { get; set; }
    }
    public class CancelCustomerWorkOrderService : IRequest<bool>
    {
        public string CustomerWorkOrder { get; set; }
    }
    public class GetCustomerWorkOrderService : IRequest<string>
    {
        public string CustomerWorkOrder { get; set; }
    }
    public class FetchCustomerWorkOrderService : IRequest<string>
    {
        public string CustomerWorkOrder { get; set; }
    }
}
