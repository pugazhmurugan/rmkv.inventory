﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.ProductionIssue
{
    public class AddProductionIssueHandler : IRequestHandler<AddProductionIssueService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddProductionIssueHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddProductionIssueService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionIssueRepository.AddProductionIssue(request.ProductionIssue);
            unitOfWork.Commit();
            return result;
        }
    }

    public class CancelProductionIssueHandler : IRequestHandler<CancelProductionIssueService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CancelProductionIssueHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(CancelProductionIssueService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionIssueRepository.CancelProductionIssue(request.ProductionIssue);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetProductionIssueHandler : IRequestHandler<GetProductionIssueService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductionIssueHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetProductionIssueService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionIssueRepository.GetProductionIssue(request.ProductionIssue);
            unitOfWork.Commit();
            return result;
        }
    }

    public class FetchProductionIssueHandler : IRequestHandler<FetchProductionIssueService, Table2>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchProductionIssueHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<Table2> Handle(FetchProductionIssueService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionIssueRepository.FetchProductionIssue(request.ProductionIssue);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetProductionIssueReconciliationHandler : IRequestHandler<GetProductionIssueReconciliationService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductionIssueReconciliationHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetProductionIssueReconciliationService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionIssueRepository.GetProductionIssueReconciliation(request.ProductionIssue);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetProductionIssueEstimatedVsReceivedHandler : IRequestHandler<GetProductionIssueEstimatedVsReceivedService, Table2>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductionIssueEstimatedVsReceivedHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<Table2> Handle(GetProductionIssueEstimatedVsReceivedService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionIssueRepository.GetProductionIssueEstimatedVsReceived(request.ProductionIssue);
            unitOfWork.Commit();
            return result;
        }
    }

    public class AddProductionIssueReconciliationHandler : IRequestHandler<AddProductionIssueReconciliationService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddProductionIssueReconciliationHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddProductionIssueReconciliationService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionIssueRepository.AddProductionIssueReconciliation(request.ProductionIssue);
            unitOfWork.Commit();
            return result;
        }
    }

}
