﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.ProductionIssue
{
    public class AddProductionIssueService : IRequest<int>
    {
        public string ProductionIssue { get; set; }
    }
    public class CancelProductionIssueService : IRequest<bool>
    {
        public string ProductionIssue { get; set; }
    }
    public class GetProductionIssueService : IRequest<string>
    {
        public string ProductionIssue { get; set; }
    }
    public class FetchProductionIssueService : IRequest<Table2>
    {
        public string ProductionIssue { get; set; }
    }
    public class GetProductionIssueReconciliationService : IRequest<string>
    {
        public string ProductionIssue { get; set; }
    }
    public class GetProductionIssueEstimatedVsReceivedService : IRequest<Table2>
    {
        public string ProductionIssue { get; set; }
    }
    public class AddProductionIssueReconciliationService : IRequest<int>
    {
        public string ProductionIssue { get; set; }
    }
}
