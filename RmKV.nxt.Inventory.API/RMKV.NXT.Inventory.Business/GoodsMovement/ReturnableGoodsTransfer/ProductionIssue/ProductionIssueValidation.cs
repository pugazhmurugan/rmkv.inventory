﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.ProductionIssue
{
    public class AddProductionIssueValidation : AbstractValidator<AddProductionIssueService>
    {

    }
    public class CancelProductionIssueValidation : AbstractValidator<CancelProductionIssueService>
    {

    }
    public class FetchProductionIssueValidations : AbstractValidator<FetchProductionIssueService>
    {

    }
    public class GetProductionIssueValidations : AbstractValidator<GetProductionIssueService>
    {

    }
    public class GetProductionIssueReconciliationValidations : AbstractValidator<GetProductionIssueReconciliationService>
    {

    }
    public class GetProductionIssueEstimatedVsReceivedValidations : AbstractValidator<GetProductionIssueEstimatedVsReceivedService>
    {

    }
    public class AddProductionIssueReconciliationValidation : AbstractValidator<AddProductionIssueReconciliationService>
    {

    }
}
