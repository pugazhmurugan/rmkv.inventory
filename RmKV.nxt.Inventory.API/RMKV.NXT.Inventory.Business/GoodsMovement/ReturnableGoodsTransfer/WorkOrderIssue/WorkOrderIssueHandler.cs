﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.ReturnableGoodsTransfer.WorkOrderIssue;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.WorkOrderIssue
{
    public class GetWorkOrderIssueHandler : IRequestHandler<GetWorkOrderIssueService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetWorkOrderIssueHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetWorkOrderIssueService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WorkOrderIssueRepository.GetWorkOrderIssue(request.WorkOrderIssue);
            return result;
        }
    }

    public class GetWorkorderIssueTransferTypeHandler : IRequestHandler<GetWorkorderIssueTransferTypeService, List<GetWorkOrderIssuetransferTypeEntity>>
    {
        private readonly IUnitofwork unitOfWork;
        public GetWorkorderIssueTransferTypeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<GetWorkOrderIssuetransferTypeEntity>> Handle(GetWorkorderIssueTransferTypeService request, CancellationToken cancellationToken)
        {
            var response = await unitOfWork.WorkOrderIssueRepository.GetTransferTypeList();
            return response;
        }

    }

    public class GetWorkOrderIssueEntryGridlistHandler : IRequestHandler<GetWorkOrderIssueEntryGridlistService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetWorkOrderIssueEntryGridlistHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetWorkOrderIssueEntryGridlistService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WorkOrderIssueRepository.GetWorkOrderIssueEntryGridList(request.WorkOrderIssue);
            return result;
        }
    }

    public class GetWorkOrderIssueDetailsListHandler : IRequestHandler<GetWorkOrderIssueDetailsListService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetWorkOrderIssueDetailsListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetWorkOrderIssueDetailsListService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WorkOrderIssueRepository.GetWorkOrderIssueDetails(request.WorkOrderIssue);
            return result;
        }
    }

    public class SaveWorkOrderIssueHandler : IRequestHandler<AddWorkOrderIssueService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public SaveWorkOrderIssueHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddWorkOrderIssueService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WorkOrderIssueRepository.AddWorkOrderIssue(request.WorkOrderIssue);
            unitOfWork.Commit();
            return result;
        }
    }

    public class FetchWorkOrderIssueHandler : IRequestHandler<FetchWorkOrderIssueService, Table4>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchWorkOrderIssueHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<Table4> Handle(FetchWorkOrderIssueService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WorkOrderIssueRepository.FetchWorkOrderIssue(request.WorkOrderIssue);
            unitOfWork.Commit();
            return result;
        }
    }


}
