﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.WorkOrderIssue
{
    public class GetWorkOrderIssueValidation : AbstractValidator<GetWorkOrderIssueService>
    {
    }

    public class GetWorkOrderIssueDetailsListValidation : AbstractValidator<GetWorkOrderIssueDetailsListService>
    {
    }

    public class AddWorkOrderIssueValidation : AbstractValidator<AddWorkOrderIssueService>
    {
    }

    public class FetchWorkOrderIssueValidation : AbstractValidator<FetchWorkOrderIssueService>
    {
    }

    public class GetWorkOrderIssueEntryGridlistValidation : AbstractValidator<GetWorkOrderIssueEntryGridlistService>
    {
    }

    public class GetWorkorderIssueTransferTypeValidation : AbstractValidator<GetWorkorderIssueTransferTypeService>
    {
    }
}
