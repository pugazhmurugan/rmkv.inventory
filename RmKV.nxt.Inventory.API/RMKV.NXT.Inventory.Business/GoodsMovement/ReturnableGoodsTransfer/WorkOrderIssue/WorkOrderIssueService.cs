﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.ReturnableGoodsTransfer.WorkOrderIssue;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.WorkOrderIssue
{
    public class GetWorkOrderIssueService : IRequest<string>
    {
        public string WorkOrderIssue { get; set; }
    }

    public class GetWorkOrderIssueEntryGridlistService : IRequest<string>
    {
        public string WorkOrderIssue { get; set; }
    }

    public class GetWorkOrderIssueDetailsListService : IRequest<string>
    {
        public string WorkOrderIssue { get; set; }
    }

    public class AddWorkOrderIssueService : IRequest<bool>
    {
        public string WorkOrderIssue { get; set; }
    }

    public class FetchWorkOrderIssueService : IRequest<Table4>
    {
        public string WorkOrderIssue { get; set; }
    }

    public class GetWorkorderIssueTransferTypeService : IRequest<List<GetWorkOrderIssuetransferTypeEntity>>
    {

    }
}
