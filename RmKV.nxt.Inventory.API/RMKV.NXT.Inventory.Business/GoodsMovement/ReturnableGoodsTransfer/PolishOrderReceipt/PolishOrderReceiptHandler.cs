﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.PolishOrderReceipt
{
    public class GetPolishOrderReceiptsHandler : IRequestHandler<GetPolishOrderReceiptService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPolishOrderReceiptsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetPolishOrderReceiptService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PolishOrderReceiptRepository.GetPolishOrderReceipts(request.PolishOrderReceipt);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetPendingPolishIssueNoHandler : IRequestHandler<GetPendingPolishIssueNoService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPendingPolishIssueNoHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetPendingPolishIssueNoService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PolishOrderReceiptRepository.GetPendingPolishIssueNos(request.PolishOrderReceipt);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetPendingPolishIssueDetailHandler : IRequestHandler<GetPendingPolishIssueDetailService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPendingPolishIssueDetailHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetPendingPolishIssueDetailService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PolishOrderReceiptRepository.GetPendingPolishIssueDetails(request.PolishOrderReceipt);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class AddPolishOrderReceiptHandler : IRequestHandler<AddPolishOrderReceiptService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddPolishOrderReceiptHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddPolishOrderReceiptService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PolishOrderReceiptRepository.AddPolishOrderReceipt(request.PolishOrderReceipt);
            unitOfWork.Commit();
            return result;
        }
    }
    public class RemovePolishOrderReceiptHandler : IRequestHandler<RemovePolishOrderReceiptService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public RemovePolishOrderReceiptHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(RemovePolishOrderReceiptService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PolishOrderReceiptRepository.RemovePolishOrderReceipt(request.PolishOrderReceipt);
            unitOfWork.Commit();
            return result;
        }
    }
    public class FetchPolishOrderReceiptHandler : IRequestHandler<FetchPolishOrderReceiptDetailService, Table1>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchPolishOrderReceiptHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<Table1> Handle(FetchPolishOrderReceiptDetailService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PolishOrderReceiptRepository.FetchPolishOrderReceiptDetails(request.FetchPolishOrderReceipt);
            unitOfWork.Commit();
            return result;
        }
    }
}
