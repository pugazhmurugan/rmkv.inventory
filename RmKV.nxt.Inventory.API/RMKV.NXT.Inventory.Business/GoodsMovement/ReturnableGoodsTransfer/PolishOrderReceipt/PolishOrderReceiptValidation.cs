﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.PolishOrderReceipt
{
    public class GetPolishOrderReceiptValidation : AbstractValidator<GetPolishOrderReceiptService>
    {

    }
    public class GetPendingPolishIssueNoValidation : AbstractValidator<GetPendingPolishIssueNoService>
    {

    }
    public class GetPendingPolishIssueDetailValidation : AbstractValidator<GetPendingPolishIssueDetailService>
    {

    }
    public class AddPolishOrderReceiptValidation : AbstractValidator<AddPolishOrderReceiptService>
    {

    }
    public class RemovePolishOrderReceiptValidation : AbstractValidator<RemovePolishOrderReceiptService>
    {

    }
    public class FetchPolishOrderReceiptValidation : AbstractValidator<FetchPolishOrderReceiptDetailService>
    {

    }
}
