﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.PolishOrderReceipt
{
    public class GetPolishOrderReceiptService : IRequest<string>
    {
        public string PolishOrderReceipt { get; set; }
    }
    public class GetPendingPolishIssueNoService : IRequest<string>
    {
        public string PolishOrderReceipt { get; set; }
    }
    public class GetPendingPolishIssueDetailService : IRequest<string>
    {
        public string PolishOrderReceipt { get; set; }
    }
    public class AddPolishOrderReceiptService: IRequest<bool>
    {
        public string PolishOrderReceipt { get; set; }
    }
    public class RemovePolishOrderReceiptService: IRequest<bool>
    {
        public string PolishOrderReceipt { get; set; }
    }
    public class FetchPolishOrderReceiptDetailService : IRequest<Table1>
    {
        public string FetchPolishOrderReceipt { get; set; }
    }
}
