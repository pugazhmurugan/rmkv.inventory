﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.PolishOrderIssue
{
    public class GetPolishOrderIssueService : IRequest<string>
    {
        public string PolishOrderIssue { get; set; }
    }

    public class AddPolishOrderIssueService : IRequest<bool>
    {
        public string PolishOrderIssue { get; set; }
    }

    public class FetchPolishOrderIssueService : IRequest<Table4>
    {
        public string PolishOrderIssue { get; set; }
    }

    public class DeletePolishOrderIssueService : IRequest<bool>
    {
        public string PolishOrderIssue { get; set; }
    }


}
