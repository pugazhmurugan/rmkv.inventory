﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.PolishOrderIssue
{
    public class GetPolishOrderIssueHandler : IRequestHandler<GetPolishOrderIssueService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPolishOrderIssueHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetPolishOrderIssueService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PolishOrderIssueReppository.GetPolishOrderList(request.PolishOrderIssue);
            return result;
        }
    }

    public class AddPolishOrderIssueHandler : IRequestHandler<AddPolishOrderIssueService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddPolishOrderIssueHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddPolishOrderIssueService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PolishOrderIssueReppository.AddPolishOrder(request.PolishOrderIssue);
            unitOfWork.Commit();
            return result;
        }
    }

    public class FetchPolishOrderIssueHandler : IRequestHandler<FetchPolishOrderIssueService, Table4>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchPolishOrderIssueHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<Table4> Handle(FetchPolishOrderIssueService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PolishOrderIssueReppository.FetchPolishOrder(request.PolishOrderIssue);
            unitOfWork.Commit();
            return result;
        }
    }

    public class DeletePolishOrderIssueHandler : IRequestHandler<DeletePolishOrderIssueService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public DeletePolishOrderIssueHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(DeletePolishOrderIssueService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PolishOrderIssueReppository.DeletePolishOrder(request.PolishOrderIssue);
            unitOfWork.Commit();
            return result;
        }
    }
}
