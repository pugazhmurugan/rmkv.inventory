﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.PolishOrderIssue
{

    public class GetPolishOrderIssueValidation : AbstractValidator<GetPolishOrderIssueService>
    {
    }

    public class AddPolishOrderIssueValidation : AbstractValidator<AddPolishOrderIssueService>
    {
    }

    public class FetchPolishOrderIssueValidation : AbstractValidator<FetchPolishOrderIssueService>
    {
    }

    public class DeletePolishOrderIssueValidation : AbstractValidator<DeletePolishOrderIssueService>
    {
    }
}
