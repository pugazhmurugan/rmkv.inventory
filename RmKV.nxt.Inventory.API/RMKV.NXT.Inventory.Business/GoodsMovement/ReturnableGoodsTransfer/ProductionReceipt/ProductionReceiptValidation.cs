﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer
{
    public class GetProductionReceiptValidation : AbstractValidator<GetProductionReceiptService>
    {

    }
    public class GetProductionIssueNoValidation : AbstractValidator<GetProductionIssueNoService>
    {

    }
    public class GetProductionIssueDetailValidation : AbstractValidator<GetProductionIssueDetailService>
    {

    }
    public class GetProductionReceiptProductLookupValidation : AbstractValidator<GetProductionReceiptProductLookupService>
    {

    }
    public class AddProductionReceiptValidation : AbstractValidator<AddProductionReceiptService>
    {

    }
    public class FetchProductionReceiptValidation : AbstractValidator<FetchProductionReceiptService>
    {

    }
    public class RemoveProductionReceiptValidation : AbstractValidator<RemoveProductionReceiptService>
    {

    }
}
