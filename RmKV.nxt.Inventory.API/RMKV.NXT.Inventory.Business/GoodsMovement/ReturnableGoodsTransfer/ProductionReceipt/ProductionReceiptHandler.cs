﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer
{
    public class GetProductionReceiptsHandler : IRequestHandler<GetProductionReceiptService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductionReceiptsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetProductionReceiptService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionReceiptRepository.GetProductionReceipts(request.ProductionReceipt);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetProductionIssueNoHandler : IRequestHandler<GetProductionIssueNoService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductionIssueNoHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetProductionIssueNoService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionReceiptRepository.GetProductionIssueNos(request.ProductionReceipt);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetProductionIssueDetailHandler : IRequestHandler<GetProductionIssueDetailService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductionIssueDetailHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetProductionIssueDetailService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionReceiptRepository.GetProductionIssueDetails(request.ProductionReceipt);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetProductionReceiptProductLookupHandler : IRequestHandler<GetProductionReceiptProductLookupService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductionReceiptProductLookupHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetProductionReceiptProductLookupService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionReceiptRepository.GetProductionReceiptProductLookup(request.ProductionReceipt);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class AddProductionReceiptHandler : IRequestHandler<AddProductionReceiptService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddProductionReceiptHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddProductionReceiptService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionReceiptRepository.AddProductionReceipt(request.ProductionReceipt);
            unitOfWork.Commit();
            return result;
        }
    }
    public class FetchProductionReceiptHandler : IRequestHandler<FetchProductionReceiptService, Table2>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchProductionReceiptHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<Table2> Handle(FetchProductionReceiptService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionReceiptRepository.FetchProductionReceipt(request.ProductionReceipt);
            unitOfWork.Commit();
            return result;
        }
    }
    public class RemoveProductionReceiptHandler : IRequestHandler<RemoveProductionReceiptService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public RemoveProductionReceiptHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(RemoveProductionReceiptService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionReceiptRepository.RemoveProductionReceipt(request.ProductionReceipt);
            unitOfWork.Commit();
            return result;
        }
    }
}
