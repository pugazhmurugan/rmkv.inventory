﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer
{
    public class GetProductionReceiptService : IRequest<string>
    {
        public string ProductionReceipt { get; set; }
    }
    public class GetProductionIssueNoService: IRequest<string>
    {
        public string ProductionReceipt { get; set; }
    }
    public class GetProductionIssueDetailService: IRequest<string>
    {
        public string ProductionReceipt { get; set; }
    }
    public class GetProductionReceiptProductLookupService : IRequest<string>
    {
        public string ProductionReceipt { get; set; }
    }
    public class AddProductionReceiptService: IRequest<bool>
    {
        public string ProductionReceipt { get; set; }
    }
    public class FetchProductionReceiptService: IRequest<Table2>
    {
        public string ProductionReceipt { get; set; }
    }
    public class RemoveProductionReceiptService : IRequest<bool>
    {
        public string ProductionReceipt { get; set; }
    }
}
