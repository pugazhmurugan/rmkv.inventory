﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.WorkOrderReceipt
{
   public class WorkOrderReceiptValidation:AbstractValidator<GetWorkOrderReceiptListServices>
    {
    }
    public class WorkOrderReceiptListPendingValidation : AbstractValidator<GetWorkOrderReceiptListPendingServices>
    {
    }
    public class AddWorkOrderReceiptListValidation : AbstractValidator<AddWorkOrderReceiptListPendingServices>
    {
    }
    public class GetWorkOrderReceiptPendindSupplierServicesValidation: AbstractValidator<GetWorkOrderReceiptPendindSupplierServices>
    {
    }
    public class GetWorkOrderReceiptPendindIsssueDetailsValidation : AbstractValidator<GetWorkOrderReceiptPendindIsssueDetailsServices>
    {
    }
    public class FetchWorkOrderReceiptValidation : AbstractValidator<FetchWorkOrderReceiptServices>
    {
    }

}
