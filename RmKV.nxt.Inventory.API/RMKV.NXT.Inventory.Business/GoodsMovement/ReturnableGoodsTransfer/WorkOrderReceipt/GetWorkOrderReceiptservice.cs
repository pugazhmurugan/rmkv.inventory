﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.WorkOrderReceipt
{
     public class GetWorkOrderReceiptListServices : IRequest<string>
    {
        public string WorkOrderIssue { get; set; }
    }
    public class GetWorkOrderReceiptListPendingServices : IRequest<string>
    {
        public string WorkOrderIssue { get; set; }
    }
    public class AddWorkOrderReceiptListPendingServices : IRequest<string>
    {
        public string WorkOrderIssue { get; set; }
    }
    public class GetWorkOrderReceiptPendindSupplierServices : IRequest<string>
    {
        public string WorkOrderIssue { get; set; }
    }
    public class GetWorkOrderReceiptPendindIsssueDetailsServices : IRequest<string>
    {
        public string WorkOrderIssue { get; set; }
    }
    public class FetchWorkOrderReceiptServices : IRequest<string>
    {
        public string WorkOrderIssue { get; set; }
    }
}
