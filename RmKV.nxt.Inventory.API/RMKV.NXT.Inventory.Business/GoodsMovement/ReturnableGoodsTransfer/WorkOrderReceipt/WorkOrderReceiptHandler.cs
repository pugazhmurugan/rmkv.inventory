﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.ReturnableGoodsTransfer.WorkOrderReceipt
{
    public class WorkOrderReceiptListHandler : IRequestHandler<GetWorkOrderReceiptListServices, string>
    {
        private readonly IUnitofwork unitOfWork;

        public WorkOrderReceiptListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWorkOrderReceiptListServices request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WorkOrderIssueRepository.GetWorkOrderReceiptList(request.WorkOrderIssue);
            unitOfWork.Commit();
            return result;
        }
 
    }
    public class WorkOrderReceiptListpendingHandler : IRequestHandler<GetWorkOrderReceiptListPendingServices, string>
    {
        private readonly IUnitofwork unitOfWork;

        public WorkOrderReceiptListpendingHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWorkOrderReceiptListPendingServices request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WorkOrderIssueRepository.GetWorkOrderpendingList(request.WorkOrderIssue);
            unitOfWork.Commit();
            return result;
        }
    }
    public class AddWorkOrderReceiptListHandler : IRequestHandler<AddWorkOrderReceiptListPendingServices, string>
    {
        private readonly IUnitofwork unitOfWork;

        public AddWorkOrderReceiptListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(AddWorkOrderReceiptListPendingServices request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WorkOrderIssueRepository.AddWorkOrderpendingList(request.WorkOrderIssue);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetWorkOrderReceiptPendindSupplierHandler : IRequestHandler<GetWorkOrderReceiptPendindSupplierServices, string>
    {
        private readonly IUnitofwork unitOfWork;

        public GetWorkOrderReceiptPendindSupplierHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWorkOrderReceiptPendindSupplierServices request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WorkOrderIssueRepository.GetWorkOrderPendindSupplier(request.WorkOrderIssue);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetWorkOrderReceiptPendindIsssueDetailsHandler : IRequestHandler<GetWorkOrderReceiptPendindIsssueDetailsServices, string>
    {
        private readonly IUnitofwork unitOfWork;

        public GetWorkOrderReceiptPendindIsssueDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWorkOrderReceiptPendindIsssueDetailsServices request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WorkOrderIssueRepository.GetWorkOrderPendindListDetails(request.WorkOrderIssue);
            unitOfWork.Commit();
            return result;
        }

    }
    public class FetchWorkOrderReceiptHandler : IRequestHandler<FetchWorkOrderReceiptServices, string>
    {
        private readonly IUnitofwork unitOfWork;

        public FetchWorkOrderReceiptHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(FetchWorkOrderReceiptServices request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WorkOrderIssueRepository.FetchWorkOrderReceipt(request.WorkOrderIssue);
            unitOfWork.Commit();
            return result;
        }

    }
}
