﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.CountertoGodownAcknowledgement
{
    public class LoadCtoGHandler : IRequestHandler<LoadCtoGService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public LoadCtoGHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(LoadCtoGService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CountertoGodownAcknowledgementRepository.LoadCtoGDetails(request.LoadAcknowledgement);
            unitOfWork.Commit();
            return result;
        }
    }

    public class LoadTransferNoBasedDataCHandler : IRequestHandler<LoadCtoGTransferNoBasedDataService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public LoadTransferNoBasedDataCHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(LoadCtoGTransferNoBasedDataService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CountertoGodownAcknowledgementRepository.LoadTransferNoBasedData(request.LoadByNoDetails);
            unitOfWork.Commit();
            return result;
        }
    }

    public class AddCtoGAcknowledgementHandler : IRequestHandler<AddCtoGAcknowledgementService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddCtoGAcknowledgementHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddCtoGAcknowledgementService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CountertoGodownAcknowledgementRepository.SaveCtoGAcknowledgementDetails(request.AddAcknowledgement);
            unitOfWork.Commit();
            return result;
        }
    }

    public class FetchCtoGAckCHandler : IRequestHandler<FetchCtoGAcknowledgementService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchCtoGAckCHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(FetchCtoGAcknowledgementService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CountertoGodownAcknowledgementRepository.FetchCtoGAcknowledgement(request.FetchCtoGAck);
            unitOfWork.Commit();
            return result;
        }
    }

    public class CheckCtoGAckTransferHandler : IRequestHandler<CheckCtoGAckTransferNoService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CheckCtoGAckTransferHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(CheckCtoGAckTransferNoService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CountertoGodownAcknowledgementRepository.CheckCtoGAcknowledgementTransferNo(request.CheckCtoGTransferNo);
            unitOfWork.Commit();
            return result;
        }
    }
}
