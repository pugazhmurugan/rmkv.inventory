﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.CountertoGodownAcknowledgement
{
    public class LoadCtoGService : IRequest<string>
    {
        public string LoadAcknowledgement { get; set; }
    }

    public class LoadCtoGTransferNoBasedDataService : IRequest<string>
    {
        public string LoadByNoDetails { get; set; }
    }

    public class AddCtoGAcknowledgementService : IRequest<int>
    {
        public string AddAcknowledgement { get; set; }
    }

    public class FetchCtoGAcknowledgementService : IRequest<string>
    {
        public string FetchCtoGAck { get; set; }
    }

    public class CheckCtoGAckTransferNoService : IRequest<bool>
    {
        public string CheckCtoGTransferNo { get; set; }
    }
}
