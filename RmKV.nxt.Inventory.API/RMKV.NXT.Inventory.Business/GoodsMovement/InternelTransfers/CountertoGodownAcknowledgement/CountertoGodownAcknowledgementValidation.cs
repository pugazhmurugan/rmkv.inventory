﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.CountertoGodownAcknowledgement
{
    public class LoadCtoGAcknowledgementValidation : AbstractValidator<LoadCtoGService>
    {

    }

    public class LoadCtoGTransferNoBasedDataValidation : AbstractValidator<LoadCtoGTransferNoBasedDataService>
    {

    }

    public class AddCtoGAcknowledgementValidation : AbstractValidator<AddCtoGAcknowledgementService>
    {

    }

    public class FetchCtoGAcknowledgementValidation : AbstractValidator<FetchCtoGAcknowledgementService>
    {

    }

    public class CheckCtoGAckTransferValidation : AbstractValidator<CheckCtoGAckTransferNoService>
    {

    }
}
