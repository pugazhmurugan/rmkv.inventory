﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.GodowntoCounterAcknowledgement
{
    public class LoadAcknowledgementValidation : AbstractValidator<LoadGtoCACKService>
    {

    }

    public class LoadTransferNoBasedDataValidation : AbstractValidator<LoadTransferNoBasedDataService>
    {

    }

    public class AddGtoCAcknowledgementValidation : AbstractValidator<AddGtoCAcknowledgementService>
    {

    }

    public class FetchGtoCAcknowledgementValidation : AbstractValidator<FetchGtoCAcknowledgementService>
    {

    }

    public class CheckGtoCAckTransferValidation : AbstractValidator<CheckGtoCAckTransferNoService>
    {

    }
}
