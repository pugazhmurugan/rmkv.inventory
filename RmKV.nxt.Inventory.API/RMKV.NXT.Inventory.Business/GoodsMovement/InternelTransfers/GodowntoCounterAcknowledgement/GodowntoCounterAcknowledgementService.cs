﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.GodowntoCounterAcknowledgement
{
    public class LoadGtoCACKService : IRequest<string>
    {
        public string LoadAcknowledgement { get; set; }
    }

    public class LoadTransferNoBasedDataService : IRequest<string>
    {
        public string LoadByNoDetails { get; set; }
    }

    public class AddGtoCAcknowledgementService : IRequest<int>
    {
        public string AddAcknowledgement { get; set; }
    }

    public class FetchGtoCAcknowledgementService : IRequest<string>
    {
        public string FetchGtoCAck { get; set; }
    }

    public class CheckGtoCAckTransferNoService : IRequest<bool>
    {
        public string CheckTransferNo { get; set; }
    }
}
