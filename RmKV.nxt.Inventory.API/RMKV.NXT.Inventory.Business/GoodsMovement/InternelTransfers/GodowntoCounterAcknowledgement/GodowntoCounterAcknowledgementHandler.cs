﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.GodowntoCounterAcknowledgement
{
    public class LoadGtoCHandler : IRequestHandler<LoadGtoCACKService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public LoadGtoCHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(LoadGtoCACKService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GodowntoCounterAcknowledgementRepository.LoadGtoCDetails(request.LoadAcknowledgement);
            unitOfWork.Commit();
            return result;
        }
    }

    public class LoadTransferNoBasedDataCHandler : IRequestHandler<LoadTransferNoBasedDataService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public LoadTransferNoBasedDataCHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(LoadTransferNoBasedDataService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GodowntoCounterAcknowledgementRepository.LoadTransferNoBasedData(request.LoadByNoDetails);
            unitOfWork.Commit();
            return result;
        }
    }

    public class AddGtoCAcknowledgementHandler : IRequestHandler<AddGtoCAcknowledgementService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddGtoCAcknowledgementHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddGtoCAcknowledgementService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GodowntoCounterAcknowledgementRepository.SaveGtoCAcknowledgementDetails(request.AddAcknowledgement);
            unitOfWork.Commit();
            return result;
        }
    }

    public class FetchGtoCAckCHandler : IRequestHandler<FetchGtoCAcknowledgementService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchGtoCAckCHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(FetchGtoCAcknowledgementService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GodowntoCounterAcknowledgementRepository.FetchGtoCAcknowledgement(request.FetchGtoCAck);
            unitOfWork.Commit();
            return result;
        }
    }

    public class CheckGtoCAckTransferHandler : IRequestHandler<CheckGtoCAckTransferNoService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CheckGtoCAckTransferHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(CheckGtoCAckTransferNoService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GodowntoCounterAcknowledgementRepository.CheckGtoCAcknowledgementTransferNo(request.CheckTransferNo);
            unitOfWork.Commit();
            return result;
        }
    }
}
