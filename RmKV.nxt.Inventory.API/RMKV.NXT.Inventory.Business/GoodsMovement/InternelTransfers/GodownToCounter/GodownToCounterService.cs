﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers
{
    public class AddGodownToCounterService : IRequest<int>
    {
        public string GoodCounter { get; set; }
    }
    public class ApproveGodownToCounterService : IRequest<bool>
    {
        public string GoodCounter { get; set; }
    }
    public class CancelGodownToCounterService : IRequest<bool>
    {
        public string GoodCounter { get; set; }
    }
    public class GetGodownToCounterService : IRequest<string>
    {
        public string GoodCounter { get; set; }
    }
    public class GetGodownToLocationsService : IRequest<string>
    {
        public string GoodCounter { get; set; }
    }
    public class GetGodownTransferNoService : IRequest<int>
    {
        public string GoodCounter { get; set; }
    }
    public class FetchGodownToCounterService : IRequest<string>
    {
        public string GoodCounter { get; set; }
    }
    ///report
    public class GetGodownToCounterReportServices : IRequest<DataSet>
    {
        public string GodownToCounterReport { get; set; }
    }

    public class CheckGodownToCounterReportServices : IRequest<bool>
    {
        public string GodownToCounterReport { get; set; }
    }
}


