﻿using MediatR;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers
{
    public class AddGodownToCounterHandler : IRequestHandler<AddGodownToCounterService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddGodownToCounterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddGodownToCounterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GodownToCounterRepository.AddGodownToCounter(request.GoodCounter);
            unitOfWork.Commit();
            return result;
        }
    }
    public class ApproveGodownToCounterHandler : IRequestHandler<ApproveGodownToCounterService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public ApproveGodownToCounterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(ApproveGodownToCounterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GodownToCounterRepository.ApproveGodownToCounter(request.GoodCounter);
            unitOfWork.Commit();
            return result;
        }

    }
    public class CancelGodownToCounterHandler : IRequestHandler<CancelGodownToCounterService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CancelGodownToCounterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(CancelGodownToCounterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GodownToCounterRepository.CancelGodownToCounter(request.GoodCounter);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetToLocationHandler : IRequestHandler<GetGodownToLocationsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetToLocationHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetGodownToLocationsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GodownToCounterRepository.GetToLocation(request.GoodCounter);
            unitOfWork.Commit();
            return result;
        }

    }


    public class GetGodownToCounterHandler : IRequestHandler<GetGodownToCounterService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetGodownToCounterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetGodownToCounterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GodownToCounterRepository.GetGodownToCounter(request.GoodCounter);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetTransferNoHandler : IRequestHandler<GetGodownTransferNoService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public GetTransferNoHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(GetGodownTransferNoService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GodownToCounterRepository.GetTransferNo(request.GoodCounter);
            unitOfWork.Commit();
            return result;
        }

    }
}
public class FetchGodownToCounterHandler : IRequestHandler<FetchGodownToCounterService, string>
{
    private readonly IUnitofwork unitOfWork;
    public FetchGodownToCounterHandler(IUnitofwork unitOfWork)
    {
        this.unitOfWork = unitOfWork;
    }

    public async Task<string> Handle(FetchGodownToCounterService request, CancellationToken cancellationToken)
    {
        var result = await unitOfWork.GodownToCounterRepository.FetchGodownToCounter(request.GoodCounter);
        unitOfWork.Commit();
        return result;
    }

}


//report

public class GetGodownToCounterReportHandler : IRequestHandler<GetGodownToCounterReportServices, DataSet>
{
    private readonly IUnitofwork unitOfWork;

    public GetGodownToCounterReportHandler(IUnitofwork unitOfWork)
    {
        this.unitOfWork = unitOfWork;
    }

    public async Task<DataSet> Handle(GetGodownToCounterReportServices request, CancellationToken cancellationToken)
    {
        var result = await unitOfWork.GodownToCounterRepository.GetGodownToCounterReport(request.GodownToCounterReport);
        this.unitOfWork.Commit();
        return result;
    }

  
}
public class CheckGodownToCounterReportHandler : IRequestHandler<CheckGodownToCounterReportServices, bool>
{
    private readonly IUnitofwork unitOfWork;
    public CheckGodownToCounterReportHandler(IUnitofwork unitOfWork)
    {
        this.unitOfWork = unitOfWork;
    }

    public async Task<bool> Handle(CheckGodownToCounterReportServices request, CancellationToken cancellationToken)
    {
        var result = await unitOfWork.GodownToCounterRepository.CheckGodownToCounterReport(request.GodownToCounterReport);
        unitOfWork.Commit();
        return result;
    }

}

