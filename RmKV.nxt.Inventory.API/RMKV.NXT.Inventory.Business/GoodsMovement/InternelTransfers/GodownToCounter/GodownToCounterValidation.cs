﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers
{
    public class AddGodownToCounterValidation : AbstractValidator<AddGodownToCounterService>
    {

    }
    public class ApproveGodownToCounterValidation : AbstractValidator<ApproveGodownToCounterService>
    {

    }
    public class CancelGodownToCounterValidation : AbstractValidator<CancelGodownToCounterService>
    {

    }
    public class GetGodownToLocationsValidation : AbstractValidator<GetGodownToLocationsService>
    {

    }
    public class GetGodownTransferNoValidations : AbstractValidator<GetGodownTransferNoService>
    {

    }
    public class FetchGodownToCounterValidations : AbstractValidator<FetchGodownToCounterService>
    {

    }
    public class GetGodownToCounterValidations : AbstractValidator<GetGodownToCounterService>
    {

    }
    //report
    public class GetGodownToCounterReportValidations : AbstractValidator<GetGodownToCounterReportServices>
    {

    }
    public class CheckGodownToCounterReportValidations : AbstractValidator<CheckGodownToCounterReportServices>
    {

    }
}
