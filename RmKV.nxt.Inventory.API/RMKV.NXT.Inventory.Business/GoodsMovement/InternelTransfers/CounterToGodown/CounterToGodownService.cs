﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.CounterToGodown
{
    public class AddCounterToGodownService : IRequest<int>
    {
        public string GoodCounter { get; set; }
    }
    public class ApproveCounterToGodownService : IRequest<bool>
    {
        public string GoodCounter { get; set; }
    }
    public class CancelCounterToGodownService : IRequest<bool>
    {
        public string GoodCounter { get; set; }
    }
    public class GetCounterToGodownService : IRequest<string>
    {
        public string GoodCounter { get; set; }
    }
    public class GetToWarehouseService : IRequest<string>
    {
        public string GoodCounter { get; set; }
    }
    public class GetCounterTransferNoService : IRequest<int>
    {
        public string GoodCounter { get; set; }
    }
    public class FetchCounterToGodownService : IRequest<string>
    {
        public string GoodCounter { get; set; }
    }

    public class GetCounterToGodownReportService : IRequest<DataSet>
    {
        public string GoodCounter { get; set; }
    }
    public class CheckCounterToGodownReportService : IRequest<int>
    {
        public string GoodCounter { get; set; }
    }
}