﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.CounterToGodown
{
    public class AddCounterToGodownValidation : AbstractValidator<AddCounterToGodownService>
    {

    }
    public class ApproveCounterToGodownValidation : AbstractValidator<ApproveCounterToGodownService>
    {

    }
    public class CancelCounterToGodownValidation : AbstractValidator<CancelCounterToGodownService>
    {

    }
    public class GetToWarehouseValidation : AbstractValidator<GetToWarehouseService>
    {

    }
    public class GetCounterTransferNoValidations : AbstractValidator<GetCounterTransferNoService>
    {

    }
    public class FetchCounterToGodownValidations : AbstractValidator<FetchCounterToGodownService>
    {

    }
    public class GetCounterToGodownValidations : AbstractValidator<GetCounterToGodownService>
    {

    }

    public class GetCounterToGodownReportValidations : AbstractValidator<GetCounterToGodownReportService>
    {

    }

    public class CheckCounterToGodownReportValidations : AbstractValidator<CheckCounterToGodownReportService>
    {

    }

}