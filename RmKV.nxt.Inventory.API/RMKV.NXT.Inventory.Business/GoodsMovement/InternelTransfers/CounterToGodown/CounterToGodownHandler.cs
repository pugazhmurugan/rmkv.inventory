﻿using MediatR;
using RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.CounterToGodown;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsTransfers.CounterToGodown
{
    public class AddCounterToGodownHandler : IRequestHandler<AddCounterToGodownService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddCounterToGodownHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddCounterToGodownService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CounterToGodownRepository.AddCounterToGodown(request.GoodCounter);
            unitOfWork.Commit();
            return result;
        }

    }
    public class ApproveCounterToGodownHandler : IRequestHandler<ApproveCounterToGodownService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public ApproveCounterToGodownHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(ApproveCounterToGodownService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CounterToGodownRepository.ApproveCounterToGodown(request.GoodCounter);
            unitOfWork.Commit();
            return result;
        }

    }
    public class CancelCounterToGodownHandler : IRequestHandler<CancelCounterToGodownService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CancelCounterToGodownHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(CancelCounterToGodownService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CounterToGodownRepository.CancelCounterToGodown(request.GoodCounter);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetToWarehouseHandler : IRequestHandler<GetToWarehouseService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetToWarehouseHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetToWarehouseService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CounterToGodownRepository.GetToWarehouse(request.GoodCounter);
            unitOfWork.Commit();
            return result;
        }

    }


    public class GetCounterToGodownHandler : IRequestHandler<GetCounterToGodownService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetCounterToGodownHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetCounterToGodownService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CounterToGodownRepository.GetCounterToGodown(request.GoodCounter);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetTransferNoHandler : IRequestHandler<GetCounterTransferNoService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public GetTransferNoHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(GetCounterTransferNoService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.CounterToGodownRepository.GetTransferNo(request.GoodCounter);
            unitOfWork.Commit();
            return result;
        }

    }
}
public class FetchCounterToGodownHandler : IRequestHandler<FetchCounterToGodownService, string>
{
    private readonly IUnitofwork unitOfWork;
    public FetchCounterToGodownHandler(IUnitofwork unitOfWork)
    {
        this.unitOfWork = unitOfWork;
    }

    public async Task<string> Handle(FetchCounterToGodownService request, CancellationToken cancellationToken)
    {
        var result = await unitOfWork.CounterToGodownRepository.FetchCounterToGodown(request.GoodCounter);
        unitOfWork.Commit();
        return result;
    }

}

public class GetCounterToGodownReportHandler : IRequestHandler<GetCounterToGodownReportService, DataSet>
{
    private readonly IUnitofwork unitOfWork;
    public GetCounterToGodownReportHandler(IUnitofwork unitOfWork)
    {
        this.unitOfWork = unitOfWork;
    }

    public async Task<DataSet> Handle(GetCounterToGodownReportService request, CancellationToken cancellationToken)
    {
        var result = await unitOfWork.CounterToGodownRepository.GetCounterToGodownReport(request.GoodCounter);
        return result;
    }

}

public class CheckCounterToGodownReportHandler : IRequestHandler<CheckCounterToGodownReportService, int>
{
    private readonly IUnitofwork unitOfWork;
    public CheckCounterToGodownReportHandler(IUnitofwork unitOfWork)
    {
        this.unitOfWork = unitOfWork;
    }

    public async Task<int> Handle(CheckCounterToGodownReportService request, CancellationToken cancellationToken)
    {
        var result = await unitOfWork.CounterToGodownRepository.CheckCounterToGodownReport(request.GoodCounter);
        unitOfWork.Commit();
        return result;
    }

}