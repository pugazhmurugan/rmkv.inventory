﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.WarehouseGatepassAcknowledgement;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.WarehouseGatePassAcknowledgment
{
    public class SaveWarehouseGatePassServices : IRequest<int>
    {
        public string SaveWarehouseGatePass { get; set; }
    }
    public class LoadWarehouseGatePassServices : IRequest<List<WarehouseGatePassEntity>>
    {
        public string LoadWarehouseGatePass { get; set; }
    }

    public class CancelWarehouseGatePassService : IRequest<bool>
    {
        public string CancelWarehouseGatePass { get; set; }
    }

    public class FetchWarhouseGatePassService : IRequest<List<FetchWarehouseGatePassEntity>>
    {
        public string FetchWarehouseGatePass { get; set; }
    }

    public class GetGatePassService : IRequest<List<GatePassEntity>>
    {
        public string GetGatePass { get; set; }
    }

    public class GetSameWarehouseService : IRequest<List<GetSameWarehouseEntity>>
    {
        public string GetSameWarehouse { get; set; }
    }

    public class GetStoreLocationService : IRequest<List<GetSameWarehouseEntity>>
    {
        public string GetSameWarehouse { get; set; }
    }

    public class GetDesignationService : IRequest<List<GetDesignationEntity>>
    {
        public string GetSameWarehouse { get; set; }
    }

    public class GetWarehouseTransferDetailService : IRequest<List<GetWarehouseTransferDetailEntity>>
    {
        public string GetWarehouseTransferDetail { get; set; }
    }

    public class GetTransferTypeService : IRequest<string>
    {
        public string GetTransferType { get; set; }
    }

    public class GetWarehouseGatePassService : IRequest<string>
    {
        public string GetWarehouseGatePass { get; set; }
    }

    public class GetWarehouseGatePassNoDetailsService : IRequest<string>
    {
        public string GetGatePassNo { get; set; }
    }

    public class GetWarehouseGRNNoListService : IRequest<string>
    {
        public string GetGRNNo { get; set; }
    }

    public class GetWarehouseGatePassDetailsService : IRequest<string>
    {
        public string GetGatePassDetails { get; set; }
    }

    //public class GetSameWarehouseService : IRequest<List<GetSameWarehouseEntity>>
    //{
    //    public string GetSameWarehouse { get; set; }
    //}

    //public class GetStoreLocationService : IRequest<List<GetSameWarehouseEntity>>
    //{
    //    public string GetSameWarehouse { get; set; }
    //}

    //public class GetGatePassTypeService : IRequest<string>
    //{
    //    public string DCEway { get; set; }
    //}
}
