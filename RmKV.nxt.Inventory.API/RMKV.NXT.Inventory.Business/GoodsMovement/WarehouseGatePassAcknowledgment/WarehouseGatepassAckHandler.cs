﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.WarehouseGatepassAcknowledgement;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.WarehouseGatePassAcknowledgment
{
    public class AddWarehouseGatePassHandler : IRequestHandler<SaveWarehouseGatePassServices, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddWarehouseGatePassHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(SaveWarehouseGatePassServices request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.WarehouseGatePassAckRepository.SaveWarehouseGatePass(request.SaveWarehouseGatePass);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetWarehouseGatePassHandler : IRequestHandler<LoadWarehouseGatePassServices, List<WarehouseGatePassEntity>>
    {
        private readonly IUnitofwork unitOfWork;

        public GetWarehouseGatePassHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<WarehouseGatePassEntity>> Handle(LoadWarehouseGatePassServices request, CancellationToken cancellationToken)
        {


            var result = await unitOfWork.WarehouseGatePassAckRepository.GetWarehouseGatePass(request.LoadWarehouseGatePass);
            return result;
        }

    }

    public class GetGatePassHandler : IRequestHandler<GetGatePassService, List<GatePassEntity>>
    {
        private readonly IUnitofwork unitOfWork;

        public GetGatePassHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<GatePassEntity>> Handle(GetGatePassService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.WarehouseGatePassAckRepository.GetGatePass(request.GetGatePass);
            return result;
        }

    }

    public class GetStoreLocationHandler : IRequestHandler<GetStoreLocationService, List<GetSameWarehouseEntity>>
    {
        private readonly IUnitofwork unitOfWork;

        public GetStoreLocationHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<GetSameWarehouseEntity>> Handle(GetStoreLocationService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.WarehouseGatePassAckRepository.GetStoreLocation(request.GetSameWarehouse);
            return result;
        }

    }

    public class GetWarehouseTransferDetailHandler : IRequestHandler<GetWarehouseTransferDetailService, List<GetWarehouseTransferDetailEntity>>
    {
        private readonly IUnitofwork unitOfWork;

        public GetWarehouseTransferDetailHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<GetWarehouseTransferDetailEntity>> Handle(GetWarehouseTransferDetailService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.WarehouseGatePassAckRepository.GetWarehouseTransferDetail(request.GetWarehouseTransferDetail);
            return result;
        }

    }

    public class GetTransferTypeHandler : IRequestHandler<GetTransferTypeService, string>
    {
        private readonly IUnitofwork unitOfWork;

        public GetTransferTypeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetTransferTypeService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.WarehouseGatePassAckRepository.GetTransferTypeDetails(request.GetTransferType);
            return result;
        }

    }

    public class GetWarehouseGatePassListHandler : IRequestHandler<GetWarehouseGatePassService, string>
    {
        private readonly IUnitofwork unitOfWork;

        public GetWarehouseGatePassListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWarehouseGatePassService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.WarehouseGatePassAckRepository.GetWarehouseGatePassDetails(request.GetWarehouseGatePass);
            return result;
        }

    }

    public class GetWarehouseGatePassNoHandler : IRequestHandler<GetWarehouseGatePassNoDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;

        public GetWarehouseGatePassNoHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWarehouseGatePassNoDetailsService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.WarehouseGatePassAckRepository.LoadWarehouseGatePassNo(request.GetGatePassNo);
            return result;
        }

    }

    public class GetSameWarehouseHandler : IRequestHandler<GetSameWarehouseService, List<GetSameWarehouseEntity>>
    {
        private readonly IUnitofwork unitOfWork;

        public GetSameWarehouseHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<GetSameWarehouseEntity>> Handle(GetSameWarehouseService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.WarehouseGatePassAckRepository.GetSameWarehouse(request.GetSameWarehouse);
            return result;
        }

    }

    public class GetWarehouseGRNNoListHandler : IRequestHandler<GetWarehouseGRNNoListService, string>
    {
        private readonly IUnitofwork unitOfWork;

        public GetWarehouseGRNNoListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWarehouseGRNNoListService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.WarehouseGatePassAckRepository.LoadWarehouseAckGRNNoList(request.GetGRNNo);
            return result;
        }

    }

    public class GetWarehouseAckGatePassDetailsHandler : IRequestHandler<GetWarehouseGatePassDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;

        public GetWarehouseAckGatePassDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWarehouseGatePassDetailsService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.WarehouseGatePassAckRepository.LoadWarehouseAckGatePassDetails(request.GetGatePassDetails);
            return result;
        }

    }

    //public class GetStoreLocationHandler : IRequestHandler<GetStoreLocationService, List<GetSameWarehouseEntity>>
    //{
    //    private readonly IUnitofwork unitOfWork;

    //    public GetStoreLocationHandler(IUnitofwork unitOfWork)
    //    {
    //        this.unitOfWork = unitOfWork;
    //    }

    //    public async Task<List<GetSameWarehouseEntity>> Handle(GetStoreLocationService request, CancellationToken cancellationToken)
    //    {

    //        var result = await unitOfWork.WarehouseGatePassAckRepository.GetStoreLocation(request.GetSameWarehouse);
    //        return result;
    //    }

    //}

    //public class GetGatePassTypeHandler : IRequestHandler<GetGatePassTypeService, string>
    //{
    //    private readonly IUnitofwork unitOfWork;
    //    public GetGatePassTypeHandler(IUnitofwork unitOfWork)
    //    {
    //        this.unitOfWork = unitOfWork;
    //    }

    //    public async Task<string> Handle(GetGatePassTypeService request, CancellationToken cancellationToken)
    //    {

    //        var result = await unitOfWork.WarehouseGatePassAckRepository.GetGatePassType(request.DCEway);
    //        unitOfWork.Commit();
    //        return result;
    //    }
    //}
}
