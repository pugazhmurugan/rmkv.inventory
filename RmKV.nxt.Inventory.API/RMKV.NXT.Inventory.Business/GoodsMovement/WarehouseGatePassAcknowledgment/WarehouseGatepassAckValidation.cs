﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.WarehouseGatePassAcknowledgment
{
    //public class GetSameWarehouseValidation : AbstractValidator<GetSameWarehouseService>
    //{
    //}

    //public class GetStoreLocationValidation : AbstractValidator<GetStoreLocationService>
    //{
    //}

    //public class GetGatePassTypeValidation : AbstractValidator<GetGatePassTypeService>
    //{
    //}

    public class SaveWarehouseGatePassValidation : AbstractValidator<SaveWarehouseGatePassServices>
    {
    }

    public class LoadWarehouseGatePassValidation : AbstractValidator<LoadWarehouseGatePassServices>
    {
    }

    public class GetGatePassValidation : AbstractValidator<GetGatePassService>
    {
    }

    public class GetSameWarehouseValidation : AbstractValidator<GetSameWarehouseService>
    {
    }

    public class GetStoreLocationValidation : AbstractValidator<GetStoreLocationService>
    {
    }

    public class GetDesignationValidation : AbstractValidator<GetDesignationService>
    {
    }

    public class GetWarehouseTransferDetailValidation : AbstractValidator<GetWarehouseTransferDetailService>
    {
    }

    public class GetTransferTypeValidation : AbstractValidator<GetTransferTypeService>
    {
    }

    public class GetWarehouseGatePassListValidation : AbstractValidator<GetWarehouseGatePassService>
    {
    }

    public class GetWarehouseGatePassNoListValidation : AbstractValidator<GetWarehouseGatePassNoDetailsService>
    {
    }

    public class GetWarehouseGRNNoListValidation : AbstractValidator<GetWarehouseGRNNoListService>
    {
    }

    public class GetWarehouseAckGatePassDetailsValidation : AbstractValidator<GetWarehouseGatePassDetailsService>
    {
    }

}
