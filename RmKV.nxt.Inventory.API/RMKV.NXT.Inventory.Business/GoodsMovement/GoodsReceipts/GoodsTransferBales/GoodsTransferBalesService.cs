﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsReceipts.GoodsTransferBales
{
    public class LoadGoodsTransferBalesService : IRequest<string>
    {
        public string LoadGoodsBales { get; set; }
    }

    public class AddGoodsTransferBalesService : IRequest<int>
    {
        public string AddGoodsBales { get; set; }
    }

    public class GetGoodsBalesSectionGRNService : IRequest<string>
    {
        public string BalesSectionGRN { get; set; }
    }
}
