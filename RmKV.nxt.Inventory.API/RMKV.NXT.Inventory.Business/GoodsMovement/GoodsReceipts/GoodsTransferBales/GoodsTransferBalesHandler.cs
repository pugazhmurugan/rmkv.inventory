﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsReceipts.GoodsTransferBales
{
    public class LoadGoodsTransferBalesHandler : IRequestHandler<LoadGoodsTransferBalesService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public LoadGoodsTransferBalesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(LoadGoodsTransferBalesService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferBalesRepository.LoadGoodsTransferBales(request.LoadGoodsBales);
            unitOfWork.Commit();
            return result;
        }
    }

    public class AddGoodsTransferBalesHandler : IRequestHandler<AddGoodsTransferBalesService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddGoodsTransferBalesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddGoodsTransferBalesService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferBalesRepository.SaveGoodsTransferBales(request.AddGoodsBales);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetBalesSectionGRNHandler : IRequestHandler<GetGoodsBalesSectionGRNService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetBalesSectionGRNHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetGoodsBalesSectionGRNService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferBalesRepository.GetGoodsBalesSectionGRN(request.BalesSectionGRN);
            unitOfWork.Commit();
            return result;
        }
    }
}
