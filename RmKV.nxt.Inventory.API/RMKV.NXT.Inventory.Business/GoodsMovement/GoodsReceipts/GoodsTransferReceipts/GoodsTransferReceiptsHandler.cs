﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsReceipts.GoodsTransferReceipts
{
    public class LoadGoodsTransferReceiptsGHandler : IRequestHandler<LoadGoodsTransferReceiptsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public LoadGoodsTransferReceiptsGHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(LoadGoodsTransferReceiptsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferReceiptsRepository.LoadGoodsReceiptDetails(request.LoadGoodsReceipts);
            unitOfWork.Commit();
            return result;
        }
    }

    public class LoadReceiptsTransferNoCHandler : IRequestHandler<LoadGoodsReceiptsTransferNoService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public LoadReceiptsTransferNoCHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(LoadGoodsReceiptsTransferNoService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferReceiptsRepository.LoadGoodsReceiptsTransferNo(request.LoadTransferNo);
            unitOfWork.Commit();
            return result;
        }
    }

    public class AddGoodsTransferReceiptsHandler : IRequestHandler<AddGoodsTransferReceiptsService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddGoodsTransferReceiptsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddGoodsTransferReceiptsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferReceiptsRepository.SaveGoodsTransferReceiptsDetails(request.AddGoodsReceipts);
            unitOfWork.Commit();
            return result;
        }
    }

    public class FetchGoodsTransferReceiptsCHandler : IRequestHandler<FetchGoodsTransferReceiptsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchGoodsTransferReceiptsCHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(FetchGoodsTransferReceiptsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferReceiptsRepository.FetchGoodsTransferReceipts(request.FetchGoodsReceipts);
            unitOfWork.Commit();
            return result;
        }
    }

    public class CheckGoodsReceiptsTransferNoHandler : IRequestHandler<CheckReceiptsAckTransferNoService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CheckGoodsReceiptsTransferNoHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(CheckReceiptsAckTransferNoService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferReceiptsRepository.CheckGoodsReceiptsTransferNo(request.CheckReceiptsTransferNo);
            unitOfWork.Commit();
            return result;
        }
    }

    public class LoadReceiptsProductDetailsHandler : IRequestHandler<LoadGoodsReceiptsProductDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public LoadReceiptsProductDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(LoadGoodsReceiptsProductDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferReceiptsRepository.LoadGoodsReceiptsProductDetails(request.LoadProductList);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetAllWarehouseListHandler : IRequestHandler<GetAllWarehouseListService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetAllWarehouseListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetAllWarehouseListService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.GoodsTransferReceiptsRepository.GetAllWarehouseList(request.GetAllWarehouse);
            unitOfWork.Commit();
            return result;
        }
    }
}
