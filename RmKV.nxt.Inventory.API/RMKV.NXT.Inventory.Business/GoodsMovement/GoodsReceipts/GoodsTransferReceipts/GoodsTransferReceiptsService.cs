﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsReceipts.GoodsTransferReceipts
{
    public class LoadGoodsTransferReceiptsService : IRequest<string>
    {
        public string LoadGoodsReceipts { get; set; }
    }

    public class LoadGoodsReceiptsTransferNoService : IRequest<string>
    {
        public string LoadTransferNo { get; set; }
    }

    public class AddGoodsTransferReceiptsService : IRequest<int>
    {
        public string AddGoodsReceipts { get; set; }
    }

    public class FetchGoodsTransferReceiptsService : IRequest<string>
    {
        public string FetchGoodsReceipts { get; set; }
    }

    public class CheckReceiptsAckTransferNoService : IRequest<bool>
    {
        public string CheckReceiptsTransferNo { get; set; }
    }

    public class LoadGoodsReceiptsProductDetailsService : IRequest<string>
    {
        public string LoadProductList { get; set; }
    }

    public class GetAllWarehouseListService : IRequest<string>
    {
        public string GetAllWarehouse { get; set; }
    }
}

