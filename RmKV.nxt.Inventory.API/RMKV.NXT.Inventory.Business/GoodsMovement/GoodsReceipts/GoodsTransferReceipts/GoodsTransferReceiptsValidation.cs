﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.GoodsMovement.GoodsReceipts.GoodsTransferReceipts
{
    public class LoadGoodsTransferReceiptsValidation : AbstractValidator<LoadGoodsTransferReceiptsService>
    {

    }

    public class LoadGoodsReceiptsTransferNoValidation : AbstractValidator<LoadGoodsReceiptsTransferNoService>
    {

    }

    public class AddGoodsTransferReceiptsValidation : AbstractValidator<AddGoodsTransferReceiptsService>
    {

    }

    public class FetchGoodsTransferReceiptsValidation : AbstractValidator<FetchGoodsTransferReceiptsService>
    {

    }

    public class CheckGoodsReceiptsTransferNoValidation : AbstractValidator<CheckReceiptsAckTransferNoService>
    {

    }

    public class LoadGoodsReceiptsProductDetailsValidation : AbstractValidator<LoadGoodsReceiptsProductDetailsService>
    {

    }

    public class GetAllWarehouseListValidation : AbstractValidator<GetAllWarehouseListService>
    {

    }
}
