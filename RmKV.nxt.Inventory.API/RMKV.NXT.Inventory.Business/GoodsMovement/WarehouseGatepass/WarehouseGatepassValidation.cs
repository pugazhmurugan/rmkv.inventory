﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.WarehouseGatepass
{
    public class GetWarehouseGatePassTypeValidation : AbstractValidator<GetWarehouseGatePassTypeService>
    {

    }
    public class FetchWarehouseGatePassValidation : AbstractValidator<FetchWarehouseGatePassService>
    {

    }
    public class AddWarehouseGatePassValidation : AbstractValidator<AddWarehouseGatePassService>
    {

    }
    public class CancelWarehouseGatePassValidation : AbstractValidator<CancelWarehouseGatePassService>
    {

    }
    public class GetWarehouseGatePassValidation : AbstractValidator<GetWarehouseGatePassService>
    {

    }
    public class GetTransferDetailsValidation : AbstractValidator<GetTransferDetailsService>
    {

    }
    public class GetGatepassTowarehouseValidation : AbstractValidator<GetGatepassTowarehouseService>
    {

    }
    public class GatepassNoValidation : AbstractValidator<GatepassNoService>
    {

    }
    public class GatepassToLocationValidation : AbstractValidator<GatepassToLocationService>
    {

    }
}
