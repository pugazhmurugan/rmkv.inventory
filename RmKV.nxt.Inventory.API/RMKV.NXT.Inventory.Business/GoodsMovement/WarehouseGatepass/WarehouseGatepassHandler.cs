﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.WarehouseGatepass
{
    public class GetWarehouseGatePassTypeHandler : IRequestHandler<GetWarehouseGatePassTypeService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetWarehouseGatePassTypeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWarehouseGatePassTypeService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WarehouseGatepassRepository.GetWarehouseGatePassType(request.WarehouseGatePass);
            unitOfWork.Commit();
            return result;
        }

    }
    public class AddWarehouseGatePassHandler : IRequestHandler<AddWarehouseGatePassService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddWarehouseGatePassHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(AddWarehouseGatePassService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WarehouseGatepassRepository.AddWarehouseGatePass(request.WarehouseGatePass);
            unitOfWork.Commit();
            return result;
        }

    }
    public class FetchWarehouseGatePassHandler : IRequestHandler<FetchWarehouseGatePassService, Table2>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchWarehouseGatePassHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<Table2> Handle(FetchWarehouseGatePassService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WarehouseGatepassRepository.FetchWarehouseGatePass(request.WarehouseGatePass);
            unitOfWork.Commit();
            return result;
        }

    }
    public class CancelWarehouseGatePassHandler : IRequestHandler<CancelWarehouseGatePassService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CancelWarehouseGatePassHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(CancelWarehouseGatePassService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WarehouseGatepassRepository.CancelWarehouseGatePass(request.WarehouseGatePass);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetWarehouseGatePassHandler : IRequestHandler<GetWarehouseGatePassService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetWarehouseGatePassHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetWarehouseGatePassService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WarehouseGatepassRepository.GetWarehouseGatePass(request.WarehouseGatePass);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetTransferDetailsHandler : IRequestHandler<GetTransferDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetTransferDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetTransferDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WarehouseGatepassRepository.GetTransferDetails(request.WarehouseGatePass);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetGatepassTowarehouseHandler : IRequestHandler<GetGatepassTowarehouseService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetGatepassTowarehouseHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetGatepassTowarehouseService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WarehouseGatepassRepository.GetGatepassTowarehouse(request.WarehouseGatePass);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GatepassNoHandler : IRequestHandler<GatepassNoService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GatepassNoHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GatepassNoService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WarehouseGatepassRepository.GatepassNo(request.WarehouseGatePass);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GatepassToLocationHandler : IRequestHandler<GatepassToLocationService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GatepassToLocationHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GatepassToLocationService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.WarehouseGatepassRepository.GatepassToLocation(request.WarehouseGatePass);
            unitOfWork.Commit();
            return result;
        }
    }
}
