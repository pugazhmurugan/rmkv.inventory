﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataContext.InterfaceRepository.GoodsMovement.WarehouseGatepass
{
    public class GetWarehouseGatePassTypeService : IRequest<string>
    {
        public string WarehouseGatePass { get; set; }
    }
    public class AddWarehouseGatePassService : IRequest<bool>
    {
        public string WarehouseGatePass { get; set; }
    }
    public class GetWarehouseGatePassService : IRequest<string>
    {
        public string WarehouseGatePass { get; set; }
    }
    public class FetchWarehouseGatePassService : IRequest<Table2>
    {
        public string WarehouseGatePass { get; set; }
    }
    public class CancelWarehouseGatePassService : IRequest<bool>
    {
        public string WarehouseGatePass { get; set; }
    }
    public class GetTransferDetailsService : IRequest<string>
    {
        public string WarehouseGatePass { get; set; }
    }
    public class GetGatepassTowarehouseService : IRequest<string>
    {
        public string WarehouseGatePass { get; set; }
    }
    public class GatepassNoService : IRequest<string>
    {
        public string WarehouseGatePass { get; set; }
    }
    public class GatepassToLocationService : IRequest<string>
    {
        public string WarehouseGatePass { get; set; }
    }
}
