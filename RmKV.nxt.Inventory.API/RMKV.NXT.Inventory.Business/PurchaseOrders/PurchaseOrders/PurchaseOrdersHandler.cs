﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.PurchaseOrders.PurchaseOrders
{
    public class GetPOAnalysisNoHandler : IRequestHandler<GetPOAnalysisService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPOAnalysisNoHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetPOAnalysisService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.GetPOAnalysis(request.POAnalysis);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetSupplierCityDetailsHandler : IRequestHandler<GetSupplierCityDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetSupplierCityDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetSupplierCityDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.GetSupplierCityDetails();
            return result;
        }
    }

    public class GetSupplierGroupDetailsHandler : IRequestHandler<GetSupplierGroupDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetSupplierGroupDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetSupplierGroupDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.GetSupplierGroupDetails(request.SupplierGroup);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetSupplierByGroupIdHandler : IRequestHandler<GetSupplierByGroupIdService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetSupplierByGroupIdHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetSupplierByGroupIdService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.GetSupplierByGroupId(request.SupplierByGroupId);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetModeOfDispatchMasterHandler : IRequestHandler<GetModeofDispatchMasterService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetModeOfDispatchMasterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetModeofDispatchMasterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.GetModeofDispatchMasterList(request.ModeOfDispatch);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetUOMDetailsHandler : IRequestHandler<GetUOMDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetUOMDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetUOMDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.GetUOMList(request.section_id);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetPurchaseOrderListHandler : IRequestHandler<GetPurchaseOrderListService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPurchaseOrderListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetPurchaseOrderListService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.GetPurchaseOrderList(request.GetPurchaseOrder);
            unitOfWork.Commit();
            return result;
        }
    }

    public class CancelPurchaseOrderListHandler : IRequestHandler<CancelPurchaseOrderListService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public CancelPurchaseOrderListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(CancelPurchaseOrderListService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.CancelPurchaseOrderList(request.CancelPurchaseOrder);
            unitOfWork.Commit();
            return result;
        }
    }

    public class SavePurchaseOrderListHandler : IRequestHandler<SavePurchaseOrderListService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public SavePurchaseOrderListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(SavePurchaseOrderListService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.SavePurchaseOrderList(request.SavePurchaseOrder);
            unitOfWork.Commit();
            return result;
        }
    }

    public class FetchPurchaseOrderListHandler : IRequestHandler<FetchPurchaseOrderListService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchPurchaseOrderListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(FetchPurchaseOrderListService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.FetchPurchaseOrderList(request.FetchPurchaseOrder);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetDispatchNameListHandler : IRequestHandler<GetDispatchNameListService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetDispatchNameListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetDispatchNameListService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.GetPoDispatchLocationList(request.GetDispatchName);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetPoSizeNameHandler : IRequestHandler<GetPurchaseOrderSizeNameService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPoSizeNameHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetPurchaseOrderSizeNameService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.GetPurchaseOrderSizeName(request.GetPoSizeName);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetPoSizeNameBasedSizesHandler : IRequestHandler<GetPOSizeNameBasedSizesService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPoSizeNameBasedSizesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetPOSizeNameBasedSizesService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.GetPurchaseOrderSizeNameBasedSizes(request.GetPoSizeNameBasedSizes);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetPONoHandler : IRequestHandler<GetPONoService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPONoHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetPONoService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.GetPurchaseOrderNo(request.GetPONo);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetEmailPurchaseOrderHandler : IRequestHandler<GetPurchaseOrderEmailService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetEmailPurchaseOrderHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetPurchaseOrderEmailService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseOrdersRepository.GetPurchaseOrderEmail(request.GetPOEmail);
            unitOfWork.Commit();
            return result;
        }
    }

}
 