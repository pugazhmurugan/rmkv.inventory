﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseOrders.PurchaseOrders
{
    public class GetPOAnalysisValidation : AbstractValidator<GetPOAnalysisService>
    {

    }
    public class GetSupplierCityDetailsValidation : AbstractValidator<GetSupplierCityDetailsService>
    {

    }

    public class GetSupplierGroupDetailsValidation : AbstractValidator<GetSupplierGroupDetailsService>
    {

    }

    public class GetSupplierByGroupIdValidation : AbstractValidator<GetSupplierByGroupIdService>
    {

    }

    public class GetModeOfDispatchMasterValidation : AbstractValidator<GetModeofDispatchMasterService>
    {

    }

    public class GetUOMDetailsValidation : AbstractValidator<GetUOMDetailsService>
    {

    }

    public class GetPurchaseOrderValidation : AbstractValidator<GetPurchaseOrderListService>
    {

    }

    public class CancelPurchaseOrderValidation : AbstractValidator<CancelPurchaseOrderListService>
    {

    }

    public class SavePurchaseOrderValidation : AbstractValidator<SavePurchaseOrderListService>
    {

    }

    public class FetchPurchaseOrderValidation : AbstractValidator<FetchPurchaseOrderListService>
    {

    }

    public class GetDispatchNameListValidation : AbstractValidator<GetDispatchNameListService>
    {

    }

    public class GetPOSizeNameValidation : AbstractValidator<GetPurchaseOrderSizeNameService>
    {

    }

    public class GetPOSizeNameBasedSizesValidation : AbstractValidator<GetPOSizeNameBasedSizesService>
    {

    }

    public class GetPONoValidation : AbstractValidator<GetPONoService>
    {

    }

    public class GetEmailPurchaseOrderValidation : AbstractValidator<GetPurchaseOrderEmailService>
    {

    }
}
