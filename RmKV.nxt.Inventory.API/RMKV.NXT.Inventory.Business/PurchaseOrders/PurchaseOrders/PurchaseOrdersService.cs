﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseOrders.PurchaseOrders
{
    public class GetPOAnalysisService : IRequest<string>
    {
        public string POAnalysis { get; set; }
    }

    public class GetSupplierCityDetailsService : IRequest<string>
    {

    }

    public class GetSupplierGroupDetailsService : IRequest<string>
    {
        public string SupplierGroup { get; set; }
    }

    public class GetSupplierByGroupIdService : IRequest<string>
    {
        public string SupplierByGroupId { get; set; }
    }

    public class GetModeofDispatchMasterService : IRequest<string>
    {
        public string ModeOfDispatch { get; set; }
    }

    public class GetUOMDetailsService : IRequest<string>
    {
        public int section_id { get; set; }
    }

    public class GetPurchaseOrderListService : IRequest<string>
    {
        public string GetPurchaseOrder { get; set; }
    }

    public class CancelPurchaseOrderListService : IRequest<int>
    {
        public string CancelPurchaseOrder { get; set; }
    }

    public class SavePurchaseOrderListService : IRequest<int>
    {
        public string SavePurchaseOrder { get; set; }
    }

    public class FetchPurchaseOrderListService : IRequest<string>
    {
        public string FetchPurchaseOrder { get; set; }
    }

    public class GetDispatchNameListService : IRequest<string>
    {
        public string GetDispatchName { get; set; }
    }

    public class GetPurchaseOrderSizeNameService : IRequest<string>
    {
        public string GetPoSizeName { get; set; }
    }

    public class GetPOSizeNameBasedSizesService : IRequest<string>
    {
        public string GetPoSizeNameBasedSizes { get; set; }
    }

    public class GetPONoService : IRequest<string>
    {
        public string GetPONo { get; set; }
    }

    public class GetPurchaseOrderEmailService : IRequest<string>
    {
        public string GetPOEmail { get; set; }
    }
}
