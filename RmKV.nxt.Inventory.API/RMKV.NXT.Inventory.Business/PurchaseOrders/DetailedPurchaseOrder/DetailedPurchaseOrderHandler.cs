﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.PurchaseOrders.DetailedPurchaseOrder
{
    public class SaveDetailedPOHandler : IRequestHandler<SaveDetailedPODetailsService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public SaveDetailedPOHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(SaveDetailedPODetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.SaveDetailedPurchaseOrder(request.SaveDetailedPO);
            unitOfWork.Commit();
            return result;
        }
    }

    public class CancelDetailedPOHandler : IRequestHandler<CancelDetailedPODetailsService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public CancelDetailedPOHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(CancelDetailedPODetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.CancelDetailedPurchaseOrder(request.CancelDetailedPO);
            unitOfWork.Commit();
            return result;
        }
    }

    public class FetchDetailedPOHandler : IRequestHandler<FetchDetailedPODetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchDetailedPOHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(FetchDetailedPODetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.FetchDetailedPurchaseOrder(request.FetchDetailedPO);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetDetailedPOHandler : IRequestHandler<LoadDetailedPODetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetDetailedPOHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(LoadDetailedPODetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.GetDetailedPurchaseOrderList(request.LoadDetailedPO);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetBrandForDetailedPOHandler : IRequestHandler<GetBrandListForDetailedPOService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetBrandForDetailedPOHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetBrandListForDetailedPOService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.GetBrandListForDetailedPO(request.GetBrandDetailedPO);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetProductGroupForDetailedPOHandler : IRequestHandler<GetProductGroupListForDetailedPOService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductGroupForDetailedPOHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetProductGroupListForDetailedPOService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.GetProductGroupListForDetailedPO(request.GetProductGroupDetailedPO);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetProductForDetailedPOHandler : IRequestHandler<GetProductListForDetailedPOService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductForDetailedPOHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetProductListForDetailedPOService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.GetProductListForDetailedPO(request.GetProductDetailedPO);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetDesignCodeForDetailedPOHandler : IRequestHandler<GetDesignCodeListForDetailedPOService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetDesignCodeForDetailedPOHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetDesignCodeListForDetailedPOService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.GetDesignCodeListForDetailedPO(request.GetDesignCodePO);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetColorForDetailedPOHandler : IRequestHandler<GetColorListForDetailedPOService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetColorForDetailedPOHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetColorListForDetailedPOService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.GetColorListForDetailedPO(request.GetColorDetailedPO);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetQualityForDetailedPOHandler : IRequestHandler<GetQualityListForDetailedPOService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetQualityForDetailedPOHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetQualityListForDetailedPOService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.GetQualityListForDetailedPO(request.GetQualityDetailedPO);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetSizeForDetailedPOHandler : IRequestHandler<GetSizeForDetailedPOService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetSizeForDetailedPOHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetSizeForDetailedPOService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.GetProductSizeListForDetailedPO(request.GetSizeDetailedPO);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetLocationsForDetailedPOHandler : IRequestHandler<GetLocationsForDetailedPOService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetLocationsForDetailedPOHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetLocationsForDetailedPOService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.GetPurchaseOrderLocations(request.GetLocationsForDPO);
            unitOfWork.Commit();
            return result;
        }

        public class GetDetailedPONoHandler : IRequestHandler<GetDetailedPONoService, string>
        {
            private readonly IUnitofwork unitOfWork;
            public GetDetailedPONoHandler(IUnitofwork unitOfWork)
            {
                this.unitOfWork = unitOfWork;
            }

            public async Task<string> Handle(GetDetailedPONoService request, CancellationToken cancellationToken)
            {
                var result = await unitOfWork.DetailedPurchaseOrderRepository.GetDetailedPurchaseOrderNo(request.GetDetailedPONo);
                unitOfWork.Commit();
                return result;
            }
        }
    }

    public class GetEmailDetailedPurchaseOrderHandler : IRequestHandler<GetDetailedPurchaseOrderEmailService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetEmailDetailedPurchaseOrderHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetDetailedPurchaseOrderEmailService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.DetailedPurchaseOrderRepository.GetDetailedPurchaseOrderEmail(request.GetDetailedPOEmail);
            unitOfWork.Commit();
            return result;
        }
    }
}
