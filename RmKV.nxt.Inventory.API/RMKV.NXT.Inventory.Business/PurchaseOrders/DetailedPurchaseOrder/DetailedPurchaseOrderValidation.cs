﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseOrders.DetailedPurchaseOrder
{
    public class SaveDetailedPOValidation : AbstractValidator<SaveDetailedPODetailsService>
    {

    }

    public class CancelDetailedPOValidation : AbstractValidator<CancelDetailedPODetailsService>
    {

    }

    public class FetchDetailedPOValidation : AbstractValidator<FetchDetailedPODetailsService>
    {

    }

    public class GetDetailedPOValidation : AbstractValidator<LoadDetailedPODetailsService>
    {

    }

    public class GetBrandListDetailedPOValidation : AbstractValidator<GetBrandListForDetailedPOService>
    {

    }

    public class GetProductGroupDetailedPOValidation : AbstractValidator<GetProductGroupListForDetailedPOService>
    {

    }

    public class GetProductDetailedPOValidation : AbstractValidator<GetProductListForDetailedPOService>
    {

    }

    public class GetDesignCodeDetailedPOValidation : AbstractValidator<GetDesignCodeListForDetailedPOService>
    {

    }

    public class GetColorDetailedPOValidation : AbstractValidator<GetColorListForDetailedPOService>
    {

    }

    public class GetQualityDetailedPOValidation : AbstractValidator<GetQualityListForDetailedPOService>
    {

    }

    public class GetSizeDetailedPOValidation : AbstractValidator<GetSizeForDetailedPOService>
    {

    }

    public class GetLocationsDetailedPOValidation : AbstractValidator<GetLocationsForDetailedPOService>
    {

    }

    public class GetDetailedPONoValidation : AbstractValidator<GetDetailedPONoService>
    {

    }

    public class GetDetailedEmailPurchaseOrderValidation : AbstractValidator<GetDetailedPurchaseOrderEmailService>
    {

    }
}
