﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseOrders.DetailedPurchaseOrder
{
    public class SaveDetailedPODetailsService : IRequest<int>
    {
        public string SaveDetailedPO { get; set; }
    }

    public class CancelDetailedPODetailsService : IRequest<int>
    {
        public string CancelDetailedPO { get; set; }
    }

    public class LoadDetailedPODetailsService : IRequest<string>
    {
        public string LoadDetailedPO { get; set; }
    }

    public class FetchDetailedPODetailsService : IRequest<string>
    {
        public string FetchDetailedPO { get; set; }
    }

    public class GetBrandListForDetailedPOService : IRequest<string>
    {
        public string GetBrandDetailedPO { get; set; }
    }

    public class GetProductGroupListForDetailedPOService : IRequest<string>
    {
        public string GetProductGroupDetailedPO { get; set; }
    }

    public class GetProductListForDetailedPOService : IRequest<string>
    {
        public string GetProductDetailedPO { get; set; }
    }

    public class GetDesignCodeListForDetailedPOService : IRequest<string>
    {
        public string GetDesignCodePO { get; set; }
    }

    public class GetColorListForDetailedPOService : IRequest<string>
    {
        public string GetColorDetailedPO { get; set; }
    }

    public class GetQualityListForDetailedPOService : IRequest<string>
    {
        public string GetQualityDetailedPO { get; set; }
    }

    public class GetSizeForDetailedPOService : IRequest<string>
    {
        public string GetSizeDetailedPO { get; set; }
    }

    public class GetLocationsForDetailedPOService : IRequest<string>
    {
        public string GetLocationsForDPO { get; set; }
    }

    public class GetDetailedPONoService : IRequest<string>
    {
        public string GetDetailedPONo { get; set; }
    }

    public class GetDetailedPurchaseOrderEmailService : IRequest<string>
    {
        public string GetDetailedPOEmail { get; set; }
    }
}
