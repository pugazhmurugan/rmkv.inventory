﻿using MediatR;
using RMKV.NXT.Inventory.Business.ApiClients.LaunchingApi;
using RMKV.NXT.Inventory.DataModel.Model;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Menus
{
    public class MenusHandler : IRequestHandler<MenusService, List<MenuItemsEntity>>
    {
        private readonly ILauncherApiClients launchingApi;

        public MenusHandler(ILauncherApiClients launchingApi)
        {
            this.launchingApi = launchingApi;
        }

        public async Task<List<MenuItemsEntity>> Handle(MenusService request, CancellationToken cancellationToken)
        {
            var data = new GetAllMenusParams()
            {
                User_ID = request.User_ID,
                Module_ID = request.Module_ID,
                Sales_Location_ID = request.Sales_Location_ID,
                Company_ID = request.Company_ID,
                Section_ID = request.Section_ID,
                WareHouse_ID = request.WareHouse_ID
            };
            var result = await launchingApi.GetMenus(data);
            return result;
        }

       
    }
}
