﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model;
using System.Collections.Generic;

namespace RMKV.NXT.Inventory.Business.Menus
{
    public class MenusService : IRequest<List<MenuItemsEntity>>
    {
        public int User_ID { get; set; }
        public int Module_ID { get; set; }
        public int Sales_Location_ID { get; set; }
        public int Company_ID { get; set; }
        public int Section_ID { get; set; }
        public int WareHouse_ID { get; set; }
    }
}
