﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Rmkv.Nxt.GRN.Business.BItMapimages
{
   public class BItMapimage
    {
        public static string BitmapToBytesCode(Bitmap image)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                var data = "data:image/png;base64," + Convert.ToBase64String(stream.ToArray());
                return data;
                //return stream.ToArray();
            }
        }
            public static byte[] BitmapToBytesCodes(Bitmap image)
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                   // var data = "data:image/png;base64," + Convert.ToBase64String(stream.ToArray());
                   // return data;
                    return stream.ToArray();
                }

            }
        }
    }

