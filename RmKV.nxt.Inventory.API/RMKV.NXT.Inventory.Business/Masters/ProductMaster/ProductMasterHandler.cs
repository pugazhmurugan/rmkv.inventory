﻿using MediatR;
using RMKV.NXT.Inventory.Business.Masters.ProductMaster;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Masters.ProductMaster
{
    public class GetProductMasterHandler : IRequestHandler<GetProductMasterService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductMasterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetProductMasterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductMasterRepository.GetProductMasters(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetProductCounterHandler : IRequestHandler<GetProductCounterService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductCounterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetProductCounterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductMasterRepository.GetProductCounter(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetProductAttributesHandler : IRequestHandler<GetProductAttributesServices, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductAttributesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetProductAttributesServices request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductMasterRepository.GetProductAttributes(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }
    }
    public class AddProductMasterHandler : IRequestHandler<AddProductMasterService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddProductMasterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(AddProductMasterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductMasterRepository.AddProductMasters(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }
    }
    public class DeleteProductMasterHandler : IRequestHandler<DeleteProductMasterService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public DeleteProductMasterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(DeleteProductMasterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductMasterRepository.DeleteProductMasters(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }
    }

    public class FetchProductMasterHandler : IRequestHandler<FetchProductMasterService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchProductMasterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(FetchProductMasterService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductMasterRepository.FetchProductMasters(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }
    }

    public class addGoodsHandler : IRequestHandler<addGoodsService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public addGoodsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(addGoodsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductMasterRepository.addGoods(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetToLocationHandler : IRequestHandler<GetToLocationsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetToLocationHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetToLocationsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductMasterRepository.GetToLocation(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }

    }


    public class GetGoodsHandler : IRequestHandler<GetGoodsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetGoodsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetGoodsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductMasterRepository.GetGoods(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetTransferNoHandler : IRequestHandler<GetTransferNoService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public GetTransferNoHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(GetTransferNoService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductMasterRepository.GetTransferNo(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }

    }
}
public class FetchGoodsHandler : IRequestHandler<FetchGoodsService, string>
{
    private readonly IUnitofwork unitOfWork;
    public FetchGoodsHandler(IUnitofwork unitOfWork)
    {
        this.unitOfWork = unitOfWork;
    }

    public async Task<string> Handle(FetchGoodsService request, CancellationToken cancellationToken)
    {
        var result = await unitOfWork.ProductMasterRepository.FetchGoods(request.ProductMaster);
        unitOfWork.Commit();
        return result;
    }

}
