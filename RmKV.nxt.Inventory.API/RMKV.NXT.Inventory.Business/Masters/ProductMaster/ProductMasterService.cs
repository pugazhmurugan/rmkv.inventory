﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductMaster
{
    public class GetProductMasterService : IRequest<string>
    {
        public string ProductMaster { get; set; }
    }
    public class GetProductCounterService : IRequest<string>
    {
        public string ProductMaster { get; set; }
    }
    public class GetProductAttributesServices : IRequest<string>
    {
        public string ProductMaster { get; set; }
    }
    public class AddProductMasterService : IRequest<bool>
    {
        public string ProductMaster { get; set; }
    }
    public class DeleteProductMasterService : IRequest<bool>
    {
        public string ProductMaster { get; set; }
    }
    public class FetchProductMasterService : IRequest<string>
    {
        public string ProductMaster { get; set; }
    }

    public class addGoodsService : IRequest<bool>
    {
        public string ProductMaster { get; set; }
    }
    public class GetGoodsService : IRequest<string>
    {
        public string ProductMaster { get; set; }
    }
    public class GetToLocationsService : IRequest<string>
    {
        public string ProductMaster { get; set; }
    }
    public class GetTransferNoService : IRequest<int>
    {
        public string ProductMaster { get; set; }
    }
    public class FetchGoodsService : IRequest<string>
    {
        public string ProductMaster { get; set; }
    }
}
