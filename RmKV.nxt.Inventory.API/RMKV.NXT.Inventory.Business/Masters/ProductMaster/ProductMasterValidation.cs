﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductMaster
{
    public class GetProductMasterValidation : AbstractValidator<GetProductMasterService>
    {

    }
    public class GetProductCounterValidation : AbstractValidator<GetProductCounterService>
    {

    }
    public class GetProductAttributesValidations : AbstractValidator<GetProductAttributesServices>
    {

    }
    public class AddProductMasterValidation : AbstractValidator<AddProductMasterService>
    {

    }
    public class DeleteProductMasterValidation : AbstractValidator<DeleteProductMasterService>
    {

    }
    public class FetchProductMasterValidation : AbstractValidator<FetchProductMasterService>
    {

    }


    public class addGoodsValidation : AbstractValidator<addGoodsService>
    {

    }
    public class GetToLocationsValidation : AbstractValidator<GetToLocationsService>
    {

    }
    public class GetTransferNoValidations : AbstractValidator<GetTransferNoService>
    {

    }
    public class FetchGoodsValidations : AbstractValidator<FetchGoodsService>
    {

    }
    public class GetGoodsValidations : AbstractValidator<GetGoodsService>
    {

    }
}