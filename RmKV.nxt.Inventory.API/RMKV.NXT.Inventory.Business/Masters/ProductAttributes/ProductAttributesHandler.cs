﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Masters.ProductAttributes
{
    public class ProductAttributesHandler : IRequestHandler<GetProductAttributesService, List<GetProductAttributesEntity>>
    {
    
        private readonly IUnitofwork unitOfWork;

        public ProductAttributesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<GetProductAttributesEntity>> Handle(GetProductAttributesService request, CancellationToken cancellationToken)
        {
            var data = new GetProductAttributesParams()
            {
                Group_Section_Id = request.Group_Section_Id
            };
            var result = await unitOfWork.ProductAttributesRepository.GetProductAttributes(data);
            return result;
        }
    }
    public class ProductAttributeValuesHandler : IRequestHandler<GetProductAttributeValuesService, List<GetProductAttributeValuesEntity>>
    {

        private readonly IUnitofwork unitOfWork;

        public ProductAttributeValuesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<GetProductAttributeValuesEntity>> Handle(GetProductAttributeValuesService request, CancellationToken cancellationToken)
        {
            var data = new AddProductAttributesEntity()
            {
                Product_Attribute_List = request.Product_Attribute_List
            };
            var result = await unitOfWork.ProductAttributesRepository.GetProductAttributeValues(data);
            return result;
        }
    }
    public class AddProductAttributesHandler : IRequestHandler<AddProductAttributesService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddProductAttributesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddProductAttributesService request, CancellationToken cancellationToken)
        {
            var data = new AddProductAttributesEntity()
            {
                Product_Attribute_List = request.Product_Attribute_List
            };
            var result = await unitOfWork.ProductAttributesRepository.AddProductAttributes(data);
            unitOfWork.Commit();
            return result;
        }
    }
    public class ModifyProductAttributesHandler : IRequestHandler<ModifyProductAttributesService, List<GetProductAttributesEntity>>
    {

        private readonly IUnitofwork unitOfWork;

        public ModifyProductAttributesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<GetProductAttributesEntity>> Handle(ModifyProductAttributesService request, CancellationToken cancellationToken)
        {
            var data = new AddProductAttributesEntity()
            {
                Product_Attribute_List = request.Product_Attribute_List
            };
            var result = await unitOfWork.ProductAttributesRepository.ModifyProductAttributes(data);
            return result;
        }
    }
    public class AddProductAttributeValuesHandler : IRequestHandler<AddProductAttributeValuesService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddProductAttributeValuesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddProductAttributeValuesService request, CancellationToken cancellationToken)
        {
            var data = new AddProductAttributesEntity()
            {
                Product_Attribute_List = request.Product_Attribute_List

            };
            var result = await unitOfWork.ProductAttributesRepository.AddProductAttributeValues(data);
            unitOfWork.Commit();
            return result;
        }
    }
    public class ModifyProductAttributeValuesHandler : IRequestHandler<ModifyProductAttributeValuesService, List<GetProductAttributeValuesEntity>>
    {

        private readonly IUnitofwork unitOfWork;

        public ModifyProductAttributeValuesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<GetProductAttributeValuesEntity>> Handle(ModifyProductAttributeValuesService request, CancellationToken cancellationToken)
        {
            var data = new AddProductAttributesEntity()
            {
                Product_Attribute_List = request.Product_Attribute_List
            };
            var result = await unitOfWork.ProductAttributesRepository.ModifyProductAttributeValues(data);
            return result;
        }
    }
}
