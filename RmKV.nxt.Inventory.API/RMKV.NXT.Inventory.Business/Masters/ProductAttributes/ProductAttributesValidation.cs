﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductAttributes
{
    public class GetProductAttributesValidation : AbstractValidator<GetProductAttributesService>
    {
    }
    public class AddProductAttributesValidation : AbstractValidator<AddProductAttributesService>
    {
    }
    public class ModifyProductAttributesValidation : AbstractValidator<ModifyProductAttributesService>
    {
    }
    public class GetProductAttributeValuesValidation : AbstractValidator<GetProductAttributeValuesService>
    {
    }
    public class AddProductAttributeValuesValidation : AbstractValidator<AddProductAttributeValuesService>
    {
    }
    public class ModifyProductAttributeValuesValidation : AbstractValidator<ModifyProductAttributeValuesService>
    {
    }
}
