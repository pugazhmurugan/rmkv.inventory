﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductAttributes
{
   
    public class GetProductAttributesService : IRequest<List<GetProductAttributesEntity>>
    {
        public int Group_Section_Id { get; set; }
    }
    public class AddProductAttributesService : IRequest<int>
    {
        public string Product_Attribute_List { get; set; }
    }
    public class ModifyProductAttributesService : IRequest<List<GetProductAttributesEntity>>
    {
        public string Product_Attribute_List { get; set; }
    }
    public class GetProductAttributeValuesService : IRequest<List<GetProductAttributeValuesEntity>>
    {
        public string Product_Attribute_List { get; set; }
    }
    public class AddProductAttributeValuesService : IRequest<int>
    {
        public string Product_Attribute_List { get; set; }
    }
    public class ModifyProductAttributeValuesService : IRequest<List<GetProductAttributeValuesEntity>>
    {
        public string Product_Attribute_List { get; set; }
    }
}
