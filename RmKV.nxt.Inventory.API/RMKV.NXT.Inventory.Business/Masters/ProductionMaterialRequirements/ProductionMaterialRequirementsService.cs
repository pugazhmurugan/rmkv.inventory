﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductionMaterialRequirements
{
    public class SaveProductionMaterialRequirementsService : IRequest<int>
    {
        public string ProdMaterialRequirements { get; set; }
    }
    public class GetProductionMaterialRequirementsService : IRequest<string>
    {
    }
    public class FetchProductionMaterialRequirementsService : IRequest<string>
    {
        public string ProdMaterialRequirements { get; set; }
    }
}
