﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Masters.ProductionMaterialRequirements
{
    public class SaveProductionMaterialRequirementsHandler : IRequestHandler<SaveProductionMaterialRequirementsService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public SaveProductionMaterialRequirementsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(SaveProductionMaterialRequirementsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionMaterialRequirementsRepository.AddProdMaterialRequirement(request.ProdMaterialRequirements);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetProductionMaterialRequirementsHandler : IRequestHandler<GetProductionMaterialRequirementsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductionMaterialRequirementsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetProductionMaterialRequirementsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionMaterialRequirementsRepository.GetProdMaterialRequirement();
            return result;
        }
    }
    public class FetchProductionMaterialRequirementsHandler : IRequestHandler<FetchProductionMaterialRequirementsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchProductionMaterialRequirementsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(FetchProductionMaterialRequirementsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionMaterialRequirementsRepository.FetchProdMaterialRequirement(request.ProdMaterialRequirements);
            unitOfWork.Commit();
            return result;
        }
    }
}
