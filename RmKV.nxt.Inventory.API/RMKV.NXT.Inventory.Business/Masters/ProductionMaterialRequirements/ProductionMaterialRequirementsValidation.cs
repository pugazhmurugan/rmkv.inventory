﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductionMaterialRequirements
{
    public class SaveProductionMaterialRequirementsValidation : AbstractValidator<SaveProductionMaterialRequirementsService>
    {

    }
    public class GetProductionMaterialRequirementsValidation : AbstractValidator<GetProductionMaterialRequirementsService>
    {

    }
    public class FetchProductionMaterialRequirementsValidation : AbstractValidator<FetchProductionMaterialRequirementsService>
    {

    }
}
