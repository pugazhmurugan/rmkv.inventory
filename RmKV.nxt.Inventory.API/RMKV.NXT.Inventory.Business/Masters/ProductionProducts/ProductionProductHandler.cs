﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Masters.ProductionProducts
{
    public class ProductionProductHandler : IRequestHandler<ProductionProductService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public ProductionProductHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<int> Handle(ProductionProductService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionProductRepository.AddProductionProduct(request.ProdProduct);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetProductionProductListHandler : IRequestHandler<GetProductionProductService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductionProductListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetProductionProductService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionProductRepository.GetProductionProduct();
            //unitOfWork.Commit();
            return result;
        }
    }
    public class FetchProductionProductListHandler : IRequestHandler<FetchProductionProductService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchProductionProductListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(FetchProductionProductService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionProductRepository.GetProductionProductDetails(request.ProdProduct);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetProductionProductLookupListHandler : IRequestHandler<GetProductionProductLookupService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductionProductLookupListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetProductionProductLookupService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionProductRepository.GetProductionProductLookup();
            //unitOfWork.Commit();
            return result;
        }
    }
}
