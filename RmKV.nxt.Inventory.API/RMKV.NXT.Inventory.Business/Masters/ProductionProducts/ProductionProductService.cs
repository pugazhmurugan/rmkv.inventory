﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductionProducts
{
    public class ProductionProductService : IRequest<int>
    {
        public string ProdProduct { get; set; }
    }
    public class GetProductionProductService : IRequest<string>
    {
        public string ProdProduct { get; set; }
    }
    public class FetchProductionProductService : IRequest<string>
    {
        public string ProdProduct { get; set; }
    }
    public class GetProductionProductLookupService : IRequest<string>
    {
        public string ProdProduct { get; set; }
    }
}
