﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductionProducts
{
   public class ProductionProductValidation: AbstractValidator<ProductionProductService>
    {

    }
    public class GetProductionProductValidation : AbstractValidator<GetProductionProductService>
    {

    }
    public class FetchProductionProductValidation : AbstractValidator<FetchProductionProductService>
    {

    }
    public class GetProductionProductLookupValidation : AbstractValidator<GetProductionProductLookupService>
    {

    }
}
