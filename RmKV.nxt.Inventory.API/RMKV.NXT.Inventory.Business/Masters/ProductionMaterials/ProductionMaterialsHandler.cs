﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace RMKV.NXT.Inventory.Business.Masters.ProductionMaterials
{
    public class AddProductionMaterialsHandler : IRequestHandler<AddProductionMaterialsService, int>
    {
        private readonly IUnitofwork unitOfWork;

        public AddProductionMaterialsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddProductionMaterialsService request, CancellationToken cancellationToken)
        { 
            var result = await unitOfWork.ProductionMaterialsRepository.AddProductionMaterials(request.ProductMaterial);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetProductionMaterialsHandler : IRequestHandler<GetProductionMaterialsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductionMaterialsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetProductionMaterialsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionMaterialsRepository.GetProductionMaterials();
            unitOfWork.Commit();
            return result;
        }

    }

    public class FetchProductionMaterialsHandler : IRequestHandler<FetchProductionMaterialsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchProductionMaterialsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(FetchProductionMaterialsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductionMaterialsRepository.FetchProductionMaterials(request.ProductMaterial);
            unitOfWork.Commit();
            return result;
        }

    }

}
