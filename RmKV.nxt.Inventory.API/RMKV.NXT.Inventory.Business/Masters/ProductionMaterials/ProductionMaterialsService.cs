﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductionMaterials
{
    public class AddProductionMaterialsService : IRequest<int>
    {
        public string ProductMaterial { get; set; }
    }

    public class GetProductionMaterialsService : IRequest<string>
    {
        
    }

    public class FetchProductionMaterialsService : IRequest<string>
    {
        public string ProductMaterial { get; set; }
    }
}
