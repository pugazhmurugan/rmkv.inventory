﻿using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductCategory
{
   
    public class SaveProductCategoryValidation : AbstractValidator<SaveProductCategoryService>
    {
    }

    public class FetchProductCategoryValidation : AbstractValidator<FerchProductCategoryService>
    {
    }

    public class GetProductCategoryValidation : AbstractValidator<LoadProductCategoryService>
    {
    }

    public class RemoveProductCategoryValidation : AbstractValidator<RemoveProductCategoryService>
    {
    }
}
