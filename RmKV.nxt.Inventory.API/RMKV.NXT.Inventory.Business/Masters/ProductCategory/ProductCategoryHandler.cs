﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Masters.ProductCategory
{

    public class SaveProductCategoryHandler : IRequestHandler<SaveProductCategoryService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public SaveProductCategoryHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(SaveProductCategoryService request, CancellationToken cancellationToken)
        {
            var data = new SaveProductCategoryEntity()
            {
                SaveCategories = request.SaveCategories
            };
            var result = await unitOfWork.ProductCategoryRepository.SaveCategory(data);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetProductCategoryHandler : IRequestHandler<LoadProductCategoryService, List<GetProductCategoryListEntity>>
    {
        private readonly IUnitofwork unitOfWork;

        public GetProductCategoryHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<GetProductCategoryListEntity>> Handle(LoadProductCategoryService request, CancellationToken cancellationToken)
        {

            var data = new GetProductCategoryParmsEntity()
            {
                Group_Section_Id = request.Group_Section_Id
            };
            var result = await unitOfWork.ProductCategoryRepository.GetCategory(data);
            return result;
        }

    }

    public class FetchProductCategoryHandler : IRequestHandler<FerchProductCategoryService, List<FetchProductCategoryListEntity>>
    {
        private readonly IUnitofwork unitOfWork;

        public FetchProductCategoryHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<FetchProductCategoryListEntity>> Handle(FerchProductCategoryService request, CancellationToken cancellationToken)
        {

            var data = new FetchProductCategoryParmsEntity()
            {
                FetchCategory = request.FetchCategory
            };
            var result = await unitOfWork.ProductCategoryRepository.FetchCategory(data);
            return result;
        }

    }

    public class RemoveProductCategoryHandler : IRequestHandler<RemoveProductCategoryService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public RemoveProductCategoryHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(RemoveProductCategoryService request, CancellationToken cancellationToken)
        {
            var data = new RemoveProductCategoryListEntity()
            {
                CancelCategory = request.CancelCategory,

            };
            var result = await unitOfWork.ProductCategoryRepository.RemoveCategory(data);
            unitOfWork.Commit();
            return result;
        }
    }


}
