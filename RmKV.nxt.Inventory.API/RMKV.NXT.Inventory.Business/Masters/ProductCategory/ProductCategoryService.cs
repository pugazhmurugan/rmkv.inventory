﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductCategory
{
   public class SaveProductCategoryService : IRequest<int>
    {
        public string SaveCategories { get; set; }
    }

    public class RemoveProductCategoryService : IRequest<int>
    {
        public string CancelCategory { get; set; }
    }

    public class LoadProductCategoryService : IRequest<List<GetProductCategoryListEntity>>
    {
        public int Group_Section_Id { get; set; }
    }

    public class FerchProductCategoryService : IRequest<List<FetchProductCategoryListEntity>>
    {
        public string FetchCategory { get; set; }
    }
}
