﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductGroupAttributes
{
    public class GetAttributeLookupService : IRequest<string>
    {
        public string GetAttributeLookup { get; set; }
    }

    public class GetProductGroupAttributeListService : IRequest<string>
    {
        public string ProductGroupAttribute { get; set; }
    }

    public class FetchProductGroupAttributeService : IRequest<string>
    {
        public string FetchProductGroupAttribute { get; set; }
    }

    public class GetProductGroupLookupService : IRequest<string>
    {
        public string GetProductGroupLookup { get; set; }
    }

    public class AddProductGroupAttributesService : IRequest<int>
    {
        public string AddGroupAttributes { get; set; }
    }

    public class RemoveProductGroupAttributesService : IRequest<int>
    {
        public string RemoveGroupAttributes { get; set; }
    }
}
