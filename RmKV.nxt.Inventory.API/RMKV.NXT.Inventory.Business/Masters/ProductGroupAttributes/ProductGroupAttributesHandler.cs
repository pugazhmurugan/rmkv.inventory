﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Masters.ProductGroupAttributes
{
    public class GetAttributeLookupHandler : IRequestHandler<GetAttributeLookupService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetAttributeLookupHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetAttributeLookupService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.ProductGroupAttributesRepository.GetAttributeLookup(request.GetAttributeLookup);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetProductGroupLookupHandler : IRequestHandler<GetProductGroupLookupService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductGroupLookupHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetProductGroupLookupService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.ProductGroupAttributesRepository.GetProductGroupLookup(request.GetProductGroupLookup);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetProductGroupAttributeListHandler : IRequestHandler<GetProductGroupAttributeListService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductGroupAttributeListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetProductGroupAttributeListService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.ProductGroupAttributesRepository.GetProductGroupAttributeList(request.ProductGroupAttribute);
            unitOfWork.Commit();
            return result;
        }
    }

    public class FetchProductGroupAttributeHandler : IRequestHandler<FetchProductGroupAttributeService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchProductGroupAttributeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(FetchProductGroupAttributeService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.ProductGroupAttributesRepository.ProductGroupAttributeFetch(request.FetchProductGroupAttribute);
            unitOfWork.Commit();
            return result;
        }
    }

    public class AddProductGroupAttributesHandler : IRequestHandler<AddProductGroupAttributesService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddProductGroupAttributesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<int> Handle(AddProductGroupAttributesService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.ProductGroupAttributesRepository.SaveProductGroupAttributes(request.AddGroupAttributes);
            unitOfWork.Commit();
            return result;
        }
    }

    public class RemoveProductGroupAttributesHandler : IRequestHandler<RemoveProductGroupAttributesService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public RemoveProductGroupAttributesHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<int> Handle(RemoveProductGroupAttributesService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.ProductGroupAttributesRepository.RemoveProductGroupAttributes(request.RemoveGroupAttributes);
            unitOfWork.Commit();
            return result;
        }
    }
}
