﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductGroupAttributes
{
    public class GetAttributeLookupValidation : AbstractValidator<GetAttributeLookupService>
    {

    }

    public class GetProductGroupLookupValidation : AbstractValidator<GetProductGroupLookupService>
    {

    }

    public class AddProductGroupAttributesValidation : AbstractValidator<AddProductGroupAttributesService>
    {

    }

    public class RemoveProductGroupAttributesValidation : AbstractValidator<RemoveProductGroupAttributesService>
    {

    }

    public class FetchProductGroupAttributesValidation : AbstractValidator<FetchProductGroupAttributeService>
    {

    }

    public class GetProductGroupAttributesValidation : AbstractValidator<GetProductGroupAttributeListService>
    {

    }
}
