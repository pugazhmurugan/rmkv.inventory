﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Masters.ProductGroup
{
    public class GetProductGroupHandler : IRequestHandler<GetProductGroupService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductGroupHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetProductGroupService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductGroupRepository.GetProductGroups(request.ProductGroup);
            unitOfWork.Commit();
            return result;
        }
    }
    public class AddProductGroupHandler : IRequestHandler<AddProductGroupService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddProductGroupHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddProductGroupService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductGroupRepository.AddProductGroups(request.ProductGroup);
            unitOfWork.Commit();
            return result;
        }
    }
    public class FetchProductGroupHandler : IRequestHandler<FetchProductGroupService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchProductGroupHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(FetchProductGroupService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductGroupRepository.FetchProductGroups(request.ProductGroup);
            unitOfWork.Commit();
            return result;
        }
    }
    public class DeleteProductGroupHandler : IRequestHandler<DeleteProductGroupService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public DeleteProductGroupHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(DeleteProductGroupService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductGroupRepository.DeleteProductGroups(request.ProductGroup);
            unitOfWork.Commit();
            return result;
        }
    }
}
