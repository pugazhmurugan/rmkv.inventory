﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductGroup
{
    public class GetProductGroupService: IRequest<string>
    {
        public string ProductGroup { get; set; }
    }
    public class AddProductGroupService: IRequest<bool>
    {
        public string ProductGroup { get; set; }
    }
    public class FetchProductGroupService : IRequest<string>
    {
        public string ProductGroup { get; set; }
    }
    public class DeleteProductGroupService: IRequest<bool>
    {
        public string ProductGroup { get; set; }
    }
}
