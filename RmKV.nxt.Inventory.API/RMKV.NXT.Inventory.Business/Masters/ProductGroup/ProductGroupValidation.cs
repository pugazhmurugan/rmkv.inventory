﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.ProductGroup
{
    public class GetProductGroupValidation : AbstractValidator<GetProductGroupService>
    {

    }
    public class AddProductGroupValidation : AbstractValidator<AddProductGroupService>
    {

    }
    public class FetchProductGroupValidation : AbstractValidator<FetchProductGroupService>
    {

    }
    public class DeleteProductGroupValidation : AbstractValidator<DeleteProductGroupService>
    {

    }
}
