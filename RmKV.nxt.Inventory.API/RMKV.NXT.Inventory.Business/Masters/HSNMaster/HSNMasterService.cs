﻿using MediatR;
using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.HSNMaster
{
   public class SaveHSNMasterService : IRequest<int>
    {
        public string SaveHSN { get; set; }
    }

    public class SaveHSNTaxService : IRequest<int>
    {
        public string SaveHSNTax { get; set; }
    }

    public class FetchHSNMasterService : IRequest<List<FetchHSNMasterListEntity>>
    {
        public string FetchHSN { get; set; }
    }

    public class FetchHSNTaxService : IRequest<List<FetchHSNTaxRateListEntity>>
    {
        public string FetchHSNTaxRate { get; set; }
    }

    public class FetchHSNTaxIndividualService : IRequest<List<FetchHSNTaxRateIndividualListEntity>>
    {
        public string FetchHSNTaxRateIndividual { get; set; }
    }

    public class RemoveHSNMasterService : IRequest<int>
    {
        public string CancelHSN { get; set; }
    }

    public class RemoveHSNTaxRateService : IRequest<int>
    {
        public string RemoveHSNTaxRate { get; set; }
    }

    public class GetHSNMasterService : IRequest<List<HSNMasterListEntity>>
    {

    }
}
