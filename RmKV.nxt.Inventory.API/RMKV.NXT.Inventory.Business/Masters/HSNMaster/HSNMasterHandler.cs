﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Masters.HSNMaster
{
   public class HSNMasterHandler : IRequestHandler<SaveHSNMasterService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public HSNMasterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(SaveHSNMasterService request, CancellationToken cancellationToken)
        {
            var data = new SaveHSNMasterEntity()
            {
                SaveHSN = request.SaveHSN
            };
            var result = await unitOfWork.HSNMasterRepository.SaveHSN(data);
            unitOfWork.Commit();
            return result;
        }
    }

    public class HSNTaxHandler : IRequestHandler<SaveHSNTaxService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public HSNTaxHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(SaveHSNTaxService request, CancellationToken cancellationToken)
        {
            var data = new SaveHSNTaxEntity()
            {
                SaveHSNTax = request.SaveHSNTax
            };
            var result = await unitOfWork.HSNMasterRepository.SaveHSNTax(data);
            unitOfWork.Commit();
            return result;
        }
    }

    public class FetchHSNMasterHandler : IRequestHandler<FetchHSNMasterService, List<FetchHSNMasterListEntity>>
    {
        private readonly IUnitofwork unitOfWork;

        public FetchHSNMasterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<FetchHSNMasterListEntity>> Handle(FetchHSNMasterService request, CancellationToken cancellationToken)
        {

            var data = new FetchHSNMasterParamsEntity()
            {
                FetchHSN = request.FetchHSN
            };
            var result = await unitOfWork.HSNMasterRepository.HSNMasterFetch(data);
            return result;
        }

    }

    public class FetchHSNTaxRateHandler : IRequestHandler<FetchHSNTaxService, List<FetchHSNTaxRateListEntity>>
    {
        private readonly IUnitofwork unitOfWork;

        public FetchHSNTaxRateHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<FetchHSNTaxRateListEntity>> Handle(FetchHSNTaxService request, CancellationToken cancellationToken)
        {

            var data = new FetchHSNTaxRateParamsEntity()
            {
                FetchHSNTaxRate = request.FetchHSNTaxRate
            };
            var result = await unitOfWork.HSNMasterRepository.HSNTaxRateFetch(data);
            return result;
        }
    }

    public class FetchHSNTaxRateIndividualHandler : IRequestHandler<FetchHSNTaxIndividualService, List<FetchHSNTaxRateIndividualListEntity>>
    {
        private readonly IUnitofwork unitOfWork;

        public FetchHSNTaxRateIndividualHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<FetchHSNTaxRateIndividualListEntity>> Handle(FetchHSNTaxIndividualService request, CancellationToken cancellationToken)
        {

            var data = new FetchHSNTaxRateIndividualParamsEntity()
            {
                FetchHSNTaxRateIndividual = request.FetchHSNTaxRateIndividual
            };
            var result = await unitOfWork.HSNMasterRepository.HSNMasterFetchIndividual(data);
            return result;
        }
    }

    public class RemoveHSNMasterHandler : IRequestHandler<RemoveHSNMasterService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public RemoveHSNMasterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(RemoveHSNMasterService request, CancellationToken cancellationToken)
        {
            var data = new RemoveHSNMasterEntity()
            {
                CancelHSN = request.CancelHSN,

            };
            var result = await unitOfWork.HSNMasterRepository.RemoveHsnMaster(data);
            unitOfWork.Commit();
            return result;
        }
    }

    public class RemoveHSNTaxRateHandler : IRequestHandler<RemoveHSNTaxRateService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public RemoveHSNTaxRateHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(RemoveHSNTaxRateService request, CancellationToken cancellationToken)
        {
            var data = new RemoveHSNTaxRateEntity()
            {
                RemoveHSNTaxRate = request.RemoveHSNTaxRate,

            };
            var result = await unitOfWork.HSNMasterRepository.RemoveHsnTaxRate(data);
            unitOfWork.Commit();
            return result;
        }
    }

    public class GetHSNMasterHandler : IRequestHandler<GetHSNMasterService, List<HSNMasterListEntity>>
    {
        private readonly IUnitofwork unitOfWork;
        public GetHSNMasterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<HSNMasterListEntity>> Handle(GetHSNMasterService request, CancellationToken cancellationToken)
        {
            var response = await unitOfWork.HSNMasterRepository.GetHSNMasterList();
            return response;
        }

    }

}
