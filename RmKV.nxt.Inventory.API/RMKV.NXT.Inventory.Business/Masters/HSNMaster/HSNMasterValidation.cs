﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Masters.HSNMaster
{
   public class SaveHSNMasterValidation :  AbstractValidator<SaveHSNMasterService>
    {
    }

    public class SaveHSNTaxValidation : AbstractValidator<SaveHSNTaxService>
    {
    }

    public class FetchHSNMasterValidation : AbstractValidator<FetchHSNMasterService>
    {
    }

    public class FetchHSNTaxRateValidation : AbstractValidator<FetchHSNTaxService>
    {
    }

    public class FetchHSNTaxRateIndividualValidation : AbstractValidator<FetchHSNTaxIndividualService>
    {
    }

    public class RemoveHSNMasteValidation : AbstractValidator<RemoveHSNMasterService>
    {
    }

    public class RemoveHSNTaxRateValidation : AbstractValidator<RemoveHSNTaxRateService>
    {
    }

    public class GetHSNMasterValidation : AbstractValidator<GetHSNMasterService>
    {
    }
}
