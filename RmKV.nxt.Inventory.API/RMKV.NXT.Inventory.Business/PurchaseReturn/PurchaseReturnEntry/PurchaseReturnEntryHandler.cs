﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnEntry
{
    public class AddPurchaseReturnEntryHandler : IRequestHandler<AddPurchaseReturnEntryService, int>
    {
        private readonly IUnitofwork unitOfWork;
        public AddPurchaseReturnEntryHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(AddPurchaseReturnEntryService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseReturnRepository.AddPurchaseReturn(request.PurchaseReturnEntry);
            unitOfWork.Commit();
            return result;
        }

    }
    public class ApprovePurchaseReturnEntryHandler : IRequestHandler<ApprovePurchaseReturnEntryService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public ApprovePurchaseReturnEntryHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(ApprovePurchaseReturnEntryService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseReturnRepository.ApprovePurchaseReturn(request.PurchaseReturnEntry);
            unitOfWork.Commit();
            return result;
        }

    }
    public class CancelPurchaseReturnEntryHandler : IRequestHandler<CancelPurchaseReturnEntryService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CancelPurchaseReturnEntryHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(CancelPurchaseReturnEntryService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseReturnRepository.CancelPurchaseReturn(request.PurchaseReturnEntry);
            unitOfWork.Commit();
            return result;
        }

    }
    public class GetPurchaseReturnEntryHandler : IRequestHandler<GetPurchaseReturnEntryService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPurchaseReturnEntryHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetPurchaseReturnEntryService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseReturnRepository.GetPurchaseReturn(request.PurchaseReturnEntry);
            unitOfWork.Commit();
            return result;
        }

    }

    public class FetchPurchaseReturnEntryHandler : IRequestHandler<FetchPurchaseReturnEntryService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchPurchaseReturnEntryHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(FetchPurchaseReturnEntryService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseReturnRepository.FetchPurchaseReturn(request.PurchaseReturnEntry);
            unitOfWork.Commit();
            return result;
        }

    }
}
