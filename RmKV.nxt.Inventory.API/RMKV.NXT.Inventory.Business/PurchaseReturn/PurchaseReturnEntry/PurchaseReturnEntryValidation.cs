﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnEntry
{
   public class AddPurchaseReturnEntryValidation : AbstractValidator<AddPurchaseReturnEntryService>
    {
    }

    public class CancelPurchaseReturnEntryValidation : AbstractValidator<CancelPurchaseReturnEntryService>
    {
    }

    public class GetPurchaseReturnEntryValidation : AbstractValidator<GetPurchaseReturnEntryService>
    {
    }

    public class ApprovePurchaseReturnEntryValidation : AbstractValidator<ApprovePurchaseReturnEntryService>
    {
    }

    public class FetchPurchaseReturnEntryValidation : AbstractValidator<FetchPurchaseReturnEntryService>
    {
    }
}
