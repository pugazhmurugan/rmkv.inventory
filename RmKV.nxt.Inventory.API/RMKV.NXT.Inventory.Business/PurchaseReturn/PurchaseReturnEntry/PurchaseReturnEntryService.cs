﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnEntry
{
    public class AddPurchaseReturnEntryService : IRequest<int>
    {
        public string PurchaseReturnEntry { get; set; }
    }
    public class ApprovePurchaseReturnEntryService : IRequest<bool>
    {
        public string PurchaseReturnEntry { get; set; }
    }
    public class CancelPurchaseReturnEntryService : IRequest<bool>
    {
        public string PurchaseReturnEntry { get; set; }
    }
    public class GetPurchaseReturnEntryService : IRequest<string>
    {
        public string PurchaseReturnEntry { get; set; }
    }

    public class FetchPurchaseReturnEntryService : IRequest<string>
    {
        public string PurchaseReturnEntry { get; set; }
    }
}
