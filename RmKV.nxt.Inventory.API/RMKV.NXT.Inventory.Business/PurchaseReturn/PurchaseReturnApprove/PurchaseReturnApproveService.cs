﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnApprove
{
    public class GetApprovedSupplierService : IRequest<string>
    {
        public string PRApprove { get; set; }
    }
    public class GetPurchaseRetunApprovedListService : IRequest<string>
    {
        public string PRApprove { get; set; }
    }
    public class RemovPurchaseReturnService : IRequest<bool>
    {
        public string PRApprove { get; set; }
    }
    public class GetSupplierCityForApproveService : IRequest<string>
    {
        public string PRApprove { get; set; }
    }
    public class GetSupplierGroupNameForApproveService : IRequest<string>
    {
        public string PRApprove { get; set; }
    }
    public class GetSupplierNameForApproveService : IRequest<string>
    {
        public string PRApprove { get; set; }
    }
    public class GeSupplierDetailsForApproveService : IRequest<string>
    {
        public string PRApprove { get; set; }
    }
    public class ApprovePurchaseReturnListService : IRequest<bool>
    {
        public string PRApprove { get; set; }
    }
}
