﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnApprove
{
    public class GetApprovedSupplierValidation : AbstractValidator<GetApprovedSupplierService>
    {
    }
    public class GetPurchaseRetunApprovedValidation : AbstractValidator<GetPurchaseRetunApprovedListService>
    {
    }
    public class RemovPurchaseReturnValidation : AbstractValidator<RemovPurchaseReturnService>
    {
    }
    public class GetSupplierCityForApproveValidation : AbstractValidator<GetSupplierCityForApproveService>
    {
    }
    public class GetSupplierGroupNameForApproveValidation : AbstractValidator<GetSupplierGroupNameForApproveService>
    {
    }
    public class GetSupplierNameForApproveValidation : AbstractValidator<GetSupplierNameForApproveService>
    {
    }
    public class GeSupplierDetailsForApproveValidation : AbstractValidator<GeSupplierDetailsForApproveService>
    {
    }
    public class ApprovePurchaseReturnListValidation : AbstractValidator<ApprovePurchaseReturnListService>
    {
    }
}
