﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnApprove
{
    public class GetApprovedSupplierHandler : IRequestHandler<GetApprovedSupplierService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetApprovedSupplierHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetApprovedSupplierService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.PurchaseReturnApproveRepository.GetApprovedSupplier(request.PRApprove);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetPurchaseRetunApprovedListHandler : IRequestHandler<GetPurchaseRetunApprovedListService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPurchaseRetunApprovedListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetPurchaseRetunApprovedListService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.PurchaseReturnApproveRepository.GetPurchaseRetunApprovedList(request.PRApprove);
            unitOfWork.Commit();
            return result;
        }
    }
    public class RemovPurchaseReturnHandler : IRequestHandler<RemovPurchaseReturnService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public RemovPurchaseReturnHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(RemovPurchaseReturnService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseReturnApproveRepository.RemovPurchaseReturn(request.PRApprove);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetSupplierCityForApproveHandler : IRequestHandler<GetSupplierCityForApproveService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetSupplierCityForApproveHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetSupplierCityForApproveService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.PurchaseReturnApproveRepository.GetSupplierCityForApprove(request.PRApprove);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetSupplierGroupNameForApproveHandler : IRequestHandler<GetSupplierGroupNameForApproveService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetSupplierGroupNameForApproveHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetSupplierGroupNameForApproveService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.PurchaseReturnApproveRepository.GetSupplierGroupNameForApprove(request.PRApprove);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetSupplierNameForApproveHandler : IRequestHandler<GetSupplierNameForApproveService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetSupplierNameForApproveHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetSupplierNameForApproveService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.PurchaseReturnApproveRepository.GetSupplierNameForApprove(request.PRApprove);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GeSupplierDetailsForApproveHandler : IRequestHandler<GeSupplierDetailsForApproveService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GeSupplierDetailsForApproveHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GeSupplierDetailsForApproveService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.PurchaseReturnApproveRepository.GeSupplierDetailsForApprove(request.PRApprove);
            unitOfWork.Commit();
            return result;
        }
    }
    public class ApprovePurchaseReturnListHandler : IRequestHandler<ApprovePurchaseReturnListService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public ApprovePurchaseReturnListHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(ApprovePurchaseReturnListService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseReturnApproveRepository.ApprovePurchaseReturnList(request.PRApprove);
            unitOfWork.Commit();
            return result;
        }
    }
}
