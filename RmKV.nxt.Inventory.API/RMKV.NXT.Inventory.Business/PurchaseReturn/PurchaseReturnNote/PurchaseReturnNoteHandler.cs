﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn
{
    public class GetPurchaseReturnNoteHandler : IRequestHandler<GetPurchaseReturnNoteService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPurchaseReturnNoteHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetPurchaseReturnNoteService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseReturnNoteRepository.GetPurchaseReturnNote(request.PurchaseReturnNote);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetPurchaseReturnByNoHandler : IRequestHandler<GetPurchaseReturnByNoService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPurchaseReturnByNoHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetPurchaseReturnByNoService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseReturnNoteRepository.GetPurchaseReturnByNos(request.PurchaseReturnNote);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class GetPurchaseReturnByNoDetailsHandler : IRequestHandler<GetPurchaseReturnByNoDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPurchaseReturnByNoDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetPurchaseReturnByNoDetailsService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseReturnNoteRepository.GetPurchaseReturnByNoDetails(request.PurchaseReturnNote);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class AddPurchaseReturnNoteHandler : IRequestHandler<AddPurchaseReturnNoteService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddPurchaseReturnNoteHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddPurchaseReturnNoteService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseReturnNoteRepository.AddPurchaseReturnNote(request.PurchaseReturnNote);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class FetchPurchaseReturnNoteHandler : IRequestHandler<FetchPurchaseReturnNoteService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchPurchaseReturnNoteHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(FetchPurchaseReturnNoteService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.PurchaseReturnNoteRepository.FetchPurchaseReturnNote(request.PurchaseReturnNote);
            //unitOfWork.Commit();
            return result;
        }
    }
}
