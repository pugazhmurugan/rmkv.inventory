﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn
{
    public class GetPurchaseReturnNoteService: IRequest<string>
    {
        public string PurchaseReturnNote { get; set; }
    }
    public class GetPurchaseReturnByNoService: IRequest<string>
    {
        public string PurchaseReturnNote { get; set; }
    }
    public class GetPurchaseReturnByNoDetailsService : IRequest<string>
    {
        public string PurchaseReturnNote { get; set; }
    }
    public class AddPurchaseReturnNoteService: IRequest<bool>
    {
        public string PurchaseReturnNote { get; set; }
    }
    public class FetchPurchaseReturnNoteService: IRequest<string>
    {
        public string PurchaseReturnNote { get; set; }
    }

}
