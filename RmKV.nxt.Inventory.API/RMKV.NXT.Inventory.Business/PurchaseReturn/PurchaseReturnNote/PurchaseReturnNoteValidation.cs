﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn
{
    public class GetPurchaseReturnNoteValidation : AbstractValidator<GetPurchaseReturnNoteService>
    {

    }
    public class GetPurchaseReturnByNoValidation : AbstractValidator<GetPurchaseReturnByNoService>
    {

    }
    public class GetPurchaseReturnByNoDetailsValidation : AbstractValidator<GetPurchaseReturnByNoDetailsService>
    {

    }
    public class AddPurchaseReturnNoteValidation: AbstractValidator<AddPurchaseReturnNoteService>
    {

    }
    public class FetchPurchaseReturnNoteValidation: AbstractValidator<FetchPurchaseReturnNoteService>
    {

    }
}
