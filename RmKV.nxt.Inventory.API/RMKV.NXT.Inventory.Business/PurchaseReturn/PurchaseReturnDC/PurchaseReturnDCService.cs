﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnDC
{
    public class LoadPurchaseReturnDCService : IRequest<string>
    {
        public string LoadReturnDC { get; set; }
    }

    public class FetchPurchaseReturnDCDetailsService : IRequest<string>
    {
        public string FetchReturnDC { get; set; }
    }

    public class GeneratePurchaseReturnDCNoService : IRequest<int>
    {
        public string GenerateDCNo { get; set; }
    }
}
