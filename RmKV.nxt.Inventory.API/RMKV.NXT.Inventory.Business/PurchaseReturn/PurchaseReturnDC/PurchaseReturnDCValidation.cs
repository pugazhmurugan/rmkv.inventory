﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnDC
{
    public class LoadPurchaseReturnValidation : AbstractValidator<LoadPurchaseReturnDCService>
    {
    }

    public class FetchPurchaseReturnDCDetailsValidation : AbstractValidator<FetchPurchaseReturnDCDetailsService>
    {
    }

    public class GeneratePurchaseReturnDCNoValidation : AbstractValidator<GeneratePurchaseReturnDCNoService>
    {
    }
}
