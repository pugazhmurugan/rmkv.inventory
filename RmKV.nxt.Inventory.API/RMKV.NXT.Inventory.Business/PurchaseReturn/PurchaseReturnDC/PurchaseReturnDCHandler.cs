﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn.PurchaseReturnDC
{
    public class LoadPurchaseReturnDCHandler : IRequestHandler<LoadPurchaseReturnDCService, string>
    {
        private readonly IUnitofwork unitOfWork;

        public LoadPurchaseReturnDCHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(LoadPurchaseReturnDCService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.PurchaseReturnDCRepository.LoadPurchaseReturnDCDetails(request.LoadReturnDC);
            return result;
        }
    }

    public class FetchPurchaseReturnDCDetailsHandler : IRequestHandler<FetchPurchaseReturnDCDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;

        public FetchPurchaseReturnDCDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(FetchPurchaseReturnDCDetailsService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.PurchaseReturnDCRepository.FetchPurchaseReturnDCDetails(request.FetchReturnDC);
            return result;
        }
    }

    public class GeneratePurchaseReturnDCNoHandler : IRequestHandler<GeneratePurchaseReturnDCNoService, int>
    {
        private readonly IUnitofwork unitOfWork;

        public GeneratePurchaseReturnDCNoHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(GeneratePurchaseReturnDCNoService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.PurchaseReturnDCRepository.GeneratePurchaseReturnDCNo(request.GenerateDCNo);
            return result;
        }
    }
}
