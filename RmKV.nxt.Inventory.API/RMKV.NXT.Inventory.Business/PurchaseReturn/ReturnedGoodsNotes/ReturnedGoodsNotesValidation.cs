﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn.ReturnedGoodsNotes
{
    public class GetRGNWiseDCDetailsValidation : AbstractValidator<GetRGNWiseDCDetailsService>
    {
    }
    public class AddRGNWiseDetailsValidation : AbstractValidator<AddRGNWiseDetailsService>
    {
    }
    public class GetRGNWiseDetailsValidation : AbstractValidator<GetRGNWiseDetailsService>
    {
    }
    public class FetchRGNWiseDetailsValidation : AbstractValidator<FetchRGNWiseDetailsService>
    {
    }
    public class GetDCDetailsValidation : AbstractValidator<GetDCDetailsService>
    {
    }
    public class GetTransporterValidation : AbstractValidator<GetTransporterService>
    {
    }
}
