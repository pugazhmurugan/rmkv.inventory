﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn.ReturnedGoodsNotes
{
    public class GetRGNWiseDCDetailsService : IRequest<string>
    {
        public string RGNDetails { get; set; }
    }
    public class GetRGNWiseDetailsService : IRequest<string>
    {
        public string RGNDetails { get; set; }
    }
    public class FetchRGNWiseDetailsService : IRequest<string>
    {
        public string RGNDetails { get; set; }
    }
    public class AddRGNWiseDetailsService : IRequest<bool>
    {
        public string RGNDetails { get; set; }
    }
    public class GetDCDetailsService : IRequest<string>
    {
        public string RGNDetails { get; set; }
    }
    public class GetTransporterService : IRequest<string>
    {
        public string RGNDetails { get; set; }
    }
}
