﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.PurchaseReturn.ReturnedGoodsNotes
{
    public class GetRGNWiseDCDetailsHandler : IRequestHandler<GetRGNWiseDCDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetRGNWiseDCDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetRGNWiseDCDetailsService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.ReturnedGoodsNotesRepository.GetRGNWiseDCDetails(request.RGNDetails);
            unitOfWork.Commit();
            return result;
        }
    }
    public class AddRGNWiseDetailsHandler : IRequestHandler<AddRGNWiseDetailsService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddRGNWiseDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddRGNWiseDetailsService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.ReturnedGoodsNotesRepository.AddRGNWiseDetails(request.RGNDetails);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetRGNWiseDetailsHandler : IRequestHandler<GetRGNWiseDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetRGNWiseDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetRGNWiseDetailsService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.ReturnedGoodsNotesRepository.GetRGNWiseDetails(request.RGNDetails);
            unitOfWork.Commit();
            return result;
        }
    }
    public class FetchRGNWiseDetailsHandler : IRequestHandler<FetchRGNWiseDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public FetchRGNWiseDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(FetchRGNWiseDetailsService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.ReturnedGoodsNotesRepository.FetchRGNWiseDetails(request.RGNDetails);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetDCDetailsHandler : IRequestHandler<GetDCDetailsService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetDCDetailsHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetDCDetailsService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.ReturnedGoodsNotesRepository.GetDCDetails(request.RGNDetails);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetTransporterHandler : IRequestHandler<GetTransporterService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetTransporterHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetTransporterService request, CancellationToken cancellationToken)
        {

            var result = await unitOfWork.ReturnedGoodsNotesRepository.GetTransporter(request.RGNDetails);
            unitOfWork.Commit();
            return result;
        }
    }
}
