﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Tools.SupplierDescriptionChange
{
    public class AddSupplierDescriptionService : IRequest<bool>
    {
        public string SupplierDescriptionChange { get; set; }
    }
}
