﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Tools.SupplierDescriptionChange
{
    public class AddSupplierDescriptionHandler : IRequestHandler<AddSupplierDescriptionService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddSupplierDescriptionHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddSupplierDescriptionService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductDescriptionChangeRepository.addsupplierDescriptionChange(request.SupplierDescriptionChange);
            unitOfWork.Commit();
            return result;
        }
    }
}
