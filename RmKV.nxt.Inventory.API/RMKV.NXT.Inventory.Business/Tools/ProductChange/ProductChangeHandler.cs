﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Tools.ProductChange
{
    public class GetProductChangeHandler : IRequestHandler<GetProductChangeService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetProductChangeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetProductChangeService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductChangeRepository.GetProductChanges(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetStyleChangeHandler : IRequestHandler<GetStyleChangeService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetStyleChangeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetStyleChangeService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductChangeRepository.GetStyleChanges(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetCounterChangeHandler : IRequestHandler<GetCounterChangeService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetCounterChangeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<string> Handle(GetCounterChangeService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductChangeRepository.GetCounterChanges(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }
    }


    public class counterchangesaveHandler : IRequestHandler<counterchangesaveService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public counterchangesaveHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(counterchangesaveService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductChangeRepository.counterchangesave(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }
    }

    public class productchangesaveHandler : IRequestHandler<productchangesaveService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public productchangesaveHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(productchangesaveService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductChangeRepository.productchangesave(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }
    }

    public class stylecodechangesaveHandler : IRequestHandler<stylecodechangesaveService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public stylecodechangesaveHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(stylecodechangesaveService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductChangeRepository.stylecodechangesave(request.ProductMaster);
            unitOfWork.Commit();
            return result;
        }
    }

    //////Report

    public class GetCounterChangeHistoryReportHandler : IRequestHandler<GetCounterChangeHistoryReportServices, DataSet>
    {
        private readonly IUnitofwork unitOfWork;

        public GetCounterChangeHistoryReportHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<DataSet> Handle(GetCounterChangeHistoryReportServices request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductChangeRepository.GetCounterChangeHistoryReport(request.CounterChangeHistoryReport);
            this.unitOfWork.Commit();
            return result;
        }

    }
    public class GetProductChangeHistoryReportHandler : IRequestHandler<GetProductChangeHistoryReportServices, DataSet>
    {
        private readonly IUnitofwork unitOfWork;

        public GetProductChangeHistoryReportHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<DataSet> Handle(GetProductChangeHistoryReportServices request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ProductChangeRepository.GetProductChangeHistoryReport(request.ProductChangeHistoryReport);
            this.unitOfWork.Commit();
            return result;
        }

    }
}