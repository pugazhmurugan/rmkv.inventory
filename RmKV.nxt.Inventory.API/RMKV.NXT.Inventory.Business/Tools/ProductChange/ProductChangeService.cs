﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Tools.ProductChange
{
    public class GetProductChangeService : IRequest<string>
    {
        public string ProductMaster { get; set; }
    }
    public class GetStyleChangeService : IRequest<string>
    {
        public string ProductMaster { get; set; }

    }
    public class GetCounterChangeService : IRequest<string>
    {
        public string ProductMaster { get; set; }
    }

    public class counterchangesaveService : IRequest<bool>
    {
        public string ProductMaster { get; set; }
    }

    public class productchangesaveService : IRequest<bool>
    {
        public string ProductMaster { get; set; }
    }

    public class stylecodechangesaveService : IRequest<bool>
    {
        public string ProductMaster { get; set; }
    }
    ////Report
    public class GetCounterChangeHistoryReportServices : IRequest<DataSet>
    {
        public string CounterChangeHistoryReport { get; set; }
    }
    public class GetProductChangeHistoryReportServices : IRequest<DataSet>
    {
        public string ProductChangeHistoryReport { get; set; }
    }
}
