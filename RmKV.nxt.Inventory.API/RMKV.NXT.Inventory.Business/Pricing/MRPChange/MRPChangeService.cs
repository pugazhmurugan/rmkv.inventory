﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Pricing
{
    public class AddMRPChangeService: IRequest<bool>
    {
        public string MRPChange { get; set; }
    }
}
