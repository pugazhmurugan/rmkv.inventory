﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Pricing
{
    public class AddMRPChangeHandler : IRequestHandler<AddMRPChangeService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddMRPChangeHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddMRPChangeService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.MRPChangeRepository.AddMrpChanges(request.MRPChange);
            unitOfWork.Commit();
            return result;
        }
    }
}
