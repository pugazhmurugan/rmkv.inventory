﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Pricing.SellingPriceChange
{
    public class GetSingleProductValidation : AbstractValidator<GetSingleProductService>
    {

    }
    public class AddSingleProductValidation : AbstractValidator<AddSingleProductService>
    {

    }
    public class GetPriceChangeHistoryReportValidations : AbstractValidator<GetPriceChangeHistoryReportService>
    {

    }
}
