﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Pricing.SellingPriceChange
{
    public class GetSingleProductService : IRequest<string>
    {
        public string SingleProduct { get; set; }
    }
    public class AddSingleProductService : IRequest<bool>
    {
        public string SingleProduct { get; set; }
    }

    public class GetPriceChangeHistoryReportService : IRequest<DataSet>
    {
        public string SingleProduct { get; set; }
    }
}
