﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Pricing.SellingPriceChange
{
    public class GetSingleProductHandler : IRequestHandler<GetSingleProductService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetSingleProductHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetSingleProductService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.SingleProductRepository.GetSingleProduct(request.SingleProduct);
            //unitOfWork.Commit();
            return result;
        }
    }
    public class AddSingleProductHandler : IRequestHandler<AddSingleProductService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddSingleProductHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddSingleProductService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.SingleProductRepository.AddSingleProduct(request.SingleProduct);
            unitOfWork.Commit();
            return result;
        }
    }


    public class GetPriceChangeHistoryReportHandler : IRequestHandler<GetPriceChangeHistoryReportService, DataSet>
    {
        private readonly IUnitofwork unitOfWork;
        public GetPriceChangeHistoryReportHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<DataSet> Handle(GetPriceChangeHistoryReportService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.SingleProductRepository.GetPriceChangeHistoryReport(request.SingleProduct);
            return result;
        }

    }

}
