﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Pricing.DoubleRate
{
    public class AddByAmountValidation: AbstractValidator<AddByAmountService>
    {

    }
    public class AddByPercentageValidation : AbstractValidator<AddByPercentageService>
    {

    }
    public class CheckMarginValidation : AbstractValidator<CheckMarginService>
    {

    }
    public class GetMarginCalculateValidation : AbstractValidator<GetMarginCalculateService>
    {

    }
}
