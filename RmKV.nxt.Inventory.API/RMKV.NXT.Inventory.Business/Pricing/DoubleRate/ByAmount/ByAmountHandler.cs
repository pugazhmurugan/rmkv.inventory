﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.Pricing.DoubleRate
{
    public class AddByAmountHandler : IRequestHandler<AddByAmountService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddByAmountHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddByAmountService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ByAmountRepository.AddByAmount(request.ByAmount);
            unitOfWork.Commit();
            return result;
        }
    }
    public class AddByPercentageHandler : IRequestHandler<AddByPercentageService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public AddByPercentageHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(AddByPercentageService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ByAmountRepository.AddByPercentage(request.ByPercentage);
            unitOfWork.Commit();
            return result;
        }
    }
    public class CheckMarginHandler : IRequestHandler<CheckMarginService, bool>
    {
        private readonly IUnitofwork unitOfWork;
        public CheckMarginHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(CheckMarginService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ByAmountRepository.CheckMargin(request.Margin);
            unitOfWork.Commit();
            return result;
        }
    }
    public class GetMarginCalculateHandler : IRequestHandler<GetMarginCalculateService, string>
    {
        private readonly IUnitofwork unitOfWork;
        public GetMarginCalculateHandler(IUnitofwork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<string> Handle(GetMarginCalculateService request, CancellationToken cancellationToken)
        {
            var result = await unitOfWork.ByAmountRepository.GetDoubleRateMarginCalculation(request.Margin);
            //unitOfWork.Commit();
            return result;
        }
    }
}
