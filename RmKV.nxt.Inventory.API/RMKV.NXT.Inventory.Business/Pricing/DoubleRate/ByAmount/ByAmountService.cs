﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.Pricing.DoubleRate
{
    public class AddByAmountService : IRequest<bool>
    {
        public string ByAmount { get; set; }
    }
    public class AddByPercentageService: IRequest<bool>
    {
        public string ByPercentage { get; set; }
    }
    public class CheckMarginService: IRequest<bool>
    {
        public string Margin { get; set; }
    }
    public class GetMarginCalculateService: IRequest<string>
    {
        public string Margin { get; set; }
    }
}
