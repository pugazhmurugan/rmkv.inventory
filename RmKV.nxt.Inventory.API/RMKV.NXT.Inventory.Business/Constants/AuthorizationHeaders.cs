﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RmKV.NXT.Inventory.Api.Constants
{
   public class AuthorizationHeaders
    {
        public const string TerritoryHeaderName = "x-territory-key";
        public const string HeaderName = "Authorization";
    }
}
