﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RmKV.NXT.Inventory.Api.Constants
{
   public class AuthenticationConstants
    {
        public const string ClaimsTerritoryId = "TerritoryId";
        public const string ClaimsRoleType = "Role";
        public const string ClaimsIdentityBearer = "Bearer";
    }
}
