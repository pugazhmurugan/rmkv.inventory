﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.ApiClients.ControlPanelApi
{
    public class ControlPanelApiClients : IControlPanelApiClients
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        private Uri BaseAddress { get; }

        public ControlPanelApiClients(Uri baseAddress, IHttpContextAccessor httpContextAccessor)
        {
            BaseAddress = baseAddress;
            this.httpContextAccessor = httpContextAccessor;
        }
    }
}
