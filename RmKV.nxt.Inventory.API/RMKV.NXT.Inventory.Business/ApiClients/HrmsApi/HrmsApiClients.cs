﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RmKV.NXT.Inventory.DataModel.Models.Shared;
using RMKV.NXT.Inventory.ApiClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.ApiClients.HrmsApi
{
    public class HrmsApiClients : IHrmsApiClients
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        private Uri BaseAddress { get; }

        public HrmsApiClients(Uri baseAddress, IHttpContextAccessor httpContextAccessor)
        {
            BaseAddress = baseAddress;
            this.httpContextAccessor = httpContextAccessor;
        }

        public async Task<List<GetEmployeeLookupEntity>> GetEmployeeLookup(GetEmployeeLookupParams employeeLookupParams)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            using (var response = await client.PostAsync($"Shared/GetEmployeeLookup", new StringContent(JsonConvert.SerializeObject(employeeLookupParams), Encoding.UTF8, "application/json")))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<List<GetEmployeeLookupEntity>>(content);
                return result.ToList();
            }
        }


    }


       
    
}
