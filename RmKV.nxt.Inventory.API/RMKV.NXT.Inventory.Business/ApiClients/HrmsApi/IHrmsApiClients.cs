﻿using RmKV.NXT.Inventory.DataModel.Models.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.ApiClients.HrmsApi
{
    public interface IHrmsApiClients
    {
        Task<List<GetEmployeeLookupEntity>> GetEmployeeLookup(GetEmployeeLookupParams employeeLookupParams);
    }
}
