﻿using RmKV.NXT.Inventory.DataModel.Model.Shared;
using RmKV.NXT.Inventory.DataModel.Model.Shared.Company;
using RmKV.NXT.Inventory.DataModel.Model.Shared.HRDocumentRootPathAndUrl;
using RmKV.NXT.Inventory.DataModel.Model.Shared.SalesLocation;
using RMKV.NXT.Inventory.DataModel.Model.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.ApiClients.CommonApi
{
    public interface ICommonApiClients
    {
        Task<string> GetUserProfile(int userProfile);
        Task<List<GetSalesLocationEntity>> GetSalesLocation(SalesLocationParams obj);
        Task<List<GetSalesLocationEntity>> GetAllSalesLocation(string SalesLocation);
        Task<List<GetCompanyEntity>> GetCompany(CompanyParams values);
        Task<GetGRNDocumentRootPathAndUrlEntity> GetGRNDocumentRootPathAndUrl(int salesLocationId);
        Task<List<GetSectionsEntity>> GetGRNSections(string warehouse);
        Task<string> GetGRNAccountsLookup(string fieldValue);
        Task<List<GetStateEntity>> GetStates();
        Task<List<GetGroupSectionsEntity>> GetGroupSections();
        Task<string> GetByNoDetails(string fieldValue);
        Task<string> GetGlobalParams(string fieldValue); 
        Task<string> GetFilePath(string fieldValue);
        Task<string> GetDocumentPathView(string fieldValue);
    }
}
