﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RmKV.NXT.Inventory.DataModel.Model.Shared;
using RmKV.NXT.Inventory.DataModel.Model.Shared.Company;
using RmKV.NXT.Inventory.DataModel.Model.Shared.HRDocumentRootPathAndUrl;
using RmKV.NXT.Inventory.DataModel.Model.Shared.SalesLocation;
using RMKV.NXT.Inventory.ApiClients;
using RMKV.NXT.Inventory.DataModel.Model.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.ApiClients.CommonApi
{
    public class CommonApiClients : ICommonApiClients
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        private Uri BaseAddress { get; }

        public CommonApiClients(Uri baseAddress, IHttpContextAccessor httpContextAccessor)
        {
            BaseAddress = baseAddress;
            this.httpContextAccessor = httpContextAccessor;
        }

        public async Task<List<GetSalesLocationEntity>> GetSalesLocation(SalesLocationParams obj)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            using (var response = await client.PostAsync($"Shared/GetSalesLocations", new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<List<GetSalesLocationEntity>>(content);
                return result.ToList();
            }
        }

        public async Task<List<GetSalesLocationEntity>> GetAllSalesLocation(string salesLocation)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            using (var response = await client.GetAsync($"Shared/GetAllSalesLocations?salesLocation={salesLocation}"))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<List<GetSalesLocationEntity>>(content);
                return result.ToList();
            }
        }

        public async Task<List<GetCompanyEntity>> GetCompany(CompanyParams values)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            using (var response = await client.PostAsync($"Shared/GetCompanies", new StringContent(JsonConvert.SerializeObject(values), Encoding.UTF8, "application/json")))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<List<GetCompanyEntity>>(content);
                return result.ToList();
            }
        }

        public async Task<GetGRNDocumentRootPathAndUrlEntity> GetGRNDocumentRootPathAndUrl(int salesLocationId)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            using (var response = await client.GetAsync($"Shared/GetGRNDocumentRootPathAndUrl?Sales_Location_Id={salesLocationId}"))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<GetGRNDocumentRootPathAndUrlEntity>(content);
                return result;
            }
        }

        public async Task<string> GetUserProfile(int userProfile)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            using (var response = await client.GetAsync($"Shared/GetUserProfile?userProfile={userProfile}"))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                return await response.Content.ReadAsStringAsync();
                //var result = JsonConvert.DeserializeObject<string>(content);
                //return result;
            }
        }
     
        public async Task<List<GetSectionsEntity>> GetGRNSections(string warehouse)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            using (var response = await client.GetAsync($"Shared/GetGRNSectionsByWarehouseId?GetGRNSections={warehouse}"))
            {
                List<GetSectionsEntity> result = null;
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    result = null;
                }
                else if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                if (content != null)
                    result = JsonConvert.DeserializeObject<List<GetSectionsEntity>>(content);
                return result;
            }
        }
        public async Task<string> GetGRNAccountsLookup(string fieldValue)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            using (var response = await client.GetAsync($"Shared/GetGRNAccountsLookup?GetLookup={fieldValue}"))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                return await response.Content.ReadAsStringAsync();
                //var result = JsonConvert.DeserializeObject<string>(content);
                //return result;
            }
        }

        public async Task<List<GetStateEntity>> GetStates()
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            using (var response = await client.GetAsync($"Shared/GetState"))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }
                List<GetStateEntity> result = null;
                string content = await response.Content.ReadAsStringAsync();
                if (content != null)
                    result = JsonConvert.DeserializeObject<List<GetStateEntity>>(content);
                return result;
            }
        }
        public async Task<List<GetGroupSectionsEntity>> GetGroupSections()
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            using (var response = await client.GetAsync($"Shared/GetGroupSections"))
            {
                List<GetGroupSectionsEntity> result = null;
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    result = null;
                }
                else if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }
                string content = await response.Content.ReadAsStringAsync();
                if (content != null)
                    result = JsonConvert.DeserializeObject<List<GetGroupSectionsEntity>>(content);
                return result;
            }
        }

        public async Task<string> GetByNoDetails(string value)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            var data = new BynoDataModel()
            {
                ByNoData = value
            };

            using (var response = await client.PostAsync($"Shared/GetByNoDetails?ByNoData", new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json")))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                return await response.Content.ReadAsStringAsync();
            }
        }
        public async Task<string> GetGlobalParams(string value)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            var data = new GlobalData()
            {
                globalParams = value
            };

            using (var response = await client.PostAsync($"Shared/GetGlobalParams?globalParams", new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json")))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                return await response.Content.ReadAsStringAsync();
            }
        }


        public async Task<string> GetFilePath(string value)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            var data = new GetDocumentPath()
            {
                documentPath = value
            };

            using (var response = await client.PostAsync($"Shared/GetDocumentPath?documentPath", new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json")))
            {
               string result = null;
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    result = null;
                }
                else if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }
                 result = await response.Content.ReadAsStringAsync();
                return result;
            }
        }
        public async Task<string> GetDocumentPathView(string value)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            var data = new GetDocumentPath()
            {
                documentPath = value
            };

            using (var response = await client.PostAsync($"Shared/GetDocumentPathView?documentPath", new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json")))
            {
                string result = null;
                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    result = null;
                }
                else if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }
                result = await response.Content.ReadAsStringAsync();
                return result;
            }
        }

    }
}
