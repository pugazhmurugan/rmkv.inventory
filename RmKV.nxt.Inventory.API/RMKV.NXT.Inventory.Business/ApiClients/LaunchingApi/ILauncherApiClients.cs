﻿using RMKV.NXT.Inventory.DataModel.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.ApiClients.LaunchingApi
{
    public interface ILauncherApiClients
    {
        Task<List<MenuItemsEntity>> GetMenus(GetAllMenusParams menusParams);
    }
}
