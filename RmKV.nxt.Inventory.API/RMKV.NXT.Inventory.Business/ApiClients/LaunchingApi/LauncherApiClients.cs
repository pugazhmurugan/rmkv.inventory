﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RMKV.NXT.Inventory.ApiClients;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.ApiClients.LaunchingApi
{
    public class LauncherApiClients: ILauncherApiClients
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        private Uri BaseAddress { get; }

        public LauncherApiClients(Uri baseAddress, IHttpContextAccessor httpContextAccessor)
        {
            BaseAddress = baseAddress;
            this.httpContextAccessor = httpContextAccessor;
        }

        public async Task<List<MenuItemsEntity>> GetMenus(GetAllMenusParams menusParams)
        {
            var client = new HttpClient { BaseAddress = BaseAddress };

            var authorizationRequestHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (authorizationRequestHeader.Any())
            {
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse(authorizationRequestHeader);
            }

            using (var response = await client.GetAsync($"Connect/GetMenus?user_ID={menusParams.User_ID}&module_ID={menusParams.Module_ID}&company_ID={menusParams.Company_ID}&sales_Location_ID={menusParams.Sales_Location_ID}&section_ID={menusParams.Section_ID}&wareHouse_ID={ menusParams.WareHouse_ID}"))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApiClientException(response);
                }

                string content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<List<MenuItemsEntity>>(content);
                return result.ToList();
            }
        }
    }
}
