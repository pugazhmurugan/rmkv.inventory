﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.ApiClients.GRNApi
{
    public class GRNApiClients : IGRNApiClients
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        private Uri BaseAddress { get; }

        public GRNApiClients(Uri baseAddress, IHttpContextAccessor httpContextAccessor)
        {
            BaseAddress = baseAddress;
            this.httpContextAccessor = httpContextAccessor;
        }
    }
}
