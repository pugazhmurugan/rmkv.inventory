﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.Business.EwayBillGeneration
{
    public class EwayBillLoginService : IRequest<string>
    {
        public string action { get; set; }
        public string userid { get; set; }
        public string password { get; set; }
        public string App { get; set; }
        public string prod { get; set; }
        public string client { get; set; }
        public string value { get; set; }
    }
    public class EwayBillValidateUserService : IRequest<string>
    {
        public string action { get; set; }
        public string userid { get; set; }
        public string gstin { get; set; }
        public string uid { get; set; }
        public string password { get; set; }
        public string token { get; set; }
        public string App { get; set; }
        public string prod { get; set; }
        public string client { get; set; }
        public string value { get; set; }
    }
    public class EwayBillDetailsService : IRequest<string>
    {
        public string action { get; set; }
        public string userid { get; set; }
        public string gstin { get; set; }
        public string action2 { get; set; }
        public string irnno { get; set; }
        public string token { get; set; }
        public string App { get; set; }
        public string prod { get; set; }
        public string client { get; set; }
        public string value { get; set; }
    }
    public class GenerateEwayBillandEInvoiceService : IRequest<string>
    {
        public string action { get; set; }
        public string userid { get; set; }
        public string gstin { get; set; }
        public string action2 { get; set; }
        public string token { get; set; }
        public string App { get; set; }
        public string prod { get; set; }
        public string client { get; set; }
        public string value { get; set; }
        public object parameter { get; set; }
    }
    public class GenerateEwayBillService : IRequest<string>
    {
        public string action { get; set; }
        public string userid { get; set; }
        public string gstin { get; set; }
        public string action2 { get; set; }
        public string token { get; set; }
        public string App { get; set; }
        public string prod { get; set; }
        public string client { get; set; }
        public string value { get; set; }
        public object parameter { get; set; }
    }
    public class CancelEwayBillandEInvoiceService : IRequest<string>
    {
        public string action { get; set; }
        public string userid { get; set; }
        public string gstin { get; set; }
        public string action2 { get; set; }
        public string token { get; set; }
        public string App { get; set; }
        public string prod { get; set; }
        public string client { get; set; }
        public string value { get; set; }
        public object parameter { get; set; }
    }
    public class CancelEwayBillService : IRequest<string>
    {
        public string action { get; set; }
        public string userid { get; set; }
        public string gstin { get; set; }
        public string action2 { get; set; }
        public string token { get; set; }
        public string App { get; set; }
        public string prod { get; set; }
        public string client { get; set; }
        public string value { get; set; }
        public object parameter { get; set; }
    }
}
