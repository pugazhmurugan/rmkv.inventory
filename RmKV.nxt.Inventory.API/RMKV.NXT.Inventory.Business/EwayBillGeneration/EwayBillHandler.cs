﻿using MediatR;
using RMKV.NXT.Inventory.DataContext.InterfaceRepository.EwayBillGeneration;
using RMKV.NXT.Inventory.DataContext.UintofWorks;
using RMKV.NXT.Inventory.DataModel.Model.EwayBill;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RMKV.NXT.Inventory.Business.EwayBillGeneration
{
    public class EwayBillLoginHandler : IRequestHandler<EwayBillLoginService, string>
    {
        private readonly IEwayBillRepository ewayBill;

        public EwayBillLoginHandler(IEwayBillRepository ewayBill)
        {
            this.ewayBill = ewayBill;
        }
        public async Task<string> Handle(EwayBillLoginService request, CancellationToken cancellationToken)
        {
            var data = new EwayBillEntity()
            {
                action = request.action,
                userid = request.userid,
                password = request.password,
                App = request.App,
                prod = request.prod,
                client = request.client,
                value = request.value
            };
            var result = await ewayBill.EwayBillLogin(data);
            return result;
        }
    }
    public class EwayBillValidateUserHandler : IRequestHandler<EwayBillValidateUserService, string>
    {
        private readonly IEwayBillRepository ewayBill;

        public EwayBillValidateUserHandler(IEwayBillRepository ewayBill)
        {
            this.ewayBill = ewayBill;
        }
        public async Task<string> Handle(EwayBillValidateUserService request, CancellationToken cancellationToken)
        {
            var data = new EwayBillEntity()
            {
                action = request.action,
                userid = request.userid,
                gstin = request.gstin,
                uid = request.uid,
                password = request.password,
                token = request.token,
                App = request.App,
                prod = request.prod,
                client = request.client,
                value = request.value
            };
            var result = await ewayBill.EwayBillValidateUser(data);
            return result;
        }
    }
    public class EwayBillDetailsUserHandler : IRequestHandler<EwayBillDetailsService, string>
    {
        private readonly IEwayBillRepository ewayBill;

        public EwayBillDetailsUserHandler(IEwayBillRepository ewayBill)
        {
            this.ewayBill = ewayBill;
        }
        public async Task<string> Handle(EwayBillDetailsService request, CancellationToken cancellationToken)
        {
            var data = new EwayBillEntity()
            {
                action = request.action,
                userid = request.userid,
                gstin = request.gstin,
                action2 = request.action2,
                irnno = request.irnno,
                token = request.token,
                App = request.App,
                prod = request.prod,
                client = request.client,
                value = request.value
            };
            var result = await ewayBill.EwayBillDetails(data);
            return result;
        }
    }
    public class GenerateEwayBillandEInvoiceHandler : IRequestHandler<GenerateEwayBillandEInvoiceService, string>
    {
        private readonly IEwayBillRepository ewayBill;

        public GenerateEwayBillandEInvoiceHandler(IEwayBillRepository ewayBill)
        {
            this.ewayBill = ewayBill;
        }
        public async Task<string> Handle(GenerateEwayBillandEInvoiceService request, CancellationToken cancellationToken)
        {
            var data = new EwayBillEntity()
            {
                action = request.action,
                userid = request.userid,
                gstin = request.gstin,
                action2 = request.action2,
                token = request.token,
                App = request.App,
                prod = request.prod,
                client = request.client,
                value = request.value,
                parameter = request.parameter
            };
            var result = await ewayBill.GenerateEwayBillandEInvoice(data);
            return result;
        }
    }
    public class GenerateEwayBillHandler : IRequestHandler<GenerateEwayBillService, string>
    {
        private readonly IEwayBillRepository ewayBill;

        public GenerateEwayBillHandler(IEwayBillRepository ewayBill)
        {
            this.ewayBill = ewayBill;
        }
        public async Task<string> Handle(GenerateEwayBillService request, CancellationToken cancellationToken)
        {
            var data = new EwayBillEntity()
            {
                action = request.action,
                userid = request.userid,
                gstin = request.gstin,
                action2 = request.action2,
                token = request.token,
                App = request.App,
                prod = request.prod,
                client = request.client,
                value = request.value,
                parameter = request.parameter
            };
            var result = await ewayBill.GenerateEwayBill(data);
            return result;
        }
    }
    public class CancelEwayBillandEInvoiceHandler : IRequestHandler<CancelEwayBillandEInvoiceService, string>
    {
        private readonly IEwayBillRepository ewayBill;

        public CancelEwayBillandEInvoiceHandler(IEwayBillRepository ewayBill)
        {
            this.ewayBill = ewayBill;
        }
        public async Task<string> Handle(CancelEwayBillandEInvoiceService request, CancellationToken cancellationToken)
        {
            var data = new EwayBillEntity()
            {
                action = request.action,
                userid = request.userid,
                gstin = request.gstin,
                action2 = request.action2,
                token = request.token,
                App = request.App,
                prod = request.prod,
                client = request.client,
                value = request.value,
                parameter = request.parameter
            };
            var result = await ewayBill.CancelEwayBillandEInvoice(data);
            return result;
        }
    }
    public class CancelEwayBillHandler : IRequestHandler<CancelEwayBillService, string>
    {
        private readonly IEwayBillRepository ewayBill;

        public CancelEwayBillHandler(IEwayBillRepository ewayBill)
        {
            this.ewayBill = ewayBill;
        }
        public async Task<string> Handle(CancelEwayBillService request, CancellationToken cancellationToken)
        {
            var data = new EwayBillEntity()
            {
                action = request.action,
                userid = request.userid,
                gstin = request.gstin,
                action2 = request.action2,
                token = request.token,
                App = request.App,
                prod = request.prod,
                client = request.client,
                value = request.value,
                parameter = request.parameter
            };
            var result = await ewayBill.CancelEwayBill(data);
            return result;
        }
    }
}
