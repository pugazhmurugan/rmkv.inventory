﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Models.Shared
{
    public class GetEmployeeLookupParams
    {
        public int Sales_Location_Id { get; set; }
        public string For { get; set; }
        public bool Resigned { get; set; }
    }
}
