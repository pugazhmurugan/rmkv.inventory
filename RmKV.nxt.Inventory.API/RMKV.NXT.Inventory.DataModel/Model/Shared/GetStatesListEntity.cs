﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Shared
{
    class GetStatesListEntity
    {
        public int States { get; set; }
    }
    public class GetStateEntity 
    {
        public int State_Id { get; set; }
        public string State_Name { get; set; }
    }
}
