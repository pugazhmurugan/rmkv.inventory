﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Shared.SalesLocation
{
    public class GetSalesLocationEntity
    {
        public int Sales_Location_ID { get; set; }
        public string Sales_Location_Name { get; set; }
    }
    public class SalesLocationParams
    {

        public int User_Id { get; set; }
        public int Sales_Location_Id { get; set; }
    }
}
