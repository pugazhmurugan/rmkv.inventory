﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Shared
{
   public class GrntoWarehouseLookupparmsEntity
    {
        public string GetGrnlookup { get; set; }
    }

    public class GrntoWarehouseLookuplistEntity
    {
        public int Grn_No { get; set; }
        public DateTime Grn_Date { get; set; }
        public string Grn_Type { get; set; }
        public int Balance_Qty { get; set; }
        public int Grn_Bale_Qty { get; set; }
        public decimal Total_Bale_Value { get; set; }
    }
}
