﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.Shared
{
    public class BynoDataModel
    {
        public string ByNoData { get; set; }
    }
    public class GlobalData
    {
        public string globalParams { get; set; }
    }
    public class GetDocumentPath
    {
        public string documentPath { get; set; }
    }
    public class BynoDetails
    {
        public string byno { get; set; }
        public string inv_byno { get; set; }
        public string byno_serial { get; set; }
        public string byno_prod_serial { get; set; }
        public string product_code { get; set; }
        public string product_name { get; set; }
        public int? uom_id { get; set; }
        public string uom_descrition { get; set; }
        public string prod_qty { get; set; }
        public int? prod_pcs { get; set; }
        public decimal? selling_price { get; set; }
        public string hsncode { get; set; }
        public decimal? gst { get; set; }
        public bool unique_byno_prod_serial { get; set; }
    }



}
