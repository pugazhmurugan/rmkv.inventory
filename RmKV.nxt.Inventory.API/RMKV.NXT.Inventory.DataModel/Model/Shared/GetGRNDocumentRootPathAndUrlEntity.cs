﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Shared.HRDocumentRootPathAndUrl
{
    public class GetGRNDocumentRootPathAndUrlEntity
    {
        public string GRN_Document_Root_Path { get; set; }
        public string GRN_Document_Root_URL { get; set; }
    }
}
