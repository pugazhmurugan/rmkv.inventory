﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Shared
{
    public class CompanyParams
    {
        public int User_Id { get; set; }
        public int Sales_Location_Id { get; set; }
    }
}
