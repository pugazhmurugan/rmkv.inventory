﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.Shared
{
    public class GetGroupSectionsEntity
    {
        public int Group_Section_ID { get; set; }
        public string Group_Section_Name { get; set; }
        public string Group_Section_Prefix { get; set; }
    }
}
