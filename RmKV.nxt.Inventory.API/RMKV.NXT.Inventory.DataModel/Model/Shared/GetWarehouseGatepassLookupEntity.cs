﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Shared
{
  
    public class GatepassLookupparmsEntity
    {
        public string GetGatePasslookup { get; set; }
    }

    public class WarehouseGatepassLookuplistEntity
    {
        public int Gatepass_No { get; set; }
        public DateTime Gatepass_Date { get; set; }
        public string Gatepass_Type { get; set; }
        public int Bale_Count { get; set; }
        public int Balance_Qty { get; set; }
    }
}
