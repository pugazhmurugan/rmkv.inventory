﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Shared
{
    public class DashboardTable
    {
        public int RETURN_VALUE { get; set; }
        public string inwardbales { get; set; }
        public string outwardbales { get; set; }
        public string transfers { get; set; }
        public string stock { get; set; }
        public string counts { get; set; }
        public string events { get; set; }
    }
}
