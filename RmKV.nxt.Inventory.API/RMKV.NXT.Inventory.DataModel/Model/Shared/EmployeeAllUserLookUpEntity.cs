﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Shared
{
   public class EmployeeAllUserLookUpEntity
    {
        public int user_id { get; set; }
        public string user_name { get; set; }
    }
    public class LRLookUpEntity
    {
        public int LR_Serial_No { get; set; }
        public string LR_No { get; set; }
        public string LR_Amount { get; set; }
    }
}
