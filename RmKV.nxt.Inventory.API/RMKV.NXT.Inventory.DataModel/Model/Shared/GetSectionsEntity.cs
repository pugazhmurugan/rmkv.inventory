﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Shared
{
    public class GetSectionsEntity
    {
        public int Wh_Section_ID { get; set; }
        public int Group_Section_ID { get; set; }
        public string Group_Section_Name { get; set; }
    }
    public class GetSectionsParams
    {
        public string GetGRNSections { get; set; }
    }
    public class GetGRNSectionsParams
    {
        public int warehouse_id { get; set; }
    }
}
