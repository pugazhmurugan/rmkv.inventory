﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Shared
{
   public class PayrollMonthLoadEntity
    {
        public string payroll_month { get; set; }
        public int sales_location_id { get; set; }
        public int company_id { get; set; }
    }
}
