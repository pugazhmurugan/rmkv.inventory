﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Shared.Company
{
    public class GetCompanyEntity
    {
        public int Company_ID { get; set; }
        public string Company_Name { get; set; }
    }
}
