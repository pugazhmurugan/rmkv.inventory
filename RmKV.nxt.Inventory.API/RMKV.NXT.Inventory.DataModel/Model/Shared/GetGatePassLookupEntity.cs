﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Shared
{
   public class GetGatePassLookupEntity
    {
        public string GatePassLookup { get; set; }
    }

    public class GetGatePassLookup
    {
        public int GatePass_No { get; set; }
        public string GatePass_Type { get; set; }
        public int Bale_Count { get; set; }
        public decimal Bale_Value { get; set; }
        public decimal Bale_weight { get; set; }
        public string Account_Name { get; set; }
        public string Supplier_Code { get; set; }
        public string GatePass_Date { get; set; }
        public string Sales_Location_Name { get; set; }
        public string Group_Section_Name { get; set; }
        public string Wh_Name { get; set; }
    }
}
