﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.Masters
{
    public class GetProductAttributesEntity
    {
        public string Attribute_Id { get; set; }
        public string Attribute_Name { get; set; }
        public bool Active { get; set; }
        public List<int> Group_Section_Id { get; set; }
    }
    public class AddProductAttributesEntity
    {
        public string Product_Attribute_List { get; set; }
    }

    public class ProductAttributesParamsEntity
    {
        public string Product_Attributtes { get; set; }
    }

    public class GetProductAttributeValuesEntity
    {
        public string Attribute_Id { get; set; }
        public string Attribute_Value_Id { get; set; }
        public string Attribute_Name { get; set; }
        public string Attribute_Value { get; set; }
        public bool Active { get; set; }
    }

    public class GetProductAttributesParams
    {
        public int Group_Section_Id { get; set; }
    }
    public class GetProductAttributeValuesParams
    {
        public string Attribute_Id { get; set; }
        public string Group_Section_Id { get; set; }
    }
}
