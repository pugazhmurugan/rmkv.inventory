﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.Masters
{
   public class SaveProductCategoryEntity
    {
        public string SaveCategories { get; set; }
    }

    public class GetProductCategoryParmsEntity
    {
        public int Group_Section_Id { get; set; }
    }

    public class GetProductCategoryListEntity
    {
        public int Category_Id { get; set; }
        public string Category_Name { get; set; }
        public bool Active { get; set; }
    }
    public class FetchProductCategoryParmsEntity
    {
        public string FetchCategory { get; set; }
    }

    public class FetchProductCategoryListEntity
    {
        public int Category_Id { get; set; }
        public string Category_Name { get; set; }
        public bool Active { get; set; }
    }

    public class RemoveProductCategoryListEntity
    {
        public string CancelCategory { get; set; }
    }
}
