﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.Masters
{
   public class SaveHSNMasterEntity
    {
        public string SaveHSN { get; set; }
    }

    public class SaveHSNTaxEntity
    {
        public string SaveHSNTax { get; set; }
    }

    public class RemoveHSNMasterEntity
    {
        public string CancelHSN { get; set; }
    }

    public class FetchHSNMasterParamsEntity
    {
        public string FetchHSN { get; set; }
    }

    public class FetchHSNMasterListEntity
    {
        public string Hsn { get; set; }
        public string Hsn_Description { get; set; }
        public bool Variable_Rate { get; set; }
        //public bool Active { get; set; }
    }

    public class HSNMasterListEntity
    {
        public string Hsn { get; set; }
        public string Hsn_Description { get; set; }
        public bool Variable_Rate { get; set; }
        public bool Active { get; set; }
    }

    public class FetchHSNTaxRateParamsEntity
    {
        public string FetchHSNTaxRate { get; set; }
    }

    public class FetchHSNTaxRateIndividualParamsEntity
    {
        public string FetchHSNTaxRateIndividual { get; set; }
    }

    public class RemoveHSNTaxRateEntity
    {
        public string RemoveHSNTaxRate { get; set; }
    }

    public class FetchHSNTaxRateListEntity
    {
        public string Hsn { get; set; }
        public string Hsn_Description { get; set; }
        public bool Variable_Rate { get; set; }
        public string HSN_Tax_Rates { get; set; }
        public Guid Hsn_Tax_Rate_Id { get; set; }
        public DateTime Effective_From { get; set; }
        public decimal Range_From { get; set; }
        public decimal Rage_To { get; set; }
        public decimal Gst_Rate { get; set; }
    }

    public class FetchHSNTaxRateIndividualListEntity
    {
        public string Hsn { get; set; }
        public string Hsn_Description { get; set; }
        public bool Variable_Rate { get; set; }
        public string HSN_Tax_Rates { get; set; }
        public Guid Hsn_Tax_Rate_Id { get; set; }
        public DateTime Effective_From { get; set; }
        public decimal Range_From { get; set; }
        public decimal Rage_To { get; set; }
        public decimal Gst_Rate { get; set; }
    }

}
