﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.Menus
{
   public class MenuItemsEntity
    {
        public int MenuId { get; set; }
        public string DisplayName { get; set; }
        public string IconName { get; set; }
        public int MenuOrder { get; set; }
        public string Route { get; set; }
        public List<MenuItemsEntity> Children { get; set; }
        public List<Rights_Master> Rights { get; set; }
        public int? ParentId { get; set; }
    }
    public class Rights_Master
    {
        public bool Status { get; set; }
        public int Right_ID { get; set; }
        public string Right_Name { get; set; }
    }
    public class GetAllMenusParams
    {
        public int User_ID { get; set; }
        public int Module_ID { get; set; }
        public int Sales_Location_ID { get; set; }
        public int Company_ID { get; set; }
        public int Section_ID { get; set; }
    }
}
