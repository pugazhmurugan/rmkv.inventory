﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.EwayBill
{
    public class EwayBillEntity
    {
        public string action { get; set; }
        public string userid { get; set; }
        public string password { get; set; }
        public string App { get; set; }
        public string prod { get; set; }
        public string client { get; set; }
        public string value { get; set; }
        public string gstin { get; set; }
        public string uid { get; set; }
        public string token { get; set; }
        public string action2 { get; set; }
        public string irnno { get; set; }
        public object parameter { get; set; }
    }

    public class EwayBillDetailsEntity
    {
        public Int64 AckNo { get; set; }
        public DateTime AckDt { get; set; }
        public string Irn { get; set; }
        public string SignedInvoice { get; set; }
        public string SignedQRCode { get; set; }
        public string Status { get; set; }
        public Int64 EwbNo { get; set; }
        public DateTime EwbDt { get; set; }
        public DateTime EwbValidTill { get; set; }
        public string Remarks { get; set; }
        public string error { get; set; }
        public string status { get; set; }
        public string detailsData { get; set; }
    }
}
