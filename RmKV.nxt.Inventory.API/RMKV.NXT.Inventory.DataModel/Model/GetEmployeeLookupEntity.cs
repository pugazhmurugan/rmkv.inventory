﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Models.Shared
{
    public class GetEmployeeLookupEntity
    {
        public int Employee_Id { get; set; }
        public string Employee_Code { get; set; }
        public string Employee_Name { get; set; }
        public string Date_Left { get; set; }
        public int? Designation_Id { get; set; }
        public string Designation_Name { get; set; }
        public int Section_Id { get; set; }
        public string Section_Name { get; set; }
        public string Company_Name { get; set; }
        public string Department_Name { get; set; }
        public int Sales_Location_Id { get; set; }
        public string Sales_Location_Name { get; set; }
        public string Salesman_Code { get; set; }
        public string Weekly_Day_Off { get; set; }
        public string Incentive_Type_Name { get; set; }
        public string Incentive_Type_Id { get; set; }
        public string Employee_Photo { get; set; }
        public string Date_Of_Join { get; set; }
        public string Active { get; set; }
        public int? Incentive_Template_Id { get; set; }
    }
}
