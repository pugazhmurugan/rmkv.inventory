﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model
{
   public class selectPrintingList
    {
        public string byno { get; set; }
        public string product_code { get; set; }
        public string product_name { get; set; }
        public string counter_code { get; set; }
        public string selling_price { get; set; }
        public bool isSelected { get; set; }
        public byte[] images { get; set; }
        public string Caption { get; set; }
        public int NoOfCopies { get; set; }


    }

    public class Barcode
    {
        public int byno { get; set; }
        public int product_code { get; set; }
        public int product_name { get; set; }
        public int counter_code { get; set; }
        public int selling_price { get; set; }
        public int isSelected { get; set; }
        public Bitmap images { get; set; }

    }

    public class BarcodePrintEntity
    {
        public string byno { get; set; }
        public string inv_byno { get; set; }
        public string byno_serial { get; set; }
        public string byno_prod_serial { get; set; }
        public string print_jari { get; set; }
        public string product_code { get; set; }
        public string style_code { get; set; }
        public string counter_code { get; set; }
        public string supplier_style_code { get; set; }
        public string product_name { get; set; }
        public string selling_price { get; set; }
        public decimal double_rate { get; set; }
        public decimal discount_rate { get; set; }
        public int no_of_copies { get; set; }
        public string size { get; set; }
        public string Supplier_Description { get; set; }
        public string Type_Name { get; set; }
        public bool Drywash { get; set; }
        public string Mts { get; set; }
        public string product_group_name { get; set; }
        public string Product_group_Uom { get; set; }
        public int Prod_Qty { get; set; }
        public int size_id { get; set; }
        public string Caption { get; set; }
        public Bitmap images { get; set; }
    }

    public class ImageByteArray{
        public byte[] photo { get; set; }
        public string Type { get; set; }
        public string extension { get; set; }
        public string FileName { get; set; }
    }
}
