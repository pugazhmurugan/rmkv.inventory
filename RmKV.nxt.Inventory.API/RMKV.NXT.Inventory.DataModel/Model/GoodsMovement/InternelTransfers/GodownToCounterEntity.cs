﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.GoodsTransfers
{
    public class GodownToCounterEntity
    {
        public string GodownToCounter { get; set; }
    }
}
