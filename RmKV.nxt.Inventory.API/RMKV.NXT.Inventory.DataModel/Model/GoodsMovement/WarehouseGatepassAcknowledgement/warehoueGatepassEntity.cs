﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.WarehouseGatepassAcknowledgement
{
    public class WarehouseGatePassEntity
    {
        public string Inward_Gatepass_Id { get; set; }
        public string Transfer_Name { get; set; }
        public string Received_Date { get; set; }
        public string Source_Warehouse_Id { get; set; }
        public string Source_Wh_Section_Id { get; set; }
        public string Source_GRN_No { get; set; }
        public string Source_Warehouse_Name { get; set; }
        public string Source_Wh_Section_Name { get; set; }
        public string Gatepass_No { get; set; }
        public string Gatepass_Title { get; set; }
        public string Gatepass_Date { get; set; }
        public int? Bale_Count { get; set; }
        public int? No_Of_Bags { get; set; }
        public string Outward_Grn_No { get; set; }
        public string Current_Location { get; set; }
    }

    public class FetchWarehouseGatePassEntity
    {
        public int? Warehouse_Id { get; set; }
        public string Inward_Gatepass_Id { get; set; }
        public string Source_Wh_Name { get; set; }
        public string Wh_Name { get; set; }
        public string Received_Date { get; set; }
        public string Group_Section_Name { get; set; }
        public int? Source_Warehouse_Id { get; set; }
        public int? Source_Wh_Section_Id { get; set; }
        public int? Source_Group_Section_Id { get; set; }
        public string Source_Group_Section_Name { get; set; }
        public string EWayBillNo { get; set; }
        public int? Wh_Section_Id { get; set; }
        public int? Group_Section_Id { get; set; }
        public string Source_GRN_No { get; set; }
        public string EwaybillNo { get; set; }
        public string Remarks { get; set; }
        public string Bale_Count { get; set; }
    }

    public class GatePassEntity
    {
        public string Outward_GRN_No { get; set; }
        public string Bale_No { get; set; }
        public string No_Of_Bags { get; set; }
    }

    public class GetDesignationEntity
    {
        public int Sales_Location_Id { get; set; }
        public string Sales_Location_Name { get; set; }
    }
    public class GetSameWarehouseEntity
    {
        public string WareHouse_Id { get; set; }
        public string Wh_Name { get; set; }

    }
    public class GetWarehouseTransferDetailEntity
    {
        public int? Transfer_Ref_No { get; set; }
        public string Transfer_Date { get; set; }
        public string Transfer_Type { get; set; }
        public int? Transfer_Qty { get; set; }
    }
}
