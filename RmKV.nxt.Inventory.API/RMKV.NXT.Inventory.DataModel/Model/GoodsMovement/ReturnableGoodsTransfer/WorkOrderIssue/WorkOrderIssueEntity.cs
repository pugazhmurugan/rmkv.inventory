﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.ReturnableGoodsTransfer.WorkOrderIssue
{
    public class GetWorkOrderIssuetransferTypeEntity
    {
        public string transfer_flag { get; set; }
        public string transfer_type { get; set; }
        public string transfer_name { get; set; }
    }
}
