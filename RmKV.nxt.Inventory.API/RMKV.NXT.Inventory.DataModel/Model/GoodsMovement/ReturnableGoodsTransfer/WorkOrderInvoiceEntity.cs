﻿using RMKV.NXT.Inventory.DataModel.Model.PurchaseReturn;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.ReturnableGoodsTransfer
{
    public  class WorkOrderInvoiceEntity
    {
        public string wh_section_id { get; set; }
        public string transfer_type { get; set; }
        public string wo_receipt_id { get; set; }
        public string wo_receipt_date { get; set; }
        public string received_from { get; set; }
        public string taxable_amount { get; set; }
        public string other_amount { get; set; }
        public string inv_supplier_code { get; set; }
        public string inv_no { get; set; }
        public string inv_amount { get; set; }
        public string igst_percentage { get; set; }
        public string cgst_percentage { get; set; }
        public string sgst_percentage { get; set; }
        public string composite { get; set; }
        public string igst_amount { get; set; }
        public string cgst_amount { get; set; }
        public string sgst_amount { get; set; }
        public string misc_addition_amt { get; set; }
        public string misc_deduction_amt { get; set; }
        public string round_off_amount { get; set; }
        public List<FilePath> file_path { get; set; }
        public string remarks { get; set; }
        public string entered_by { get; set; }
        public string company_section_id { get; set; }
        public string inv_date { get; set; }
        public List<DetailsReceipt>  details { get; set; }
    }

    public class DetailsReceipt
    {
        public string serial_no { get; set; }
        public string issue_wh_section_id { get; set; }
        public string issue_transfer_type { get; set; }
        public string issue_id { get; set; }
        public string issue_serial_no { get; set; }
        public string charges { get; set; }
        public string customer_charges { get; set; }
        public string cgst { get; set; }
        public string cgst_amount { get; set; }
        public string igst { get; set; }
        public string igst_amount { get; set; }
        public string sgst { get; set; }
        public string sgst_amount { get; set; }
        public string total_charges { get; set; }
        public string hsn { get; set; }
        public string margin_code { get; set; }
    }
}
