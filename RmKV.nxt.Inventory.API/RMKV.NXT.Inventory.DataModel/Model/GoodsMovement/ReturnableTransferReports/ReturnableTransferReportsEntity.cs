﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.ReturnableTransferReports
{
    public class GetWorkOrderIssueListEntity
    {
        public string WorkOrderIssueListReportView { get; set; }
    }
    public class GetWorkOrderReceiptListEntity
    {
        public string WorkOrderReceiptListReportView { get; set; }
    }

    public class GetMismatchStoneQtyEntity
    {
        public string MismatchStoneQtyReportView { get; set; }
    }
}
