﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.TransfersExceptionReports
{
   public class GodownToCounterExceptionEntity
    {
        public string GodownToCounterException_List { get; set; }
        public bool Type { get; set; }
    }
}
