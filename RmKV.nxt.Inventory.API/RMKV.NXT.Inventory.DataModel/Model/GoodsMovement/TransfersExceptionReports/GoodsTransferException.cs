﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.TransfersExceptionReports
{
    public class GoodsTransferExceptionEntity
    {
        public string Goods_Transfer_Exception_Report { get; set; }
        public bool Type { get; set; }
    }
}
