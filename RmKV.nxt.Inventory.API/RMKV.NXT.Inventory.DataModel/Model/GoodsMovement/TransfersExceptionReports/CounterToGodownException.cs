﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.TransfersExceptionReports
{
    public class CounterToGodownExceptionEntity
    {
        public string CounterToGodownException_List { get; set; }
        public bool Type { get; set; }
    }
}
