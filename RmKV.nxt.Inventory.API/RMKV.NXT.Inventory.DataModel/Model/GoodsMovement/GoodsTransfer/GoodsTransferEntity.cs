﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.GoodsMovement.GoodsTransfer
{
   public class GoodsTransferEntity
    {
        public string GoodsTransferReport { get; set; }
        public bool Type { get; set; }
    }
}
