﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.PurchaseOrders
{
    public class AddPurchaseOrdersList
    {
        public int po_id { get; set; }
        public string po_no { get; set; }
        public int company_section_id { get; set; }
        public string po_date { get; set; }
        public string supplier_code { get; set; }
        public string supplier_name { get; set; }
        public string supplier_group { get; set; }
        public string city { get; set; }
        public string address { get; set; }
        public string gstin_no { get; set; }
        public string email_id { get; set; }
        public string phone_no { get; set; }
        public string reference_no { get; set; }
        public string despatch_name { get; set; }
        public int despatch_location_id { get; set; }
        public string delivery_schedule { get; set; }
        public int mode_of_despatch { get; set; }
        public string payment_terms { get; set; }
        public string raised_by { get; set; }
        public string uom { get; set; }
        public string remarks { get; set; }
        public string agent_name { get; set; }
        public string agent_email_id { get; set; }
        public string agent_email_status { get; set; }
        public string bank_name { get; set; }
        public string ifsc_code { get; set; }
        public string supplier_account_no { get; set; }
        public string branch_name { get; set; }
        public string new_supplier { get; set; }
        public string supplier_status { get; set; }
        public string return_type { get; set; }
        public List<FilePath> file_path { get; set; }
        public List<BankFilePath> bank_file_path { get; set; }
        public string entered_by { get; set; }
        public List<GetGridDetails> details { get; set; }
        //public List<GetSizeDetails> sizedetails { get; set; }
        public string OldDocumentPathForPO { get; set; }
        public string OldDocumentPathForBank { get; set; }
        public bool isBankFile { get; set; }
        public bool isCommonFile { get; set; }
    }

    //public class AddPurchaseOrdersListWithFile
    //{
    //    public int po_id { get; set; }
    //    public string po_no { get; set; }
    //    public int company_section_id { get; set; }
    //    public string po_date { get; set; }
    //    public string supplier_code { get; set; }
    //    public string supplier_name { get; set; }
    //    public string supplier_group { get; set; }
    //    public string city { get; set; }
    //    public string address { get; set; }
    //    public string gstin_no { get; set; }
    //    public string email_id { get; set; }
    //    public string phone_no { get; set; }
    //    public string reference_no { get; set; }
    //    public string despatch_name { get; set; }
    //    public int despatch_location_id { get; set; }
    //    public string delivery_schedule { get; set; }
    //    public int mode_of_despatch { get; set; }
    //    public string payment_terms { get; set; }
    //    public string raised_by { get; set; }
    //    public string uom { get; set; }
    //    public string remarks { get; set; }
    //    public string agent_name { get; set; }
    //    public string agent_email_id { get; set; }
    //    public string agent_email_status { get; set; }
    //    public string bank_name { get; set; }
    //    public string ifsc_code { get; set; }
    //    public string supplier_account_no { get; set; }
    //    public string branch_name { get; set; }
    //    public string new_supplier { get; set; }
    //    public string supplier_status { get; set; }
    //    public string return_type { get; set; }
    //    public List<FilePath> file_path { get; set; }
    //    public List<BankFilePath> bank_file_path { get; set; }
    //    public string entered_by { get; set; }
    //    public List<GetGridDetails> details { get; set; }
    //    public string? OldDocumentPathForPO { get; set; }
    //    public string? OldDocumentPathForBank { get; set; }
    //    public bool isBankFile { get; set; }
    //    public bool isCommonFile { get; set; }
    //}

    public class BankFilePath
    {
        public string bank_file_path { get; set; }
    }

    public class FilePath
    {
        public string file_path { get; set; }
    }
    public class GetGridDetails
    {
        public int serial_no { get; set; }
        public string item_description { get; set; }
        public string image_path { get; set; }
        public string hsncode { get; set; }
        public string brand { get; set; }
        public string sleeve { get; set; }
        public string color { get; set; }
        public string no_of_design { get; set; }
        public string no_of_sets { get; set; }
        public string no_of_colors { get; set; }
        public string quantity { get; set; }
        public string pcs { get; set; }
        public string rate { get; set; }
        public string amount { get; set; }
        public List<GetSizeDetails> sizedetails { get; set; }
    }

    public class GetSizeDetails
    {
        public int po_serial_no { get; set; }
        public int size_id { get; set; }
        public string sizes { get; set; }
        public int quantity { get; set; }
    }
}
