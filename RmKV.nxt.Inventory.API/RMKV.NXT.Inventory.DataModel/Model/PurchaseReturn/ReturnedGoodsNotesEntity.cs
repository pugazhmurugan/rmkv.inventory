﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.PurchaseReturn
{
    public class AddRGNEntity
    {
        public string rgn_no { get; set; }
        public string rgn_date { get; set; }
        public string lr_no { get; set; }
        public string lr_date { get; set; }
        public decimal lr_amount { get; set; }
        public string to_pay { get; set; }
        public string invoice_no { get; set; }
        public string invoice_date { get; set; }
        public int bales { get; set; }
        public string dc_no { get; set; }
        public string mode_of_transfer { get; set; }
        public string transporter_code { get; set; }
        public string carrier_name { get; set; }
        public string docket_no { get; set; }
        public string coolie { get; set; }
        public string delivered_by { get; set; }
        public string kind_attention { get; set; }
        public List<FilePath> file_path { get; set; }
        public List<Details> details { get; set; }
        public int entered_by { get; set; }
        public int company_section_id { get; set; }
        public int sales_location_id { get; set; }
        public string remarks { get; set; }
    }
    public class Details
    {
        public string dc_no { get; set; }
        public string dc_date { get; set; }
        public string supplier_name { get; set; }
        public string supplier_code { get; set; }
        public string trading_drcr_no { get; set; }
        public string city { get; set; }
        public int pcs { get; set; }
    }
    public class FilePath
    {
        public string file_path1 { get; set; }
        public string file_path2 { get; set; }
        public string file_path3 { get; set; }
        public string file_path4 { get; set; }
    }
}