﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.Pricing.DoubleRate
{
    public class ByAmountEntity
    {
        public string inv_byno { get; set; }
        public string byno_serial { get; set; }
        public string byno_prod_serial { get; set; }
        public decimal old_selling_price { get; set; }
        public decimal new_selling_price { get; set; }
        public decimal selling_price { get; set; }
        public int entered_by { get; set; }
        public int company_section_id { get; set; }
        public decimal double_rate { get; set; }
        public decimal discount_rate { get; set; }
    }

    public class ByPercentageEntity
    {
        public string inv_byno { get; set; }
        public string byno_serial { get; set; }
        public string byno_prod_serial { get; set; }
        public decimal old_selling_price { get; set; }
        public decimal new_selling_price { get; set; }
        public decimal selling_price { get; set; }
        public int entered_by { get; set; }
        public int company_section_id { get; set; }
        public decimal double_rate { get; set; }
        public decimal discount_rate { get; set; }
        public int add_margin { get; set; }
        public int less_margin { get; set; }
        public string aadi_pricecode { get; set; }
    }
}
