﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.Purchases
{
    public class ReturnTable
    {
        public string RETURN_VALUE { get; set; }
        public string Records1 { get; set; }
        public string Records2 { get; set; }
    }
}
