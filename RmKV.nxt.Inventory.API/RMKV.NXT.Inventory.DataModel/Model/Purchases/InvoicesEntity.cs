﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMKV.NXT.Inventory.DataModel.Model.Purchases
{
    public class InvoicesTable
    {
        public string Inv { get; set; }
        public string Inv_Det { get; set; }
        public int RETURN_VALUE { get; set; }
        public string Invoice_Check_List_Report { get; set; }
    }
}
