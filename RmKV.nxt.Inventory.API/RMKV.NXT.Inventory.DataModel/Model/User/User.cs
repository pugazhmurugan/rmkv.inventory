﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  RmKV.NXT.Inventory.DataModel.Model.User
{
    public class UserDTO
    {
        public string User_Name { get; set; }
        public string Password { get; set; }
    }

    public class Users
    {
        public int User_ID { get; set; }

        public string User_Name { get; set; }

        public long? Password { get; set; }
    }
}
