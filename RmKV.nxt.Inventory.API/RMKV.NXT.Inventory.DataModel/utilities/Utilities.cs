﻿using FastReport;
using FastReport.Export.PdfSimple;
using Microsoft.AspNetCore.Mvc;
using QRCoder;
using RMKV.NXT.Inventory.DataModel;
using RMKV.NXT.Inventory.DataModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace  RmKV.NXT.Inventory.DataModel.utilities
{
    public static class Utilities
    {
        public static string SerializeObject(Object obj)
        {
            return SerializeObject(obj, false);
        }


        public static string SerializeObject(Object obj, bool generateNamespace)
        {
            XmlSerializerNamespaces ns = null;
            XmlSerializer ser = new XmlSerializer(obj.GetType()); ;

            if (generateNamespace)
            {
                ns = new XmlSerializerNamespaces();
                ns.Add("", "");
            }
            StringBuilder sb = new StringBuilder();
            using (XmlWriter writer = XmlWriter.Create(sb, new XmlWriterSettings() { OmitXmlDeclaration = true }))
            {
                if (ns == null)
                    ser.Serialize(writer, obj);
                else
                    ser.Serialize(writer, obj, ns);
                writer.Flush();
                writer.Close();

            }
            return sb.ToString();
        }


        //public static string JsonSerializeObject(object obj)
        //{
        //    JavaScriptSerializer objJsonSer = new JavaScriptSerializer();
        //    return objJsonSer.Serialize(obj);

        //}

        public static T deserializeObject<T>(string serializedObject)
        {
            TextReader objTextReaader = new StringReader(serializedObject);
            XmlSerializer objXmlSer = new XmlSerializer(typeof(T));
            return ((T)(Convert.ChangeType(objXmlSer.Deserialize(objTextReaader), typeof(T))));

        }

        //public static T JsonDeserializeObject<T>(string serializedObject)
        //{
        //    JavaScriptSerializer objJsonSer = new JavaScriptSerializer();
        //    return ((T)(Convert.ChangeType(objJsonSer.Deserialize<T>(serializedObject), typeof(T))));
        //}

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public static DataTable ToDataTable<T>(this IList<T> objList)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable objDataTable = new DataTable();
            foreach (PropertyDescriptor property in properties)
                objDataTable.Columns.Add(property.Name, Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType);
            if (objList != null)
            {
                foreach (T item in objList)
                {
                    DataRow row = objDataTable.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    objDataTable.Rows.Add(row);
                }
            }
            return objDataTable;
        }

        public static DataTable ConvertEntityListToDataTable<T>(this IList<T> objList, List<Type> excludedTypes)
        {
            if (excludedTypes == null)
                excludedTypes = new List<Type>();
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable objDataTable = new DataTable();
            foreach (PropertyDescriptor property in properties)
                if (!excludedTypes.Contains(property.PropertyType))
                    objDataTable.Columns.Add(property.Name, Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType);
            if (objList != null)
            {
                foreach (T item in objList)
                {
                    DataRow row = objDataTable.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    objDataTable.Rows.Add(row);

                }
            }
            return objDataTable;
        }

        private static T ConvertDataRowToEntity<T>(this DataRow tableRow) where T : new()
        {
            //create a new type of the entity i want 
            Type t = typeof(T);
            T returnObject = new T();

            foreach (DataColumn col in tableRow.Table.Columns)
            {
                string colName = col.ColumnName;

                //Look for the object's property with the columns name , ignore case
                PropertyInfo pInfo = t.GetProperty(colName.ToLower(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                //did we find the property ?

                if (pInfo != null)
                {
                    object val = tableRow[colName];

                    //is this Nullable<> type 

                    bool ISNullable = (Nullable.GetUnderlyingType(pInfo.PropertyType) != null);
                    if (ISNullable)
                    {
                        if (val is System.DBNull)
                        {
                            val = null;

                        }

                        else
                        {
                            //cont thee db type into the T we have in our Nullable<T> type
                            val = Convert.ChangeType(val, Nullable.GetUnderlyingType(pInfo.PropertyType));

                        }
                    }
                    else
                    {
                        // convert the db type into the type of the proerty in our entity 
                        if (pInfo.PropertyType.IsEnum)
                            val = Enum.ToObject(pInfo.PropertyType, val);
                        else
                            val = Convert.ChangeType(val, pInfo.PropertyType);
                    }
                    //set the value of the property with the value from the db
                    pInfo.SetValue(returnObject, val, null);

                }

            }
            //return the entity object with values
            return returnObject;
        }

        public static List<T> ConvertDataTableToEntityList<T>(this DataTable table) where T : new()
        {
            Type t = typeof(T);

            //create a list of the entities we wnat to return 
            List<T> returnObject = new List<T>();

            //Iterate through the DataTable's rows
            foreach (DataRow dr in table.Rows)
            {
                //convert each row into an entity object and add to the list
                T newRow = dr.ConvertDataRowToEntity<T>();
                returnObject.Add(newRow);
            }
            return returnObject;
        }

        public static string Base64Encode(string plainText)
        {
            return System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(plainText));
        }


        public static string Base64Decode(string base64EncodedData)
        {
            return System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(base64EncodedData));
        }

        public static string SerializeObjectUsingDataContractSerializer(Object obj)
        {
            var serializer = new DataContractSerializer(obj.GetType());
            using (var sw = new StringWriter())
            {
                using (var writer = new XmlTextWriter(sw))
                {
                    writer.Formatting = System.Xml.Formatting.None;
                    serializer.WriteObject(writer, obj);
                    writer.Flush();
                    return sw.ToString();
                }
            }
        }
        public class XMLFromObject
        {
            public string GetXMLFromObject(object o)
            {
                StringWriter sw = new StringWriter();
                XmlTextWriter tw = null;
                try
                {
                    XmlSerializer serializer = new XmlSerializer(o.GetType());
                    tw = new XmlTextWriter(sw);
                    serializer.Serialize(tw, o);
                }
                catch (Exception ex)
                {
                    //Handle Exception Code
                    throw ex;
                }
                finally
                {
                    sw.Close();
                    if (tw != null)
                    {
                        tw.Close();
                    }
                }
                return sw.ToString();
            }
        }

        public static FileStreamResult ReturnStreamReport(DataSet dataSet, string webRootPath)
        {
            MemoryStream stream = new MemoryStream();
            var contentType = "application/pdf";

            if (dataSet.Tables[0].Rows.Count != 0)
            {
                using (Report rpt = new Report())
                {
                    rpt.Load(webRootPath);
                    rpt.RegisterData(dataSet, "NorthWind");
                    rpt.PreparePhase1();
                    rpt.PreparePhase2();
                    PDFSimpleExport pdf = new PDFSimpleExport();
                    rpt.Export(pdf, stream);
                }
                stream.Flush();
                byte[] file = stream.ToArray();
                MemoryStream ms1 = new MemoryStream();
                ms1.Write(file, 0, file.Length);
                ms1.Position = 0;
                byte[] bytes = ms1.ToArray();
                return new FileStreamResult(ms1, contentType);
            }
            else
            {
                return new FileStreamResult(stream, contentType);
            }

            //Report webReport = new Report(); // Create a Web Report Object
            //webReport.Report.Load(webRootPath);
            //webReport.Report.RegisterData(dataSet, "NorthWind");
            //webReport.Report.PreparePhase1();
            //webReport.Report.PreparePhase2();
            //PDFSimpleExport pdf = new PDFSimpleExport();
            //MemoryStream stream = new MemoryStream();
            //webReport.Report.Export(pdf,stream);
            //try
            //{
            //    webReport.Report.Prepare();
            //}
            //catch (Exception ex)
            //{
            //    Debug.WriteLine(ex);
            //}


            //PDFSimpleExport export = new PDFSimpleExport();

            //    export.Export(webReport, stream);
        }

        public static int GenerateBarCode(List<BarcodePrintEntity> bynodata)
        {
            var strbynovalue = string.Empty;
            var strbynoserialvalue = string.Empty;
            var strbynoprodserialvalue = string.Empty;
            QRCodeGenerator _qrCode = new QRCodeGenerator();
            List<BarcodePrintEntity> list = new List<BarcodePrintEntity>();
            foreach (var data in bynodata)
            {
                var byno = data.byno;
                var bynoparts = byno.Split('/');
                var strbyno = bynoparts[0];
                var bynoserial = bynoparts[1];
                var bynoprodserial = bynoparts[2];
                var dblsellingpricevalue = string.Empty;
                var strbarcode = string.Empty;
                
                for (var i = 4; i > bynoserial.Length; i--)
                {
                    strbynovalue += ("0");
                }
                for (var j = 4; j > bynoprodserial.Length; j--)
                {
                    strbynoserialvalue += ("0");
                }
                for (var k = 5; k > data.selling_price.Length; k--)
                {
                    dblsellingpricevalue += ("0");
                }
                var bynumber = strbyno + strbynovalue + bynoserial + strbynoserialvalue + bynoprodserial;
                strbarcode = bynumber + dblsellingpricevalue + data.selling_price + "00" + dblsellingpricevalue + data.selling_price + "00";
                QRCodeData _qrCodeData = _qrCode.CreateQrCode(Convert.ToString(strbarcode), QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(_qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(20);
                list.Add(new BarcodePrintEntity
                {
                    images = qrCodeImage,
                    byno = byno,
                    inv_byno = data.inv_byno,
                    byno_serial = data.byno_serial,
                    byno_prod_serial = data.byno_prod_serial,
                    print_jari = data.print_jari,
                    product_code = data.product_code,
                    style_code = data.style_code,
                    counter_code = data.counter_code,
                    supplier_style_code = data.supplier_style_code,
                    product_name = data.product_name,
                    selling_price = data.selling_price,
                    double_rate = data.double_rate,
                    discount_rate = data.discount_rate,
                    no_of_copies = data.no_of_copies,
                    size = data.size,
                    Supplier_Description = data.Supplier_Description,
                    Type_Name = data.Type_Name,
                    Drywash = data.Drywash,
                    Mts = data.Mts,
                    product_group_name = data.product_group_name,
                    Product_group_Uom = data.Product_group_Uom,
                    Prod_Qty = data.Prod_Qty,
                    size_id = data.size_id,
                    Caption = data.Caption
                }); ;
            }

            if(list.Count == 0)
            {
                return 0;
            }
            else
            {
                foreach (var data in list)
                {
                    if(data.no_of_copies > 0)
                    {
                        for(var i = 1; i <= data.no_of_copies; i++)
                        {
                            if (data.Caption == "")
                            {
                                StringFormat format1 = new StringFormat(StringFormatFlags.NoClip);
                                StringFormat format2 = new StringFormat(format1);
                                format1.LineAlignment = StringAlignment.Far;
                                format1.Alignment = StringAlignment.Far;
                                var rupees = "Rs. " + data.selling_price + "." + "00";
                                //var rupees = "Rs. " + data.selling_price;
                                //var product = "LLMF026X-C12A";
                                PrintDocument pd = new PrintDocument();
                                pd.OriginAtMargins = true;
                                pd.DefaultPageSettings.Landscape = false;
                                pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
                                pd.DefaultPageSettings.PrinterResolution = new PrinterResolution();
                                pd.PrintPage += (sender, ev) =>
                                {
                                    ev.Graphics.DrawImage(data.images, new RectangleF(3, 1, 90, 90));
                                    ev.Graphics.DrawString(data.byno.ToString(), new Font("Swiss 721 BT Bold", 11, FontStyle.Bold), Brushes.Black, new RectangleF(87, 7, 260, 50), new StringFormat(format2));
                                    ev.Graphics.DrawString(data.product_code + " -" + data.counter_code, new Font("Swiss 721 BT Bold", 9, FontStyle.Bold), Brushes.Black, new RectangleF(87, 25, 250, 65), new StringFormat(format2));
                                    ev.Graphics.DrawString(rupees, new Font("Swiss 721 BT Bold", 12, FontStyle.Bold), Brushes.Black, new RectangleF(87, 40, 210, 65), new StringFormat(format2));
                                };
                                pd.Print();
                                pd.Dispose();
                            }
                            else
                            {
                                StringFormat format1 = new StringFormat(StringFormatFlags.NoClip);
                                StringFormat format2 = new StringFormat(format1);
                                format1.LineAlignment = StringAlignment.Far;
                                format1.Alignment = StringAlignment.Far;
                                var rupees = "Rs. " + data.selling_price + "." + "00";
                                //var rupees = "Rs. " + data.selling_price;
                                //var product = "LLMF026X-C12A";
                                PrintDocument pd = new PrintDocument();
                                pd.OriginAtMargins = true;
                                pd.DefaultPageSettings.Landscape = false;
                                pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
                                pd.DefaultPageSettings.PrinterResolution = new PrinterResolution();
                                pd.PrintPage += (sender, ev) =>
                                {
                                    //ev.Graphics.DrawImage(data.images, new RectangleF(5, 5, 95, 95));
                                    ev.Graphics.DrawImage(data.images, new RectangleF(3, -1, 90, 90));
                                    //ev.Graphics.DrawString(data.byno.ToString(), new Font("Swiss 721 BT Bold", 10, FontStyle.Bold), Brushes.Black, new RectangleF(95, 5, 260, 50), new StringFormat(format2));
                                    ev.Graphics.DrawString(data.byno.ToString(), new Font("Swiss 721 BT Bold", 11, FontStyle.Bold), Brushes.Black, new RectangleF(87, 7, 260, 50), new StringFormat(format2));
                                    ev.Graphics.DrawString(data.product_code + "-" + data.counter_code + "-" + data.Caption, new Font("Swiss 721 BT Bold", 9, FontStyle.Bold), Brushes.Black, new RectangleF(87, 25, 250, 65), new StringFormat(format2));
                                    ev.Graphics.DrawString(rupees, new Font("Calibri", 13, FontStyle.Bold), Brushes.Black, new RectangleF(87, 40, 230, 65), new StringFormat(format2));
                                    ev.Graphics.DrawString(rupees, new Font("Calibri", 13, FontStyle.Bold), Brushes.Black, new RectangleF(190, 40, 150, 65), new StringFormat(format2));
                                    //ev.Graphics.DrawString(rupees, new Font("Swiss 721 BT Bold", 12, FontStyle.Bold), Brushes.Black, new RectangleF(93, 60, 210, 65), new StringFormat(format2));
                                };
                                pd.Print();
                                pd.Dispose();
                            }
                        }
                    }
                }
                return 1;
            }
        }

        public static FileConfig GetFileNameAndPath(string fileRootPath, string screenPath, string fileOriginName)
        {
            string extension = Path.GetExtension(fileOriginName);
            var fileName = string.Format(@"{0}" + extension, Guid.NewGuid());
            var filePath = screenPath + "\\" + fileName;
            var fileSavePath = fileRootPath + "\\" + filePath;
            var directory = fileRootPath + "\\" + screenPath + "\\";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            FileConfig fileConfig = new FileConfig();
            fileConfig.fileName = screenPath + "\\" + fileName;
            fileConfig.fileSavePath = fileSavePath;

            return fileConfig;
        }
    }


}
