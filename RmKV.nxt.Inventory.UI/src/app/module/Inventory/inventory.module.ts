import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { EmployeeLookupComponent } from 'src/app/common/shared/employee-lookup/employee-lookup.component';
import { EmployeeLookupService } from 'src/app/common/services/employee-lookup/employee-lookup.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material';
import { TimeFormat } from 'src/app/common/shared/directives/TimeFormat';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ConfirmationDeleteComponent } from 'src/app/common/shared/confirmation-delete/confirmation-delete.component';
import { ProductGroupsComponent } from './pages/masters/product-groups/product-groups.component';
import { ProductAttributesComponent } from './pages/masters/product-attributes/product-attributes.component';
import { ProductMasterComponent } from './pages/masters/product-master/product-master.component';
import { ProductCategoriesComponent } from './pages/masters/product-categories/product-categories.component';
import { HsnMasterComponent } from './pages/masters/hsn-master/hsn-master.component';
import { ProductAttributeValuesComponent } from './pages/masters/product-attribute-values/product-attribute-values.component';
import { ProductGroupAttributesComponent } from './pages/masters/product-group-attributes/product-group-attributes.component';
import { HsnTaxRatesComponent } from './pages/masters/hsn-tax-rates/hsn-tax-rates.component';
import { AttributeLookupComponent } from 'src/app/common/shared/attribute-lookup/attribute-lookup.component';
import { ProductGroupLookupComponent } from 'src/app/common/shared/product-group-lookup/product-group-lookup.component';
import { HsnLookupComponent } from 'src/app/common/shared/hsn-lookup/hsn-lookup.component';
import { EditableGridComponent } from 'src/app/common/pages/editable-grid/editable-grid.component';
import { InventoryRoute } from './inventory.routing';
import { InvoiceRegisterComponent } from './pages/purchases/invoice-register/invoice-register.component';
import { InvoiceInBalesComponent } from './pages/purchases/invoice-in-bales/invoice-in-bales.component';
import { InvoicesComponent } from './pages/purchases/invoices/invoices.component';
import { InvoiceDetailsComponent } from './pages/purchases/invoice-details/invoice-details.component';
import { BarcodePrintingStandardComponent } from './pages/purchases/barcode-printing/barcode-printing-standard/barcode-printing-standard.component';
import { BarcodePrintingSmallComponent } from './pages/purchases/barcode-printing/barcode-printing-small/barcode-printing-small.component';
import { BarcodePrintingDoubleRateComponent } from './pages/purchases/barcode-printing/barcode-printing-double-rate/barcode-printing-double-rate.component';
import { GodownToCounterComponent } from './pages/goods-movement/internal-transfers/godown-to-counter/godown-to-counter.component';
import { CounterToGodownComponent } from './pages/goods-movement/internal-transfers/counter-to-godown/counter-to-godown.component';
import { GodownToCounterAcknowledgementComponent } from './pages/goods-movement/internal-transfers/godown-to-counter-acknowledgement/godown-to-counter-acknowledgement.component';
import { CounterToGodownAcknowledgementComponent } from './pages/goods-movement/internal-transfers/counter-to-godown-acknowledgement/counter-to-godown-acknowledgement.component';
import { GoodsTransferCityComponent } from './pages/goods-movement/goods-transfers/goods-transfer-city/goods-transfer-city.component';
import { GoodsTransferStateComponent } from './pages/goods-movement/goods-transfers/goods-transfer-state/goods-transfer-state.component';
import { PurchaseReturnEntryComponent } from './pages/purchase-return/purchase-return-entry/purchase-return-entry.component';
import { PurchaseOrderComponent } from './pages/purchase-orders/purchase-order/purchase-order.component';
import { SupplierBankDetailsComponent } from './pages/purchase-orders/supplier-bank-details/supplier-bank-details.component';
import { SingleFileAttachComponent } from 'src/app/common/shared/single-file-attach/single-file-attach.component';
import { GoodsTransferOtherStateComponent } from './pages/goods-movement/goods-transfers/goods-transfer-other-state/goods-transfer-other-state.component';
import { GoodsTransferStoreToStoreComponent } from './pages/goods-movement/goods-transfers/goods-transfer-store-to-store/goods-transfer-store-to-store.component';
import { GoodsTransferReturnableComponent } from './pages/goods-movement/goods-transfers/goods-transfer-returnable/goods-transfer-returnable.component';
import { PoAnalysisComponent } from './pages/purchase-orders/po-analysis/po-analysis.component';
import { InvoiceGstStatusUpdateComponent } from './pages/purchases/invoice-gst-status-update/invoice-gst-status-update.component';
import { PurchaseReturnApproveEditComponent } from './pages/purchase-return/purchase-return-approve-edit/purchase-return-approve-edit.component';
import { PurchaseReturnApproveComponent } from './pages/purchase-return/purchase-return-approve/purchase-return-approve.component';
import { PurchaseReturnDcComponent } from './pages/purchase-return/purchase-return-dc/purchase-return-dc.component';
import { PurchaseReturnNoteComponent } from './pages/purchase-return/purchase-return-note/purchase-return-note.component';
import { DaywiseTradingChecklistComponent } from './pages/purchase-return/purchase-return-reports/daywise-trading-checklist/daywise-trading-checklist.component';
import { PackingSlipComponent } from './pages/purchase-return/purchase-return-reports/packing-slip/packing-slip.component';
import { PurchaseReturnDcPrintComponent } from './pages/purchase-return/purchase-return-reports/purchase-return-dc-print/purchase-return-dc-print.component';
import { PurchaseReturnListComponent } from './pages/purchase-return/purchase-return-reports/purchase-return-list/purchase-return-list.component';
import { SupplierwisePurchaseReturnChecklistComponent } from './pages/purchase-return/purchase-return-reports/supplierwise-purchase-return-checklist/supplierwise-purchase-return-checklist.component';
import { TradingDrCrReportsComponent } from './pages/purchase-return/purchase-return-reports/trading-dr-cr-reports/trading-dr-cr-reports.component';
import { GoodsTransferInBalesComponent } from './pages/goods-movement/goods-receipt/goods-transfer-in-bales/goods-transfer-in-bales.component';
import { GoodsTransferReceiptsComponent } from './pages/goods-movement/goods-receipt/goods-transfer-receipts/goods-transfer-receipts.component';
import { GoodsTransferReceiptsReturnableComponent } from './pages/goods-movement/goods-receipt/goods-transfer-receipts-returnable/goods-transfer-receipts-returnable.component';
import { ProductionIssueWithImageComponent } from './pages/goods-movement/returnable-goods-transfer/production-issue-with-image/production-issue-with-image.component';
import { ProductionReceiptWithImageComponent } from './pages/goods-movement/returnable-goods-transfer/production-receipt-with-image/production-receipt-with-image.component';
import { WorkOrderComponent } from './pages/goods-movement/returnable-goods-transfer/work-order/work-order.component';
import { WorkOrderIssueComponent } from './pages/goods-movement/returnable-goods-transfer/work-order-issue/work-order-issue.component';
import { WorkOrderReceiptComponent } from './pages/goods-movement/returnable-goods-transfer/work-order-receipt/work-order-receipt.component';
import { PolishOrderComponent } from './pages/goods-movement/returnable-goods-transfer/polish-order/polish-order.component';
import { PolishOrderIssueComponent } from './pages/goods-movement/returnable-goods-transfer/polish-order-issue/polish-order-issue.component';
import { PolishOrderReceiptComponent } from './pages/goods-movement/returnable-goods-transfer/polish-order-receipt/polish-order-receipt.component';
import { SrtReportComponent } from './pages/goods-movement/srt/srt-report/srt-report.component';
import { SalesReturnOtherLocationComponent } from './pages/goods-movement/srt/sales-return-other-location/sales-return-other-location.component';
import { GoodsTransferReportsComponent } from './pages/goods-movement/transfer-reports/goods-transfer-reports/goods-transfer-reports.component';
import { GodownCounterTransferComponent } from './pages/goods-movement/transfer-reports/godown-counter-transfer/godown-counter-transfer.component';
import { CounterGodownTransferComponent } from './pages/goods-movement/transfer-reports/counter-godown-transfer/counter-godown-transfer.component';
import { PendingProductionIssueListComponent } from './pages/goods-movement/returnable-transfer-reports/pending-production-issue-list/pending-production-issue-list.component';
import { ProductionReceiptListComponent } from './pages/goods-movement/returnable-transfer-reports/production-receipt-list/production-receipt-list.component';
import { PolishPendingIssueReportComponent } from './pages/goods-movement/returnable-transfer-reports/polish-pending-issue-report/polish-pending-issue-report.component';
import { PendingWorkOrderListComponent } from './pages/goods-movement/returnable-transfer-reports/pending-work-order-list/pending-work-order-list.component';
import { PendingWorkOrderIssueComponent } from './pages/goods-movement/returnable-transfer-reports/pending-work-order-issue/pending-work-order-issue.component';
import { CustomersWorkOrderListComponent } from './pages/goods-movement/returnable-transfer-reports/customers-work-order-list/customers-work-order-list.component';
import { WorkOrderIssueListComponent } from './pages/goods-movement/returnable-transfer-reports/work-order-issue-list/work-order-issue-list.component';
import { WorkOrderReceiptListComponent } from './pages/goods-movement/returnable-transfer-reports/work-order-receipt-list/work-order-receipt-list.component';
import { MismatchStoneQuantityComponent } from './pages/goods-movement/returnable-transfer-reports/mismatch-stone-quantity/mismatch-stone-quantity.component';
import { DebitCreditNoteGstr1NonFilerComponent } from './pages/purchase-return/debit-credit-note/debit-credit-note-gstr1-non-filer/debit-credit-note-gstr1-non-filer.component';
import { DebitCreditNoteComponent } from './pages/purchase-return/debit-credit-note/debit-credit-note/debit-credit-note.component';
import { SupplierwiseCreditNoteComponent } from './pages/purchase-return/debit-credit-note/supplierwise-credit-note/supplierwise-credit-note.component';
import { GoodsTransferReturnableReportComponent } from './pages/goods-movement/transfer-reports/goods-transfer-returnable-report/goods-transfer-returnable-report.component';
import { PurchaseReturnNoteReportsComponent } from './pages/purchase-return/purchase-return-reports/purchase-return-note-reports/purchase-return-note-reports.component';
import { DiscountLookupComponent } from './pages/purchases/discount-lookup/discount-lookup.component';
import { WarehouseGatepassGenerationComponent } from './pages/goods-movement/warehouse-gatepass-generation/warehouse-gatepass-generation.component';
import { FloorGatepassGenerationComponent } from './pages/goods-movement/floor-gatepass-generation/floor-gatepass-generation.component';
import { SupplierDescriptionChangeComponent } from './pages/tools/supplier-description-change/supplier-description-change.component';
import { SingleProductsComponent } from './pages/tools/counter-change/single-products/single-products.component';
import { MultipleProductsComponent } from './pages/tools/counter-change/multiple-products/multiple-products.component';
import { GoodsTransferedProductsComponent } from './pages/tools/counter-change/goods-transfered-products/goods-transfered-products.component';
import { ProductChangeSingleComponent } from './pages/tools/product-change/product-change-single/product-change-single.component';
import { ProductChangeProductCodeComponent } from './pages/tools/product-change/product-change-product-code/product-change-product-code.component';
import { ProductChangeBynoSerialComponent } from './pages/tools/product-change/product-change-byno-serial/product-change-byno-serial.component';
import { ImageShootComponent } from './pages/tools/image/image-shoot/image-shoot.component';
import { ImageViewComponent } from './pages/tools/image/image-view/image-view.component';
import { SplitGoodsEntryComponent } from './pages/tools/split-goods/split-goods-entry/split-goods-entry.component';
import { SplitGoodsDetailsComponent } from './pages/tools/split-goods/split-goods-details/split-goods-details.component';
import { MergeGoodsEntryComponent } from './pages/tools/merge-goods/merge-goods-entry/merge-goods-entry.component';
import { MergeGoodsDetailsComponent } from './pages/tools/merge-goods/merge-goods-details/merge-goods-details.component';
import { GenerateNilBynoComponent } from './pages/tools/byno-generation/generate-nil-byno/generate-nil-byno.component';
import { BynoTrackingLocalGoodsComponent } from './pages/tools/byno-generation/byno-tracking-local-goods/byno-tracking-local-goods.component';
import { BynoTrackingTransferedGoodsComponent } from './pages/tools/byno-generation/byno-tracking-transfered-goods/byno-tracking-transfered-goods.component';
import { SupplierStyleCodeChangeComponent } from './pages/tools/supplier-style-code-change/supplier-style-code-change.component';
import { CostPriceModificationComponent } from './pages/pricing/cost-price-modification/cost-price-modification.component';
import { ByAmountComponent } from './pages/pricing/double-rate/by-amount/by-amount.component';
import { ByMarginCodeComponent } from './pages/pricing/selling-price-change/by-margin-code/by-margin-code.component';
import { ByMarginMultipleBynoComponent } from './pages/pricing/selling-price-change/by-margin-multiple-byno/by-margin-multiple-byno.component';
import { ByMarginSonComponent } from './pages/pricing/selling-price-change/by-margin-son/by-margin-son.component';
import { ByMarginComponent } from './pages/pricing/selling-price-change/by-margin/by-margin.component';
import { ByPercentageComponent } from './pages/pricing/selling-price-change/by-percentage/by-percentage.component';
import { DoubleRateToSingleRateComponent } from './pages/pricing/selling-price-change/double-rate-to-single-rate/double-rate-to-single-rate.component';
import { SingleProductWithPrintComponent } from './pages/pricing/selling-price-change/single-product-with-print/single-product-with-print.component';
import { SingleProductComponent } from './pages/pricing/selling-price-change/single-product/single-product.component';
import { TransferdGoodsComponent } from './pages/pricing/selling-price-change/transferd-goods/transferd-goods.component';
import { ByPercentageDoubleRateComponent } from './pages/pricing/double-rate/by-percentage-double-rate/by-percentage-double-rate.component';
import { ByPercentageDoubleRateSonComponent } from './pages/pricing/double-rate/by-percentage-double-rate-son/by-percentage-double-rate-son.component';
import { ByMarginDoubleRateComponent } from './pages/pricing/double-rate/by-margin-double-rate/by-margin-double-rate.component';
import { BynoWiseDiscountComponent } from './pages/pricing/double-rate/byno-wise-discount/byno-wise-discount.component';
import { EanCodeImportComponent } from './pages/ean/ean-code-import/ean-code-import.component';
import { EanCodeMasterComponent } from './pages/ean/ean-code-master/ean-code-master.component';
import { CoreMasterComponent } from './pages/ean/core-master/core-master.component';
import { CoreRatioMasterComponent } from './pages/ean/core-ratio-master/core-ratio-master.component';
import { RatioVsStockVsRefilComponent } from './pages/ean/ratio-vs-stock-vs-refil/ratio-vs-stock-vs-refil.component';
import { GeneratePoFromCoreComponent } from './pages/ean/generate-po-from-core/generate-po-from-core.component';
import { EanInvoiceComponent } from './pages/ean/ean-invoice/ean-invoice.component';
import { EanInvoiceDetailsComponent } from './pages/ean/ean-invoice-details/ean-invoice-details.component';
import { StyleCodeScaningComponent } from './pages/ean/utilities/style-code-scaning/style-code-scaning.component';
import { ScannedVsRatioVsStockComponent } from './pages/ean/utilities/scanned-vs-ratio-vs-stock/scanned-vs-ratio-vs-stock.component';
import { GetCoreInfoComponent } from './pages/ean/utilities/get-core-info/get-core-info.component';
import { CorewiseSalesReportComponent } from './pages/ean/utilities/corewise-sales-report/corewise-sales-report.component';
import { CorewiseStockReportComponent } from './pages/ean/utilities/corewise-stock-report/corewise-stock-report.component';
import { RatioVsStockVsRefillSummaryReportComponent } from './pages/ean/utilities/ratio-vs-stock-vs-refill-summary-report/ratio-vs-stock-vs-refill-summary-report.component';
import { InvoiceAnalysisComponent } from './pages/analysis/invoice-analysis/invoice-analysis.component';
import { UnpaidInvoiceAnalysisComponent } from './pages/analysis/unpaid-invoice-analysis/unpaid-invoice-analysis.component';
import { TradingAnalysisComponent } from './pages/analysis/trading-analysis/trading-analysis.component';
import { GoodsTransferAnalysisComponent } from './pages/analysis/goods-transfer-analysis/goods-transfer-analysis.component';
import { GoodsReceiptAnalysisComponent } from './pages/analysis/goods-receipt-analysis/goods-receipt-analysis.component';
import { BynoHistoryComponent } from './pages/analysis/byno-history/byno-history.component';
import { MovementCounterWiseComponent } from './pages/analysis/movement/movement-counter-wise/movement-counter-wise.component';
import { MovementProductWiseComponent } from './pages/analysis/movement/movement-product-wise/movement-product-wise.component';
import { StockAnalysisComponent } from './pages/analysis/stock/stock-analysis/stock-analysis.component';
import { StockAnalysisQtyComponent } from './pages/analysis/stock/stock-analysis-qty/stock-analysis-qty.component';
import { SupplierwiseStockAnalysisComponent } from './pages/analysis/stock/supplierwise-stock-analysis/supplierwise-stock-analysis.component';
import { NegativeStockComponent } from './pages/analysis/stock/negative-stock/negative-stock.component';
import { CurrentStockComponent } from './pages/analysis/stock/current-stock/current-stock.component';
import { BynowiseStockReportComponent } from './pages/analysis/stock/bynowise-stock-report/bynowise-stock-report.component';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { PhysicalStockClearanceComponent } from './pages/stock-audit/physical-stock-clearance/physical-stock-clearance.component';
import { StockVerificationGodownEntryComponent } from './pages/stock-audit/stock-verification/stock-verification-godown-entry/stock-verification-godown-entry.component';
import { StockVerificationCounterEntryComponent } from './pages/stock-audit/stock-verification/stock-verification-counter-entry/stock-verification-counter-entry.component';
import { StockVariationGodownStockComponent } from './pages/stock-audit/stock-variation-report/stock-variation-godown-stock/stock-variation-godown-stock.component';
import { StockVariationCounterStockComponent } from './pages/stock-audit/stock-variation-report/stock-variation-counter-stock/stock-variation-counter-stock.component';
import { GodownStockScaningComponent } from './pages/stock-audit/annual-stock-auditing/godown-stock-scaning/godown-stock-scaning.component';
import { GodownStockAckComponent } from './pages/stock-audit/annual-stock-auditing/godown-stock-ack/godown-stock-ack.component';
import { CounterStockScaningComponent } from './pages/stock-audit/annual-stock-auditing/counter-stock-scaning/counter-stock-scaning.component';
import { CounterStockAckComponent } from './pages/stock-audit/annual-stock-auditing/counter-stock-ack/counter-stock-ack.component';
import { GodownStockMtrsComponent } from './pages/stock-audit/annual-stock-auditing/godown-stock-mtrs/godown-stock-mtrs.component';
import { CounterStockMtrsComponent } from './pages/stock-audit/annual-stock-auditing/counter-stock-mtrs/counter-stock-mtrs.component';
import { CounterExcessEntryComponent } from './pages/stock-audit/stock-adjusment-entry/counter-excess-entry/counter-excess-entry.component';
import { CounterShortageEntryComponent } from './pages/stock-audit/stock-adjusment-entry/counter-shortage-entry/counter-shortage-entry.component';
import { GodownExcessEntryComponent } from './pages/stock-audit/stock-adjusment-entry/godown-excess-entry/godown-excess-entry.component';
import { GodownShortageEntryComponent } from './pages/stock-audit/stock-adjusment-entry/godown-shortage-entry/godown-shortage-entry.component';
import { GSTTaxComponent } from './pages/pricing/gst-price-change/gsttax/gsttax.component';
import { GstTaxSmallComponent } from './pages/pricing/gst-price-change/gst-tax-small/gst-tax-small.component';
import { GstTaxMaterialComponent } from './pages/pricing/gst-price-change/gst-tax-material/gst-tax-material.component';
import { MrpChangeComponent } from './pages/pricing/mrp-change/mrp-change.component';
import { ImageLookupComponent } from 'src/app/common/shared/image-lookup/image-lookup.component';
import { ProductLookupComponent } from 'src/app/common/shared/product-lookup/product-lookup.component';
import { WarehouseGatepassAcknowledgementComponent } from './pages/goods-movement/warehouse-gatepass-acknowledgement/warehouse-gatepass-acknowledgement.component';
import { ReturnedGoodsNotesComponent } from './pages/purchase-return/returned-goods-notes/returned-goods-notes.component';
import { ImagecaptureComponent } from './pages/goods-movement/returnable-goods-transfer/imagecapture/imagecapture.component';
import { PrintPageComponent } from './pages/tools/split-goods/print-page/print-page.component';
import { GodownToCounterExceptionComponent } from './pages/goods-movement/transfers-exception-reports/godown-to-counter-exception/godown-to-counter-exception.component';
import { CounterToGodownExceptionComponent } from './pages/goods-movement/transfers-exception-reports/counter-to-godown-exception/counter-to-godown-exception.component';
import { GoodsTransferExceptionComponent } from './pages/goods-movement/transfers-exception-reports/goods-transfer-exception/goods-transfer-exception.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CloneDirective } from 'src/app/common/shared/clone-thead';
import { PurchaseOrderSizeComponent } from './pages/purchase-orders/purchase-order-size/purchase-order-size.component';
import { SizeAndProductComponent } from './pages/goods-movement/returnable-goods-transfer/size-and-product/size-and-product.component';
import { BynoSelectionComponent } from "./pages/goods-movement/returnable-goods-transfer/byno-selection/byno-selection.component";
import { MultipleFilesAttachComponent } from "src/app/common/shared/multiple-files-attach/multiple-files-attach.component";
import { ReceiptDetailsComponent } from './pages/goods-movement/returnable-goods-transfer/receipt-details/receipt-details.component';
import { EstimatedVsReceivedComponent } from './pages/goods-movement/returnable-goods-transfer/estimated-vs-received/estimated-vs-received.component';
import { SizeDetailsComponent } from './pages/goods-movement/returnable-goods-transfer/size-details/size-details.component';
import { DetailedPurchaseOrderComponent } from './pages/purchase-orders/detailed-purchase-order/detailed-purchase-order.component';
import { StockVariationComponent } from './pages/stock-audit/stock-variation-report/stock-variation/stock-variation.component';
import { BynoSelectionWorkOrderIssueComponent } from "./pages/goods-movement/returnable-goods-transfer/byno-selection-work-order-issue/byno-selection-work-order-issue.component";
import { WebcamModule } from "ngx-webcam";
import { BynoSelectWorkorderInvoiceComponent } from './pages/goods-movement/returnable-goods-transfer/byno-select-workorder-invoice/byno-select-workorder-invoice.component';
import { EditCostPriceComponent } from './pages/purchases/edit-cost-price/edit-cost-price.component';
import { InvoiceProductDetailViewComponent } from './pages/purchases/invoice-product-detail-view/invoice-product-detail-view.component';
import { PriceChageHistoryComponent } from './pages/pricing/price-chage-history/price-chage-history.component';
import { CounterChangeHistoryComponent } from './pages/tools/counter-change/counter-change-history/counter-change-history.component';
import { ProductChangeHistoryComponent } from './pages/tools/product-change/product-change-history/product-change-history.component';
import { PurchaseOrderMailComponent } from './pages/purchase-orders/purchase-order-mail/purchase-order-mail.component';
import { InvoicesExcessShortageComponent } from './pages/purchases/invoices-excess-shortage/invoices-excess-shortage.component';
import { DetailedPurchaseOrderLocationComponent } from './pages/purchase-orders/detailed-purchase-order-location/detailed-purchase-order-location.component';
import { ProductionIssueComponent } from './pages/goods-movement/returnable-goods-transfer/production-issue/production-issue.component';
import { ProductionReceiptComponent } from './pages/goods-movement/returnable-goods-transfer/production-receipt/production-receipt.component';
import { IssueDetailComponent } from './pages/goods-movement/returnable-goods-transfer/issue-detail/issue-detail.component';
import { ProductionMaterialsComponent } from './pages/masters/production-materials/production-materials/production-materials.component';
import { ProductionProductsComponent } from './pages/masters/production-products/production-products.component';
import { ProductionProductLookupComponent } from "src/app/common/shared/production-product-lookup/production-product-lookup.component";
import { ProductionMaterialLookupComponent } from "src/app/common/shared/production-material-lookup/production-material-lookup.component";
import { BynoSelectionPolishOrderReceiptComponent } from './pages/goods-movement/returnable-goods-transfer/byno-selection-polish-order-receipt/byno-selection-polish-order-receipt.component';
import { MaterialLookupComponent } from "src/app/common/shared/material-lookup/material-lookup.component";
import { ProductionMaterialRequirementsComponent } from './pages/masters/production-material-requirements/production-material-requirements.component';
// import { MaterialLookupComponent } from "src/app/common/shared/material-lookup/material-lookup.component";
import { EstimatedProductLookupComponent } from "src/app/common/shared/estimated-product-lookup/estimated-product-lookup.component";
import { ProductionItemPricingComponent } from './pages/goods-movement/returnable-goods-transfer/production-item-pricing/production-item-pricing.component';

@NgModule({
  imports: [ 
    CommonModule,
    AngularMaterialModule,
    InventoryRoute,
    BrowserModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,
    NgSelectModule,
    NgxChartsModule,
    PerfectScrollbarModule,
    ScrollingModule,
    WebcamModule
  ],
  declarations: [
    CloneDirective,
    // MaterialLookupComponent,
    TimeFormat,
    ProductGroupsComponent,
    ProductAttributesComponent,
    ProductMasterComponent,
    ProductCategoriesComponent,
    HsnMasterComponent,
    ProductAttributeValuesComponent,
    ProductGroupAttributesComponent,
    HsnTaxRatesComponent,
    EditableGridComponent,
    PurchaseOrderComponent,
    SupplierBankDetailsComponent,
    InvoiceRegisterComponent,
    InvoiceInBalesComponent,
    InvoicesComponent,
    InvoiceDetailsComponent,
    BarcodePrintingStandardComponent,
    BarcodePrintingSmallComponent,
    BarcodePrintingDoubleRateComponent,
    GodownToCounterComponent,
    CounterToGodownComponent,
    GodownToCounterAcknowledgementComponent,
    CounterToGodownAcknowledgementComponent,
    GoodsTransferCityComponent,
    GoodsTransferStateComponent,
    PurchaseReturnEntryComponent,
    GoodsTransferOtherStateComponent,
    GoodsTransferStoreToStoreComponent,
    GoodsTransferReturnableComponent,
    PoAnalysisComponent,
    SupplierwiseCreditNoteComponent,
    DebitCreditNoteComponent,
    DebitCreditNoteGstr1NonFilerComponent,
    InvoiceGstStatusUpdateComponent,
    PurchaseReturnApproveComponent,
    PurchaseReturnApproveEditComponent,
    PurchaseReturnNoteComponent,
    PurchaseReturnDcComponent,
    PurchaseReturnListComponent,
    PackingSlipComponent,
    DaywiseTradingChecklistComponent,
    TradingDrCrReportsComponent,
    PurchaseReturnDcPrintComponent,
    SupplierwisePurchaseReturnChecklistComponent,
    GoodsTransferInBalesComponent,
    GoodsTransferReceiptsComponent,
    GoodsTransferReceiptsReturnableComponent,
    ProductionIssueWithImageComponent,
    ProductionReceiptWithImageComponent,
    WorkOrderComponent,
    WorkOrderIssueComponent,
    WorkOrderReceiptComponent,
    PolishOrderComponent,
    PolishOrderIssueComponent,
    PolishOrderReceiptComponent,
    SrtReportComponent,
    SalesReturnOtherLocationComponent,
    GoodsTransferReportsComponent,
    GodownCounterTransferComponent,
    CounterGodownTransferComponent,
    PendingProductionIssueListComponent,
    ProductionReceiptListComponent,
    PolishPendingIssueReportComponent,
    PendingWorkOrderListComponent,
    PendingWorkOrderIssueComponent,
    CustomersWorkOrderListComponent,
    WorkOrderIssueListComponent,
    WorkOrderReceiptListComponent,
    MismatchStoneQuantityComponent,
    GoodsTransferReturnableReportComponent,
    PurchaseReturnNoteReportsComponent,
    DiscountLookupComponent,
    WarehouseGatepassGenerationComponent,
    FloorGatepassGenerationComponent,
    SupplierDescriptionChangeComponent,
    SingleProductsComponent,
    MultipleProductsComponent,
    GoodsTransferedProductsComponent,
    ProductChangeSingleComponent,
    ProductChangeProductCodeComponent,
    ProductChangeBynoSerialComponent,
    ImageShootComponent,
    ImageViewComponent,
    SplitGoodsEntryComponent,
    SplitGoodsDetailsComponent,
    MergeGoodsEntryComponent,
    MergeGoodsDetailsComponent,
    GenerateNilBynoComponent,
    BynoTrackingLocalGoodsComponent,
    BynoTrackingTransferedGoodsComponent,
    SupplierStyleCodeChangeComponent,
    CostPriceModificationComponent,
    SingleProductComponent,
    SingleProductWithPrintComponent,
    TransferdGoodsComponent,
    ByPercentageComponent,
    DoubleRateToSingleRateComponent,
    ByMarginComponent,
    ByMarginSonComponent,
    ByMarginCodeComponent,
    ByMarginMultipleBynoComponent,
    ByAmountComponent,
    ByPercentageDoubleRateComponent,
    ByPercentageDoubleRateSonComponent,
    ByMarginDoubleRateComponent,
    BynoWiseDiscountComponent,
    EanCodeImportComponent,
    EanCodeMasterComponent,
    CoreMasterComponent,
    CoreRatioMasterComponent,
    RatioVsStockVsRefilComponent,
    GeneratePoFromCoreComponent,
    EanInvoiceComponent,
    EanInvoiceDetailsComponent,
    StyleCodeScaningComponent,
    ScannedVsRatioVsStockComponent,
    GetCoreInfoComponent,
    CorewiseSalesReportComponent,
    CorewiseStockReportComponent,
    RatioVsStockVsRefillSummaryReportComponent,
    InvoiceAnalysisComponent,
    UnpaidInvoiceAnalysisComponent,
    TradingAnalysisComponent,
    GoodsTransferAnalysisComponent,
    GoodsReceiptAnalysisComponent,
    BynoHistoryComponent,
    MovementCounterWiseComponent,
    MovementProductWiseComponent,
    StockAnalysisComponent,
    StockAnalysisQtyComponent,
    SupplierwiseStockAnalysisComponent,
    NegativeStockComponent,
    CurrentStockComponent,
    BynowiseStockReportComponent,
    PhysicalStockClearanceComponent,
    StockVerificationGodownEntryComponent,
    StockVerificationCounterEntryComponent,
    StockVariationGodownStockComponent,
    StockVariationCounterStockComponent,
    GodownStockScaningComponent,
    GodownStockAckComponent,
    CounterStockScaningComponent,
    CounterStockAckComponent,
    GodownStockMtrsComponent,
    CounterStockMtrsComponent,
    CounterExcessEntryComponent,
    CounterShortageEntryComponent,
    GodownExcessEntryComponent,
    GodownShortageEntryComponent,
    GSTTaxComponent,
    GstTaxSmallComponent,
    GstTaxMaterialComponent,
    MrpChangeComponent,
    WarehouseGatepassAcknowledgementComponent,
    ReturnedGoodsNotesComponent,
    ImagecaptureComponent,
    PrintPageComponent,
    GodownToCounterExceptionComponent,
    CounterToGodownExceptionComponent,
    GoodsTransferExceptionComponent,
    PurchaseOrderSizeComponent,
    SizeAndProductComponent,
    BynoSelectionComponent,
    ReceiptDetailsComponent,
    EstimatedVsReceivedComponent,
    SizeDetailsComponent,
    DetailedPurchaseOrderComponent,
    StockVariationComponent,
    BynoSelectionWorkOrderIssueComponent,
    BynoSelectWorkorderInvoiceComponent,
    EditCostPriceComponent,
    InvoiceProductDetailViewComponent,
    PriceChageHistoryComponent,
    CounterChangeHistoryComponent,
    ProductChangeHistoryComponent,
    PurchaseOrderMailComponent,
    InvoicesExcessShortageComponent,
    DetailedPurchaseOrderLocationComponent,
    ProductionIssueComponent,
    ProductionReceiptComponent,
    IssueDetailComponent,
    ProductionMaterialsComponent,
    ProductionProductsComponent,
    BynoSelectionPolishOrderReceiptComponent,
    ProductionMaterialRequirementsComponent,
    MaterialLookupComponent,
    EstimatedProductLookupComponent,
    ProductionItemPricingComponent
  ],
  exports: [EditableGridComponent],
  providers: [
    MinMaxDate,
    EmployeeLookupService,
    EmployeeLookupComponent,
    ConfirmationDeleteComponent,
  ],
  entryComponents: [
    EmployeeLookupComponent,
    ConfirmationDeleteComponent,
    AttributeLookupComponent,
    ProductGroupLookupComponent,
    HsnLookupComponent,
    SingleFileAttachComponent,
    SupplierBankDetailsComponent,
    DiscountLookupComponent,
    AccountsLookupComponent,
    ImageLookupComponent,
    ProductLookupComponent,
    ImagecaptureComponent,
    MultipleFilesAttachComponent,
    PurchaseOrderSizeComponent,
    SizeAndProductComponent,
    BynoSelectionComponent,
    PurchaseOrderSizeComponent,
    EstimatedVsReceivedComponent,
    ReceiptDetailsComponent,
    SizeDetailsComponent,
    BynoSelectionWorkOrderIssueComponent,
    BynoSelectWorkorderInvoiceComponent,
    EditCostPriceComponent,
    PurchaseOrderMailComponent,
    DetailedPurchaseOrderLocationComponent,
    IssueDetailComponent,
    ProductionProductLookupComponent,
    ProductionMaterialLookupComponent,
    BynoSelectionPolishOrderReceiptComponent,
    MaterialLookupComponent,
    EstimatedProductLookupComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class InventoryModule { }
