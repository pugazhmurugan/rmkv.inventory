import { TestBed } from '@angular/core/testing';

import { DetailedPurchaseOrderService } from './detailed-purchase-order.service';

describe('DetailedPurchaseOrderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetailedPurchaseOrderService = TestBed.get(DetailedPurchaseOrderService);
    expect(service).toBeTruthy();
  });
});
