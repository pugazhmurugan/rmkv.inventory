import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class DetailedPurchaseOrderService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getDetailedPOList(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/GetDetailedPODetails', obj)
      .pipe(catchError(this.handleError));
  }

  public addDetailedPODetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/SaveDetailedPODetails', obj)
      .pipe(catchError(this.handleError));
  }  

  public fetchDetailedPODetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/FetchDetailedPODetails', obj)
      .pipe(catchError(this.handleError));
  } 

  public deleteDetailedPODetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/CancelDetailedPODetails', obj)
      .pipe(catchError(this.handleError));
  } 
  
  public getBrandDetailedPODetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/GetBrandDetailedPODetails', obj)
      .pipe(catchError(this.handleError));
  } 

  public getProductGroupDetailedPODetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/GetProductGroupDetailedPODetails', obj)
      .pipe(catchError(this.handleError));
  } 

  public getProductDetailedPODetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/GetProductDetailedPODetails', obj)
      .pipe(catchError(this.handleError));
  }

  public getDesignCodeDetailedPODetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/GetDesignCodeDetailedPODetails', obj)
      .pipe(catchError(this.handleError));
  }

  public getColorListDetailedPODetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/GetColorListForDetailedPODetails', obj)
      .pipe(catchError(this.handleError));
  }

  public getQualityListDetailedPODetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/GetQualityListForDetailedPODetails', obj)
      .pipe(catchError(this.handleError));
  }

  public getSizeListDetailedPODetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/GetSizeListForDetailedPODetails', obj)
      .pipe(catchError(this.handleError));
  }

  public getLocationsDetailedPODetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/GetLocationsDetailedPODetails', obj)
      .pipe(catchError(this.handleError));
  }

  public getDetailedPONoDetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/GetDetailedPONoDetails', obj)
      .pipe(catchError(this.handleError));
  }

  public getMailDetailedlPurchaseOrder(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'DetailedPurchaseOrder/GetEmailDetailedPurchaseOrderDetails', obj)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
}
