import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class PurchaseOrdersService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getPOAnalysis(objPO: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/GetPOAnalysis', objPO)
      .pipe(catchError(this.handleError));
  }

  public getSupplierCitiesList() {
    return this._httpClient.get(this._localStorage.getApiUrl() + 'PurchaseOrders/GetSupplierCityDetails')
      .pipe(catchError(this.handleError));
  }

  public getSupplierGroupList(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/GetSupplierGroupDetails', obj)
      .pipe(catchError(this.handleError));
  }

  public getSupplierByGroupIdList(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/GetSupplierByGroupIdList', obj)
      .pipe(catchError(this.handleError));
  }

  public getModeOfDispatchList(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/GetModeOfDispatchDetails', obj)
      .pipe(catchError(this.handleError));
  }

  public getUOMMasterListDetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/GetUOMMasterDetails', obj)
      .pipe(catchError(this.handleError));
  }

  public getPurchaseOrderList(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/GetPurchaseOrderDetails', obj)
      .pipe(catchError(this.handleError));
  }

  public addPurchaseOrdersDetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/SavePurchaseOrderDetails', obj)
      .pipe(catchError(this.handleError));
  }  

  public getDispatchNameListDetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/GetDispatchNameListDetails', obj)
      .pipe(catchError(this.handleError));
  } 

  public fetchPurchaseOrderDetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/FetchPurchaseOrderDetails', obj)
      .pipe(catchError(this.handleError));
  } 

  public getPOSizeNameDetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/GetPurchaseOrderSizeName', obj)
      .pipe(catchError(this.handleError));
  } 

  public getPOSizeNameBasedSizesDetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/GetPOSizeNameBasedSizes', obj)
      .pipe(catchError(this.handleError));
  }

  public getPONoDetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/GetPONoDetails', obj)
      .pipe(catchError(this.handleError));
  }

  public UploadTempFile(formData: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Shared/UploadGRNTempFile', formData)
    .pipe(catchError(this.handleError));
  }

  public deletePurchaseOrdersDetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/CancelPurchaseOrderDetails', obj)
      .pipe(catchError(this.handleError));
  } 

///////////////////////// Purchase Order Email //////////////////////////////////////////////////////////////

public sendManualMail(obj: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/GetPONoDetails', obj)
    .pipe(catchError(this.handleError));
}

public getMailPurchaseOrderDetails(obj: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseOrders/GetEmailPurchaseOrderDetails', obj)
    .pipe(catchError(this.handleError));
}

  private handleError(error: Response) {
    return throwError(error);
  }
}
