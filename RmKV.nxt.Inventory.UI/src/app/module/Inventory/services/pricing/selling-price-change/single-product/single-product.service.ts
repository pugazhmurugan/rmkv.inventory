import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class SingleProductService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getSingleProduct(objGet: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'SingleProduct/GetSingleProduct', objGet)
      .pipe(catchError(this.handleError));
  }

  public addSingleProduct(objAdd: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'SingleProduct/AddSingleProduct', objAdd)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }



  public getPriceChangeReport(objView : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'SingleProduct/GetPriceChangeHistoryReport', objView
    , { responseType: 'blob' as 'json' }).pipe(catchError(this.handleError));
  }

}
