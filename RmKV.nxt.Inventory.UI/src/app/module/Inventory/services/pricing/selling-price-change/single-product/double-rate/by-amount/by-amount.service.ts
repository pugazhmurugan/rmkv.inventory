import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class ByAmountService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public addByAmount(objAdd: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ByAmount/AddByAmount', objAdd)
      .pipe(catchError(this.handleError));
  }

  public addByPercentage(objAdd: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ByAmount/AddByPercentage', objAdd)
      .pipe(catchError(this.handleError));
  }

  public checkMargin(objCheck: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ByAmount/CheckMargin', objCheck)
      .pipe(catchError(this.handleError));
  }

  public getMarginCalculation(objGet: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ByAmount/GetMarginCalculate', objGet)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
}
