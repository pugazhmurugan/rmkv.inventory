import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class MRPChangeService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public addMRPChanges(objAdd: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'MRPChange/AddMRPChanges', objAdd)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
}
