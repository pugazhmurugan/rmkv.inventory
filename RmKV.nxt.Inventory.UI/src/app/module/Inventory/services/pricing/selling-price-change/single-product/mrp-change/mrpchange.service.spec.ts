import { TestBed } from '@angular/core/testing';

import { MRPChangeService } from './mrpchange.service';

describe('MRPChangeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MRPChangeService = TestBed.get(MRPChangeService);
    expect(service).toBeTruthy();
  });
});
