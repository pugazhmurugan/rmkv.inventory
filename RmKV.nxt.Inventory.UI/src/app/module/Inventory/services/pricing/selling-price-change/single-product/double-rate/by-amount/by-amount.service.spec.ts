import { TestBed } from '@angular/core/testing';

import { ByAmountService } from './by-amount.service';

describe('ByAmountService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ByAmountService = TestBed.get(ByAmountService);
    expect(service).toBeTruthy();
  });
});
