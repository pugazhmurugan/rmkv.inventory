import { TestBed } from '@angular/core/testing';

import { EwaybillService } from './ewaybill.service';

describe('EwaybillService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EwaybillService = TestBed.get(EwaybillService);
    expect(service).toBeTruthy();
  });
});
