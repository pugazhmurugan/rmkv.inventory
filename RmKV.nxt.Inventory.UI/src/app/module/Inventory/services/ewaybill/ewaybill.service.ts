import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class EwaybillService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public ewayBillLogin(obj) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + "EwayBill/EwayBillLogin",obj)
      .pipe(catchError(this.handleError));;
  }

  public ewayBillValidate(obj) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + "EwayBill/EwayBillValidate",obj)
      .pipe(catchError(this.handleError));;
  }

  public ewayBillDetails(obj) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + "EwayBill/EwayBillDetails",obj)
      .pipe(catchError(this.handleError));;
  }

  public generateEwayBillandEInvoice(obj) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + "EwayBill/GenerateEwayBillandEInvoice",obj)
      .pipe(catchError(this.handleError));;
  }

  public generateEwayBill(obj) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + "EwayBill/GenerateEwayBill",obj)
      .pipe(catchError(this.handleError));;
  }

  public cancelEwayBillandEInvoice(obj) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + "EwayBill/CancelEwayBillandEInvoice",obj)
      .pipe(catchError(this.handleError));;
  }

  public cancelEwayBill(obj) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + "EwayBill/CancelEwayBill",obj)
      .pipe(catchError(this.handleError));;
  }
 
  private handleError(error: Response) {
    return throwError(error);
  }
}
