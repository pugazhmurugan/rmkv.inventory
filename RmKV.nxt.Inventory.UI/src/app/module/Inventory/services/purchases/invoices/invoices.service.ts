import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class InvoicesService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public checkPriceCode(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/CheckPriceCode', objInvoice)
      .pipe(catchError(this.handleError));
  }

  public getSupplierDescription(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/GetSupplierDescription', objInvoice)
      .pipe(catchError(this.handleError));
  }

  public checkInvoiceEntryConfigs(objCheck: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/CheckDiscountAmount', objCheck)
      .pipe(catchError(this.handleError));
  }

  public getGstByHSNCode(objGet: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/GetGstByHSN', objGet)
      .pipe(catchError(this.handleError));
  }

  public getInvoiceNos(objGet: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/GetInvoiceNos', objGet)
      .pipe(catchError(this.handleError));
  }

  public getMasterDiscount(objGet: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/GetMasterDiscount', objGet)
      .pipe(catchError(this.handleError));
  }

  public getInvoicePoNos(objGet: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/GetInvoicePoNos', objGet)
      .pipe(catchError(this.handleError));
  }

  public addInvoices(objAdd: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/AddInvoices', objAdd)
      .pipe(catchError(this.handleError));
  }

  public fetchInvoices(objFetch: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/FetchInvoices', objFetch)
      .pipe(catchError(this.handleError));
  }

  public getInvoices(objGet: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/GetInvoices', objGet)
      .pipe(catchError(this.handleError));
  }

  public approveInvoices(objApprove: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/ApproveInvoices', objApprove)
      .pipe(catchError(this.handleError));
  }

  public postInvoices(objPost: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/PostInvoices', objPost)
      .pipe(catchError(this.handleError));
  }

  public editCostPrice(objEdit: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/EditCostPrice', objEdit)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }

  public getInvoiceCheckListReport(objLoad: any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Invoices/GetInvoiceCheckListReport', objLoad, {responseType: 'blob' as 'json'})
      .pipe(catchError(this.handleError));
  }
}
