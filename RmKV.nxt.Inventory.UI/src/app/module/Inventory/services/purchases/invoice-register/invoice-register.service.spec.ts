import { TestBed } from '@angular/core/testing';

import { InvoiceRegisterService } from './invoice-register.service';

describe('InvoiceRegisterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InvoiceRegisterService = TestBed.get(InvoiceRegisterService);
    expect(service).toBeTruthy();
  });
});
