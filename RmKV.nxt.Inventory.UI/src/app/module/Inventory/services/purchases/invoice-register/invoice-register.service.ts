import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class InvoiceRegisterService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  
  public getInvoiceRegister(InvoiceRegister: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceRegister/GetInvoiceRegister', InvoiceRegister)
      .pipe(catchError(this.handleError));
  }

  public saveInvoiceRegister(InvoiceRegister: any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceRegister/AddInvoiceRegister', InvoiceRegister)
      .pipe(catchError(this.handleError));
  }

  public fetchInvoiceRegister(InvoiceRegister: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceRegister/FetchInvoiceRegister', InvoiceRegister)
      .pipe(catchError(this.handleError));
  }

  public removeInvoiceRegister(InvoiceRegister: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceRegister/DeleteInvoiceRegister', InvoiceRegister)
      .pipe(catchError(this.handleError));
  }

  public approveInvoiceRegister(InvoiceRegister: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceRegister/ApproveInvoiceRegister', InvoiceRegister)
      .pipe(catchError(this.handleError));
  }


  private handleError(error: Response) {
    return throwError(error);
  }
}
