import { TestBed } from '@angular/core/testing';

import { InvoiceInBalesService } from './invoice-in-bales.service';

describe('InvoiceInBalesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InvoiceInBalesService = TestBed.get(InvoiceInBalesService);
    expect(service).toBeTruthy();
  });
});
