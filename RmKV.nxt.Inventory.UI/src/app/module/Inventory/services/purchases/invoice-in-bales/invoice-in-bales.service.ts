import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class InvoiceInBalesService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getInvoiceInBales(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceInBale/GetInvoiceInBales', objInvoice)
      .pipe(catchError(this.handleError));
  }

  public addInvoiceInBales(objInvoice : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceInBale/AddInvoiceInBales', objInvoice)
      .pipe(catchError(this.handleError));
  }

  public fetchInvoiceInBales(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceInBale/FetchInvoiceInBales', objInvoice)
      .pipe(catchError(this.handleError));
  }

  public deleteInvoiceInBales(objInvoice : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceInBale/DeleteInvoiceInBales', objInvoice)
    .pipe(catchError(this.handleError));
  }

  public getSectionGRNNos(objSectionGRN: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceInBale/GetSectionGRNNos', objSectionGRN)
    .pipe(catchError(this.handleError));
  }
  private handleError(error: Response) {
    return throwError(error);
  }
}
