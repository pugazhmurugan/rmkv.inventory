import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class InvoiceDetailsService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getInvoiceProdDetails(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceProdDetails/GetInvoiceProdDetails', objInvoice)
      .pipe(catchError(this.handleError));
  }
  public getProductLookupList(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceProdDetails/GetInvoiceProductLookup', objInvoice)
      .pipe(catchError(this.handleError));
  }
  public getProductLookupDetails(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceProdDetails/GetProductLookupDetails', objInvoice)
      .pipe(catchError(this.handleError));
  }
  public getInvoiceProdAttributes(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceProdDetails/GetInvoiceProdAttributes', objInvoice)
      .pipe(catchError(this.handleError));
  }
  public getCounterDetails(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceProdDetails/GetCounterDetails', objInvoice)
      .pipe(catchError(this.handleError));
  }
  public fetchInvoiceProdDetails(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceProdDetails/FetchInvoiceProductDetails', objInvoice)
      .pipe(catchError(this.handleError));
  }
  public sectionWiseAttribute(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceProdDetails/SectionWiseAttribute', objInvoice)
      .pipe(catchError(this.handleError));
  }
  public getAttributesWiseProductDetails(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceProdDetails/GetAttributesWiseProductDetails', objInvoice)
      .pipe(catchError(this.handleError));
  }
  public addInvoiceProdDetails(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceProdDetails/AddInvoiceProdDetails', objInvoice)
      .pipe(catchError(this.handleError));
  }
  public getAttributeSizeList(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceProdDetails/GetAttributesValuesDetails', objInvoice)
      .pipe(catchError(this.handleError));
  }
  public getProductDetailsByBrandType(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'InvoiceProdDetails/GetProductDetailsByBrandType', objInvoice)
      .pipe(catchError(this.handleError));
  }
  private handleError(error: Response) {
    return throwError(error);
  }
}