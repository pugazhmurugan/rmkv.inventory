import { TestBed } from '@angular/core/testing';

import { BaarcodePrintingService } from './baarcode-printing.service';

describe('BaarcodePrintingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BaarcodePrintingService = TestBed.get(BaarcodePrintingService);
    expect(service).toBeTruthy();
  });
});
