import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class BaarcodePrintingService {


  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getBarcodePrinting(InvoiceRegister: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'BarCodePrinting/GetBarCodePrinting', InvoiceRegister)
      .pipe(catchError(this.handleError));
  }

  public selectBarCodePrinting(selectPrintingList: any): Observable<any> {
    debugger;   
    return this._httpClient.post(this._localStorage.getApiUrl() + 'BarCodePrinting/SelectBarCodePrinting', selectPrintingList)
      .pipe(catchError(this.handleError));
  }
  public doubleRateBarCodePrinting(selectPrintingList: any): Observable<any> {
    debugger;   
    return this._httpClient.post(this._localStorage.getApiUrl() + 'BarCodePrinting/DoubleRateBarCodePrinting', selectPrintingList)
      .pipe(catchError(this.handleError));
  }
  public ImageBytes(selectPrintingList: any): Observable<any> {
    debugger;   
    return this._httpClient.post(this._localStorage.getApiUrl() + 'BarCodePrinting/ImageBytes', selectPrintingList)
      .pipe(catchError(this.handleError));
  }
  private handleError(error: Response) {
    return throwError(error);
  }
}
