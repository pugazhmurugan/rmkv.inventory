import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class ProductionIssueService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public addProductionIssue(objPI: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionIssue/AddProductionIssue', objPI)
      .pipe(catchError(this.handleError));
  }

  public cancelProductionIssue(objPI: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionIssue/CancelProductionIssue', objPI)
      .pipe(catchError(this.handleError));
  }

  public getProductionIssue(objPI: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionIssue/GetProductionIssue', objPI)
      .pipe(catchError(this.handleError));
  }

  public fetchProductionIssue(objPI: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionIssue/FetchProductionIssue', objPI)
      .pipe(catchError(this.handleError));
  }

  public getProductionIssueReconciliation(objPI: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionIssue/GetProductionIssueReconciliation', objPI)
      .pipe(catchError(this.handleError));
  }

  public getProductionIssueEstimatedVsReceived(objPI: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionIssue/GetProductionIssueEstimatedVsReceived', objPI)
      .pipe(catchError(this.handleError));
  }

  public addProductionIssueReconciliation(objPI: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionIssue/AddProductionIssueReconciliation', objPI)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }

}
