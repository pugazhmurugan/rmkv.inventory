import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class ProductionReceiptService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getProductionReceipts(objLoad: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionReceipt/GetProductionReceipts', objLoad)
      .pipe(catchError(this.handleError));
  }

  public getPendingProductionIssueNos(objLoad: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionReceipt/GetProductionIssueNos', objLoad)
      .pipe(catchError(this.handleError));
  }

  public getPendingProductionIssueDetails(objLoad: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionReceipt/GetProductionIssueDetails', objLoad)
      .pipe(catchError(this.handleError));
  }

  public getProductionReceiptProductLookup(objLoad: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionReceipt/GetProductionReceiptProductLookup', objLoad)
      .pipe(catchError(this.handleError));
  }

  public addProductionReceipts(objAdd: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionReceipt/AddProductionReceipt', objAdd)
      .pipe(catchError(this.handleError));
  }

  public removeProductionReceipts(objRemove: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionReceipt/RemoveProductionReceipt', objRemove)
      .pipe(catchError(this.handleError));
  }

  public fetchProductionReceipts(objFetch: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionReceipt/FetchProductionReceipt', objFetch)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
}
