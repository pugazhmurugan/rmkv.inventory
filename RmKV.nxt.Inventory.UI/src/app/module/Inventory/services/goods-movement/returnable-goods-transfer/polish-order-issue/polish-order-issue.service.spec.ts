import { TestBed } from '@angular/core/testing';

import { PolishOrderIssueService } from './polish-order-issue.service';

describe('PolishOrderIssueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PolishOrderIssueService = TestBed.get(PolishOrderIssueService);
    expect(service).toBeTruthy();
  });
});
