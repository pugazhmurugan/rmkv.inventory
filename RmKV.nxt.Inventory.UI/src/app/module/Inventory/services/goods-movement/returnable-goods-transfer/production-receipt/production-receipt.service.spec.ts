import { TestBed } from '@angular/core/testing';

import { ProductionReceiptService } from './production-receipt.service';

describe('ProductionReceiptService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductionReceiptService = TestBed.get(ProductionReceiptService);
    expect(service).toBeTruthy();
  });
});
