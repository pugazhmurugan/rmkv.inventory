import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class WorkOrderIssueService {


  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }
  
  public getWorkOrderIssueList(WorkOrderIssue: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WorkOrderIssue/GetWorkOrderIssue', WorkOrderIssue)
      .pipe(catchError(this.handleError));
  }
  
  public getWorkOrderIssueDetailsList(WorkOrderIssue: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WorkOrderIssue/GetWorkOrderIssueDetails', WorkOrderIssue)
      .pipe(catchError(this.handleError));
  }

  public getTransferTypeList() {
    return this._httpClient.get(this._localStorage.getApiUrl() + 'WorkOrderIssue/GetWorkOrderIssueTransferType')
      .pipe(catchError(this.handleError));
  }

  public getWorkOrderIssueEntryGridList(WorkOrderIssue: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WorkOrderIssue/GetWorkOrderIssueEntryGridList', WorkOrderIssue)
      .pipe(catchError(this.handleError));
  }

  public saveWorkOrderissue(WorkOrderIssue: any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WorkOrderIssue/AddWorkOrderIssue', WorkOrderIssue)
      .pipe(catchError(this.handleError));
  }

  public fetchWorkOrderissue(WorkOrderIssue: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WorkOrderIssue/FetchWorkOrderIssue', WorkOrderIssue)
      .pipe(catchError(this.handleError));
  }
//workorderreceiptList//

public getWorkOrderReceiptList(WorkOrderIssue: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'WorkOrderReceipt/GetWorkOrderReceiptList', WorkOrderIssue)
    .pipe(catchError(this.handleError));
}
public getWorkOrderReceiptListPending(WorkOrderIssue: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'WorkOrderReceipt/GetWorkOrderReceiptListPending', WorkOrderIssue)
    .pipe(catchError(this.handleError));
}

public addWorkOrderinvoice(WorkOrderIssue: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'WorkOrderReceipt/AddWorkOrderReceiptList', WorkOrderIssue)
    .pipe(catchError(this.handleError));
}
public getWorkOrderReceiptDetailsList(WorkOrderIssue: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'WorkOrderReceipt/GetWorkOrderReceiptPendindIsssueDetails', WorkOrderIssue)
    .pipe(catchError(this.handleError));
}
public fetchWorkOrderReceipt(WorkOrderIssue: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'WorkOrderReceipt/FetchWorkOrderReceipt', WorkOrderIssue)
    .pipe(catchError(this.handleError));
}
  private handleError(error: Response) {
    return throwError(error);
  }
}
