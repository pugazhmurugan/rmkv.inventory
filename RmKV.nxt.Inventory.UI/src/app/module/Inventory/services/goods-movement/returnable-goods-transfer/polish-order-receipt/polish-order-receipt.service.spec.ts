import { TestBed } from '@angular/core/testing';

import { PolishOrderReceiptService } from './polish-order-receipt.service';

describe('PolishOrderReceiptService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PolishOrderReceiptService = TestBed.get(PolishOrderReceiptService);
    expect(service).toBeTruthy();
  });
});
