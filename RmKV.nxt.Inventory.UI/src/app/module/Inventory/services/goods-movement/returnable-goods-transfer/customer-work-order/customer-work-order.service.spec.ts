import { TestBed } from '@angular/core/testing';

import { CustomerWorkOrderService } from './customer-work-order.service';

describe('CustomerWorkOrderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomerWorkOrderService = TestBed.get(CustomerWorkOrderService);
    expect(service).toBeTruthy();
  });
});
