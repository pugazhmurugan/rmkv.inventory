import { TestBed } from '@angular/core/testing';

import { ProductionIssueService } from './production-issue.service';

describe('ProductionIssueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductionIssueService = TestBed.get(ProductionIssueService);
    expect(service).toBeTruthy();
  });
});
