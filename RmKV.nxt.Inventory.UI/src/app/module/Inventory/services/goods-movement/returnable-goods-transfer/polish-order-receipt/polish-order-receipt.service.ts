import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class PolishOrderReceiptService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getPolishOrderReceipts(objLoad: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PolishOrderReceipt/GetPolishOrderReceipts', objLoad)
      .pipe(catchError(this.handleError));
  }

  public getPendingPolishIssueNosList(objLoad: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PolishOrderReceipt/GetPendingPolishIssueNos', objLoad)
      .pipe(catchError(this.handleError));
  }

  public getPendingPolishIssueDetails(objLoad: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PolishOrderReceipt/GetPendingPolishIssueDetails', objLoad)
      .pipe(catchError(this.handleError));
  }

  public addPolishOrderReceipts(objAdd: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PolishOrderReceipt/AddPolishOrderReceipts', objAdd)
      .pipe(catchError(this.handleError));
  }

  public fetchPolishOrderReceipts(objAdd: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PolishOrderReceipt/FetchPolishOrderReceiptDetails', objAdd)
      .pipe(catchError(this.handleError));
  }

  public removePolishOrderReceipts(objRemove: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PolishOrderReceipt/RemovePolishOrderReceipts', objRemove)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
}
