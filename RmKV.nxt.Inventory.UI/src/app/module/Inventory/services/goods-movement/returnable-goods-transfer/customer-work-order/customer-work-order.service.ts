import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class CustomerWorkOrderService {
  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public addCustomerWorkOrder(objCWO: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CustomerWorkOrder/AddCustomerWorkOrder', objCWO)
      .pipe(catchError(this.handleError));
  }

  public cancelCustomerWorkOrder(objCWO: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CustomerWorkOrder/CancelCustomerWorkOrder', objCWO)
      .pipe(catchError(this.handleError));
  }

  public getCustomerWorkOrder(objCWO: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CustomerWorkOrder/GetCustomerWorkOrder', objCWO)
      .pipe(catchError(this.handleError));
  }

  public fetchCustomerWorkOrder(objCWO: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CustomerWorkOrder/FetchCustomerWorkOrder', objCWO)
      .pipe(catchError(this.handleError));
  }

  public getToWarehouse(objCWO: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CounterToGodown/GetToWarehouse', objCWO)
      .pipe(catchError(this.handleError));
  }

  public getTransferTypeList() {
    return this._httpClient.get(this._localStorage.getApiUrl() + 'WorkOrderIssue/GetWorkOrderIssueTransferType')
      .pipe(catchError(this.handleError));
  }

  public addCustomerWorkOrderDocuments(formData: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CustomerWorkOrder/UpdateCustomerWorkOrderDocuments', formData)
      .pipe(catchError(this.handleError));
  }


  private handleError(error: Response) {
    return throwError(error);
  }

}