import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class PolishOrderIssueService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }
 
  public getPolishOrderIssueList(PolishOrderIssue: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PolishOrderIssue/GetPolishOrderIssue', PolishOrderIssue)
      .pipe(catchError(this.handleError));
  }

  public deletePolishOrderIssue(PolishOrderIssue: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PolishOrderIssue/DeletePolishOrderIssue', PolishOrderIssue)
      .pipe(catchError(this.handleError));
  }

  public addPolishOrderIssue(PolishOrderIssue: any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PolishOrderIssue/AddPolishOrderIssue', PolishOrderIssue)
      .pipe(catchError(this.handleError));
  }

  public fetchPolishOrderIssue(PolishOrderIssue: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PolishOrderIssue/FetchPolishOrderIssue', PolishOrderIssue)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }

}
