import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class GoodsTransferService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }
  public getWithincityVirtualToStore(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetWithincityVirtualToStore', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getWithinStateToLocation(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetWithinStateToLocation', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getOtherStateToLocation(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetOtherStateToLocation', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getWithinStateVirtualStoreToStore(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetWithinStateVirtualStoreToStore', objProduct)
      .pipe(catchError(this.handleError));
  }
  public goodsTransferFromStore(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetGoodsTransferFromStore', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getGoodsTransferType(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetGoodsTransferType', objProduct)
      .pipe(catchError(this.handleError));
  } 
  ////////////////////////////////////////Goods Transfer (with In City)/////////////////////////////////////////

  public addWithincity(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/AddWithincity', objProduct)
      .pipe(catchError(this.handleError));
  }
  public cancelWithincity(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/CancelWithincity', objProduct)
      .pipe(catchError(this.handleError));
  }
  public fetchWithincity(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/FetchWithincity', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getWithincity(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetWithincity', objProduct)
      .pipe(catchError(this.handleError));
  }
  public approveWithincity(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/ApproveWithincity', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getTransferNoWithincity(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetTransferNoWithincity', objProduct)
      .pipe(catchError(this.handleError));
  }



 ////////////////////////////////////////Goods Transfer (with In State)/////////////////////////////////////////

 public addWithinState(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/AddWithinState', objProduct)
    .pipe(catchError(this.handleError));
}
public cancelWithinState(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/CancelWithinState', objProduct)
    .pipe(catchError(this.handleError));
}
public fetchWithinState(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/FetchWithinState', objProduct)
    .pipe(catchError(this.handleError));
}
public getWithinState(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetWithinState', objProduct)
    .pipe(catchError(this.handleError));
}
public approveWithinState(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/ApproveWithinState', objProduct)
    .pipe(catchError(this.handleError));
}
public getTransferNoWithinState(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetTransferNoWithinState', objProduct)
    .pipe(catchError(this.handleError));
}


 ////////////////////////////////////////Goods Transfer (OtherState)/////////////////////////////////////////

 public addOtherState(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/AddOtherState', objProduct)
    .pipe(catchError(this.handleError));
}
public cancelOtherState(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/CancelOtherState', objProduct)
    .pipe(catchError(this.handleError));
}
public fetchOtherState(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/FetchOtherState', objProduct)
    .pipe(catchError(this.handleError));
}
public getOtherState(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetOtherState', objProduct)
    .pipe(catchError(this.handleError));
}
public approveOtherState(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/ApproveOtherState', objProduct)
    .pipe(catchError(this.handleError));
}
public getTransferNoOtherState(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetTransferNoOtherState', objProduct)
    .pipe(catchError(this.handleError));
}



////////////////////////////////////////Goods Transfer (StoreToStore)/////////////////////////////////////////

public addStoreToStore(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/AddStoreToStore', objProduct)
    .pipe(catchError(this.handleError));
}
public cancelStoreToStore(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/CancelStoreToStore', objProduct)
    .pipe(catchError(this.handleError));
}
public fetchStoreToStore(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/FetchStoreToStore', objProduct)
    .pipe(catchError(this.handleError));
}
public getStoreToStore(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetStoreToStore', objProduct)
    .pipe(catchError(this.handleError));
}
public approveStoreToStore(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/ApproveStoreToStore', objProduct)
    .pipe(catchError(this.handleError));
}
public getTransferNoStoreToStore(objProduct: any) {
  return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GetTransferNoStoreToStore', objProduct)
    .pipe(catchError(this.handleError));
}

  private handleError(error: Response) {
    return throwError(error);
  }

  ////////////////////////////////////////Goods Transfer Report/////////////////////////////////////////

  public getGoodTransferReport(objLoad: any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/GoodsTransferReportDetails', objLoad, {responseType: 'blob' as 'json'})
      .pipe(catchError(this.handleError));
  }

  public checkGoodsTransferReport(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransfer/CheckGoodsTransferReportDetails', objProduct)
      .pipe(catchError(this.handleError));
  }

}
