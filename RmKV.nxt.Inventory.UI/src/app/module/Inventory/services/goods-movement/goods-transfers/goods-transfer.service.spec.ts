import { TestBed } from '@angular/core/testing';

import { GoodsTransferService } from './goods-transfer.service';

describe('GoodsTransferService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GoodsTransferService = TestBed.get(GoodsTransferService);
    expect(service).toBeTruthy();
  });
});
