import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class TransfersExceptionReportsService {

  constructor(public _httpClient: HttpClient,
    public _localStorage: LocalStorage) { }

  public getGoodsTransferException(objReport: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransferException/GetGoodsTransferExceptionReport', objReport, {responseType: 'blob' as 'json'})
      .pipe(catchError(this.handleError));
  }

  public getGodownToCounterExceptionReport(objReport: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodownToCounterException/GetGodownToCounterException', objReport,{responseType: 'blob' as 'json'})
      .pipe(catchError(this.handleError));
  }
  public getCounterToGodownExceptionReport(objReport: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CounterToGodownException/GetCounterToGodownException', 
    objReport, { responseType: 'blob' as 'json' })
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
}
