import { TestBed } from '@angular/core/testing';

import { TransfersExceptionReportsService } from './transfers-exception-reports.service';

describe('TransfersExceptionReportsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TransfersExceptionReportsService = TestBed.get(TransfersExceptionReportsService);
    expect(service).toBeTruthy();
  });
});
