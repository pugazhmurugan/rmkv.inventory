import { TestBed } from '@angular/core/testing';

import { GoodsTransferBalesService } from './goods-transfer-bales.service';

describe('GoodsTransferBalesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GoodsTransferBalesService = TestBed.get(GoodsTransferBalesService);
    expect(service).toBeTruthy();
  });
});
