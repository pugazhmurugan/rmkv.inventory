import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class GoodsTransferBalesService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public loadGoodsTransferBales(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransferBales/LoadGoodsTransferBalesDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public addGoodsTransferBales(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransferBales/AddGoodsTransferBalesDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public getGoodsBalesSectionGRN(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransferBales/GetBalesSectionGRNList',objData)
      .pipe(catchError(this.handleError));
  }
  
  private handleError(error: Response) {
    return throwError(error);
  }}
