import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class GoodsTransferReceiptsService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public loadGoodsTransferReceipts(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransferReceipts/LoadGoodsTransferReceiptsDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public loadGoodsReceiptsTransferNo(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransferReceipts/LoadGoodsReceiptsTransferNo',objData)
      .pipe(catchError(this.handleError));
  }

  public addGoodsTransferReceiptsDetails(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransferReceipts/AddGoodsTransferReceiptsDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public fetchGoodsTransferReceiptsDetails(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransferReceipts/FetchGoodsTransferReceiptsDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public checkGoodsReceiptsTransferNoDetails(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransferReceipts/CheckGoodsTrandferReceiptsTransferNoDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public loadGoodsReceiptsProductDetails(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransferReceipts/LoadGoodsReceiptsProductDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public getAllWarehouseListDetails(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GoodsTransferReceipts/GetAllWarehouseListDetails',objData)
      .pipe(catchError(this.handleError));
  }
  
  private handleError(error: Response) {
    return throwError(error);
  }
}
