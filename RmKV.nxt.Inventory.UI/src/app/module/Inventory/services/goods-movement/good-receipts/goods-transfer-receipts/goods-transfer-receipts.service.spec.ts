import { TestBed } from '@angular/core/testing';

import { GoodsTransferReceiptsService } from './goods-transfer-receipts.service';

describe('GoodsTransferReceiptsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GoodsTransferReceiptsService = TestBed.get(GoodsTransferReceiptsService);
    expect(service).toBeTruthy();
  });
});
