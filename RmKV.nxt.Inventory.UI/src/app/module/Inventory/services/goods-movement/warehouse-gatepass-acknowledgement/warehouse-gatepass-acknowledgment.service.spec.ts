import { TestBed } from '@angular/core/testing';

import { WarehouseGatepassAcknowledgmentService } from './warehouse-gatepass-acknowledgment.service';

describe('WarehouseGatepassAcknowledgmentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WarehouseGatepassAcknowledgmentService = TestBed.get(WarehouseGatepassAcknowledgmentService);
    expect(service).toBeTruthy();
  });
});
