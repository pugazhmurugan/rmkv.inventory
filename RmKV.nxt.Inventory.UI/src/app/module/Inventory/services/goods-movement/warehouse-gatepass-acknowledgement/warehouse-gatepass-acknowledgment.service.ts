import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class WarehouseGatepassAcknowledgmentService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getGatePass(objInwardGatePass: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepassAcknowledgment/GetGatePass', objInwardGatePass)
      .pipe(catchError(this.handleError));
  }
  public getInwardGatePass(objInwardGatePass: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepassAcknowledgment/LoadInwardGatePass', objInwardGatePass)
      .pipe(catchError(this.handleError));
  }

  public getInwardTransferDetail(objInwardGatePass: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepassAcknowledgment/GetInwardTransferDetail', objInwardGatePass)
      .pipe(catchError(this.handleError));
  }

  public getTransferTypeDteails(objGet: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepassAcknowledgment/GetTransferTypeDetails', objGet)
      .pipe(catchError(this.handleError));
  }
  public getInwardGatePassDteails(objGet: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepassAcknowledgment/GetInwardGatePassListDetails', objGet)
      .pipe(catchError(this.handleError));
  }
  public getInwardGatePassNoList(objGet: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepassAcknowledgment/GetInwardGatePassNoList', objGet)
      .pipe(catchError(this.handleError));
  }

    public GetStoreLocation(objInwardGatePass: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepassAcknowledgment/GetStoreLocation', objInwardGatePass)
      .pipe(catchError(this.handleError));
  }

  public getSameWareHouse(objInwardGatePass: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepassAcknowledgment/GetSameWarehouse', objInwardGatePass)
      .pipe(catchError(this.handleError));
  }

  public getGRNNoListDetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepassAcknowledgment/GetWarehouseGRNNoListAck', obj)
      .pipe(catchError(this.handleError));
  }

  public getWarehouseAckGatePassDetails(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepassAcknowledgment/GetWarehouseAckGatePassDetails', obj)
      .pipe(catchError(this.handleError));
  }

  public addWarehouseAckGatePass(objInwardGatePass: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepassAcknowledgment/SaveWarehouseGatePassAck', objInwardGatePass)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }

  // constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }


  // getWarehouseBasedCompanySection(obj: any) {
  //   return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetCompanySectionDetails", obj)
  //     .pipe(catchError(this.handleError));
  // }

  // public getType(objDC: any) {
  //   return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepassAcknowledgment/GetGatePassType', objDC)
  //     .pipe(catchError(this.handleError));
  // }

  // private handleError(error: Response) {
  //   return throwError(error);
  // }
}
