import { TestBed } from '@angular/core/testing';

import { FloorGatepassService } from './floor-gatepass.service';

describe('FloorGatepassService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FloorGatepassService = TestBed.get(FloorGatepassService);
    expect(service).toBeTruthy();
  });
});
