import { TestBed } from '@angular/core/testing';

import { WarehouseGatepassService } from './warehouse-gatepass.service';

describe('WarehouseGatepassService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WarehouseGatepassService = TestBed.get(WarehouseGatepassService);
    expect(service).toBeTruthy();
  });
});
