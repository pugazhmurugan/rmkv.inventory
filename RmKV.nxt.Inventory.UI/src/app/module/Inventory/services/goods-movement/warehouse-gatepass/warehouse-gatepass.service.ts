import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class WarehouseGatepassService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }
  public getWarehouseGatePassType(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepass/GetWarehouseGatePassType', objProduct)
      .pipe(catchError(this.handleError));
  }
  public addWarehouseGatePass(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepass/AddWarehouseGatePass', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getWarehouseGatePass(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepass/GetWarehouseGatePass', objProduct)
      .pipe(catchError(this.handleError));
  }
  public cancelWarehouseGatePass(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepass/CancelWarehouseGatePass', objProduct)
      .pipe(catchError(this.handleError));
  }
  public fetchWarehouseGatePass(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepass/FetchWarehouseGatePass', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getTransferDetails(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepass/GetTransferDetails', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getGatepassTowarehouse(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepass/GetGatepassTowarehouse', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getGatepassNo(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepass/GatepassNo', objProduct)
      .pipe(catchError(this.handleError));
  }

  public getGatepassToLocation(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'WarehouseGatepass/GatepassToLocation', objProduct)
      .pipe(catchError(this.handleError));
  }
  private handleError(error: Response) {
    return throwError(error);
  }
}
