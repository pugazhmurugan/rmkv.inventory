import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReturnableGoodsTransferReportsService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }
 
  public getWorkOrderIssueListReport(objView: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PayrollReports/GetWorkOrderIssueListReport', objView
      , { responseType: 'blob' as 'json' }).pipe(catchError(this.handleError));
  }

  public getWorkOrderReceiptListReport(objView: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PayrollReports/GetWorkOrderReceiptListReport', objView
      , { responseType: 'blob' as 'json' }).pipe(catchError(this.handleError));
  }
  
  public getMismatchStoneQuantityReport(objView : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'SingleProduct/GetMismatchStoneQuantityReport', objView
    , { responseType: 'blob' as 'json' }).pipe(catchError(this.handleError));
  }

  public getPolishPendingIssueReport(objView : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ReturnableTransferReports/GetPolishPendingIssueReport', objView
    , { responseType: 'blob' as 'json' }).pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
  
}
