import { TestBed } from '@angular/core/testing';

import { ReturnableGoodsTransferReportsService } from './returnable-goods-transfer-reports.service';

describe('ReturnableGoodsTransferReportsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReturnableGoodsTransferReportsService = TestBed.get(ReturnableGoodsTransferReportsService);
    expect(service).toBeTruthy();
  });
});
