import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class GodownToCounterService {
  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public addGodownToCounter(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodownToCounter/addGodownToCounter', objProduct)
      .pipe(catchError(this.handleError));
  }
  public approveGodownToCounter(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodownToCounter/ApproveGodownToCounter', objProduct)
      .pipe(catchError(this.handleError));
  }
  public cancelGodownToCounter(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodownToCounter/CancelGodownToCounter', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getToLocation(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodownToCounter/GetToLocation', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getGodownToCounter(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodownToCounter/GetGodownToCounter', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getTransferNo(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodownToCounter/GetTransferNo', objProduct)
      .pipe(catchError(this.handleError));
  }
  public fetchGodownToCounter(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodownToCounter/FetchGodownToCounter', objProduct)
      .pipe(catchError(this.handleError));
  }
  private handleError(error: Response) {
    return throwError(error);
  }

  ///report

  
  public getGodownToCounterReport(objLoad: any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodownToCounter/GetGodownToCounterReport', objLoad, {responseType: 'blob' as 'json'})
      .pipe(catchError(this.handleError));
  }

  public checkGodownToCounterReport(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodownToCounter/CheckGodownToCounterReportDetails', objProduct)
      .pipe(catchError(this.handleError));
  }

}