import { TestBed } from '@angular/core/testing';

import { CounterToGodownService } from './counter-to-godown.service';

describe('CounterToGodownService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CounterToGodownService = TestBed.get(CounterToGodownService);
    expect(service).toBeTruthy();
  });
});
