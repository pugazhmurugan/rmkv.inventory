import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class CounterToGodownService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public addCounterToGodown(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CounterToGodown/AddCounterToGodown', objProduct)
      .pipe(catchError(this.handleError));
  }
  public approveCounterToGodown(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CounterToGodown/ApproveCounterToGodown', objProduct)
      .pipe(catchError(this.handleError));
  }
  public cancelCounterToGodown(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CounterToGodown/CancelCounterToGodown', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getToWarehouse(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CounterToGodown/GetToWarehouse', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getCounterToGodown(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CounterToGodown/GetCounterToGodown', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getTransferNo(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CounterToGodown/GetCounterTransferNo', objProduct)
      .pipe(catchError(this.handleError));
  }
  public fetchCounterToGodown(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CounterToGodown/FetchCounterToGodown', objProduct)
      .pipe(catchError(this.handleError));
  }
  private handleError(error: Response) {
    return throwError(error);
  }

  public getCounterToGodownReport(objView : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CounterToGodown/GetCounterToGodownReport', objView
    , { responseType: 'blob' as 'json' }).pipe(catchError(this.handleError));
  }

  public checkCounterToGodownReport(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CounterToGodown/CheckCounterToGodownReport', objProduct)
      .pipe(catchError(this.handleError));
  }
}