import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class CounterToGodownAcknowledgementService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public loadCtoGAcknowledgement(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CountertoGodownAcknowledgement/LoadCtoGAcknowledgementDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public loadCtoGTransferNoBasedDetails(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CountertoGodownAcknowledgement/LoadCtoGTransferNoBasedDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public addCtoGAcknowledgementDetails(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CountertoGodownAcknowledgement/AddCtoGAcknowledgementDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public fetchCtoGAcknowledgementDetails(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CountertoGodownAcknowledgement/FetchCtoGAcknowledgementDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public checkCtoGAckTransferNoDetails(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'CountertoGodownAcknowledgement/CheckCtoGAckTransferNoDetails',objData)
      .pipe(catchError(this.handleError));
  }
  
  private handleError(error: Response) {
    return throwError(error);
  }
}
