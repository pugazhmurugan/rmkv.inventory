import { TestBed } from '@angular/core/testing';

import { GodownToCounterAcknowledgementService } from './godown-to-counter-acknowledgement.service';

describe('GodownToCounterAcknowledgementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GodownToCounterAcknowledgementService = TestBed.get(GodownToCounterAcknowledgementService);
    expect(service).toBeTruthy();
  });
});
