import { TestBed } from '@angular/core/testing';

import { CounterToGodownAcknowledgementService } from './counter-to-godown-acknowledgement.service';

describe('CounterToGodownAcknowledgementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CounterToGodownAcknowledgementService = TestBed.get(CounterToGodownAcknowledgementService);
    expect(service).toBeTruthy();
  });
});
