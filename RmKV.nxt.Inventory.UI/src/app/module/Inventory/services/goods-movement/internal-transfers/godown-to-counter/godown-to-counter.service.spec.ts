import { TestBed } from '@angular/core/testing';

import { GodownToCounterService } from './godown-to-counter.service';

describe('GodownToCounterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GodownToCounterService = TestBed.get(GodownToCounterService);
    expect(service).toBeTruthy();
  });
});
