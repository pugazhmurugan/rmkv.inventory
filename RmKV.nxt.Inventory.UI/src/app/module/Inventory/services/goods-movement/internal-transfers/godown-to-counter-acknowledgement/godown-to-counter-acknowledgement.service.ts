import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class GodownToCounterAcknowledgementService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public loadGtoCACK(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodowntoCounterAcknowledgement/LoadGtoCAcknowledgementDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public loadTransferNoBasedDetails(objData : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodowntoCounterAcknowledgement/LoadTransferNoBasedDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public addGtoCAcknowledgementDetails(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodowntoCounterAcknowledgement/AddGtoCAcknowledgementDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public fetchGtoCAcknowledgementDetails(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodowntoCounterAcknowledgement/FetchGtoCAcknowledgementDetails',objData)
      .pipe(catchError(this.handleError));
  }

  public checkGtoCAckTransferNoDetails(objData : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'GodowntoCounterAcknowledgement/CheckGtoCAckTransferNoDetails',objData)
      .pipe(catchError(this.handleError));
  }
  
  private handleError(error: Response) {
    return throwError(error);
  }
}
