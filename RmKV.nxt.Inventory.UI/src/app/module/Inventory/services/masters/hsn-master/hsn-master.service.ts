import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HsnMasterService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public saveHSN(objHSN : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'HSNMaster/SaveHSNMaster',objHSN)
      .pipe(catchError(this.handleError));
  }

  public saveHSNTax(objHSN : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'HSNMaster/SaveHSNTax',objHSN)
      .pipe(catchError(this.handleError));
  }

  public fetchHSNMaster(objHSN : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'HSNMaster/FetchHSNMaster',objHSN)
      .pipe(catchError(this.handleError));
  }

  public fetchHSNTaxRate(objHSN : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'HSNMaster/FetchHSNTaxRate',objHSN)
      .pipe(catchError(this.handleError));
  }

  public fetchHSNTaxRateIndividual(objHSN : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'HSNMaster/FetchHSNTaxRateIndividual',objHSN)
      .pipe(catchError(this.handleError));
  }

  public RemoveHsnMaster(objHSN : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'HSNMaster/RemoveHSNMaster',objHSN)
      .pipe(catchError(this.handleError));
  }
 
  public RemoveHsnTaxRate(objHSN : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'HSNMaster/RemoveHSNTaxRate',objHSN)
      .pipe(catchError(this.handleError));
  }

  public getHSNMaster() {
    debugger;
    return this._httpClient.get(this._localStorage.getApiUrl() + 'HSNMaster/GetHSNMAsterMaster')
      .pipe(catchError(this.handleError));
  }
 
  private handleError(error: Response) {
    return throwError(error);
  }
}
