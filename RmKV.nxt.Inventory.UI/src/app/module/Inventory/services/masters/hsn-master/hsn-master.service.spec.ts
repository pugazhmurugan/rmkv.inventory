import { TestBed } from '@angular/core/testing';

import { HsnMasterService } from './hsn-master.service';

describe('HsnMasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HsnMasterService = TestBed.get(HsnMasterService);
    expect(service).toBeTruthy();
  });
});
