import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ProdMaterials } from 'src/app/module/Inventory/model/masters/production-materials.model';

@Injectable({
  providedIn: 'root'
})
export class ProductionMaterialsService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }


  public getProductionMaterialDetails() {
    return this._httpClient.get(this._localStorage.getApiUrl() + 'ProductionMaterials/GetProductionMaterials')
      .pipe(catchError(this.handleError));
  }

  public addProductionMaterialDetails(objMaterialDetails: any) {
    debugger
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionMaterials/AddProductionMaterials', objMaterialDetails)
      .pipe(catchError(this.handleError))
  }

  public fetchProductionMaterialDetails(objMaterialDetails: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionMaterials/FetchProductionMaterials', objMaterialDetails)
      .pipe(catchError(this.handleError));
  }


  public handleError(error: Response) {
    return throwError(error);
  }
}
