import { TestBed } from '@angular/core/testing';

import { ProductionMaterialsService } from './production-materials.service';

describe('ProductionMaterialsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductionMaterialsService = TestBed.get(ProductionMaterialsService);
    expect(service).toBeTruthy();
  });
});
