import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class ProductAttributesService {

  
  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  //Product Attribute

  getProductAttributes(objAttributes) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductAttributes/GetProductAttributes', objAttributes)
      .pipe(catchError(this.handleError));
  }

  addProductAttributes(objAttributes) {
    debugger
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductAttributes/AddProductAttributes', objAttributes)
      .pipe(catchError(this.handleError))
  }

  modifyProductAttributes(objAttributes) {
    debugger
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductAttributes/ModifyProductAttributes', objAttributes)
      .pipe(catchError(this.handleError))
  }

  //Product Attribute Values

  addProductAttributeValues(objAttributes) {
    debugger
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductAttributes/AddProductAttributeValues', objAttributes)
      .pipe(catchError(this.handleError))
  }
 
  getProductAttributeValues(objAttributes) {
    debugger
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductAttributes/GetProductAttributeValues', objAttributes)
      .pipe(catchError(this.handleError))
  }

  modifyProductAttributeValues(objAttributes) {
    debugger
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductAttributes/ModifyProductAttributeValues', objAttributes)
      .pipe(catchError(this.handleError))
  }
  
  handleError(error: Response) {
    return throwError(error);
  }
}
