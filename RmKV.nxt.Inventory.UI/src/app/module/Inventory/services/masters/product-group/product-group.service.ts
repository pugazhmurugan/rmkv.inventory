import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class ProductGroupService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getProductGroups(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductGroup/GetProductGroups', objProduct)
      .pipe(catchError(this.handleError));
  }

  public addProductGroups(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductGroup/AddProductGroups', objProduct)
      .pipe(catchError(this.handleError));
  }

  public fetchProductGroups(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductGroup/FetchProductGroups', objProduct)
      .pipe(catchError(this.handleError));
  }

  public deleteProductGroups(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductGroup/DeleteProductGroups', objProduct)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
}
