import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductCategoryService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getCategory(objCategory : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductCategory/LoadCategory',objCategory)
      .pipe(catchError(this.handleError));
  }

  public saveCategory(objCategory : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductCategory/SaveCategory',objCategory)
      .pipe(catchError(this.handleError));
  }


  public FetchCategory(objCategory : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductCategory/FetchCategory',objCategory)
      .pipe(catchError(this.handleError));
  }

  public RemoveCategory(objCategory : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductCategory/RemoveCategory',objCategory)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
}
