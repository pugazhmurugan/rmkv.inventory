import { TestBed } from '@angular/core/testing';

import { ProductionProductsService } from './production-products.service';

describe('ProductionProductsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductionProductsService = TestBed.get(ProductionProductsService);
    expect(service).toBeTruthy();
  });
});
