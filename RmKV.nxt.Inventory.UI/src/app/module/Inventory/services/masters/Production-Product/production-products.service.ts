import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class ProductionProductsService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public saveProductionProduct(objProductionProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionProduct/AddProdProduct', objProductionProduct)
      .pipe(catchError(this.handleError));
  }
  public getProductionProduct() {
    debugger;
    return this._httpClient.get(this._localStorage.getApiUrl() + 'ProductionProduct/GetProdProduct',)
      .pipe(catchError(this.handleError));
  }
  
  public getProductionProductLookup() {
    debugger;
    return this._httpClient.get(this._localStorage.getApiUrl() + 'ProductionProduct/GetProdProductLookup',)
      .pipe(catchError(this.handleError));
  }

  public FetchProductionProduct(objProductionProduct : any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionProduct/FetchProdProduct',objProductionProduct)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
}
