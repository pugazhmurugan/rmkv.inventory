import { TestBed } from '@angular/core/testing';

import { ProductionMaterialRequirementService } from './production-material-requirement.service';

describe('ProductionMaterialRequirementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductionMaterialRequirementService = TestBed.get(ProductionMaterialRequirementService);
    expect(service).toBeTruthy();
  });
});
