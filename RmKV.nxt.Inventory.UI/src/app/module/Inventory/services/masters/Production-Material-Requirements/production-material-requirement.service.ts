import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class ProductionMaterialRequirementService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public saveProductionMaterialRequirements(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionMaterialRequirements/SaveProdMaterialRequirements', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getProductionMaterialRequirements() {
    return this._httpClient.get(this._localStorage.getApiUrl() + 'ProductionMaterialRequirements/GetProdMaterialRequirements')
      .pipe(catchError(this.handleError));
  }
  public fetchProductionMaterialRequirements(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductionMaterialRequirements/FetchProdMaterialRequirements', objProduct)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
}
