import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class ProductMasterService {
  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getProductMasters(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductMaster/GetProductMasters', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getProductCounter(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductMaster/GetProductCounter', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getProductAttributes(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductMaster/GetProductAttributes', objProduct)
      .pipe(catchError(this.handleError));
  }
  public addProductMasters(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductMaster/AddProductMasters', objProduct)
      .pipe(catchError(this.handleError));
  }
  public fetchProductMasters(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductMaster/FetchProductMasters', objProduct)
      .pipe(catchError(this.handleError));
  }
  public deleteProductMasters(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductMaster/DeleteProductMasters', objProduct)
      .pipe(catchError(this.handleError));
  }
  public addGoods(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductMaster/addGoods', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getToLocation(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductMaster/GetToLocation', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getGoods(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductMaster/GetGoods', objProduct)
      .pipe(catchError(this.handleError));
  }
  public getTransferNo(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductMaster/GetTransferNo', objProduct)
      .pipe(catchError(this.handleError));
  }
  public fetchGoods(objProduct: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductMaster/FetchGood', objProduct)
      .pipe(catchError(this.handleError));
  }
  private handleError(error: Response) {
    return throwError(error);
  }
}
