import { TestBed } from '@angular/core/testing';

import { ProductMasterService } from './product-master.service';

describe('ProductMasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductMasterService = TestBed.get(ProductMasterService);
    expect(service).toBeTruthy();
  });
});
