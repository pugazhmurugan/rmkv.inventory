import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class ProductGroupAttributesService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getAttributesLookup(obj : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "ProductGroupAttributes/GetAtttributeLookupData",obj)
      .pipe(catchError(this.handleError)); 
  }

  public GetProductGroupLookupDetails(obj : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "ProductGroupAttributes/GetProductGroupLookupData",obj)
      .pipe(catchError(this.handleError));
  }

  public GetProductGroupAttributeDetails(obj : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "ProductGroupAttributes/GetProductGroupAttributeList",obj)
      .pipe(catchError(this.handleError));
  }

  public FetchProductGroupAttributeDetails(obj : any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "ProductGroupAttributes/FetchProductGroupAttributeData",obj)
      .pipe(catchError(this.handleError));
  }

  public addProductGroupAttributes(objAdd: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "ProductGroupAttributes/AddProductGroupDetails", objAdd)
      .pipe(catchError(this.handleError));
  }

  public removeProductGroupAttributes(objRemove: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "ProductGroupAttributes/RemoveProductGroupDetails", objRemove)
      .pipe(catchError(this.handleError));
  }

  public handleError(error: Response) {
    return throwError(error);
  }

}
