import { TestBed } from '@angular/core/testing';

import { ProductGroupAttributesService } from './product-group-attributes.service';

describe('ProductGroupAttributesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductGroupAttributesService = TestBed.get(ProductGroupAttributesService);
    expect(service).toBeTruthy();
  });
});
