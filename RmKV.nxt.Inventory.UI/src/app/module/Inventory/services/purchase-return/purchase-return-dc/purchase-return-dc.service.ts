import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class PurchaseReturnDcService {

  constructor(
    public _httpClient : HttpClient,
    public _localStorage : LocalStorage
  ) { }

  public getPurchaseReturnDCList(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnDC/LoadPurchaseReturnDCDetails', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }

  public generatePurchaseReturnDCNo(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnDC/GeneratePurchaseReturnDCNoList', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }

  public fetchPurchaseReturnDCDetails(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnDC/FetchPurchaseReturnDCList', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
}
