import { TestBed } from '@angular/core/testing';

import { PurchaseReturnDcService } from './purchase-return-dc.service';

describe('PurchaseReturnDcService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PurchaseReturnDcService = TestBed.get(PurchaseReturnDcService);
    expect(service).toBeTruthy();
  });
});
