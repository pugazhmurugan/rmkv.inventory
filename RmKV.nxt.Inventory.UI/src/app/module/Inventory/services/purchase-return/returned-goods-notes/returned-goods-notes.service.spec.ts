import { TestBed } from '@angular/core/testing';

import { ReturnedGoodsNotesService } from './returned-goods-notes.service';

describe('ReturnedGoodsNotesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReturnedGoodsNotesService = TestBed.get(ReturnedGoodsNotesService);
    expect(service).toBeTruthy();
  });
});
