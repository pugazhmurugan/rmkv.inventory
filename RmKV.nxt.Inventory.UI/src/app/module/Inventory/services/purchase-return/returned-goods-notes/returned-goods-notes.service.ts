import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class ReturnedGoodsNotesService {

  constructor(public _httpClient: HttpClient, public _localStorage: LocalStorage) { }

  public getRGNDCDetails(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ReturnedGoodsNotes/GetRGNWiseDCDetails', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }
  public addRGNDetails(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ReturnedGoodsNotes/AddRGNWiseDetails', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }
  public getRGNDetails(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ReturnedGoodsNotes/GetRGNWiseDetails', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }
  public getDCDetails(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ReturnedGoodsNotes/GetDCDetails', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }
  public getTransporter(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ReturnedGoodsNotes/GetTransporter', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }
  public fetchRGNDetails(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ReturnedGoodsNotes/FetchRGNWiseDetails', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }
  private handleError(error: Response) {
    return throwError(error);
  }
}
