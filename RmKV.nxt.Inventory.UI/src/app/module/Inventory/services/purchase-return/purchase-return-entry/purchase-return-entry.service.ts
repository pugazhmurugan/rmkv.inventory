import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class PurchaseReturnEntryService {

 
  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public AddPurchaseReturn(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnEntry/AddPurchaseReturn', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }

  public ApprovePurchaseReturn(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnEntry/ApprovePurchaseReturn', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }

  public CancelPurchaseReturn(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnEntry/CancelPurchaseReturn', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }

  public GetPurchaseReturn(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnEntry/GetPurchaseReturn', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }

  public FetchPurchaseReturn(PurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnEntry/FetchPurchaseReturn', PurchaseReturn)
      .pipe(catchError(this.handleError));
  }


  private handleError(error: Response) {
    return throwError(error);
  }
}
