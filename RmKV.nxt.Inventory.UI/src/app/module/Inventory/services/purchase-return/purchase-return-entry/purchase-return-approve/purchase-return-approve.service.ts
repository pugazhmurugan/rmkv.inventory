import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class PurchaseReturnApproveService {
  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getApprovedSupplier(objDC: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnApprove/GetApprovedSupplier', objDC)
      .pipe(catchError(this.handleError));
  }

  public getPurchaseRetunApprovedList(objDC: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnApprove/GetPurchaseRetunApprovedList', objDC)
      .pipe(catchError(this.handleError));
  } 
  
  public removPurchaseReturn(objDC: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnApprove/RemovPurchaseReturn', objDC)
      .pipe(catchError(this.handleError));
  }

  public getSupplierCityForApprove(objDC: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnApprove/GetSupplierCityForApprove', objDC)
      .pipe(catchError(this.handleError));
  }

  public getSupplierGroupNameForApprove(objDC: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnApprove/GetSupplierGroupNameForApprove', objDC)
      .pipe(catchError(this.handleError));
  }

  public getSupplierNameForApprove(objDC: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnApprove/GetSupplierNameForApprove', objDC)
      .pipe(catchError(this.handleError));
  }

  public geSupplierDetailsForApprove(objDC: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnApprove/GeSupplierDetailsForApprove', objDC)
      .pipe(catchError(this.handleError));
  }

  public approvePurchaseReturnList(objDC: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnApprove/ApprovePurchaseReturnList', objDC)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }

}