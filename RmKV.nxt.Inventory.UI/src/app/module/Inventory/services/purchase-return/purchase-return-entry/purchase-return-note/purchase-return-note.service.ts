import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class PurchaseReturnNoteService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public getPurchaseReturnNote(objPurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnNote/GetPurchaseReturnNote', objPurchaseReturn)
      .pipe(catchError(this.handleError));
  }

  public getPurchaseReturnByNos(objPurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnNote/GetPurchaseReturnByNo', objPurchaseReturn)
      .pipe(catchError(this.handleError));
  }

  public getPurchaseRetunByNoDetails(objPurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnNote/GetPurchaseReturnByNoDetails', objPurchaseReturn)
      .pipe(catchError(this.handleError));
  }

  public addPurchaseReturnNote(objPurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnNote/AddPurchaseReturnNote', objPurchaseReturn)
      .pipe(catchError(this.handleError));
  }

  public fetchPurchaseReturnNote(objPurchaseReturn: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'PurchaseReturnNote/FetchPurchaseReturnNote', objPurchaseReturn)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(error);
  }
}
