import { TestBed } from '@angular/core/testing';

import { PurchaseReturnNoteService } from './purchase-return-note.service';

describe('PurchaseReturnNoteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PurchaseReturnNoteService = TestBed.get(PurchaseReturnNoteService);
    expect(service).toBeTruthy();
  });
});
