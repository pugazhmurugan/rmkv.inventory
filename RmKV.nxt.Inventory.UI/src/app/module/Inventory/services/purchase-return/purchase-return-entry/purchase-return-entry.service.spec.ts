import { TestBed } from '@angular/core/testing';

import { PurchaseReturnEntryService } from './purchase-return-entry.service';

describe('PurchaseReturnEntryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PurchaseReturnEntryService = TestBed.get(PurchaseReturnEntryService);
    expect(service).toBeTruthy();
  });
});
