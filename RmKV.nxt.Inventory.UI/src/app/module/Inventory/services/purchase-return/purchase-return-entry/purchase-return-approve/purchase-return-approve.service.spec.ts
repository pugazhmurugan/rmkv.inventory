import { TestBed } from '@angular/core/testing';

import { PurchaseReturnApproveService } from './purchase-return-approve.service';

describe('PurchaseReturnApproveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PurchaseReturnApproveService = TestBed.get(PurchaseReturnApproveService);
    expect(service).toBeTruthy();
  });
});
