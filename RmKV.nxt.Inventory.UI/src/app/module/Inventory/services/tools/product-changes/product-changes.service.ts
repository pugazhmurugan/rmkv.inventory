import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class ProductChangesService {

  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) { }

  public saveProductCode(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductChange/Addproductchange', objInvoice)
      .pipe(catchError(this.handleError));
  }

  public saveCounterCode(objInvoice: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductChange/Addcounterchange', objInvoice)
      .pipe(catchError(this.handleError));
  }

  public saveStyleCode(objCheck: any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductChange/Addstylecodechange', objCheck)
      .pipe(catchError(this.handleError));
  }
  ///Report
  public getProductChangeHistoryReport(objLoad: any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductChange/GetProductChangeHistoryReport', objLoad, {responseType: 'blob' as 'json'})
      .pipe(catchError(this.handleError));
  }
  public getCounterChangeHistoryReport(objLoad: any) {
    debugger;
    return this._httpClient.post(this._localStorage.getApiUrl() + 'ProductChange/GetCounterChangeHistoryReport', objLoad, {responseType: 'blob' as 'json'})
      .pipe(catchError(this.handleError));
  }
  private handleError(error: Response) {
    return throwError(error);
  }
}
