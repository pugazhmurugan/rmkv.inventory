import { TestBed } from '@angular/core/testing';

import { ProductChangesService } from './product-changes.service';

describe('ProductChangesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductChangesService = TestBed.get(ProductChangesService);
    expect(service).toBeTruthy();
  });
});
