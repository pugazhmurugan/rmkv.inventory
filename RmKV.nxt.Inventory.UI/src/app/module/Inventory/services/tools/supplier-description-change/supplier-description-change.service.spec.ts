import { TestBed } from '@angular/core/testing';

import { SupplierDescriptionChangeService } from './supplier-description-change.service';

describe('SupplierDescriptionChangeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SupplierDescriptionChangeService = TestBed.get(SupplierDescriptionChangeService);
    expect(service).toBeTruthy();
  });
});
