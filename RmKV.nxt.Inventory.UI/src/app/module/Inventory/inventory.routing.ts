import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { HeaderComponent } from 'src/app/common/pages/header/header.component';
import { AuthendicationGuard, basicAuthendication } from 'src/app/common/services/authendication/guard/authendication-guard.guard';
import { MenusResolveGuard, UserProfileResolveGuard } from 'src/app/common/pages/resolve-guards/menus-resolve-guard/menus-resolve-guard';
import { ProductAttributesComponent } from './pages/masters/product-attributes/product-attributes.component';
import { ProductGroupsComponent } from './pages/masters/product-groups/product-groups.component';
import { ProductMasterComponent } from './pages/masters/product-master/product-master.component';
import { ProductCategoriesComponent } from './pages/masters/product-categories/product-categories.component';
import { HsnMasterComponent } from './pages/masters/hsn-master/hsn-master.component';
import { ProductAttributeValuesComponent } from './pages/masters/product-attribute-values/product-attribute-values.component';
import { ProductGroupAttributesComponent } from './pages/masters/product-group-attributes/product-group-attributes.component';
import { HsnTaxRatesComponent } from './pages/masters/hsn-tax-rates/hsn-tax-rates.component';
import { PurchaseOrderComponent } from './pages/purchase-orders/purchase-order/purchase-order.component';
import { GodownToCounterComponent } from './pages/goods-movement/internal-transfers/godown-to-counter/godown-to-counter.component';
import { CounterToGodownComponent } from './pages/goods-movement/internal-transfers/counter-to-godown/counter-to-godown.component';
import { CounterToGodownAcknowledgementComponent } from './pages/goods-movement/internal-transfers/counter-to-godown-acknowledgement/counter-to-godown-acknowledgement.component';
import { GodownToCounterAcknowledgementComponent } from './pages/goods-movement/internal-transfers/godown-to-counter-acknowledgement/godown-to-counter-acknowledgement.component';
import { GoodsTransferCityComponent } from './pages/goods-movement/goods-transfers/goods-transfer-city/goods-transfer-city.component';
import { GoodsTransferStateComponent } from './pages/goods-movement/goods-transfers/goods-transfer-state/goods-transfer-state.component';
import { GoodsTransferOtherStateComponent } from './pages/goods-movement/goods-transfers/goods-transfer-other-state/goods-transfer-other-state.component';
import { GoodsTransferStoreToStoreComponent } from './pages/goods-movement/goods-transfers/goods-transfer-store-to-store/goods-transfer-store-to-store.component';
import { GoodsTransferReturnableComponent } from './pages/goods-movement/goods-transfers/goods-transfer-returnable/goods-transfer-returnable.component';
import { PoAnalysisComponent } from './pages/purchase-orders/po-analysis/po-analysis.component';
import { DebitCreditNoteGstr1NonFilerComponent } from './pages/purchase-return/debit-credit-note/debit-credit-note-gstr1-non-filer/debit-credit-note-gstr1-non-filer.component';
import { DebitCreditNoteComponent } from './pages/purchase-return/debit-credit-note/debit-credit-note/debit-credit-note.component';
import { SupplierwiseCreditNoteComponent } from './pages/purchase-return/debit-credit-note/supplierwise-credit-note/supplierwise-credit-note.component';
import { PurchaseReturnApproveEditComponent } from './pages/purchase-return/purchase-return-approve-edit/purchase-return-approve-edit.component';
import { PurchaseReturnApproveComponent } from './pages/purchase-return/purchase-return-approve/purchase-return-approve.component';
import { PurchaseReturnDcComponent } from './pages/purchase-return/purchase-return-dc/purchase-return-dc.component';
import { PurchaseReturnEntryComponent } from './pages/purchase-return/purchase-return-entry/purchase-return-entry.component';
import { DaywiseTradingChecklistComponent } from './pages/purchase-return/purchase-return-reports/daywise-trading-checklist/daywise-trading-checklist.component';
import { PackingSlipComponent } from './pages/purchase-return/purchase-return-reports/packing-slip/packing-slip.component';
import { PurchaseReturnDcPrintComponent } from './pages/purchase-return/purchase-return-reports/purchase-return-dc-print/purchase-return-dc-print.component';
import { PurchaseReturnListComponent } from './pages/purchase-return/purchase-return-reports/purchase-return-list/purchase-return-list.component';
import { SupplierwisePurchaseReturnChecklistComponent } from './pages/purchase-return/purchase-return-reports/supplierwise-purchase-return-checklist/supplierwise-purchase-return-checklist.component';
import { TradingDrCrReportsComponent } from './pages/purchase-return/purchase-return-reports/trading-dr-cr-reports/trading-dr-cr-reports.component';
import { BarcodePrintingDoubleRateComponent } from './pages/purchases/barcode-printing/barcode-printing-double-rate/barcode-printing-double-rate.component';
import { BarcodePrintingSmallComponent } from './pages/purchases/barcode-printing/barcode-printing-small/barcode-printing-small.component';
import { BarcodePrintingStandardComponent } from './pages/purchases/barcode-printing/barcode-printing-standard/barcode-printing-standard.component';
import { InvoiceDetailsComponent } from './pages/purchases/invoice-details/invoice-details.component';
import { InvoiceGstStatusUpdateComponent } from './pages/purchases/invoice-gst-status-update/invoice-gst-status-update.component';
import { InvoiceInBalesComponent } from './pages/purchases/invoice-in-bales/invoice-in-bales.component';
import { InvoiceRegisterComponent } from './pages/purchases/invoice-register/invoice-register.component';
import { InvoicesComponent } from './pages/purchases/invoices/invoices.component';
import { GoodsTransferInBalesComponent } from './pages/goods-movement/goods-receipt/goods-transfer-in-bales/goods-transfer-in-bales.component';
import { GoodsTransferReceiptsReturnableComponent } from './pages/goods-movement/goods-receipt/goods-transfer-receipts-returnable/goods-transfer-receipts-returnable.component';
import { GoodsTransferReceiptsComponent } from './pages/goods-movement/goods-receipt/goods-transfer-receipts/goods-transfer-receipts.component';
import { PolishOrderIssueComponent } from './pages/goods-movement/returnable-goods-transfer/polish-order-issue/polish-order-issue.component';
import { PolishOrderReceiptComponent } from './pages/goods-movement/returnable-goods-transfer/polish-order-receipt/polish-order-receipt.component';
import { PolishOrderComponent } from './pages/goods-movement/returnable-goods-transfer/polish-order/polish-order.component';
import { ProductionIssueWithImageComponent } from './pages/goods-movement/returnable-goods-transfer/production-issue-with-image/production-issue-with-image.component';
import { ProductionReceiptWithImageComponent } from './pages/goods-movement/returnable-goods-transfer/production-receipt-with-image/production-receipt-with-image.component';
import { WorkOrderIssueComponent } from './pages/goods-movement/returnable-goods-transfer/work-order-issue/work-order-issue.component';
import { WorkOrderReceiptComponent } from './pages/goods-movement/returnable-goods-transfer/work-order-receipt/work-order-receipt.component';
import { WorkOrderComponent } from './pages/goods-movement/returnable-goods-transfer/work-order/work-order.component';
import { SalesReturnOtherLocationComponent } from './pages/goods-movement/srt/sales-return-other-location/sales-return-other-location.component';
import { SrtReportComponent } from './pages/goods-movement/srt/srt-report/srt-report.component';
import { CounterGodownTransferComponent } from './pages/goods-movement/transfer-reports/counter-godown-transfer/counter-godown-transfer.component';
import { GodownCounterTransferComponent } from './pages/goods-movement/transfer-reports/godown-counter-transfer/godown-counter-transfer.component';
import { GoodsTransferReportsComponent } from './pages/goods-movement/transfer-reports/goods-transfer-reports/goods-transfer-reports.component';
import { CustomersWorkOrderListComponent } from './pages/goods-movement/returnable-transfer-reports/customers-work-order-list/customers-work-order-list.component';
import { MismatchStoneQuantityComponent } from './pages/goods-movement/returnable-transfer-reports/mismatch-stone-quantity/mismatch-stone-quantity.component';
import { PendingProductionIssueListComponent } from './pages/goods-movement/returnable-transfer-reports/pending-production-issue-list/pending-production-issue-list.component';
import { PendingWorkOrderIssueComponent } from './pages/goods-movement/returnable-transfer-reports/pending-work-order-issue/pending-work-order-issue.component';
import { PendingWorkOrderListComponent } from './pages/goods-movement/returnable-transfer-reports/pending-work-order-list/pending-work-order-list.component';
import { PolishPendingIssueReportComponent } from './pages/goods-movement/returnable-transfer-reports/polish-pending-issue-report/polish-pending-issue-report.component';
import { ProductionReceiptListComponent } from './pages/goods-movement/returnable-transfer-reports/production-receipt-list/production-receipt-list.component';
import { WorkOrderIssueListComponent } from './pages/goods-movement/returnable-transfer-reports/work-order-issue-list/work-order-issue-list.component';
import { WorkOrderReceiptListComponent } from './pages/goods-movement/returnable-transfer-reports/work-order-receipt-list/work-order-receipt-list.component';
import { GoodsTransferReturnableReportComponent } from './pages/goods-movement/transfer-reports/goods-transfer-returnable-report/goods-transfer-returnable-report.component';
import { PurchaseReturnNoteComponent } from './pages/purchase-return/purchase-return-note/purchase-return-note.component';
import { PurchaseReturnNoteReportsComponent } from './pages/purchase-return/purchase-return-reports/purchase-return-note-reports/purchase-return-note-reports.component';
import { WarehouseGatepassGenerationComponent } from './pages/goods-movement/warehouse-gatepass-generation/warehouse-gatepass-generation.component';
import { FloorGatepassGenerationComponent } from './pages/goods-movement/floor-gatepass-generation/floor-gatepass-generation.component';
import { BynoTrackingLocalGoodsComponent } from './pages/tools/byno-generation/byno-tracking-local-goods/byno-tracking-local-goods.component';
import { BynoTrackingTransferedGoodsComponent } from './pages/tools/byno-generation/byno-tracking-transfered-goods/byno-tracking-transfered-goods.component';
import { GenerateNilBynoComponent } from './pages/tools/byno-generation/generate-nil-byno/generate-nil-byno.component';
import { GoodsTransferedProductsComponent } from './pages/tools/counter-change/goods-transfered-products/goods-transfered-products.component';
import { MultipleProductsComponent } from './pages/tools/counter-change/multiple-products/multiple-products.component';
import { SingleProductsComponent } from './pages/tools/counter-change/single-products/single-products.component';
import { ImageShootComponent } from './pages/tools/image/image-shoot/image-shoot.component';
import { ImageViewComponent } from './pages/tools/image/image-view/image-view.component';
import { MergeGoodsDetailsComponent } from './pages/tools/merge-goods/merge-goods-details/merge-goods-details.component';
import { MergeGoodsEntryComponent } from './pages/tools/merge-goods/merge-goods-entry/merge-goods-entry.component';
import { ProductChangeBynoSerialComponent } from './pages/tools/product-change/product-change-byno-serial/product-change-byno-serial.component';
import { ProductChangeProductCodeComponent } from './pages/tools/product-change/product-change-product-code/product-change-product-code.component';
import { ProductChangeSingleComponent } from './pages/tools/product-change/product-change-single/product-change-single.component';
import { SplitGoodsDetailsComponent } from './pages/tools/split-goods/split-goods-details/split-goods-details.component';
import { SplitGoodsEntryComponent } from './pages/tools/split-goods/split-goods-entry/split-goods-entry.component';
import { SupplierDescriptionChangeComponent } from './pages/tools/supplier-description-change/supplier-description-change.component';
import { SupplierStyleCodeChangeComponent } from './pages/tools/supplier-style-code-change/supplier-style-code-change.component';
import { ByAmountComponent } from './pages/pricing/double-rate/by-amount/by-amount.component';
import { CostPriceModificationComponent } from './pages/pricing/cost-price-modification/cost-price-modification.component';
import { ByMarginCodeComponent } from './pages/pricing/selling-price-change/by-margin-code/by-margin-code.component';
import { ByMarginMultipleBynoComponent } from './pages/pricing/selling-price-change/by-margin-multiple-byno/by-margin-multiple-byno.component';
import { ByMarginSonComponent } from './pages/pricing/selling-price-change/by-margin-son/by-margin-son.component';
import { ByMarginComponent } from './pages/pricing/selling-price-change/by-margin/by-margin.component';
import { ByPercentageComponent } from './pages/pricing/selling-price-change/by-percentage/by-percentage.component';
import { DoubleRateToSingleRateComponent } from './pages/pricing/selling-price-change/double-rate-to-single-rate/double-rate-to-single-rate.component';
import { SingleProductWithPrintComponent } from './pages/pricing/selling-price-change/single-product-with-print/single-product-with-print.component';
import { SingleProductComponent } from './pages/pricing/selling-price-change/single-product/single-product.component';
import { TransferdGoodsComponent } from './pages/pricing/selling-price-change/transferd-goods/transferd-goods.component';
import { ByPercentageDoubleRateComponent } from './pages/pricing/double-rate/by-percentage-double-rate/by-percentage-double-rate.component';
import { ByPercentageDoubleRateSonComponent } from './pages/pricing/double-rate/by-percentage-double-rate-son/by-percentage-double-rate-son.component';
import { ByMarginDoubleRateComponent } from './pages/pricing/double-rate/by-margin-double-rate/by-margin-double-rate.component';
import { BynoWiseDiscountComponent } from './pages/pricing/double-rate/byno-wise-discount/byno-wise-discount.component';
import { CoreMasterComponent } from './pages/ean/core-master/core-master.component';
import { CoreRatioMasterComponent } from './pages/ean/core-ratio-master/core-ratio-master.component';
import { EanCodeImportComponent } from './pages/ean/ean-code-import/ean-code-import.component';
import { EanCodeMasterComponent } from './pages/ean/ean-code-master/ean-code-master.component';
import { EanInvoiceDetailsComponent } from './pages/ean/ean-invoice-details/ean-invoice-details.component';
import { EanInvoiceComponent } from './pages/ean/ean-invoice/ean-invoice.component';
import { GeneratePoFromCoreComponent } from './pages/ean/generate-po-from-core/generate-po-from-core.component';
import { RatioVsStockVsRefilComponent } from './pages/ean/ratio-vs-stock-vs-refil/ratio-vs-stock-vs-refil.component';
import { CorewiseSalesReportComponent } from './pages/ean/utilities/corewise-sales-report/corewise-sales-report.component';
import { CorewiseStockReportComponent } from './pages/ean/utilities/corewise-stock-report/corewise-stock-report.component';
import { GetCoreInfoComponent } from './pages/ean/utilities/get-core-info/get-core-info.component';
import { RatioVsStockVsRefillSummaryReportComponent } from './pages/ean/utilities/ratio-vs-stock-vs-refill-summary-report/ratio-vs-stock-vs-refill-summary-report.component';
import { ScannedVsRatioVsStockComponent } from './pages/ean/utilities/scanned-vs-ratio-vs-stock/scanned-vs-ratio-vs-stock.component';
import { StyleCodeScaningComponent } from './pages/ean/utilities/style-code-scaning/style-code-scaning.component';
import { BynoHistoryComponent } from './pages/analysis/byno-history/byno-history.component';
import { GoodsReceiptAnalysisComponent } from './pages/analysis/goods-receipt-analysis/goods-receipt-analysis.component';
import { GoodsTransferAnalysisComponent } from './pages/analysis/goods-transfer-analysis/goods-transfer-analysis.component';
import { InvoiceAnalysisComponent } from './pages/analysis/invoice-analysis/invoice-analysis.component';
import { MovementCounterWiseComponent } from './pages/analysis/movement/movement-counter-wise/movement-counter-wise.component';
import { MovementProductWiseComponent } from './pages/analysis/movement/movement-product-wise/movement-product-wise.component';
import { BynowiseStockReportComponent } from './pages/analysis/stock/bynowise-stock-report/bynowise-stock-report.component';
import { CurrentStockComponent } from './pages/analysis/stock/current-stock/current-stock.component';
import { NegativeStockComponent } from './pages/analysis/stock/negative-stock/negative-stock.component';
import { StockAnalysisQtyComponent } from './pages/analysis/stock/stock-analysis-qty/stock-analysis-qty.component';
import { StockAnalysisComponent } from './pages/analysis/stock/stock-analysis/stock-analysis.component';
import { SupplierwiseStockAnalysisComponent } from './pages/analysis/stock/supplierwise-stock-analysis/supplierwise-stock-analysis.component';
import { TradingAnalysisComponent } from './pages/analysis/trading-analysis/trading-analysis.component';
import { UnpaidInvoiceAnalysisComponent } from './pages/analysis/unpaid-invoice-analysis/unpaid-invoice-analysis.component';
import { GSTTaxComponent } from './pages/pricing/gst-price-change/gsttax/gsttax.component';
import { GstTaxSmallComponent } from './pages/pricing/gst-price-change/gst-tax-small/gst-tax-small.component';
import { GstTaxMaterialComponent } from './pages/pricing/gst-price-change/gst-tax-material/gst-tax-material.component';
import { MrpChangeComponent } from './pages/pricing/mrp-change/mrp-change.component';
import { CounterStockAckComponent } from './pages/stock-audit/annual-stock-auditing/counter-stock-ack/counter-stock-ack.component';
import { CounterStockMtrsComponent } from './pages/stock-audit/annual-stock-auditing/counter-stock-mtrs/counter-stock-mtrs.component';
import { CounterStockScaningComponent } from './pages/stock-audit/annual-stock-auditing/counter-stock-scaning/counter-stock-scaning.component';
import { GodownStockAckComponent } from './pages/stock-audit/annual-stock-auditing/godown-stock-ack/godown-stock-ack.component';
import { GodownStockMtrsComponent } from './pages/stock-audit/annual-stock-auditing/godown-stock-mtrs/godown-stock-mtrs.component';
import { GodownStockScaningComponent } from './pages/stock-audit/annual-stock-auditing/godown-stock-scaning/godown-stock-scaning.component';
import { PhysicalStockClearanceComponent } from './pages/stock-audit/physical-stock-clearance/physical-stock-clearance.component';
import { CounterExcessEntryComponent } from './pages/stock-audit/stock-adjusment-entry/counter-excess-entry/counter-excess-entry.component';
import { CounterShortageEntryComponent } from './pages/stock-audit/stock-adjusment-entry/counter-shortage-entry/counter-shortage-entry.component';
import { GodownExcessEntryComponent } from './pages/stock-audit/stock-adjusment-entry/godown-excess-entry/godown-excess-entry.component';
import { GodownShortageEntryComponent } from './pages/stock-audit/stock-adjusment-entry/godown-shortage-entry/godown-shortage-entry.component';
import { StockVariationCounterStockComponent } from './pages/stock-audit/stock-variation-report/stock-variation-counter-stock/stock-variation-counter-stock.component';
import { StockVariationGodownStockComponent } from './pages/stock-audit/stock-variation-report/stock-variation-godown-stock/stock-variation-godown-stock.component';
import { StockVerificationCounterEntryComponent } from './pages/stock-audit/stock-verification/stock-verification-counter-entry/stock-verification-counter-entry.component';
import { StockVerificationGodownEntryComponent } from './pages/stock-audit/stock-verification/stock-verification-godown-entry/stock-verification-godown-entry.component';
import { WarehouseGatepassAcknowledgementComponent } from './pages/goods-movement/warehouse-gatepass-acknowledgement/warehouse-gatepass-acknowledgement.component';
import { ReturnedGoodsNotesComponent } from './pages/purchase-return/returned-goods-notes/returned-goods-notes.component';
import { GodownToCounterExceptionComponent } from './pages/goods-movement/transfers-exception-reports/godown-to-counter-exception/godown-to-counter-exception.component';
import { CounterToGodownExceptionComponent } from './pages/goods-movement/transfers-exception-reports/counter-to-godown-exception/counter-to-godown-exception.component';
import { GoodsTransferExceptionComponent } from './pages/goods-movement/transfers-exception-reports/goods-transfer-exception/goods-transfer-exception.component';
import { StockVariationComponent } from "./pages/stock-audit/stock-variation-report/stock-variation/stock-variation.component";

import { DetailedPurchaseOrderComponent } from "./pages/purchase-orders/detailed-purchase-order/detailed-purchase-order.component";
import { InvoiceProductDetailViewComponent } from "./pages/purchases/invoice-product-detail-view/invoice-product-detail-view.component";
import { PriceChageHistoryComponent } from "./pages/pricing/price-chage-history/price-chage-history.component";
import { CounterChangeHistoryComponent } from "./pages/tools/counter-change/counter-change-history/counter-change-history.component";
import { ProductChangeHistoryComponent } from "./pages/tools/product-change/product-change-history/product-change-history.component";
import { InvoicesExcessShortageComponent } from "./pages/purchases/invoices-excess-shortage/invoices-excess-shortage.component";
import { ProductionIssueComponent } from "./pages/goods-movement/returnable-goods-transfer/production-issue/production-issue.component";
import { ProductionReceiptComponent } from "./pages/goods-movement/returnable-goods-transfer/production-receipt/production-receipt.component";
import { ProductionMaterialsComponent } from "./pages/masters/production-materials/production-materials/production-materials.component";
import { ProductionProductsComponent } from "./pages/masters/production-products/production-products.component";
import { ProductionMaterialRequirementsComponent } from "./pages/masters/production-material-requirements/production-material-requirements.component";
import { ProductionItemPricingComponent } from "./pages/goods-movement/returnable-goods-transfer/production-item-pricing/production-item-pricing.component";

const InventoryRoutes: Routes = [
  {
    path: "Inventory",
    component: HeaderComponent,
    resolve: {
      menus: MenusResolveGuard,
      userProfile: UserProfileResolveGuard,
    },
    canActivate: [basicAuthendication],
    children: [
      // {
      //   path: "Dashboard",
      //   component: DashboardComponent
      // },
      // { path: "", redirectTo: "Dashboard", pathMatch: "full" },
      // {
      //   path: "Mail",
      //   component: MailComponent
      // },     
      {
        path: "ProductGroups",
        component: ProductGroupsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductAttributes",
        component: ProductAttributesComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductMaster",
        component: ProductMasterComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductCategories",
        component: ProductCategoriesComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductionMaterials",
        component: ProductionMaterialsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductionProducts",
        component: ProductionProductsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "HSNMaster",
        component: HsnMasterComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductAttributeValues",
        component: ProductAttributeValuesComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductGroupAttributes",
        component: ProductGroupAttributesComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "HSNTaxRates",
        component: HsnTaxRatesComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PurchaseOrder",
        component: PurchaseOrderComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "POAnalysis",
        component: PoAnalysisComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "InvoiceRegister",
        component: InvoiceRegisterComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "InvoiceinBales",
        component: InvoiceInBalesComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "Invoices",
        component: InvoicesComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "InvoiceDetails",
        component: InvoiceDetailsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "BarcodePrintingStandard",
        component: BarcodePrintingStandardComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "BarcodePrintingSmall",
        component: BarcodePrintingSmallComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "BarcodePrintingDoubleRate",
        component: BarcodePrintingDoubleRateComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "InvoiceGSTStatusUpdate",
        component: InvoiceGstStatusUpdateComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PurchaseReturnEntry",
        component: PurchaseReturnEntryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PurchaseReturnApprove",
        component: PurchaseReturnApproveComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PurchaseReturnEdit",
        component: PurchaseReturnApproveEditComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PurchaseReturnVapasNote",
        component: PurchaseReturnNoteComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PurchaseReturnVapasDC",
        component: PurchaseReturnDcComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "SupplierwisePurchaseReturnChecklist",
        component: SupplierwisePurchaseReturnChecklistComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PurchaseReturnList",
        component: PurchaseReturnListComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PackingSlip",
        component: PackingSlipComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "DaywiseTradingChecklist",
        component: DaywiseTradingChecklistComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "TradingDrCrReports",
        component: TradingDrCrReportsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PurchaseReturnNote",
        component: PurchaseReturnNoteReportsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ReturnedGoodsNotes",
        component: ReturnedGoodsNotesComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PurchaseReturnVapasDCPrint",
        component: PurchaseReturnDcPrintComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "SupplierCreditNoteVapasReturn",
        component: SupplierwiseCreditNoteComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "DebitCreditNote",
        component: DebitCreditNoteComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "DebitCreditNoteGSTRNonFiler",
        component: DebitCreditNoteGstr1NonFilerComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GodowntoCounter",
        component: GodownToCounterComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CountertoGodown",
        component: CounterToGodownComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GodowntoCounterAcknowledgement",
        component: GodownToCounterAcknowledgementComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CountertoGodownAcknowledgement",
        component: CounterToGodownAcknowledgementComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsTransfer",
        component: GoodsTransferCityComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsTransferWithinState",
        component: GoodsTransferStateComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsTransferOtherState",
        component: GoodsTransferOtherStateComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsTransferStoretoStore",
        component: GoodsTransferStoreToStoreComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsTransferReturnable",
        component: GoodsTransferReturnableComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsTransferinBales",
        component: GoodsTransferInBalesComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsTransferReceipts",
        component: GoodsTransferReceiptsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsTransferReceiptsReturnable",
        component: GoodsTransferReceiptsReturnableComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductionIssue",
        component: ProductionIssueComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductionReceipt",
        component: ProductionReceiptComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "WorkOrder",
        component: WorkOrderComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "WorkOrderIssue",
        component: WorkOrderIssueComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "WorkOrderReceipt",
        component: WorkOrderReceiptComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PolishOrder",
        component: PolishOrderComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "StoneandPolishIssue",
        component: PolishOrderIssueComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "StoneandPolishReceipt",
        component: PolishOrderReceiptComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "SalesReturnOtherLocation",
        component: SalesReturnOtherLocationComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "SRTReport",
        component: SrtReportComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsTransferReports",
        component: GoodsTransferReportsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GodownCounterTransfer",
        component: GodownCounterTransferComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CounterGodownTransfer",
        component: CounterGodownTransferComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsTransferReturnableReports",
        component: GoodsTransferReturnableReportComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PendingProductionIssueListWithImage",
        component: PendingProductionIssueListComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductionReceiptListWithImage",
        component: ProductionReceiptListComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PolishPendingIssueReport",
        component: PolishPendingIssueReportComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "WorkOrderPending",
        component: PendingWorkOrderListComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PendingWorkOrderIssue",
        component: PendingWorkOrderIssueComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CustomersWorkOrderList",
        component: CustomersWorkOrderListComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "WorkOrderIssueList",
        component: WorkOrderIssueListComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "WorkOrderReceiptList",
        component: WorkOrderReceiptListComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "MismatchStoneQty",
        component: MismatchStoneQuantityComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "WHGatepassGeneration",
        component: WarehouseGatepassGenerationComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "SingleProducts",
        component: SingleProductsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "MultipleProducts",
        component: MultipleProductsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsTransferedProducts",
        component: GoodsTransferedProductsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductChangeSingelProducts",
        component: ProductChangeSingleComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductChangeProductCode",
        component: ProductChangeProductCodeComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductChangeBynoSerial",
        component: ProductChangeBynoSerialComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "SupplierDescriptionChange",
        component: SupplierDescriptionChangeComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "SupplierStyleCodeChange",
        component: SupplierStyleCodeChangeComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ImageShoot",
        component: ImageShootComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ImageView",
        component: ImageViewComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "SplitGoodsEntry",
        component: SplitGoodsEntryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "SplitGoodsDetails",
        component: SplitGoodsDetailsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "MergeGoodsEntry",
        component: MergeGoodsEntryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "MergeGoodsDetails",
        component: MergeGoodsDetailsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GenerateNilByno",
        component: GenerateNilBynoComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "BynoTrackingLocalGoods",
        component: BynoTrackingLocalGoodsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "BynoTrackingTransferedGoods",
        component: BynoTrackingTransferedGoodsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "FloorGatepassGeneration",
        component: FloorGatepassGenerationComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CostPriceModification",
        component: CostPriceModificationComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "SingleProduct",
        component: SingleProductComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "SingleProductWithPrint",
        component: SingleProductWithPrintComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "TransferedGoods",
        component: TransferdGoodsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ByPercentage",
        component: ByPercentageComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "DoubleRatetoSingleRate",
        component: DoubleRateToSingleRateComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ByMargin",
        component: ByMarginComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ByMarginCode",
        component: ByMarginCodeComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ByMarginSons",
        component: ByMarginSonComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "BymarginMultipleByno",
        component: ByMarginMultipleBynoComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ByAmount",
        component: ByAmountComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ByPercentageDoubleRate",
        component: ByPercentageDoubleRateComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ByPercentageSons",
        component: ByPercentageDoubleRateSonComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ByPercentageSons",
        component: ByMarginDoubleRateComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ByMarginDoubleRate",
        component: ByMarginDoubleRateComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CoreMaster",
        component: CoreMasterComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CoreRatioMaster",
        component: CoreRatioMasterComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "EanCodeImport",
        component: EanCodeImportComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "EanCodeMaster",
        component: EanCodeMasterComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "EANInvoice",
        component: EanInvoiceComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "EANInvoiceDetails",
        component: EanInvoiceDetailsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GeneratePOFromCore",
        component: GeneratePoFromCoreComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "RationVsStockVsRefil",
        component: RatioVsStockVsRefilComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CorewiseSalesReport",
        component: CorewiseSalesReportComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CorewiseStockReport",
        component: CorewiseStockReportComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GetCoreInfo",
        component: GetCoreInfoComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "RatioVsStockVsRefillSummaryReport",
        component: RatioVsStockVsRefillSummaryReportComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ScannedVsRatioVsStock",
        component: ScannedVsRatioVsStockComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "StyleCodeScaning",
        component: StyleCodeScaningComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "InvoiceAnalysis",
        component: InvoiceAnalysisComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "UnpaidInvoiceAnalysis",
        component: UnpaidInvoiceAnalysisComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "TradingAnalysis",
        component: TradingAnalysisComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "Counterwise",
        component: MovementCounterWiseComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "Productwise",
        component: MovementProductWiseComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsTransferAnalysis",
        component: GoodsTransferAnalysisComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GoodsReceiptAnalysis",
        component: GoodsReceiptAnalysisComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "BynoHistory",
        component: BynoHistoryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "StockAnalysis",
        component: StockAnalysisComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "StockAnalysisQty",
        component: StockAnalysisQtyComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "SupplierwiseStockAnalysis",
        component: SupplierwiseStockAnalysisComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "NegativeStock",
        component: NegativeStockComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CurrentStock",
        component: CurrentStockComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "BynowiseStockReport",
        component: BynowiseStockReportComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PhysicalStockClearance",
        component: PhysicalStockClearanceComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GodownEntry",
        component: StockVerificationGodownEntryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CounterEntry",
        component: StockVerificationCounterEntryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GodownStockScaning",
        component: GodownStockScaningComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GodownStockAcknowledgement",
        component: GodownStockAckComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CounterStockScaning",
        component: CounterStockScaningComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CounterStockAcknowledgment",
        component: CounterStockAckComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GodownStockMtrs",
        component: GodownStockMtrsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CounterStockMtrs",
        component: CounterStockMtrsComponent,
        canActivate: [AuthendicationGuard]
      },
      // {
      //   path: "GodownStock/:Section_Name",
      //   component: StockVariationGodownStockComponent,
      //   canActivate: [AuthendicationGuard]
      // },StockVariationComponent
      {
        path: "GodownStock",
        component: StockVariationGodownStockComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "StockVariation",
        component: StockVariationComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CounterStock",
        component: StockVariationCounterStockComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CounterExcessEntry",
        component: CounterExcessEntryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CounterShortageEntry",
        component: CounterShortageEntryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GodownExcessEntry",
        component: GodownExcessEntryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GodownShortageEntry",
        component: GodownShortageEntryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "BynowiseDiscount",
        component: BynoWiseDiscountComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GSTTax",
        component: GSTTaxComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GSTTaxSmall",
        component: GstTaxSmallComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GSTTaxMaterial",
        component: GstTaxMaterialComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "MRPChange",
        component: MrpChangeComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "WarehouseGatepassAck",
        component: WarehouseGatepassAcknowledgementComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "GodowntoCounterException",
        component: GodownToCounterExceptionComponent,
        canActivate: [AuthendicationGuard]
      },

      {
        path: "CounterGodownException",
        component: CounterToGodownExceptionComponent,
        canActivate: [AuthendicationGuard]
      },

      {
        path: "GoodsTransferException",
        component: GoodsTransferExceptionComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "DetailedPurchaseOrder",
        component: DetailedPurchaseOrderComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "InvoiceProductDetailView",
        component: InvoiceProductDetailViewComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "PriceChangeHistory",
        component: PriceChageHistoryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "CounterChangeHistory",
        component: CounterChangeHistoryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductionMaterialRequirements",
        component: ProductionMaterialRequirementsComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductChangeHistory",
        component: ProductChangeHistoryComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "InvoicesExcessShortage",
        component: InvoicesExcessShortageComponent,
        canActivate: [AuthendicationGuard]
      },
      {
        path: "ProductionItemPricing",
        component: ProductionItemPricingComponent,
        canActivate: [AuthendicationGuard]
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(InventoryRoutes)],
  exports: [RouterModule]
})

export class InventoryRoute { }