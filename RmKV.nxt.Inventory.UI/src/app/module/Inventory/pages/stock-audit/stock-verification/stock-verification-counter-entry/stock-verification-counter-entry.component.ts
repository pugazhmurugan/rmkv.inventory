import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-stock-verification-counter-entry',
  templateUrl: './stock-verification-counter-entry.component.html',
  styleUrls: ['./stock-verification-counter-entry.component.scss']
})
export class StockVerificationCounterEntryComponent implements OnInit {

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
