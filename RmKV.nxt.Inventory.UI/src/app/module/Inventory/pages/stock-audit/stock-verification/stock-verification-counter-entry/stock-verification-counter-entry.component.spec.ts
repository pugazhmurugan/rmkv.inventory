import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockVerificationCounterEntryComponent } from './stock-verification-counter-entry.component';

describe('StockVerificationCounterEntryComponent', () => {
  let component: StockVerificationCounterEntryComponent;
  let fixture: ComponentFixture<StockVerificationCounterEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockVerificationCounterEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockVerificationCounterEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
