import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockVerificationGodownEntryComponent } from './stock-verification-godown-entry.component';

describe('StockVerificationGodownEntryComponent', () => {
  let component: StockVerificationGodownEntryComponent;
  let fixture: ComponentFixture<StockVerificationGodownEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockVerificationGodownEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockVerificationGodownEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
