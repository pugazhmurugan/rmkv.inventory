import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialogRef, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { EditableGridComponent } from 'src/app/common/pages/editable-grid/editable-grid.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { IndexedDBCommonFN } from 'src/app/common/shared/IndexedDB/indexed_db_common';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-stock-verification-godown-entry',
  templateUrl: './stock-verification-godown-entry.component.html',
  styleUrls: ['./stock-verification-godown-entry.component.scss']
})
export class StockVerificationGodownEntryComponent implements OnInit {
 

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
