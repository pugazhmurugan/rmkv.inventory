import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-stock-variation-counter-stock',
  templateUrl: './stock-variation-counter-stock.component.html',
  styleUrls: ['./stock-variation-counter-stock.component.scss']
})
export class StockVariationCounterStockComponent implements OnInit {
  componentVisibility: boolean = true;
  displayedColumns = ["Serial_No", "Counter_Code", "Counter_Name", "Physical_Qty", "System_Qty", "Variation_Qty",];

  dataSource: any = new MatTableDataSource([]);

  objStock: any = {
    Section_Name: ''
  }

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

  VariationQty(): void {
    this.componentVisibility = !this.componentVisibility;
  }

  clear(): void {
    this.componentVisibility = !this.componentVisibility;
  }

  public redirectToNextComponent(): void {
    debugger;
    // this.objStock.Section_Name = 'Silks';
    this._router.navigate(['/Inventory/StockVariation']);
  }

}
