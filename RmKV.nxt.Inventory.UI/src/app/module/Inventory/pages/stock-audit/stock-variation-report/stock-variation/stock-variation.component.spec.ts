import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockVariationComponent } from './stock-variation.component';

describe('StockVariationComponent', () => {
  let component: StockVariationComponent;
  let fixture: ComponentFixture<StockVariationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockVariationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockVariationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
