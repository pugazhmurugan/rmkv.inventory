import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockVariationGodownStockComponent } from './stock-variation-godown-stock.component';

describe('StockVariationGodownStockComponent', () => {
  let component: StockVariationGodownStockComponent;
  let fixture: ComponentFixture<StockVariationGodownStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockVariationGodownStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockVariationGodownStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
