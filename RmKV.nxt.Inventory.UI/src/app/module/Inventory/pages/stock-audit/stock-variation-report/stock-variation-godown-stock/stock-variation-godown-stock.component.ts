import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-stock-variation-godown-stock',
  templateUrl: './stock-variation-godown-stock.component.html',
  styleUrls: ['./stock-variation-godown-stock.component.scss']
})
export class StockVariationGodownStockComponent implements OnInit {

  componentVisibility: boolean = true;
  displayedColumns = ["Serial_No", "Counter_Code", "Counter_Name", "Physical_Qty", "System_Qty", "Variation_Qty",];

  objStock: any = {
    Section_Name: ''
  }
  dataSource: any = new MatTableDataSource([]);

  constructor(
    public _router: Router,
    public _localStorage: LocalStorage,
    private _activatedRoute: ActivatedRoute
  ) {
    debugger;
    this.objStock.Section_Name = this._activatedRoute.snapshot.params;
  }

  ngOnInit() {
  }

  VariationQty(): void {
    this.componentVisibility = !this.componentVisibility;
  }

  clear(): void {
    this.componentVisibility = !this.componentVisibility;
  }


}
