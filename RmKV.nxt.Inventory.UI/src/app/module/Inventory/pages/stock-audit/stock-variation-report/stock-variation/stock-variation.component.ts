import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-stock-variation',
  templateUrl: './stock-variation.component.html',
  styleUrls: ['./stock-variation.component.scss']
})
export class StockVariationComponent implements OnInit {

  
  objStock: any = {
    Section_Name: ''
  }

  constructor(  public _router: Router,
    public _localStorage: LocalStorage,
    private _activatedRoute: ActivatedRoute) {
    this.objStock.Section_Name = this._activatedRoute.snapshot.params;
   }

  ngOnInit() {
  }

}
