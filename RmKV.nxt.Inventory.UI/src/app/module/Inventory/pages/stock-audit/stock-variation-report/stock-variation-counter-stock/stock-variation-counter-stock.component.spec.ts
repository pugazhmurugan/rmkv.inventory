import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockVariationCounterStockComponent } from './stock-variation-counter-stock.component';

describe('StockVariationCounterStockComponent', () => {
  let component: StockVariationCounterStockComponent;
  let fixture: ComponentFixture<StockVariationCounterStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockVariationCounterStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockVariationCounterStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
