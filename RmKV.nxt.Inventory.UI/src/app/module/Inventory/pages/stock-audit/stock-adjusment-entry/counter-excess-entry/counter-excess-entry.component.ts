import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialogRef, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { EditableGridComponent } from 'src/app/common/pages/editable-grid/editable-grid.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { IndexedDBCommonFN } from 'src/app/common/shared/IndexedDB/indexed_db_common';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-counter-excess-entry',
  templateUrl: './counter-excess-entry.component.html',
  styleUrls: ['./counter-excess-entry.component.scss'],
  providers: [DatePipe]
})
export class CounterExcessEntryComponent implements OnInit {
  componentVisibility: boolean = true;

  objLoad: any = {
    section: this._localStorage.getCompanySectionName(),
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  fromDate1: any = new Date();
  toDate1: any = new Date();

  
  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  showTable: boolean = false;

  objEditableGrid = {
    tabledata: [],
    obj: {
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0
      , uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: 0.00, hsncode: '', gst: 0, unique_byno_prod_serial: null
    },
    flags: { isProductCode: false, isProductName: true, isUOM: true, isSellingPrice: true,isProdQty:true,isProdPcs:true, isDisable: false },
    schema_name: 'physicalStock'
  }

  objGodown = [{
    byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
    uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
    isDelete: false, valid: false
  }]

  public dateValidation: DateValidation = new DateValidation();
  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Adjustment_ID", "Adjustment_Date", "Source_Type", "Adjustment_Type", "Transfer_Pcs",];
  public commonIndexed: IndexedDBCommonFN = new IndexedDBCommonFN(this.dbService, this._localStorage);

  @ViewChild(EditableGridComponent, null) byno_data;

  constructor(public _localStorage: LocalStorage, 
    public _router: Router, private _datePipe: DatePipe, public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _matDialog: MatDialog, private dbService: NgxIndexedDBService,public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _confirmationDialogComponent: ConfirmationDialogComponent,public _excelService: ExcelService,) {
    this.showTable = false;
    this.objLoad.FromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
  }

  ngOnInit() {
    this.showTable = true;
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.objLoad.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.objLoad.FromDate = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.objLoad.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoad.ToDate = date[0];
    this.toDate1 = date[1];
  }

  newClick():void {
    this.componentVisibility = !this.componentVisibility;
  }

  OnClear():void {
    this.componentVisibility = !this.componentVisibility;
  }

}
