import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterExcessEntryComponent } from './counter-excess-entry.component';

describe('CounterExcessEntryComponent', () => {
  let component: CounterExcessEntryComponent;
  let fixture: ComponentFixture<CounterExcessEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterExcessEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterExcessEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
