import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GodownShortageEntryComponent } from './godown-shortage-entry.component';

describe('GodownShortageEntryComponent', () => {
  let component: GodownShortageEntryComponent;
  let fixture: ComponentFixture<GodownShortageEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GodownShortageEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GodownShortageEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
