import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GodownExcessEntryComponent } from './godown-excess-entry.component';

describe('GodownExcessEntryComponent', () => {
  let component: GodownExcessEntryComponent;
  let fixture: ComponentFixture<GodownExcessEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GodownExcessEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GodownExcessEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
