import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterShortageEntryComponent } from './counter-shortage-entry.component';

describe('CounterShortageEntryComponent', () => {
  let component: CounterShortageEntryComponent;
  let fixture: ComponentFixture<CounterShortageEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterShortageEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterShortageEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
