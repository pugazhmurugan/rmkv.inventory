import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterStockScaningComponent } from './counter-stock-scaning.component';

describe('CounterStockScaningComponent', () => {
  let component: CounterStockScaningComponent;
  let fixture: ComponentFixture<CounterStockScaningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterStockScaningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterStockScaningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
