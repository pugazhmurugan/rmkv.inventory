import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterStockMtrsComponent } from './counter-stock-mtrs.component';

describe('CounterStockMtrsComponent', () => {
  let component: CounterStockMtrsComponent;
  let fixture: ComponentFixture<CounterStockMtrsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterStockMtrsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterStockMtrsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
