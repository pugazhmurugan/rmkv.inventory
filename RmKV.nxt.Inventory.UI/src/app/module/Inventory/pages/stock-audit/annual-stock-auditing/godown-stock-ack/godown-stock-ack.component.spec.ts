import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GodownStockAckComponent } from './godown-stock-ack.component';

describe('GodownStockAckComponent', () => {
  let component: GodownStockAckComponent;
  let fixture: ComponentFixture<GodownStockAckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GodownStockAckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GodownStockAckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
