import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialogRef, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { EditableGridComponent } from 'src/app/common/pages/editable-grid/editable-grid.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { IndexedDBCommonFN } from 'src/app/common/shared/IndexedDB/indexed_db_common';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-counter-stock-ack',
  templateUrl: './counter-stock-ack.component.html',
  styleUrls: ['./counter-stock-ack.component.scss'],
  providers: [DatePipe]
})
export class CounterStockAckComponent implements OnInit {
  componentVisibility: boolean = true;


  showTable: boolean = false;

  objEditableGrid = {
    tabledata: [],
    obj: {
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0
      , uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: 0.00, hsncode: '', gst: 0, unique_byno_prod_serial: null
    },
    flags: { isProductCode: false, isProductName: true, isUOM: true,isCounter:true, isSellingPrice: true,isProdQty:true,isProdPcs:true, isDisable: false },
    schema_name: 'physicalStock'
  }

  objGodown = [{
    byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
    uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
    isDelete: false, valid: false
  }]

  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Lot_No", "Lot_Date", "Counter_code", "Pcs", "qty", "Action"];
  public commonIndexed: IndexedDBCommonFN = new IndexedDBCommonFN(this.dbService, this._localStorage);

  @ViewChild(EditableGridComponent, null) byno_data;

  constructor(public _localStorage: LocalStorage, 
    public _router: Router, private _datePipe: DatePipe, public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _matDialog: MatDialog, private dbService: NgxIndexedDBService,public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _confirmationDialogComponent: ConfirmationDialogComponent,public _excelService: ExcelService,) {
    this.showTable = false;
  
  }

  ngOnInit() {
    this.showTable = true;
  }

  newClick():void {
    this.componentVisibility = !this.componentVisibility;
  }

  OnClear():void {
    this.componentVisibility = !this.componentVisibility;
  }

}
