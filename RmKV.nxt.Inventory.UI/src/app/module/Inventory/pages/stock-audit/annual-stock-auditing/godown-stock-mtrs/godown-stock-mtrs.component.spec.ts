import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GodownStockMtrsComponent } from './godown-stock-mtrs.component';

describe('GodownStockMtrsComponent', () => {
  let component: GodownStockMtrsComponent;
  let fixture: ComponentFixture<GodownStockMtrsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GodownStockMtrsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GodownStockMtrsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
