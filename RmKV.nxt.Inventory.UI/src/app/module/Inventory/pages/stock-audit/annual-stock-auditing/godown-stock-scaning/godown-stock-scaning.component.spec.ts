import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GodownStockScaningComponent } from './godown-stock-scaning.component';

describe('GodownStockScaningComponent', () => {
  let component: GodownStockScaningComponent;
  let fixture: ComponentFixture<GodownStockScaningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GodownStockScaningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GodownStockScaningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
