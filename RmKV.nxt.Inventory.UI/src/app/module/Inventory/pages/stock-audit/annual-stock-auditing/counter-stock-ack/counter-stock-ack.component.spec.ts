import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterStockAckComponent } from './counter-stock-ack.component';

describe('CounterStockAckComponent', () => {
  let component: CounterStockAckComponent;
  let fixture: ComponentFixture<CounterStockAckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterStockAckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterStockAckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
