import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-physical-stock-clearance',
  templateUrl: './physical-stock-clearance.component.html',
  styleUrls: ['./physical-stock-clearance.component.scss']
})
export class PhysicalStockClearanceComponent implements OnInit {

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
