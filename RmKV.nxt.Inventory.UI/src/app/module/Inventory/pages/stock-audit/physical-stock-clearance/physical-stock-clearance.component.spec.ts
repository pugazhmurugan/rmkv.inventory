import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalStockClearanceComponent } from './physical-stock-clearance.component';

describe('PhysicalStockClearanceComponent', () => {
  let component: PhysicalStockClearanceComponent;
  let fixture: ComponentFixture<PhysicalStockClearanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysicalStockClearanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalStockClearanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
