import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { BaarcodePrintingService } from 'src/app/module/Inventory/services/purchases/bar-code-printing/baarcode-printing.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-image-shoot',
  templateUrl: './image-shoot.component.html',
  styleUrls: ['./image-shoot.component.scss']
})
export class ImageShootComponent implements OnInit {

  data:any = {};
  thumbnail: any;

  constructor(  public _localStorage: LocalStorage,private sanitizer: DomSanitizer,
    public _router: Router,private _services : BaarcodePrintingService) { }

  ngOnInit() {
  }

  getImages():void{
    this._services.ImageBytes(this.data).subscribe(
      (res)=>{
        console.log(res);
        // var byteArray = res[0].photo;
        let objectURL = 'data:'+res[0].Type+';base64,' + res[0].photo;
        this.thumbnail = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        console.log(this.thumbnail);
        var byteArray = new Uint8Array(res[0].photo);
        let blob = this.dataURItoBlob(res[0].photo,res[0].Type);
        if (["image/gif",
        "image/jpeg",
        "image/pjpeg",
        "image/png",
        "image/x-png",
        "image/tiff",
        "image/bmp",
        "image/x-xbitmap",
        "image/x-jg",
        "image/x-emf",
        "image/x-wmf",
        "application/pdf"].indexOf(res[0].Type) !== -1) {
          var fileURL = URL.createObjectURL(blob);
          window.open(fileURL);
        }
        else{
          saveAs(blob, res[0].FileName+'.'+res[0].extension);
        }
      }
    )
  }

  dataURItoBlob(dataURI,type) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: type });    
    return blob;
 }

}
