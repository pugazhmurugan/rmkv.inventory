import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageShootComponent } from './image-shoot.component';

describe('ImageShootComponent', () => {
  let component: ImageShootComponent;
  let fixture: ComponentFixture<ImageShootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageShootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageShootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
