import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductChangesService } from 'src/app/module/Inventory/services/tools/product-changes/product-changes.service';

@Component({
  selector: 'app-counter-change-history',
  templateUrl: './counter-change-history.component.html',
  styleUrls: ['./counter-change-history.component.scss'],
  providers: [DatePipe]
})
export class CounterChangeHistoryComponent implements OnInit {
  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),

  }

  fromDate1: any = new Date();
  toDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  constructor(public _localStorage: LocalStorage, private _datePipe: DatePipe,
    public _router: Router,
    public _CounterChangesService: ProductChangesService,
    public _confirmationDialogComponent: ConfirmationDialogComponent) {
    this.load.FromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
  }

  ngOnInit() {
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }
  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }
  public CounterChangeHistoryReport(): void {
    let objLoad = [{
      from_date: this.load.FromDate,
      to_date: this.load.ToDate,
      company_section_id:this._localStorage.getCompanySectionId()
    }];
    let objData = {
      CounterChangeHistoryReport: JSON.stringify(objLoad)
    };
    this._CounterChangesService.getCounterChangeHistoryReport(objData).subscribe((result: any) => {
      debugger;
      if (result.size != 0) {
        let dataType = result.type;
        const file = new Blob([result], { type: dataType });
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      } else
        this._confirmationDialogComponent.openAlertDialog('No record found', 'Counter Change History Report');
    });
  }
}
