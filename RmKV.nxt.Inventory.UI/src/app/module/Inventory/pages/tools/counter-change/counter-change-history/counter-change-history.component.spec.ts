import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterChangeHistoryComponent } from './counter-change-history.component';

describe('CounterChangeHistoryComponent', () => {
  let component: CounterChangeHistoryComponent;
  let fixture: ComponentFixture<CounterChangeHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterChangeHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterChangeHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
