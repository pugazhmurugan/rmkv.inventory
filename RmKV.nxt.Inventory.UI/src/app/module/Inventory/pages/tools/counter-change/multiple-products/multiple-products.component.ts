import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { CounterCode } from 'src/app/common/models/countercode-lookup-model';
import { EditableGridComponent } from 'src/app/common/pages/editable-grid/editable-grid.component';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ToolsService } from 'src/app/common/services/Tools/tools.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { CounterCodeLookupComponent } from 'src/app/common/shared/counter-code-lookup/counter-code-lookup.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { IndexedDBCommonFN } from 'src/app/common/shared/IndexedDB/indexed_db_common';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductChangesService } from 'src/app/module/Inventory/services/tools/product-changes/product-changes.service';
declare var jsPDF: any;
@Component({
  selector: 'app-multiple-products',
  templateUrl: './multiple-products.component.html',
  styleUrls: ['./multiple-products.component.scss'],
  providers: [DatePipe]
})
export class MultipleProductsComponent implements OnInit {
  showTable: boolean = false;
  objCounter = {
    Counter_Code: '',
    Counter_Id: '',
    Counter_Name: '',
    ByNo_details: []
  }
  CounterCodeList: CounterCode[] = [];

  objEditableGrid = {
    tabledata: [],
    counterData: [],
    obj: {
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0
      , uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: 0.00, hsncode: '', gst: 0, unique_byno_prod_serial: null
    },
    flags: {
      isProductCode: true, isProductName: false, isUOM: true, isSellingPrice: true, prod_qty: false, prod_pcs: false,
      isDisable: false, isProductSupplier: true, isCounter: true, isStyleCode: false, isCounterEdit: true
    },
    schema_name: 'multipleProduct'
  }

  objGodown = [{
    byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
    uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
    isDelete: false, valid: false
  }]

  public commonIndexed: IndexedDBCommonFN = new IndexedDBCommonFN(this.dbService, this._localStorage);

  @ViewChild(EditableGridComponent, null) byno_data;
  @ViewChildren("countercode") countercode: ElementRef | any;
  constructor(public _localStorage: LocalStorage,public _excelService: ExcelService,
    public _router: Router, private dbService: NgxIndexedDBService,
    public _matDialog: MatDialog,
    private _datePipe: DatePipe,
    public _productChanges: ProductChangesService,
    public _CounterCodeService: ToolsService,
    public _confirmationDialogComponent: ConfirmationDialogComponent) {
    this.showTable = false;
  }

  ngOnInit() {
    this.showTable = true;
    this.getCounterCodeList();
  }

  private getCounterCodeList(): void {
    let objdata = {
      ProductMaster: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._CounterCodeService.getCounterChangeLookup(objdata).subscribe((result: any[]) => {
      this.CounterCodeList = result;
    });
  }
  public openCounterCodeLookupDialog(event): void {
    if (event.keyCode == 13 && !this.validateCounterCode()) {
      const dialogRef = this._matDialog.open(CounterCodeLookupComponent, {
        width: "500px",
        panelClass: "custom-dialog-container",
        data: {
          searchString: this.objCounter.Counter_Code
        }
      });
      dialogRef.afterClosed().subscribe((result: any) => {
        console.log(result)
        if (result) {
          this.objCounter.Counter_Code = result.counter_code;
          this.objCounter.Counter_Name = result.counter_name;
          this.objCounter.Counter_Id = result.counter_id;
        } else {
          this.objCounter.Counter_Code = "";
          this.objCounter.Counter_Name = "";
          this.objCounter.Counter_Id = "";
        }
      });
    }
  }

  public validateCounterCode(): boolean {
    let index = this.CounterCodeList.findIndex(element => element.counter_code.toString().trim() === this.objCounter.Counter_Code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public onEnterCounterCodeLookupDetails(): void {
    debugger;
    let CounterCodelookUpList: any = {
      counter_code: "",
      counter_name: "",
      counter_id: "",
    };
    CounterCodelookUpList = this.CounterCodeList.find(act => act.counter_code.toLowerCase().trim() === this.objCounter.Counter_Code.toString().trim().toLowerCase());
    this.objCounter.Counter_Name = CounterCodelookUpList && CounterCodelookUpList.counter_name ? CounterCodelookUpList.counter_name : '';
    this.objCounter.Counter_Id = CounterCodelookUpList && CounterCodelookUpList.counter_id ? CounterCodelookUpList.counter_id : 0;
    this.objCounter.Counter_Name = CounterCodelookUpList && CounterCodelookUpList.counter_name ? CounterCodelookUpList.counter_name : '';
  }

  public saveCounterCode(): void {
    debugger;
    if (this.beforeSaveValidation()) {
      // let objProductMaster: any = {
      //   ProductMaster: JSON.stringify([{
      //     company_section_id: +this._localStorage.getCompanySectionId(),
      //     new_counter_id: this.objCounter.Counter_Id,
      //     byno_details: this.objCounter.ByNo_details,
      //     entered_by: this._localStorage.intGlobalUserId(),
      //   }]),
      // }
      for (let i = 0; i < this.objEditableGrid.tabledata.length; i++) {
        if (this.objEditableGrid.tabledata[i].product_code == '') {
          this.objEditableGrid.tabledata.splice(i, 1);
        }
      }
      let objDetails = [];
      for (let i = 0; i < this.byno_data.objEditableGrid.tabledata.length; i++) {
        objDetails.push(Object.assign({
          serial_no: i + 1,
          byno: this.byno_data.objEditableGrid.tabledata[i].byno,
          inv_byno: this.byno_data.objEditableGrid.tabledata[i].inv_byno,
          byno_serial: this.byno_data.objEditableGrid.tabledata[i].byno_serial,
          byno_prod_serial: this.byno_data.objEditableGrid.tabledata[i].byno_prod_serial,
          // uom_id: this.byno_data.objEditableGrid.tabledata[i].uom_id,
          // uom_descrition: this.byno_data.objEditableGrid.tabledata[i].uom_descrition,
          old_counter_id: this.byno_data.objEditableGrid.tabledata[i].counter_id,
          // return_pcs: this.byno_data.objEditableGrid.tabledata[i].prod_pcs,
          // return_quantity: this.byno_data.objEditableGrid.tabledata[i].prod_qty,
          // selling_price: this.byno_data.objEditableGrid.tabledata[i].selling_price,
          // hsncode: this.byno_data.objEditableGrid.tabledata[i].hsncode,
          // gst: this.byno_data.objEditableGrid.tabledata[i].gst,
          // unique_byno_prod_serial: this.byno_data.objEditableGrid.tabledata[i].unique_byno_prod_serial,
        }));
      }
      let objsave = {
        ProductMaster: JSON.stringify([{
          company_section_id: +this._localStorage.getCompanySectionId(),
          new_counter_id: this.objCounter.Counter_Id,
          entered_by: this._localStorage.intGlobalUserId(),
          byno_details: objDetails,
        }]),
      }
      this._productChanges.saveCounterCode(objsave).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog("New counter code updated", "Multiple Products");
          // this.resetScreen();
        }
      });
    }
  }

  private beforeSaveValidation(): boolean {
    debugger;
    if ([null, undefined, 0, ""].indexOf(this.objCounter.Counter_Code) !== -1) {
      document.getElementById("counterCode").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter counter Code', 'Multiple Products')
      return false;
    }
    else if ([null, undefined, 0, ""].indexOf(this.byno_data.objEditableGrid.tabledata[0].byno) !== -1) {
      let input = this.byno_data.byno.toArray();
      input[0].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog('Enter byno', 'Multiple Products')
      return false;
    }

    else return true;
  }

  // exportToPdf(): void {

  //   if (this.objEditableGrid.tabledata.length != 1) {
  //     var prepare = [];
  //     this.objEditableGrid.tabledata.forEach(e => {
  //       var tempObj = [];
  //       tempObj.push(e.byno);
  //       tempObj.push(e.product_code);
  //       tempObj.push(e.product_supplier_name);
  //       tempObj.push(e.uom_description);
  //       tempObj.push(e.selling_price);
  //       tempObj.push(e.counter_code);
  //       prepare.push(tempObj);
  //     });

  //     const doc = new jsPDF('l', 'pt', "a4");
  //     doc.autoTable({
  //       head: [["byno", "Product Code", "Product Supplier Description", "UOM","Selling Price","Counter"]],
  //       body: prepare,
  //       styles: { overflow: 'linebreak', columnWidth: 'wrap' },
  //       columnStyles: { text: { columnWidth: 'auto' } },
  //       didParseCell: function (table) {
  //         if (table.section === 'head') {
  //           table.cell.styles.textColor = '#FFFFFF';
  //           table.cell.styles.fillColor = '#d32f2f';
  //         }
  //       }
  //     });

  //     doc.save('Multiple Product' + '.pdf');
  //   } else
  //     this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Multiple Product");
  // }


  
  // public exportToExcel(): void {
  //   if (this.objEditableGrid.tabledata.length > 1) {
  //     let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
  //     let json = [];
  //     for (let i = 0; i < this.objEditableGrid.tabledata.length; i++) {
  //       json[i] = Object.assign({
  //         "byno": this.objEditableGrid.tabledata[i].byno,
  //         "Product Code": this.objEditableGrid.tabledata[i].product_code,
  //         "Product Supplier Description": this.objEditableGrid.tabledata[i].product_supplier_name,
  //         "UOM": this.objEditableGrid.tabledata[i].uom_description,
  //         "Selling Price": this.objEditableGrid.tabledata[i].selling_price,
  //         "Counter": this.objEditableGrid.tabledata[i].counter_code,
  //       });
  //     }
  //     this._excelService.exportAsExcelFile(json, "Multiple Products", datetime);
  //   } else
  //     this._confirmationDialogComponent.openAlertDialog("No record found", "Multiple Products");
  // }


}


