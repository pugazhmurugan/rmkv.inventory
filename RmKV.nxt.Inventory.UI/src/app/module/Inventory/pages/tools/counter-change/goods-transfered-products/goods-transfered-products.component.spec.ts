import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsTransferedProductsComponent } from './goods-transfered-products.component';

describe('GoodsTransferedProductsComponent', () => {
  let component: GoodsTransferedProductsComponent;
  let fixture: ComponentFixture<GoodsTransferedProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsTransferedProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsTransferedProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
