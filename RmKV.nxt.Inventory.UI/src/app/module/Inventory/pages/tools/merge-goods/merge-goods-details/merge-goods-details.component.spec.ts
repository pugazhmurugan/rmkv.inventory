import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeGoodsDetailsComponent } from './merge-goods-details.component';

describe('MergeGoodsDetailsComponent', () => {
  let component: MergeGoodsDetailsComponent;
  let fixture: ComponentFixture<MergeGoodsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeGoodsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeGoodsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
