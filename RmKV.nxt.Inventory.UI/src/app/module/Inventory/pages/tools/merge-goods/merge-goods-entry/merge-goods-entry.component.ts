import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-merge-goods-entry',
  templateUrl: './merge-goods-entry.component.html',
  styleUrls: ['./merge-goods-entry.component.scss']
})
export class MergeGoodsEntryComponent implements OnInit {

  constructor(  public _localStorage: LocalStorage,
    public _router: Router,) { }

  ngOnInit() {
  }

}
