import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeGoodsEntryComponent } from './merge-goods-entry.component';

describe('MergeGoodsEntryComponent', () => {
  let component: MergeGoodsEntryComponent;
  let fixture: ComponentFixture<MergeGoodsEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeGoodsEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeGoodsEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
