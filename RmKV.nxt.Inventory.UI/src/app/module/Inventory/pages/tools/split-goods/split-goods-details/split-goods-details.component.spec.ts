import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitGoodsDetailsComponent } from './split-goods-details.component';

describe('SplitGoodsDetailsComponent', () => {
  let component: SplitGoodsDetailsComponent;
  let fixture: ComponentFixture<SplitGoodsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitGoodsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitGoodsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
