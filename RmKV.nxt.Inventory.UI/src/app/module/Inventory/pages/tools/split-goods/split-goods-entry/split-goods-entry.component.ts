import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-split-goods-entry',
  templateUrl: './split-goods-entry.component.html',
  styleUrls: ['./split-goods-entry.component.scss']
})
export class SplitGoodsEntryComponent implements OnInit {
  title = 'angular-print-service';
  isPrinting = false;
  constructor(  public _localStorage: LocalStorage,
    public _router: Router,private _httpClient : HttpClient) { }

  ngOnInit() {

  }

  // public GenrateBarcode():void{
  //   window.print();
  // }

  public GenrateBarcode(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
}

  printDocument(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this._router.navigate(['/',
      { outlets: {
        'print': ['print', documentName, documentData.join()]
      }}]);
  }

  onDataReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this._router.navigate([{ outlets: { print: null }}]);
    });
  }

  GenrateBarcodeService(data: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetByNoDetails", data)
    .pipe(catchError(this.handleError));
  }

  handleError(error: Response) {
    return throwError(error);
  }

}
