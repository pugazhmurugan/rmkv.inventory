import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitGoodsEntryComponent } from './split-goods-entry.component';

describe('SplitGoodsEntryComponent', () => {
  let component: SplitGoodsEntryComponent;
  let fixture: ComponentFixture<SplitGoodsEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitGoodsEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitGoodsEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
