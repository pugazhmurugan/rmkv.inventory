import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateNilBynoComponent } from './generate-nil-byno.component';

describe('GenerateNilBynoComponent', () => {
  let component: GenerateNilBynoComponent;
  let fixture: ComponentFixture<GenerateNilBynoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateNilBynoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateNilBynoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
