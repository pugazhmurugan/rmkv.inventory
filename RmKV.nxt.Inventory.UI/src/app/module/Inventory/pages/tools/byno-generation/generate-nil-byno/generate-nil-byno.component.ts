import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-generate-nil-byno',
  templateUrl: './generate-nil-byno.component.html',
  styleUrls: ['./generate-nil-byno.component.scss']
})
export class GenerateNilBynoComponent implements OnInit {

  constructor(  public _localStorage: LocalStorage,
    public _router: Router,) { }

  ngOnInit() {
  }

}
