import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BynoTrackingLocalGoodsComponent } from './byno-tracking-local-goods.component';

describe('BynoTrackingLocalGoodsComponent', () => {
  let component: BynoTrackingLocalGoodsComponent;
  let fixture: ComponentFixture<BynoTrackingLocalGoodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BynoTrackingLocalGoodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BynoTrackingLocalGoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
