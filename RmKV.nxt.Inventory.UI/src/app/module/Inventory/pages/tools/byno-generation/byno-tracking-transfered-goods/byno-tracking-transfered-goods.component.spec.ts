import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BynoTrackingTransferedGoodsComponent } from './byno-tracking-transfered-goods.component';

describe('BynoTrackingTransferedGoodsComponent', () => {
  let component: BynoTrackingTransferedGoodsComponent;
  let fixture: ComponentFixture<BynoTrackingTransferedGoodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BynoTrackingTransferedGoodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BynoTrackingTransferedGoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
