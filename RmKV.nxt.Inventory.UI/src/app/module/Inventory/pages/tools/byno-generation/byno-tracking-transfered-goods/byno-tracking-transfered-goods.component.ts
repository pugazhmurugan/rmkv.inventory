import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ImageLookupComponent } from 'src/app/common/shared/image-lookup/image-lookup.component';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-byno-tracking-transfered-goods',
  templateUrl: './byno-tracking-transfered-goods.component.html',
  styleUrls: ['./byno-tracking-transfered-goods.component.scss']
})
export class BynoTrackingTransferedGoodsComponent implements OnInit {

 
 
  componentVisibility: boolean = true;

  constructor(  public _localStorage: LocalStorage,
    public _router: Router,public _matDialog: MatDialog,) { }

  ngOnInit() {
  }

  newClick():void {
    this.componentVisibility = !this.componentVisibility;
  }

  onClear():void {
    this.componentVisibility = !this.componentVisibility;
  }

  public openImageDialog(): void {
    const dialogRef = this._matDialog.open(ImageLookupComponent, {
      width: "600px",
      panelClass: "custom-dialog-container",
      data: {}
    });
    dialogRef.afterClosed().subscribe((result: any) => {
    });
  }

}
