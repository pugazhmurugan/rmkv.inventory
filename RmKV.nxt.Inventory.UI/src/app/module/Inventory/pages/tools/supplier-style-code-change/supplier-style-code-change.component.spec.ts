import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierStyleCodeChangeComponent } from './supplier-style-code-change.component';

describe('SupplierStyleCodeChangeComponent', () => {
  let component: SupplierStyleCodeChangeComponent;
  let fixture: ComponentFixture<SupplierStyleCodeChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierStyleCodeChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierStyleCodeChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
