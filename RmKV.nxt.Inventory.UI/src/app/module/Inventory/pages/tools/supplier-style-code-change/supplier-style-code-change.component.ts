import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { EditableGridComponent } from 'src/app/common/pages/editable-grid/editable-grid.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { IndexedDBCommonFN } from 'src/app/common/shared/IndexedDB/indexed_db_common';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { StyleCodeLookupComponent } from 'src/app/common/shared/style-code-lookup/style-code-lookup.component';
import { ProductChangesService } from '../../../services/tools/product-changes/product-changes.service';

@Component({
  selector: 'app-supplier-style-code-change',
  templateUrl: './supplier-style-code-change.component.html',
  styleUrls: ['./supplier-style-code-change.component.scss']
})
export class SupplierStyleCodeChangeComponent implements OnInit {
  objStyle = {
    Style_Code: '',
    ByNo_details: []
  }


  
  objEditableGrid = {
    tabledata: [],
    obj: {
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0
      , uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: 0.00, hsncode: '', gst: 0, unique_byno_prod_serial: null
    },
    flags: { isProductCode: true, isProductName: false, isUOM: true, isSellingPrice: true, prod_qty: false, prod_pcs: false,
      isDisable: false, isProductSupplier : true,isCounter : false, isStyleCode : true },
    schema_name: 'ProductChangeStyleCode'
  }

  objGodown = [{
    byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
    uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
    isDelete: false, valid: false
  }]

  public commonIndexed: IndexedDBCommonFN = new IndexedDBCommonFN(this.dbService, this._localStorage);

  @ViewChild(EditableGridComponent, null) byno_data;
  showTable: boolean = false;

  constructor(public _localStorage: LocalStorage, private dbService: NgxIndexedDBService,
    public _router: Router, public _matDialog: MatDialog,
    public _productChanges: ProductChangesService,
    public _confirmationDialogComponent:ConfirmationDialogComponent) {
      this.showTable = false;
     }

  ngOnInit() {
    this.showTable = true;
  }
  public openStyleCodeLookupDialog(event): void {
    if (event.keyCode == 13) {
      const dialogRef = this._matDialog.open(StyleCodeLookupComponent, {
        width: "500px",
        panelClass: "custom-dialog-container",
        data: {
          searchString: this.objStyle.Style_Code
        }
      });
      dialogRef.afterClosed().subscribe((result: any) => {
        console.log(result)
        this.objStyle.Style_Code = result.supplier_style_code;

      });
    }
  }

  public saveStyleCode():void {
    debugger;
    if (this.beforeSaveValidation()) {
      for (let i = 0; i < this.objEditableGrid.tabledata.length; i++) {
        if (this.objEditableGrid.tabledata[i].product_code == '') {
          this.objEditableGrid.tabledata.splice(i, 1);
        }
      }
      let objDetails = [];
      for (let i = 0; i < this.byno_data.objEditableGrid.tabledata.length; i++) {
        objDetails.push(Object.assign({
          serial_no: i + 1,
          byno: this.byno_data.objEditableGrid.tabledata[i].byno,
          inv_byno: this.byno_data.objEditableGrid.tabledata[i].inv_byno,
          byno_serial: this.byno_data.objEditableGrid.tabledata[i].byno_serial,
          byno_prod_serial: this.byno_data.objEditableGrid.tabledata[i].byno_prod_serial,
          // uom_id: this.byno_data.objEditableGrid.tabledata[i].uom_id,
          // uom_descrition: this.byno_data.objEditableGrid.tabledata[i].uom_descrition,
          old_style_code: this.byno_data.objEditableGrid.tabledata[i].style_code,
          // return_pcs: this.byno_data.objEditableGrid.tabledata[i].prod_pcs,
          // return_quantity: this.byno_data.objEditableGrid.tabledata[i].prod_qty,
          // selling_price: this.byno_data.objEditableGrid.tabledata[i].selling_price,
          // hsncode: this.byno_data.objEditableGrid.tabledata[i].hsncode,
          // gst: this.byno_data.objEditableGrid.tabledata[i].gst,
          // unique_byno_prod_serial: this.byno_data.objEditableGrid.tabledata[i].unique_byno_prod_serial,
        }));
      }
    let objProductMaster: any = {
      ProductMaster: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        new_product_code: this.objStyle.Style_Code,
        entered_by: this._localStorage.intGlobalUserId(),
        byno_details: objDetails,
      }]),
    }
    this._productChanges.saveStyleCode(objProductMaster).subscribe((result: any) => {
      if (result) {
        this._confirmationDialogComponent.openAlertDialog("New style code updated", "Supplier style code");
       // this.resetScreen();
      }
    });
  }
}

private beforeSaveValidation(): boolean {
  debugger;
  if ([null, undefined, 0, ""].indexOf(this.objStyle.Style_Code) !== -1) {
    document.getElementById("styleCode").focus();
    this._confirmationDialogComponent.openAlertDialog('Enter style Code', 'Supplier style code')
    return false;
  }
  else if ([null, undefined, 0, ""].indexOf(this.byno_data.objEditableGrid.tabledata[0].byno) !== -1) {
    let input = this.byno_data.byno.toArray();
    input[0].nativeElement.focus();
    this._confirmationDialogComponent.openAlertDialog('Enter byno', 'Supplier style code')
    return false;
  } else return true;
}

}