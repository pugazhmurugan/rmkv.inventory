import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductChangeSingleComponent } from './product-change-single.component';

describe('ProductChangeSingleComponent', () => {
  let component: ProductChangeSingleComponent;
  let fixture: ComponentFixture<ProductChangeSingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductChangeSingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductChangeSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
