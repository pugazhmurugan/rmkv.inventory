import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { ProductCode } from 'src/app/common/models/product-code-lookup-model';
import { EditableGridComponent } from 'src/app/common/pages/editable-grid/editable-grid.component';
import { ToolsService } from 'src/app/common/services/Tools/tools.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { IndexedDBCommonFN } from 'src/app/common/shared/IndexedDB/indexed_db_common';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductCodeLookupComponent } from 'src/app/common/shared/product-code-lookup/product-code-lookup.component';
import { ProductChangesService } from 'src/app/module/Inventory/services/tools/product-changes/product-changes.service';

@Component({
  selector: 'app-product-change-byno-serial',
  templateUrl: './product-change-byno-serial.component.html',
  styleUrls: ['./product-change-byno-serial.component.scss']
})
export class ProductChangeBynoSerialComponent implements OnInit {
  showTable: boolean = false;
  objProduct = {
    Product_Code: '',
    Product_Name: '',
    ByNo_details: []
  }

  objEditableGrid = {
    tabledata: [],
    obj: {
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0
      , uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: 0.00, hsncode: '', gst: 0, unique_byno_prod_serial: null
    },
    flags: { isProductCode: true, isProductName: false, isUOM: true, isSellingPrice: true, prod_qty: false, prod_pcs: false,
      isDisable: false, isProductSupplier : true,isCounter : false, isStyleCode : false },
    schema_name: 'ProductChangebyno'
  }

  objGodown = [{
    byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
    uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
    isDelete: false, valid: false
  }]

  public commonIndexed: IndexedDBCommonFN = new IndexedDBCommonFN(this.dbService, this._localStorage);

  @ViewChild(EditableGridComponent, null) byno_data;
  ProductCodeList: ProductCode[] = [];

  constructor(public _localStorage: LocalStorage, private dbService: NgxIndexedDBService,
    public _router: Router, public _matDialog: MatDialog,
    public _productChanges: ProductChangesService,
    public _ProductCodeService: ToolsService,
    public _confirmationDialogComponent: ConfirmationDialogComponent) {
      this.getProductCodeList();
    this.showTable = false;
  }

  ngOnInit() {
    this.showTable = true;
  }

  private getProductCodeList(): void {
    let objdata = {
      ProductMaster: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._ProductCodeService.getProductChangeLookup(objdata).subscribe((result: any[]) => {
      this.ProductCodeList = result;
    });
  }


  public openProductCodeLookupDialog(event): void {
    if (event.keyCode == 13 && !this.validateCounterCode()) {
      const dialogRef = this._matDialog.open(ProductCodeLookupComponent, {
        width: "600px",
        panelClass: "custom-dialog-container",
        data: {
          searchString: this.objProduct.Product_Code
        }
      });
      dialogRef.afterClosed().subscribe((result: any) => {
        console.log(result,"productList")
        if(result) {
        this.objProduct.Product_Code = result.product_code;
        this.objProduct.Product_Name = result.product_name;
        } else {
          this.objProduct.Product_Code = "";
          this.objProduct.Product_Name ="";
        }
      });
    }
  }

  public validateCounterCode(): boolean {
    let index = this.ProductCodeList.findIndex(element => element.product_code.toString().trim() === this.objProduct.Product_Code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public onEnterProductCodeLookupDetails(): void {
    debugger;
    let CounterCodelookUpList: any = {
      product_code: "",
      product_name: "",
    };
    CounterCodelookUpList = this.ProductCodeList.find(act => act.product_code.toLowerCase().trim() === this.objProduct.Product_Code.toString().trim().toLowerCase());
    this.objProduct.Product_Code = CounterCodelookUpList && CounterCodelookUpList.counter_code ? CounterCodelookUpList.counter_code : this.objProduct.Product_Code.toString().trim();
    this.objProduct.Product_Name = CounterCodelookUpList && CounterCodelookUpList.product_name ? CounterCodelookUpList.product_name : '';
  }

  public saveProductCode():void {
    if (this.beforeSaveValidation()) {
    // let objProductMaster: any = {
    //   ProductMaster: JSON.stringify([{
    //     company_section_id: +this._localStorage.getCompanySectionId(),
    //     new_product_code: this.objProduct.Product_Code,
    //     byno_details: this.objProduct.ByNo_details,
    //     entered_by: this._localStorage.intGlobalUserId(),
    //   }]),
    // }
    for (let i = 0; i < this.objEditableGrid.tabledata.length; i++) {
      if (this.objEditableGrid.tabledata[i].product_code == '') {
        this.objEditableGrid.tabledata.splice(i, 1);
      }
    }
    let objDetails = [];
    for (let i = 0; i < this.byno_data.objEditableGrid.tabledata.length; i++) {
      objDetails.push(Object.assign({
        serial_no: i + 1,
        byno: this.byno_data.objEditableGrid.tabledata[i].byno,
        inv_byno: this.byno_data.objEditableGrid.tabledata[i].inv_byno,
        byno_serial: this.byno_data.objEditableGrid.tabledata[i].byno_serial,
        byno_prod_serial: this.byno_data.objEditableGrid.tabledata[i].byno_prod_serial,
        // uom_id: this.byno_data.objEditableGrid.tabledata[i].uom_id,
        // uom_descrition: this.byno_data.objEditableGrid.tabledata[i].uom_descrition,
        old_product_code: this.byno_data.objEditableGrid.tabledata[i].product_code.toString().trim(),
        // return_pcs: this.byno_data.objEditableGrid.tabledata[i].prod_pcs,
        // return_quantity: this.byno_data.objEditableGrid.tabledata[i].prod_qty,
        // selling_price: this.byno_data.objEditableGrid.tabledata[i].selling_price,
        // hsncode: this.byno_data.objEditableGrid.tabledata[i].hsncode,
        // gst: this.byno_data.objEditableGrid.tabledata[i].gst,
        // unique_byno_prod_serial: this.byno_data.objEditableGrid.tabledata[i].unique_byno_prod_serial,
      }));
    }
    let objsave = {
      ProductMaster: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        new_product_code: this.objProduct.Product_Code.toString().trim(),
        entered_by: this._localStorage.intGlobalUserId(),
        byno_details: objDetails,

      }]),
    }
    this._productChanges.saveProductCode(objsave).subscribe((result: any) => {
      if (result) {
        this._confirmationDialogComponent.openAlertDialog("New product code updated", "Product Changes");
        // this.resetScreen();
      }
    });
  }
}

  private beforeSaveValidation(): boolean {
    debugger;
    if ([null, undefined, 0, ""].indexOf(this.objProduct.Product_Code) !== -1) {
      document.getElementById("productcode").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter product Code', 'Product Changes')
      return false;
    }
    else if ([null, undefined, 0, ""].indexOf(this.byno_data.objEditableGrid.tabledata[0].byno) !== -1) {
      let input = this.byno_data.byno.toArray();
      input[0].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog('Enter byno', 'Product Changes')
      return false;
    } else return true;
  }

}


