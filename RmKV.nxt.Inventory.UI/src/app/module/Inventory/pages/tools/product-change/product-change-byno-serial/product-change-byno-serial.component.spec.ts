import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductChangeBynoSerialComponent } from './product-change-byno-serial.component';

describe('ProductChangeBynoSerialComponent', () => {
  let component: ProductChangeBynoSerialComponent;
  let fixture: ComponentFixture<ProductChangeBynoSerialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductChangeBynoSerialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductChangeBynoSerialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
