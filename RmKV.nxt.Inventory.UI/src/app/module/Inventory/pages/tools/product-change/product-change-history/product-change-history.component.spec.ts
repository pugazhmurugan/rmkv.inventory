import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductChangeHistoryComponent } from './product-change-history.component';

describe('ProductChangeHistoryComponent', () => {
  let component: ProductChangeHistoryComponent;
  let fixture: ComponentFixture<ProductChangeHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductChangeHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductChangeHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
