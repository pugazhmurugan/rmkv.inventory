import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductChangeProductCodeComponent } from './product-change-product-code.component';

describe('ProductChangeProductCodeComponent', () => {
  let component: ProductChangeProductCodeComponent;
  let fixture: ComponentFixture<ProductChangeProductCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductChangeProductCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductChangeProductCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
