import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-product-change-product-code',
  templateUrl: './product-change-product-code.component.html',
  styleUrls: ['./product-change-product-code.component.scss']
})
export class ProductChangeProductCodeComponent implements OnInit {

  constructor(  public _localStorage: LocalStorage,
    public _router: Router,) { }

  ngOnInit() {
  }

}
