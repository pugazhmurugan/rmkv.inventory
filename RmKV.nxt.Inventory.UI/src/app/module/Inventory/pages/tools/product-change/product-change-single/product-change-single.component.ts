import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-product-change-single',
  templateUrl: './product-change-single.component.html',
  styleUrls: ['./product-change-single.component.scss']
})
export class ProductChangeSingleComponent implements OnInit {

  constructor(  public _localStorage: LocalStorage,
    public _router: Router,) { }

  ngOnInit() {
  }

}
