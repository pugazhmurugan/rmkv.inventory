import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierDescriptionChangeComponent } from './supplier-description-change.component';

describe('SupplierDescriptionChangeComponent', () => {
  let component: SupplierDescriptionChangeComponent;
  let fixture: ComponentFixture<SupplierDescriptionChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierDescriptionChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierDescriptionChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
