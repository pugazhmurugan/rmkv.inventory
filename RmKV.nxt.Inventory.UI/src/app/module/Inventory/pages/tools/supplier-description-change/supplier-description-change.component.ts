import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { data } from 'jquery';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ForCheckBynoData } from '../../../model/common.model';
import { SingleProductService } from '../../../services/pricing/selling-price-change/single-product/single-product.service';
import { SupplierDescriptionChangeService } from '../../../services/tools/supplier-description-change/supplier-description-change.service';
declare var jsPDF: any;

@Component({
  selector: 'app-supplier-description-change',
  templateUrl: './supplier-description-change.component.html',
  styleUrls: ['./supplier-description-change.component.scss'],
  providers: [DatePipe]
})
export class SupplierDescriptionChangeComponent implements OnInit {
  objSupplierDescription: any = {
    byno: "",
  }

  objUnChangeSupplierDescription: any = {
    byno: "",
  }

  ForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  UnChangedForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  Inv_Byno: string = "";
  Byno_Serial: string = "";
  Byno_Prod_Serial: string = "";
  keyUpEvent: any;

  supplierDescriptionChangeList: any[] = [];
  columns: any[] = [
    { display: 'supplierdescription', editable: true },
    { display: 'doublePercent', editable: true },
    { display: 'doubleRate', editable: true },
    { display: 'discountPercent', editable: true },
  ];
  @ViewChildren('supplierdescription') supplierdescription: ElementRef | any;
  @ViewChildren('byno') byNo: ElementRef | any;
  @ViewChildren('doublePercent') doublePercent: ElementRef | any;
  @ViewChildren('doubleRate') doubleRate: ElementRef | any;
  @ViewChildren('discountPercent') discountPercent: ElementRef | any;
  
  constructor(public _localStorage: LocalStorage, private _supplierDescriptionChangeService: SupplierDescriptionChangeService,
    public _router: Router,
    private _matDialog: MatDialog,
    private _singleProductService: SingleProductService,
    public _commonService: CommonService,
    private _excelService: ExcelService,
    private _datePipe: DatePipe,
    private _gridKeyEvents: GridKeyEvents,
    public _confirmationDialogComponent: ConfirmationDialogComponent,) { }

  ngOnInit() {
  }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.supplierDescriptionChangeList);
        // this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.doubleRateByPercentList);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.supplierDescriptionChangeList);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.supplierDescriptionChangeList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.supplierDescriptionChangeList);
        break;
    }
  }

  public onKeyPress(keyUpEvent: any) {
    debugger;
    this.supplierDescriptionChangeList = [];
    if (keyUpEvent.keyCode === 13 && this.objSupplierDescription.byno === '') {
      document.getElementById("byno").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter byno', 'Supplier Description Change');
    } else if (keyUpEvent.keyCode === 13 && this.objSupplierDescription.byno.length === 8) {
      this.objSupplierDescription.byno = this.objSupplierDescription.byno.toString().trim().toUpperCase();
      this.ForCheckBynoData.Inv_Byno = this.objSupplierDescription.byno.toString().trim().toUpperCase();
      this.ForCheckBynoData.Byno_Serial = "";
      this.ForCheckBynoData.Byno_Prod_Serial = "";
      this.getDblRateByPercenttList();
    } else if (keyUpEvent.keyCode === 13 && this.objSupplierDescription.byno.length == 16 && !this.objSupplierDescription.byno.includes("/")) {
      this.byNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objSupplierDescription.byno.length == 28 && !this.objSupplierDescription.byno.includes("/")) {
      this.OldByNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objSupplierDescription.byno.length == 30 && !this.objSupplierDescription.byno.includes("/")) {
      this.byNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objSupplierDescription.byno.includes("/")) {
      debugger
      this.splitByNumber();
    } else if (this.objSupplierDescription.byno.length >= 28 && keyUpEvent.keyCode === 13) {
      this.autoScannerCall();
    } else {
      if (keyUpEvent.keyCode === 13 && !this.objSupplierDescription.byno.includes("/") && this.objSupplierDescription.byno.length > 16) {
        document.getElementById('byno').focus();
        this._confirmationDialogComponent.openAlertDialog("Invalid Byno", "Supplier description change");
      } else if (keyUpEvent.keyCode === 13 && !this.checkValid()) {
        document.getElementById('byno').focus();
        this._confirmationDialogComponent.openAlertDialog("Invalid Byno", "Supplier description change");
        this.objSupplierDescription.byno = '';
      }
    }
  }

  private byNoWithoutSlash() {
    debugger;
    this.objSupplierDescription.byno = this.objSupplierDescription.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objSupplierDescription.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objSupplierDescription.byno.substr(9, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objSupplierDescription.byno.substr(13, 3);
    this.getDblRateByPercenttList();
  }

  private OldByNoWithoutSlash() {
    debugger;
    this.objSupplierDescription.byno = this.objSupplierDescription.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objSupplierDescription.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objSupplierDescription.byno.substr(8, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objSupplierDescription.byno.substr(11, 3);
    this.getDblRateByPercenttList();
  }

  private splitByNumber() {
    debugger;
    this.objSupplierDescription.byno = this.objSupplierDescription.byno.toString().trim().toUpperCase();
    var x = this.objSupplierDescription.byno.split("/");
    this.Inv_Byno = x[0];
    this.Byno_Serial = x[1];
    this.Byno_Prod_Serial = [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(x[2]) !== -1 ? "" : x[2];
    if (this.Byno_Serial.length == 2) {
      this.Byno_Serial = "00".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 2) {
      this.Byno_Prod_Serial = "00".concat(this.Byno_Prod_Serial);
    }
    if (this.Byno_Serial.length == 1) {
      this.Byno_Serial = "000".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 1) {
      this.Byno_Prod_Serial = "000".concat(this.Byno_Prod_Serial);
    } if (this.Byno_Serial.length == 0) {
      document.getElementById('byno').focus();
      this._confirmationDialogComponent.openAlertDialog('Invalid Byno', 'Supplier description change');
      return;
    } if (this.Byno_Prod_Serial.length == 0) {
      document.getElementById('byno').focus();
      this._confirmationDialogComponent.openAlertDialog('Invalid Byno', 'Supplier description change');
      return;
    }
    this.ForCheckBynoData.Inv_Byno = this.Inv_Byno.toString().trim();
    this.ForCheckBynoData.Byno_Serial = this.Byno_Serial.toString().trim();
    this.ForCheckBynoData.Byno_Prod_Serial = this.Byno_Prod_Serial.toString().trim();
    this.getDblRateByPercenttList();
  }

  private autoScannerCall() {
    debugger;
    this.ForCheckBynoData.Inv_Byno = this.objSupplierDescription.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = this.objSupplierDescription.byno.substr(8, 4);
    this.ForCheckBynoData.Byno_Prod_Serial = this.objSupplierDescription.byno.substr(11, 4);
    this.getDblRateByPercenttList();
  }

  private checkValid(): boolean {
    debugger;
    let index = this.supplierDescriptionChangeList.findIndex(x => x.byno.toString().trim().toUpperCase() === this.objSupplierDescription.inv_byno.toString().trim().toUpperCase());
    if (index !== -1)
      return true;
    else
      return false;
  }

   /*************************************************** CRUD Operations ***************************************************/
 

  private getDblRateByPercenttList(): void {
    // if (this.beforeLoadValidate()) {
    let objGet: any = {
      SingleProduct: JSON.stringify([{
        inv_byno: this.ForCheckBynoData.Inv_Byno.toString().trim().toUpperCase(),
        byno_serial: this.ForCheckBynoData.Byno_Serial.toString().trim().toUpperCase(),
        byno_prod_serial: this.ForCheckBynoData.Byno_Prod_Serial.toString().trim().toUpperCase(),
        company_section_id: this._localStorage.getCompanySectionId()
      }])
    }
    this._singleProductService.getSingleProduct(objGet).subscribe((result: any) => {
      if (result) {
        this.supplierDescriptionChangeList = JSON.parse(JSON.stringify(result));
        console.log(result, "supplierDescriptionChangeList")
        this.afterLoadSingleProductCosts();
        for(var i = 0; i < this.supplierDescriptionChangeList.length; i++){
          this.supplierDescriptionChangeList[i].new_supplier_desc = this.supplierDescriptionChangeList[i].supplier_description
        }
      } else {
        this.objSupplierDescription.byno = '';
        this._confirmationDialogComponent.openAlertDialog('No records found', 'Supplier description change');
      }
    });
    // }
  }

  private afterLoadSingleProductCosts(): void {
    this.supplierDescriptionChangeList.forEach((x, i) => {
      x.new_selling_price = x.selling_price;
      x.double_percent = 0;
      x.double_rate = x.selling_price;
      x.discount_percent = 0;
      x.discount_rate = x.selling_price;
    });
    // setTimeout(() => {
    //   let input = this.doublePercent.toArray();
    //   input[0].nativeElement.focus();
    // }, 100);
  }

  // public openReceivedByNo(element: any): any {
  //   debugger;
  //  let obj = {
  //     ByNoData: JSON.stringify([{
  //       inv_byno: element.Inv_Byno,
  //       byno_serial: element.Byno_Serial,
  //       byno_prod_serial: element.Byno_Prod_Serial,
  //       company_section_id: +this._localStorage.getCompanySectionId()
  //     }])
  //   }
  //   this._commonService.getByNoDetails(obj).subscribe((result: any) => {
  //     if (result !== 'null' && JSON.parse(result).length > 0) {
  //       this.supplierDescriptionChangeList = JSON.parse(result); 
  //        let data = JSON.parse(result);
  //       console.log(data,'dbhb')
  //     }
  //   })
  // }

  public addSupplierDescription(): void {
    debugger;
    if (this.beforeSaveValidate()) {
    let objDetails: any = [];
    for (let i = 0; i < this.supplierDescriptionChangeList.length; i++) {
      if([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.supplierDescriptionChangeList[i].new_supplier_desc) === -1){
        objDetails.push({
          inv_byno: this.supplierDescriptionChangeList[i].inv_byno,
          byno_serial: this.supplierDescriptionChangeList[i].byno_serial,
          new_supplier_desc: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.supplierDescriptionChangeList[i].new_supplier_desc) === -1 ? this.supplierDescriptionChangeList[i].new_supplier_desc : '',
          old_supplier_desc: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.supplierDescriptionChangeList[i].supplier_description) === -1 ? this.supplierDescriptionChangeList[i].supplier_description : ''
        });
      }
    }
    let objsave = {
      SupplierDescriptionChange: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        entered_by: this._localStorage.intGlobalUserId(),
        byno_details: objDetails,
      }]),
    }

    // let tempArr: any = [];
    // for (let i = 0; i < this.supplierDescriptionChangeList.length; i++) {
    //   if (([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.supplierDescriptionChangeList[i].supplier_description) === -1 )) {
    //     tempArr.push({
    //       inv_byno: this.doubleRateByPercentList[i].inv_byno,
    //       byno_serial: this.doubleRateByPercentList[i].byno_serial,
    //       byno_prod_serial: this.doubleRateByPercentList[i].byno_prod_serial,
    //       old_selling_price: +this.doubleRateByPercentList[i].selling_price,
    //       new_selling_price: +this.doubleRateByPercentList[i].discount_rate,
    //       selling_price: +this.doubleRateByPercentList[i].selling_price,
    //       entered_by: this._localStorage.intGlobalUserId(),
    //       company_section_id: this._localStorage.getCompanySectionId(),
    //       double_rate: +this.doubleRateByPercentList[i].double_rate,
    //       discount_rate: +this.doubleRateByPercentList[i].discount_rate,
    //       add_margin: +this.doubleRateByPercentList[i].double_percent,
    //       less_margin: +this.doubleRateByPercentList[i].discount_percent,
    //       aadi_pricecode: ''
    //     });
    //   }
    // }
    // let objsave: any = {
    //   SupplierDescriptionChange: JSON.stringify(tempArr)
    // }

    this._supplierDescriptionChangeService.saveSupplierDescription(objsave).subscribe((result: boolean) => {
      debugger;
      if (result) {
        this.openAlertDialog('Saved successfully');
      }
    });
  }
}

private beforeSaveValidate(): boolean {
  debugger;
  if (this.supplierDescriptionChangeList.length === 0) {
    this._confirmationDialogComponent.openAlertDialog('No records to save, Load the data first', 'Supplier description change');
    return false;
  }  else return true;
}

  openAlertDialog(meassge: any,) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = meassge;
    _dialogRef.componentInstance.componentName = "Supplier description change";
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.openPrintConfirmationDialog();
      _dialogRef = null;
    });
  }

  public openPrintConfirmationDialog(): any {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    _dialogRef.componentInstance.confirmMessage = "Do you want to print this record ?"
    _dialogRef.componentInstance.componentName = "Supplier description change";
    return _dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.print();
      else {
        this.resetScreen();
      }

      _dialogRef = null;
    });
  }

  private print() {
    this.resetScreen();
  }

  private resetScreen(): void {
    this.supplierDescriptionChangeList = [];
    this.ForCheckBynoData = JSON.parse(JSON.stringify(this.UnChangedForCheckBynoData));
    this.objSupplierDescription = JSON.parse(JSON.stringify(this.objUnChangeSupplierDescription));
  }

    /*************************************************** Exports ************************************************/

    public exportToExcel(): void {
      if (this.supplierDescriptionChangeList.length > 0) {
        let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
        let json = [];
        for (let i = 0; i < this.supplierDescriptionChangeList.length; i++) {
          json[i] = Object.assign({
            "INV Byno": this.supplierDescriptionChangeList[i].byno,
            "Product Group Name": this.supplierDescriptionChangeList[i].product_name,
            "Supplier Description": this.supplierDescriptionChangeList[i].new_supplier_desc,
          });
        }
        this._excelService.exportAsExcelFile(json, "Supplier description", datetime);
      } else
        this._confirmationDialogComponent.openAlertDialog("No records found, Load the data first", "Supplier description");
    }
  
    public exportToPdf(): void {
      if (this.supplierDescriptionChangeList.length != 0) {
        var prepare = [];
        this.supplierDescriptionChangeList.forEach(e => {
          var tempObj = [];
          tempObj.push(e.byno);
          tempObj.push(e.product_name);
          tempObj.push(e.new_supplier_desc);
          prepare.push(tempObj);
        });
  
        const doc = new jsPDF('l', 'pt', "a4");
        doc.autoTable({
          head: [["INV Byno", "Product Group Name", "Supplier Description",]],
          body: prepare,
          styles: { overflow: 'linebreak', columnWidth: 'wrap' },
          columnStyles: { text: { columnWidth: 'auto' } },
          didParseCell: function (table) {
            if (table.section === 'head') {
              table.cell.styles.textColor = '#FFFFFF';
              table.cell.styles.fillColor = '#d32f2f';
            }
          }
        });
  
        doc.save('Supplier description' + '.pdf');
      } else
        this._confirmationDialogComponent.openAlertDialog("No records found, Load the data first", "Supplier description");
    }

}

