import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, ViewChildren } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { EditMode } from 'src/app/common/models/common-model';
import { AccountsLookupService } from 'src/app/common/services/accounts-lookup/accounts-lookup.service';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductGroupLookupComponent } from 'src/app/common/shared/product-group-lookup/product-group-lookup.component';
import { SingleFileAttachComponent } from 'src/app/common/shared/single-file-attach/single-file-attach.component';
import { Flags, InvoiceDetails, InvoicePoNos, Invoices, InvoicesLoad, PendingInvoiceNos, SupplierDescription } from '../../../model/purchases/invoices-model';
import { ProductGroupAttributesService } from '../../../services/masters/product-group-attributes/product-group-attributes.service';
import { ByAmountService } from '../../../services/pricing/selling-price-change/single-product/double-rate/by-amount/by-amount.service';
import { InvoicesService } from '../../../services/purchases/invoices/invoices.service';
import { DiscountLookupComponent } from '../discount-lookup/discount-lookup.component';

@Component({
  selector: 'app-edit-cost-price',
  templateUrl: './edit-cost-price.component.html',
  styleUrls: ['./edit-cost-price.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class EditCostPriceComponent implements OnInit {

  invByno: string = '';
  supplierDescList: SupplierDescription[] = [];
  newSupplierLookupList: any = [];
  loadSupplierLookupList: any = [];
  productGroupLookupList: any = [];
  invoiceNosList: PendingInvoiceNos[] = [];
  poNosList: InvoicePoNos[] = []

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  fromDate1: any = new Date();
  toDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  dataSource = new MatTableDataSource([]);
  invoicesList: any[] = [];
  displayedColumns = ["Serial_No", "By_No", "Invoice_No", "Invoice_Date", "Supplier_Name", "Invoice_Amount", "Actual_Amount", "Action"];

  flags: Flags = {
    isRegistered: false,
    isIgst: true,
    isComposite: false,
    isDiscountAmount: false,
    isSupplierDesc: false,
    isMrp: false,
    isDryWash: false,
    isAddMargin: false,
  }

  modifyFlags: Flags = {
    isRegistered: false,
    isIgst: true,
    isComposite: false,
    isDiscountAmount: false,
    isSupplierDesc: false,
    isMrp: false,
    isDryWash: false,
    isAddMargin: false,
  }

  unChangedFlags: Flags = {
    isRegistered: false,
    isIgst: true,
    isComposite: false,
    isDiscountAmount: false,
    isSupplierDesc: false,
    isMrp: false,
    isDryWash: false,
    isAddMargin: false,
  }


  objInvoicesLoad: InvoicesLoad = {
    company_section_name: this._localStorage.getCompanySectionName(),
    from_inv_entry_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    to_inv_entry_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    status: 'For Approval',
    supplier_code: '',
    supplier_name: '',
    all_suppliers: false,
    inv_byno: '',
    is_mismatched_invoices: false
  }

  objInvoices: Invoices = {
    inv_byno: '',
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    supplier_gstn_no: '',
    supplier_email: '',
    entered_gstn_no: '',
    supplier_invoice_no: '',
    supplier_invoice_date: new Date(),
    invoice_amount: '0.00',
    invoice_actual_amount: 0,
    grn_no: 0,
    po_id: 0,
    purchase_order_no: '',
    invoice_entry_date: new Date(),
    master_discount_percent: 0,
    master_discount_amount: 0,
    credit_days: 15,
    special_discount_percent: 0,
    special_discount_amount: 0,
    tcs_percent: 0,
    tcs_percent_amount: 0,
    tcs_amount: '0.00',
    actual_tcs_amount: '0.00',
    irn: '',
    product_total: 0,
    actual_product_total: 0,
    discount_total: '0.00',
    actual_discount_total: 0,
    freight_charges: '0.00',
    cgst_total: '0.00',
    actual_cgst_total: '0.00',
    sgst_total: '0.00',
    actual_sgst_total: '0.00',
    igst_total: '0.00',
    actual_igst_total: '0.00',
    round_off_total: 0,
    actual_round_off_total: 0,
    grand_total: '0.00',
    actual_grand_total: '0.00',
    additions: '0.00',
    deductions: '0.00',
    merchandiser_name: '',
    qty_total: 0,
    undeducted_product_total: 0,
    actual_undeducted_product_total: 0,
    include_freight_in_tax: false,
    include_additions_in_tax: false,
    include_deductions_in_tax: false,
  }

  objModifyInvoices: Invoices = {
    inv_byno: '',
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    supplier_gstn_no: '',
    supplier_email: '',
    entered_gstn_no: '',
    supplier_invoice_no: '',
    supplier_invoice_date: new Date(),
    invoice_amount: '0.00',
    invoice_actual_amount: 0,
    grn_no: 0,
    po_id: 0,
    purchase_order_no: '',
    invoice_entry_date: new Date(),
    master_discount_percent: 0,
    master_discount_amount: 0,
    credit_days: 15,
    special_discount_percent: 0,
    special_discount_amount: 0,
    tcs_percent: 0,
    tcs_percent_amount: 0,
    tcs_amount: '0.00',
    actual_tcs_amount: '0.00',
    irn: '',
    product_total: 0,
    actual_product_total: 0,
    discount_total: '0.00',
    actual_discount_total: 0,
    freight_charges: '0.00',
    cgst_total: '0.00',
    actual_cgst_total: '0.00',
    sgst_total: '0.00',
    actual_sgst_total: '0.00',
    igst_total: '0.00',
    actual_igst_total: '0.00',
    round_off_total: 0,
    actual_round_off_total: 0,
    grand_total: '0.00',
    actual_grand_total: '0.00',
    additions: '0.00',
    deductions: '0.00',
    merchandiser_name: '',
    qty_total: 0,
    undeducted_product_total: 0,
    actual_undeducted_product_total: 0,
    include_freight_in_tax: false,
    include_additions_in_tax: false,
    include_deductions_in_tax: false,
  }

  objUnChangedInvoices: Invoices = {
    inv_byno: '',
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    supplier_gstn_no: '',
    supplier_email: '',
    entered_gstn_no: '',
    supplier_invoice_no: '',
    supplier_invoice_date: new Date(),
    invoice_amount: '0.00',
    invoice_actual_amount: 0,
    grn_no: 0,
    po_id: 0,
    purchase_order_no: '',
    invoice_entry_date: new Date(),
    master_discount_percent: 0,
    master_discount_amount: 0,
    credit_days: 15,
    special_discount_percent: 0,
    special_discount_amount: 0,
    tcs_percent: 0,
    tcs_percent_amount: 0,
    tcs_amount: '0.00',
    actual_tcs_amount: '0.00',
    irn: '',
    product_total: 0,
    actual_product_total: 0,
    discount_total: '0.00',
    actual_discount_total: 0,
    freight_charges: '0.00',
    cgst_total: '0.00',
    actual_cgst_total: '0.00',
    sgst_total: '0.00',
    actual_sgst_total: '0.00',
    igst_total: '0.00',
    actual_igst_total: '0.00',
    round_off_total: 0,
    actual_round_off_total: 0,
    grand_total: '0.00',
    actual_grand_total: '0.00',
    additions: '0.00',
    deductions: '0.00',
    merchandiser_name: '',
    qty_total: 0,
    undeducted_product_total: 0,
    actual_undeducted_product_total: 0,
    include_freight_in_tax: false,
    include_additions_in_tax: false,
    include_deductions_in_tax: false,
  }

  invoiceDetailsList: InvoiceDetails[] = [];
  unChangedInvoiceDetailsList: InvoiceDetails[] = [{
    inv_byno: '',
    byno_serial: 0,
    product_group_id: 0,
    product_group_name: '',
    product_group_uom: '',
    product_qty: 0,
    actual_product_qty: 0,
    no_of_pcs: 0,
    actual_no_of_pcs: 0,
    cost_price: 0,
    actual_cost_price: 0,
    discount_amount: 0,
    discount_percent: 0,
    discount_percent_amt: 0,
    price_code: '',
    add_margin: '',
    supplier_desc: '',
    dry_wash: false,
    mrp: 0,
    hsn_code: '',
    sgst: 0,
    actual_sgst: 0,
    sgst_amount: 0,
    actual_sgst_amount: 0,
    cgst: 0,
    actual_cgst: 0,
    cgst_amount: 0,
    actual_cgst_amount: 0,
    igst: 0,
    actual_igst: 0,
    igst_amount: 0,
    actual_igst_amount: 0,
    tax_percentage: 0,
    round_off: 0,
    actual_round_off: 0,
    total_taxable_value: '0.00',
    actual_total_taxable_value: '0.00',
    productGroupFocus: false,
    profit_percentage: 0,
    selling_price: 0
  }];

  modifyInvoiceDetailsList: InvoiceDetails[] = [{
    inv_byno: '',
    byno_serial: 0,
    product_group_id: 0,
    product_group_name: '',
    product_group_uom: '',
    product_qty: 0,
    actual_product_qty: 0,
    no_of_pcs: 0,
    actual_no_of_pcs: 0,
    cost_price: 0,
    actual_cost_price: 0,
    discount_amount: 0,
    discount_percent: 0,
    discount_percent_amt: 0,
    price_code: '',
    add_margin: '',
    supplier_desc: '',
    dry_wash: false,
    mrp: 0,
    hsn_code: '',
    sgst: 0,
    actual_sgst: 0,
    sgst_amount: 0,
    actual_sgst_amount: 0,
    cgst: 0,
    actual_cgst: 0,
    cgst_amount: 0,
    actual_cgst_amount: 0,
    igst: 0,
    actual_igst: 0,
    igst_amount: 0,
    actual_igst_amount: 0,
    tax_percentage: 0,
    round_off: 0,
    actual_round_off: 0,
    total_taxable_value: '0.00',
    actual_total_taxable_value: '0.00',
    productGroupFocus: false,
    profit_percentage: 0,
    selling_price: 0
  }];

  objInvoiceDetails: InvoiceDetails = {
    inv_byno: '',
    byno_serial: 0,
    product_group_id: 0,
    product_group_name: '',
    product_group_uom: '',
    product_qty: 0,
    actual_product_qty: 0,
    no_of_pcs: 0,
    actual_no_of_pcs: 0,
    cost_price: 0,
    actual_cost_price: 0,
    discount_amount: 0,
    discount_percent: 0,
    discount_percent_amt: 0,
    price_code: '',
    add_margin: '',
    supplier_desc: '',
    dry_wash: false,
    mrp: 0,
    hsn_code: '',
    sgst: 0,
    actual_sgst: 0,
    sgst_amount: 0,
    actual_sgst_amount: 0,
    cgst: 0,
    actual_cgst: 0,
    cgst_amount: 0,
    actual_cgst_amount: 0,
    igst: 0,
    actual_igst: 0,
    igst_amount: 0,
    actual_igst_amount: 0,
    tax_percentage: 0,
    round_off: 0,
    actual_round_off: 0,
    total_taxable_value: 0,
    actual_total_taxable_value: '0.00',
    productGroupFocus: false,
    profit_percentage: 0,
    selling_price: 0
  }

  myControl = new FormControl();
  filteredOptions: Observable<SupplierDescription[]>;

  columns: any[] = [
    { display: 'sNo', editable: false },
    { display: 'productGroup', editable: false },
    { display: 'productGroupUOM', editable: false },
    { display: "qty", editable: false },
    { display: "actualQty", editable: false },
    { display: "pcs", editable: false },
    { display: "actualPcs", editable: false },
    { display: "costPrice", editable: false },
    { display: "actualCostPrice", editable: true },
    { display: "discountAmt", editable: false },
    { display: "discountPercent", editable: false },
    { display: "priceCode", editable: false },
    { display: "addMargin", editable: false },
    { display: "supplierDesc", editable: false },
    { display: "dryWash", editable: false },
    { display: "mrp", editable: false },
    { display: "hsnCode", editable: false },
    { display: "sgst", editable: false },
    { display: "cgst", editable: false },
    { display: "igst", editable: false },
    { display: "roundOff", editable: false },
    { display: "taxableValue", editable: false },
    { display: "action", editable: false }
  ];

  @ViewChildren('productGroup') productGroup: ElementRef | any;
  @ViewChildren('productGroupUOM') productGroupUOM: ElementRef | any;
  @ViewChildren('qty') qty: ElementRef | any;
  @ViewChildren('actualQty') actualQty: ElementRef | any;
  @ViewChildren('pcs') pcs: ElementRef | any;
  @ViewChildren('actualPcs') actualPcs: ElementRef | any;
  @ViewChildren('costPrice') costPrice: ElementRef | any;
  @ViewChildren('actualCostPrice') actualCostPrice: ElementRef | any;
  @ViewChildren('discountAmt') discountAmt: ElementRef | any;
  @ViewChildren('discountPercent') discountPercent: ElementRef | any;
  @ViewChildren('priceCode') priceCode: ElementRef | any;
  @ViewChildren('addMargin') addMargin: ElementRef | any;
  @ViewChildren('supplierDesc') supplierDesc: ElementRef | any;
  @ViewChildren('dryWash') dryWash: ElementRef | any;
  @ViewChildren('mrp') mrp: ElementRef | any;
  @ViewChildren('hsnCode') hsnCode: ElementRef | any;
  @ViewChildren('sgst') sgst: ElementRef | any;
  @ViewChildren('cgst') cgst: ElementRef | any;
  @ViewChildren('igst') igst: ElementRef | any;
  @ViewChildren('roundOff') roundOff: ElementRef | any;
  @ViewChildren('taxableValue') taxableValue: ElementRef | any;
  @ViewChildren('action') action: ElementRef | any;

  @ViewChildren('newSupplier') newSupplier: ElementRef | any;
  @ViewChildren('supplierInvoiceNo') supplierInvoiceNo: ElementRef | any;
  @ViewChildren('invoiceAmount') invoiceAmount: ElementRef | any;
  @ViewChildren('discountTotal') discountTotal: ElementRef | any;
  @ViewChildren('freightCharges') freightCharges: ElementRef | any;

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    private _datePipe: DatePipe,
    public _matDialog: MatDialog,
    private _decimalPipe: DecimalPipe,

    private _gridKeyEvents: GridKeyEvents,
    private _keyPressEvents: KeyPressEvents,
    private _invoicesService: InvoicesService,
    private _byAmountService: ByAmountService,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    private _confirmationDialog: ConfirmationDialogComponent,
    private _accountsLookupService: AccountsLookupService,
    public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _cfDislogRef: MatDialogRef<EditCostPriceComponent>,
    private _productGroupAttributeService: ProductGroupAttributesService
  ) {
    this.objInvoicesLoad.from_inv_entry_date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
    this.invByno = JSON.parse(JSON.stringify(this._data.byno));
    this.getSupplierLookupList();
    this.getProductGroupLookupList();
    this.fetchInvoices();
    this._cfDislogRef.disableClose = true;
  }

  // ngAfterViewInit(): void {
  //   setTimeout(() => {
  //     let input = this.actualCostPrice.toArray();
  //     input[0].nativeElement.focus();
  //   }, 100);
  // }
  ngOnInit() {

  }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number, isDetailsColumn?: boolean): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        if (this.checkValidDetails(rowIndex, colIndex))
          this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.invoiceDetailsList);
        break;

      case 38: // Arrow Up
        if (this.checkValidDetails(rowIndex, colIndex))
          this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 40: // Arrow Down
        if (this.checkValidDetails(rowIndex, colIndex))
          this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.invoiceDetailsList);
        break;
    }
  }

  private _filter(supplier_description: string): SupplierDescription[] {
    const filterValue = supplier_description.toLowerCase();
    return this.supplierDescList.filter(option => option.supplier_description.toLowerCase().indexOf(filterValue) === 0);
  }

  public displayFn(supplierDescId?: number): string {
    debugger;
    if (supplierDescId) {
      const supplierDesc = this.supplierDescList.find(f => f.supplier_description_id === supplierDescId);
      return supplierDesc && supplierDesc.supplier_description ? supplierDesc.supplier_description : '';
    }
  }

  /*************************************************** CRUD Operations *******************************************************/

  private getSupplierLookupList(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._accountsLookupService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      debugger;
      if (result) {
        this.newSupplierLookupList = JSON.parse(result);
        this.loadSupplierLookupList = JSON.parse(result);
        this.newSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
        this.loadSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
      }
    });
  }

  private getProductGroupLookupList(): void {
    let objDetails: any = {
      GetProductGroupLookup: JSON.stringify([{
        group_section_id: +this._localStorage.getGlobalGroupSectionId()
      }])
    }
    this._productGroupAttributeService.GetProductGroupLookupDetails(objDetails).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0) {
        debugger;
        this.productGroupLookupList = JSON.parse(result);
      }
    });
  }

  private getOtherDetailsBasedOnSupplier(isModify?: boolean): void {
    this.checkIsRegisteredSupplier();
    this.getSupplierDescList();
    this.getInvoiceNos(isModify);
    this.getMasterDiscount(isModify);
    this.getInvoicePoNos(isModify);
    this.setTypeOfGst();
  }

  private checkIsRegisteredSupplier(): void {
    this.flags.isRegistered = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_gstn_no) === -1 ? true : false;
  }

  public setActualQuantity(i: number): void {
    this.invoiceDetailsList[i].actual_product_qty = this.invoiceDetailsList[i].product_qty;
    this.setNoOfPcs(i);
  }

  private setNoOfPcs(i: number): void {
    if (this.invoiceDetailsList[i].product_group_uom === 'No.s') {
      this.invoiceDetailsList[i].no_of_pcs = +this.invoiceDetailsList[i].product_qty;
      this.invoiceDetailsList[i].actual_no_of_pcs = +this.invoiceDetailsList[i].actual_product_qty;
      this.setEditableTrueOrFalse('pcs', false);
    }
    this.calculateTaxableValue(i);
  }

  public setProductFocus(i: number): void {
    this.invoiceDetailsList[i].productGroupFocus = true;
  }

  public resetProductFocus(i: number): void {
    this.invoiceDetailsList[i].productGroupFocus = false;
  }

  public setActualPcs(i: number): void {
    this.invoiceDetailsList[i].actual_no_of_pcs = this.invoiceDetailsList[i].no_of_pcs;
    this.setNoOfQty(i);
  }

  public setNoOfQty(i: number): void {
    if (this.invoiceDetailsList[i].product_group_uom === 'No.s') {
      this.invoiceDetailsList[i].product_qty = +this.invoiceDetailsList[i].no_of_pcs;
      this.invoiceDetailsList[i].actual_product_qty = +this.invoiceDetailsList[i].no_of_pcs;
      this.setEditableTrueOrFalse('qty', false);
    }
    this.calculateTaxableValue(i);
  }

  public setActualCostPrice(i: number): void {
    this.invoiceDetailsList[i].actual_cost_price = this.invoiceDetailsList[i].cost_price;
    this.calculateTaxableValue(i);
  }

  public setTypeOfGst(): void {
    let enteredGst: string = this.objInvoices.entered_gstn_no.substr(0, 2);
    if (+enteredGst === +this._localStorage.getStateCode()) {
      this.flags.isIgst = false;
      this.setEditableTrueOrFalse('sgst', true);
      this.setEditableTrueOrFalse('cgst', true);
      this.setEditableTrueOrFalse('igst', false);
    }
    else {
      this.flags.isIgst = true;
      this.setEditableTrueOrFalse('sgst', false);
      this.setEditableTrueOrFalse('cgst', false);
      this.setEditableTrueOrFalse('igst', true);
    }
    this.resetGstValues();
    this.resetActualGstValues();
  }

  public disableTaxableColumns(): void {
    debugger;
    this.resetGstValues();
    if (this.flags.isComposite) {
      this.setEditableTrueOrFalse('sgst', false);
      this.setEditableTrueOrFalse('cgst', false);
      this.setEditableTrueOrFalse('igst', false);
    } else if (!this.flags.isComposite) {
      this.setEditableTrueOrFalse('sgst', true);
      this.setEditableTrueOrFalse('cgst', true);
      this.setEditableTrueOrFalse('igst', true);
    }
  }

  public calculateTaxableValue(i: number): void {
    debugger;
    let deducted_total: number = this.getDeductedTotal(i);
    let gst_total: number = this.getGstTotal(i, deducted_total);
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(deducted_total) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(gst_total) === -1) {
      this.invoiceDetailsList[i].total_taxable_value = +deducted_total + +gst_total;
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.invoiceDetailsList[i].round_off) === -1) {
        if (this.invoiceDetailsList[i].round_off.toString().includes('.')) {
          if (+(this.invoiceDetailsList[i].round_off.toString().split('.')[1]) > 0) {
            this.invoiceDetailsList[i].total_taxable_value = +this.invoiceDetailsList[i].total_taxable_value + +(this.invoiceDetailsList[i].round_off);
          } else if (+(this.invoiceDetailsList[i].round_off.toString().split('.')[1]) < 0) {
            this.invoiceDetailsList[i].total_taxable_value = +this.invoiceDetailsList[i].total_taxable_value - +(this.invoiceDetailsList[i].round_off);
          } else if (+(this.invoiceDetailsList[i].round_off.toString().split('.')[1]) === 0) {
            this.invoiceDetailsList[i].total_taxable_value = +this.invoiceDetailsList[i].total_taxable_value + 0;
          }
        } else if (+(this.invoiceDetailsList[i].round_off.toString())) {
          this.invoiceDetailsList[i].total_taxable_value = +this.invoiceDetailsList[i].total_taxable_value + +(this.invoiceDetailsList[i].round_off);
        }
      }
      this.getQuantityTotal();
      this.getUnDeductedProductTotal();
      this.getGrandProductTotal();
      this.getDiscountGrandTotal();
      if (this.flags.isIgst)
        this.getIgstGrandTotal();
      else if (!this.flags.isIgst) {
        this.getCgstGrandTotal();
        this.getSgstGrandTotal();
      }
      this.getGrandTotal();
      this.convertToDecimal(i);
    }
  }

  private getDeductedTotal(i: number): number {
    debugger;
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.invoiceDetailsList[i].cost_price) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].discount_amount) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].discount_percent) === -1) {
      let total: number = +((+this.invoiceDetailsList[i].product_qty * +this.invoiceDetailsList[i].cost_price) - (+this.invoiceDetailsList[i].product_qty * +this.invoiceDetailsList[i].discount_amount));
      this.invoiceDetailsList[i].discount_percent_amt = +total * (+this.invoiceDetailsList[i].discount_percent / 100);
      return +(+total - +this.invoiceDetailsList[i].discount_percent_amt);
    } else return 0;
  }

  private getGstTotal(i: number, deductedTotal?: number): number {
    debugger;
    if (this.flags.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].igst) === -1) {
        this.invoiceDetailsList[i].igst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.invoiceDetailsList[i].igst) === -1 ? deductedTotal * (+this.invoiceDetailsList[i].igst / 100) : 0;
        return +this.invoiceDetailsList[i].igst_amount;
      }
    } else if (!this.flags.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].cgst) === -1 &&
        [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].sgst) === -1) {
        this.invoiceDetailsList[i].cgst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.invoiceDetailsList[i].cgst) === -1 ? deductedTotal * (+this.invoiceDetailsList[i].cgst / 100) : 0;
        this.invoiceDetailsList[i].sgst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.invoiceDetailsList[i].sgst) === -1 ? deductedTotal * (+this.invoiceDetailsList[i].sgst / 100) : 0;
        return +(+this.invoiceDetailsList[i].cgst_amount + +this.invoiceDetailsList[i].sgst_amount);
      }
    }
  }

  private getGrandProductTotal(): void {
    this.objInvoices.product_total = this.invoiceDetailsList.map(t => (((+t.product_qty * +t.cost_price) - (+t.product_qty * +t.discount_amount)) - +t.discount_percent_amt)).reduce((acc, value) => acc + value, 0);
  }

  private getIgstGrandTotal(): void {
    this.objInvoices.igst_total = 0;
    this.objInvoices.igst_total = this.invoiceDetailsList.map(t => +t.igst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getCgstGrandTotal(): void {
    this.objInvoices.cgst_total = 0;
    this.objInvoices.cgst_total = this.invoiceDetailsList.map(t => +t.cgst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getSgstGrandTotal(): void {
    this.objInvoices.sgst_total = 0;
    this.objInvoices.sgst_total = this.invoiceDetailsList.map(t => +t.sgst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getDiscountGrandTotal(): void {
    this.objInvoices.discount_total = this.invoiceDetailsList.map(t => ((+t.product_qty * +t.discount_amount) + (+t.discount_percent_amt))).reduce((acc, value) => acc + value, 0);
  }

  public getQuantityTotal(): void {
    this.objInvoices.qty_total = this.invoiceDetailsList.map(t => (+t.no_of_pcs)).reduce((acc, value) => acc + value, 0);
  }

  public getUnDeductedProductTotal(): void {
    this.objInvoices.undeducted_product_total = this.invoiceDetailsList.map(t => (+t.product_qty * +t.cost_price)).reduce((acc, value) => acc + value, 0);
  }

  public getGrandTotal(isModify?: boolean): number {
    // debugger;
    // if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.product_total) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.discount_total) === -1) {
    //   let tempTotal: number = (Number(this.objInvoices.product_total) - Number(this.objInvoices.discount_total));
    //   if (this.flags.isIgst) {
    //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.igst_total) === -1)
    //       this.objInvoices.grand_total = +tempTotal + +this.objInvoices.igst_total;
    //     else this.objInvoices.grand_total = +tempTotal + 0;
    //   }
    //   else if (!this.flags.isIgst) {
    //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.cgst_total) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.sgst_total) === -1)
    //       this.objInvoices.grand_total = tempTotal + +this.objInvoices.cgst_total + +this.objInvoices.sgst_total;
    //     else this.objInvoices.grand_total = +tempTotal + 0;
    //   }
    let grandTotal: number = this.invoiceDetailsList.map(t => (+t.total_taxable_value)).reduce((acc, value) => acc + value, 0);
    this.objInvoices.grand_total = (+grandTotal + +this.objInvoices.freight_charges + +this.objInvoices.additions) - +this.objInvoices.deductions + +this.objInvoices.tcs_amount;
    if (!isModify)
      this.calculateTcsAmountByTcsPercent();

    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.round_off_total) === -1) {
      if (this.objInvoices.round_off_total.toString().includes('.')) {
        if (+(this.objInvoices.round_off_total.toString().split('.')[1]) > 0) {
          this.objInvoices.grand_total = +this.objInvoices.grand_total + +(this.objInvoices.round_off_total);
        } else if (+(this.objInvoices.round_off_total.toString().split('.')[1]) < 0) {
          this.objInvoices.grand_total = +this.objInvoices.grand_total - +(this.objInvoices.round_off_total);
        } else if (+(this.objInvoices.round_off_total.toString().split('.')[1]) === 0) {
          this.objInvoices.grand_total = +this.objInvoices.grand_total + 0;
        }
      } else if (+(this.objInvoices.round_off_total.toString())) {
        this.objInvoices.grand_total = +this.objInvoices.grand_total + +(this.objInvoices.round_off_total);
      }
    }
    return +this.objInvoices.grand_total;
    // } else return 0;
  }

  public calculateTcsAmountByTcsPercent(): void {
    debugger;
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN'].indexOf(+this.objInvoices.tcs_percent) === -1) {
      let tcsAmount: number = (+this.objInvoices.grand_total * (+this.objInvoices.tcs_percent / 100));
      // this.objInvoices.grand_total = +this.objInvoices.grand_total + +tcsAmount;
      this.objInvoices.tcs_amount = +tcsAmount;
      this.calculateAdditionsAndDeductions('');
    }
  }

  public calculateAdditionsAndDeductions(editableColumn: string): void {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.objInvoices.tcs_amount) === -1)
      this.objInvoices.grand_total = +this.objInvoices.product_total + +this.getAdditionsAndDeductionsTotal() + this.getGstTotals();
    this.convertTotalsToDecimal(editableColumn);
  }


  private getGstTotals(): number {
    return +this.objInvoices.cgst_total + +this.objInvoices.sgst_total + +this.objInvoices.igst_total;
  }


  /********************************************************* Actual Calculations ************************************************/

  public calculateActualTaxableValue(i: number): void {
    debugger;
    let deducted_total: number = this.getActualDeductedTotal(i);
    let gst_total: number = this.getActualGstTotal(i, deducted_total);
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(deducted_total) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(gst_total) === -1) {
      this.invoiceDetailsList[i].actual_total_taxable_value = +deducted_total + +gst_total;
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.invoiceDetailsList[i].actual_round_off) === -1) {
        if (this.invoiceDetailsList[i].actual_round_off.toString().includes('.')) {
          if (+(this.invoiceDetailsList[i].actual_round_off.toString().split('.')[1]) > 0) {
            this.invoiceDetailsList[i].actual_total_taxable_value = +this.invoiceDetailsList[i].actual_total_taxable_value + +(this.invoiceDetailsList[i].actual_round_off);
          } else if (+(this.invoiceDetailsList[i].actual_round_off.toString().split('.')[1]) < 0) {
            this.invoiceDetailsList[i].actual_total_taxable_value = +this.invoiceDetailsList[i].actual_total_taxable_value - +(this.invoiceDetailsList[i].actual_round_off);
          } else if (+(this.invoiceDetailsList[i].actual_round_off.toString().split('.')[1]) === 0) {
            this.invoiceDetailsList[i].actual_total_taxable_value = +this.invoiceDetailsList[i].actual_total_taxable_value + 0;
          }
        } else if (+(this.invoiceDetailsList[i].round_off.toString())) {
          this.invoiceDetailsList[i].actual_total_taxable_value = +this.invoiceDetailsList[i].actual_total_taxable_value + +(this.invoiceDetailsList[i].actual_round_off);
        }
      }
      this.getActualQuantityTotal();
      this.getActualUnDeductedProductTotal();
      this.getActualGrandProductTotal();
      this.getActualDiscountGrandTotal();
      if (this.flags.isIgst)
        this.getActualIgstGrandTotal();
      else if (!this.flags.isIgst) {
        this.getActualCgstGrandTotal();
        this.getActualSgstGrandTotal();
      }
      this.getActualGrandTotal();
      this.convertActualsToDecimal(i);
    }
  }

  private getActualDeductedTotal(i: number): number {
    debugger;
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.invoiceDetailsList[i].actual_cost_price) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].discount_amount) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].discount_percent) === -1) {
      let total: number = +((+this.invoiceDetailsList[i].actual_product_qty * +this.invoiceDetailsList[i].actual_cost_price) - (+this.invoiceDetailsList[i].actual_product_qty * +this.invoiceDetailsList[i].discount_amount));
      this.invoiceDetailsList[i].discount_percent_amt = +total * (+this.invoiceDetailsList[i].discount_percent / 100);
      return +(+total - +this.invoiceDetailsList[i].discount_percent_amt);
    } else return 0;
  }

  private getActualGstTotal(i: number, deductedTotal?: number): number {
    debugger;
    if (this.flags.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].actual_igst) === -1) {
        this.invoiceDetailsList[i].actual_igst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.invoiceDetailsList[i].actual_igst) === -1 ? deductedTotal * (+this.invoiceDetailsList[i].actual_igst / 100) : 0;
        return +this.invoiceDetailsList[i].actual_igst_amount;
      }
    } else if (!this.flags.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].actual_cgst) === -1 &&
        [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].actual_sgst) === -1) {
        this.invoiceDetailsList[i].actual_cgst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.invoiceDetailsList[i].actual_cgst) === -1 ? deductedTotal * (+this.invoiceDetailsList[i].actual_cgst / 100) : 0;
        this.invoiceDetailsList[i].actual_sgst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.invoiceDetailsList[i].actual_sgst) === -1 ? deductedTotal * (+this.invoiceDetailsList[i].actual_sgst / 100) : 0;
        return +(+this.invoiceDetailsList[i].actual_cgst_amount + +this.invoiceDetailsList[i].actual_sgst_amount);
      }
    }
  }

  private getActualGrandProductTotal(): void {
    this.objInvoices.actual_product_total = this.invoiceDetailsList.map(t => (((+t.actual_product_qty * +t.actual_cost_price) - (+t.actual_product_qty * +t.discount_amount)) - +t.discount_percent_amt)).reduce((acc, value) => acc + value, 0);
  }

  private getActualIgstGrandTotal(): void {
    this.objInvoices.actual_igst_total = 0;
    this.objInvoices.actual_igst_total = this.invoiceDetailsList.map(t => +t.actual_igst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getActualCgstGrandTotal(): void {
    this.objInvoices.actual_cgst_total = 0;
    this.objInvoices.actual_cgst_total = this.invoiceDetailsList.map(t => +t.actual_cgst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getActualSgstGrandTotal(): void {
    this.objInvoices.actual_sgst_total = 0;
    this.objInvoices.actual_sgst_total = this.invoiceDetailsList.map(t => +t.actual_sgst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getActualDiscountGrandTotal(): void {
    this.objInvoices.actual_discount_total = this.invoiceDetailsList.map(t => ((+t.actual_product_qty * +t.discount_amount) + (+t.discount_percent_amt))).reduce((acc, value) => acc + value, 0);
  }

  public getActualQuantityTotal(): void {
    this.objInvoices.qty_total = this.invoiceDetailsList.map(t => (+t.actual_no_of_pcs)).reduce((acc, value) => acc + value, 0);
  }

  public getActualUnDeductedProductTotal(): void {
    this.objInvoices.actual_undeducted_product_total = this.invoiceDetailsList.map(t => (+t.actual_product_qty * +t.actual_cost_price)).reduce((acc, value) => acc + value, 0);
  }

  public getActualGrandTotal(isModify?: boolean): number {
    // debugger;
    // if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.product_total) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.discount_total) === -1) {
    //   let tempTotal: number = (Number(this.objInvoices.product_total) - Number(this.objInvoices.discount_total));
    //   if (this.flags.isIgst) {
    //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.igst_total) === -1)
    //       this.objInvoices.grand_total = +tempTotal + +this.objInvoices.igst_total;
    //     else this.objInvoices.grand_total = +tempTotal + 0;
    //   }
    //   else if (!this.flags.isIgst) {
    //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.cgst_total) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.sgst_total) === -1)
    //       this.objInvoices.grand_total = tempTotal + +this.objInvoices.cgst_total + +this.objInvoices.sgst_total;
    //     else this.objInvoices.grand_total = +tempTotal + 0;
    //   }
    let grandTotal: number = this.invoiceDetailsList.map(t => (+t.actual_total_taxable_value)).reduce((acc, value) => acc + value, 0);
    this.objInvoices.actual_grand_total = (+grandTotal + +this.objInvoices.freight_charges + +this.objInvoices.additions) - +this.objInvoices.deductions + +this.objInvoices.actual_tcs_amount;
    if (!isModify)
      this.calculateActualTcsAmountByTcsPercent();
    else this.convertTotalsToDecimal('');

    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.actual_round_off_total) === -1) {
      if (this.objInvoices.actual_round_off_total.toString().includes('.')) {
        if (+(this.objInvoices.actual_round_off_total.toString().split('.')[1]) > 0) {
          this.objInvoices.actual_grand_total = +this.objInvoices.actual_grand_total + +(this.objInvoices.actual_round_off_total);
        } else if (+(this.objInvoices.actual_round_off_total.toString().split('.')[1]) < 0) {
          this.objInvoices.actual_grand_total = +this.objInvoices.actual_grand_total - +(this.objInvoices.actual_round_off_total);
        } else if (+(this.objInvoices.actual_round_off_total.toString().split('.')[1]) === 0) {
          this.objInvoices.actual_grand_total = +this.objInvoices.actual_grand_total + 0;
        }
      } else if (+(this.objInvoices.actual_round_off_total.toString())) {
        this.objInvoices.actual_grand_total = +this.objInvoices.actual_grand_total + +(this.objInvoices.actual_round_off_total);
      }
    }
    return +this.objInvoices.actual_grand_total;
    // } else return 0;
  }

  private convertActualsToDecimal(i: number): void {
    this.invoiceDetailsList[i].actual_total_taxable_value = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.invoiceDetailsList[i].actual_total_taxable_value.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.invoiceDetailsList[i].actual_total_taxable_value, '1.2-2')).replace(/,/g, '') : "0.00";
    this.convertActualTotalsToDecimal('');
    this.convertCostPriceToDecimal(i, 'ActualCostPrice');
  }


  public calculateActualTcsAmountByTcsPercent(): void {
    debugger;
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN'].indexOf(+this.objInvoices.tcs_percent) === -1) {
      let tcsAmount: number = (+this.objInvoices.actual_grand_total * (+this.objInvoices.tcs_percent / 100));
      // this.objInvoices.grand_total = +this.objInvoices.grand_total + +tcsAmount;
      this.objInvoices.actual_tcs_amount = +tcsAmount;
      this.calculateActualAdditionsAndDeductions('');
    }
  }

  public calculateActualAdditionsAndDeductions(editableColumn: string): void {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.objInvoices.actual_tcs_amount) === -1)
      this.objInvoices.actual_grand_total = +this.objInvoices.actual_product_total + +this.getActualAdditionsAndDeductionsTotal() + this.getActualGstTotals();
    this.convertTotalsToDecimal(editableColumn);
  }

  private getActualGstTotals(): number {
    return +this.objInvoices.actual_cgst_total + +this.objInvoices.actual_sgst_total + +this.objInvoices.actual_igst_total;
  }

  public convertActualTotalsToDecimal(editableColumn: string): void {
    this.objInvoices.actual_product_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.actual_product_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.actual_product_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.invoice_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.invoice_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.invoice_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.actual_discount_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.actual_discount_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.actual_discount_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.actual_cgst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.actual_cgst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.actual_cgst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.actual_sgst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.actual_sgst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.actual_sgst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.actual_igst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.actual_igst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.actual_igst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.tcs_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.tcs_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.tcs_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.actual_tcs_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.actual_tcs_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.actual_tcs_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.actual_round_off_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.actual_round_off_total) === -1 ? (this._decimalPipe.transform(+this.objInvoices.actual_round_off_total, '1.0-0')).replace(/,/g, '') : "0.00";
  }


  private resetGstValues(): void {
    for (let i = 0; i < this.invoiceDetailsList.length; i++) {
      this.invoiceDetailsList[i].sgst = 0;
      this.invoiceDetailsList[i].sgst_amount = 0;
      this.invoiceDetailsList[i].cgst = 0;
      this.invoiceDetailsList[i].cgst_amount = 0;
      this.invoiceDetailsList[i].igst = 0;
      this.invoiceDetailsList[i].igst_amount = 0;
      this.invoiceDetailsList[i].hsn_code = '';
      this.calculateTaxableValue(i);
    }
  }

  private resetActualGstValues(): void {
    for (let i = 0; i < this.invoiceDetailsList.length; i++) {
      this.invoiceDetailsList[i].actual_sgst = 0;
      this.invoiceDetailsList[i].actual_sgst_amount = 0;
      this.invoiceDetailsList[i].actual_cgst = 0;
      this.invoiceDetailsList[i].actual_cgst_amount = 0;
      this.invoiceDetailsList[i].actual_igst = 0;
      this.invoiceDetailsList[i].actual_igst_amount = 0;
      this.invoiceDetailsList[i].hsn_code = '';
      this.calculateActualTaxableValue(i);
    }
  }

  private calculateSellingPrice(i: number): void {
    debugger;
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.invoiceDetailsList[i].actual_cost_price) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].discount_amount) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].discount_percent) === -1) {
      let total: number = +(+this.invoiceDetailsList[i].actual_cost_price - +this.invoiceDetailsList[i].discount_amount);
      let percentAmt: number = +total * (+this.invoiceDetailsList[i].discount_percent / 100);
      let taxableValue: number = +(+total - +percentAmt);
      this.invoiceDetailsList[i].selling_price = Math.round(+taxableValue + (+taxableValue * (+this.invoiceDetailsList[i].profit_percentage / 100)));
    }
  }

  public getActualAdditionsAndDeductionsTotal(): number {
    return (+this.objInvoices.freight_charges) + (+this.objInvoices.additions) - (+this.objInvoices.deductions) + (+this.objInvoices.actual_tcs_amount);
  }
  public getAdditionsAndDeductionsTotal(): number {
    return (+this.objInvoices.freight_charges) + (+this.objInvoices.additions) - (+this.objInvoices.deductions) + (+this.objInvoices.tcs_amount);
  }

  private convertToDecimal(i: number): void {
    this.invoiceDetailsList[i].total_taxable_value = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.invoiceDetailsList[i].total_taxable_value.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.invoiceDetailsList[i].total_taxable_value, '1.2-2')).replace(/,/g, '') : "0.00";
    this.convertTotalsToDecimal('');
    this.convertCostPriceToDecimal(i, 'ActualCostPrice');
  }

  public convertCostPriceToDecimal(i: number, editableColumn: string): void {
    if (editableColumn !== 'ActualCostPrice')
      this.invoiceDetailsList[i].actual_cost_price = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.invoiceDetailsList[i].actual_cost_price.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.invoiceDetailsList[i].actual_cost_price, '1.2-2')).replace(/,/g, '') : "0.00";
  }

  public convertTotalsToDecimal(editableColumn: string): void {
    this.objInvoices.product_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.product_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.product_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.invoice_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.invoice_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.invoice_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.discount_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.discount_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.discount_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.cgst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.cgst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.cgst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.sgst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.sgst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.sgst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.igst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.igst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.igst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.freight_charges = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.freight_charges.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.freight_charges, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.additions = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.additions.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.additions, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.deductions = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.deductions.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.deductions, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.tcs_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.tcs_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.tcs_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.tcs_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.tcs_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.tcs_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objInvoices.round_off_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.round_off_total) === -1 ? (this._decimalPipe.transform(+this.objInvoices.round_off_total, '1.0-0')).replace(/,/g, '') : "0.00";
  }

  private getSupplierDescList(): void {
    this.supplierDescList = [];
    let objDesc: any = {
      ProductDesc: JSON.stringify([{
        account_code: this.objInvoices.supplier_code
      }])
    }
    this._invoicesService.getSupplierDescription(objDesc).subscribe((result: any) => {
      debugger;
      if (result) {
        this.supplierDescList = JSON.parse(JSON.stringify(result));
        this.filteredOptions = this.myControl.valueChanges
          .pipe(startWith(''), map(value => typeof value === 'string' ? value : value.supplier_description),
            map(name => name ? this._filter(name) : this.supplierDescList.slice()));
      }
    });
  }

  public getInvoiceNos(isModify?: boolean): void {
    this.invoiceNosList = [];
    this.objInvoices.supplier_invoice_no = '';
    this.objInvoices.supplier_invoice_date = new Date();
    this.objInvoices.grn_no = 0;
    let objInvoice: any = {
      InvoiceNos: JSON.stringify([{
        supplier_code: this.objInvoices.supplier_code.toString().trim(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        warehouse_id: +this._localStorage.getGlobalWarehouseId()
      }])
    }
    this._invoicesService.getInvoiceNos(objInvoice).subscribe((result: any) => {
      if (result) {
        // console.log(result, 'Invoice Nos list');
        this.invoiceNosList = JSON.parse(JSON.stringify(result));
        if (isModify)
          this.getInvoiceNoDetails();
      }
    });
  }

  public getInvoiceNoDetails(): void {
    debugger;
    this.objInvoices.supplier_invoice_date = this.invoiceNosList[this.invoiceNosList.findIndex(x => x.invoice_no.toString().trim() === this.objInvoices.supplier_invoice_no.toString().trim())].invoice_date;
    this.objInvoices.grn_no = this.invoiceNosList[this.invoiceNosList.findIndex(x => x.invoice_no.toString().trim() === this.objInvoices.supplier_invoice_no.toString().trim())].grn_no;
    this.objModifyInvoices.supplier_invoice_date = this.invoiceNosList[this.invoiceNosList.findIndex(x => x.invoice_no.toString().trim() === this.objInvoices.supplier_invoice_no.toString().trim())].invoice_date;
    this.objModifyInvoices.grn_no = this.invoiceNosList[this.invoiceNosList.findIndex(x => x.invoice_no.toString().trim() === this.objInvoices.supplier_invoice_no.toString().trim())].grn_no;
  }

  public getMasterDiscount(isModify?: boolean): void {
    this.objInvoices.master_discount_percent = 0;
    this.objInvoices.master_discount_amount = 0;
    let objInvoice: any = {
      Discount: JSON.stringify([{
        company_id: this._localStorage.getGlobalCompanyId(),
        supplier_code: this.objInvoices.supplier_code.toString().trim()
      }])
    }
    this._invoicesService.getMasterDiscount(objInvoice).subscribe((result: any) => {
      this.objInvoices.master_discount_percent = result ? result[0].v_disc : 0;
      if (isModify)
        this.objModifyInvoices.master_discount_percent = result ? result[0].v_disc : 0;
    });
  }

  public getInvoicePoNos(isModify?: boolean): void {
    this.poNosList = [];
    this.objInvoices.po_id = 0;
    this.objInvoices.merchandiser_name = '';
    let objInvoice: any = {
      PoNos: JSON.stringify([{
        warehouse_id: +this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        supplier_code: this.objInvoices.supplier_code.toString().trim()
      }])
    }
    this._invoicesService.getInvoicePoNos(objInvoice).subscribe((result: any) => {
      // console.log(result, 'PO Nos');
      this.poNosList = result ? JSON.parse(JSON.stringify(result)) : [];
      if (isModify) {
        this.objInvoices.po_id = this.poNosList[this.poNosList.findIndex(x => x.po_no === this.objInvoices.purchase_order_no)].po_id;
        this.objModifyInvoices.po_id = this.poNosList[this.poNosList.findIndex(x => x.po_no === this.objModifyInvoices.purchase_order_no)].po_id;
        this.getSupplierPoDetails();
      }
    });
  }

  public getSupplierPoDetails(): void {
    this.objInvoices.merchandiser_name = this.poNosList[this.poNosList.findIndex(x => +x.po_id === +this.objInvoices.po_id)].merchandiser_name;
    this.objModifyInvoices.merchandiser_name = this.poNosList[this.poNosList.findIndex(x => +x.po_id === +this.objModifyInvoices.po_id)].merchandiser_name;
  }

  public checkIsValidPriceCode(i: number): void {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(this.invoiceDetailsList[i].price_code) === -1) {
      let objPriceCode: any = {
        PriceCode: JSON.stringify([{
          company_section_id: +this._localStorage.getCompanySectionId(),
          price_code: this.invoiceDetailsList[i].price_code.toString().trim().toUpperCase()
        }])
      }
      this._invoicesService.checkPriceCode(objPriceCode).subscribe((result: boolean) => {
        if (result) {
          this.invoiceDetailsList[i].price_code = this.invoiceDetailsList[i].price_code;
          this.getSellingPricePercentage(i);
        } else this.openInvalidAlertDialog('Invalid Price Code', i, 'Edit Cost Price', 'priceCode');
      });
    }
  }

  private getSellingPricePercentage(i: number): void {
    let objGet: any = {
      Margin: JSON.stringify([{
        price_code: this.invoiceDetailsList[i].price_code.toString().trim().toUpperCase(),
        company_section_id: this._localStorage.getCompanySectionId()
      }])
    }
    this._byAmountService.getMarginCalculation(objGet).subscribe((result: any) => {
      if (result) {
        debugger;
        this.invoiceDetailsList[i].price_code = result[0].price_code;
        this.invoiceDetailsList[i].profit_percentage = +result[0].profit_percentage;
        this.calculateSellingPrice(i);
      }
    });
  }

  public getGstByHsnCode(i: number): void {
    if (!this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.invoiceDetailsList[i].hsn_code) === -1) {
      let objGet = {
        GstByHSN: JSON.stringify([{
          supplier_gstn: this.objInvoices.entered_gstn_no,
          company_section_id: this._localStorage.getCompanySectionId(),
          hsn: this.invoiceDetailsList[i].hsn_code,
          supplier_inv_date: this._datePipe.transform(this.objInvoices.supplier_invoice_date, 'dd/MM/yyyy'),
          cost_price: this.getDeductedTotal(i)
        }])
      }
      this._invoicesService.getGstByHSNCode(objGet).subscribe((result: any) => {
        if (result) {
          debugger;
          this.invoiceDetailsList[i].sgst = JSON.parse(JSON.stringify(result))[0].sgst;
          this.invoiceDetailsList[i].cgst = JSON.parse(JSON.stringify(result))[0].cgst;
          this.invoiceDetailsList[i].igst = JSON.parse(JSON.stringify(result))[0].igst;
          this.calculateTaxableValue(i);
        } else
          this.openInvalidAlertDialog('Invalid hsn code', i, 'Edit Cost Price', 'hsnCode');
      });
    }
  }

  public getActualGstByHsnCode(i: number): void {
    if (!this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.invoiceDetailsList[i].hsn_code) === -1) {
      let objGet = {
        GstByHSN: JSON.stringify([{
          supplier_gstn: this.objInvoices.entered_gstn_no,
          company_section_id: this._localStorage.getCompanySectionId(),
          hsn: this.invoiceDetailsList[i].hsn_code,
          supplier_inv_date: this._datePipe.transform(this.objInvoices.supplier_invoice_date, 'dd/MM/yyyy'),
          cost_price: this.getActualDeductedTotal(i)
        }])
      }
      this._invoicesService.getGstByHSNCode(objGet).subscribe((result: any) => {
        if (result) {
          debugger;
          this.invoiceDetailsList[i].actual_sgst = JSON.parse(JSON.stringify(result))[0].sgst;
          this.invoiceDetailsList[i].actual_cgst = JSON.parse(JSON.stringify(result))[0].cgst;
          this.invoiceDetailsList[i].actual_igst = JSON.parse(JSON.stringify(result))[0].igst;
          this.calculateActualTaxableValue(i);
        } else
          this.openInvalidAlertDialog('Invalid hsn code', i, 'Edit Cost Price', 'hsnCode');
      });
    }
  }

  private checkInvoiceEntryConfigs(): void {
    let objCheck = {
      group_section_id: +this._localStorage.getGlobalGroupSectionId()
    }
    this._invoicesService.checkInvoiceEntryConfigs(objCheck).subscribe((result: any) => {
      debugger;
      if (result) {
        this.flags.isDiscountAmount = result[0].grid_discount_amount ? true : false;
        this.flags.isDiscountAmount ? this.setEditableTrueOrFalse('discountAmt', true) : this.setEditableTrueOrFalse('discountAmt', false);
        this.flags.isSupplierDesc = result[0].supplier_description ? true : false;
        this.flags.isSupplierDesc ? this.setEditableTrueOrFalse('supplierDesc', true) : this.setEditableTrueOrFalse('supplierDesc', false);
        this.flags.isDryWash = result[0].dry_wash ? true : false;
        this.flags.isDryWash ? this.setEditableTrueOrFalse('dryWash', true) : this.setEditableTrueOrFalse('dryWash', false);
        this.flags.isMrp = result[0].mrp ? true : false;
        this.flags.isMrp ? this.setEditableTrueOrFalse('mrp', true) : this.setEditableTrueOrFalse('mrp', false);
        this.flags.isAddMargin = result[0].add_margin ? true : false;
        this.flags.isAddMargin ? this.setEditableTrueOrFalse('addMargin', true) : this.setEditableTrueOrFalse('addMargin', false);
        this.modifyFlags = JSON.parse(JSON.stringify(this.flags));
      }
    });
  }

  public editCostPrice(): void {
    debugger;
    if (this.beforeSaveValidate()) {
      let objSave: any = {
        Invoices: JSON.stringify([{
          company_section_id: +this._localStorage.getCompanySectionId(),
          inv_byno: this.objInvoices.inv_byno,
          invoice_actual_amount: +this.objInvoices.grand_total,
          actual_discount_amount: +this.objInvoices.actual_discount_total,
          // discount_amount: +this.objInvoices.discount_total,
          actual_assessable_value: +this.objInvoices.actual_product_total,
          // assessable_value: +this.objInvoices.product_total,
          entered_by: +this._localStorage.intGlobalUserId(),
          actual_igst_tax_amt: +this.objInvoices.actual_igst_total,
          // inv_igst_tax_amt: +this.objInvoices.igst_total,
          actual_cgst_tax_amt: +this.objInvoices.actual_cgst_total,
          // inv_cgst_tax_amt: +this.objInvoices.cgst_total,
          actual_sgst_tax_amt: +this.objInvoices.actual_sgst_total,
          // inv_sgst_tax_amt: +this.objInvoices.sgst_total,
          actual_round_off_amt: +this.objInvoices.actual_round_off_total,
          actual_tcs_amount: +this.objInvoices.actual_tcs_amount,
          // round_off_amt: +this.objInvoices.round_off_total,
          details: this.getInvoiceDetailsToAdd()
        }])
      }
      this._invoicesService.editCostPrice(objSave).subscribe((result: any) => {
        if (result) {
          this._cfDislogRef.disableClose = false;
          this._cfDislogRef.close(true);
        }
      });
    }
  }


  // private afterAddInvoices(result: any): void {
  //   this.objInvoicesLoad.status = 'For Approval';
  //   this.objInvoicesLoad.all_suppliers = false;
  //   this.objInvoicesLoad.supplier_code = this.objInvoices.supplier_code;
  //   this.objInvoicesLoad.supplier_name = this.objInvoices.supplier_name;
  //   this.objInvoicesLoad.inv_byno = JSON.parse(JSON.stringify(result.inv_byno));
  //   this.getInvoices();
  //   this.listClick();
  // }

  private getInvoiceDetailsToAdd(): any[] {
    let tempArr: any[] = [];
    debugger;
    for (let i = 0; i < this.invoiceDetailsList.length; i++) {
      let element: any = this.invoiceDetailsList[i];
      if (this.checkIsValidRowToSave(element)) {
        tempArr.push({
          byno_serial: element.byno_serial,
          actual_cost_price: +element.actual_cost_price,
          actual_round_off_amount: +element.actual_round_off,
          // round_off_amount: +element.round_off,
          actual_igst: +element.actual_igst,
          // igst: +element.igst,
          actual_igst_tax_amount: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+element.actual_igst_amount) === -1 ? +element.actual_igst_amount : 0,
          // igst_tax_amount: +element.igst_amount,
          actual_cgst: +element.actual_cgst,
          // cgst: +element.cgst,
          actual_cgst_tax_amount: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+element.actual_cgst_amount) === -1 ? +element.actual_cgst_amount : 0,
          // cgst_tax_amount: +element.cgst_amount,
          actual_sgst: +element.actual_sgst,
          // sgst: +element.sgst,
          actual_sgst_tax_amount: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+element.actual_sgst_amount) === -1 ? +element.actual_sgst_amount : 0,
          // sgst_tax_amount: +element.sgst_amount
        });
      }
    }
    return tempArr;
  }

  private getSupplierPoNo(): string {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.po_id) === -1)
      return this.poNosList[this.poNosList.findIndex(x => +x.po_id === +this.objInvoices.po_id)].po_no;
  }

  public fetchInvoices(): void {

    let objFetch: any = {
      Invoices: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        inv_byno: this.invByno.toString().trim()
      }])
    }
    this._invoicesService.fetchInvoices(objFetch).subscribe((result: any) => {
      if (result && result.returN_VALUE === 1) {
        let tempObj = JSON.parse(result.inv)[0];
        this.objInvoices = JSON.parse(result.inv)[0];
        this.objModifyInvoices = JSON.parse(result.inv)[0];
        this.getOtherDetailsBasedOnSupplier(true);
        this.invoiceDetailsList = JSON.parse(result.inv_Det);
        this.modifyInvoiceDetailsList = JSON.parse(result.inv_Det);
        this.objInvoices.supplier_invoice_no = JSON.parse(result.inv)[0].supplier_invoice_no;
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_email) !== -1) {
          this.objInvoices.supplier_address = tempObj.supplier_name + '\n' + tempObj.address1 + ' ' + tempObj.address2 + ' ' + tempObj.address3 + '\nGSTIN No : ' + this.objInvoices.supplier_gstn_no;
        } else if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_email) === -1) {
          this.objInvoices.supplier_address = tempObj.supplier_name + '\n' + tempObj.address1 + ' ' + tempObj.address2 + ' ' + tempObj.address3 + '\nEmail : ' + this.objInvoices.supplier_email + '\nGSTIN No : ' + this.objInvoices.supplier_gstn_no;
        }
        this.checkInvoiceEntryConfigs();
        this.setInvoiceEntryDate();
        for (let i = 0; i < this.invoiceDetailsList.length; i++) {
          this.calculateTaxableValue(i);
          this.calculateActualTaxableValue(i);
        }
        this.objInvoices.round_off_total = JSON.parse(JSON.stringify(tempObj.round_off_total));
        this.objInvoices.tcs_amount = JSON.parse(JSON.stringify(tempObj.tcs_amount));
        this.objInvoices.actual_tcs_amount = JSON.parse(JSON.stringify(tempObj.actual_tcs_amount));
        // this.objInvoices.tcs_percent = JSON.parse(JSON.stringify(tempObj.tcs_percent));
        this.getGrandTotal(true);
        this.getActualGrandTotal(true);
        this.objModifyInvoices = JSON.parse(JSON.stringify(this.objInvoices));
        this.modifyInvoiceDetailsList = JSON.parse(JSON.stringify(this.invoiceDetailsList));
      }
    });
  }

  private setInvoiceEntryDate(): void {
    let dateString: any[] = this.objInvoices.invoice_entry_date.toString().split('/');
    this.objInvoices.invoice_entry_date = dateString[2] + '-' + dateString[1] + '-' + dateString[0];
    this.objModifyInvoices.invoice_entry_date = dateString[2] + '-' + dateString[1] + '-' + dateString[0];
  }

  public matTableConfig(tableRecords?: any[]): void {
    tableRecords ? this.dataSource = new MatTableDataSource(tableRecords) : this.dataSource = new MatTableDataSource([]);
    if (this.dataSource.data.length > 0)
      this.dataSource.data.forEach(x => {
        x.Selected = false;
      });
  }

  public searchInvoicesByByno(searchValue: string): void {
    if (this.invoicesList.length > 0) {
      searchValue = searchValue.toString().trim();
      searchValue = searchValue ? searchValue.toString().toLocaleLowerCase() : "";
      let searchString = searchValue;
      let filteredInvoices = this.invoicesList.filter(element =>
        element.inv_byno.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
      this.matTableConfig(filteredInvoices);
    }
  }

  public filterMismatchedInvoices(): void {
    debugger;
    if (this.objInvoicesLoad.is_mismatched_invoices) {
      let mismatchedInvoices: any[] = [];
      this.invoicesList.forEach(x => {
        if (+x.invoice_amount !== +x.invoice_actual_amount) {
          mismatchedInvoices.push(x);
        }
      });
      this.matTableConfig(mismatchedInvoices);
    } else this.matTableConfig(this.invoicesList);
  }

  private setEditableTrueOrFalse(columnName: string, editable: boolean): void {
    debugger;
    let i = this.columns.findIndex(x => x.display === columnName)
    this.columns[i].editable = editable;
  }

  public onClear(exitFlag?: boolean): void {
    if (this.checkAnyChangesMade()) {
      this.openConfirmationDialog(exitFlag ? 'Changes will be lost, are you sure?' : 'Do you want to clear all the fields?', exitFlag);
    } else if (exitFlag)
      this.dialogRefClose();
  }

  public dialogRefClose(): void {
    this._cfDislogRef.disableClose = false;
    this._cfDislogRef.close(null);
  }

  public openAttachFileDialog(): void {
    const dialogRef = this._matDialog.open(SingleFileAttachComponent, {
      width: '53vw',
      panelClass: "custom-dialog-container",
      data: {}
    });
    dialogRef.afterClosed().subscribe((result: any) => {
    });
  }

  public resetInvoiceEntry(): void {
    this.flags = JSON.parse(JSON.stringify(this.modifyFlags));
    this.objInvoices = JSON.parse(JSON.stringify(this.objModifyInvoices));
    this.invoiceDetailsList = JSON.parse(JSON.stringify(this.modifyInvoiceDetailsList));
  }

  /****************************************************** Validations *******************************************************/

  private openAlertDialog(value: string, componentName: string, focus: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        document.getElementById(focus).focus();
      _dialogRef = null;
    });
  }

  private openInvalidAlertDialog(value: string, i: number, componentName: string, focus: any) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this[focus].toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  public onlyAllwDecimalForInvoiceDetails(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public onlyAllwNumbersForInvoiceDetails(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    }
  }

  public onlyAllowRoundOffDecimalForInvoiceDetails(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number) {
    debugger;
    if (event.keyCode !== 45) {
      let key = key1 + '[' + index + ']' + '.' + key2;
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key] = "";
          event.target.value = "";
        }
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      } else {
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      }
    } else {
      let key = key1 + '[' + index + ']' + '.' + key2;
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key] = "";
          event.target.value = "";
        }
      }
    }
  }

  public onlyAllowRoundOffDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string) {
    debugger;
    if (event.keyCode !== 45) {
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      } else {
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      }
    } else {
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
      }
    }
  }

  public onlyAllwDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    // let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public onlyAllowNumbers(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    // let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    }
  }

  public checkIsValidGstNo(): void {
    if (!this.flags.isComposite) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_gstn_no) === -1 &&
        [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.entered_gstn_no) === -1)
        if (this.objInvoices.entered_gstn_no.toString().trim() !== this.objInvoices.supplier_gstn_no.toString().trim()) {
          this.openAlertDialog('Invalid GSTIN No', 'Edit Cost Price', 'gstnNo');
        }
    }
  }

  private checkIsValidRowToSave(element: any): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.product_group_id) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.product_qty) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.no_of_pcs) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.cost_price) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.hsn_code) === -1)
      return true;
    else return false;
  }

  public beforeSaveValidate(): boolean {
    if ([null, 'null', undefined, 'undfefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_code) !== -1) {
      this.openAlertDialog('Select supplier', 'Edit Cost Price', 'newSupplier');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_code) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_name) !== -1) {
      this.openAlertDialog('Invalid supplier', 'Edit Cost Price', 'newSupplier');
      return false;
    } if (!this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.entered_gstn_no) !== -1) {
      this.openAlertDialog('Enter supplier gstn no', 'Edit Cost Price', 'gstnNo');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_invoice_no) !== -1) {
      this.openAlertDialog('Select supplier invoice no', 'Edit Cost Price', 'supplierInvoiceNo');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.invoice_amount) !== -1) {
      this.openAlertDialog('Enter invoice amount', 'Edit Cost Price', 'invoiceAmount');
      return false;
    } if (!this.beforeSaveValidateInvoiceDetails()) {
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.qty_total) !== -1) {
      this._confirmationDialog.openAlertDialog('Enter atleast 1 number Of pieces', 'Edit Cost Price');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.undeducted_product_total) !== -1) {
      this._confirmationDialog.openAlertDialog('Product total cannot be 0', 'Edit Cost Price');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.product_total) !== -1) {
      this._confirmationDialog.openAlertDialog('Taxable value cannot be 0', 'Edit Cost Price');
      return false;
    } if (!this.flags.isIgst && !this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.cgst_total) !== -1) {
      this._confirmationDialog.openAlertDialog('Cgst cannot be 0', 'Edit Cost Price');
      return false;
    } if (!this.flags.isIgst && !this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.sgst_total) !== -1) {
      this._confirmationDialog.openAlertDialog('Sgst cannot be 0', 'Edit Cost Price');
      return false;
    } if (this.flags.isIgst && !this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.igst_total) !== -1) {
      this._confirmationDialog.openAlertDialog('Igst cannot be 0', 'Edit Cost Price');
      return false;
    } else return true;
  }

  private beforeSaveValidateInvoiceDetails(): boolean {
    for (let i = 0; i < this.invoiceDetailsList.length; i++) {
      let element: any = this.invoiceDetailsList[i];
      if (this.invoiceDetailsList.length === 1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.product_group_id) !== -1) {
        this._confirmationDialog.openAlertDialog('No records to save', 'Edit Cost Price');
        return false;
      } if (!this.validateProductGroupName(i)) {
        this.openInvalidAlertDialog('Invalid product group name', i, 'Edit Cost Price', 'productGroup');
        return false;
      } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.product_qty) !== -1) {
        this.openInvalidAlertDialog('Enter quantity', i, 'Edit Cost Price', 'qty');
        return false;
      } if (+element.product_qty < 0) {
        this.openInvalidAlertDialog('Invalid quantity', i, 'Edit Cost Price', 'qty');
        return false;
      } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.no_of_pcs) !== -1) {
        this.openInvalidAlertDialog('Enter pieces', i, 'Edit Cost Price', 'pcs');
        return false;
      } if (+element.no_of_pcs < 0) {
        this.openInvalidAlertDialog('Invalid pieces', i, 'Edit Cost Price', 'pcs');
        return false;
      } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.actual_cost_price) !== -1) {
        this.openInvalidAlertDialog('Enter cost price', i, 'Edit Cost Price', 'costPrice');
        return false;
      } if (+element.actual_cost_price < 0) {
        this.openInvalidAlertDialog('Invalid cost price', i, 'Edit Cost Price', 'costPrice');
        return false;
      } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.discount_amount) === -1 && +element.discount_amount < 0) {
        this.openInvalidAlertDialog('Invalid discount amount', i, 'Edit Cost Price', 'discountAmt');
        return false;
      } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.discount_percent) === -1 && +element.discount_percent < 0) {
        this.openInvalidAlertDialog('Invalid discount percentage', i, 'Edit Cost Price', 'discountPercent');
        return false;
      } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.price_code.toString().trim()) !== -1) {
        this.openInvalidAlertDialog('Enter price code', i, 'Edit Cost Price', 'priceCode');
        return false;
      } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.hsn_code.toString().trim()) !== -1) {
        this.openInvalidAlertDialog('Enter hsn code', i, 'Edit Cost Price', 'hsnCode');
        return false;
      } if (+element.hsn_code < 0) {
        this.openInvalidAlertDialog('Invalid hsn code', i, 'Edit Cost Price', 'hsnCode');
        return false;
      } if (!this.flags.isComposite && this.flags.isIgst && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.igst.toString().trim()) !== -1) {
        this.openInvalidAlertDialog('Enter igst', i, 'Edit Cost Price', 'igst');
        return false;
      } if (!this.flags.isComposite && this.flags.isIgst && +element.igst < 0) {
        this.openInvalidAlertDialog('Invalid igst', i, 'Edit Cost Price', 'igst');
        return false;
      } if (!this.flags.isComposite && !this.flags.isIgst && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.cgst.toString().trim()) !== -1) {
        this.openInvalidAlertDialog('Enter cgst', i, 'Edit Cost Price', 'cgst');
        return false;
      } if (!this.flags.isComposite && !this.flags.isIgst && +element.cgst < 0) {
        this.openInvalidAlertDialog('Invalid cgst', i, 'Edit Cost Price', 'cgst');
        return false;
      } if (!this.flags.isComposite && !this.flags.isIgst && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.sgst.toString().trim()) !== -1) {
        this.openInvalidAlertDialog('Enter sgst', i, 'Edit Cost Price', 'sgst');
        return false;
      } if (!this.flags.isComposite && !this.flags.isIgst && +element.sgst < 0) {
        this.openInvalidAlertDialog('Invalid sgst', i, 'Edit Cost Price', 'sgst');
        return false;
      }
    } return true;
  }

  private checkValidDetails(rowIndex: number, colIndex: number): boolean {
    debugger;
    if (this.columns[colIndex].display === 'productGroup') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(this.invoiceDetailsList[rowIndex].product_group_name) !== -1) {
        this.openInvalidAlertDialog('Enter product group', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'qty') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].product_qty) !== -1) {
        this.openInvalidAlertDialog('Enter quantity', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.invoiceDetailsList[rowIndex].product_group_uom === 'Mt.s' && this.columns[colIndex].display === 'pcs') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].no_of_pcs) !== -1) {
        this.openInvalidAlertDialog('Enter pieces', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'costPrice') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].cost_price) !== -1) {
        this.openInvalidAlertDialog('Enter cost price', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } if (+this.invoiceDetailsList[rowIndex].cost_price < 0) {
        this.openInvalidAlertDialog('Invalid cost price', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'actualCostPrice') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].actual_cost_price) !== -1) {
        this.openInvalidAlertDialog('Enter actual cost price', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } if (+this.invoiceDetailsList[rowIndex].actual_cost_price < 0) {
        this.openInvalidAlertDialog('Invalid actual cost price', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'discountAmt') {
      if (+this.invoiceDetailsList[rowIndex].discount_amount < 0) {
        this.openInvalidAlertDialog('Invalid discount amount', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'discountPercent') {
      if (+this.invoiceDetailsList[rowIndex].discount_amount < 0) {
        this.openInvalidAlertDialog('Invalid discount percent', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'priceCode') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].price_code) !== -1) {
        this.openInvalidAlertDialog('Enter price code', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'hsnCode') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].hsn_code) !== -1) {
        this.openInvalidAlertDialog('Enter hsn code', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (!this.flags.isComposite && this.flags.isIgst && this.columns[colIndex].display === 'igst') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].igst) !== -1) {
        this.openInvalidAlertDialog('Enter igst', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } if (+this.invoiceDetailsList[rowIndex].igst < 0) {
        this.openInvalidAlertDialog('Invalid igst', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (!this.flags.isComposite && !this.flags.isIgst && this.columns[colIndex].display === 'sgst') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].sgst) !== -1) {
        this.openInvalidAlertDialog('Enter sgst', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } if (+this.invoiceDetailsList[rowIndex].sgst < 0) {
        this.openInvalidAlertDialog('Invalid sgst', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (!this.flags.isComposite && !this.flags.isIgst && this.columns[colIndex].display === 'cgst') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].cgst) !== -1) {
        this.openInvalidAlertDialog('Enter cgst', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } if (+this.invoiceDetailsList[rowIndex].cgst < 0) {
        this.openInvalidAlertDialog('Invalid cgst', rowIndex, 'Edit Cost Price', this.columns[colIndex].display);
        return false;
      } else return true;
    } else return true;
  }

  public validateProductGroupName(i: number): boolean {
    let index = this.productGroupLookupList.findIndex(element => element.product_group_name.toString().trim() === this.invoiceDetailsList[i].product_group_name.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public checkIsValidProductQty(i: number): void {
    debugger;
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.invoiceDetailsList[i].product_qty) !== -1)
      this.openInvalidAlertDialog('Enter quantity', i, 'Edit Cost Price', 'qty');
  }

  public checkIsValidCostPrice(i: number): void {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.invoiceDetailsList[i].actual_cost_price) !== -1)
      this.openInvalidAlertDialog('Enter cost price', i, 'Edit Cost Price', 'costPrice');
    if (+ this.invoiceDetailsList[i].actual_cost_price < 0)
      this.openInvalidAlertDialog('Invalid cost price', i, 'Edit Cost Price', 'costPrice');
  }

  public checkIsValidHsnCode(i: number): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.invoiceDetailsList[i].hsn_code) !== -1) {
      this.openInvalidAlertDialog('Enter hsn code', i, 'Edit Cost Price', 'hsnCode');
      return false;
    } if (+this.invoiceDetailsList[i].hsn_code < 0) {
      this.openInvalidAlertDialog('Invalid hsn code', i, 'Edit Cost Price', 'hsnCode');
      return false;
    } else return true;
  }

  public onlyAllowUpperCases(event: KeyboardEvent | any): void {
    if (event.keyCode >= 65 && event.keyCode <= 90)
      event.target.value = event.target.value.toString().trim().toUpperCase();
  }

  public resetSupplierLookupOnLoad(): void {
    this.objInvoicesLoad.supplier_code = '';
    this.objInvoicesLoad.supplier_name = '';
    this.matTableConfig();
  }

  private checkAnyChangesMade(): boolean {
    if (this.checkChangesInDetails() || this.checkChangesInFlags())
      return true;
    else return false;
  }

  private checkChangesInDetails(): boolean {
    debugger;
    if (JSON.stringify(this.invoiceDetailsList) !== (JSON.stringify(this.modifyInvoiceDetailsList)))
      return true;
    else return false;
  }

  private checkChangesInFlags(): boolean {
    debugger;
    if (JSON.stringify(this.flags) !== (JSON.stringify(this.modifyFlags)))
      return true;
    else return false;
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Invoices";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.dialogRefClose() : this.resetInvoiceEntry();
      dialogRef = null;
    });
  }
}
