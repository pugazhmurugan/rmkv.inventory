import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCostPriceComponent } from './edit-cost-price.component';

describe('EditCostPriceComponent', () => {
  let component: EditCostPriceComponent;
  let fixture: ComponentFixture<EditCostPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCostPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCostPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
