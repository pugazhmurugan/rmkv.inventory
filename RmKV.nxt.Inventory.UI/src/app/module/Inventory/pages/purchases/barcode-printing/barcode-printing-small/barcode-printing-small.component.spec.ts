import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarcodePrintingSmallComponent } from './barcode-printing-small.component';

describe('BarcodePrintingSmallComponent', () => {
  let component: BarcodePrintingSmallComponent;
  let fixture: ComponentFixture<BarcodePrintingSmallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarcodePrintingSmallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarcodePrintingSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
