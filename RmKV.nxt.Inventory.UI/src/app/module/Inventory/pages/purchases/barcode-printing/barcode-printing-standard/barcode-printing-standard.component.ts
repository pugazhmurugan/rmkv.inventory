import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { BaarcodePrintingService } from 'src/app/module/Inventory/services/purchases/bar-code-printing/baarcode-printing.service';

@Component({
  selector: 'app-barcode-printing-standard',
  templateUrl: './barcode-printing-standard.component.html',
  styleUrls: ['./barcode-printing-standard.component.scss']
})
export class BarcodePrintingStandardComponent implements OnInit {
  masterSelected: boolean;
  Caption: any;
  NoOfCopy: any;
  barcodePrintingList: any = []
  SelectbarcodePrintingList: any = [];
  objBarcodeprinting: any = {
    inv_byno: ""
  }
  

  constructor(public _router: Router,
    private _confirmationDialog: ConfirmationDialogComponent,
    public _localStorage: LocalStorage, public _baarcodePrintingService: BaarcodePrintingService,) {
    this.masterSelected = false;
  }

  ngOnInit() {
  }

  public getBarCodePrinting(): void {
    if (this.beforeLoadValidate()) {
      let objBarCode = {
        BarCodePrinting: JSON.stringify([{
          company_section_id: +this._localStorage.getCompanySectionId(),
          inv_byno: this.objBarcodeprinting.inv_byno,
        }])
      }
      this._baarcodePrintingService.getBarcodePrinting(objBarCode).subscribe((result: string) => {
        debugger;
        if (result) {
          this.barcodePrintingList = result;
          console.log(result,"barcodePrintingList")
          for (let i = 0; i < this.barcodePrintingList.length; i++) {
            this.barcodePrintingList[i].isSelected = this.masterSelected;
          }        
        }
        else
          this._confirmationDialog.openAlertDialog('No records found', 'Barcode Printing');
      });
    }
  }
  //****************************************** BarCode Printing (Standard)********************************************** 
  
  public selectBarCodePrinting(): void {
    debugger;
    if (this.beforeSelectBrcodePrintValidate()) {
      let objBarCode = {
        BarCodePrinting: JSON.stringify(this.SelectbarcodePrintingList),
        Caption:this.Caption,
        NoOfCopies: + this.NoOfCopy
      }
      this._baarcodePrintingService.selectBarCodePrinting(objBarCode).subscribe((result: string) => {
        debugger;
        if (result) {
          let item = JSON.parse(JSON.stringify(result));
          for (let i = 0; i < this.barcodePrintingList.length; i++) {
            if (this.barcodePrintingList[i].isSelected === true) {
              this.barcodePrintingList.splice(i--, 1);
            }
          }
          this.SelectbarcodePrintingList = [];
        }
        else
          this._confirmationDialog.openAlertDialog('No records found', 'Barcode Printing');
      });

    }
  }
  //****************************************** BarCode Printing checkUncheckAll********************************************** 
  public checkUncheckAll() {
    debugger;
    for (var i = 0; i < this.barcodePrintingList.length; i++) {
      this.barcodePrintingList[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }
  public isAllSelected() {
    debugger;
    this.masterSelected = this.barcodePrintingList.every(function (item: any) {
      return item.isSelected == true;
    })
    this.getCheckedItemList();
  }
  private getCheckedItemList() {
    debugger;
    let tempbarcodePrintingList = [];
    for (var i = 0; i < this.barcodePrintingList.length; i++) {
      if (this.barcodePrintingList[i].isSelected)
        tempbarcodePrintingList.push(this.barcodePrintingList[i]);
    }
    this.SelectbarcodePrintingList = tempbarcodePrintingList;
  }


  public changeGrid(): void {
    this.barcodePrintingList = []
  }


  private beforeLoadValidate(): boolean {
    debugger;
    if ([null, undefined, 0, ""].indexOf(this.objBarcodeprinting.inv_byno) !== -1 || !this.objBarcodeprinting.inv_byno.toString().trim()) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog("Enter Byno", "Barcode Printing");
      return false;
    } else {
      return true;
    }
  }
  private beforeSelectBrcodePrintValidate(): boolean {
    debugger;
    if (this.SelectbarcodePrintingList.length == 0) {
      this._confirmationDialog.openAlertDialog("Select Byno", "Barcode Printing");
      return false;
    } else {
      return true;
    }
  }


}
