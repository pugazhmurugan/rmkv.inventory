import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarcodePrintingStandardComponent } from './barcode-printing-standard.component';

describe('BarcodePrintingStandardComponent', () => {
  let component: BarcodePrintingStandardComponent;
  let fixture: ComponentFixture<BarcodePrintingStandardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarcodePrintingStandardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarcodePrintingStandardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
