import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarcodePrintingDoubleRateComponent } from './barcode-printing-double-rate.component';

describe('BarcodePrintingDoubleRateComponent', () => {
  let component: BarcodePrintingDoubleRateComponent;
  let fixture: ComponentFixture<BarcodePrintingDoubleRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarcodePrintingDoubleRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarcodePrintingDoubleRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
