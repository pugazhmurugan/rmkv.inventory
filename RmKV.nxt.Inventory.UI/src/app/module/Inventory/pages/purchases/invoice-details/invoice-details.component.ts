import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode } from 'src/app/common/models/common-model';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { InvoiceAttributesLookupComponent } from 'src/app/common/shared/invoice-attributes-lookup/invoice-attributes-lookup.component';
import { InvoiceProdDetailsConfirmationDialogComponent } from 'src/app/common/shared/invoice-prod-details-confirmation-dialog/invoice-prod-details-confirmation-dialog.component';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductLookupComponent } from 'src/app/common/shared/product-lookup/product-lookup.component';
import { InvoiceDetailsService } from '../../../services/purchases/invoice-prod-details/invoice-details.service';
import { InvoicesService } from '../../../services/purchases/invoices/invoices.service';
declare var jsPDF: any;

@Component({
  selector: 'app-invoice-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class InvoiceDetailsComponent implements OnInit {

  componentVisibility: boolean = true;
  productList: any = [];
  counterList: any;
  attributesList: any = [];
  configList: any;
  clearDatas: any;
  sizeist: any;
  poNosList: any[];
  displayedColumns = ["Serial_No", "Product_Group", "Qty", "Pcs", "Supplier_Description", "Selling_Price", "HSN_Code", "Product_Entered", "Product_Details"];
  dataSource: any = new MatTableDataSource([]);

  objInvoiceProdDetail: any = {
    inv_byno: '',
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    gstn_no: '',
    supplier_invoice_no: '',
    invoice_amount: '',
    invoice_entry_date: '',
    table_datas: [{
      product_group_desc: '',
      inv_det_actual_qty: '',
      inv_det_actual_pcs: '',
      supplier_description: '',
      hsncode: '',
      product_entered: ''
    }]
  }

  objInvoiceProdDetailEdit: any = {
    inv_byno: '',
    product_group_id: 0,
    product_group_desc: '',
    unique_serial: '',
    bypass: '',
    product_uom: '',
    supplier_description: '',
    total_pieces: '',
    cost_price: '',
    selling_price: '',
    po_id: 0,
    product_details: [{
      product_code: '',
      product_description: '',
      inv_det_actual_qty: '',
      inv_det_actual_pcs: '',
      style_code: '',
      hsncode: [],
      gstn_no: '',
      counter: 0,
      size: '',
      attributesList: []
    }]
  }

  unChangedInvoiceProdDetailEdit: any = {
    inv_byno: '',
    product_group_id: 0,
    product_group_desc: '',
    unique_serial: '',
    bypass: '',
    product_uom: '',
    supplier_description: '',
    po_id: 0,
    total_pieces: '',
    cost_price: '',
    selling_price: '',
    product_details: [{
      product_code: '',
      product_description: '',
      inv_det_actual_qty: '',
      inv_det_actual_pcs: '',
      style_code: '',
      hsncode: [],
      gstn_no: '',
      counter: 0,
      size: '',
      attributesList: []
    }]
  }

  modifiyInvoiceProdDetailEdit: any = {
    inv_byno: '',
    product_group_id: 0,
    product_group_desc: '',
    unique_serial: '',
    bypass: '',
    product_uom: '',
    supplier_description: '',
    po_id: 0,
    total_pieces: '',
    product_details: [{
      product_code: '',
      product_description: '',
      inv_det_actual_qty: '',
      inv_det_actual_pcs: '',
      style_code: '',
      hsncode: [],
      gstn_no: '',
      counter: 0,
      size: '',
      attributesList: []
    }]
  }

  bynoList: any = [{
    inv_byno: '',
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    gstn_no: '',
    supplier_invoice_no: '',
    invoice_amount: '',
    invoice_entry_date: '',
    table_datas: [{
      product_group_desc_desc: '',
      inv_det_actual_qty: '',
      inv_det_actual_pcs: '',
      supplier_description: '',
      hsncode: '',
      product_entered: ''
    }]
  }]

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  columns: any[] = [
    { display: "sno", editable: false },
    { display: "prodCode", editable: true },
    { display: "prodDesc", editable: false },
    { display: "mts", editable: false },
    { display: "pieces", editable: false },
    { display: "stylecode", editable: true },
    { display: "hsn", editable: true },
    { display: "gst", editable: false },
    { display: "counter", editable: true },
    { display: "size", editable: true }
  ]

  @ViewChildren("prodCode") prodCode: ElementRef | any;
  @ViewChildren("stylecode") stylecode: ElementRef | any;
  @ViewChildren("mts") mts: ElementRef | any;
  @ViewChildren("hsn") hsn: ElementRef | any;
  @ViewChildren("counter") counter: ElementRef | any;
  @ViewChildren("size") size: ElementRef | any;


  constructor(public _localStorage: LocalStorage,
    public _datePipe: DatePipe,
    public _router: Router,
    public _matDialog: MatDialog,
    public _gridKeyEvents: GridKeyEvents,
    public _invoiceProdDetails: InvoiceDetailsService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _excelService: ExcelService,
    public _decimalPipe: DecimalPipe,
    public _invoicesService: InvoicesService,
    public _keyPressEvents: KeyPressEvents) {
  }

  ngAfterViewInit() {
    document.getElementById('byno').focus();
  }

  ngOnInit() {
    this.checkInvoiceEntryConfigs();
    this.getCounterDetails();
  }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.objInvoiceProdDetailEdit.product_details);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.objInvoiceProdDetailEdit.product_details);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.objInvoiceProdDetailEdit.product_details);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.objInvoiceProdDetailEdit.product_details);
        break;
    }
  }
  public getAttributeSizeList(): void {
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        attribute_id: +this.configList[0].ipd_size_attribute_id
      }])
    }
    this._invoiceProdDetails.getAttributeSizeList(objData).subscribe((result: any) => {
      if (result) {
        console.log(result, 'trestiojg')
        this.sizeist = result;
      }
    });
  }
  public getProductLookupDetails(event, i): void {
    if (!this.configList[0].ipd_select_all_attributes) {
      if (event.keyCode == 13 && this.objInvoiceProdDetailEdit.product_details[i].product_code !== '') {
        let objData = {
          InvoiceProdDetails: JSON.stringify([{
            company_section_id: +this._localStorage.getCompanySectionId(),
            product_code: this.objInvoiceProdDetailEdit.product_details[i].product_code
          }])
        }
        this._invoiceProdDetails.getProductLookupDetails(objData).subscribe((result: any) => {
          if (result) {
            console.log(result, 'trestiojg')
            this.productList = result;
            this.objInvoiceProdDetailEdit.product_details[i].product_code = this.productList[0].product_code;
            this.objInvoiceProdDetailEdit.product_details[i].product_description = this.productList[0].product_name;
            this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = this.objInvoiceProdDetailEdit.bypass == true ? this.objInvoiceProdDetailEdit.total_pieces : 1; //this.productList && this.productList.inv_det_actual_pcs ? this.productList.inv_det_actual_pcs : "";
            this.objInvoiceProdDetailEdit.product_details[i].hsncode = result[0].product_hsn;
            this.objInvoiceProdDetailEdit.product_details[i].counter = this.productList[0].counter_id;
            console.log(this.objInvoiceProdDetailEdit.product_details, 'testing')
          } else {
            this.openProductLookup(i);
          }
        });
      } else if (event.keyCode == 13) {
        this.openProductLookup(i);
      }
    } else if (event.keyCode == 13) {
      this.openProductLookup(i);
    }
  }

  public getCounterDetails(): void {
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._invoiceProdDetails.getCounterDetails(objData).subscribe((result: any) => {
      if (result) {
        this.counterList = result;
      }
    });
  }

  public onClearChange(i): void {
    let productCode = this.objInvoiceProdDetailEdit.product_details[i].product_code;
    let attributeList = this.objInvoiceProdDetailEdit.product_details[i].attributesList;
    if (this.objInvoiceProdDetailEdit.product_details[i].product_code.length !== 8) {
      this.objInvoiceProdDetailEdit.product_details[i] = Object.assign({
        product_code: productCode,
        product_description: '',
        inv_det_actual_qty: '',
        inv_det_actual_pcs: '',
        style_code: '',
        hsncode: [],
        gstn_no: '',
        counter: 0,
        attributesList: attributeList
      })
      for (let j = 0; j < this.attributesList.length; j++) {
        this.objInvoiceProdDetailEdit.product_details[i].attributesList[j].attribute_value = '';
      }
      //this.unChangedInvoiceProdDetailEdit.product_details;
      // this.objInvoiceProdDetailEdit.product_details[i].product_code = productCode;
      setTimeout(() => {
        let input = this.prodCode.toArray();
        input[i].nativeElement.focus();
      }, 100);
    }
  }
  public getInvoiceProdDetails(): void {
    if (this.objInvoiceProdDetail.inv_byno.length == 8) {
      let objData = {
        InvoiceProdDetails: JSON.stringify([{
          byno: this.objInvoiceProdDetail.inv_byno,
          company_section_id: this._localStorage.getCompanySectionId(),
          warehouse_id: this._localStorage.getGlobalWarehouseId()
        }]),
      }
      this._invoiceProdDetails.getInvoiceProdDetails(objData).subscribe((result: any) => {
        if (result) {
          console.log(JSON.parse(result.records1));
          this.bynoList = JSON.parse(result.records1);
          if (JSON.parse(result.records2) !== null) {
            this.bynoList.table_datas = JSON.parse(result.records2);
            this.matTableConfig(this.bynoList.table_datas);
          } else {
            this._confirmationDialogComponent.openAlertDialog('No product entry for this byno', 'Invoice Product Details')
          }
          this.onEnterBynoLookupDetails()
        }
      });
    }
  }

  public onChangeClear(): void {
    this.bynoList = [];
    this.matTableConfig([]);
  }

  public matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }

  public newClick(): void {
    this.componentVisibility = !this.componentVisibility;
  }

  public onListClick(): void {
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  public onEnterBynoLookupDetails(): void {
    let bynoLookupList = {
      inv_byno: '',
      supplier_code: "",
      supplier_name: "",
      supplier_address: "",
      gstn_no: "",
      supplier_invoice_no: "",
      invoice_amount: "",
      invoice_entry_date: "",
      table_datas: []
    };
    bynoLookupList = this.bynoList.find(byno => byno.inv_byno.toLowerCase().trim() === this.objInvoiceProdDetail.inv_byno.toString().trim().toLowerCase());
    this.objInvoiceProdDetail.inv_byno = bynoLookupList && bynoLookupList.inv_byno ? bynoLookupList.inv_byno : this.objInvoiceProdDetail.inv_byno;
    this.objInvoiceProdDetail.supplier_code = bynoLookupList && bynoLookupList.supplier_code ? bynoLookupList.supplier_code : '';
    this.objInvoiceProdDetail.supplier_name = bynoLookupList && bynoLookupList.supplier_name ? bynoLookupList.supplier_name : '';
    this.objInvoiceProdDetail.supplier_address = bynoLookupList && bynoLookupList.supplier_address ? bynoLookupList.supplier_address : '';
    this.objInvoiceProdDetail.gstn_no = bynoLookupList && bynoLookupList.gstn_no ? bynoLookupList.gstn_no : '';
    this.objInvoiceProdDetail.supplier_invoice_no = bynoLookupList && bynoLookupList.supplier_invoice_no ? bynoLookupList.supplier_invoice_no : '';
    this.objInvoiceProdDetail.invoice_amount = bynoLookupList && bynoLookupList.invoice_amount ? this._decimalPipe.transform(bynoLookupList.invoice_amount, '1.2-2') : '';
    this.objInvoiceProdDetail.invoice_entry_date = bynoLookupList && bynoLookupList.invoice_entry_date ? this._datePipe.transform(bynoLookupList.invoice_entry_date, 'dd/MM/yyyy') : '';
  }

  public fetchInvoiceProductDetail(data): void {
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        inv_byno: data.inv_byno.toString().trim(),
        byno_serial: data.byno_serial.toString().trim(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._invoiceProdDetails.fetchInvoiceProdDetails(objData).subscribe((result: any) => {
      // if (result) {
      this.getInvoicePoNos();
      this.objAction.isEditing = true;
      this.objAction.isView = false;
      console.log(result);
      this.objInvoiceProdDetailEdit.inv_byno = data.inv_byno + '/' + data.byno_serial;
      this.objInvoiceProdDetailEdit.product_group_desc = data.product_group_desc;
      this.objInvoiceProdDetailEdit.product_uom = data.product_uom;
      if (this.objInvoiceProdDetailEdit.product_uom !== 'No.s') {
        this.columns[3].editable = true;
      }
      this.objInvoiceProdDetailEdit.product_group_id = data.product_group_id;
      this.objInvoiceProdDetailEdit.unique_serial = data.unique_serial ? 'Yes' : 'No';//'No'
      this.objInvoiceProdDetailEdit.supplier_description = data.supplier_description;
      this.objInvoiceProdDetailEdit.total_pieces = data.inv_det_actual_pcs;
      this.objInvoiceProdDetailEdit.cost_price = data.actual_cost_price;
      this.objInvoiceProdDetailEdit.selling_price = data.selling_price;
      this.objInvoiceProdDetailEdit.po_id = data.po_id;
      if (result) {
        let attributeData = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit.product_details[0].attributesList));
        this.objInvoiceProdDetailEdit.product_details = result;
        let attributeValue = '';
        for (let i = 0; i < this.objInvoiceProdDetailEdit.product_details.length; i++) {
          for (let j = 0; j < result[0].attribute_values.length; j++) {
            if (this.configList[0].ipd_size_entry == true && result[i].attribute_values[j].attribute_name == 'Size') {
              attributeValue = result[i].attribute_values[j].attribute_value;
              result[i].attribute_values.splice(j, 1);
            }
          }
          this.objInvoiceProdDetailEdit.product_details[i].product_code = result[i].product_code,
            this.objInvoiceProdDetailEdit.product_details[i].product_description = result[i].product_name,
            this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty = result[i].prod_qty,
            this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = result[i].prod_pcs,
            this.objInvoiceProdDetailEdit.product_details[i].style_code = result[i].supplier_style_code,
            this.objInvoiceProdDetailEdit.product_details[i].hsncode = result[i].hsncode,
            this.objInvoiceProdDetailEdit.product_details[i].gstn_no = result[i].gstn_no,
            this.objInvoiceProdDetailEdit.product_details[i].counter = result[i].counter_id,
            this.objInvoiceProdDetailEdit.product_details[i].size = attributeValue,//result[i].counter_id,
            this.objInvoiceProdDetailEdit.product_details[i].attributesList = result[i].attribute_values.length > 1 ? JSON.parse(JSON.stringify(result[i].attribute_values)) : JSON.parse(JSON.stringify(attributeData))
        }
      } else {
        this.objInvoiceProdDetailEdit.po_id = 0;
        let attributeData = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit.product_details[0].attributesList));
        this.objInvoiceProdDetailEdit.product_details = [];
        // if (this.objInvoiceProdDetailEdit.unique_serial == 'Yes') {
        for (let i = 0; i < +this.objInvoiceProdDetailEdit.total_pieces; i++) {
          this.objInvoiceProdDetailEdit.product_details.push({
            product_code: '',
            product_description: '',
            inv_det_actual_qty: '',
            inv_det_actual_pcs: '',
            style_code: '',
            hsncode: [],
            gstn_no: '',
            counter: 0,
            size: '',
            attributesList: JSON.parse(JSON.stringify(attributeData))
          })
        }
        for (let i = 0; i < this.objInvoiceProdDetailEdit.product_details.length; i++) {
          for (let j = 0; j < this.attributesList.length; j++) {
            this.objInvoiceProdDetailEdit.product_details[i].attributesList[j].attribute_value = '';
          }
        }
      }
      this.clearDatas = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit.product_details));
      console.log(this.objInvoiceProdDetailEdit.product_details, 'dataaaa')
      this.componentVisibility = !this.componentVisibility;
      this.objInvoiceProdDetailEdit.bypass = false;
      this.unChangedInvoiceProdDetailEdit = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit));
      setTimeout(() => {
        let input = this.prodCode.toArray();
        input[0].nativeElement.focus();
      }, 100);
    })
    //})
  }

  public viewInvoiceProductDetail(data): void {
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        inv_byno: data.inv_byno.toString().trim(),
        byno_serial: data.byno_serial.toString().trim(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._invoiceProdDetails.fetchInvoiceProdDetails(objData).subscribe((result: any) => {
      // if (result) {
      this.objAction.isEditing = false;
      this.objAction.isView = true;
      this.componentVisibility = !this.componentVisibility;
      this.objInvoiceProdDetailEdit.inv_byno = data.inv_byno + '/' + data.byno_serial;
      this.objInvoiceProdDetailEdit.product_group_desc = data.product_group_desc;
      this.objInvoiceProdDetailEdit.product_uom = data.product_uom;
      this.objInvoiceProdDetailEdit.product_group_id = data.product_group_id;
      this.objInvoiceProdDetailEdit.unique_serial = data.unique_serial ? 'Yes' : 'No';
      this.objInvoiceProdDetailEdit.supplier_description = data.supplier_description;
      this.objInvoiceProdDetailEdit.total_pieces = data.inv_det_actual_pcs;
      if (result) {
        let attributeData = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit.product_details[0].attributesList));
        this.objInvoiceProdDetailEdit.product_details = [];
        this.objInvoiceProdDetailEdit.product_details = result;
        let attributeValue = '';
        for (let i = 0; i < this.objInvoiceProdDetailEdit.product_details.length; i++) {
          for (let j = 0; j < result[0].attribute_values.length; j++) {
            if (this.configList[0].ipd_size_entry == true && result[i].attribute_values[j].attribute_name == 'Size') {
              attributeValue = result[i].attribute_values[j].attribute_value;
              result[i].attribute_values.splice(j, 1);
            }
          }
        }
        for (let i = 0; i < this.objInvoiceProdDetailEdit.product_details.length; i++) {
          this.objInvoiceProdDetailEdit.product_details[i].product_code = result[i].product_code,
            this.objInvoiceProdDetailEdit.product_details[i].product_description = result[i].product_name,
            this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty = result[i].prod_qty,
            this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = result[i].prod_pcs,
            this.objInvoiceProdDetailEdit.product_details[i].style_code = result[i].supplier_style_code,
            this.objInvoiceProdDetailEdit.product_details[i].hsncode = result[i].hsncode,
            this.objInvoiceProdDetailEdit.product_details[i].gstn_no = result[i].gstn_no,
            this.objInvoiceProdDetailEdit.product_details[i].counter = result[i].counter_id,
            this.objInvoiceProdDetailEdit.product_details[i].size = attributeValue,
            this.objInvoiceProdDetailEdit.product_details[i].attributesList = result[i].attribute_values.length > 1 ? JSON.parse(JSON.stringify(result[i].attribute_values)) : JSON.parse(JSON.stringify(attributeData))
        }
      }
      this.clearDatas = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit.product_details));
    })
  }

  public openProductAttributesDialog(i): void {
    const dialogRef = this._matDialog.open(InvoiceAttributesLookupComponent, {
      width: "500px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objInvoiceProdDetailEdit.product_details[i].product_code,
        product_group_desc: this.objInvoiceProdDetailEdit.product_group_desc,
        product_uom: this.objInvoiceProdDetailEdit.product_uom,
        product_group_id: this.objInvoiceProdDetailEdit.product_group_id,
        attributeValue: this.objInvoiceProdDetailEdit.product_details[i].attributesList,
        viewFlag: this.objInvoiceProdDetailEdit.product_details[i].attributesList[0].attribute_value !== '' ? true : false
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        let productList = result[1].productDetails;
        let attributeList = result[0];
        let checkFlag = result[2].applyCheck;
        if (!checkFlag) {
          for (let k = 0; k < attributeList.length; k++) {
            this.objInvoiceProdDetailEdit.product_details[i].attributesList[k].attribute_value = attributeList[k].attribute_value;
            this.objInvoiceProdDetailEdit.product_details[i].attributesList[k].attribute_value_id = attributeList[k].attribute_value_id;
          }
          this.objInvoiceProdDetailEdit.product_details[i].product_code = productList[0].product_code;
          this.objInvoiceProdDetailEdit.product_details[i].product_description = productList[0].product_name;
          this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = 1;
          this.objInvoiceProdDetailEdit.product_details[i].hsncode = productList[0].product_hsn !== "{}" ? productList[0].product_hsn : [];
          this.objInvoiceProdDetailEdit.product_details[i].counter = this.objInvoiceProdDetailEdit.product_details[i].counter != 0 ? this.objInvoiceProdDetailEdit.product_details[i].counter : productList[0].counter_id;
        } else {
          for (let j = i; j < this.objInvoiceProdDetailEdit.product_details.length; j++) {
            for (let k = 0; k < attributeList.length; k++) {
              this.objInvoiceProdDetailEdit.product_details[j].attributesList[k].attribute_value = attributeList[k].attribute_value;
              this.objInvoiceProdDetailEdit.product_details[j].attributesList[k].attribute_value_id = attributeList[k].attribute_value_id;
            }
            this.objInvoiceProdDetailEdit.product_details[j].product_code = productList[0].product_code;
            this.objInvoiceProdDetailEdit.product_details[j].product_description = productList[0].product_name;
            this.objInvoiceProdDetailEdit.product_details[j].inv_det_actual_pcs = 1;
            this.objInvoiceProdDetailEdit.product_details[j].hsncode = productList[0].product_hsn !== "{}" ? productList[0].product_hsn : [];
            this.objInvoiceProdDetailEdit.product_details[j].counter = this.objInvoiceProdDetailEdit.product_details[j].counter != 0 ? this.objInvoiceProdDetailEdit.product_details[j].counter : productList[0].counter_id;
          }
        }
      }
    })
    //}
  }

  public openProductLookup(i): void {
    if (!this.configList[0].ipd_select_all_attributes)
      this.openProductDialog(i);
    else
      this.openProductAttributesDialog(i);
  }

  public openProductDialog(i): void {
    if (!this.objInvoiceProdDetailEdit.product_details[i].product_description) {
      const dialogRef = this._matDialog.open(ProductLookupComponent, {
        width: "950px",
        panelClass: "custom-dialog-container",
        data: {
          searchString: this.objInvoiceProdDetailEdit.product_details[i].product_code,
          product_group_desc: this.objInvoiceProdDetailEdit.product_group_desc,
          product_uom: this.objInvoiceProdDetailEdit.product_uom,
          product_group_id: this.objInvoiceProdDetailEdit.product_group_id
        }
      });
      dialogRef.afterClosed().subscribe((result: any) => {
        if (result[0] !== undefined) {
          if (result[1].checkbox == true) {
            for (let j = i; j < this.objInvoiceProdDetailEdit.product_details.length; i++) {
              this.objInvoiceProdDetailEdit.product_details[i].product_code = result[0].product_code;
              this.objInvoiceProdDetailEdit.product_details[i].product_description = result[0].product_name;
              //this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty = result[0].inv_det_actual_qty;
              this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = this.objInvoiceProdDetailEdit.bypass == true ? this.objInvoiceProdDetailEdit.total_pieces : 1;;
              //this.objInvoiceProdDetailEdit.product_details[i].gstn_no = result[0].gstn_no;
              this.objInvoiceProdDetailEdit.product_details[i].hsncode = result[0].product_hsn;
              //this.objInvoiceProdDetailEdit.product_details[i].style_code = '';
              this.objInvoiceProdDetailEdit.product_details[i].counter = result[0].counter_id;
            }
            if (!this.columns[3].editable) {
              setTimeout(() => {
                let input = this.stylecode.toArray();
                input[0].nativeElement.focus();
              }, 100);
            } else {
              setTimeout(() => {
                let input = this.mts.toArray();
                input[0].nativeElement.focus();
              }, 100);
            }
          } else {
            this.objInvoiceProdDetailEdit.product_details[i].product_code = result[0].product_code;
            this.objInvoiceProdDetailEdit.product_details[i].product_description = result[0].product_name;
            // this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty = result[0].inv_det_actual_qty;
            this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = this.objInvoiceProdDetailEdit.bypass == true ? this.objInvoiceProdDetailEdit.total_pieces : 1;;
            //this.objInvoiceProdDetailEdit.product_details[i].gstn_no = result[0].gstn_no;
            this.objInvoiceProdDetailEdit.product_details[i].hsncode = result[0].product_hsn;
            //this.objInvoiceProdDetailEdit.product_details[i].style_code = '';
            this.objInvoiceProdDetailEdit.product_details[i].counter = result[0].counter_id;
            console.log(result);
          }
          if (!this.columns[3].editable) {
            setTimeout(() => {
              let input = this.stylecode.toArray();
              input[i].nativeElement.focus();
            }, 100);
          } else {
            setTimeout(() => {
              let input = this.mts.toArray();
              input[i].nativeElement.focus();
            }, 100);
          }
        }
      });
    }
  }

  public onEnterproductsLookUpDetail(i): void {
    let productsLookUpList: any = {
      product_code: "",
      product_description: "",
      inv_det_actual_qty: "",
      inv_det_actual_pcs: "",
      gstn_no: "",
      product_hsn: [],
      style_code: "",
      counter_data: []
    };
    productsLookUpList = this.productList;
    this.objInvoiceProdDetailEdit.product_details[i].product_code = productsLookUpList && productsLookUpList.product_code ? productsLookUpList.product_code : this.objInvoiceProdDetailEdit.product_details[i].product_code;
    this.objInvoiceProdDetailEdit.product_details[i].product_description = productsLookUpList && productsLookUpList.product_description ? productsLookUpList.product_description : "";
    this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty = productsLookUpList && productsLookUpList.inv_det_actual_qty ? productsLookUpList.inv_det_actual_qty : "";
    this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = 1;//productsLookUpList && productsLookUpList.inv_det_actual_pcs ? productsLookUpList.inv_det_actual_pcs : "";
    this.objInvoiceProdDetailEdit.product_details[i].gstn_no = productsLookUpList && productsLookUpList.gstn_no ? productsLookUpList.gstn_no : "";
    this.objInvoiceProdDetailEdit.product_details[i].hsncode = [];
    for (let i = 0; i < productsLookUpList[0].product_hsn.length; i++) {
      this.objInvoiceProdDetailEdit.product_details[i].hsncode.push(productsLookUpList[i].product_hsn);
    }
    console.log(this.objInvoiceProdDetailEdit.product_details[i].hsncode)
    this.objInvoiceProdDetailEdit.product_details[i].counter_data = productsLookUpList && productsLookUpList.counter_data ? productsLookUpList.counter_data : [];
  }

  public getSectionWiseAttribute(): void {
    let objData = {
      InvoiceProdDetails: +this._localStorage.getCompanySectionId()
    }
    this._invoiceProdDetails.sectionWiseAttribute(objData).subscribe((result: any) => {
      if (result) {
        for (let i = 0; i < result.length; i++) {
          if (this.configList[0].ipd_size_entry == true && result[i].attribute_id !== 15) {
            this.attributesList.push({
              attribute_id: result[i].attribute_id,
              attribute_name: result[i].attribute_name,
              attribute_value: '',
              attribute_value_id: ''
            })
            for (let j = 0; j < this.objInvoiceProdDetailEdit.product_details.length; j++) {
              this.objInvoiceProdDetailEdit.product_details[j].attributesList.push({
                attribute_id: result[i].attribute_id,
                attribute_name: result[i].attribute_name,
                attribute_value: '',
                attribute_value_id: ''
              })
              this.unChangedInvoiceProdDetailEdit.product_details[j].attributesList.push({
                attribute_id: result[i].attribute_id,
                attribute_name: result[i].attribute_name,
                attribute_value: '',
                attribute_value_id: ''
              })
            }
          } else if (this.configList[0].ipd_size_entry == false) {
            this.attributesList.push({
              attribute_id: result[i].attribute_id,
              attribute_name: result[i].attribute_name,
              attribute_value: '',
              attribute_value_id: ''
            })
            for (let j = 0; j < this.objInvoiceProdDetailEdit.product_details.length; j++) {
              this.objInvoiceProdDetailEdit.product_details[j].attributesList.push({
                attribute_id: result[i].attribute_id,
                attribute_name: result[i].attribute_name,
                attribute_value: '',
                attribute_value_id: ''
              })
              this.unChangedInvoiceProdDetailEdit.product_details[j].attributesList.push({
                attribute_id: result[i].attribute_id,
                attribute_name: result[i].attribute_name,
                attribute_value: '',
                attribute_value_id: ''
              })
            }
          }
        }
        console.log(this.objInvoiceProdDetailEdit.product_details, 'testst')
      }
    });
  }

  private checkInvoiceEntryConfigs(): void {
    let objCheck = {
      group_section_id: +this._localStorage.getGlobalGroupSectionId()
    }
    this._invoicesService.checkInvoiceEntryConfigs(objCheck).subscribe((result: any) => {
      this.configList = result;
      //  if (this.configList[0].ipd_size_attribute_id)    
      this.getSectionWiseAttribute();
      this.getAttributeSizeList();
      console.log(this.configList, 'confifg')
    })
  }


  public onClear(exitFlag: boolean): void {
    if (!this.objAction.isView) {
      if (this.checkAnyChangesMade())
        this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
      else if (exitFlag)
        this.onListClick();
    } else {
      this.onListClick();
    }
  }

  private checkAnyChangesMade(): boolean {
    if (JSON.stringify(this.objInvoiceProdDetailEdit.product_details) == JSON.stringify(this.unChangedInvoiceProdDetailEdit.product_details))
      return false;
    else
      return true;
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Invoice Product Details";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  public openStyleCodeLookup(k, value): void {
    if (this.objInvoiceProdDetailEdit.product_details[k].style_code !== '') {
      if (this.objInvoiceProdDetailEdit.product_details[k].style_code !== '' && this.objInvoiceProdDetailEdit.product_details[k + 1].style_code == '') {
        let dialogRef = this._matDialog.open(InvoiceProdDetailsConfirmationDialogComponent, {
          width: "950px",
          panelClass: "custom-dialog-container",
          data: {
            confirmMessage: 'Apply same style code to',
            row: +this.objInvoiceProdDetailEdit.total_pieces
          }
        });
        dialogRef.afterClosed().subscribe((result: any) => {
          if (result) {
            for (let j = k; j < result; j++) {
              this.objInvoiceProdDetailEdit.product_details[j].style_code = value;
            }
            for (let y = 0; y < this.objInvoiceProdDetailEdit.product_details.length; y++) {
              if (this.objInvoiceProdDetailEdit.product_details[y].hsncode.length == 0) {
                let input = this.hsn.toArray();
                input[y].nativeElement.focus();
                return
              }
            }
          }
          dialogRef = null;
        });
        return
      };
    }
  }

  public openCounterlookup(k, value): void {
    if (this.objInvoiceProdDetailEdit.product_details[k].counter !== 0) {
      if (this.objInvoiceProdDetailEdit.product_details[k].counter !== 0 && this.objInvoiceProdDetailEdit.product_details[k + 1].counter == 0) {
        let dialogRef = this._matDialog.open(InvoiceProdDetailsConfirmationDialogComponent, {
          width: "950px",
          panelClass: "custom-dialog-container",
          data: {
            confirmMessage: 'Apply same counter to',
            row: +this.objInvoiceProdDetailEdit.total_pieces
          }
        });
        dialogRef.afterClosed().subscribe((result: any) => {
          if (result) {
            for (let j = k; j < result; j++) {
              this.objInvoiceProdDetailEdit.product_details[j].counter = value;
            }
            for (let y = 0; y < this.objInvoiceProdDetailEdit.product_details.length; y++) {
              if (this.objInvoiceProdDetailEdit.product_details[y].product_code == '') {
                let input = this.prodCode.toArray();
                input[y].nativeElement.focus();
                return
              }
            }
          }
          dialogRef = null;
        });
        return
      };
    }
  }

  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Product Group": this.dataSource.data[i].product_group_desc,
          "Qts/Mts": this.dataSource.data[i].inv_det_actual_qty,
          "Pcs": this.dataSource.data[i].inv_det_actual_pcs,
          "Supplier Description": this.dataSource.data[i].supplier_description,
          "Selling Price": this.dataSource.data[i].selling_price,
          "HSN Code": this.dataSource.data[i].hsncode,
          "Product Entered": this.dataSource.data[i].product_entered ? 'Yes' : 'No'
        });
      }
      this._excelService.exportAsExcelFile(json, "Invoice_Product_Details", datetime);
    } else
      this._confirmationDialogComponent.openAlertDialog("No records found, Load the data first", "Invoice Product Details");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.product_group_desc);
        tempObj.push(e.inv_det_actual_qty);
        tempObj.push(e.inv_det_actual_pcs);
        tempObj.push(e.supplier_description);
        tempObj.push(e.selling_price);
        tempObj.push(e.hsncode);
        tempObj.push(e.product_entered ? 'Yes' : 'No');
        prepare.push(tempObj);
      });
      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Product Group", "Qts/Mts", "Pcs", "Supplier Description", "Selling Price", "HSN Code", "Product Entered"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Invoice_Product_Details' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No records found, Load the data first", "Invoice Product Details");
  }

  private resetScreen(): void {
    this.objInvoiceProdDetailEdit.product_details = [];
    this.objInvoiceProdDetailEdit.product_details = JSON.parse(JSON.stringify(this.clearDatas));
    this.objInvoiceProdDetailEdit.product_details = JSON.parse(JSON.stringify(this.unChangedInvoiceProdDetailEdit.product_details));
  }

  public addInvoiceProduct(): void {
    if ([null, undefined, "", 0, '0'].indexOf(this.objInvoiceProdDetailEdit.po_id) !== -1) {
      document.getElementById('poNo').focus();
      this._confirmationDialogComponent.openAlertDialog('Select po no', 'Invoice Product Deatils');
      return
    }
    let poNo = this.poNosList[this.poNosList.findIndex((item: any) => item.po_id === +this.objInvoiceProdDetailEdit.po_id)].po_no;
    let tempData = [];
    for (let i = 0; i < this.objInvoiceProdDetailEdit.product_details.length; i++) {
      if ([null, undefined, ""].indexOf(this.objInvoiceProdDetailEdit.product_details[i].product_code) !== -1) {
        let input = this.prodCode.toArray();
        input[i].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog('Enter product code', 'Invoice Product Deatils');
        return
      } else if ([null, undefined, ""].indexOf(this.objInvoiceProdDetailEdit.product_details[i].product_description) !== -1) {
        let input = this.prodCode.toArray();
        input[i].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog('Enter valid product code', 'Invoice Product Deatils');
        return
      } else if ([null, undefined, "", 0, '0'].indexOf(this.objInvoiceProdDetailEdit.product_details[i].hsncode.length) !== -1) {
        let input = this.hsn.toArray();
        input[i].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog('Enter hsn code', 'Invoice Product Deatils');
        return
      }
      // if (this.objInvoiceProdDetailEdit.product_details[i].size) {
      //   for (let k = 0; k < this.objInvoiceProdDetailEdit.product_details[i].attributesList.length; k++) {
      //     if (this.objInvoiceProdDetailEdit.product_details[i].attributesList[k].attribute_name == 'Size') {
      //       this.objInvoiceProdDetailEdit.product_details[i].attributesList[k].attribute_value = this.objInvoiceProdDetailEdit.product_details[i].size;
      //     }
      //   }
      // }
      let attributeTempData = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit.product_details[i].attributesList));
      if (this.configList[0].ipd_size_entry == true) {
        let attributeId = 0;
        let attributeName = '';
        let attributeValue = '';
        let attributeValueId = '';
        for (let x = 0; x < this.sizeist.length; x++) {
          if (this.sizeist[x].attribute_value == this.objInvoiceProdDetailEdit.product_details[i].size) {
            attributeId = this.sizeist[x].attribute_id;
            attributeName = this.sizeist[x].attribute_name;
            attributeValue = this.sizeist[x].attribute_value;
            attributeValueId = this.sizeist[x].attribute_value_id;
          }
        }
        for (let y = 0; y < this.objInvoiceProdDetailEdit.product_details[i].attributesList.length; y++) {
          if (this.objInvoiceProdDetailEdit.product_details[i].attributesList[y].attribute_id == attributeId) {
            attributeTempData.splice(y, 1);
          }
        }
        attributeTempData.push({
          attribute_id: attributeId,
          attribute_name: attributeName,
          attribute_value: this.objInvoiceProdDetailEdit.product_details[i].size,
          attribute_value_id: attributeValueId,
        })
      }

      let attributeData = [];
      for (let k = 0; k < attributeTempData.length; k++) {
        if (attributeTempData[k]) {
          if (attributeTempData[k].attribute_value !== '') {
            attributeData.push(attributeTempData[k]);
          }
        }
      }
      tempData.push({
        serial_no: i + 1,
        prod_qty: this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty == '' ? 0 : this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty,
        prod_pcs: this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs,
        quantity: this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty == '' ? 0 : this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty,
        cost_price: this.objInvoiceProdDetailEdit.cost_price,
        selling_price: this.objInvoiceProdDetailEdit.selling_price,
        product_code: this.objInvoiceProdDetailEdit.product_details[i].product_code,
        hsncode: this.objInvoiceProdDetailEdit.product_details[i].hsncode.length == 0 ? [""] : JSON.parse(this.objInvoiceProdDetailEdit.product_details[i].hsncode),
        gst: 1,//this.objInvoiceProdDetailEdit.product_details[i].pcs,
        supplier_style_code: this.objInvoiceProdDetailEdit.product_details[i].style_code,
        counter_id: this.objInvoiceProdDetailEdit.product_details[i].counter,
        attribute_values: attributeData.length == 0 ? '[{}]' : attributeData
      })
    }
    var x = this.objInvoiceProdDetailEdit.inv_byno.split("/");
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        inv_byno: x[0],
        byno_serial: x[1],
        po_id: this.objInvoiceProdDetailEdit.po_id,
        po_no: poNo,
        details: tempData,
        entered_by: +this._localStorage.intGlobalUserId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        sales_location_id: +this._localStorage.getGlobalSalesLocationId()
      }]),
    }
    this._invoiceProdDetails.addInvoiceProdDetails(objData).subscribe((result: any) => {
      if (result) {
        this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New RGN record has been created", 'Invoice Product Deatils');
        this.componentVisibility = !this.componentVisibility;
        // this.resetScreen();
        this.getInvoiceProdDetails();
      }
    });
  }

  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }
  public getInvoicePoNos(): void {
    this.poNosList = [];
    let objInvoice: any = {
      PoNos: JSON.stringify([{
        warehouse_id: +this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        supplier_code: this.objInvoiceProdDetail.supplier_code.toString().trim()
      }])
    }
    this._invoicesService.getInvoicePoNos(objInvoice).subscribe((result: any) => {
      this.poNosList = result ? JSON.parse(JSON.stringify(result)) : [];
    });
  }

}

