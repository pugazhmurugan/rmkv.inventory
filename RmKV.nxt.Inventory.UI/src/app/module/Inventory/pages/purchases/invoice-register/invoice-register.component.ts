import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog, MatDialogRef, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { filter } from 'rxjs/operators';
import { EditMode } from 'src/app/common/models/common-model';
import { AccountsLookupService } from 'src/app/common/services/accounts-lookup/accounts-lookup.service';
import { CommonService } from 'src/app/common/services/common/common.service';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ReasonComponent } from 'src/app/common/shared/reason/reason.component';
import { InvoiceRegister, Invoices } from '../../../model/purchases/invoice-register';
import { CounterToGodownService } from '../../../services/goods-movement/internal-transfers/counter-to-godown/counter-to-godown.service';
import { GodownToCounterService } from '../../../services/goods-movement/internal-transfers/godown-to-counter/godown-to-counter.service';
import { InvoiceRegisterService } from '../../../services/purchases/invoice-register/invoice-register.service';
declare var jsPDF: any;
@Component({
  selector: 'app-invoice-register',
  templateUrl: './invoice-register.component.html',
  styleUrls: ['./invoice-register.component.scss'],
  providers: [DatePipe]
})
export class InvoiceRegisterComponent implements OnInit {
  componentVisibility: boolean = true;
  displayedColumns = ["Serial_No", "by_no", "supplier_code", "supplier_name", "invoice_no", "invoice_date", "grn_no", "lr_no", "ewaybillno", "entered_date",
    "sub_total", "other_amount", "taxable_amount", "cgst_amount", "sgst_amount", "igst_amount", "Action"];

  Date: any = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Status: 'All'
  }
  supplierLookupList: any = [];
  focusFlag: boolean = false;

  objInvoiceRegister: InvoiceRegister = {
    register_id: 0,
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    gst_no: '',
    supplier_email: '',
    gstn_no: '',
    supplier_gstn_no: "",
    ewaybillno: '',
    invoice_no: '',
    invoice_date: this._datePipe.transform(new Date(), 'yyyy-MM-dd'),
    sub_total: 0,
    other_amount: 0,
    taxable_amount: 0,
    cgst_amount: 0,
    sgst_amount: 0,
    igst_amount: 0,
    cess: 0,
    round_off: 0,
    invoice_amount: 0,
    warehouse_id: +this._localStorage.getGlobalWarehouseId(),
    company_section_id: +this._localStorage.getCompanySectionId(),
    entered_by: +this._localStorage.intGlobalUserId(),
  }

  objModifyInvoiceRegister: InvoiceRegister = {
    register_id: 0,
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    gst_no: '',
    supplier_email: '',
    gstn_no: '',
    supplier_gstn_no: "",
    ewaybillno: '',
    invoice_no: '',
    invoice_date: this._datePipe.transform(new Date(), 'yyyy-MM-dd'),
    sub_total: 0,
    other_amount: 0,
    taxable_amount: 0,
    cgst_amount: 0,
    sgst_amount: 0,
    igst_amount: 0,
    cess: 0,
    round_off: 0,
    invoice_amount: 0,
    warehouse_id: +this._localStorage.getGlobalWarehouseId(),
    company_section_id: +this._localStorage.getCompanySectionId(),
    entered_by: +this._localStorage.intGlobalUserId(),
  }

  objUnchangedInvoiceRegister: InvoiceRegister = {
    register_id: 0,
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    gst_no: '',
    supplier_email: '',
    gstn_no: '',
    supplier_gstn_no: "",
    ewaybillno: '',
    invoice_no: '',
    invoice_date: this._datePipe.transform(new Date(), 'yyyy-MM-dd'),
    sub_total: 0,
    other_amount: 0,
    taxable_amount: 0,
    cgst_amount: 0,
    sgst_amount: 0,
    igst_amount: 0,
    cess: 0,
    round_off: 0,
    invoice_amount: 0,
    warehouse_id: +this._localStorage.getGlobalWarehouseId(),
    company_section_id: +this._localStorage.getCompanySectionId(),
    entered_by: +this._localStorage.intGlobalUserId(),
  }

  newSupplierLookupList: any = [];
  loadSupplierLookupList: any = [];
  // objInvoiceRegister: Invoices = {
  //   supplier_code: '',
  //   supplier_name: '',
  //   supplier_address: '',
  //   supplier_gstn_no: '',
  //   supplier_email:'',
  // }

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };



  InvoiceRegisterList: any[] = [];

  locationList: any;
  dataSource: any = new MatTableDataSource([]);

  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  @ViewChild(MatSort, null) sort: MatSort;
  @ViewChildren('supplierInput') supplierInput: ElementRef | any;

  constructor(public _minMaxDate: MinMaxDate, public _router: Router, private dbService: NgxIndexedDBService,
    public _localStorage: LocalStorage, private _datePipe: DatePipe, public _CommonService: CommonService,
    public _invoiceRegisterService: InvoiceRegisterService,
    private _accountsLookupService: AccountsLookupService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    private _matDialog: MatDialog, public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _keyPressEvents: KeyPressEvents, public _excelService: ExcelService,
    private _confirmationDialog: ConfirmationDialogComponent,) {
    this.load.FromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
    this.getSupplierLookupList();
  }

  ngOnInit() {
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }
  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }

  public newClick(): void {
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  /************************************************ Validations ************************************************/


  public calculateTaxableValue(): void {
    debugger;
    this.objInvoiceRegister.sub_total = this.objInvoiceRegister.sub_total.toString() === "" ? 0 : this.objInvoiceRegister.sub_total;
    let subTotal = isNaN(+this.objInvoiceRegister.sub_total) ? 0 : +this.objInvoiceRegister.sub_total;
    this.objInvoiceRegister.other_amount = this.objInvoiceRegister.other_amount.toString() === "" ? 0 : this.objInvoiceRegister.other_amount;
    let others = isNaN(+this.objInvoiceRegister.other_amount) ? 0 : +this.objInvoiceRegister.other_amount;
    this.objInvoiceRegister.taxable_amount = Number(subTotal) + (+others);

  }

  public calculateToatalInvoiceAmount(): void {
    debugger;
    this.objInvoiceRegister.invoice_amount = Number(this.objInvoiceRegister.sub_total) + (+this.objInvoiceRegister.other_amount) + (+this.objInvoiceRegister.igst_amount) + (+this.objInvoiceRegister.cgst_amount)
      + (+this.objInvoiceRegister.sgst_amount) + (+this.objInvoiceRegister.cess)
    if (this.objInvoiceRegister.round_off.toString().includes('.')) {
      if (+(this.objInvoiceRegister.round_off.toString().split('.')[1]) >= 0) {
        this.objInvoiceRegister.invoice_amount = this.objInvoiceRegister.invoice_amount + +(this.objInvoiceRegister.round_off);
      } else if (+(this.objInvoiceRegister.round_off.toString().split('.')[1]) < 0) {
        this.objInvoiceRegister.invoice_amount = this.objInvoiceRegister.invoice_amount - +(this.objInvoiceRegister.round_off);
      }
    } else if (+(this.objInvoiceRegister.round_off.toString())) {
      this.objInvoiceRegister.invoice_amount = this.objInvoiceRegister.invoice_amount + +(this.objInvoiceRegister.round_off);
    }

  }


  private beforeLoadValidate(): boolean {
    debugger;
    if ([null, undefined, 0, ""].indexOf(this.load.FromDate) !== -1) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Select from date", "Invoice Register");
      return false;
    } else if (this.load.FromDate.length != 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Invalid from date", "Invoice Register");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.load.ToDate) !== -1) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Select todate", "Invoice Register");
      return false;
    } else if (this.load.ToDate.length != 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Invalid todate", "Invoice Register");
      return false;
    } else {
      return true;
    }
  }

  private beforeSaveValidate(): boolean {
    debugger;
    if ([null, undefined, 0, ""].indexOf(this.objInvoiceRegister.supplier_code) !== -1 || !this.objInvoiceRegister.supplier_code.toString().trim()) {
      document.getElementById('newSupplier').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Supplier Code", "Invoice Register");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objInvoiceRegister.gstn_no) !== -1 || !this.objInvoiceRegister.gstn_no.toString().trim()) {
      document.getElementById('gstnno').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter GSTN No", "Invoice Register");
      return false;
    } else if ([null, undefined, "", 0, '0'].indexOf(this.objInvoiceRegister.ewaybillno) !== -1 || !this.objInvoiceRegister.ewaybillno.toString().trim()) {
      document.getElementById('ewaybillno').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Eway bill no", "Invoice Register");
      return false;
    } else if ([null, undefined, "", 0, '0'].indexOf(this.objInvoiceRegister.invoice_no) !== -1 || !this.objInvoiceRegister.invoice_no.toString().trim()) {
      document.getElementById('invoiceno').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Invoice no", "Invoice Register");
      return false;
    } else if ([null, undefined, "", 0, '0'].indexOf(this.objInvoiceRegister.sub_total) !== -1 || !this.objInvoiceRegister.sub_total.toString().trim()) {
      document.getElementById('subtotal').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Sub Total", "Invoice Register");
      return false;
    }
    // else if ([null, undefined, "", 0, '0'].indexOf(this.objInvoiceRegister.other_amount) !== -1 || !this.objInvoiceRegister.other_amount.toString().trim()) {
    //   document.getElementById('others').focus();
    //   this._confirmationDialogComponent.openAlertDialog("Enter Others", "Invoice Register");
    //   return false;
    // } 
    else if ([null, undefined, "", 0, '0'].indexOf(this.objInvoiceRegister.sgst_amount) !== -1 && [null, undefined, "", 0, '0'].indexOf(this.objInvoiceRegister.igst_amount) !== -1
      && [null, undefined, "", 0, '0'].indexOf(this.objInvoiceRegister.cgst_amount) !== -1) {
      document.getElementById('cgst').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter CGST", "Invoice Register");
      return false;
    }

    else {
      return true;
    }
  }

  public onClear(exitFlag: boolean): void {
    debugger;
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objInvoiceRegister) !== ((this.objAction.isEditing) ? JSON.stringify(this.objModifyInvoiceRegister)
      : JSON.stringify(this.objUnchangedInvoiceRegister)))
      return true;
    else
      return false;
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Invoice Register";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  public onClickDelete(index: any): any {
    this.dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this.dialogRef.componentInstance.confirmMessage =
      "Do you want to cancel of '" + this.dataSource.data[index].supplier_code + "'";
    this.dialogRef.componentInstance.componentName = "Invoice Register";
    return this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.openReasonDialog(index);
      }
      this.dialogRef = null;
    });
  }

  public openReasonDialog(index: number): void {
    debugger;
    let dialogRef = this._matDialog.open(ReasonComponent, {
      width: "75vw",
      panelClass: "custom-dialog-container",
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== null) {
        this.deleteInvoiceRegister(index, result);
      }
      this.dialogRef = null;
    });
  }

  public onClickApproved(index: number): any {
    debugger;

    // if (this.checkCandidates(index)) {
    this.dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this.dialogRef.componentInstance.confirmMessage =
      "Invoice Register Has been approved?";
    this.dialogRef.componentInstance.componentName = "Invoice Register";
    return this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.approvedInvoiveRegister(index);
      }
      this.dialogRef = null;
    });
    // }
  }

  public onListClick(): void {
    this.getInvoiceRegisterList();
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  private resetScreen(): void {
    this.objAction = Object.assign({}, this.unChangedAction);
    this.objInvoiceRegister = Object.assign({}, this.objUnchangedInvoiceRegister);
    this.objModifyInvoiceRegister = Object.assign({}, this.objUnchangedInvoiceRegister);
  }

  public changeIGST(): void {
    this.objInvoiceRegister.cgst_amount = this.objInvoiceRegister.sgst_amount;
  }

  public changeInIGST(): void {
    this.objInvoiceRegister.sgst_amount = this.objInvoiceRegister.cgst_amount;
  }

  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  // public onlyAllowDecimalNegative(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
  //   debugger;
  //   if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
  //     const pattern = /^[+-]?\d+$/;
  //     if (pattern.test(event.key)) {
  //       this[key1][key2] = "";
  //       event.target.value = "";
  //     }
  //     return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
  //   } else {
  //     return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
  //   }
  // }

  public onlyAllowRoundOffDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string) {
    debugger;
    if (event.keyCode !== 45) {
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      } else {
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      }
    } else {
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
      }
    }
  }


  /************************************************ CRUD OPERATION ************************************************/

  public getInvoiceRegisterList(): void {
    debugger;
    if (this.beforeLoadValidate()) {
      let InvoiceRegister = {
        InvoiceRegister: JSON.stringify([{
          from_date: this.load.FromDate,
          to_date: this.load.ToDate,
          company_section_id: +this._localStorage.getCompanySectionId(),
          status:this.load.Status,
        }])
      }
      this._invoiceRegisterService.getInvoiceRegister(InvoiceRegister).subscribe((result: any[]) => {
        console.log(result, "list")
        this.InvoiceRegisterList = result;
        this.InvoiceRegisterList != null ? this.matTableConfig(this.InvoiceRegisterList) : this._confirmationDialogComponent.openAlertDialog("No record found", "Invoice Register");
      });
    }
  }

  public matTableConfig(tableRecords: any[]): void {
    this.dataSource = new MatTableDataSource(tableRecords);
    this.dataSource.sort = this.sort;
  }

  public saveInvoiceRegister(): void {
    debugger;
    if (this.beforeSaveValidate()) {
      let tempSaveArr = [];

      let objData = {

        register_id: this.objAction.isEditing ? this.objInvoiceRegister.register_id : 0,
        supplier_code: this.objInvoiceRegister.supplier_code.toString().trim(),
        supplier_name: this.objInvoiceRegister.supplier_name,
        gstn_no: this.objInvoiceRegister.gstn_no,
        ewaybillno: this.objInvoiceRegister.ewaybillno,
        invoice_no: this.objInvoiceRegister.invoice_no,
        invoice_date: this._datePipe.transform(this.objInvoiceRegister.invoice_date, 'dd/MM/yyyy'),
        sub_total: this.objInvoiceRegister.sub_total,
        other_amount: this.objInvoiceRegister.other_amount,
        taxable_amount: this.objInvoiceRegister.taxable_amount,
        cgst_amount: this.objInvoiceRegister.cgst_amount,
        sgst_amount: this.objInvoiceRegister.sgst_amount,
        igst_amount: this.objInvoiceRegister.igst_amount,
        cess: this.objInvoiceRegister.cess,
        round_off: this.objInvoiceRegister.round_off,
        invoice_amount: this.objInvoiceRegister.invoice_amount,
        warehouse_id: +this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        entered_by: +this._localStorage.intGlobalUserId(),
      }
      tempSaveArr.push(objData);
      let objSave = {
        InvoiceRegister: JSON.stringify(tempSaveArr)
      }
      // let objSave = {
      //   invoice: JSON.stringify([this.objInvoiceRegister])
      // }

      this._invoiceRegisterService.saveInvoiceRegister(objSave).subscribe((result: any[]) => {
        debugger;
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New invoice register record has been created", "Invoice Register");
          this.getInvoiceRegisterList();
          this.componentVisibility = !this.componentVisibility;
        }
      });
    }
  }

  public modifyInvoiceRegister(isEditing: boolean, index: number): void {

    this.objAction = {
      isEditing: isEditing ? true : false
    }

    let objFetch = {
      InvoiceRegister: JSON.stringify([{
        register_id: this.dataSource.data[index].register_id,
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._invoiceRegisterService.fetchInvoiceRegister(objFetch).subscribe((result: any) => {
      if (result) {
        debugger;
        this.objAction.isEditing = true;
        this.objInvoiceRegister = JSON.parse(JSON.stringify(result[0]));
        this.objModifyInvoiceRegister = JSON.parse(JSON.stringify(result[0]));
        let i = this.loadSupplierLookupList.findIndex(x => x.account_code.toString().trim() == this.objModifyInvoiceRegister.supplier_code.toString().trim());
        var supplier_data = JSON.parse(JSON.stringify(this.loadSupplierLookupList[i]));
        this.objInvoiceRegister.gstn_no = [null, 'null', undefined, 'undefined', NaN, 'NaN', '', 0, '0'].indexOf(supplier_data.gstn_no) === -1 ? supplier_data.gstn_no : this.objInvoiceRegister.gstn_no;
        this.objModifyInvoiceRegister = [null, 'null', undefined, 'undefined', NaN, 'NaN', '', 0, '0'].indexOf(supplier_data.gstn_no) === -1 ? supplier_data.gstn_no : this.objInvoiceRegister.gstn_no;
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(supplier_data.email) !== -1) {
          this.objInvoiceRegister.supplier_address = supplier_data.account_name + '\n' + supplier_data.address1 + '\n' + supplier_data.address2 + '\n' + ([null, 'null', undefined, 'undefined', NaN, 'NaN', '', 0, '0'].indexOf(supplier_data.address3) === -1 ? supplier_data.address3 : '') + '\nGSTIN No : ' + ([null, 'null', undefined, 'undefined', NaN, 'NaN', '', 0, '0'].indexOf(supplier_data.gstn_no) === -1 ? supplier_data.gstn_no : this.objInvoiceRegister.gstn_no);
        } else if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(supplier_data.email) === -1) {
          this.objInvoiceRegister.supplier_address = supplier_data.account_name + '\n' + supplier_data.address1 + '\n' + supplier_data.address2 + '\n' + supplier_data.address3 + '\nEmail : ' + supplier_data.supplier_email + '\nGSTIN No : ' + ([null, 'null', undefined, 'undefined', NaN, 'NaN', '', 0, '0'].indexOf(supplier_data.gstn_no) === -1 ? supplier_data.gstn_no : this.objInvoiceRegister.gstn_no);
        }
        console.log(result, "fetch")
        this.componentVisibility = !this.componentVisibility;
      }
    })
  }

  private deleteInvoiceRegister(row: any, Reason: string): void {
    debugger;
    let tempSaveArr = [];
    let objData = {
      register_id: this.dataSource.data[row].register_id,
      company_section_id: +this._localStorage.getCompanySectionId(),
      cancel: false,
      entered_by: +this._localStorage.intGlobalUserId(),
      cancelled_reason: Reason,
    }
    tempSaveArr.push(objData);
    let objCancel = {
      InvoiceRegister: JSON.stringify(tempSaveArr)
    }
    this._invoiceRegisterService.removeInvoiceRegister(objCancel).subscribe(
      (result: any) => {
        if (result.error) {
          this._confirmationDialogComponent.openAlertDialog(result.error, "Invoice Register");
        } else if (result) {
          this._confirmationDialogComponent.openAlertDialog("Cancelled", "Invoice Register");
        } else {
          this._confirmationDialogComponent.openAlertDialog(result.error, "Invoice Register");
        }
        this.getInvoiceRegisterList();
      });
    this.dialogRef = null;
  }

  private approvedInvoiveRegister(index: number): void {
    debugger;
    let tempSaveArr = [];
    let objData = {
      register_id: this.dataSource.data[index].register_id,
      company_section_id: +this._localStorage.getCompanySectionId(),
      approve: "true",
      entered_by: +this._localStorage.intGlobalUserId(),
    }
    tempSaveArr.push(objData);
    let objSave = {
      InvoiceRegister: JSON.stringify(tempSaveArr)
    }
    this._invoiceRegisterService.approveInvoiceRegister(objSave).subscribe(
      (result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog("Added to approved list", "Invoice Register");
          this.getInvoiceRegisterList();
        }
      }
    );
  }


  /************************************************ Lookup Functionalities ************************************************/
  private getSupplierLookupList(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._accountsLookupService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      debugger;
      if (result) {
        this.newSupplierLookupList = JSON.parse(result);
        this.loadSupplierLookupList = JSON.parse(result);
        this.newSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
        this.loadSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
      }
    });
  }

  private openNewSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "650px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objInvoiceRegister.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objInvoiceRegister.supplier_code = result.account_code.toString().trim();
        this.objInvoiceRegister.supplier_name = result.account_name.toString().trim();
        this.objInvoiceRegister.supplier_email = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(result.email) === -1 ? result.email : '';
        this.objInvoiceRegister.gstn_no = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(result.gstn_no) === -1 ? result.gstn_no : '';
        result.address1 = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(result.address1) === -1 ? result.address1 : '';
        result.address2 = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(result.address2) === -1 ? result.address2 : '';
        result.address3 = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(result.address3) === -1 ? result.address3 : '';
        if (this.objInvoiceRegister.supplier_email === '')
          this.objInvoiceRegister.supplier_address = result.account_name + '\n' + result.address1 + ' ' + result.address2 + ' ' + result.address3 + '\nGSTIN No : ' + this.objInvoiceRegister.gstn_no;
        else if (this.objInvoiceRegister.supplier_email !== '')
          this.objInvoiceRegister.supplier_address = result.account_name + '\n' + result.address1 + ' ' + result.address2 + ' ' + result.address3 + '\nEmail : ' + this.objInvoiceRegister.supplier_email + '\nGSTIN No : ' + this.objInvoiceRegister.gstn_no;
        // this.checkIsRegisteredSupplier();
        // this.getSupplierDescList();
      } else {
        this.objInvoiceRegister.supplier_code = '';
        this.objInvoiceRegister.supplier_name = '';
        this.objInvoiceRegister.supplier_email = '';
        this.objInvoiceRegister.supplier_address = '';
        this.objInvoiceRegister.gstn_no = '';
        // this.resetGstValues();
      }
    });
  }

  public openNewSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierCode()) {
      this.openNewSupplierLookupDialog();
    }
  }

  // private openNewSupplierLookupDialog(): void {
  //   this.focusFlag = true;
  //   const dialogRef = this._matDialog.open(AccountsLookupComponent, {
  //     width: "550px",
  //     panelClass: "custom-dialog-container",
  //     data: {
  //       searchString: this.objInvoiceRegister.supplier_code,
  //       For: 1
  //     }
  //   });
  //   dialogRef.afterClosed().subscribe((result: any) => {
  //     this.focusFlag = false;
  //     if (result) {
  //       debugger;
  //       console.log(result,"lookup")
  //       this.objInvoiceRegister.supplier_code = result.account_code.toString().trim();
  //       this.objInvoiceRegister.supplier_name = result.account_name.toString().trim();
  //       this.objInvoiceRegister.supplier_email = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(result.email) === -1 ? result.email : '';
  //       if (this.objInvoiceRegister.supplier_email === '')
  //         this.objInvoiceRegister.supplier_address = result.account_name + '\n' + result.address1 + '\n' + result.address2 + '\n' + result.address3;
  //       else if (this.objInvoiceRegister.supplier_email !== '')
  //         this.objInvoiceRegister.supplier_address = result.account_name + '\n' + result.address1 + '\n' + result.address2 + '\n' + result.address3 + '\nEmail : ' + this.objInvoiceRegister.supplier_email;
  //       this.objInvoiceRegister.gstn_no = result.gstn_no;
  //     } else {
  //       this.objInvoiceRegister.supplier_code = '';
  //       this.objInvoiceRegister.supplier_name = '';
  //       this.objInvoiceRegister.supplier_address = '';
  //       this.objInvoiceRegister.gstn_no = '';
  //     }
  //   });
  // }

  // public onEnterNewSupplierLookupDetails(): void {
  //   debugger;
  //   let supplierLookupList: any = {
  //     account_code: "",
  //     account_name: "",
  //     email: "",
  //     address1: "",
  //     address2: "",
  //     address3: "",
  //     gstn_no: ""
  //   };

  //   supplierLookupList = this.newSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objInvoiceRegister.supplier_code.toString().trim().toLowerCase());
  //   this.objInvoiceRegister.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objInvoiceRegister.supplier_code.toString().trim();
  //   this.objInvoiceRegister.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
  //   this.objInvoiceRegister.supplier_email = supplierLookupList && supplierLookupList.email ? supplierLookupList.email : '';
  //   let address1: string = supplierLookupList && supplierLookupList.address1 ? supplierLookupList.address1 : '';
  //   let address2: string = supplierLookupList && supplierLookupList.address2 ? supplierLookupList.address2 : '';
  //   let address3: string = supplierLookupList && supplierLookupList.address3 ? supplierLookupList.address3 : '';
  //   if (this.objInvoiceRegister.supplier_email === '')
  //     this.objInvoiceRegister.supplier_address = this.objInvoiceRegister.supplier_name + '\n' + address1 + '\n' + address2 + '\n' + address3;
  //   else if (this.objInvoiceRegister.supplier_email !== '')
  //     this.objInvoiceRegister.supplier_address = this.objInvoiceRegister.supplier_name + '\n' + address1 + '\n' + address2 + '\n' + address3 + '\nEmail : ' + this.objInvoiceRegister.supplier_email;
  //   this.objInvoiceRegister.gstn_no = supplierLookupList && supplierLookupList.gstn_no ? supplierLookupList.gstn_no : '';
  // }

  public onEnterNewSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      email: "",
      address1: "",
      address2: "",
      address3: "",
      gstn_no: ""
    };

    supplierLookupList = this.newSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objInvoiceRegister.supplier_code.toString().trim().toLowerCase());
    this.objInvoiceRegister.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objInvoiceRegister.supplier_code.toString().trim();
    this.objInvoiceRegister.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
    this.objInvoiceRegister.supplier_email = supplierLookupList && supplierLookupList.email ? supplierLookupList.email : '';
    this.objInvoiceRegister.gstn_no = supplierLookupList && supplierLookupList.gstn_no ? supplierLookupList.gstn_no : '';
    let address1: string = supplierLookupList && supplierLookupList.address1 ? supplierLookupList.address1 : '';
    let address2: string = supplierLookupList && supplierLookupList.address2 ? supplierLookupList.address2 : '';
    let address3: string = supplierLookupList && supplierLookupList.address3 ? supplierLookupList.address3 : '';
    if (this.objInvoiceRegister.supplier_email === '')
      this.objInvoiceRegister.supplier_address = this.objInvoiceRegister.supplier_name + '\n' + address1 + ' ' + address2 + ' ' + address3 + '\nGSTIN No : ' + this.objInvoiceRegister.gstn_no;
    else if (this.objInvoiceRegister.supplier_email !== '')
      this.objInvoiceRegister.supplier_address = this.objInvoiceRegister.supplier_name + '\n' + address1 + ' ' + address2 + ' ' + address3 + '\nEmail : ' + this.objInvoiceRegister.supplier_email + '\nGSTIN No : ' + this.objInvoiceRegister.gstn_no;
    this.objInvoiceRegister.supplier_address = supplierLookupList && supplierLookupList.gstn_no ? this.objInvoiceRegister.supplier_address : ''
    // if (!this.validateSupplierCode())
    //   this.resetGstValues();
    // this.checkIsRegisteredSupplier();
    // this.getSupplierDescList();
  }
  public checkValidSupplierName(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objInvoiceRegister.supplier_code.toString().trim() !== '' && this.objInvoiceRegister.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier code", "Invoice Register", focus);
    }
  }

  public validateSupplierCode(): boolean {
    let index = this.newSupplierLookupList.findIndex(element => element.account_code.toString().trim() === this.objInvoiceRegister.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  openAlertDialog(value: string, componentName?: string, focus?: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        document.getElementById(focus).focus();
        _dialogRef = null;
      }
      _dialogRef = null;
    });
  }


  // public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {

  //   if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
  //     const pattern = /^[0-9]*$/;
  //     if (pattern.test(event.key)) {
  //       this[key1][key2] = "";
  //       event.target.value = "";
  //     }
  //     return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
  //   } else {
  //     return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
  //   }
  // }

  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Supplier Code": this.dataSource.data[i].supplier_code,
          "Supplier Name": this.dataSource.data[i].supplier_name,
          "Supplier Invoice No": this.dataSource.data[i].invoice_no,
          "Supplier Invoice Date": this._datePipe.transform(this.dataSource.data[i].invoice_date, 'dd/MM/yyyy'),
          "GRN No": this.dataSource.data[i].grn_no,
          "Eway Bill No": this.dataSource.data[i].ewaybillno,
          "Invoice Entry Date": this._datePipe.transform(this.dataSource.data[i].entered_date, 'dd/MM/yyyy'),
          "Sub Total": this.dataSource.data[i].sub_total,
          "Others": this.dataSource.data[i].other_amount,
          "Taxable Value": this.dataSource.data[i].taxable_amount,
          "CGST": this.dataSource.data[i].cgst_amount,
          "SGST": this.dataSource.data[i].sgst_amount,
          "IGST": this.dataSource.data[i].igst_amount,
        });
      }
      this._excelService.exportAsExcelFile(json, "Invoice Register", datetime);
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found", "Invoice Register");
  }

  exportToPdf(): void {

    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.supplier_code);
        tempObj.push(e.supplier_name);
        tempObj.push(e.invoice_no);
        tempObj.push(this._datePipe.transform(e.invoice_date, 'dd/MM/yyyy'));
        tempObj.push(e.grn_no);
        tempObj.push(this._datePipe.transform(e.entered_date, 'dd//MM/yyyy'));
        tempObj.push(e.sub_total);
        tempObj.push(e.other_amount);
        tempObj.push(e.taxable_amount);
        tempObj.push(e.cgst_amount);
        tempObj.push(e.sgst_amount);
        tempObj.push(e.igst_amount);
        prepare.push(tempObj);
      });

      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Supplier Code", "Supplier Name", "Supplier Invoice No", "Supplier Invoice Date", "GRN No", "Eway Bill No",
          "Invoice Entry Date", "Sub Total", "Others", "Taxable Value", "CGST", "SGST", "IGST"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('Invoice Register' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Invoice Register");
  }
  public SuplierCodeFilter(searchValue: string, event: KeyboardEvent | any): void {
    searchValue = searchValue.toString().trim() ? searchValue.toString().toLocaleLowerCase() : "";
    let filteredOptimalList = [];
    if (event.keyCode === 8 || event.keyCode === 46) {
      filteredOptimalList = this.InvoiceRegisterList.filter((item: any) =>
        item.supplier_code.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    } else {
      filteredOptimalList = this.dataSource.data.filter((item: any) =>
        item.supplier_code.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    }
    this.matTableConfig(filteredOptimalList);
  }
  public SuplierNameFilter(searchValue: string, event: KeyboardEvent | any): void {
    searchValue = searchValue.toString().trim() ? searchValue.toString().toLocaleLowerCase() : "";
    let filteredOptimalList = [];
    if (event.keyCode === 8 || event.keyCode === 46) {
      filteredOptimalList = this.InvoiceRegisterList.filter((item: any) =>
        item.supplier_name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    } else {
      filteredOptimalList = this.dataSource.data.filter((item: any) =>
        item.supplier_name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    }
    this.matTableConfig(filteredOptimalList);
  }
  public SuplierInvoiceNoFilter(searchValue: string, event: KeyboardEvent | any): void {
    searchValue = searchValue.toString().trim() ? searchValue.toString().toLocaleLowerCase() : "";
    let filteredOptimalList = [];
    if (event.keyCode === 8 || event.keyCode === 46) {
      filteredOptimalList = this.InvoiceRegisterList.filter((item: any) =>
        item.invoice_no.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    } else {
      filteredOptimalList = this.dataSource.data.filter((item: any) =>
        item.invoice_no.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    }
    this.matTableConfig(filteredOptimalList);
  }
  public SuplierInvoiceDateFilter(searchValue: string, event: KeyboardEvent | any): void {
    searchValue = searchValue.toString().trim() ? searchValue.toString().toLocaleLowerCase() : "";
    let filteredOptimalList = [];
    if (event.keyCode === 8 || event.keyCode === 46) {
      filteredOptimalList = this.InvoiceRegisterList.filter((item: any) =>
        item.invoice_date.toLocaleLowerCase().startswith(searchValue.toLocaleLowerCase()));
    } else {
      filteredOptimalList = this.dataSource.data.filter((item: any) =>
        item.invoice_date.toLocaleLowerCase().startswith(searchValue.toLocaleLowerCase()));
    }
    this.matTableConfig(filteredOptimalList);
  }
}
