import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { EditMode } from 'src/app/common/models/common-model';
import { AccountsLookupService } from 'src/app/common/services/accounts-lookup/accounts-lookup.service';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductGroupLookupComponent } from 'src/app/common/shared/product-group-lookup/product-group-lookup.component';
import { SingleFileAttachComponent } from 'src/app/common/shared/single-file-attach/single-file-attach.component';
import { Flags, InvoiceDetails, InvoicePoNos, Invoices, InvoicesLoad, PendingInvoiceNos, SupplierDescription } from '../../../model/purchases/invoices-model';
import { ProductGroupAttributesService } from '../../../services/masters/product-group-attributes/product-group-attributes.service';
import { ByAmountService } from '../../../services/pricing/selling-price-change/single-product/double-rate/by-amount/by-amount.service';
import { InvoicesService } from '../../../services/purchases/invoices/invoices.service';
import { EditCostPriceComponent } from '../edit-cost-price/edit-cost-price.component';
declare var jsPDF: any;

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class InvoicesComponent implements OnInit {
  invByno: string = '';
  focusFlag: boolean;
  componentVisibility: boolean = true;
  supplierDescList: SupplierDescription[] = [];
  newSupplierLookupList: any = [];
  loadSupplierLookupList: any = [];
  productGroupLookupList: any = [];
  invoiceNosList: PendingInvoiceNos[] = [];
  poNosList: InvoicePoNos[] = []

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  fromDate1: any = new Date();
  toDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  dataSource = new MatTableDataSource([]);
  invoicesList: any[] = [];
  displayedColumns = ["Serial_No", "By_No", "Invoice_No", "Invoice_Date", "Supplier_Name", "Invoice_Amount", "Actual_Amount", "Details", "Action"];

  flags: Flags = {
    isRegistered: false,
    isIgst: true,
    isComposite: false,
    isDiscountAmount: false,
    isSupplierDesc: false,
    isMrp: false,
    isDryWash: false,
    isAddMargin: false,
  }

  modifyFlags: Flags = {
    isRegistered: false,
    isIgst: true,
    isComposite: false,
    isDiscountAmount: false,
    isSupplierDesc: false,
    isMrp: false,
    isDryWash: false,
    isAddMargin: false,
  }

  unChangedFlags: Flags = {
    isRegistered: false,
    isIgst: true,
    isComposite: false,
    isDiscountAmount: false,
    isSupplierDesc: false,
    isMrp: false,
    isDryWash: false,
    isAddMargin: false,
  }

  objAction: EditMode = {
    isEditing: false,
    isView: false,
  }

  objUnchangedAction: EditMode = {
    isEditing: false,
    isView: false,
  }

  objInvoicesLoad: InvoicesLoad = {
    company_section_name: this._localStorage.getCompanySectionName(),
    from_inv_entry_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    to_inv_entry_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    status: 'For Approval',
    supplier_code: '',
    supplier_name: '',
    all_suppliers: true,
    inv_byno: '',
    is_mismatched_invoices: false
  }

  objInvoices: Invoices = {
    inv_byno: '',
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    supplier_gstn_no: '',
    supplier_email: '',
    entered_gstn_no: '',
    supplier_invoice_no: '',
    supplier_invoice_date: new Date(),
    invoice_amount: '0.00',
    invoice_actual_amount: 0,
    grn_no: 0,
    po_id: 0,
    purchase_order_no: '',
    invoice_entry_date: new Date(),
    master_discount_percent: 0,
    master_discount_amount: 0,
    credit_days: 15,
    special_discount_percent: 0,
    special_discount_amount: 0,
    tcs_percent: 0,
    tcs_percent_amount: 0,
    tcs_amount: '0.00',
    actual_tcs_amount: '0.00',
    irn: '',
    product_total: 0,
    actual_product_total: 0,
    discount_total: '0.00',
    actual_discount_total: 0,
    freight_charges: '0.00',
    cgst_total: '0.00',
    actual_cgst_total: '0.00',
    sgst_total: '0.00',
    actual_sgst_total: '0.00',
    igst_total: '0.00',
    actual_igst_total: '0.00',
    round_off_total: 0,
    actual_round_off_total: 0,
    grand_total: '0.00',
    actual_grand_total: '0.00',
    additions: '0.00',
    deductions: '0.00',
    merchandiser_name: '',
    qty_total: 0,
    undeducted_product_total: 0,
    actual_undeducted_product_total: 0,
    include_freight_in_tax: false,
    include_additions_in_tax: false,
    include_deductions_in_tax: false,
  }

  objModifyInvoices: Invoices = {
    inv_byno: '',
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    supplier_gstn_no: '',
    supplier_email: '',
    entered_gstn_no: '',
    supplier_invoice_no: '',
    supplier_invoice_date: new Date(),
    invoice_amount: '0.00',
    invoice_actual_amount: 0,
    grn_no: 0,
    po_id: 0,
    purchase_order_no: '',
    invoice_entry_date: new Date(),
    master_discount_percent: 0,
    master_discount_amount: 0,
    credit_days: 15,
    special_discount_percent: 0,
    special_discount_amount: 0,
    tcs_percent: 0,
    tcs_percent_amount: 0,
    tcs_amount: '0.00',
    actual_tcs_amount: '0.00',
    irn: '',
    product_total: 0,
    actual_product_total: 0,
    discount_total: '0.00',
    actual_discount_total: 0,
    freight_charges: '0.00',
    cgst_total: '0.00',
    actual_cgst_total: '0.00',
    sgst_total: '0.00',
    actual_sgst_total: '0.00',
    igst_total: '0.00',
    actual_igst_total: '0.00',
    round_off_total: 0,
    actual_round_off_total: 0,
    grand_total: '0.00',
    actual_grand_total: '0.00',
    additions: '0.00',
    deductions: '0.00',
    merchandiser_name: '',
    qty_total: 0,
    undeducted_product_total: 0,
    actual_undeducted_product_total: 0,
    include_freight_in_tax: false,
    include_additions_in_tax: false,
    include_deductions_in_tax: false,
  }

  objUnChangedInvoices: Invoices = {
    inv_byno: '',
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    supplier_gstn_no: '',
    supplier_email: '',
    entered_gstn_no: '',
    supplier_invoice_no: '',
    supplier_invoice_date: new Date(),
    invoice_amount: '0.00',
    invoice_actual_amount: 0,
    grn_no: 0,
    po_id: 0,
    purchase_order_no: '',
    invoice_entry_date: new Date(),
    master_discount_percent: 0,
    master_discount_amount: 0,
    credit_days: 15,
    special_discount_percent: 0,
    special_discount_amount: 0,
    tcs_percent: 0,
    tcs_percent_amount: 0,
    tcs_amount: '0.00',
    actual_tcs_amount: '0.00',
    irn: '',
    product_total: 0,
    actual_product_total: 0,
    discount_total: '0.00',
    actual_discount_total: 0,
    freight_charges: '0.00',
    cgst_total: '0.00',
    actual_cgst_total: '0.00',
    sgst_total: '0.00',
    actual_sgst_total: '0.00',
    igst_total: '0.00',
    actual_igst_total: '0.00',
    round_off_total: 0,
    actual_round_off_total: 0,
    grand_total: '0.00',
    actual_grand_total: '0.00',
    additions: '0.00',
    deductions: '0.00',
    merchandiser_name: '',
    qty_total: 0,
    undeducted_product_total: 0,
    actual_undeducted_product_total: 0,
    include_freight_in_tax: false,
    include_additions_in_tax: false,
    include_deductions_in_tax: false,
  }

  invoiceDetailsList: InvoiceDetails[] = [];
  unChangedInvoiceDetailsList: InvoiceDetails[] = [{
    inv_byno: '',
    byno_serial: 0,
    product_group_id: 0,
    product_group_name: '',
    product_group_uom: '',
    product_qty: 0,
    actual_product_qty: 0,
    no_of_pcs: 0,
    actual_no_of_pcs: 0,
    cost_price: 0,
    actual_cost_price: 0,
    discount_amount: 0,
    discount_percent: 0,
    discount_percent_amt: 0,
    price_code: '',
    add_margin: '',
    supplier_desc: '',
    dry_wash: false,
    mrp: 0,
    hsn_code: '',
    sgst: 0,
    actual_sgst: 0,
    sgst_amount: 0,
    actual_sgst_amount: 0,
    cgst: 0,
    actual_cgst: 0,
    cgst_amount: 0,
    actual_cgst_amount: 0,
    igst: 0,
    actual_igst: 0,
    igst_amount: 0,
    actual_igst_amount: 0,
    tax_percentage: 0,
    round_off: 0,
    actual_round_off: 0,
    total_taxable_value: '0.00',
    actual_total_taxable_value: '0.00',
    productGroupFocus: false,
    profit_percentage: 0,
    selling_price: 0
  }];

  modifyInvoiceDetailsList: InvoiceDetails[] = [{
    inv_byno: '',
    byno_serial: 0,
    product_group_id: 0,
    product_group_name: '',
    product_group_uom: '',
    product_qty: 0,
    actual_product_qty: 0,
    no_of_pcs: 0,
    actual_no_of_pcs: 0,
    cost_price: 0,
    actual_cost_price: 0,
    discount_amount: 0,
    discount_percent: 0,
    discount_percent_amt: 0,
    price_code: '',
    add_margin: '',
    supplier_desc: '',
    dry_wash: false,
    mrp: 0,
    hsn_code: '',
    sgst: 0,
    actual_sgst: 0,
    sgst_amount: 0,
    actual_sgst_amount: 0,
    cgst: 0,
    actual_cgst: 0,
    cgst_amount: 0,
    actual_cgst_amount: 0,
    igst: 0,
    actual_igst: 0,
    igst_amount: 0,
    actual_igst_amount: 0,
    tax_percentage: 0,
    round_off: 0,
    actual_round_off: 0,
    total_taxable_value: '0.00',
    actual_total_taxable_value: '0.00',
    productGroupFocus: false,
    profit_percentage: 0,
    selling_price: 0
  }];

  objInvoiceDetails: InvoiceDetails = {
    inv_byno: '',
    byno_serial: 0,
    product_group_id: 0,
    product_group_name: '',
    product_group_uom: '',
    product_qty: 0,
    actual_product_qty: 0,
    no_of_pcs: 0,
    actual_no_of_pcs: 0,
    cost_price: 0,
    actual_cost_price: 0,
    discount_amount: 0,
    discount_percent: 0,
    discount_percent_amt: 0,
    price_code: '',
    add_margin: '',
    supplier_desc: '',
    dry_wash: false,
    mrp: 0,
    hsn_code: '',
    sgst: 0,
    actual_sgst: 0,
    sgst_amount: 0,
    actual_sgst_amount: 0,
    cgst: 0,
    actual_cgst: 0,
    cgst_amount: 0,
    actual_cgst_amount: 0,
    igst: 0,
    actual_igst: 0,
    igst_amount: 0,
    actual_igst_amount: 0,
    tax_percentage: 0,
    round_off: 0,
    actual_round_off: 0,
    total_taxable_value: 0,
    actual_total_taxable_value: '0.00',
    productGroupFocus: false,
    profit_percentage: 0,
    selling_price: 0
  }

  myControl = new FormControl();
  filteredOptions: Observable<SupplierDescription[]>;

  columns: any[] = [
    { display: 'sNo', editable: false },
    { display: 'productGroup', editable: true },
    { display: 'productGroupUOM', editable: false },
    { display: "qty", editable: true },
    { display: "actualQty", editable: false },
    { display: "pcs", editable: true },
    { display: "actualPcs", editable: false },
    { display: "costPrice", editable: true },
    { display: "actualCostPrice", editable: false },
    { display: "discountAmt", editable: true },
    { display: "discountPercent", editable: true },
    { display: "priceCode", editable: true },
    { display: "addMargin", editable: true },
    { display: "supplierDesc", editable: true },
    { display: "dryWash", editable: true },
    { display: "mrp", editable: true },
    { display: "hsnCode", editable: true },
    { display: "sgst", editable: false },
    { display: "cgst", editable: false },
    { display: "igst", editable: true },
    { display: "roundOff", editable: true },
    { display: "taxableValue", editable: false },
    { display: "action", editable: true }
  ];

  @ViewChildren('productGroup') productGroup: ElementRef | any;
  @ViewChildren('productGroupUOM') productGroupUOM: ElementRef | any;
  @ViewChildren('qty') qty: ElementRef | any;
  @ViewChildren('actualQty') actualQty: ElementRef | any;
  @ViewChildren('pcs') pcs: ElementRef | any;
  @ViewChildren('actualPcs') actualPcs: ElementRef | any;
  @ViewChildren('costPrice') costPrice: ElementRef | any;
  @ViewChildren('actualCostPrice') actualCostPrice: ElementRef | any;
  @ViewChildren('discountAmt') discountAmt: ElementRef | any;
  @ViewChildren('discountPercent') discountPercent: ElementRef | any;
  @ViewChildren('priceCode') priceCode: ElementRef | any;
  @ViewChildren('addMargin') addMargin: ElementRef | any;
  @ViewChildren('supplierDesc') supplierDesc: ElementRef | any;
  @ViewChildren('dryWash') dryWash: ElementRef | any;
  @ViewChildren('mrp') mrp: ElementRef | any;
  @ViewChildren('hsnCode') hsnCode: ElementRef | any;
  @ViewChildren('sgst') sgst: ElementRef | any;
  @ViewChildren('cgst') cgst: ElementRef | any;
  @ViewChildren('igst') igst: ElementRef | any;
  @ViewChildren('roundOff') roundOff: ElementRef | any;
  @ViewChildren('taxableValue') taxableValue: ElementRef | any;
  @ViewChildren('action') action: ElementRef | any;

  @ViewChildren('newSupplier') newSupplier: ElementRef | any;
  @ViewChildren('supplierInvoiceNo') supplierInvoiceNo: ElementRef | any;
  @ViewChildren('invoiceAmount') invoiceAmount: ElementRef | any;
  @ViewChildren('discountTotal') discountTotal: ElementRef | any;
  @ViewChildren('freightCharges') freightCharges: ElementRef | any;


  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    private _datePipe: DatePipe,
    public _matDialog: MatDialog,
    private _decimalPipe: DecimalPipe,
    private _excelService: ExcelService,
    private _gridKeyEvents: GridKeyEvents,
    private _keyPressEvents: KeyPressEvents,
    private _invoicesService: InvoicesService,
    private _byAmountService: ByAmountService,
    private _confirmationDialog: ConfirmationDialogComponent,
    private _accountsLookupService: AccountsLookupService,
    public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    private _productGroupAttributeService: ProductGroupAttributesService
  ) {
    this.objInvoicesLoad.from_inv_entry_date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
    debugger;
    let objInvoiceView: any = JSON.parse(localStorage.getItem('InvoiceByNoForInvoiceView'));
    if (objInvoiceView) {
      this.resetToInvoiceViewScreen(objInvoiceView);
      // this.fetchInvoices(0, false, objInvoiceView.view, true);
    } else {
      this.getSupplierLookupList();
      this.getProductGroupLookupList();
      this.addNewRowToInvoiceDetails();
    }
  }

  private resetToInvoiceViewScreen(objInvoiceView): void {
    if (objInvoiceView) {
      this.objAction.isView = objInvoiceView.view;
      this.newSupplierLookupList = JSON.parse(JSON.stringify(objInvoiceView.newSupplierLookupList));
      this.productGroupLookupList = JSON.parse(JSON.stringify(objInvoiceView.productGroupLookupList));
      this.invoiceNosList = JSON.parse(JSON.stringify(objInvoiceView.invoiceNosList));
      this.poNosList = JSON.parse(JSON.stringify(objInvoiceView.poNosList));
      this.objInvoices = JSON.parse(JSON.stringify(objInvoiceView.objInvoices));
      this.objModifyInvoices = JSON.parse(JSON.stringify(objInvoiceView.objInvoices));
      this.invoiceDetailsList = JSON.parse(JSON.stringify(objInvoiceView.invoiceDetailsList));
      this.modifyInvoiceDetailsList = JSON.parse(JSON.stringify(objInvoiceView.invoiceDetailsList));
      this.flags = JSON.parse(JSON.stringify(objInvoiceView.flags));
      this.modifyFlags = JSON.parse(JSON.stringify(objInvoiceView.flags))
      this.componentVisibility = false;
    }
  }

  ngOnInit() {

  }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number, isDetailsColumn?: boolean): void {
    // if (this.checkIsValidRow(rowIndex)) {
    switch (event.keyCode) {
      case 9:
        this.focusInAndOutOfTable(event, isDetailsColumn, rowIndex);
        break;
      case 35:
        this.focusInAndOutOfTable(event, isDetailsColumn, rowIndex);
        break;
      case 13: // Enter Key
        if (!this.invoiceDetailsList[rowIndex].productGroupFocus) {
          if (this.checkValidDetails(rowIndex, colIndex)) {
            let response = this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.invoiceDetailsList);
            if (response && this.checkValidDetails(rowIndex, colIndex))
              this.addNewRowToInvoiceDetails();
          }
        }
        break;

      case 37: // Arrow Left
        if (this.checkValidDetails(rowIndex, colIndex)) {
          this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.invoiceDetailsList);
          this.invoiceDetailsList[rowIndex].productGroupFocus = false;
        }
        break;

      case 38: // Arrow Up
        if (this.checkValidDetails(rowIndex, colIndex)) {
          this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
          this.invoiceDetailsList[rowIndex].productGroupFocus = false;
        }
        break;

      case 39: // Arrow Right
        if (this.checkValidDetails(rowIndex, colIndex)) {
          this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.invoiceDetailsList);
          this.invoiceDetailsList[rowIndex].productGroupFocus = false;
        }
        break;

      case 40: // Arrow Down
        if (this.checkValidDetails(rowIndex, colIndex)) {
          this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.invoiceDetailsList);
          this.invoiceDetailsList[rowIndex].productGroupFocus = false;
        }
        break;
    }
    // }
  }

  public focusInAndOutOfTable(event: KeyboardEvent | any, isDetailsColumn?: boolean, i?: number): void {
    debugger;
    if (event.keyCode === 9 || event.keyCode === 35) {
      if (isDetailsColumn) {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(this.objInvoices.discount_total) === -1) {
          let input = this.discountTotal.toArray();
          input[input.length - 1].nativeElement.focus();
        } else {
          let input = this.freightCharges.toArray();
          input[input.length - 1].nativeElement.focus();
        }
        this.invoiceDetailsList[i].productGroupFocus = false;
      }
    } else if (event.keyCode === 36) {
      let input = this.productGroup.toArray();
      input[0].nativeElement.focus();
      this.invoiceDetailsList[0].productGroupFocus = false;
    }
  }

  private _filter(supplier_description: string): SupplierDescription[] {
    const filterValue = supplier_description.toLowerCase();
    return this.supplierDescList.filter(option => option.supplier_description.toLowerCase().indexOf(filterValue) === 0);
  }

  public displayFn(supplierDescId?: number): string {
    debugger;
    if (supplierDescId) {
      const supplierDesc = this.supplierDescList.find(f => f.supplier_description_id === supplierDescId);
      return supplierDesc && supplierDesc.supplier_description ? supplierDesc.supplier_description : '';
    }
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.objInvoicesLoad.from_inv_entry_date, this.fromDate1, this.minDate, this.maxDate);
    this.objInvoicesLoad.from_inv_entry_date = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.objInvoicesLoad.to_inv_entry_date, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.objInvoicesLoad.to_inv_entry_date = date[0];
    this.toDate1 = date[1];
  }

  /************************************************* Lookup Functionalities **************************************************/

  private getSupplierLookupList(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._accountsLookupService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      debugger;
      if (result) {
        this.newSupplierLookupList = JSON.parse(result);
        this.loadSupplierLookupList = JSON.parse(result);
        this.newSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
        this.loadSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
      }
    });
  }

  public openNewSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierCode()) {
      this.openNewSupplierLookupDialog();
    }
  }

  private openNewSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "550px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objInvoices.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objInvoices.supplier_code = result.account_code.toString().trim();
        this.objInvoices.supplier_name = result.account_name.toString().trim();
        this.objInvoices.supplier_email = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(result.email) === -1 ? result.email : '';
        this.objInvoices.supplier_gstn_no = result.gstn_no;
        this.objInvoices.entered_gstn_no = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_gstn_no) === -1 ? this.objInvoices.supplier_gstn_no : '';
        if (this.objInvoices.supplier_email === '')
          this.objInvoices.supplier_address = result.account_name + '\n' + result.address1 + ' ' + result.address2 + ' ' + result.address3 + '\nGSTIN No : ' + this.objInvoices.supplier_gstn_no;
        else if (this.objInvoices.supplier_email !== '')
          this.objInvoices.supplier_address = result.account_name + '\n' + result.address1 + ' ' + result.address2 + ' ' + result.address3 + '\nEmail : ' + this.objInvoices.supplier_email + '\nGSTIN No : ' + this.objInvoices.supplier_gstn_no;
        this.getOtherDetailsBasedOnSupplier();
      } else {
        this.objInvoices.supplier_code = '';
        this.objInvoices.supplier_name = '';
        this.objInvoices.supplier_email = '';
        this.objInvoices.supplier_address = '';
        this.objInvoices.supplier_gstn_no = '';
        this.objInvoices.entered_gstn_no = '';
        this.resetGstValues();
      }
    });
  }

  public onEnterNewSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      email: "",
      address1: "",
      address2: "",
      address3: "",
      gstn_no: ""
    };

    supplierLookupList = this.newSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objInvoices.supplier_code.toString().trim().toLowerCase());
    this.objInvoices.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objInvoices.supplier_code.toString().trim();
    this.objInvoices.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
    this.objInvoices.supplier_email = supplierLookupList && supplierLookupList.email ? supplierLookupList.email : '';
    this.objInvoices.supplier_gstn_no = supplierLookupList && supplierLookupList.gstn_no ? supplierLookupList.gstn_no : '';
    let address1: string = supplierLookupList && supplierLookupList.address1 ? supplierLookupList.address1 : '';
    let address2: string = supplierLookupList && supplierLookupList.address2 ? supplierLookupList.address2 : '';
    let address3: string = supplierLookupList && supplierLookupList.address3 ? supplierLookupList.address3 : '';
    if (this.objInvoices.supplier_email === '')
      this.objInvoices.supplier_address = this.objInvoices.supplier_name + '\n' + address1 + ' ' + address2 + ' ' + address3 + '\nGSTIN No : ' + this.objInvoices.supplier_gstn_no;
    else if (this.objInvoices.supplier_email !== '')
      this.objInvoices.supplier_address = this.objInvoices.supplier_name + '\n' + address1 + ' ' + address2 + ' ' + address3 + '\nEmail : ' + this.objInvoices.supplier_email + '\nGSTIN No : ' + this.objInvoices.supplier_gstn_no;
    this.objInvoices.supplier_address = supplierLookupList && supplierLookupList.gstn_no ? this.objInvoices.supplier_address : ''
    this.objInvoices.entered_gstn_no = supplierLookupList && supplierLookupList.gstn_no ? this.objInvoices.supplier_gstn_no : '';
    if (!this.validateSupplierCode())
      this.resetGstValues();
    this.getOtherDetailsBasedOnSupplier();
  }

  public openLoadSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateLoadSupplierCode()) {
      this.openLoadSupplierLookupDialog();
    }
  }

  private openLoadSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "550px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objInvoicesLoad.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      this.matTableConfig();
      if (result) {
        debugger;
        this.objInvoicesLoad.supplier_code = result.account_code.toString().trim();
        this.objInvoicesLoad.supplier_name = result.account_name.toString().trim();
      } else {
        this.objInvoicesLoad.supplier_code = '';
        this.objInvoicesLoad.supplier_name = '';
      }
    });
  }

  public onEnterLoadSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
    };
    this.matTableConfig();
    supplierLookupList = this.loadSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objInvoicesLoad.supplier_code.toString().trim().toLowerCase());
    this.objInvoicesLoad.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objInvoicesLoad.supplier_code.toString().trim();
    this.objInvoicesLoad.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
  }


  private getOtherDetailsBasedOnSupplier(isModify?: boolean): void {
    this.checkIsRegisteredSupplier(isModify);
    this.getSupplierDescList();
    this.getInvoiceNos(isModify);
    this.getMasterDiscount(isModify);
    this.getInvoicePoNos(isModify);
    this.setTypeOfGst();
  }

  public checkValidLoadSupplierName(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objInvoicesLoad.supplier_code.toString().trim() !== '' && this.objInvoicesLoad.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier name", "Invoices In Bale", focus);
    }
  }

  public validateLoadSupplierCode(): boolean {
    let index = this.loadSupplierLookupList.findIndex(element => element.account_code.toString().trim() === this.objInvoicesLoad.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public checkValidSupplierName(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objInvoices.supplier_code.toString().trim() !== '' && this.objInvoices.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier name", "Invoices In Bale", focus);
    }
  }

  public validateSupplierCode(): boolean {
    let index = this.newSupplierLookupList.findIndex(element => element.account_code.toString().trim() === this.objInvoices.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  private checkIsRegisteredSupplier(isModify?: boolean): void {
    debugger;
    this.flags.isRegistered = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_gstn_no) === -1 ? true : false;
    if (isModify)
      this.modifyFlags.isRegistered = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objModifyInvoices.supplier_gstn_no) === -1 ? true : false;
  }

  public checkIsValidSupplier(): void {
    if (!this.validateSupplierCode())
      this.openAlertDialog('Select supplier', 'Invoices', 'newSupplier');
  }

  private getProductGroupLookupList(): void {
    let objDetails: any = {
      GetProductGroupLookup: JSON.stringify([{
        group_section_id: +this._localStorage.getGlobalGroupSectionId()
      }])
    }
    this._productGroupAttributeService.GetProductGroupLookupDetails(objDetails).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0) {
        debugger;
        this.productGroupLookupList = JSON.parse(result);
      }
    });
  }

  public openProductGroupLookup(event: any, i: number): void {
    if (event.keyCode === 45) {
      this.invoiceDetailsList[i].productGroupFocus = false;
      if (JSON.stringify(this.invoiceDetailsList[i]) !== JSON.stringify(this.objInvoiceDetails) && this.validateProductGroupName(i) && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.invoiceDetailsList[i].total_taxable_value) === -1)
        this.addMiddleRowToInvoiceDetails(i);
    } else if (event.keyCode === 46) {
      debugger;
      this.invoiceDetailsList[i].productGroupFocus = false;
      this.invoiceDetailsList[i].total_taxable_value = +this.invoiceDetailsList[i].total_taxable_value;
      if (JSON.stringify(this.invoiceDetailsList[i]) === JSON.stringify(this.objInvoiceDetails) && this.invoiceDetailsList.length > 1)
        this.deleteRow(i);
      else if (JSON.stringify(this.invoiceDetailsList[i]) !== JSON.stringify(this.objInvoiceDetails) && this.invoiceDetailsList.length > 1) {
        this.focusFlag = true;
        this.openRemoveConfirmationDialog('Are you sure want to remove this row from the list', 'Invoices', i)
      }
    } else if (event.keyCode === 13 && this.validateProductGroupName(i)) {
      this.invoiceDetailsList[i].productGroupFocus = false;
    } else if (event.keyCode === 13 && !this.validateProductGroupName(i)) {
      this.openProductGroupLookupDialog(i);
    }
  }

  private openProductGroupLookupDialog(i: number): void {
    this.focusFlag = true;
    this.invoiceDetailsList[i].productGroupFocus = true;
    const dialogRef = this._matDialog.open(ProductGroupLookupComponent, {
      width: "500px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.invoiceDetailsList[i].product_group_name
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      this.invoiceDetailsList[i].productGroupFocus = false;
      if (result) {
        this.invoiceDetailsList[i].product_group_id = result.product_group_id;
        this.invoiceDetailsList[i].product_group_name = result.product_group_name;
        this.invoiceDetailsList[i].product_group_uom = result.product_group_uom;
        if (+this.invoiceDetailsList[i].product_group_uom === 1) {
          this.invoiceDetailsList[i].product_group_uom = 'No.s';
          this.setEditableTrueOrFalse('pcs', false);
        }
        else if (+this.invoiceDetailsList[i].product_group_uom === 2) {
          this.invoiceDetailsList[i].product_group_uom = 'Mt.s';
          this.setEditableTrueOrFalse('pcs', true);
        }
      } else {
        this.invoiceDetailsList[i].product_group_id = 0;
        this.invoiceDetailsList[i].product_group_name = '';
        this.invoiceDetailsList[i].product_group_uom = '';
      }
    });
  }

  public onEnterProductGroupLookupDetails(i: number): void {
    let productGroupList: any = {
      product_group_id: 0,
      product_group_name: "",
      product_group_uom: ''
    }
    productGroupList = this.productGroupLookupList.find(ele => ele.product_group_name.toString().toLowerCase().trim() ===
      this.invoiceDetailsList[i].product_group_name.toString().trim().toLowerCase());
    this.invoiceDetailsList[i].product_group_id = productGroupList && productGroupList.product_group_id ? productGroupList.product_group_id : 0;
    this.invoiceDetailsList[i].product_group_name = productGroupList && productGroupList.product_group_name ? productGroupList.product_group_name : this.invoiceDetailsList[i].product_group_name;
    this.invoiceDetailsList[i].product_group_uom = productGroupList && productGroupList.product_group_uom ? productGroupList.product_group_uom : '';
    if (+ this.invoiceDetailsList[i].product_group_uom === 1) {
      this.invoiceDetailsList[i].product_group_uom = 'No.s';
      this.setEditableTrueOrFalse('pcs', false);
    } else if (+this.invoiceDetailsList[i].product_group_uom === 2) {
      this.invoiceDetailsList[i].product_group_uom = 'Mt.s';
      this.setEditableTrueOrFalse('pcs', true);
    }
    this.invoiceDetailsList[i].productGroupFocus = productGroupList && productGroupList.product_group_uom ? false : true;
  }

  public validateProductGroupName(i: number): boolean {
    let index = this.productGroupLookupList.findIndex(element => element.product_group_name.toString().trim() === this.invoiceDetailsList[i].product_group_name.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public checkValidProductGroupName(i: number, event: KeyboardEvent): void {
    if (event.keyCode !== 13 && !this.focusFlag && this.invoiceDetailsList[i].product_group_name.toString().trim() !== '' && +this.invoiceDetailsList[i].product_group_id === 0)
      this.openInvalidAlertDialog("Invalid product group", i, "Invoices", 'productGroup');
  }

  /*************************************************** CRUD Operations *******************************************************/

  public setActualQuantity(i: number): void {
    this.invoiceDetailsList[i].actual_product_qty = this.invoiceDetailsList[i].product_qty;
    this.setNoOfPcs(i);
  }

  private setNoOfPcs(i: number): void {
    if (this.invoiceDetailsList[i].product_group_uom === 'No.s') {
      this.invoiceDetailsList[i].no_of_pcs = +this.invoiceDetailsList[i].product_qty;
      this.invoiceDetailsList[i].actual_no_of_pcs = +this.invoiceDetailsList[i].actual_product_qty;
      this.setEditableTrueOrFalse('pcs', false);
    }
    this.calculateTaxableValue(i);
  }

  public setProductFocus(i: number): void {
    this.invoiceDetailsList[i].productGroupFocus = true;
  }

  public resetProductFocus(i: number): void {
    this.invoiceDetailsList[i].productGroupFocus = false;
  }

  public setActualPcs(i: number): void {
    this.invoiceDetailsList[i].actual_no_of_pcs = this.invoiceDetailsList[i].no_of_pcs;
    this.setNoOfQty(i);
  }

  public setNoOfQty(i: number): void {
    if (this.invoiceDetailsList[i].product_group_uom === 'No.s') {
      this.invoiceDetailsList[i].product_qty = +this.invoiceDetailsList[i].no_of_pcs;
      this.invoiceDetailsList[i].actual_product_qty = +this.invoiceDetailsList[i].no_of_pcs;
      this.setEditableTrueOrFalse('qty', false);
    }
    this.calculateTaxableValue(i);
  }

  public setActualCostPrice(i: number): void {
    this.invoiceDetailsList[i].actual_cost_price = this.invoiceDetailsList[i].cost_price;
    this.calculateTaxableValue(i);
  }

  public setTypeOfGst(): void {
    let enteredGst: string = this.objInvoices.entered_gstn_no.substr(0, 2);
    if (+ enteredGst === +this._localStorage.getStateCode()) {
      this.flags.isIgst = false;
      this.setEditableTrueOrFalse('sgst', true);
      this.setEditableTrueOrFalse('cgst', true);
      this.setEditableTrueOrFalse('igst', false);
    }
    else {
      this.flags.isIgst = true;
      this.setEditableTrueOrFalse('sgst', false);
      this.setEditableTrueOrFalse('cgst', false);
      this.setEditableTrueOrFalse('igst', true);
    }
    this.resetGstValues();
  }

  public disableTaxableColumns(): void {
    debugger;
    this.resetGstValues();
    if (this.flags.isComposite) {
      this.setEditableTrueOrFalse('sgst', false);
      this.setEditableTrueOrFalse('cgst', false);
      this.setEditableTrueOrFalse('igst', false);
    } else if (!this.flags.isComposite) {
      this.setEditableTrueOrFalse('sgst', true);
      this.setEditableTrueOrFalse('cgst', true);
      this.setEditableTrueOrFalse('igst', true);
    }
  }

  public calculateTaxableValue(i: number): void {
    debugger;
    let deducted_total: number = this.getDeductedTotal(i);
    let gst_total: number = this.getGstTotal(i, deducted_total);
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(deducted_total) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(gst_total) === -1) {
      this.invoiceDetailsList[i].total_taxable_value = +deducted_total + +gst_total;
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.invoiceDetailsList[i].round_off) === -1) {
        if (this.invoiceDetailsList[i].round_off.toString().includes('.')) {
          if (+(this.invoiceDetailsList[i].round_off.toString().split('.')[1]) > 0) {
            this.invoiceDetailsList[i].total_taxable_value = +this.invoiceDetailsList[i].total_taxable_value + +(this.invoiceDetailsList[i].round_off);
          } else if (+(this.invoiceDetailsList[i].round_off.toString().split('.')[1]) < 0) {
            this.invoiceDetailsList[i].total_taxable_value = +this.invoiceDetailsList[i].total_taxable_value - +(this.invoiceDetailsList[i].round_off);
          } else if (+(this.invoiceDetailsList[i].round_off.toString().split('.')[1]) === 0) {
            this.invoiceDetailsList[i].total_taxable_value = +this.invoiceDetailsList[i].total_taxable_value + 0;
          }
        } else if (+(this.invoiceDetailsList[i].round_off.toString())) {
          this.invoiceDetailsList[i].total_taxable_value = +this.invoiceDetailsList[i].total_taxable_value + +(this.invoiceDetailsList[i].round_off);
        }
      }
      this.getQuantityTotal();
      this.getUnDeductedProductTotal();
      this.getGrandProductTotal();
      this.getDiscountGrandTotal();
      if (this.flags.isIgst)
        this.getIgstGrandTotal();
      else if (!this.flags.isIgst) {
        this.getCgstGrandTotal();
        this.getSgstGrandTotal();
      }
      this.getGrandTotal();
      this.convertToDecimal(i);
    }
  }

  private getDeductedTotal(i: number): number {
    debugger;
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.invoiceDetailsList[i].cost_price) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].discount_amount) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].discount_percent) === -1) {
      let total: number = +((+this.invoiceDetailsList[i].product_qty * +this.invoiceDetailsList[i].cost_price) - (+this.invoiceDetailsList[i].product_qty * +this.invoiceDetailsList[i].discount_amount));
      this.invoiceDetailsList[i].discount_percent_amt = +total * (+this.invoiceDetailsList[i].discount_percent / 100);
      return +(+total - +this.invoiceDetailsList[i].discount_percent_amt);
    } else return 0;
  }

  private getGstTotal(i: number, deductedTotal?: number): number {
    debugger;
    if (this.flags.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].igst) === -1) {
        this.invoiceDetailsList[i].igst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.invoiceDetailsList[i].igst) === -1 ? deductedTotal * (+this.invoiceDetailsList[i].igst / 100) : 0;
        return +this.invoiceDetailsList[i].igst_amount;
      }
    } else if (!this.flags.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].cgst) === -1 &&
        [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].sgst) === -1) {
        this.invoiceDetailsList[i].cgst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.invoiceDetailsList[i].cgst) === -1 ? deductedTotal * (+this.invoiceDetailsList[i].cgst / 100) : 0;
        this.invoiceDetailsList[i].sgst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.invoiceDetailsList[i].sgst) === -1 ? deductedTotal * (+this.invoiceDetailsList[i].sgst / 100) : 0;
        return +(+this.invoiceDetailsList[i].cgst_amount + +this.invoiceDetailsList[i].sgst_amount);
      }
    }
  }

  private getGrandProductTotal(): void {
    this.objInvoices.product_total = this.invoiceDetailsList.map(t => (((+t.product_qty * +t.cost_price) - (+t.product_qty * +t.discount_amount)) - +t.discount_percent_amt)).reduce((acc, value) => acc + value, 0);
  }

  private getIgstGrandTotal(): void {
    this.objInvoices.igst_total = 0;
    this.objInvoices.igst_total = this.invoiceDetailsList.map(t => +t.igst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getCgstGrandTotal(): void {
    this.objInvoices.cgst_total = 0;
    this.objInvoices.cgst_total = this.invoiceDetailsList.map(t => +t.cgst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getSgstGrandTotal(): void {
    this.objInvoices.sgst_total = 0;
    this.objInvoices.sgst_total = this.invoiceDetailsList.map(t => +t.sgst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getDiscountGrandTotal(): void {
    this.objInvoices.discount_total = this.invoiceDetailsList.map(t => ((+t.product_qty * +t.discount_amount) + (+t.discount_percent_amt))).reduce((acc, value) => acc + value, 0);
  }

  public getQuantityTotal(): void {
    this.objInvoices.qty_total = this.invoiceDetailsList.map(t => (+t.no_of_pcs)).reduce((acc, value) => acc + value, 0);
  }

  public getUnDeductedProductTotal(): void {
    this.objInvoices.undeducted_product_total = this.invoiceDetailsList.map(t => (+t.product_qty * +t.cost_price)).reduce((acc, value) => acc + value, 0);
  }

  public getGrandTotal(isModify?: boolean): number {
    // debugger;
    // if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.product_total) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.discount_total) === -1) {
    //   let tempTotal: number = (Number(this.objInvoices.product_total) - Number(this.objInvoices.discount_total));
    //   if (this.flags.isIgst) {
    //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.igst_total) === -1)
    //       this.objInvoices.grand_total = +tempTotal + +this.objInvoices.igst_total;
    //     else this.objInvoices.grand_total = +tempTotal + 0;
    //   }
    //   else if (!this.flags.isIgst) {
    //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.cgst_total) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.sgst_total) === -1)
    //       this.objInvoices.grand_total = tempTotal + +this.objInvoices.cgst_total + +this.objInvoices.sgst_total;
    //     else this.objInvoices.grand_total = +tempTotal + 0;
    //   }
    let grandTotal: number = this.invoiceDetailsList.map(t => (+t.total_taxable_value)).reduce((acc, value) => acc + value, 0);
    this.objInvoices.grand_total = (+grandTotal + +this.objInvoices.freight_charges + +this.objInvoices.additions) - +this.objInvoices.deductions + +this.objInvoices.tcs_amount;
    if (!isModify)
      this.calculateTcsAmountByTcsPercent();
    else this.convertTotalsToDecimal('');

    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objInvoices.round_off_total) === -1) {
      if (this.objInvoices.round_off_total.toString().includes('.')) {
        if (+(this.objInvoices.round_off_total.toString().split('.')[1]) > 0) {
          this.objInvoices.grand_total = +this.objInvoices.grand_total + +(this.objInvoices.round_off_total);
        } else if (+(this.objInvoices.round_off_total.toString().split('.')[1]) < 0) {
          this.objInvoices.grand_total = +this.objInvoices.grand_total - +(this.objInvoices.round_off_total);
        } else if (+(this.objInvoices.round_off_total.toString().split('.')[1]) === 0) {
          this.objInvoices.grand_total = +this.objInvoices.grand_total + 0;
        }
      } else if (+(this.objInvoices.round_off_total.toString())) {
        this.objInvoices.grand_total = +this.objInvoices.grand_total + +(this.objInvoices.round_off_total);
      }
    }
    return +this.objInvoices.grand_total;
    // } else return 0;
  }

  private resetGstValues(): void {
    for (let i = 0; i < this.invoiceDetailsList.length; i++) {
      this.invoiceDetailsList[i].sgst = 0;
      this.invoiceDetailsList[i].sgst_amount = 0;
      this.invoiceDetailsList[i].cgst = 0;
      this.invoiceDetailsList[i].cgst_amount = 0;
      this.invoiceDetailsList[i].igst = 0;
      this.invoiceDetailsList[i].igst_amount = 0;
      this.invoiceDetailsList[i].hsn_code = '';
      this.calculateTaxableValue(i);
    }
  }

  private calculateSellingPrice(i: number): void {
    debugger;
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.invoiceDetailsList[i].cost_price) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].discount_amount) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.invoiceDetailsList[i].discount_percent) === -1) {
      let total: number = +(+this.invoiceDetailsList[i].cost_price - +this.invoiceDetailsList[i].discount_amount);
      let percentAmt: number = +total * (+this.invoiceDetailsList[i].discount_percent / 100);
      let taxableValue: number = +(+total - +percentAmt);
      this.invoiceDetailsList[i].selling_price = Math.round(+taxableValue + (+taxableValue * (+this.invoiceDetailsList[i].profit_percentage / 100)));
      // console.log(this.invoiceDetailsList[i].profit_percentage, 'Profit Percentage');
      // console.log(this.invoiceDetailsList[i].selling_price, 'Selling Price');
    }
  }

  public calculateTaxableValueAfterDiscountEdit(editableColumn: string): void {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.objInvoices.discount_total) === -1) {
      this.objInvoices.product_total = +this.objInvoices.undeducted_product_total - +this.objInvoices.discount_total; // + +this.objInvoices.freight_charges + +this.objInvoices.additions - +this.objInvoices.deductions + +this.objInvoices.tcs_amount;
      this.calculateTaxesAndTotalAfterGrandValueChanges(editableColumn);
    }
  }

  public calculateGrandTotalAfterGSTChanges(editableColumn: string): void {
    if (this.flags.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', , ''].indexOf(+this.objInvoices.igst_total) === -1)
        this.objInvoices.grand_total = +this.objInvoices.product_total + +this.getAdditionsAndDeductionsTotal() + +this.objInvoices.igst_total;
    } else if (!this.flags.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.objInvoices.cgst_total) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.objInvoices.sgst_total) === -1)
        this.objInvoices.grand_total = +this.objInvoices.product_total + +this.getAdditionsAndDeductionsTotal() + +this.objInvoices.cgst_total + +this.objInvoices.sgst_total;
    }
    this.convertTotalsToDecimal(editableColumn);
  }

  // public includeFreightChargesIntoTaxableValue(editableColumn: string): void {
  //   debugger;
  //   if (this.objInvoices.include_freight_in_tax) {
  //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.old_freight_charges) === -1)
  //       this.objInvoices.product_total = +this.objInvoices.product_total - +this.objInvoices.old_freight_charges;
  //     this.objInvoices.old_freight_charges = JSON.parse(JSON.stringify(this.objInvoices.freight_charges));
  //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.freight_charges) === -1)
  //       this.objInvoices.product_total = +this.objInvoices.product_total + +this.objInvoices.freight_charges;
  //     this.calculateTaxesAndTotalAfterGrandValueChanges(editableColumn);
  //   } else if (!this.objInvoices.include_freight_in_tax) {
  //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.grand_total) === -1)
  //       this.objInvoices.old_grand_total = JSON.parse(JSON.stringify(this.objInvoices.grand_total));
  //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.old_freight_charges) === -1)
  //       this.objInvoices.grand_total = +this.objInvoices.grand_total - +this.objInvoices.old_freight_charges;
  //     this.objInvoices.old_freight_charges = JSON.parse(JSON.stringify(this.objInvoices.freight_charges));
  //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.freight_charges) === -1)
  //       this.objInvoices.grand_total = +this.objInvoices.grand_total + +this.objInvoices.freight_charges;
  //     this.convertTotalsToDecimal(editableColumn);
  //   }
  // }
  // public includeFreightChargesIntoTaxableValue(editableColumn: string): void {
  //   if (this.objInvoices.include_freight_in_tax) {
  //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.old_freight_charges) === -1)
  //       this.objInvoices.grand_total = +this.objInvoices.product_total - +this.objInvoices.old_freight_charges;
  //     this.objInvoices.old_freight_charges = JSON.parse(JSON.stringify(this.objInvoices.freight_charges));
  //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.freight_charges) === -1)
  //       this.objInvoices.product_total = +this.objInvoices.product_total + +this.objInvoices.freight_charges;
  //   } else if (!this.objInvoices.include_freight_in_tax) {
  //     if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.old_freight_charges) === -1)
  //       this.objInvoices.product_total = +this.objInvoices.product_total - +this.objInvoices.old_freight_charges;
  //     this.objInvoices.old_freight_charges = 0;
  //   }
  //   this.calculateTaxesAndTotalAfterGrandValueChanges(editableColumn);
  // }

  // public onEnterFreightCharges(editableColumn: string): void {
  //   if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.objInvoices.freight_charges) === -1)
  //     this.objInvoices.grand_total = +this.objInvoices.product_total + (+this.objInvoices.freight_charges) + +this.objInvoices.cgst_total + +this.objInvoices.sgst_total + +this.objInvoices.igst_total;
  //   this.convertTotalsToDecimal(editableColumn);
  // }

  // public onEnterAdditions(editableColumn: string): void {
  //   if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.objInvoices.additions) === -1)
  //     this.objInvoices.grand_total = +this.objInvoices.product_total + (+this.objInvoices.freight_charges) + (+this.objInvoices.additions) + this.getGstTotals();
  //   this.convertTotalsToDecimal(editableColumn);
  // }

  // public onEnterDeductions(editableColumn: string): void {
  //   if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.objInvoices.deductions) === -1)
  //     this.objInvoices.grand_total = +this.objInvoices.product_total + (+this.objInvoices.freight_charges) + (+this.objInvoices.additions) - (+this.objInvoices.deductions) + this.getGstTotals();
  //   this.convertTotalsToDecimal(editableColumn);
  // }

  public calculateTcsAmountByTcsPercent(): void {
    debugger;
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN'].indexOf(+this.objInvoices.tcs_percent) === -1) {
      let tcsAmount: number = (+this.objInvoices.grand_total * (+this.objInvoices.tcs_percent / 100));
      // this.objInvoices.grand_total = +this.objInvoices.grand_total + +tcsAmount;
      this.objInvoices.tcs_amount = +tcsAmount;
      this.calculateAdditionsAndDeductions('');
    }
  }

  public calculateAdditionsAndDeductions(editableColumn: string): void {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.objInvoices.tcs_amount) === -1)
      this.objInvoices.grand_total = +this.objInvoices.product_total + +this.getAdditionsAndDeductionsTotal() + this.getGstTotals();
    this.convertTotalsToDecimal(editableColumn);
  }

  public getAdditionsAndDeductionsTotal(): number {
    return (+this.objInvoices.freight_charges) + (+this.objInvoices.additions) - (+this.objInvoices.deductions) + (+this.objInvoices.tcs_amount);
  }

  private getGstTotals(): number {
    // this.getIgstGrandTotal();
    // this.getCgstGrandTotal();
    // this.getSgstGrandTotal();
    return +this.objInvoices.cgst_total + +this.objInvoices.sgst_total + +this.objInvoices.igst_total;
  }

  private calculateTaxesAndTotalAfterGrandValueChanges(editableColumn: string): void {
    this.getIgstGrandTotal();
    this.getCgstGrandTotal();
    this.getSgstGrandTotal();
    this.objInvoices.grand_total = +this.objInvoices.product_total + +this.getAdditionsAndDeductionsTotal() + +this.objInvoices.igst_total + +this.objInvoices.cgst_total + +this.objInvoices.sgst_total;
    this.convertTotalsToDecimal(editableColumn);
  }

  private convertToDecimal(i: number): void {
    this.invoiceDetailsList[i].total_taxable_value = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.invoiceDetailsList[i].total_taxable_value.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.invoiceDetailsList[i].total_taxable_value, '1.2-2')).replace(/,/g, '') : "0.00";
    this.convertTotalsToDecimal('');
  }

  public convertTotalsToDecimal(editableColumn: string): void {
    this.objInvoices.product_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.product_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.product_total, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'PattialAmount')
      this.objInvoices.invoice_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.invoice_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.invoice_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'Discount')
      this.objInvoices.discount_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.discount_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.discount_total, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'CGST')
      this.objInvoices.cgst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.cgst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.cgst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'SGST')
      this.objInvoices.sgst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.sgst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.sgst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'IGST')
      this.objInvoices.igst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.igst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.igst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'Freight')
      this.objInvoices.freight_charges = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.freight_charges.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.freight_charges, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'Additions')
      this.objInvoices.additions = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.additions.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.additions, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'Deductions')
      this.objInvoices.deductions = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.deductions.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.deductions, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'TCS')
      this.objInvoices.tcs_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.tcs_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objInvoices.tcs_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    // this.objInvoices.round_off_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.round_off_total) === -1 ? (this._decimalPipe.transform(+this.objInvoices.round_off_total, '1.0-0')).replace(/,/g, '') : "0.00";
  }

  private getSupplierDescList(): void {
    this.supplierDescList = [];
    if (this.validateSupplierCode()) {
      let objDesc: any = {
        ProductDesc: JSON.stringify([{
          account_code: this.objInvoices.supplier_code
        }])
      }
      this._invoicesService.getSupplierDescription(objDesc).subscribe((result: any) => {
        debugger;
        if (result) {
          this.supplierDescList = JSON.parse(JSON.stringify(result));
          this.filteredOptions = this.myControl.valueChanges
            .pipe(startWith(''), map(value => typeof value === 'string' ? value : value.supplier_description),
              map(name => name ? this._filter(name) : this.supplierDescList.slice()));
        }
      });
    }
  }

  public getInvoiceNos(isModify?: boolean): void {
    this.invoiceNosList = [];
    this.objInvoices.supplier_invoice_no = '';
    this.objInvoices.supplier_invoice_date = new Date();
    this.objInvoices.grn_no = 0;
    if (this.validateSupplierCode()) {
      let objInvoice: any = {
        InvoiceNos: JSON.stringify([{
          supplier_code: this.objInvoices.supplier_code.toString().trim(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          warehouse_id: +this._localStorage.getGlobalWarehouseId()
        }])
      }
      debugger;
      this._invoicesService.getInvoiceNos(objInvoice).subscribe((result: any) => {
        if (result) {
          // console.log(result, 'Invoice Nos list');
          debugger;
          this.invoiceNosList = JSON.parse(JSON.stringify(result));

          if (isModify)
            this.getInvoiceNoDetails();
        }
      });
    }
  }

  public getInvoiceNoDetails(): void {
    debugger;
    this.objInvoices.supplier_invoice_date = this.invoiceNosList[this.invoiceNosList.findIndex(x => x.invoice_no.toString().trim() === this.objInvoices.supplier_invoice_no.toString().trim())].invoice_date;
    this.objInvoices.grn_no = this.invoiceNosList[this.invoiceNosList.findIndex(x => x.invoice_no.toString().trim() === this.objInvoices.supplier_invoice_no.toString().trim())].grn_no;
    this.objModifyInvoices.supplier_invoice_date = this.invoiceNosList[this.invoiceNosList.findIndex(x => x.invoice_no.toString().trim() === this.objInvoices.supplier_invoice_no.toString().trim())].invoice_date;
    this.objModifyInvoices.grn_no = this.invoiceNosList[this.invoiceNosList.findIndex(x => x.invoice_no.toString().trim() === this.objInvoices.supplier_invoice_no.toString().trim())].grn_no;
  }

  public getMasterDiscount(isModify?: boolean): void {
    this.objInvoices.master_discount_percent = 0;
    this.objInvoices.master_discount_amount = 0;
    if (this.validateSupplierCode()) {
      let objInvoice: any = {
        Discount: JSON.stringify([{
          company_id: this._localStorage.getGlobalCompanyId(),
          supplier_code: this.objInvoices.supplier_code.toString().trim()
        }])
      }
      this._invoicesService.getMasterDiscount(objInvoice).subscribe((result: any) => {
        this.objInvoices.master_discount_percent = result ? result[0].v_disc : 0;
        if (isModify)
          this.objModifyInvoices.master_discount_percent = result ? result[0].v_disc : 0;
      });
    }
  }

  public getInvoicePoNos(isModify?: boolean, detailView?: boolean, tempObj?: any): void {
    this.poNosList = [];
    this.objInvoices.po_id = 0;
    this.objInvoices.merchandiser_name = '';
    if (this.validateSupplierCode()) {
      let objInvoice: any = {
        PoNos: JSON.stringify([{
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          supplier_code: this.objInvoices.supplier_code.toString().trim()
        }])
      }
      debugger;
      this._invoicesService.getInvoicePoNos(objInvoice).subscribe((result: any) => {
        // console.log(result, 'PO Nos');
        debugger;
        this.poNosList = result ? JSON.parse(JSON.stringify(result)) : [];
        if (detailView)
          this.objInvoices.po_id = +tempObj.po_id;
        if (isModify) {
          this.objInvoices.po_id = this.poNosList[this.poNosList.findIndex(x => x.po_no === this.objInvoices.purchase_order_no)].po_id;
          this.objModifyInvoices.po_id = this.poNosList[this.poNosList.findIndex(x => x.po_no === this.objModifyInvoices.purchase_order_no)].po_id;
          this.getSupplierPoDetails();
        }
      });
    }
  }

  public getSupplierPoDetails(): void {
    this.objInvoices.merchandiser_name = this.poNosList[this.poNosList.findIndex(x => +x.po_id === +this.objInvoices.po_id)].merchandiser_name;
    this.objModifyInvoices.merchandiser_name = this.poNosList[this.poNosList.findIndex(x => +x.po_id === +this.objModifyInvoices.po_id)].merchandiser_name;
  }

  public checkIsValidPriceCode(i: number): void {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.invoiceDetailsList[i].price_code) === -1) {
      let objPriceCode: any = {
        PriceCode: JSON.stringify([{
          company_section_id: +this._localStorage.getCompanySectionId(),
          price_code: this.invoiceDetailsList[i].price_code.toString().trim().toUpperCase()
        }])
      }
      this._invoicesService.checkPriceCode(objPriceCode).subscribe((result: boolean) => {
        if (result) {
          this.invoiceDetailsList[i].price_code = this.invoiceDetailsList[i].price_code;
          this.getSellingPricePercentage(i);
        } else this.openInvalidAlertDialog('Invalid price code', i, 'Invoices', 'priceCode');
      });
    }
  }

  private getSellingPricePercentage(i: number): void {
    let objGet: any = {
      Margin: JSON.stringify([{
        price_code: this.invoiceDetailsList[i].price_code.toString().trim().toUpperCase(),
        company_section_id: this._localStorage.getCompanySectionId()
      }])
    }
    this._byAmountService.getMarginCalculation(objGet).subscribe((result: any) => {
      if (result) {
        debugger;
        this.invoiceDetailsList[i].price_code = result[0].price_code;
        this.invoiceDetailsList[i].profit_percentage = +result[0].profit_percentage;
        this.calculateSellingPrice(i);
      }
    });
  }

  public getGstByHsnCode(i: number): void {
    if (!this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.invoiceDetailsList[i].hsn_code) === -1) {
      let objGet = {
        GstByHSN: JSON.stringify([{
          supplier_gstn: this.objInvoices.entered_gstn_no,
          company_section_id: this._localStorage.getCompanySectionId(),
          hsn: this.invoiceDetailsList[i].hsn_code,
          supplier_inv_date: this._datePipe.transform(this.objInvoices.supplier_invoice_date, 'dd/MM/yyyy'),
          cost_price: this.getDeductedTotal(i)
        }])
      }
      this._invoicesService.getGstByHSNCode(objGet).subscribe((result: any) => {
        if (result) {
          debugger;
          this.invoiceDetailsList[i].sgst = JSON.parse(JSON.stringify(result))[0].sgst;
          this.invoiceDetailsList[i].cgst = JSON.parse(JSON.stringify(result))[0].cgst;
          this.invoiceDetailsList[i].igst = JSON.parse(JSON.stringify(result))[0].igst;
          this.calculateTaxableValue(i);
          // if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.invoiceDetailsList[i].sgst) === -1) {
          //   let input = this.sgst.toArray();
          //   input[i].nativeElement.focus();
          //   input[i].nativeElement.select();
          // } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.invoiceDetailsList[i].cgst) === -1) {
          //   let input = this.cgst.toArray();
          //   input[i].nativeElement.focus();
          //   input[i].nativeElement.select();
          // } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.invoiceDetailsList[i].igst) === -1) {
          //   let input = this.igst.toArray();
          //   input[i].nativeElement.focus();
          //   input[i].nativeElement.select();
          // }
        } else {
          this.invoiceDetailsList[i].igst = 0;
          this.invoiceDetailsList[i].cgst = 0;
          this.invoiceDetailsList[i].sgst = 0;
          this.openInvalidAlertDialog('Invalid hsn code', i, 'Invoices', 'hsnCode');
        }
      });
    }
  }

  private checkInvoiceEntryConfigs(): void {
    let objCheck = {
      group_section_id: +this._localStorage.getGlobalGroupSectionId()
    }
    this._invoicesService.checkInvoiceEntryConfigs(objCheck).subscribe((result: any) => {
      debugger;
      if (result) {
        this.flags.isDiscountAmount = result[0].grid_discount_amount ? true : false;
        this.flags.isDiscountAmount ? this.setEditableTrueOrFalse('discountAmt', true) : this.setEditableTrueOrFalse('discountAmt', false);
        this.flags.isSupplierDesc = result[0].supplier_description ? true : false;
        this.flags.isSupplierDesc ? this.setEditableTrueOrFalse('supplierDesc', true) : this.setEditableTrueOrFalse('supplierDesc', false);
        this.flags.isDryWash = result[0].dry_wash ? true : false;
        this.flags.isDryWash ? this.setEditableTrueOrFalse('dryWash', true) : this.setEditableTrueOrFalse('dryWash', false);
        this.flags.isMrp = result[0].mrp ? true : false;
        this.flags.isMrp ? this.setEditableTrueOrFalse('mrp', true) : this.setEditableTrueOrFalse('mrp', false);
        this.flags.isAddMargin = result[0].add_margin ? true : false;
        this.flags.isAddMargin ? this.setEditableTrueOrFalse('addMargin', true) : this.setEditableTrueOrFalse('addMargin', false);
        this.unChangedFlags = JSON.parse(JSON.stringify(this.flags));
        this.modifyFlags = (this.objAction.isEditing || this.objAction.isView) ? JSON.parse(JSON.stringify(this.flags)) : JSON.parse(JSON.stringify(this.modifyFlags));
      }
    });
  }

  public addInvoiceEntries(): void {
    debugger;
    if (this.beforeSaveValidate()) {
      let objSave: any = {
        Invoices: JSON.stringify([{
          inv_byno: this.objInvoices.inv_byno,
          company_section_id: +this._localStorage.getCompanySectionId(),
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          grn_no: +this.objInvoices.grn_no,
          invoice_entry_date: this._datePipe.transform(this.objInvoices.invoice_entry_date, 'dd/MM/yyyy'),
          supplier_code: this.objInvoices.supplier_code,
          supplier_name: this.objInvoices.supplier_name,
          po_id: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.po_id) === -1 ? this.objInvoices.po_id : 0,
          purchase_order_no: this.getSupplierPoNo(), //this.objInvoices.po_no,
          supplier_invoice_no: this.objInvoices.supplier_invoice_no,
          supplier_invoice_date: this._datePipe.transform(this.objInvoices.supplier_invoice_date, 'dd/MM/yyyy'),
          invoice_amount: +this.objInvoices.invoice_amount,
          invoice_actual_amount: +this.objInvoices.grand_total,
          freight_charges: +this.objInvoices.freight_charges,
          other_charges: 0,
          discount_percentage: 0,
          discount_amount: +this.objInvoices.discount_total,
          assessable_value: +this.objInvoices.product_total,
          ed_percentage: 0,
          ed_amount: 0,
          other_deductions: 0,
          lst_percentage: 0,
          lst_amount: 0,
          cst_percentage: 0,
          cst_amount: 0,
          cess_percentage: 0,
          cess_amount: 0,
          surcharge_percentage: 0,
          surcharge_amount: 0,
          credit_days: this.objInvoices.credit_days,
          overall_margin: 0,
          entered_by: +this._localStorage.intGlobalUserId(),
          composite: this.flags.isComposite,
          gst_account_type: '',
          inv_igst_tax_amt: +this.objInvoices.igst_total,
          inv_cgst_tax_amt: +this.objInvoices.cgst_total,
          inv_sgst_tax_amt: +this.objInvoices.sgst_total,
          master_dis_percentage: +this.objInvoices.master_discount_percent,
          spl_dis_percentage: +this.objInvoices.special_discount_percent,
          rcm_invoice_no: '',
          supplier_gstn_no: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_gstn_no) === -1 ? this.objInvoices.supplier_gstn_no.toString().trim() : '',
          merchandiser_name: '',
          tcs_percentage: +this.objInvoices.tcs_percent,
          tcs_amount: +this.objInvoices.tcs_amount,
          round_off_amt: +this.objInvoices.round_off_total,
          misc_addition_amt: +this.objInvoices.additions,
          misc_deduction_amt: +this.objInvoices.deductions,
          details: this.getInvoiceDetailsToAdd()
        }])
      }
      this._invoicesService.addInvoices(objSave).subscribe((result: any) => {
        if (result && result.length > 0) {
          this._confirmationDialog.openAlertDialog(this.objAction.isEditing ? 'Invoice byno ' + result[0].inv_byno + ' has been updated successfully' : 'New byno is ' + result[0].inv_byno, 'Invoices');
          this.afterAddInvoices(result[0]);
        }
      });
    }
  }

  private afterAddInvoices(result: any): void {
    this.objInvoicesLoad.status = 'For Approval';
    this.objInvoicesLoad.all_suppliers = false;
    this.objInvoicesLoad.supplier_code = this.objInvoices.supplier_code;
    this.objInvoicesLoad.supplier_name = this.objInvoices.supplier_name;
    this.objInvoicesLoad.inv_byno = JSON.parse(JSON.stringify(result.inv_byno));
    this.getInvoices();
    this.listClick();
  }

  private getInvoiceDetailsToAdd(): any[] {
    let tempArr: any[] = [];
    debugger;
    for (let i = 0; i < this.invoiceDetailsList.length; i++) {
      let element: any = this.invoiceDetailsList[i];
      if (this.checkIsValidRowToSave(element)) {
        tempArr.push({
          serial_no: i + 1,
          product_group_id: element.product_group_id,
          inv_det_qty: +element.product_qty,
          inv_det_pcs: +element.no_of_pcs,
          cost_price: +element.cost_price,
          price_code: element.price_code.toString().trim().toUpperCase(),
          round_off_amount: +element.round_off,
          selling_price: +element.selling_price,
          drywash: element.dry_wash,
          discount_percentage: +element.discount_percent,
          discount_amount: +element.discount_amount,
          add_margin: +element.add_margin,
          supplier_description: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.supplier_description) === -1 ? element.supplier_description.toString().trim() : '',
          mrp_amount: +element.mrp,
          hsncode: element.hsn_code.toString().trim(),
          igst: +element.igst,
          igst_tax_amount: +element.igst_amount,
          cgst: +element.cgst,
          cgst_tax_amount: +element.cgst_amount,
          sgst: +element.sgst,
          sgst_tax_amount: +element.sgst_amount
        });
      }
    }
    return tempArr;
  }

  private getSupplierPoNo(): string {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.po_id) === -1)
      return this.poNosList[this.poNosList.findIndex(x => +x.po_id === +this.objInvoices.po_id)].po_no;
    else '';
  }

  public fetchInvoices(i: number, isEditing: boolean, isView: boolean, detailView?: boolean): void {
    this.objAction = {
      isEditing: isEditing ? true : false,
      isView: isView ? true : false
    }
    let objFetch: any = {
      Invoices: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        inv_byno: detailView ? JSON.parse(localStorage.getItem('InvoiceByNoForInvoiceView')).inv_byno : this.dataSource.data[i].inv_byno.toString().trim()
      }])
    }
    this._invoicesService.fetchInvoices(objFetch).subscribe((result: any) => {
      debugger;
      if (result && result.returN_VALUE === 1) {
        let tempObj = JSON.parse(result.inv)[0];
        this.objInvoices = JSON.parse(result.inv)[0];
        this.objModifyInvoices = JSON.parse(result.inv)[0];
        this.getOtherDetailsBasedOnSupplier(true);
        this.invoiceDetailsList = JSON.parse(result.inv_Det);
        this.modifyInvoiceDetailsList = JSON.parse(result.inv_Det);
        this.objInvoices.supplier_invoice_no = JSON.parse(result.inv)[0].supplier_invoice_no;
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_email) !== -1)
          this.objInvoices.supplier_address = tempObj.supplier_name + '\n' + tempObj.address1 + ' ' + tempObj.address2 + ' ' + tempObj.address3 + '\nGSTIN No : ' + this.objInvoices.supplier_gstn_no;
        else if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_email) === -1)
          this.objInvoices.supplier_address = tempObj.supplier_name + '\n' + tempObj.address1 + ' ' + tempObj.address2 + ' ' + tempObj.address3 + '\nEmail : ' + this.objInvoices.supplier_email + '\nGSTIN No : ' + this.objInvoices.supplier_gstn_no;
        this.checkInvoiceEntryConfigs();
        this.setInvoiceEntryDate();
        for (let i = 0; i < this.invoiceDetailsList.length; i++)
          this.calculateTaxableValue(i);
        this.objInvoices.round_off_total = JSON.parse(JSON.stringify(tempObj.round_off_total));
        // this.objInvoices.tcs_percent = JSON.parse(JSON.stringify(tempObj.tcs_percent));
        this.objInvoices.tcs_amount = JSON.parse(JSON.stringify(tempObj.tcs_amount));
        this.getGrandTotal(true);
        debugger;
        this.objModifyInvoices = JSON.parse(JSON.stringify(this.objInvoices));
        this.modifyInvoiceDetailsList = JSON.parse(JSON.stringify(this.invoiceDetailsList));
        this.componentVisibility = !this.componentVisibility;
      }
    });
  }

  private setInvoiceEntryDate(): void {
    let dateString: any[] = this.objInvoices.invoice_entry_date.toString().split('/');
    this.objInvoices.invoice_entry_date = dateString[2] + '-' + dateString[1] + '-' + dateString[0];
    this.objModifyInvoices.invoice_entry_date = dateString[2] + '-' + dateString[1] + '-' + dateString[0];
  }

  public getInvoices(): void {
    if (this.beforeLoadValidate()) {
      let objGet: any = {
        Invoices: JSON.stringify([{
          from_date: this.objInvoicesLoad.from_inv_entry_date,
          to_date: this.objInvoicesLoad.to_inv_entry_date,
          company_section_id: +this._localStorage.getCompanySectionId(),
          status: this.objInvoicesLoad.status.toString().trim().toUpperCase(),
          supplier_code: this.objInvoicesLoad.all_suppliers ? '' : this.objInvoicesLoad.supplier_code,
          inv_byno: this.objInvoicesLoad.inv_byno,
          mismatch: this.objInvoicesLoad.is_mismatched_invoices
        }])
      }
      this._invoicesService.getInvoices(objGet).subscribe((result: any[]) => {
        if (result && result.length > 0) {
          this.matTableConfig(JSON.parse(JSON.stringify(result)));
          this.invoicesList = JSON.parse(JSON.stringify(result));
        } else this._confirmationDialog.openAlertDialog('No records found', 'Invoices');
      });
    }
  }

  public matTableConfig(tableRecords?: any[]): void {
    this.invByno = '';
    this.dataSource = tableRecords ? new MatTableDataSource(tableRecords) : new MatTableDataSource([]);
    if (this.dataSource.data.length > 0)
      this.dataSource.data.forEach(x => {
        x.Selected = false;
      });
  }

  public searchInvoicesByByno(searchValue: string): void {
    if (this.invoicesList.length > 0) {
      searchValue = searchValue.toString().trim();
      searchValue = searchValue ? searchValue.toString().toLocaleLowerCase() : "";
      let filteredInvoices = this.invoicesList.filter(element =>
        element.inv_byno.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
      this.matTableConfig(filteredInvoices);
    }
  }

  public filterMismatchedInvoices(): void {
    debugger;
    if (this.objInvoicesLoad.is_mismatched_invoices) {
      let mismatchedInvoices: any[] = [];
      this.invoicesList.forEach(x => {
        if (+x.invoice_amount !== +x.invoice_actual_amount) {
          mismatchedInvoices.push(x);
        }
      });
      this.matTableConfig(mismatchedInvoices);
    } else this.matTableConfig(this.invoicesList);
  }

  private setEditableTrueOrFalse(columnName: string, editable: boolean): void {
    debugger;
    let i = this.columns.findIndex(x => x.display === columnName)
    this.columns[i].editable = editable;
  }

  public addNewRowToInvoiceDetails(): void {
    if (this.invoiceDetailsList.length > 0) {
      this.objInvoiceDetails.product_group_id = this.invoiceDetailsList[0].product_group_id;
      this.objInvoiceDetails.product_group_name = this.invoiceDetailsList[0].product_group_name;
      this.objInvoiceDetails.product_group_uom = this.invoiceDetailsList[0].product_group_uom;
      this.objInvoiceDetails.discount_amount = this.invoiceDetailsList[0].discount_amount;
      this.objInvoiceDetails.discount_percent = this.invoiceDetailsList[0].discount_percent;
      this.objInvoiceDetails.discount_percent_amt = this.invoiceDetailsList[0].discount_percent_amt;
      this.objInvoiceDetails.price_code = this.invoiceDetailsList[0].price_code.toString().trim().toUpperCase();
      this.objInvoiceDetails.hsn_code = this.invoiceDetailsList[0].hsn_code.toString().trim();
    }
    this.invoiceDetailsList.push(Object.assign({}, this.objInvoiceDetails));
    setTimeout(() => {
      let input = this.productGroup.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100);
  }

  public addMiddleRowToInvoiceDetails(i: number): void {
    debugger;
    let details: any[] = JSON.parse(JSON.stringify(this.invoiceDetailsList));
    this.invoiceDetailsList = [];
    if (i === 0) {
      this.invoiceDetailsList.push(Object.assign({}, this.objInvoiceDetails));
      for (let k = 0; k < details.length; k++)
        this.invoiceDetailsList.push(details[k]);
    } else {
      for (let k = 0; k < i; k++)
        this.invoiceDetailsList.push(details[k]);
      this.invoiceDetailsList.push(Object.assign({}, this.objInvoiceDetails));
      for (let k = i; k >= i && k <= (details.length - 1); k++)
        this.invoiceDetailsList.push(details[k]);
    }
    setTimeout(() => {
      let input = this.productGroup.toArray();
      input[i].nativeElement.focus();
    }, 100);
  }

  public removeRowFromInvoiceDetails(i: number): void {
    this.openRemoveConfirmationDialog('Are you sure want to remove row ' + (i + 1) + ' from the list', 'Invoices', i);
  }

  public newClick(): void {
    this.objAction = JSON.parse(JSON.stringify(this.objUnchangedAction));
    this.checkInvoiceEntryConfigs();
    this.resetInvoiceEntry();
    this.componentVisibility = !this.componentVisibility;
    setTimeout(() => {
      let input = this.newSupplier.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100);
  }

  public onClear(exitFlag?: boolean): void {
    if (this.checkAnyChangesMade()) {
      this.openConfirmationDialog(exitFlag ? 'Changes will be lost, are you sure?' : 'Do you want to clear all the fields?', exitFlag);
    } else if (exitFlag)
      this.listClick();
  }

  public listClick(): void {
    this.resetInvoiceEntry();
    this.componentVisibility = !this.componentVisibility;
  }

  // public openDiscountDialog(): void {
  //   const dialogRef = this._matDialog.open(DiscountLookupComponent, {
  //     width: "400px",
  //     panelClass: "custom-dialog-container",
  //     data: {}
  //   });
  //   dialogRef.afterClosed().subscribe((result: any) => {
  //   });
  // }

  public openAttachFileDialog(): void {
    const dialogRef = this._matDialog.open(SingleFileAttachComponent, {
      width: '53vw',
      panelClass: "custom-dialog-container",
      data: {}
    });
    dialogRef.afterClosed().subscribe((result: any) => {
    });
  }

  public resetInvoiceEntry(): void {
    if (this.objAction.isEditing) {
      this.flags = JSON.parse(JSON.stringify(this.modifyFlags));
      this.objInvoices = JSON.parse(JSON.stringify(this.objModifyInvoices));
      this.invoiceDetailsList = JSON.parse(JSON.stringify(this.modifyInvoiceDetailsList));
    } else if (!this.objAction.isEditing) {
      this.flags = JSON.parse(JSON.stringify(this.unChangedFlags));
      this.objInvoices = JSON.parse(JSON.stringify(this.objUnChangedInvoices));
      this.invoiceDetailsList = JSON.parse(JSON.stringify(this.unChangedInvoiceDetailsList));
      this.invoiceNosList = [];
      this.poNosList = [];
      this.checkInvoiceEntryConfigs();
    }
    localStorage.setItem("InvoiceByNoForInvoiceView", JSON.stringify(null));
  }

  public setSelectedByno(k: number): void {
    this.invByno = '';
    if (this.dataSource.data[k].Selected) {
      for (let i = 0; i < this.dataSource.data.length; i++) {
        if (i !== k) {
          if (this.dataSource.data[k].Selected) {
            this.invByno = this.dataSource.data[k].inv_byno;
            this.dataSource.data[i].Selected = false;
            this.dataSource.data[k].Selected = true;
          } else this.invByno = this.dataSource.data[k].inv_byno;
        } else this.invByno = this.dataSource.data[k].inv_byno;
      }
    } else
      this.invByno = '';
  }

  public onClickInvoiceProductDetails(i: number): void {
    let objInvDetails: any = {
      inv_byno: this.invoiceDetailsList[i].inv_byno.toString().trim(),
      byno_serial: +this.invoiceDetailsList[i].byno_serial,
      newSupplierLookupList: JSON.parse(JSON.stringify(this.newSupplierLookupList)),
      productGroupLookupList: JSON.parse(JSON.stringify(this.productGroupLookupList)),
      objInvoices: JSON.parse(JSON.stringify(this.objInvoices)),
      invoiceDetailsList: JSON.parse(JSON.stringify(this.invoiceDetailsList)),
      invoiceNosList: JSON.parse(JSON.stringify(this.invoiceNosList)),
      poNosList: JSON.parse(JSON.stringify(this.poNosList)),
      flags: JSON.parse(JSON.stringify(this.flags))
    }
    localStorage.setItem("InvoiceByNoForInvoiceDetails", JSON.stringify(objInvDetails));
    this._router.navigate(["/Inventory/InvoiceProductDetailView"]);
  }

  public onExitClick(): void {
    localStorage.setItem('InvoiceByNoForInvoiceView', JSON.stringify(null));
    localStorage.setItem('InvoiceByNoForInvoiceDetails', JSON.stringify(null));
    this._router.navigate([this._localStorage.getMenuPath()])
  }

  /********************************* Invoice Approval/Post/Cost Price Edit/Excess/Shortage **********************************/

  public onClickApprove(): any {
    if (this.beforeApproveValidate()) {
      debugger;
      this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
        panelClass: "custom-dialog-container",
        data: { confirmationDialog: 1 }
      });
      this._dialogRef.componentInstance.confirmMessage =
        "Do you want to approve the invoice byno '" + this.invByno + "'";
      this._dialogRef.componentInstance.componentName = "Invoices";
      return this._dialogRef.afterClosed().subscribe(result => {
        result ? this.approveSelectedInvoice() : this._dialogRef = null;
      });
    }
  }

  public approveSelectedInvoice(): void {
    let objApprove: any = {
      Invoices: JSON.stringify([{
        company_section_id: this._localStorage.getCompanySectionId(),
        inv_byno: this.invByno,
        approved_by: this._localStorage.intGlobalUserId()
      }])
    }
    this._invoicesService.approveInvoices(objApprove).subscribe((result: boolean) => {
      if (result) {
        this.afterApprovedInvoice();
        this._confirmationDialog.openAlertDialog('Approved', 'Invoices');
      }
    });
  }

  private afterApprovedInvoice(): void {
    this.objInvoicesLoad.status = 'Approved';
    this.objInvoicesLoad.all_suppliers = true;
    this.objInvoicesLoad.supplier_code = '';
    this.objInvoicesLoad.supplier_name = '';
    this.objInvoicesLoad.inv_byno = JSON.parse(JSON.stringify(this.invByno));
    this.objInvoicesLoad.is_mismatched_invoices = false;
    this.getInvoices();
    this.invByno = '';
  }

  public onClickPost(): any {
    if (this.beforeApproveValidate()) {
      debugger;
      this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
        panelClass: "custom-dialog-container",
        data: { confirmationDialog: 1 }
      });
      this._dialogRef.componentInstance.confirmMessage =
        "Do you want to post the invoice byno '" + this.invByno + "'";
      this._dialogRef.componentInstance.componentName = "Invoices";
      return this._dialogRef.afterClosed().subscribe(result => {
        result ? this.postSelectedInvoice() : this._dialogRef = null;
      });
    }
  }

  public postSelectedInvoice(): void {
    let objPost: any = {
      Invoices: JSON.stringify([{
        company_section_id: this._localStorage.getCompanySectionId(),
        inv_byno: this.invByno,
        posted_by: this._localStorage.intGlobalUserId()
      }])
    }
    this._invoicesService.postInvoices(objPost).subscribe((result: boolean) => {
      if (result) {
        this.afterPostedInvoice();
        this._confirmationDialog.openAlertDialog('Posted', 'Invoices');
      }
    });
  }

  private afterPostedInvoice(): void {
    this.objInvoicesLoad.status = 'Posted';
    this.objInvoicesLoad.all_suppliers = true;
    this.objInvoicesLoad.supplier_code = '';
    this.objInvoicesLoad.supplier_name = '';
    this.objInvoicesLoad.inv_byno = JSON.parse(JSON.stringify(this.invByno));
    this.objInvoicesLoad.is_mismatched_invoices = false;
    this.getInvoices();
    this.invByno = '';
  }

  public onClickCostPriceEdit(): void {
    if (this.beforeApproveValidate()) {
      const dialogRef = this._matDialog.open(EditCostPriceComponent, {
        width: "100%",
        panelClass: "custom-dialog-container",
        data: {
          byno: JSON.parse(JSON.stringify(this.invByno))
        }
      });
      dialogRef.afterClosed().subscribe((result: any) => {
        if (result) {
          this._confirmationDialog.openAlertDialog('Invoice byno ' + this.invByno + ' has been updated successfully', 'Invoices');
        }
      });
    }
  }

  public onClickExcessShortage(): void {
    if (this.beforeApproveValidate()) {
      let objExcessShortage: any = {
        inv_byno: JSON.parse(JSON.stringify(this.invByno))
      }
      localStorage.setItem('invoiceDetailForExcessShortage', JSON.stringify(objExcessShortage));
      this._router.navigate(['/Inventory/InvoicesExcessShortage']);
    }
  }

  /****************************************************** Validations *******************************************************/

  private openAlertDialog(value: string, componentName: string, focus: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        document.getElementById(focus).focus();
      _dialogRef = null;
    });
  }

  private openInvalidAlertDialog(value: string, i: number, componentName: string, focus: any) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this[focus].toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  public onlyAllwDecimalForInvoiceDetails(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public onlyAllowRoundOffDecimalForInvoiceDetails(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number) {
    debugger;
    if (event.keyCode !== 45) {
      let key = key1 + '[' + index + ']' + '.' + key2;
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key] = "";
          event.target.value = "";
        }
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      } else {
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      }
    } else {
      let key = key1 + '[' + index + ']' + '.' + key2;
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key] = "";
          event.target.value = "";
        }
      }
    }
  }

  public onlyAllowRoundOffDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string) {
    debugger;
    if (event.keyCode !== 45) {
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      } else {
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      }
    } else {
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
      }
    }
  }

  public onlyAllwDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    // let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public onlyAllwDecimalForTcsPercent(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    // let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public onlyAllowNumbers(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    // let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    }
  }

  public onlyAllwNumbersForInvoiceDetails(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    }
  }

  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, allowCharacters?: boolean): boolean {
    if (allowCharacters) {
      if ((event.keyCode >= 48 && event.keyCode <= 57) || event.keyCode === 46) {
        if (+(event.target.value.toString().concat(event.key !== "." ? event.key : "")) <= maxValue) {
          if (event.target.value.toString().includes(".") === false || event.keyCode !== 46) {
            if (event.target.value.toString().includes(".") === true) {
              let maxLength = maxValue.toString().split(".")[0].length;
              let intLength = event.target.value.toString().concat(event.key).split(".")[0].length;
              let decimalLength = event.target.value.toString().concat(event.key).split(".")[1].length;
              if (decimalLength <= 3) {
                if (intLength <= maxLength)
                  return true;
                else return false;
              } else return false;
            } else return true;
          } else return false;
        } else return false;
      } else return false;
    } else {
      if (event.charCode >= 48 && event.charCode <= 57) {
        if (+(event.target.value.toString().concat(event.key)) <= maxValue)
          return true;
        else return false;
      } else return false;
    }
  }

  public checkIsValidGstNo(): void {
    if (!this.flags.isComposite) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_gstn_no) === -1 &&
        [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.entered_gstn_no) === -1)
        if (this.objInvoices.entered_gstn_no.toString().trim() !== this.objInvoices.supplier_gstn_no.toString().trim()) {
          this.openAlertDialog('Invalid GSTIN No', 'Invoices', 'gstnNo');
        }
    }
  }

  private checkIsValidRowToSave(element: any): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.product_group_id) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.product_qty) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.no_of_pcs) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.cost_price) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.hsn_code) === -1)
      return true;
    else return false;
  }

  private checkValidDetails(rowIndex: number, colIndex: number): boolean {
    debugger;
    if (this.columns[colIndex].display === 'productGroup') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(this.invoiceDetailsList[rowIndex].product_group_name) !== -1) {
        this.openInvalidAlertDialog('Enter product group', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'qty') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].product_qty) !== -1) {
        this.openInvalidAlertDialog('Enter quantity', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.invoiceDetailsList[rowIndex].product_group_uom === 'Mt.s' && this.columns[colIndex].display === 'pcs') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].no_of_pcs) !== -1) {
        this.openInvalidAlertDialog('Enter pieces', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'costPrice') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].cost_price) !== -1) {
        this.openInvalidAlertDialog('Enter cost price', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } if (+this.invoiceDetailsList[rowIndex].cost_price < 0) {
        this.openInvalidAlertDialog('Invalid cost price', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'discountAmt') {
      if (+this.invoiceDetailsList[rowIndex].discount_amount < 0) {
        this.openInvalidAlertDialog('Invalid discount amount', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'discountPercent') {
      if (+this.invoiceDetailsList[rowIndex].discount_amount < 0) {
        this.openInvalidAlertDialog('Invalid discount percent', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'priceCode') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].price_code) !== -1) {
        this.openInvalidAlertDialog('Enter price code', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'hsnCode') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].hsn_code) !== -1) {
        this.openInvalidAlertDialog('Enter hsn code', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (!this.flags.isComposite && this.flags.isIgst && this.columns[colIndex].display === 'igst') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].igst) !== -1) {
        this.openInvalidAlertDialog('Enter igst', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } if (+this.invoiceDetailsList[rowIndex].igst < 0) {
        this.openInvalidAlertDialog('Invalid igst', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (!this.flags.isComposite && !this.flags.isIgst && this.columns[colIndex].display === 'sgst') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].sgst) !== -1) {
        this.openInvalidAlertDialog('Enter sgst', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } if (+this.invoiceDetailsList[rowIndex].sgst < 0) {
        this.openInvalidAlertDialog('Invalid sgst', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (!this.flags.isComposite && !this.flags.isIgst && this.columns[colIndex].display === 'cgst') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.invoiceDetailsList[rowIndex].cgst) !== -1) {
        this.openInvalidAlertDialog('Enter cgst', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } if (+this.invoiceDetailsList[rowIndex].cgst < 0) {
        this.openInvalidAlertDialog('Invalid cgst', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else return true;
  }

  private beforeLoadValidate(): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoicesLoad.status) !== -1) {
      document.getElementById('status').focus();
      this._confirmationDialog.openAlertDialog('Select status', 'Invoices');
      return false;
    } if (!this.objInvoicesLoad.all_suppliers && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoicesLoad.supplier_code) !== -1) {
      document.getElementById('loadSupplier').focus();
      this._confirmationDialog.openAlertDialog('Select supplier', 'Invoices');
      return false;
    } if (!this.objInvoicesLoad.all_suppliers && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoicesLoad.supplier_code) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoicesLoad.supplier_name) !== -1) {
      document.getElementById('loadSupplier');
      this._confirmationDialog.openAlertDialog('Invalid supplier', 'Invoices');
      return false;
    } else return true;
  }

  public beforeSaveValidate(): boolean {
    if ([null, 'null', undefined, 'undfefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_code) !== -1) {
      this.openAlertDialog('Select supplier', 'Invoices', 'newSupplier');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_code) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_name) !== -1) {
      this.openAlertDialog('Invalid supplier', 'Invoices', 'newSupplier');
      return false;
    } if (!this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.entered_gstn_no) !== -1) {
      this.openAlertDialog('Enter supplier gstn no', 'Invoices', 'gstnNo');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.supplier_invoice_no) !== -1) {
      this.openAlertDialog('Select supplier invoice no', 'Invoices', 'supplierInvoiceNo');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(this.objInvoices.invoice_amount) !== -1) {
      this.openAlertDialog('Enter invoice amount', 'Invoices', 'invoiceAmount');
      return false;
    } if (!this.beforeSaveValidateInvoiceDetails()) {
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoices.qty_total) !== -1) {
      this._confirmationDialog.openAlertDialog('Enter atleast 1 number Of pieces', 'Invoices');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(this.objInvoices.undeducted_product_total) !== -1) {
      this._confirmationDialog.openAlertDialog('Product total cannot be 0', 'Invoices');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(this.objInvoices.product_total) !== -1) {
      this._confirmationDialog.openAlertDialog('Taxable value cannot be 0', 'Invoices');
      return false;
    } if (!this.flags.isIgst && !this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(this.objInvoices.cgst_total) !== -1) {
      this._confirmationDialog.openAlertDialog('Cgst cannot be 0', 'Invoices');
      return false;
    } if (!this.flags.isIgst && !this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(this.objInvoices.sgst_total) !== -1) {
      this._confirmationDialog.openAlertDialog('Sgst cannot be 0', 'Invoices');
      return false;
    } if (this.flags.isIgst && !this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(this.objInvoices.igst_total) !== -1) {
      this._confirmationDialog.openAlertDialog('Igst cannot be 0', 'Invoices');
      return false;
    } else return true;
  }

  private beforeSaveValidateInvoiceDetails(): boolean {
    for (let i = 0; i < this.invoiceDetailsList.length; i++) {
      let element: any = this.invoiceDetailsList[i];
      if (JSON.stringify(element) !== JSON.stringify(this.objInvoiceDetails)) {
        if (this.invoiceDetailsList.length === 1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.product_group_id) !== -1) {
          this._confirmationDialog.openAlertDialog('No records to save', 'Invoices');
          return false;
        } if (!this.validateProductGroupName(i)) {
          this.openInvalidAlertDialog('Invalid product group name', i, 'Invoices', 'productGroup');
          return false;
        } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(element.product_qty) !== -1) {
          this.openInvalidAlertDialog('Enter quantity', i, 'Invoices', 'qty');
          return false;
        } if (+element.product_qty < 0) {
          this.openInvalidAlertDialog('Invalid quantity', i, 'Invoices', 'qty');
          return false;
        } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.no_of_pcs) !== -1) {
          this.openInvalidAlertDialog('Enter pieces', i, 'Invoices', 'pcs');
          return false;
        } if (+element.no_of_pcs < 0) {
          this.openInvalidAlertDialog('Invalid pieces', i, 'Invoices', 'pcs');
          return false;
        } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(element.cost_price) !== -1) {
          this.openInvalidAlertDialog('Enter cost price', i, 'Invoices', 'costPrice');
          return false;
        } if (+element.cost_price < 0) {
          this.openInvalidAlertDialog('Invalid cost price', i, 'Invoices', 'costPrice');
          return false;
        } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(element.discount_amount) === -1 && +element.discount_amount < 0) {
          this.openInvalidAlertDialog('Invalid discount amount', i, 'Invoices', 'discountAmt');
          return false;
        } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(element.discount_percent) === -1 && +element.discount_percent < 0) {
          this.openInvalidAlertDialog('Invalid discount percentage', i, 'Invoices', 'discountPercent');
          return false;
        } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.price_code.toString().trim()) !== -1) {
          this.openInvalidAlertDialog('Enter price code', i, 'Invoices', 'priceCode');
          return false;
        } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.hsn_code.toString().trim()) !== -1) {
          this.openInvalidAlertDialog('Enter hsn code', i, 'Invoices', 'hsnCode');
          return false;
        } if (+element.hsn_code < 0) {
          this.openInvalidAlertDialog('Invalid hsn code', i, 'Invoices', 'hsnCode');
          return false;
        } if (!this.flags.isComposite && this.flags.isIgst && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(element.igst.toString().trim()) !== -1) {
          this.openInvalidAlertDialog('Enter igst', i, 'Invoices', 'igst');
          return false;
        } if (!this.flags.isComposite && this.flags.isIgst && +element.igst < 0) {
          this.openInvalidAlertDialog('Invalid igst', i, 'Invoices', 'igst');
          return false;
        } if (!this.flags.isComposite && !this.flags.isIgst && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(element.cgst.toString().trim()) !== -1) {
          this.openInvalidAlertDialog('Enter cgst', i, 'Invoices', 'cgst');
          return false;
        } if (!this.flags.isComposite && !this.flags.isIgst && +element.cgst < 0) {
          this.openInvalidAlertDialog('Invalid cgst', i, 'Invoices', 'cgst');
          return false;
        } if (!this.flags.isComposite && !this.flags.isIgst && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(element.sgst.toString().trim()) !== -1) {
          this.openInvalidAlertDialog('Enter sgst', i, 'Invoices', 'sgst');
          return false;
        } if (!this.flags.isComposite && !this.flags.isIgst && +element.sgst < 0) {
          this.openInvalidAlertDialog('Invalid sgst', i, 'Invoices', 'sgst');
          return false;
        }
      } else return true;
    } return true;

  }

  private beforeApproveValidate(): boolean {
    if (this.dataSource.data.length === 0) {
      this._confirmationDialog.openAlertDialog('No records found', 'Invoices');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.invByno) !== -1) {
      this._confirmationDialog.openAlertDialog('Select a invoice byno', 'Invoices');
      return false;
    } else return true;
  }

  public onlyAllowUpperCases(event: KeyboardEvent | any): void {
    if (event.keyCode >= 65 && event.keyCode <= 90)
      event.target.value = event.target.value.toString().trim().toUpperCase();
  }

  public resetSupplierLookupOnLoad(): void {
    this.objInvoicesLoad.supplier_code = '';
    this.objInvoicesLoad.supplier_name = '';
    this.matTableConfig();
  }

  private checkAnyChangesMade(): boolean {
    if (this.checkChangesInMaster() || this.checkChangesInDetails() || this.checkChangesInFlags())
      return true;
    else return false;
  }

  private checkChangesInMaster(): boolean {
    debugger;
    if (JSON.stringify(this.objInvoices) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.objModifyInvoices) :
      JSON.stringify(this.objUnChangedInvoices)))
      return true;
    else false;
  }

  private checkChangesInDetails(): boolean {
    debugger;
    if (JSON.stringify(this.invoiceDetailsList) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.modifyInvoiceDetailsList) :
      JSON.stringify(this.unChangedInvoiceDetailsList)))
      return true;
    else return false;
  }

  private checkChangesInFlags(): boolean {
    debugger;
    if (JSON.stringify(this.flags) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.modifyFlags) :
      JSON.stringify(this.unChangedFlags)))
      return true;
    else return false;
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Invoices";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.listClick() : this.resetInvoiceEntry();
      dialogRef = null;
    });
  }

  public openRemoveConfirmationDialog(message: string, componentName: string, i: number): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = componentName;
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deleteRow(i);
      dialogRef = null;
    });
  }

  private deleteRow(i: number) {
    this.invoiceDetailsList.splice(i, 1);
    if (i === this.invoiceDetailsList.length) {
      setTimeout(() => {
        let inputEls = this.productGroup.toArray();
        inputEls[i - 1].nativeElement.focus();
      }, 100);
    } else {
      setTimeout(() => {
        let inputEls = this.productGroup.toArray();
        inputEls[i].nativeElement.focus();
      }, 100);
    }
  }

  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Byno": this.dataSource.data[i].inv_byno,
          "Invoice No": this.dataSource.data[i].supplier_invoice_no,
          "Invoice Date": this.dataSource.data[i].supplier_invoice_date,
          "Supplier Name": this.dataSource.data[i].supplier_name,
          "Invoice Amount": this.dataSource.data[i].invoice_amount,
          "Actual Amount": this.dataSource.data[i].invoice_actual_amount,
        });
      }
      this._excelService.exportAsExcelFile(json, "Invoices", datetime);
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "Invoices");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.inv_byno);
        tempObj.push(e.supplier_invoice_no);
        tempObj.push(e.supplier_invoice_date);
        tempObj.push(e.supplier_name);
        tempObj.push(e.invoice_amount);
        tempObj.push(e.invoice_actual_amount);
        prepare.push(tempObj);
      });

      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Byno", "Invoice No", "Invoice Date", "Supplier Name", "Invoice Amount", "Actual Amount"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('Invoices' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "Invoices");
  }

  public generateInvoiceCheckListReport() {
    if (this.beforeApproveValidate()) {
      let objLoad: any = {
        Invoice_Check_List: JSON.stringify([{
          byno: this.invByno.toString().trim(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          wh_section_id: +this._localStorage.getWhSectionId(),
          type: true
        }])
      }
      this._invoicesService.getInvoiceCheckListReport(objLoad).subscribe((result: any) => {
        if (result.size != 0) {
          let dataType = result.type;
          console.log(result);
          const file = new Blob([result], { type: dataType });
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL);
        } else
          this._confirmationDialog.openAlertDialog('No record found', 'Invoices');
      });
    }
  }
}