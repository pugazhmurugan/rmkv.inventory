import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceProductDetailViewComponent } from './invoice-product-detail-view.component';

describe('InvoiceProductDetailViewComponent', () => {
  let component: InvoiceProductDetailViewComponent;
  let fixture: ComponentFixture<InvoiceProductDetailViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceProductDetailViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceProductDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
