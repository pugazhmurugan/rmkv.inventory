import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { InvoiceDetailsService } from '../../../services/purchases/invoice-prod-details/invoice-details.service';
import { InvoicesService } from '../../../services/purchases/invoices/invoices.service';

@Component({
  selector: 'app-invoice-product-detail-view',
  templateUrl: './invoice-product-detail-view.component.html',
  styleUrls: ['./invoice-product-detail-view.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class InvoiceProductDetailViewComponent implements OnInit {
  productList: any = [];
  counterList: any;
  attributesList: any = [];
  configList: any;
  clearDatas: any;
  sizeist: any;
  objInvoiceProdDetailEdit: any = {
    inv_byno: '',
    product_group_id: 0,
    product_group_desc: '',
    unique_serial: '',
    bypass: '',
    product_uom: '',
    supplier_description: '',
    total_pieces: '',
    cost_price: '',
    selling_price: '',
    product_details: [{
      product_code: '',
      product_description: '',
      inv_det_actual_qty: '',
      inv_det_actual_pcs: '',
      style_code: '',
      hsncode: [],
      gstn_no: '',
      counter: 0,
      size: '',
      attributesList: []
    }]
  }
  bynodata = {
    inv_byno: '',
    byno_serial: '',
    newSupplierLookupList: [],
    productGroupLookupList: [],
    objInvoices: {},
    invoiceDetailsList: [],
    invoiceNosList: [],
    poNosList: [],
    flags: {}
  }
  constructor(public _localStorage: LocalStorage,
    public _datePipe: DatePipe,
    public _router: Router,
    public _matDialog: MatDialog,
    public _gridKeyEvents: GridKeyEvents,
    public _invoiceProdDetails: InvoiceDetailsService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _excelService: ExcelService,
    public _decimalPipe: DecimalPipe,
    public _invoicesService: InvoicesService,
    public _keyPressEvents: KeyPressEvents) {
  }

  ngOnInit() {
    debugger
    this.bynodata = JSON.parse(localStorage.getItem('InvoiceByNoForInvoiceDetails'));
    this.checkInvoiceEntryConfigs();
    // this.bynodata.inv_byno = 'CSKC0013'
    // this.bynodata.byno_serial = '001'
  }

  public getCounterDetails(): void {
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._invoiceProdDetails.getCounterDetails(objData).subscribe((result: any) => {
      if (result) {
        this.counterList = result;
      }
      this.fetchInvoiceProductDetail(this.bynodata);
      this.getAttributeSizeList();
    });
  }
  private checkInvoiceEntryConfigs(): void {
    let objCheck = {
      group_section_id: +this._localStorage.getGlobalGroupSectionId()
    }
    this._invoicesService.checkInvoiceEntryConfigs(objCheck).subscribe((result: any) => {
      this.configList = result;
      this.getSectionWiseAttribute();
      //  if (this.configList[0].ipd_size_attribute_id)
      console.log(this.configList, 'confifg')
    })
  }
  public getAttributeSizeList(): void {
    debugger;
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        attribute_id: +this.configList[0].ipd_size_attribute_id
      }])
    }
    this._invoiceProdDetails.getAttributeSizeList(objData).subscribe((result: any) => {
      if (result) {
        console.log(result, 'trestiojg')
        this.sizeist = result;
      }
    });
  }
  getSectionWiseAttribute() {
    let objData = {
      InvoiceProdDetails: +this._localStorage.getCompanySectionId()
    }
    this._invoiceProdDetails.sectionWiseAttribute(objData).subscribe((result: any) => {
      if (result) {
        for (let i = 0; i < result.length; i++) {
          if (this.configList[0].ipd_size_entry == true && result[i].attribute_id !== 15) {
            this.attributesList.push({
              attribute_id: result[i].attribute_id,
              attribute_name: result[i].attribute_name,
              attribute_value: '',
              attribute_value_id: ''
            })
            for (let j = 0; j < this.objInvoiceProdDetailEdit.product_details.length; j++) {
              this.objInvoiceProdDetailEdit.product_details[j].attributesList.push({
                attribute_id: result[i].attribute_id,
                attribute_name: result[i].attribute_name,
                attribute_value: '',
                attribute_value_id: ''
              })
            }
          } else if (this.configList[0].ipd_size_entry == false) {
            this.attributesList.push({
              attribute_id: result[i].attribute_id,
              attribute_name: result[i].attribute_name,
              attribute_value: '',
              attribute_value_id: ''
            })
            for (let j = 0; j < this.objInvoiceProdDetailEdit.product_details.length; j++) {
              this.objInvoiceProdDetailEdit.product_details[j].attributesList.push({
                attribute_id: result[i].attribute_id,
                attribute_name: result[i].attribute_name,
                attribute_value: '',
                attribute_value_id: ''
              })
            }
          }
        }
        this.getCounterDetails();
        console.log(this.objInvoiceProdDetailEdit.product_details, 'testst')
      }
    });
  }
  fetchInvoiceProductDetail(data) {
    debugger
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        inv_byno: data.inv_byno.toString().trim(),
        byno_serial: '000' + data.byno_serial.toString().trim(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._invoiceProdDetails.fetchInvoiceProdDetails(objData).subscribe((result: any) => {
      console.log(result);
      this.objInvoiceProdDetailEdit.inv_byno = data.inv_byno + '/' + data.byno_serial;
      this.objInvoiceProdDetailEdit.product_group_desc = data.product_group_desc;
      this.objInvoiceProdDetailEdit.product_uom = "No.s";//data.product_uom;
      this.objInvoiceProdDetailEdit.product_group_id = data.product_group_id;
      this.objInvoiceProdDetailEdit.unique_serial = data.unique_serial ? 'Yes' : 'No';//'No'
      this.objInvoiceProdDetailEdit.supplier_description = data.supplier_description;
      this.objInvoiceProdDetailEdit.total_pieces = data.inv_det_actual_pcs;
      this.objInvoiceProdDetailEdit.cost_price = data.actual_cost_price;
      this.objInvoiceProdDetailEdit.selling_price = data.selling_price;
      if (result) {
        debugger
        let attributeData = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit.product_details[0].attributesList));
        this.objInvoiceProdDetailEdit.product_details = result;
        let attributeValue = '';
        for (let i = 0; i < this.objInvoiceProdDetailEdit.product_details.length; i++) {
          for (let j = 0; j < result[0].attribute_values.length; j++) {
            if (this.configList[0].ipd_size_entry == true && result[i].attribute_values[j].attribute_name == 'Size') {
              attributeValue = result[i].attribute_values[j].attribute_value;
              result[i].attribute_values.splice(j, 1);
            }
          }
          this.objInvoiceProdDetailEdit.product_details[i].product_code = result[i].product_code,
            this.objInvoiceProdDetailEdit.product_details[i].product_description = result[i].product_name,
            this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty = result[i].prod_qty,
            this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = result[i].prod_pcs,
            this.objInvoiceProdDetailEdit.product_details[i].style_code = result[i].supplier_style_code,
            this.objInvoiceProdDetailEdit.product_details[i].hsncode = result[i].hsncode,
            this.objInvoiceProdDetailEdit.product_details[i].gstn_no = result[i].gstn_no,
            this.objInvoiceProdDetailEdit.product_details[i].counter = result[i].counter_id,
            this.objInvoiceProdDetailEdit.product_details[i].size = attributeValue,//result[i].counter_id,
            this.objInvoiceProdDetailEdit.product_details[i].attributesList = result[i].attribute_values.length > 1 ? JSON.parse(JSON.stringify(result[i].attribute_values)) : JSON.parse(JSON.stringify(attributeData))
        }
      } else {
        this.openAlertDialog('No product entry for this byno', 'Invoice Product Details View')
      }
    })
  }
  openAlertDialog(value: string, componentName?: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        debugger;
        let objInvDetails: any = {
          inv_byno: this.bynodata.inv_byno.toString().trim(),
          byno_serial: +this.bynodata.byno_serial,
          company_section_id: +this._localStorage.getCompanySectionId(),
          view: true,
          newSupplierLookupList: JSON.parse(JSON.stringify(this.bynodata.newSupplierLookupList)),
          productGroupLookupList: JSON.parse(JSON.stringify(this.bynodata.productGroupLookupList)),
          objInvoices: JSON.parse(JSON.stringify(this.bynodata.objInvoices)),
          invoiceDetailsList: JSON.parse(JSON.stringify(this.bynodata.invoiceDetailsList)),
          invoiceNosList: JSON.parse(JSON.stringify(this.bynodata.invoiceNosList)),
          poNosList: JSON.parse(JSON.stringify(this.bynodata.poNosList)),
          flags: JSON.parse(JSON.stringify(this.bynodata.flags))
        }
        localStorage.setItem("InvoiceByNoForInvoiceDetails", JSON.stringify(null));
        localStorage.setItem("InvoiceByNoForInvoiceView", JSON.stringify(objInvDetails));
        this._router.navigate(["/Inventory/Invoices"]);
      }
      _dialogRef = null;
    });
  }

  onExit() {
    let objInvDetails: any = {
      inv_byno: this.bynodata.inv_byno.toString().trim(),
      byno_serial: +this.bynodata.byno_serial,
      company_section_id: +this._localStorage.getCompanySectionId(),
      view: true,
      newSupplierLookupList: JSON.parse(JSON.stringify(this.bynodata.newSupplierLookupList)),
      productGroupLookupList: JSON.parse(JSON.stringify(this.bynodata.productGroupLookupList)),
      objInvoices: JSON.parse(JSON.stringify(this.bynodata.objInvoices)),
      invoiceDetailsList: JSON.parse(JSON.stringify(this.bynodata.invoiceDetailsList)),
      invoiceNosList: JSON.parse(JSON.stringify(this.bynodata.invoiceNosList)),
      poNosList: JSON.parse(JSON.stringify(this.bynodata.poNosList)),
      flags: JSON.parse(JSON.stringify(this.bynodata.flags))
    }
    localStorage.setItem("InvoiceByNoForInvoiceView", JSON.stringify(objInvDetails));
    this._router.navigate(["/Inventory/Invoices"]);
  }

}
