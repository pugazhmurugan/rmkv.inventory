import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicesExcessShortageComponent } from './invoices-excess-shortage.component';

describe('InvoicesExcessShortageComponent', () => {
  let component: InvoicesExcessShortageComponent;
  let fixture: ComponentFixture<InvoicesExcessShortageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicesExcessShortageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicesExcessShortageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
