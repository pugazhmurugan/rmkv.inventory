import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode } from 'src/app/common/models/common-model';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { InvoiceAttributesLookupComponent } from 'src/app/common/shared/invoice-attributes-lookup/invoice-attributes-lookup.component';
import { InvoiceProdDetailsConfirmationDialogComponent } from 'src/app/common/shared/invoice-prod-details-confirmation-dialog/invoice-prod-details-confirmation-dialog.component';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductLookupComponent } from 'src/app/common/shared/product-lookup/product-lookup.component';
import { InvoiceDetailsService } from '../../../services/purchases/invoice-prod-details/invoice-details.service';
import { InvoicesService } from '../../../services/purchases/invoices/invoices.service';
declare var jsPDF: any;
@Component({
  selector: 'app-invoices-excess-shortage',
  templateUrl: './invoices-excess-shortage.component.html',
  styleUrls: ['./invoices-excess-shortage.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class InvoicesExcessShortageComponent implements OnInit {

  componentVisibility: boolean = true;
  productList: any = [];
  counterList: any;
  attributesList: any = [];
  configList: any;
  clearDatas: any;
  sizeist: any;
  InsertJson: any[] = [];
  DeleteJson: any[] = [];
  rowLength: any;
  removeJsonForInsert: any[] = [];
  removeJsonForDelete: any[] = [];
  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Product_Group", "Qty", "ActQty", "Pcs", "ActPcs", "Supplier_Description", "Selling_Price", "HSN_Code", "Product_Entered", "Product_Details"];

  objInvoiceProdDetail: any = {
    inv_byno: '',
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    gstn_no: '',
    supplier_invoice_no: '',
    invoice_amount: '',
    invoice_entry_date: '',
    table_datas: [{
      product_group_desc: '',
      inv_det_actual_qty: '',
      inv_det_actual_pcs: '',
      supplier_description: '',
      hsncode: '',
      product_entered: ''
    }]
  }

  objInvoiceProdDetailEdit: any = {
    inv_byno: '',
    product_group_id: 0,
    product_group_desc: '',
    unique_serial: '',
    bypass: '',
    byno_serial: '',
    product_uom: '',
    supplier_description: '',
    total_pieces: '',
    cost_price: '',
    selling_price: '',
    inv_det_qty: '',
    inv_det_actual_qty: '',
    inv_det_pcs: '',
    inv_det_actual_pcs: '',
    product_details: [{
      product_code: '',
      product_description: '',
      inv_det_actual_qty: '',
      inv_det_actual_pcs: '',
      style_code: '',
      hsncode: [],
      gstn_no: '',
      counter: 0,
      size: '',
      attributesList: [],
      disabled: false,
      shortageFlag: false
    }]
  }

  unChangedInvoiceProdDetailEdit: any = {
    inv_byno: '',
    product_group_id: 0,
    product_group_desc: '',
    unique_serial: '',
    bypass: '',
    product_uom: '',
    byno_serial: '',
    supplier_description: '',
    total_pieces: '',
    cost_price: '',
    selling_price: '',
    inv_det_qty: '',
    inv_det_actual_qty: '',
    inv_det_pcs: '',
    inv_det_actual_pcs: '',
    product_details: [{
      product_code: '',
      product_description: '',
      inv_det_actual_qty: '',
      inv_det_actual_pcs: '',
      style_code: '',
      hsncode: [],
      gstn_no: '',
      counter: 0,
      size: '',
      attributesList: [],
      disabled: false,
      shortageFlag: false
    }]
  }

  modifiyInvoiceProdDetailEdit: any = {
    inv_byno: '',
    product_group_id: 0,
    product_group_desc: '',
    unique_serial: '',
    byno_serial: '',
    bypass: '',
    product_uom: '',
    supplier_description: '',
    total_pieces: '',
    inv_det_qty: '',
    inv_det_actual_qty: '',
    inv_det_pcs: '',
    inv_det_actual_pcs: '',
    product_details: [{
      product_code: '',
      product_description: '',
      inv_det_actual_qty: '',
      inv_det_actual_pcs: '',
      style_code: '',
      hsncode: [],
      gstn_no: '',
      counter: 0,
      size: '',
      attributesList: [],
      disabled: false,
      shortageFlag: false
    }]
  }

  bynoList: any = [{
    inv_byno: '',
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    gstn_no: '',
    supplier_invoice_no: '',
    invoice_amount: '',
    invoice_entry_date: '',
    table_datas: [{
      product_group_desc_desc: '',
      inv_det_actual_qty: '',
      inv_det_actual_pcs: '',
      supplier_description: '',
      hsncode: '',
      product_entered: ''
    }]
  }]

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  columns: any[] = [
    { display: "sno", editable: false },
    { display: "prodCode", editable: true },
    { display: "prodDesc", editable: false },
    { display: "mts", editable: false },
    { display: "pieces", editable: false },
    { display: "stylecode", editable: true },
    { display: "hsn", editable: true },
    { display: "gst", editable: false },
    { display: "counter", editable: true },
    { display: "size", editable: true }
  ]

  @ViewChildren("prodCode") prodCode: ElementRef | any;
  @ViewChildren("stylecode") stylecode: ElementRef | any;
  @ViewChildren("mts") mts: ElementRef | any;
  @ViewChildren("hsn") hsn: ElementRef | any;
  @ViewChildren("counter") counter: ElementRef | any;
  @ViewChildren("size") size: ElementRef | any;


  constructor(public _localStorage: LocalStorage,
    public _datePipe: DatePipe,
    public _router: Router,
    public _matDialog: MatDialog,
    public _gridKeyEvents: GridKeyEvents,
    public _invoiceProdDetails: InvoiceDetailsService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _excelService: ExcelService,
    public _decimalPipe: DecimalPipe,
    public _invoicesService: InvoicesService,
    public _keyPressEvents: KeyPressEvents) {
  }

  ngAfterViewInit() {
    document.getElementById('byno').focus();
  }

  ngOnInit() {
    this.objInvoiceProdDetail.inv_byno = JSON.parse(localStorage.getItem('invoiceDetailForExcessShortage')).inv_byno;

    this.checkInvoiceEntryConfigs();
    this.getCounterDetails();
    this.getInvoiceProdDetails()
  }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.objInvoiceProdDetailEdit.product_details);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.objInvoiceProdDetailEdit.product_details);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.objInvoiceProdDetailEdit.product_details);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.objInvoiceProdDetailEdit.product_details);
        break;
    }
  }

  public getAttributeSizeList(): void {
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        attribute_id: +this.configList[0].ipd_size_attribute_id
      }])
    }
    this._invoiceProdDetails.getAttributeSizeList(objData).subscribe((result: any) => {
      if (result)
        this.sizeist = result;
    });
  }

  public getProductLookupDetails(event, i): void {
    if (!this.configList[0].ipd_select_all_attributes) {
      if (event.keyCode == 13 && this.objInvoiceProdDetailEdit.product_details[i].product_code !== '') {
        let objData = {
          InvoiceProdDetails: JSON.stringify([{
            company_section_id: +this._localStorage.getCompanySectionId(),
            product_code: this.objInvoiceProdDetailEdit.product_details[i].product_code
          }])
        }
        this._invoiceProdDetails.getProductLookupDetails(objData).subscribe((result: any) => {
          if (result) {
            this.productList = result;
            this.objInvoiceProdDetailEdit.product_details[i].product_code = this.productList[0].product_code;
            this.objInvoiceProdDetailEdit.product_details[i].product_description = this.productList[0].product_name;
            this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = this.objInvoiceProdDetailEdit.bypass == true ? this.objInvoiceProdDetailEdit.total_pieces : 1; //this.productList && this.productList.inv_det_actual_pcs ? this.productList.inv_det_actual_pcs : "";
            this.objInvoiceProdDetailEdit.product_details[i].hsncode = result[0].product_hsn;
            this.objInvoiceProdDetailEdit.product_details[i].counter = this.productList[0].counter_id;
          } else {
            this.openProductLookup(i);
          }
        });
      } else if (event.keyCode == 13) {
        this.openProductLookup(i);
      }
    } else if (event.keyCode == 13) {
      this.openProductLookup(i);
    }
  }

  public getCounterDetails(): void {
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._invoiceProdDetails.getCounterDetails(objData).subscribe((result: any) => {
      if (result) {
        this.counterList = result;
      }
    });
  }

  public onClearChange(i): void {
    let productCode = this.objInvoiceProdDetailEdit.product_details[i].product_code;
    let attributeList = this.objInvoiceProdDetailEdit.product_details[i].attributesList;
    if (this.objInvoiceProdDetailEdit.product_details[i].product_code.length !== 8) {
      this.objInvoiceProdDetailEdit.product_details[i] = Object.assign({
        product_code: productCode,
        product_description: '',
        inv_det_actual_qty: '',
        inv_det_actual_pcs: '',
        style_code: '',
        hsncode: [],
        gstn_no: '',
        counter: 0,
        attributesList: attributeList
      })
      for (let j = 0; j < this.attributesList.length; j++) {
        this.objInvoiceProdDetailEdit.product_details[i].attributesList[j].attribute_value = '';
      }
      //this.unChangedInvoiceProdDetailEdit.product_details;
      // this.objInvoiceProdDetailEdit.product_details[i].product_code = productCode;
      setTimeout(() => {
        let input = this.prodCode.toArray();
        input[i].nativeElement.focus();
      }, 100);
    }
  }

  public getInvoiceProdDetails(): void {
    if (this.objInvoiceProdDetail.inv_byno.length == 8) {
      let objData = {
        InvoiceProdDetails: JSON.stringify([{
          byno: this.objInvoiceProdDetail.inv_byno,
          company_section_id: this._localStorage.getCompanySectionId(),
          warehouse_id: this._localStorage.getGlobalWarehouseId()
        }]),
      }
      this._invoiceProdDetails.getInvoiceProdDetails(objData).subscribe((result: any) => {
        if (result) {
          this.bynoList = JSON.parse(result.records1);
          if (JSON.parse(result.records2) !== null) {
            this.bynoList.table_datas = JSON.parse(result.records2);
            this.matTableConfig(this.bynoList.table_datas);
          } else {
            this._confirmationDialogComponent.openAlertDialog('No product entry for this byno', 'Invoices Excess/Shortage')
          }
          this.onEnterBynoLookupDetails()
        }
      });
    }
  }

  public onChangeClear(): void {
    this.bynoList = [];
    this.matTableConfig([]);
  }

  public matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }

  public newClick(): void {
    this.componentVisibility = !this.componentVisibility;
  }

  public onListClick(): void {
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  public onEnterBynoLookupDetails(): void {
    let bynoLookupList = {
      inv_byno: '',
      supplier_code: "",
      supplier_name: "",
      supplier_address: "",
      gstn_no: "",
      supplier_invoice_no: "",
      invoice_amount: "",
      invoice_entry_date: "",
      table_datas: []
    };
    bynoLookupList = this.bynoList.find(byno => byno.inv_byno.toLowerCase().trim() === this.objInvoiceProdDetail.inv_byno.toString().trim().toLowerCase());
    this.objInvoiceProdDetail.inv_byno = bynoLookupList && bynoLookupList.inv_byno ? bynoLookupList.inv_byno : this.objInvoiceProdDetail.inv_byno;
    this.objInvoiceProdDetail.supplier_code = bynoLookupList && bynoLookupList.supplier_code ? bynoLookupList.supplier_code : '';
    this.objInvoiceProdDetail.supplier_name = bynoLookupList && bynoLookupList.supplier_name ? bynoLookupList.supplier_name : '';
    this.objInvoiceProdDetail.supplier_address = bynoLookupList && bynoLookupList.supplier_address ? bynoLookupList.supplier_address : '';
    this.objInvoiceProdDetail.gstn_no = bynoLookupList && bynoLookupList.gstn_no ? bynoLookupList.gstn_no : '';
    this.objInvoiceProdDetail.supplier_invoice_no = bynoLookupList && bynoLookupList.supplier_invoice_no ? bynoLookupList.supplier_invoice_no : '';
    this.objInvoiceProdDetail.invoice_amount = bynoLookupList && bynoLookupList.invoice_amount ? this._decimalPipe.transform(bynoLookupList.invoice_amount, '1.2-2') : '';
    this.objInvoiceProdDetail.invoice_entry_date = bynoLookupList && bynoLookupList.invoice_entry_date ? this._datePipe.transform(bynoLookupList.invoice_entry_date, 'dd/MM/yyyy') : '';
  }

  public fetchInvoiceProductDetail(data): void {
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        inv_byno: data.inv_byno.toString().trim(),
        byno_serial: data.byno_serial.toString().trim(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._invoiceProdDetails.fetchInvoiceProdDetails(objData).subscribe((result: any) => {
      this.objAction.isEditing = true;
      this.objAction.isView = false;
      this.objInvoiceProdDetailEdit.inv_byno = data.inv_byno + '/' + data.byno_serial;
      this.objInvoiceProdDetailEdit.byno_serial = data.byno_serial;
      this.objInvoiceProdDetailEdit.product_group_desc = data.product_group_desc;
      this.objInvoiceProdDetailEdit.product_uom = data.product_uom;
      if (this.objInvoiceProdDetailEdit.product_uom !== 'No.s') {
        this.columns[3].editable = true;
      }
      this.objInvoiceProdDetailEdit.product_group_id = data.product_group_id;
      this.objInvoiceProdDetailEdit.unique_serial = data.unique_serial ? 'Yes' : 'No';//'No'
      this.objInvoiceProdDetailEdit.supplier_description = data.supplier_description;
      this.objInvoiceProdDetailEdit.total_pieces = data.inv_det_actual_pcs;
      this.objInvoiceProdDetailEdit.cost_price = data.actual_cost_price;
      this.objInvoiceProdDetailEdit.selling_price = data.selling_price;
      this.objInvoiceProdDetailEdit.inv_det_pcs = data.inv_det_pcs;
      this.objInvoiceProdDetailEdit.inv_det_qty = data.inv_det_qty;
      this.objInvoiceProdDetailEdit.inv_det_actual_pcs = data.inv_det_actual_pcs;
      this.objInvoiceProdDetailEdit.inv_det_actual_qty = data.inv_det_actual_qty;
      if (result) {
        let attributeData = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit.product_details[0].attributesList));
        this.objInvoiceProdDetailEdit.product_details = result;
        let attributeValue = '';
        for (let i = 0; i < this.objInvoiceProdDetailEdit.product_details.length; i++) {
          for (let j = 0; j < result[0].attribute_values.length; j++) {
            if (this.configList[0].ipd_size_entry == true && result[i].attribute_values[j].attribute_name == 'Size') {
              attributeValue = result[i].attribute_values[j].attribute_value;
              result[i].attribute_values.splice(j, 1);
            }
          }
          this.objInvoiceProdDetailEdit.product_details[i].product_code = result[i].product_code,
            this.objInvoiceProdDetailEdit.product_details[i].product_description = result[i].product_name,
            this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty = result[i].prod_qty,
            this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = result[i].prod_pcs,
            this.objInvoiceProdDetailEdit.product_details[i].style_code = result[i].supplier_style_code,
            this.objInvoiceProdDetailEdit.product_details[i].hsncode = result[i].hsncode,
            this.objInvoiceProdDetailEdit.product_details[i].gstn_no = result[i].gstn_no,
            this.objInvoiceProdDetailEdit.product_details[i].counter = result[i].counter_id,
            this.objInvoiceProdDetailEdit.product_details[i].size = attributeValue,
            this.objInvoiceProdDetailEdit.product_details[i].disabled = true,
            this.objInvoiceProdDetailEdit.product_details[i].shortageFlag = false,
            this.objInvoiceProdDetailEdit.product_details[i].attributesList = result[i].attribute_values.length > 1 ? JSON.parse(JSON.stringify(result[i].attribute_values)) : JSON.parse(JSON.stringify(attributeData))
        }
        if (+this.objInvoiceProdDetailEdit.inv_det_pcs < +this.objInvoiceProdDetailEdit.inv_det_actual_pcs) {
          if (this.InsertJson.length != 0) {
            for (let i = 0; i < this.InsertJson.length; i++) {
              if (this.InsertJson[i].inv_byno == data.inv_byno && this.InsertJson[i].byno_serial == this.objInvoiceProdDetailEdit.byno_serial) {
                this.objInvoiceProdDetailEdit.product_details.push(this.InsertJson[i])
              }
            }
          }
        }
        if (this.objInvoiceProdDetailEdit.inv_det_pcs > this.objInvoiceProdDetailEdit.inv_det_actual_pcs) {
          if (this.DeleteJson.length != 0) {
            for (let i = 0; i < this.DeleteJson.length; i++) {
              if (this.DeleteJson[i].inv_byno == data.inv_byno && this.DeleteJson[i].byno_serial == this.objInvoiceProdDetailEdit.byno_serial) {
                for (let z = +this.objInvoiceProdDetailEdit.inv_det_actual_pcs; z < this.objInvoiceProdDetailEdit.product_details.length; z++) {
                  this.objInvoiceProdDetailEdit.product_details[z].shortageFlag = true;
                }
              }
            }
          }
        }
        this.clearDatas = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit.product_details));
        this.componentVisibility = !this.componentVisibility;
        this.objInvoiceProdDetailEdit.bypass = false;
        this.unChangedInvoiceProdDetailEdit = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit));
        setTimeout(() => {
          let input = this.prodCode.toArray();
          input[0].nativeElement.focus();
        }, 100);
      } else {
        this._confirmationDialogComponent.openAlertDialog('No product entry for this byno serial', 'Invoices Excess/Shortage')
      }
    })
  }

  public addExcessRow(event: any): void {
    if (event.keyCode == 13) {
      this.rowLength = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit.product_details.length));
      if (+this.objInvoiceProdDetailEdit.inv_det_pcs < +this.objInvoiceProdDetailEdit.inv_det_actual_pcs) {
        let index = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit.product_details.length));
        let different = index - (+this.objInvoiceProdDetailEdit.inv_det_actual_pcs);
        let splice = index - different;
        if (index > +this.objInvoiceProdDetailEdit.inv_det_actual_pcs) {
          this.objInvoiceProdDetailEdit.product_details.splice(splice, different);
        } else {
          for (let i = index; i < this.objInvoiceProdDetailEdit.inv_det_actual_pcs; i++) {
            this.objInvoiceProdDetailEdit.product_details.push(JSON.parse(JSON.stringify(this.unChangedInvoiceProdDetailEdit.product_details[0])));
            this.objInvoiceProdDetailEdit.product_details[i].disabled = false;
            this.objInvoiceProdDetailEdit.product_details[i].style_code = '';
          }
        }
        for (let x = 0; x < this.objInvoiceProdDetailEdit.product_details.length; x++) {
          this.objInvoiceProdDetailEdit.product_details[x].shortageFlag = false;
        }
      } else if (+this.objInvoiceProdDetailEdit.inv_det_pcs > +this.objInvoiceProdDetailEdit.inv_det_actual_pcs) {
        let different = (+this.objInvoiceProdDetailEdit.inv_det_pcs) - (+this.objInvoiceProdDetailEdit.inv_det_pcs - +this.objInvoiceProdDetailEdit.inv_det_actual_pcs);
        this.objInvoiceProdDetailEdit.product_details = [];
        for (let x = 0; x < this.unChangedInvoiceProdDetailEdit.product_details.length; x++) {
          if (this.unChangedInvoiceProdDetailEdit.product_details[x].disabled)
            this.objInvoiceProdDetailEdit.product_details.push(JSON.parse(JSON.stringify(this.unChangedInvoiceProdDetailEdit.product_details[x])));
        }
        for (let z = 0; z < this.objInvoiceProdDetailEdit.product_details.length; z++) {
          this.objInvoiceProdDetailEdit.product_details[z].shortageFlag = false;
        }
        for (let y = +this.objInvoiceProdDetailEdit.inv_det_actual_pcs; y < this.objInvoiceProdDetailEdit.product_details.length; y++) {
          this.objInvoiceProdDetailEdit.product_details[y].shortageFlag = true;
        }
      } else if (+this.objInvoiceProdDetailEdit.inv_det_pcs == +this.objInvoiceProdDetailEdit.inv_det_actual_pcs) {
        this.objInvoiceProdDetailEdit.product_details = [];
        for (let x = 0; x < this.unChangedInvoiceProdDetailEdit.product_details.length; x++) {
          if (this.unChangedInvoiceProdDetailEdit.product_details[x].disabled) {
            this.objInvoiceProdDetailEdit.product_details.push(JSON.parse(JSON.stringify(this.unChangedInvoiceProdDetailEdit.product_details[x])));
          }
          this.objInvoiceProdDetailEdit.product_details[x].shortageFlag = false;
        }
      }
    }
  }

  public openProductAttributesDialog(i): void {
    const dialogRef = this._matDialog.open(InvoiceAttributesLookupComponent, {
      width: "500px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objInvoiceProdDetailEdit.product_details[i].product_code,
        product_group_desc: this.objInvoiceProdDetailEdit.product_group_desc,
        product_uom: this.objInvoiceProdDetailEdit.product_uom,
        product_group_id: this.objInvoiceProdDetailEdit.product_group_id,
        attributeValue: this.objInvoiceProdDetailEdit.product_details[i].attributesList,
        viewFlag: this.objInvoiceProdDetailEdit.product_details[i].attributesList[0].attribute_value !== '' ? true : false
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        let productList = result[1].productDetails;
        let attributeList = result[0];
        let checkFlag = result[2].applyCheck;
        if (!checkFlag) {
          for (let k = 0; k < attributeList.length; k++) {
            this.objInvoiceProdDetailEdit.product_details[i].attributesList[k].attribute_value = attributeList[k].attribute_value;
            this.objInvoiceProdDetailEdit.product_details[i].attributesList[k].attribute_value_id = attributeList[k].attribute_value_id;
          }
          this.objInvoiceProdDetailEdit.product_details[i].product_code = productList[0].product_code;
          this.objInvoiceProdDetailEdit.product_details[i].product_description = productList[0].product_name;
          this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = 1;
          this.objInvoiceProdDetailEdit.product_details[i].hsncode = productList[0].product_hsn !== "{}" ? productList[0].product_hsn : [];
          this.objInvoiceProdDetailEdit.product_details[i].counter = productList[0].counter_id;
        } else {
          for (let j = i; j < this.objInvoiceProdDetailEdit.product_details.length; j++) {
            for (let k = 0; k < attributeList.length; k++) {
              this.objInvoiceProdDetailEdit.product_details[j].attributesList[k].attribute_value = attributeList[k].attribute_value;
              this.objInvoiceProdDetailEdit.product_details[j].attributesList[k].attribute_value_id = attributeList[k].attribute_value_id;
            }
            this.objInvoiceProdDetailEdit.product_details[j].product_code = productList[0].product_code;
            this.objInvoiceProdDetailEdit.product_details[j].product_description = productList[0].product_name;
            this.objInvoiceProdDetailEdit.product_details[j].inv_det_actual_pcs = 1;
            this.objInvoiceProdDetailEdit.product_details[j].hsncode = productList[0].product_hsn !== "{}" ? productList[0].product_hsn : [];
            this.objInvoiceProdDetailEdit.product_details[j].counter = productList[0].counter_id;
          }
        }
      }
    })
    //}
  }

  public openProductLookup(i): void {
    if (!this.configList[0].ipd_select_all_attributes)
      this.openProductDialog(i);
    else
      this.openProductAttributesDialog(i);
  }

  public openProductDialog(i): void {
    if (!this.objInvoiceProdDetailEdit.product_details[i].product_description) {
      const dialogRef = this._matDialog.open(ProductLookupComponent, {
        width: "950px",
        panelClass: "custom-dialog-container",
        data: {
          searchString: this.objInvoiceProdDetailEdit.product_details[i].product_code,
          product_group_desc: this.objInvoiceProdDetailEdit.product_group_desc,
          product_uom: this.objInvoiceProdDetailEdit.product_uom,
          product_group_id: this.objInvoiceProdDetailEdit.product_group_id
        }
      });
      dialogRef.afterClosed().subscribe((result: any) => {
        if (result[0] !== undefined) {
          if (result[1].checkbox == true) {
            for (let j = i; j < this.objInvoiceProdDetailEdit.product_details.length; i++) {
              this.objInvoiceProdDetailEdit.product_details[i].product_code = result[0].product_code;
              this.objInvoiceProdDetailEdit.product_details[i].product_description = result[0].product_name;
              //this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty = result[0].inv_det_actual_qty;
              this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = this.objInvoiceProdDetailEdit.bypass == true ? this.objInvoiceProdDetailEdit.total_pieces : 1;;
              //this.objInvoiceProdDetailEdit.product_details[i].gstn_no = result[0].gstn_no;
              this.objInvoiceProdDetailEdit.product_details[i].hsncode = result[0].product_hsn;
              //this.objInvoiceProdDetailEdit.product_details[i].style_code = '';
              this.objInvoiceProdDetailEdit.product_details[i].counter = result[0].counter_id;
            }
            if (!this.columns[3].editable) {
              setTimeout(() => {
                let input = this.stylecode.toArray();
                input[0].nativeElement.focus();
              }, 100);
            } else {
              setTimeout(() => {
                let input = this.mts.toArray();
                input[0].nativeElement.focus();
              }, 100);
            }
          } else {
            this.objInvoiceProdDetailEdit.product_details[i].product_code = result[0].product_code;
            this.objInvoiceProdDetailEdit.product_details[i].product_description = result[0].product_name;
            // this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty = result[0].inv_det_actual_qty;
            this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = this.objInvoiceProdDetailEdit.bypass == true ? this.objInvoiceProdDetailEdit.total_pieces : 1;;
            //this.objInvoiceProdDetailEdit.product_details[i].gstn_no = result[0].gstn_no;
            this.objInvoiceProdDetailEdit.product_details[i].hsncode = result[0].product_hsn;
            //this.objInvoiceProdDetailEdit.product_details[i].style_code = '';
            this.objInvoiceProdDetailEdit.product_details[i].counter = result[0].counter_id;
          }
          if (!this.columns[3].editable) {
            setTimeout(() => {
              let input = this.stylecode.toArray();
              input[i].nativeElement.focus();
            }, 100);
          } else {
            setTimeout(() => {
              let input = this.mts.toArray();
              input[i].nativeElement.focus();
            }, 100);
          }
        }
      });
    }
  }

  public onEnterproductsLookUpDetail(i): void {
    let productsLookUpList: any = {
      product_code: "",
      product_description: "",
      inv_det_actual_qty: "",
      inv_det_actual_pcs: "",
      gstn_no: "",
      product_hsn: [],
      style_code: "",
      counter_data: []
    };
    productsLookUpList = this.productList;
    this.objInvoiceProdDetailEdit.product_details[i].product_code = productsLookUpList && productsLookUpList.product_code ? productsLookUpList.product_code : this.objInvoiceProdDetailEdit.product_details[i].product_code;
    this.objInvoiceProdDetailEdit.product_details[i].product_description = productsLookUpList && productsLookUpList.product_description ? productsLookUpList.product_description : "";
    this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty = productsLookUpList && productsLookUpList.inv_det_actual_qty ? productsLookUpList.inv_det_actual_qty : "";
    this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs = 1;//productsLookUpList && productsLookUpList.inv_det_actual_pcs ? productsLookUpList.inv_det_actual_pcs : "";
    this.objInvoiceProdDetailEdit.product_details[i].gstn_no = productsLookUpList && productsLookUpList.gstn_no ? productsLookUpList.gstn_no : "";
    this.objInvoiceProdDetailEdit.product_details[i].hsncode = [];
    for (let i = 0; i < productsLookUpList[0].product_hsn.length; i++) {
      this.objInvoiceProdDetailEdit.product_details[i].hsncode.push(productsLookUpList[i].product_hsn);
    }
    this.objInvoiceProdDetailEdit.product_details[i].counter_data = productsLookUpList && productsLookUpList.counter_data ? productsLookUpList.counter_data : [];
  }

  public getSectionWiseAttribute(): void {
    let objData = {
      InvoiceProdDetails: +this._localStorage.getCompanySectionId()
    }
    this._invoiceProdDetails.sectionWiseAttribute(objData).subscribe((result: any) => {
      if (result) {
        for (let i = 0; i < result.length; i++) {
          if (this.configList[0].ipd_size_entry == true && result[i].attribute_id !== 15) {
            this.attributesList.push({
              attribute_id: result[i].attribute_id,
              attribute_name: result[i].attribute_name,
              attribute_value: '',
              attribute_value_id: ''
            })
            for (let j = 0; j < this.objInvoiceProdDetailEdit.product_details.length; j++) {
              this.objInvoiceProdDetailEdit.product_details[j].attributesList.push({
                attribute_id: result[i].attribute_id,
                attribute_name: result[i].attribute_name,
                attribute_value: '',
                attribute_value_id: ''
              })
              this.unChangedInvoiceProdDetailEdit.product_details[j].attributesList.push({
                attribute_id: result[i].attribute_id,
                attribute_name: result[i].attribute_name,
                attribute_value: '',
                attribute_value_id: ''
              })
              this.modifiyInvoiceProdDetailEdit.product_details[j].attributesList.push({
                attribute_id: result[i].attribute_id,
                attribute_name: result[i].attribute_name,
                attribute_value: '',
                attribute_value_id: ''
              })
            }
          } else if (this.configList[0].ipd_size_entry == false) {
            this.attributesList.push({
              attribute_id: result[i].attribute_id,
              attribute_name: result[i].attribute_name,
              attribute_value: '',
              attribute_value_id: ''
            })
            for (let j = 0; j < this.objInvoiceProdDetailEdit.product_details.length; j++) {
              this.objInvoiceProdDetailEdit.product_details[j].attributesList.push({
                attribute_id: result[i].attribute_id,
                attribute_name: result[i].attribute_name,
                attribute_value: '',
                attribute_value_id: ''
              })
              this.unChangedInvoiceProdDetailEdit.product_details[j].attributesList.push({
                attribute_id: result[i].attribute_id,
                attribute_name: result[i].attribute_name,
                attribute_value: '',
                attribute_value_id: ''
              })
              this.modifiyInvoiceProdDetailEdit.product_details[j].attributesList.push({
                attribute_id: result[i].attribute_id,
                attribute_name: result[i].attribute_name,
                attribute_value: '',
                attribute_value_id: ''
              })
            }
          }
        }
      }
    });
  }

  private checkInvoiceEntryConfigs(): void {
    let objCheck = {
      group_section_id: +this._localStorage.getGlobalGroupSectionId()
    }
    this._invoicesService.checkInvoiceEntryConfigs(objCheck).subscribe((result: any) => {
      this.configList = result;
      //  if (this.configList[0].ipd_size_attribute_id)
      this.getSectionWiseAttribute();
      this.getAttributeSizeList();
    })
  }


  public onClear(exitFlag: boolean): void {
    if (!this.objAction.isView) {
      if (this.checkAnyChangesMade())
        this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
      else if (exitFlag)
        this.onListClick();
    } else {
      this.onListClick();
    }
  }

  private checkAnyChangesMade(): boolean {
    if (JSON.stringify(this.objInvoiceProdDetailEdit.product_details) == JSON.stringify(this.unChangedInvoiceProdDetailEdit.product_details))
      return false;
    else
      return true;
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Invoices Excess/Shortage";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  public openStyleCodeLookup(k, value): void {
    if (this.objInvoiceProdDetailEdit.product_details[k].style_code !== '') {
      if (this.objInvoiceProdDetailEdit.product_details[k].style_code !== '' && this.objInvoiceProdDetailEdit.product_details[k + 1].style_code == '') {
        let dialogRef = this._matDialog.open(InvoiceProdDetailsConfirmationDialogComponent, {
          width: "950px",
          panelClass: "custom-dialog-container",
          data: {
            confirmMessage: 'Apply same style code to',
            row: +this.objInvoiceProdDetailEdit.inv_det_actual_pcs
          }
        });
        dialogRef.afterClosed().subscribe((result: any) => {
          if (result) {
            for (let j = k; j < result; j++) {
              this.objInvoiceProdDetailEdit.product_details[j].style_code = value;
            }
            for (let y = 0; y < this.objInvoiceProdDetailEdit.product_details.length; y++) {
              if (this.objInvoiceProdDetailEdit.product_details[y].hsncode.length == 0) {
                let input = this.hsn.toArray();
                input[y].nativeElement.focus();
                return
              }
            }
          }
          dialogRef = null;
        });
        return
      };
    }
  }

  public openCounterlookup(k, value): void {
    if (this.objInvoiceProdDetailEdit.product_details[k].counter !== 0) {
      if (this.objInvoiceProdDetailEdit.product_details[k].counter !== 0 && this.objInvoiceProdDetailEdit.product_details[k + 1].counter == 0) {
        let dialogRef = this._matDialog.open(InvoiceProdDetailsConfirmationDialogComponent, {
          width: "950px",
          panelClass: "custom-dialog-container",
          data: {
            confirmMessage: 'Apply same counter to',
            row: +this.objInvoiceProdDetailEdit.total_pieces
          }
        });
        dialogRef.afterClosed().subscribe((result: any) => {
          if (result) {
            for (let j = k; j < result; j++) {
              this.objInvoiceProdDetailEdit.product_details[j].counter = value;
            }
            for (let y = 0; y < this.objInvoiceProdDetailEdit.product_details.length; y++) {
              if (this.objInvoiceProdDetailEdit.product_details[y].product_code == '') {
                let input = this.prodCode.toArray();
                input[y].nativeElement.focus();
                return
              }
            }
          }
          dialogRef = null;
        });
        return
      };
    }
  }

  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Product Group": this.dataSource.data[i].product_group_desc,
          "Qts/Mts": this.dataSource.data[i].inv_det_actual_qty,
          "Pcs": this.dataSource.data[i].product_billing_desc,
          "Supplier Description": this.dataSource.data[i].active,
          "Selling Price": this.dataSource.data[i].selling_price,
          "HSN Code": this.dataSource.data[i].hsncode,
          "Product Entered": this.dataSource.data[i].product_entered ? 'Yes' : 'No'
        });
      }
      this._excelService.exportAsExcelFile(json, "Invoice_Product_Details", datetime);
    } else
      this._confirmationDialogComponent.openAlertDialog("No records found, Load the data first", "Invoices Excess/Shortage");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.product_code);
        tempObj.push(e.product_name);
        tempObj.push(e.product_billing_desc);
        tempObj.push(e.active ? 'Yes' : 'No');
        prepare.push(tempObj);
      });
      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Product Code", "Product Name", "Billing Name", "Active"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Invoice_Product_Details' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No records found, Load the data first", "Invoices Excess/Shortage");
  }

  private resetScreen(): void {
    this.objInvoiceProdDetailEdit.product_details = [];
    this.objInvoiceProdDetailEdit.product_details = JSON.parse(JSON.stringify(this.clearDatas));
    this.objInvoiceProdDetailEdit.product_details = JSON.parse(JSON.stringify(this.unChangedInvoiceProdDetailEdit.product_details));
    let tempArray = [];
    for (let i = 0; i < this.objInvoiceProdDetailEdit.product_details.length; i++) {
      if (this.objInvoiceProdDetailEdit.product_details[i].shortageFlag == false) {
        tempArray.push(this.objInvoiceProdDetailEdit.product_details[i]);
      }
    }
    this.objInvoiceProdDetailEdit.inv_det_actual_pcs = +tempArray.length;
    this.objInvoiceProdDetailEdit.inv_det_actual_qty = +tempArray.length;
  }

  public addInvoiceProduct(): void {
    let tempData = [];
    for (let i = 0; i < this.objInvoiceProdDetailEdit.product_details.length; i++) {
      if ([null, undefined, ""].indexOf(this.objInvoiceProdDetailEdit.product_details[i].product_code) !== -1) {
        let input = this.prodCode.toArray();
        input[i].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog('Enter product code', 'Invoices Excess/Shortage');
        return
      } else if ([null, undefined, ""].indexOf(this.objInvoiceProdDetailEdit.product_details[i].product_description) !== -1) {
        let input = this.prodCode.toArray();
        input[i].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog('Enter valid product code', 'Invoices Excess/Shortage');
        return
      } else if ([null, undefined, "", 0, '0'].indexOf(this.objInvoiceProdDetailEdit.product_details[i].hsncode.length) !== -1) {
        let input = this.hsn.toArray();
        input[i].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog('Enter hsn code', 'Invoices Excess/Shortage');
        return
      }
      let attributeTempData = JSON.parse(JSON.stringify(this.objInvoiceProdDetailEdit.product_details[i].attributesList));
      if (this.configList[0].ipd_size_entry == true) {
        let attributeId = 0;
        let attributeName = '';
        let attributeValue = '';
        let attributeValueId = '';
        for (let x = 0; x < this.sizeist.length; x++) {
          if (this.sizeist[x].attribute_value == this.objInvoiceProdDetailEdit.product_details[i].size) {
            attributeId = this.sizeist[x].attribute_id;
            attributeName = this.sizeist[x].attribute_name;
            attributeValue = this.sizeist[x].attribute_value;
            attributeValueId = this.sizeist[x].attribute_value_id;
          }
        }
        for (let y = 0; y < this.objInvoiceProdDetailEdit.product_details[i].attributesList.length; y++) {
          if (this.objInvoiceProdDetailEdit.product_details[i].attributesList[y].attribute_id == attributeId) {
            attributeTempData.splice(y, 1);
          }
        }
        attributeTempData.push({
          attribute_id: attributeId,
          attribute_name: attributeName,
          attribute_value: this.objInvoiceProdDetailEdit.product_details[i].size,
          attribute_value_id: attributeValueId,
        })
      }
      let attributeData = [];
      for (let k = 0; k < attributeTempData.length; k++) {
        if (attributeTempData[k]) {
          if (attributeTempData[k].attribute_value !== '') {
            attributeData.push(attributeTempData[k]);
          }
        }
      }
      tempData.push({
        serial_no: i + 1,
        prod_qty: this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty == '' ? 0 : this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty,
        prod_pcs: this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_pcs,
        quantity: this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty == '' ? 0 : this.objInvoiceProdDetailEdit.product_details[i].inv_det_actual_qty,
        cost_price: this.objInvoiceProdDetailEdit.cost_price,
        selling_price: this.objInvoiceProdDetailEdit.selling_price,
        product_code: this.objInvoiceProdDetailEdit.product_details[i].product_code,
        hsncode: this.objInvoiceProdDetailEdit.product_details[i].hsncode.length == 0 ? [""] : JSON.parse(this.objInvoiceProdDetailEdit.product_details[i].hsncode),
        gst: 1,//this.objInvoiceProdDetailEdit.product_details[i].pcs,
        supplier_style_code: this.objInvoiceProdDetailEdit.product_details[i].style_code,
        counter_id: this.objInvoiceProdDetailEdit.product_details[i].counter,
        attribute_values: attributeData.length == 0 ? '[{}]' : attributeData
      })
    }
    var x = this.objInvoiceProdDetailEdit.inv_byno.split("/");
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        inv_byno: x[0],
        byno_serial: x[1],
        po_id: 1,//this.objInvoiceProdDetailEdit.LR_No,
        po_no: 1,// this.objInvoiceProdDetailEdit.LR_Date,
        details: tempData,
        entered_by: +this._localStorage.intGlobalUserId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        sales_location_id: +this._localStorage.getGlobalSalesLocationId()
      }]),
    }
    this._invoiceProdDetails.addInvoiceProdDetails(objData).subscribe((result: any) => {
      if (result) {
        this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New RGN record has been created", 'Invoices Excess/Shortage');
        this.componentVisibility = !this.componentVisibility;
        // this.resetScreen();
        this.getInvoiceProdDetails();
      }
    });
  }

  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public bynoProdSerialCorrection(x): string {
    if (x.toString().length == 1)
      return '000' + (x + 1);
    else if (x.toString().length == 2)
      return '00' + (x + 1);
    else if (x.toString().length == 3)
      return '0' + (x + 1);
    else
      return (x + 1).toString();
  }
  public addInsertOrDeleteJson(): void {
    let tempArray = [];
    for (let i = 0; i < this.objInvoiceProdDetailEdit.product_details.length; i++) {
      if (this.objInvoiceProdDetailEdit.product_details[i].shortageFlag == false) {
        tempArray.push(this.objInvoiceProdDetailEdit.product_details[i]);
      }
    }
    if (tempArray.length == this.objInvoiceProdDetailEdit.inv_det_actual_pcs) {
      if (tempArray.length == this.objInvoiceProdDetailEdit.inv_det_pcs) {
        for (let b = 0; b < this.dataSource.data.length; b++) {
          if (this.dataSource.data[b].byno_serial == this.objInvoiceProdDetailEdit.byno_serial) {
            this.dataSource.data[b].inv_det_actual_pcs = this.objInvoiceProdDetailEdit.inv_det_actual_pcs;
            this.dataSource.data[b].inv_det_actual_qty = this.objInvoiceProdDetailEdit.inv_det_actual_pcs;
          }
        }
        var byno = this.objInvoiceProdDetailEdit.inv_byno.split("/");
        this.removeJsonForInsert = [];
        for (let y = 0; y < this.InsertJson.length; y++) {
          if (this.InsertJson[y].byno_serial !== byno[1])
            this.removeJsonForInsert.push(this.InsertJson[y]);
        }
        this.InsertJson = [];
        this.InsertJson = this.removeJsonForInsert;
        this.removeJsonForDelete = [];
        for (let y = 0; y < this.DeleteJson.length; y++) {
          if (this.DeleteJson[y].byno_serial !== byno[1])
            this.removeJsonForDelete.push(this.DeleteJson[y]);
        }
        this.DeleteJson = [];
        this.DeleteJson = this.removeJsonForDelete;
        this.componentVisibility = !this.componentVisibility;
        console.log(this.InsertJson, 'InsertJson');
        console.log(this.DeleteJson, 'DeleteJson');
        this._confirmationDialogComponent.openAlertDialog('Changes have been saved', 'Invoices Excess/Shortage')
      } else {
        let Qty = this.objInvoiceProdDetailEdit.product_details.map(t => +t.inv_det_actual_qty).reduce((acc, value) => acc + value, 0);
        if (this.objInvoiceProdDetailEdit.inv_det_actual_qty !== Qty && this.objInvoiceProdDetailEdit.product_uom !== 'No.s') {
          this._confirmationDialogComponent.openAlertDialog('Please verify qty/pcs', 'Invoices Excess/Shortage');
          return
        }
        if (this.objInvoiceProdDetailEdit.inv_det_pcs < this.objInvoiceProdDetailEdit.inv_det_actual_pcs) {
          this.removeJsonForInsert = [];
          var byno = this.objInvoiceProdDetailEdit.inv_byno.split("/");
          for (let y = 0; y < this.InsertJson.length; y++) {
            if (this.InsertJson[y].byno_serial !== byno[1])
              this.removeJsonForInsert.push(this.InsertJson[y]);
          }
          this.InsertJson = [];
          this.InsertJson = this.removeJsonForInsert;
        }
        for (let x = 0; x < this.objInvoiceProdDetailEdit.product_details.length; x++) {
          if ([null, undefined, ""].indexOf(this.objInvoiceProdDetailEdit.product_details[x].product_code) !== -1) {
            let input = this.prodCode.toArray();
            input[x].nativeElement.focus();
            this._confirmationDialogComponent.openAlertDialog('Enter product code', 'Invoices Excess/Shortage');
            return
          } else if ([null, undefined, ""].indexOf(this.objInvoiceProdDetailEdit.product_details[x].product_description) !== -1) {
            let input = this.prodCode.toArray();
            input[x].nativeElement.focus();
            this._confirmationDialogComponent.openAlertDialog('Enter valid product code', 'Invoices Excess/Shortage');
            return
          } else if ([null, undefined, "", 0, '0'].indexOf(this.objInvoiceProdDetailEdit.product_details[x].hsncode.length) !== -1) {
            let input = this.hsn.toArray();
            input[x].nativeElement.focus();
            this._confirmationDialogComponent.openAlertDialog('Enter hsn code', 'Invoices Excess/Shortage');
            return
          }
          if (this.objInvoiceProdDetailEdit.product_details[x].disabled == false && this.objInvoiceProdDetailEdit.product_details[x].shortageFlag == false) {
            let bynoProdSerial = this.bynoProdSerialCorrection(x);
            if (bynoProdSerial == '00010')
              bynoProdSerial = '0010'
            else if (bynoProdSerial == '00100')
              bynoProdSerial = '0100'
            else if (bynoProdSerial == '01000')
              bynoProdSerial = '1000'
            this.InsertJson.push({
              attributesList: this.objInvoiceProdDetailEdit.product_details[x].attributesList,
              byno_prod_serial: bynoProdSerial,
              byno_serial: byno[1],
              counter: this.objInvoiceProdDetailEdit.product_details[x].counter,
              disabled: this.objInvoiceProdDetailEdit.product_details[x].disabled,
              gstn_no: this.objInvoiceProdDetailEdit.product_details[x].gstn_no,
              hsncode: this.objInvoiceProdDetailEdit.product_details[x].hsncode,
              inv_byno: byno[0],
              inv_det_actual_pcs: this.objInvoiceProdDetailEdit.product_details[x].inv_det_actual_pcs,
              inv_det_actual_qty: this.objInvoiceProdDetailEdit.product_details[x].inv_det_actual_qty,
              product_code: this.objInvoiceProdDetailEdit.product_details[x].product_code,
              product_description: this.objInvoiceProdDetailEdit.product_details[x].product_description,
              shortageFlag: this.objInvoiceProdDetailEdit.product_details[x].shortageFlag,
              size: this.objInvoiceProdDetailEdit.product_details[x].size,
              style_code: this.objInvoiceProdDetailEdit.product_details[x].style_code
            })
            this.removeJsonForDelete = [];
            for (let y = 0; y < this.DeleteJson.length; y++) {
              if (this.DeleteJson[y].byno_serial !== byno[1])
                this.removeJsonForDelete.push(this.DeleteJson[y]);
            }
            //if (this.removeJsonForDelete) {
            this.DeleteJson = [];
            this.DeleteJson = this.removeJsonForDelete;
            //}
          } else if (this.objInvoiceProdDetailEdit.product_details[x].disabled == true && this.objInvoiceProdDetailEdit.product_details[x].shortageFlag == true) {
            var byno = this.objInvoiceProdDetailEdit.inv_byno.split("/");
            let bynoProd = this.bynoProdSerialCorrection(x);
            if (bynoProd == '00010')
              bynoProd = '0010'
            else if (bynoProd == '00100')
              bynoProd = '0100'
            else if (bynoProd == '01000')
              bynoProd = '1000'
            let index = this.DeleteJson.findIndex(e => e.inv_byno == byno[0] && e.byno_serial == byno[1] && e.byno_prod_serial == bynoProd);
            if (index !== -1) {
              this.DeleteJson.splice(index, 1);
            }
            let bynoProdSerial = this.bynoProdSerialCorrection(x);
            if (bynoProdSerial == '00010')
              bynoProdSerial = '0010'
            else if (bynoProdSerial == '00100')
              bynoProdSerial = '0100'
            else if (bynoProdSerial == '01000')
              bynoProdSerial = '1000'
            this.DeleteJson.push({
              byno_prod_serial: bynoProdSerial,
              byno_serial: byno[1],
              inv_byno: byno[0],
            })
            this.removeJsonForInsert = [];
            for (let y = 0; y < this.InsertJson.length; y++) {
              if (this.InsertJson[y].byno_serial !== byno[1])
                this.removeJsonForInsert.push(this.InsertJson[y]);
            }
            // if (this.removeJsonForInsert.length != 0) {
            this.InsertJson = [];
            this.InsertJson = this.removeJsonForInsert;
            //}
          }
        }
        for (let b = 0; b < this.dataSource.data.length; b++) {
          if (this.dataSource.data[b].byno_serial == this.objInvoiceProdDetailEdit.byno_serial) {
            this.dataSource.data[b].inv_det_actual_pcs = this.objInvoiceProdDetailEdit.inv_det_actual_pcs;
            this.dataSource.data[b].inv_det_actual_qty = this.objInvoiceProdDetailEdit.inv_det_actual_pcs;
          }
        }
        this.componentVisibility = !this.componentVisibility;
        this._confirmationDialogComponent.openAlertDialog('Changes have been saved', 'Invoices Excess/Shortage');
        console.log(this.InsertJson, 'InsertJson');
        console.log(this.DeleteJson, 'DeleteJson');
      }
    } else {
      document.getElementById('actualpcs').focus();
      this._confirmationDialogComponent.openAlertDialog('Please verify qty/pcs', 'Invoices Excess/Shortage');
    }
  }

  public onQtyChange(): void {
    this.objInvoiceProdDetailEdit.inv_det_actual_qty = this.objInvoiceProdDetailEdit.inv_det_actual_pcs;
  }


  public onExit(): void {
    localStorage.setItem("invoiceDetailForExcessShortage", JSON.stringify(null));
    this._router.navigate(["/Inventory/Invoices"]);
  }
}

