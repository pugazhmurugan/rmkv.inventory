import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { CounterToGodownService } from '../../../services/goods-movement/internal-transfers/counter-to-godown/counter-to-godown.service';
import { GodownToCounterService } from '../../../services/goods-movement/internal-transfers/godown-to-counter/godown-to-counter.service';

@Component({
  selector: 'app-invoice-gst-status-update',
  templateUrl: './invoice-gst-status-update.component.html',
  styleUrls: ['./invoice-gst-status-update.component.scss'],
  providers: [DatePipe]
})
export class InvoiceGstStatusUpdateComponent implements OnInit {
  Date:any = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }
  
  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  showTable: boolean = false;

  constructor(public _minMaxDate: MinMaxDate,public _router: Router,
    public _localStorage: LocalStorage, private _datePipe: DatePipe,public _CommonService:CommonService,
    public _godowntocounter: GodownToCounterService,public _godownstocounter: CounterToGodownService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,) { 
      this.load.FromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
      this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
    }

  ngOnInit() {
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }
  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }


}
