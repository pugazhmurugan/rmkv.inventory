import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceGstStatusUpdateComponent } from './invoice-gst-status-update.component';

describe('InvoiceGstStatusUpdateComponent', () => {
  let component: InvoiceGstStatusUpdateComponent;
  let fixture: ComponentFixture<InvoiceGstStatusUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceGstStatusUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceGstStatusUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
