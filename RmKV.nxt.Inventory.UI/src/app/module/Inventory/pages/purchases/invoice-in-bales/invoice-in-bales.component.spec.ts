import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceInBalesComponent } from './invoice-in-bales.component';

describe('InvoiceInBalesComponent', () => {
  let component: InvoiceInBalesComponent;
  let fixture: ComponentFixture<InvoiceInBalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceInBalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceInBalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
