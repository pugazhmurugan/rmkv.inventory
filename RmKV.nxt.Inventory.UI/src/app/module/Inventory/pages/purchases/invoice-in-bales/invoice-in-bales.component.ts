import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode, Rights } from 'src/app/common/models/common-model';
import { AccountsLookupService } from 'src/app/common/services/accounts-lookup/accounts-lookup.service';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { InvoiceInBales } from '../../../model/purchases/invoice-in-bales-model';
import { InvoiceInBalesService } from '../../../services/purchases/invoice-in-bales/invoice-in-bales.service';
declare var jsPDF: any;

@Component({
  selector: 'app-invoice-in-bales',
  templateUrl: './invoice-in-bales.component.html',
  styleUrls: ['./invoice-in-bales.component.scss'],
  providers: [DatePipe]
})
export class InvoiceInBalesComponent implements OnInit {

  componentVisibility: boolean = true;
  supplierLookupList: any = [];
  focusFlag: boolean = false;
  sectionGrnList: any = [];
  invoiceBalesList: any = [];
  modifyInvoiceBalesList: any = [];
  objAction: EditMode = {
    isEditing: false,
    isView: false
  }

  objUnChangedAction: EditMode = {
    isEditing: false,
    isView: false
  }

  loadInvoiceInBales: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  objInvoiceBales: InvoiceInBales = {
    company_section_id: +this._localStorage.getCompanySectionId(),
    company_section_name: this._localStorage.getCompanySectionName(),
    section_name: this._localStorage.getCompanySectionName(),
    section_grn_no: 0,
    grn_no: 0,
    supplier_code: '',
    supplier_name: '',
    grn_date: '',
    no_of_invoices: 0,
    current_invoices: 0
  }

  objModifyInvoiceBales: InvoiceInBales = {
    company_section_id: +this._localStorage.getCompanySectionId(),
    company_section_name: this._localStorage.getCompanySectionName(),
    section_name: this._localStorage.getCompanySectionName(),
    section_grn_no: 0,
    grn_no: 0,
    supplier_code: '',
    supplier_name: '',
    grn_date: '',
    no_of_invoices: 0,
    current_invoices: 0
  }

  objUnChangedInvoiceBales: InvoiceInBales = {
    company_section_id: +this._localStorage.getCompanySectionId(),
    company_section_name: this._localStorage.getCompanySectionName(),
    section_name: this._localStorage.getCompanySectionName(),
    section_grn_no: 0,
    grn_no: 0,
    supplier_code: '',
    supplier_name: '',
    grn_date: '',
    no_of_invoices: 0,
    current_invoices: 0
  }

  rights: Rights = {
    Add: true,
    Update: true,
    View: true
  };
  Date: any = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  fromDate1: any = new Date();
  toDate1: any = new Date();
  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  public dateValidation: DateValidation = new DateValidation();

  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "GRN_No", "GRN_Date", "No_Of_Invoices", "Total_Invoices", "Action"];

  columns: any[] = [
    { display: 'sNo', editable: false },
    { display: 'supplierCode', editable: true },
    { display: "supplierName", editable: false },
    { display: "invoiceNo", editable: true },
    { display: "invoiceAmt", editable: true },
    { display: "invoiceDate", editable: true },
    { display: "invoiceEntered", editable: false },
    { display: "action", editable: true }
  ];
  @ViewChildren("sNo") sNo: ElementRef | any;
  @ViewChildren("supplierCode") supplierCode: ElementRef | any;
  @ViewChildren("supplierName") supplierName: ElementRef | any;
  @ViewChildren("invoiceNo") invoiceNo: ElementRef | any;
  @ViewChildren("invoiceAmt") invoiceAmt: ElementRef | any;
  @ViewChildren("invoiceDate") invoiceDate: ElementRef | any;
  @ViewChildren("invoiceEntered") invoiceEntered: ElementRef | any;
  @ViewChildren("action") action: ElementRef | any;
  @ViewChildren('supplierInput') supplierInput: ElementRef | any;

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    private _datePipe: DatePipe,
    private _matDialog: MatDialog,
    private _excelService: ExcelService,
    private _keyPressEvents: KeyPressEvents,
    private _gridKeyEvents: GridKeyEvents,
    private _confirmationDialog: ConfirmationDialogComponent,
    private _invoiceInBaleService: InvoiceInBalesService,
    private _accountsLookupService: AccountsLookupService,
  ) {
    this.loadInvoiceInBales.FromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
    this.getSectionGRNNos();
    this.getSupplierLookupList();
  }

  ngOnInit() {
  }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    debugger;
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.invoiceBalesList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.invoiceBalesList);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.invoiceBalesList);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.invoiceBalesList);
        break;
    }
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.loadInvoiceInBales.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.loadInvoiceInBales.FromDate = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.loadInvoiceInBales.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.loadInvoiceInBales.ToDate = date[0];
    this.toDate1 = date[1];
  }

  getRights() {
    var rights = this._localStorage.getMenuRights("/InvoiceinBales");
    // console.log('rights', rights);
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "View" && rights[x].Status == true) {
        this.rights.View = true;
      }
    }

  }

  /************************************************ Lookup Functionalities ************************************************/
  private getSupplierLookupList(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._accountsLookupService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      debugger;
      if (result) {
        this.supplierLookupList = JSON.parse(result);
        for (let i = 0; i < this.supplierLookupList.length; i++) {
          this.supplierLookupList[i].account_code = this.supplierLookupList[i].account_code.toString().trim();
          this.supplierLookupList[i].account_name = this.supplierLookupList[i].account_name.toString().trim();
        }
      }
    });
  }

  public openSupplierLookup(event: KeyboardEvent | any, i: number): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierName(i)) {
      this.openSupplierLookupDialog(i);
    }
  }

  private openSupplierLookupDialog(i: number): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "550px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.invoiceBalesList[i].supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        this.invoiceBalesList[i].supplier_code = result.account_code.toString().trim();
        this.invoiceBalesList[i].supplier_name = result.account_name.toString().trim();
      } else {
        this.invoiceBalesList[i].supplier_code = "";
        this.invoiceBalesList[i].supplier_name = "";
      }
    });
  }

  public onEnterSupplierLookupDetails(i: number): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: ""
    };

    supplierLookupList = this.supplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.invoiceBalesList[i].supplier_code.toString().trim().toLowerCase());
    this.invoiceBalesList[i].supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.invoiceBalesList[i].supplier_code.toString().trim();
    this.invoiceBalesList[i].supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
  }
  public checkValidSupplierName(i: number, event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.invoiceBalesList[i].supplier_code.toString().trim() !== '' && this.invoiceBalesList[i].supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier name", i, "Invoices In Bale",);
    }
  }

  // public checkValidSupplierName(i: number,event: KeyboardEvent, focus: string): void {
  //   if (event.keyCode !== 13 && !this.focusFlag && this.invoiceBalesList[i].supplier_code.toString().trim() !== '') {
  //     let index = this.supplierLookupList.findIndex(element => element.account_code.toString().trim() === this.invoiceBalesList[i].supplier_code.toString().trim());
  //     if (index === -1)
  //       this.openAlertDialog('Invalid supplier name', 'Invoice In Bales', focus);
  //   }
  // }

  public validateSupplierName(i: number): boolean {
    let index = this.supplierLookupList.findIndex(element => element.account_code.toString().trim() === this.invoiceBalesList[i].supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  /********************************************** CRUD Functionalities ************************************************/

  private getSectionGRNNos(): void {
    let objSection = {
      SectionGRN: JSON.stringify([{
        warehouse_id: +this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._invoiceInBaleService.getSectionGRNNos(objSection).subscribe((result: string) => {
      debugger;
      if (result) {
        this.sectionGrnList = JSON.parse(JSON.stringify(result));
        // console.log(this.sectionGrnList, 'Section GRN Nos');
      }
    });
  }

  public getSectionGRNDetails(): void {
    debugger;
    this.objInvoiceBales.grn_date = this._datePipe.transform((this.sectionGrnList[this.sectionGrnList.findIndex(
      x => +x.section_grn_no === +this.objInvoiceBales.section_grn_no)].grn_date), 'dd/MM/yyyy');
    this.objInvoiceBales.supplier_name = this.sectionGrnList[this.sectionGrnList.findIndex(
      x => +x.section_grn_no === +this.objInvoiceBales.section_grn_no)].supplier_name;
    this.objInvoiceBales.supplier_code = this.sectionGrnList[this.sectionGrnList.findIndex(
      x => +x.section_grn_no === +this.objInvoiceBales.section_grn_no)].supplier_code;
    this.objInvoiceBales.grn_no = this.sectionGrnList[this.sectionGrnList.findIndex(
      x => +x.section_grn_no === +this.objInvoiceBales.section_grn_no)].grn_no;
  }

  public loadInvoiceBales(): void {
    if (this.beforeLoadValidate()) {
      let objInvoice: any = {
        InvoiceInBale: JSON.stringify([{
          from_date: this.loadInvoiceInBales.FromDate,
          to_date: this.loadInvoiceInBales.ToDate,
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          company_section_id: +this._localStorage.getCompanySectionId()
        }])
      }
      debugger;
      this._invoiceInBaleService.getInvoiceInBales(objInvoice).subscribe((result: any) => {
        // console.log(result, 'Result');
        if (result)
          this.matTableConfig(JSON.parse(JSON.stringify(result)));
        else
          this._confirmationDialog.openAlertDialog('No records found', 'Invoices In Bale');
      });
    }
  }

  public addInvoiceRows(): void {
    let noOfRowsToAdd: number = 0;
    let lastIndex = this.objInvoiceBales.current_invoices;
    // if (event.keyCode === 13) {
    if (+this.objInvoiceBales.no_of_invoices !== 0) {
      if (+this.invoiceBalesList.length !== +this.objInvoiceBales.no_of_invoices) {
        if (+this.objInvoiceBales.current_invoices > +this.objInvoiceBales.no_of_invoices) {
          noOfRowsToAdd = +this.objInvoiceBales.no_of_invoices;
        } else if (+this.objInvoiceBales.current_invoices < +this.objInvoiceBales.no_of_invoices) {
          noOfRowsToAdd = +this.objInvoiceBales.no_of_invoices - +this.objInvoiceBales.current_invoices;
        } else if (+this.objInvoiceBales.current_invoices === +this.objInvoiceBales.no_of_invoices) {
          noOfRowsToAdd = 0;
        }
      }
    }
    for (let i = 0; i < +noOfRowsToAdd; i++) {
      this.invoiceBalesList.push({
        supplier_code: '',
        supplier_name: '',
        invoice_no: '',
        invoice_amount: 0,
        invoice_date: new Date(),
        invoice_entered: false
      });
    }
    if (this.invoiceBalesList.length > 0) {
      setTimeout(() => {
        let inputEls = this.supplierInput.toArray();
        inputEls[lastIndex].nativeElement.focus();
      }, 100);
    }
    this.objInvoiceBales.current_invoices = this.invoiceBalesList.length;
    // }
  }

  public removeInvoiceRows(i: number): void {
    this.invoiceBalesList.splice(i, 1);
    this.objInvoiceBales.current_invoices = this.invoiceBalesList.length;
  }

  public addInvoicesInBales(): void {
    if (this.beforeSaveValidate()) {
      let invoiceDetailsList: any[] = [];
      for (let i = 0; i < this.invoiceBalesList.length; i++) {
        invoiceDetailsList.push({
          serial_no: i + 1,
          company_section_id: +this._localStorage.getCompanySectionId(),
          supplier_code: this.invoiceBalesList[i].supplier_code,
          invoice_no: this.invoiceBalesList[i].invoice_no,
          invoice_date: this._datePipe.transform(this.invoiceBalesList[i].invoice_date, 'dd/MM/yyyy'),
          invoice_amount: +this.invoiceBalesList[i].invoice_amount,
          invoice_entered: this.invoiceBalesList[i].invoice_entered
        })
      }
      debugger;
      let objInvoice: any = {
        InvoiceInBale: JSON.stringify([{
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          grn_no: this.objInvoiceBales.grn_no,
          details: invoiceDetailsList,
          entered_by: +this._localStorage.intGlobalUserId(),
          company_section_id: +this._localStorage.getCompanySectionId()
        }])
      }
      this._invoiceInBaleService.addInvoiceInBales(objInvoice).subscribe((result: boolean) => {
        if (result) {
          this.loadInvoiceBales();
          this._confirmationDialog.openAlertDialog(this.objAction.isEditing ? 'Changes have been saved' : 'New invoice in bale record has been created', 'Invoices In Bale');
          this.listClick();
        }
      })
    }
  }

  public modifyOrView(i: number, isEditing: boolean, isView: boolean): void {
    if (isEditing)
      this.objAction.isEditing = true;
    else if (isView)
      this.objAction.isView = true;
    let objInvoice = {
      InvoiceInBale: JSON.stringify([{
        warehouse_id: +this.dataSource.data[i].warehouse_id,
        grn_no: +this.dataSource.data[i].grn_no,
        company_section_id: +this.dataSource.data[i].company_section_id
        // company_section_id: this._localStorage.getCompanySectionId()
      }])
    }
    this._invoiceInBaleService.fetchInvoiceInBales(objInvoice).subscribe((result: any) => {
      debugger;
      if (result.returN_VALUE) {
        if (result.records1 != null && result.records2 != null) {
          this.objInvoiceBales = JSON.parse(result.records1)[0];
          this.objModifyInvoiceBales = JSON.parse(result.records1)[0];
          this.invoiceBalesList = JSON.parse(result.records2);
          this.modifyInvoiceBalesList = JSON.parse(result.records2);
          this.objInvoiceBales.grn_date = this._datePipe.transform(this.objInvoiceBales.grn_date, 'dd/MM/yyyy');
          this.objModifyInvoiceBales.grn_date = this._datePipe.transform(this.objModifyInvoiceBales.grn_date, 'dd/MM/yyyy');
          this.componentVisibility = !this.componentVisibility;
        }
        this.getSectionGRNNos();
      }
    });
  }

  public newClick(): void {
    if (this.rights.Add) {
      this.objAction = JSON.parse(JSON.stringify(this.objUnChangedAction));
      this.resetScreen();
      this.getSectionGRNNos();
      this.componentVisibility = !this.componentVisibility;
    }
  }

  public onClear(exitFlag?: boolean): void {
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost, Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.listClick();
  }

  public listClick(): void {
    this.objAction = JSON.parse(JSON.stringify(this.objUnChangedAction));
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  private matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }

  /****************************************************** Validations ***************************************************/

  openAlertDialog(value: string, i: number, componentName?: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this.supplierInput.toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  private beforeLoadValidate(): boolean {
    if (this.loadInvoiceInBales.FromDate.toString().trim().length !== 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialog.openAlertDialog('Invalid from date', 'Invoices In Bale');
      return false;
    } if (this.loadInvoiceInBales.ToDate.toString().trim().length !== 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialog.openAlertDialog('Invalid to date', 'Invoices In Bale');
      return false;
    } if (this.fromDate1 > this.toDate1) {
      document.getElementById('toDate').focus();
      this._confirmationDialog.openAlertDialog('From date must be less than to date', 'Invoices In Bale');
      return false;
    } else return true;
  }

  private beforeSaveValidate(): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoiceBales.section_grn_no) !== -1) {
      document.getElementById('sectionGrn').focus();
      this._confirmationDialog.openAlertDialog('Select section grn no', 'Invoices In Bale');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objInvoiceBales.no_of_invoices) !== -1) {
      document.getElementById('noOfInvoices').focus();
      this._confirmationDialog.openAlertDialog('Enter no of invoices', 'Invoices In Bale');
      return false;
    } if (+this.invoiceBalesList.length !== +this.objInvoiceBales.no_of_invoices) {
      this._confirmationDialog.openAlertDialog('Check no of Invoices and remove unwanted records');
      return false;
    } if (!this.checkIsAllRecordsFilledOut()) {
      return false;
    } else return true;
  }

  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  private checkIsAllRecordsFilledOut(): boolean {
    debugger;
    for (let i = 0; i < this.invoiceBalesList.length; i++) {
      if ([null, 'null', undefined, 'undefined', 0, '0', NaN, 'NaN', ''].indexOf(this.invoiceBalesList[i].supplier_code) !== -1) {
        this.openMustAlertDialog('Select supplier code', i, 'supplierCode', 'Invoices In Bale');
        return false;
      } if ([null, 'null', undefined, 'undefined', 0, '0', NaN, 'NaN', ''].indexOf(this.invoiceBalesList[i].invoice_no) !== -1) {
        this.openMustAlertDialog('Enter supplier invoice no', i, 'invoiceNo', 'Invoices In Bale');
        return false;
      } if (!this.checkIsInvoiceNoExists(i)) {
        this.openMustAlertDialog('Invoice no ' + this.invoiceBalesList[i].invoice_no + ' already exists', i, 'invoiceNo', 'Invoices In Bale');
        return false;
      } if ([null, 'null', undefined, 'undefined', 0, '0', NaN, 'NaN', ''].indexOf(this.invoiceBalesList[i].invoice_amount) !== -1) {
        this.openMustAlertDialog('Enter supplier invoice amount', i, 'invoiceAmt', 'Invoices In Bale');
        return false;
      } if (+this.invoiceBalesList[i].invoice_amount < 0) {
        this.openMustAlertDialog('Invalid supplier invoice amount', i, 'invoiceAmt', 'Invoices In Bale');
        return false;
      } if ([null, 'null', undefined, 'undefined', 0, '0', NaN, 'NaN', ''].indexOf(this.invoiceBalesList[i].invoice_date) !== -1 &&
        this.invoiceBalesList[i].invoice_date > new Date()) {
        this.openMustAlertDialog('Invalid supplier invoice date', i, 'invoiceDate', 'Invoices In Bale');
        return false;
      }
    } return true;
  }

  // private checkIsInvoiceNoExists(i: number): boolean {
  //   debugger;
  //   let sindex = this.invoiceBalesList.findIndex(x => x.supplier_code.toString().trim() === this.invoiceBalesList[i].supplier_code.toString().trim());
  //   let index = this.invoiceBalesList.findIndex(x => x.invoice_no.toString().trim() === this.invoiceBalesList[i].invoice_no.toString().trim());
  //   if (sindex !== -1 && index !== -1)
  //     return false;
  //   else return true;
  // }

  private checkIsInvoiceNoExists(i: number): boolean {
    // for (let x = 0; x < this.invoiceBalesList.length; x++) {
    //   if (i !== x) {
    //     if (this.invoiceBalesList[x].supplier_code === this.invoiceBalesList[i].supplier_code && this.invoiceBalesList[x].invoice_no === this.invoiceBalesList[i].invoice_no)
    //       return false;
    //   }
    // } return true;
    debugger;
    for (let x = 0; x < this.invoiceBalesList.length; x++) {
      for (let y = 0; y < this.invoiceBalesList.length; y++) {
        if (x !== y) {
          if (this.invoiceBalesList[x].supplier_code === this.invoiceBalesList[y].supplier_code && this.invoiceBalesList[x].invoice_no === this.invoiceBalesList[y].invoice_no)
            return false;
        }
      }
    } return true;
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objInvoiceBales) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.objModifyInvoiceBales)
      : JSON.stringify(this.objUnChangedInvoiceBales)) || JSON.stringify(this.invoiceBalesList) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.modifyInvoiceBalesList) : JSON.stringify([])))
      return true;
    else
      return false;
  }

  private openMustAlertDialog(value: string, i: number, focus: any, componentName: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        debugger;
        let input = this[focus].toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "GRN TO Warehouse";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.listClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  private resetScreen(): void {
    if (this.objAction.isEditing) {
      this.objInvoiceBales = JSON.parse(JSON.stringify(this.objModifyInvoiceBales));
      this.objModifyInvoiceBales = JSON.parse(JSON.stringify(this.objModifyInvoiceBales));
      this.invoiceBalesList = JSON.parse(JSON.stringify(this.modifyInvoiceBalesList));
    } else {
      this.Date = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
      this.objAction = JSON.parse(JSON.stringify(this.objUnChangedAction));
      this.objInvoiceBales = JSON.parse(JSON.stringify(this.objUnChangedInvoiceBales));
      this.objModifyInvoiceBales = JSON.parse(JSON.stringify(this.objUnChangedInvoiceBales));
      this.invoiceBalesList = [];
      this.modifyInvoiceBalesList = [];
    }
  }

  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "GRN No": this.dataSource.data[i].grn_no,
          "GRN Date": this._datePipe.transform(this.dataSource.data[i].grn_date, 'dd/MM/yyyy'),
          "Total Invoices": this.dataSource.data[i].total_invoices,
          "Invoices Entered": this.dataSource.data[i].invoices_entered
        });
      }
      this._excelService.exportAsExcelFile(json, "Invoices_In_Bale", datetime);
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "Invoices In Bale");
  }

  exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.grn_no);
        tempObj.push(this._datePipe.transform(e.grn_date, 'dd/MM/yyyy'));
        tempObj.push(e.total_invoices);
        tempObj.push(e.invoices_entered);
        prepare.push(tempObj);
      });

      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["GRN No", "GRN Date", "Total Invoices", "Invoices Entered"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('Invoices_In_Bale' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "Invoices_In_Bale");
  }
}
