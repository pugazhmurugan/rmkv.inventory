import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { searchDialog } from 'src/app/common/shared/attribute-lookup/attribute-lookup.component';

@Component({
  selector: 'app-discount-lookup',
  templateUrl: './discount-lookup.component.html',
  styleUrls: ['./discount-lookup.component.scss']
})
export class DiscountLookupComponent implements OnInit {

  constructor(public _dialogRef: MatDialogRef<DiscountLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: searchDialog,
    public _matDialog: MatDialog,) { this._dialogRef.disableClose = true; }

  ngOnInit() {
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }
  
}
