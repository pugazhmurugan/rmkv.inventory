import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscountLookupComponent } from './discount-lookup.component';

describe('DiscountLookupComponent', () => {
  let component: DiscountLookupComponent;
  let fixture: ComponentFixture<DiscountLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscountLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
