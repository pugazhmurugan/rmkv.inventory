import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseOrderMailComponent } from './purchase-order-mail.component';

describe('PurchaseOrderMailComponent', () => {
  let component: PurchaseOrderMailComponent;
  let fixture: ComponentFixture<PurchaseOrderMailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseOrderMailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseOrderMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
