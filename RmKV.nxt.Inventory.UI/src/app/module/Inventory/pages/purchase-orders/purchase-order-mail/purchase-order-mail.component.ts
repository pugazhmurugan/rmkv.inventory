import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { PurchaseOrdersService } from '../../../services/purchase-orders/purchase-orders/purchase-orders.service';

@Component({
  selector: 'app-purchase-order-mail',
  templateUrl: './purchase-order-mail.component.html',
  styleUrls: ['./purchase-order-mail.component.scss']
})
export class PurchaseOrderMailComponent implements OnInit {
  objMail: any = {
    po_id : '',
    email_id: '',
    bcc: '',
    agent_email_id: ''
  }
  constructor(
    public _router: Router,
    public _dialogRef: MatDialogRef<PurchaseOrderMailComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _localStorage: LocalStorage,
    public _purchaseOrderService: PurchaseOrdersService
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getInitialising();
  }

  private getInitialising() {
    this.objMail.po_id = this._data.emailPO.po_id;
    this.objMail.email_id = this._data.emailPO.email_id;
    this.objMail.bcc = this._data.emailPO.bcc;
    this.objMail.agent_email_id = this._data.emailPO.agent_email_id;
  }

  private beforeSendEmailValidate(): boolean {
    debugger;
    if (this.objMail.email_id.toString().trim() === '') {
      document.getElementById("toEmail").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter to mail id", "Purchase Order");
      return false;
    } else if (!this.objMail.email_id.toString().includes('@')) {
      document.getElementById('toEmail').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid to mail id", "Purchase Order");
      return false;
    } else if (this.objMail.email_id.toString().includes('@') && !this.objMail.email_id.toString().includes('.')) {
      document.getElementById('toEmail').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid to mail id", "Purchase Order");
      return false;
    } else if (this.objMail.bcc.toString().trim() === '') {
      document.getElementById("bccMail").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter bcc mail id", "Purchase Order");
      return false;
    } else if (!this.objMail.bcc.toString().includes('@')) {
      document.getElementById('bccMail').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid bcc mail id", "Purchase Order");
      return false;
    } else if (this.objMail.bcc.toString().includes('@') && !this.objMail.bcc.toString().includes('.')) {
      document.getElementById('bccMail').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid bcc mail id", "Purchase Order");
      return false;
    }  else if (this.objMail.agent_email_id.toString().trim() === '') {
      document.getElementById("agentEmail").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter agent mail id", "Purchase Order");
      return false;
    } else if (!this.objMail.agent_email_id.toString().includes('@')) {
      document.getElementById('agentEmail').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid agent mail id", "Purchase Order");
      return false;
    } else if (this.objMail.agent_email_id.toString().includes('@') && !this.objMail.agent_email_id.toString().includes('.')) {
      document.getElementById('agentEmail').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid agent mail id", "Purchase Order");
      return false;
    } else return true;
  }

  public checkForAgentEmailDetails() {
    debugger;
    if (this.beforeSendEmailValidate()) {
      if ([null, 'null', NaN, 'NaN', undefined, 'undefined', '0', 0, ''].indexOf(this.objMail.agent_email_id) !== -1) {
        this.openConfirmationDialog('Are you sure want to sent a mail to your agent?');
      } else {
        this.sendManualMail();
      }
    }
  }

  private openConfirmationDialog(value) {
    debugger;
    const confirmationDialog = 1;
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      data: { confirmationDialog: confirmationDialog },
      panelClass: "custom-dialog-container",
      disableClose: false
    });
    dialogRef.componentInstance.confirmMessage = value;
    dialogRef.componentInstance.componentName = "Purchase Order";

    dialogRef.afterClosed().subscribe(result => {
      debugger;
      if (result == true) {
        this.objMail.agent_email_id = this.objMail.agent_email_id;
        this.sendManualMail();
      } else {
        this.objMail.agent_email_id = '';
        this.sendManualMail();
      }
      dialogRef = null;
    });
  }

  private sendManualMail() : void{
    debugger;
    // if (this.objMail.email_id.ToString().trim() != '') {
    //   this.objMail.cc = this.objMail.agent_email_id;
    //   this.objMail.section_name = this._localStorage.getCompanySectionName();
    //   this._purchaseOrderService.sendManualMail(this.objMail).subscribe((response: any) => {
    //     this._dialogRef.close(response);
    //   });
    // } else {
    //   this._confirmationDialogComponent.openAlertDialog('You have not entered to address', 'Purchase Order');
    // }
      let obj = {
        GetPOEmail: JSON.stringify([{
          po_id: this.objMail.po_id,
          company_section_id: +this._localStorage.getCompanySectionId(),
          email_id: this.objMail.email_id,
          bcc: 'teamleader@rmkv.com',
          cc: this.objMail.agent_email_id,
        }])
      }
      this._purchaseOrderService.getMailPurchaseOrderDetails(obj).subscribe((result: any) => {
        if (JSON.parse(result) !== null) {
          this._dialogRef.close(JSON.parse(result));
        }
      })
    }

  public onClear(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }


}
