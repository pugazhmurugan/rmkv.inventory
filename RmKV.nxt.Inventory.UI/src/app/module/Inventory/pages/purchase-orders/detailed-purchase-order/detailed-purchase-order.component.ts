import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatTableDataSource, MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { NgSelectComponent } from '@ng-select/ng-select';
import { EditMode } from 'src/app/common/models/common-model';
import { AccountsLookupService } from 'src/app/common/services/accounts-lookup/accounts-lookup.service';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ReasonComponent } from 'src/app/common/shared/reason/reason.component';
import { DetailedPurchaseOrderService } from '../../../services/purchase-orders/detailed-purchase-order/detailed-purchase-order.service';
import { PurchaseOrdersService } from '../../../services/purchase-orders/purchase-orders/purchase-orders.service';
import { DetailedPurchaseOrderLocationComponent } from '../detailed-purchase-order-location/detailed-purchase-order-location.component';
declare var jsPDF: any;

@Component({
  selector: 'app-detailed-purchase-order',
  templateUrl: './detailed-purchase-order.component.html',
  styleUrls: ['./detailed-purchase-order.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class DetailedPurchaseOrderComponent implements OnInit {
  componentVisibility: boolean = true;
  displayedColumns = ["Serial_No", "PO_No", "Po_Date", "Supplier_Name", "Brand", "Total_Amount", "Action"];
  Date: string = this._datePipe.transform(new Date());
  pono: any = "RF/2021/E2454";
  dataSource = new MatTableDataSource([]);
  isCancelled: boolean = false;
  newSupplierLookupList: any = [];
  loadSupplierLookupList: any = [];
  getDetailedPOList: any[] = [];
  focusFlag: boolean = false;
  newSupplier: boolean = false;
  sourceLocationList: any[] = [];
  getBrandList: any[] = [];
  getProductGroupList: any[] = [];
  getProductList: any[] = [];
  getColorList: any[] = [];
  getQualityList: any[] = [];
  getSizeList: any[] = [];
  getDesignCodeList: any[] = [];
  getLocationList: any[] = [];
  inputEls: any[];
  isClearMaster: boolean = true;
  isEditingLocation: boolean = true;

  objDetailedPO: any = {
    po_no: 0,
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    reference_no: '',
    raised_by: '',
    brand: '',
    sales_location_id: 0,
    po_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    delivery_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    delivery_schedule: '',
    to_sales_location_id: 0,
    payment_terms: '',
    credit_days: '',
    remarks: '',
    net_quantity: '',
    net_total: '',
    new_supplier: false
  }

  unChangedDetailedPO: any = {
    po_no: 0,
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    reference_no: '',
    raised_by: '',
    brand: '',
    sales_location_id: 0,
    po_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    delivery_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    delivery_schedule: '',
    to_sales_location_id: 0,
    payment_terms: '',
    credit_days: '',
    remarks: '',
    net_quantity: '',
    net_total: '',
    new_supplier: false
  }

  modifyDetailedPO: any = {
    po_no: 0,
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    reference_no: '',
    raised_by: '',
    brand: '',
    sales_location_id: 0,
    po_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    delivery_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    delivery_schedule: '',
    to_sales_location_id: 0,
    payment_terms: '',
    credit_days: '',
    remarks: '',
    net_quantity: '',
    net_total: '',
    new_supplier: false
  }

  objDetailedPOGridList: any = {
    product_group_name: '',
    product: '',
    design_code: '',
    color: '',
    quality: '',
    size: '',
    cost_price: '',
    qty: '',
    amount: '',
    getProductGroupList: [],
    getProductList: [],
    getLocationTableList: []
  }

  getDetailedPOGirdList: any[] = [{
    product_group_name: '',
    product: '',
    design_code: '',
    color: '',
    quality: '',
    size: '',
    cost_price: '',
    qty: '',
    amount: '',
    getProductGroupList: [],
    getProductList: [],
    getLocationTableList: []
  }];

  unChangedDetailedPOGirdList: any[] = [{
    product_group_name: '',
    product: '',
    design_code: '',
    color: '',
    quality: '',
    size: '',
    cost_price: '',
    qty: '',
    amount: '',
    getProductGroupList: [],
    getProductList: [],
    getLocationTableList: []
  }];

  modifyDetailedPOGirdList: any[] = [{
    product_group_name: '',
    product: '',
    design_code: '',
    color: '',
    quality: '',
    size: '',
    cost_price: '',
    qty: '',
    amount: '',
    getProductGroupList: [],
    getProductList: [],
    getLocationTableList: []
  }];


  objLoadDetailedPO: any = {
    from_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    to_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    supplier_code: '',
    supplier_name: '',
    status: "0",
    brand: ''
  }

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  contextMenuModel: any = {
    From_Row: "",
    To_Row: "",
  };

  getTotal: any = {
    amount_total: 0
  }


  columns: any[] = [
    { display: "sno", editable: false },
    { display: "productGroup", editable: true },
    { display: "product", editable: true },
    { display: "designCode", editable: true },
    { display: "color", editable: true },
    { display: "quality", editable: true },
    { display: "size", editable: true },
    { display: "cost", editable: true },
    { display: "qty", editable: true },
    { display: "amount", editable: false },
    { display: "action", editable: true }
  ]

  @ViewChildren("productGroup") productGroup: QueryList<NgSelectComponent>;
  @ViewChildren("product") product: QueryList<NgSelectComponent>;
  @ViewChildren("designCode") designCode: QueryList<NgSelectComponent>;
  @ViewChildren("color") color: QueryList<NgSelectComponent>;
  @ViewChildren("quality") quality: QueryList<NgSelectComponent>;
  @ViewChildren("size") size: QueryList<NgSelectComponent>;
  @ViewChildren("cost") cost: ElementRef | any;
  @ViewChildren("qty") qty: ElementRef | any;
  @ViewChildren("amount") amount: ElementRef | any;
  @ViewChildren("action") action: ElementRef | any;


  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        let response = this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.getDetailedPOGirdList);
        if (response && this.emptyFieldAlert(rowIndex))
          this.addNewDetailedGridEntry();
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.getDetailedPOGirdList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.getDetailedPOGirdList);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.getDetailedPOGirdList);
        break;
    }
  }

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    private _datePipe: DatePipe,
    public _matDialog: MatDialog,
    private _decimalPipe: DecimalPipe,
    private _gridKeyEvents: GridKeyEvents,
    private _keyPressEvents: KeyPressEvents,
    private _detailedPurchaseOrderService: DetailedPurchaseOrderService,
    private _confirmationDialog: ConfirmationDialogComponent,
    private _accountsLookupService: AccountsLookupService,
    public _purchaseOrdersService: PurchaseOrdersService,
    public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _excelService: ExcelService
  ) {
    this.getSupplierLookupList();
    this.getSourceLocations();
    this.addNewDetailedGridEntry();
    this.getBrandDetailsForPO();
    this.loadDetailedPoNoDetails();
    this.getDeliveryLocationListForPO();
    this.objLoadDetailedPO.from_date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
    this.startDate1 = new Date(new Date().setDate((new Date().getDate() - 2)));
  }

  ngOnInit() {

  }

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  public dateValidation: DateValidation = new DateValidation();
  startDate1: any = new Date();
  endDate1: any = new Date();
  deliveryDate1: any = new Date();
  poDate1: any = new Date();

  public validateStartDate(): void {
    let date = this.dateValidation.validateDate(this.objDetailedPO.from_date, this.startDate1, this.minDate, this.maxDate);
    this.objDetailedPO.from_date = date[0];
    this.startDate1 = date[1];
  }

  public validateEndDate(): void {
    let date = this.dateValidation.validateDate(this.objDetailedPO.to_date, this.endDate1, this._datePipe.transform(this.startDate1, 'dd/MM/yyyy'), this.maxDate);
    this.objDetailedPO.to_date = date[0];
    this.endDate1 = date[1];
  }

  public validateDeliveryDate(): void {
    let date = this.dateValidation.validateDate(this.objDetailedPO.delivery_schedule, this.deliveryDate1, this.minDate, this.maxDate);
    this.objDetailedPO.delivery_schedule = date[0];
    this.deliveryDate1 = date[1];
  }

  public validatePODate(): void {
    let date = this.dateValidation.validateDate(this.objDetailedPO.po_date, this.poDate1, this.minDate, this.maxDate);
    this.objDetailedPO.po_date = date[0];
    this.poDate1 = date[1];
  }

  /////////////////////////// Get Detailed PO No ///////////////////////////////////////

  private loadDetailedPoNoDetails(): void {
    debugger;
    let obj = {
      GetPONo: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._purchaseOrdersService.getPONoDetails(obj).subscribe((result: any) => {
      if (result && result.length > 0) {
        debugger;
        this.objDetailedPO.po_no = result;
        this.unChangedDetailedPO.po_no = result;
      }
    });
  }

  //////////////// Add New Detailed Grid Row /////////////////////////////////

  addNewDetailedGridEntry(isLocation? : boolean): void {
    debugger;
      isLocation = false;
      this.getDetailedPOGirdList.push(Object.assign({},
        {
          serial_no: 0,
          product_group_name: this.getDetailedPOGirdList[this.getDetailedPOGirdList.length - 1].product_group_name,
          product: this.getDetailedPOGirdList[this.getDetailedPOGirdList.length - 1].product,
          design_code: this.getDetailedPOGirdList[this.getDetailedPOGirdList.length - 1].design_code,
          color: this.getDetailedPOGirdList[this.getDetailedPOGirdList.length - 1].color,
          quality: this.getDetailedPOGirdList[this.getDetailedPOGirdList.length - 1].quality,
          size: this.getDetailedPOGirdList[this.getDetailedPOGirdList.length - 1].size,
          cost_price: '',
          qty: '',
          amount: '',
          getProductGroupList: [],
          getProductList: this.getDetailedPOGirdList[this.getDetailedPOGirdList.length - 1].getProductList,
        },
      ));
    this.focusProductGroup();
  }

  private emptyFieldAlert(index: number): boolean {
    debugger;
    if (this.getDetailedPOGirdList[index].product_group_name == "") {
      this.focusProductGroup();
      this._confirmationDialog.openAlertDialog("Enter product group", "Detailed Purchase Order");
      return false;
    } else if (this.getDetailedPOGirdList[index].product_group_name !== "" && this.getDetailedPOGirdList[index].cost_price === "") {
      let input = this.cost.toArray();
      input[index].nativeElement.focus();
      this._confirmationDialog.openAlertDialog("Enter cost price", "Detailed Purchase Order");
      return false;
    } else {
      return true;
    }
  }

  private getSourceLocations(): void {
    let objAllWorkingLocation = {
      GetDispatchName: JSON.stringify([{

      }])
    };
    this._purchaseOrdersService.getDispatchNameListDetails(objAllWorkingLocation).subscribe(
      (res: any) => {
        this.sourceLocationList = JSON.parse(res);
        this.sourceLocationList.splice(0, 1);
      });
  }

  ///////////////////// Get Brand List //////////////////////////////////////////

  private getBrandDetailsForPO(): void {
    let obj = {
      GetBrandDetailedPO: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    };
    this._detailedPurchaseOrderService.getBrandDetailedPODetails(obj).subscribe(
      (res: any) => {
        this.getBrandList = JSON.parse(res);
      });
  }

  ///////////////////// Get Product Group List //////////////////////////////////////////

  public getProductGroupDetailsForPO(): void {
    this.getDetailedPOGirdList.length = 0;
    this.getDetailedPOGirdList.push(Object.assign({}, this.objDetailedPOGridList));
    let obj = {
      GetProductGroupDetailedPO: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        brand: this.objDetailedPO.brand
      }])
    };
    this._detailedPurchaseOrderService.getProductGroupDetailedPODetails(obj).subscribe(
      (res: any) => {
        this.getProductGroupList = JSON.parse(res);
      });
  }

  ///////////////////// Get Product List //////////////////////////////////////////

  public getProductListForPO(index: number): void {
    let obj = {
      GetProductDetailedPO: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        brand: this.objDetailedPO.brand,
        product_group: this.getDetailedPOGirdList[index].product_group_name
      }])
    };
    this._detailedPurchaseOrderService.getProductDetailedPODetails(obj).subscribe(
      (res: any) => {
        this.getProductList = JSON.parse(res);
      });
  }

  ///////////////////// Get Design Code List //////////////////////////////////////////

  public getDesignCodeListForPO(index: number): void {
    let obj = {
      GetDesignCodePO: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        brand: this.objDetailedPO.brand,
        product_group: this.getDetailedPOGirdList[index].product_group_name,
        product: this.getDetailedPOGirdList[index].product,
      }])
    };
    this._detailedPurchaseOrderService.getDesignCodeDetailedPODetails(obj).subscribe(
      (res: any) => {
        this.getDesignCodeList = JSON.parse(res);
      });
  }

  ///////////////////// Get Delivery Location List //////////////////////////////////////////

  public getDeliveryLocationListForPO(): void {
    let obj = {
      GetLocationsForDPO: JSON.stringify([{
      }])
    };
    this._detailedPurchaseOrderService.getLocationsDetailedPODetails(obj).subscribe(
      (res: any) => {
        this.getLocationList = JSON.parse(res);
      });
  }

  ///////////////////// Get Color List //////////////////////////////////////////

  public getColorListForPO(index: number): void {
    let obj = {
      GetColorDetailedPO: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        brand: this.objDetailedPO.brand,
        product_group: this.getDetailedPOGirdList[index].product_group_name,
        product: this.getDetailedPOGirdList[index].product,
        style_code: this.getDetailedPOGirdList[index].design_code
      }])
    };
    this._detailedPurchaseOrderService.getColorListDetailedPODetails(obj).subscribe(
      (res: any) => {
        this.getColorList = JSON.parse(res);
      });
  }


  ///////////////////// Get Quality List //////////////////////////////////////////

  public getQualityListForPO(index: number): void {
    let obj = {
      GetQualityDetailedPO: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        brand: this.objDetailedPO.brand,
        product_group: this.getDetailedPOGirdList[index].product_group_name,
        product: this.getDetailedPOGirdList[index].product,
        style_code: this.getDetailedPOGirdList[index].design_code,
        color: this.getDetailedPOGirdList[index].color
      }])
    };
    this._detailedPurchaseOrderService.getQualityListDetailedPODetails(obj).subscribe(
      (res: any) => {
        this.getQualityList = JSON.parse(res);
      });
  }

  ///////////////////// Get Size List //////////////////////////////////////////

  public getizeListForPO(index: number): void {
    let obj = {
      GetSizeDetailedPO: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        brand: this.objDetailedPO.brand,
        product_group: this.getDetailedPOGirdList[index].product_group_name,
        product: this.getDetailedPOGirdList[index].product,
        style_code: this.getDetailedPOGirdList[index].design_code,
        color: this.getDetailedPOGirdList[index].color
      }])
    };
    this._detailedPurchaseOrderService.getSizeListDetailedPODetails(obj).subscribe(
      (res: any) => {
        this.getSizeList = JSON.parse(res);
      });
  }

  ////////////////////// Delete Detailed PO Grid ////////////////////////////////

  public deleteDetailedPOGrid(index) {
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this._dialogRef.componentInstance.confirmMessage =
      "Do you want to remove the product group?"
    this._dialogRef.componentInstance.componentName = "Detailed Purchase Order";
    return this._dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getDetailedPOGirdList.splice(index, 1);
      }
      this._dialogRef = null;
    });
  }

  public onCheckNewSupplier(): any {
    if (this.objDetailedPO.new_supplier === true) {
      this.objDetailedPO.supplier_code = '';
      this.objDetailedPO.supplier_name = '';
      this.objDetailedPO.supplier_address = '';
    } else {
      this.objDetailedPO.supplier_name = '';
      this.objDetailedPO.supplier_address = '';
      this.objDetailedPO.supplier_code = '';
    }
  }

  ///////////////////////////  New Rows Added Grid ////////////////////////////////////

  onEnterFromRow(event: KeyboardEvent, currentValue: String): boolean {
    if (event.keyCode >= 48 && event.keyCode <= 57) {
      currentValue = currentValue.concat(event.key);
      if (+currentValue > 0 && +currentValue <= this.getDetailedPOGirdList.length) {
        return true;
      } else
        return false;
    } else
      return false;
  }

  onEnterToRow(event: KeyboardEvent, currentValue: String): boolean {
    if (event.keyCode >= 48 && event.keyCode <= 57) {
      currentValue = currentValue.concat(event.key);
      if (+currentValue <= this.getDetailedPOGirdList.length) {
        let fromRowLength = this.contextMenuModel.From_Row.length;
        if (fromRowLength >= 2) {
          if ((currentValue.length === 1)) {
            if (+currentValue <= +this.contextMenuModel.From_Row.substring(0, 1)) {
              return true;
            } else {
              return false;
            }
          } else if ((currentValue.length === 2)) {
            if (+currentValue >= +this.contextMenuModel.From_Row) {
              return true;
            } else {
              return false;
            }
          }
        } else if (fromRowLength <= 1) {
          return true;
        }
      } else
        return false;
    } else
      return false;
  }

  onContextMenuAddRow(objRow: any): void {
    if (!this.getDetailedPOGirdList[0].product_group_name) {
      this.focusProductGroup();
      this._confirmationDialog.openAlertDialog("Enter product group", "Detailed Purchase Order");
    } else if (!this.getDetailedPOGirdList[0].product) {
      this._confirmationDialog.openAlertDialog("Enter product", "Detailed Purchase Order");
      this.focusProduct();
    } else {
      let validateFlag: boolean = false;
      if (this.getDetailedPOGirdList.length > 1) {
        this.getDetailedPOGirdList.forEach((item: any, i: number) => {
          if (!item.product_group_name && !item.product)
            this.getDetailedPOGirdList.splice(i, 1);
        });
      }
      if (objRow.From_Row.toString().trim() !== "" && +objRow.From_Row !== 0 && objRow.To_Row.toString().trim() !== "" && +objRow.To_Row !== 0) {
        if (+objRow.From_Row <= +objRow.To_Row) {
          for (let i = +objRow.From_Row - 1; i < +objRow.To_Row; i++)
            this.createNewRow(i);
        } else {
          validateFlag = true;
          document.getElementById('toRow').focus();
          this._confirmationDialog.openAlertDialog("Enter value greater than from row", "Detailed Purchase Order");
        }
      } else if (objRow.From_Row.toString().trim() !== "" && +objRow.From_Row !== 0 && !objRow.To_Row || +objRow.To_Row === 0) {
        if (+objRow.From_Row <= +objRow.To_Row) {
          this.createNewRow(+objRow.From_Row - 1);
        } else {
          validateFlag = true;
          document.getElementById('toRow').focus();
          this._confirmationDialog.openAlertDialog("Enter value greater than from row", "Detailed Purchase Order");
        }
      }
      if (!validateFlag) {
        this.focusProductGroup();
        this.contextMenuModel = { From_Row: "", To_Row: "" };
      }
    }
  }

  focusProductGroup(): any {
    debugger;
    setTimeout(() => {
      this.productGroup.last.filterInput.nativeElement.focus();
      // this.productGroup.element.nativeElement.focus();
    }, 100);
  }

  focusProduct(): any {
    setTimeout(() => {
      this.product.last.filterInput.nativeElement.focus();
    }, 100);
  }

  focusDesignCode(): any {
    setTimeout(() => {
      this.designCode.last.filterInput.nativeElement.focus();
    }, 100);
  }

  focusColor(): any {
    setTimeout(() => {
      this.color.last.filterInput.nativeElement.focus();
    }, 100);
  }

  focusQuality(): any {
    setTimeout(() => {
      this.quality.last.filterInput.nativeElement.focus();
    }, 100);
  }

  focusSize(): any {
    setTimeout(() => {
      this.size.last.filterInput.nativeElement.focus();
    }, 100);
  }

  createNewRow(index: number): void {
    this.getDetailedPOGirdList.push(Object.assign({},
      {
        serial_no: 0,
        product_group_name: this.getDetailedPOGirdList[index].product_group_name,
        product: this.getDetailedPOGirdList[index].product,
        design_code: this.getDetailedPOGirdList[index].design_code,
        color: this.getDetailedPOGirdList[index].color,
        quality: this.getDetailedPOGirdList[index].quality,
        size: this.getDetailedPOGirdList[index].size,
        sales_location_name: '',
        cost_price: "",
        qty: "",
        Amount: "",
        getProductGroupList: [],
        getProductList: this.getDetailedPOGirdList[index].getProductList
      }
    ));
  }

  //////////////////// Get Supplier List //////////////////////////////

  private getSupplierLookupList(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._accountsLookupService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      debugger;
      if (result) {
        this.newSupplierLookupList = JSON.parse(result);
        this.loadSupplierLookupList = JSON.parse(result);
        this.newSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
        this.loadSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
      }
    });
  }

  //////////////////////////////// New Supplier Lookup /////////////////////////////////////////////

  public validateSupplierCode(): boolean {
    let index = this.newSupplierLookupList.findIndex(element => element.account_code.toString().trim() === this.objDetailedPO.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  // public checkValidNewSupplierName(event: KeyboardEvent, focus: string): any {
  //   if (event.keyCode !== 13 && !this.focusFlag && this.objDetailedPO.supplier_code.toString().trim() !== ''
  //     && this.objDetailedPO.supplier_name.toString().trim() === '') {
  //     this.openAlertDialog("Invalid supplier name", "Detailed Purchase Order", focus);
  //   }
  // }

  public openNewSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierCode()) {
      this.openNewSupplierLookupDialog();
    }
  }

  private openNewSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "550px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objDetailedPO.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objDetailedPO.supplier_code = result.account_code.toString().trim();
        this.objDetailedPO.supplier_name = result.account_name.toString().trim();
        this.objDetailedPO.supplier_address = result.account_name + '\n' + result.address1 + ' ' + result.address2 + ' ' + result.address3;
      } else {
        this.objDetailedPO.supplier_code = '';
        this.objDetailedPO.supplier_name = '';
        this.objDetailedPO.supplier_address = '';
      }
    });
  }

  public onEnterNewSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      address1: "",
      address2: "",
      address3: "",
    };
    supplierLookupList = this.newSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objDetailedPO.supplier_code.toString().trim().toLowerCase());
    this.objDetailedPO.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objDetailedPO.supplier_code.toString().trim();
    this.objDetailedPO.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
    let address1: string = supplierLookupList && supplierLookupList.address1 !== null ? supplierLookupList.address1 : '';
    let address2: string = supplierLookupList && supplierLookupList.address2 !== null ? supplierLookupList.address2 : '';
    let address3: string = supplierLookupList && supplierLookupList.address3 !== null ? supplierLookupList.address3 : '';
    this.objDetailedPO.supplier_address = this.objDetailedPO.supplier_name + '\n' + address1 + ' ' + address2 + ' ' + address3;
  }

  ///////////////////////////////// Load Supplier Lookup //////////////////////////////////////

  public validateLoadSupplierCode(): boolean {
    let index = this.loadSupplierLookupList.findIndex(element => element.account_code.toString().trim() === this.objLoadDetailedPO.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public openLoadSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateLoadSupplierCode()) {
      this.openLoadSupplierLookupDialog();
    }
  }

  // public checkValidLoadSupplierName(event: KeyboardEvent, focus: string): any {
  //   if (event.keyCode !== 13 && !this.focusFlag && this.objLoadDetailedPO.supplier_code.toString().trim() !== ''
  //     && this.objLoadDetailedPO.supplier_name.toString().trim() === '') {
  //     this.openAlertDialog("Invalid supplier name", "Detailed Purchase Order", focus);
  //   }
  // }

  private openAlertDialog(value: string, componentName: string, focus: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        document.getElementById(focus).focus();
      _dialogRef = null;
    });
  }

  private openLoadSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "550px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objLoadDetailedPO.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objLoadDetailedPO.supplier_code = result.account_code.toString().trim();
        this.objLoadDetailedPO.supplier_name = result.account_name.toString().trim();
        this.objLoadDetailedPO.supplier_address = result.account_name + '\n' + result.address1 + ' ' + result.address2 + ' ' + result.address3;
      } else {
        this.objLoadDetailedPO.supplier_code = '';
        this.objLoadDetailedPO.supplier_name = '';
        this.objLoadDetailedPO.supplier_address = '';
      }
    });
  }

  public onEnterLoadSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      address1: "",
      address2: "",
      address3: "",
    };
    supplierLookupList = this.loadSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objLoadDetailedPO.supplier_code.toString().trim().toLowerCase());
    this.objLoadDetailedPO.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objLoadDetailedPO.supplier_code.toString().trim();
    this.objLoadDetailedPO.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
  }

  ///////////////////////////// Load Detailed Purchase Order List /////////////////////////

  public matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }

  private beforeLoadValidate(): boolean {
    if (this.objLoadDetailedPO.from_date.toString() === '') {
      document.getElementById("startDate").focus();
      this._confirmationDialog.openAlertDialog("Enter from date", "Detailed Purchase Order");
      return false;
    } else if (this.objLoadDetailedPO.from_date.toString().length !== 10) {
      document.getElementById("startDate").focus();
      this._confirmationDialog.openAlertDialog("Enter valid from date", "Detailed Purchase Order");
      this.objLoadDetailedPO.from_date = '';
      return false;
    } else if (this.objLoadDetailedPO.to_date.toString() === '') {
      document.getElementById("endDate").focus();
      this._confirmationDialog.openAlertDialog("Enter to date", "Detailed Purchase Order");
      return false;
    } else if (this.objLoadDetailedPO.to_date.toString().length !== 10) {
      document.getElementById("endDate").focus();
      this._confirmationDialog.openAlertDialog("Enter valid to date", "Purchase Order");
      this.objLoadDetailedPO.to_date = '';
      return false;
    }
    else return true;
  }

  public loadDetailedPurchaseOrderList(): void {
    if (this.beforeLoadValidate()) {
      let obj = {
        LoadDetailedPO: JSON.stringify([{
          from_date: this.objLoadDetailedPO.from_date,
          to_date: this.objLoadDetailedPO.to_date,
          company_section_id: +this._localStorage.getCompanySectionId(),
          supplier_code: this.objLoadDetailedPO.supplier_code,
          brand: this.objLoadDetailedPO.brand,
          status: this.objLoadDetailedPO.status
        }])
      }
      this._detailedPurchaseOrderService.getDetailedPOList(obj).subscribe((result: any) => {
        if (JSON.parse(result) !== null) {
          this.getDetailedPOList = JSON.parse(result);
          this.matTableConfig(JSON.parse(result));
          console.log(this.getDetailedPOList, 'list')
        } else this._confirmationDialog.openAlertDialog("No record found", "Detailed Purchase Order")
      })
    }
  }


  //////////////////////////////////// Save Detailed Purchase Order List ///////////////////////////////////

  public beforeSaveValidate(): boolean {
    if (this.objDetailedPO.po_date.toString() === '') {
      document.getElementById("toDate").focus();
      this._confirmationDialog.openAlertDialog("Enter po date", "Detailed Purchase Order");
      return false;
    } else if (this.objDetailedPO.po_date.toString().length !== 10) {
      document.getElementById("toDate").focus();
      this._confirmationDialog.openAlertDialog("Enter valid po date", "Detailed Purchase Order");
      this.objDetailedPO.po_date = '';
      return false;
    } else if (this.newSupplier === false && this.objDetailedPO.supplier_code.toString().trim() === '') {
      document.getElementById("code").focus();
      this._confirmationDialog.openAlertDialog("Enter supplier code", "Detailed Purchase Order");
      return false;
    } else if (this.newSupplier === false && this.objDetailedPO.supplier_code.toString().trim() !== '' && this.objDetailedPO.supplier_name.toString().trim() === '') {
      document.getElementById("code").focus();
      this._confirmationDialog.openAlertDialog("Enter valid supplier code", "Detailed Purchase Order");
      return false;
    } else if (this.newSupplier === true && this.objDetailedPO.supplier_name.toString().trim() === '') {
      document.getElementById("name").focus();
      this._confirmationDialog.openAlertDialog("Enter supplier name", "Detailed Purchase Order");
      return false;
    } else if (this.newSupplier === true && this.objDetailedPO.supplier_address.toString().trim() === '') {
      document.getElementById("textareas").focus();
      this._confirmationDialog.openAlertDialog("Enter supplier address", "Detailed Purchase Order");
      return false;
    } else if (this.objDetailedPO.reference_no.toString().trim() === '') {
      document.getElementById("refNO").focus();
      this._confirmationDialog.openAlertDialog("Enter reference no", "Detailed Purchase Order");
      return false;
    } else if (this.objDetailedPO.brand === '') {
      document.getElementById("brand").focus();
      this._confirmationDialog.openAlertDialog("Select brand", "Detailed Purchase Order");
      return false;
    } else if (this.objDetailedPO.raised_by.toString().trim() === '') {
      document.getElementById("raisedBy").focus();
      this._confirmationDialog.openAlertDialog("Enter raised by", "Detailed Purchase Order");
      return false;
    } else if (this.objDetailedPO.delivery_schedule.toString().trim() === '') {
      document.getElementById("schedule").focus();
      this._confirmationDialog.openAlertDialog("Enter delivery schedule", "Detailed Purchase Order");
      return false;
    } else if (this.objDetailedPO.to_sales_location_id === 0) {
      document.getElementById('deliveryLoc').focus();
      this._confirmationDialog.openAlertDialog("Select delivery location", "Detailed Purchase Order");
      return false;
    }
    if (this.objDetailedPO.delivery_date.toString() === '') {
      document.getElementById("deliveryDate").focus();
      this._confirmationDialog.openAlertDialog("Enter delivery date", "Detailed Purchase Order");
      return false;
    } else if (this.objDetailedPO.delivery_date.toString().length !== 10) {
      document.getElementById("deliveryDate").focus();
      this._confirmationDialog.openAlertDialog("Enter valid delivery date", "Detailed Purchase Order");
      this.objDetailedPO.delivery_date = '';
      return false;
    } else if (this.objDetailedPO.payment_terms.toString().trim() === '') {
      document.getElementById("terms").focus();
      this._confirmationDialog.openAlertDialog("Enter payment terms", "Detailed Purchase Order");
      return false;
    } else if (this.objDetailedPO.credit_days.toString().trim() === '') {
      document.getElementById("creditDays").focus();
      this._confirmationDialog.openAlertDialog("Enter credit days", "Detailed Purchase Order");
      return false;
    } else return true;
  }

  public checkGridListEmptyRow(index: number): boolean {
    if (this.getDetailedPOGirdList.length === 1 && this.getDetailedPOGirdList[index].product_group_name.toString().trim() === '') {
      this.focusProductGroup();
      this._confirmationDialog.openAlertDialog("Enter product group details", "Detailed Purchase Order");
      return true;
    } else if (this.getDetailedPOGirdList.length === 1 && this.getDetailedPOGirdList[index].product_group_name.toString().trim() !== ''
      && this.getDetailedPOGirdList[index].product.toString().trim() === '') {
      this.focusProduct();
      this._confirmationDialog.openAlertDialog("Enter product details", "Detailed Purchase Order");
      return true;
    } else if (this.getDetailedPOGirdList.length === 1 && this.getDetailedPOGirdList[index].design_code.toString().trim() === '') {
      this.focusDesignCode();
      this._confirmationDialog.openAlertDialog("Enter product details", "Detailed Purchase Order");
      return true;
    } else if (this.getDetailedPOGirdList.length === 1 && this.getDetailedPOGirdList[index].color.toString().trim() === '') {
      this.focusColor();
      this._confirmationDialog.openAlertDialog("Enter product details", "Detailed Purchase Order");
      return true;
    } else if (this.getDetailedPOGirdList.length === 1 && this.getDetailedPOGirdList[index].quality.toString().trim() === '') {
      this.focusQuality();
      this._confirmationDialog.openAlertDialog("Enter product details", "Detailed Purchase Order");
      return true;
    } else if (this.getDetailedPOGirdList.length === 1 && this.getDetailedPOGirdList[index].size.toString().trim() === '') {
      this.focusSize();
      this._confirmationDialog.openAlertDialog("Enter product details", "Detailed Purchase Order");
      return true;
    } else if (this.getDetailedPOGirdList.length === 1 && this.getDetailedPOGirdList[index].cost_price.toString().trim() === '') {
      setTimeout(() => {
        this.cost.last.filterInput.nativeElement.focus();
      }, 100);
      this._confirmationDialog.openAlertDialog("Enter product details", "Detailed Purchase Order");
      return true;
    } else if (this.getDetailedPOGirdList.length === 1 && this.getDetailedPOGirdList[index].qty.toString().trim() === '') {
      setTimeout(() => {
        this.qty.last.filterInput.nativeElement.focus();
      }, 100);
      this._confirmationDialog.openAlertDialog("Enter product details", "Detailed Purchase Order");
      return true;
    } else return false;
  }

  public onClickProductGroupValidate(): boolean {
    debugger;
    if (this.objDetailedPO.brand === '') {
      document.getElementById("brand").focus();
      this._confirmationDialog.openAlertDialog("Select brand name ", "Detailed Purchase Order");
      return false;
    } else return true;
  }

  public onClickProductGroupGridValidate(i: number): boolean {
    debugger;
    if (this.getDetailedPOGirdList[i].product_group_name === '') {
      this.focusProductGroup();
      this._confirmationDialog.openAlertDialog("Select product group name ", "Detailed Purchase Order");
      return false;
    } else return true;
  }

  public onClickDesignCodeValidate(i: number): boolean {
    if (this.getDetailedPOGirdList[i].product === '') {
      this.focusProduct();
      this._confirmationDialog.openAlertDialog("Select product", "Detailed Purchase Order");
      return false;
    } else return true;
  }

  public onClickColorValidate(i: number): boolean {
    if (this.getDetailedPOGirdList[i].design_code === '') {
      setTimeout(() => {
        this.designCode.last.filterInput.nativeElement.focus();
      }, 100);
      this._confirmationDialog.openAlertDialog("Select design code", "Detailed Purchase Order");
      return false;
    } else return true;
  }

  public onClickQualityValidate(i: number): boolean {
    if (this.getDetailedPOGirdList[i].quality === '') {
      setTimeout(() => {
        this.quality.last.filterInput.nativeElement.focus();
      }, 100);
      this._confirmationDialog.openAlertDialog("Select quality", "Detailed Purchase Order");
      return false;
    } else return true;
  }

  public onClickSizeValidate(i: number): boolean {
    if (this.getDetailedPOGirdList[i].size === '') {
      setTimeout(() => {
        this.size.last.filterInput.nativeElement.focus();
      }, 100);
      this._confirmationDialog.openAlertDialog("Select quality", "Detailed Purchase Order");
      return false;
    } else return true;
  }

  private getLocationDetails(i: number): any[] {
    let locationdetails: any[] = [];
    for (let j = 0; j < this.getDetailedPOGirdList[i].getLocationTableList.length; j++) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.getDetailedPOGirdList[i].getLocationTableList[j].qty) === -1) {
        locationdetails.push({
          po_serial_no: i + 1,
          sales_location_id: this._localStorage.getGlobalSalesLocationId(),
          quantity: this.getDetailedPOGirdList[i].getLocationTableList[j].qty
        });
      }
    } return locationdetails;
  }

  public onlyAllowDecimalForCostPrice(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public onlyAllowDecimalForQty(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public getNetQuantityTotal(): void {
    this.objDetailedPO.net_quantity = this.getDetailedPOGirdList.map(t => (+t.qty)).reduce((acc, value) => acc + value, 0);
  }

  public getNetTotalTotal(): void {
    this.objDetailedPO.net_total = this.getDetailedPOGirdList.map(t => (+t.amount)).reduce((acc, value) => acc + value, 0);
  }

  public getTotalAmount(element: any): void {
    debugger;
    let costPrice: number = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.cost_price) === -1 ?
      element.cost_price : 0;
    let qty: number = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.qty) === -1 ?
      element.qty : 0;
    element.amount = +costPrice * +qty;
    this.getNetTotalTotal();
  }

  public onSaveDetailedPurchaseOrders(): void {
    debugger;
    let details: any = [];
    if (this.beforeSaveValidate()) {
      for (let i = 0; i < this.getDetailedPOGirdList.length; i++) {
        if (this.checkGridListEmptyRow(i)) {
          return;
        } else if (this.getDetailedPOGirdList.length !== 1 && this.getDetailedPOGirdList[i].product_group_name.toString().trim() === '') {
          this.getDetailedPOGirdList.splice(i, 0);
        } else {
          details.push({
            serial_no: i + 1,
            product_category: this.getDetailedPOGirdList[i].product_group_name,
            product: this.getDetailedPOGirdList[i].product,
            design_code: this.getDetailedPOGirdList[i].design_code,
            color: this.getDetailedPOGirdList[i].color,
            quality: this.getDetailedPOGirdList[i].quality,
            size: this.getDetailedPOGirdList[i].size,
            cost_price: +this.getDetailedPOGirdList[i].cost_price,
            quantity: +this.getDetailedPOGirdList[i].qty,
            amount: +this.getDetailedPOGirdList[i].amount,
            location_details: this.getDetailedPOGirdList[i].getLocationTableList ? this.getLocationDetails(i) : null,
          });
        }
      }
      let obj = {
        SaveDetailedPO: JSON.stringify([{
          sales_location_id: +this._localStorage.getGlobalSalesLocationId(),
          po_id: this.objAction.isEditing ? +this.objDetailedPO.po_id : 0,
          po_no: this.objDetailedPO.po_no,
          company_section_id: +this._localStorage.getCompanySectionId(),
          po_date: this.objDetailedPO.po_date,
          delivery_date: this.objDetailedPO.delivery_date,
          supplier_code: this.objDetailedPO.supplier_code,
          supplier_name: this.objDetailedPO.supplier_name,
          address: this.objDetailedPO.supplier_address,
          reference_no: this.objDetailedPO.reference_no.toString().trim(),
          raised_by: this.objDetailedPO.raised_by,
          brand: this.objDetailedPO.brand,
          net_amount: +this.objDetailedPO.net_total,
          net_quantity: +this.objDetailedPO.net_quantity,
          delivery_schedule: this.objDetailedPO.delivery_schedule,
          despatch_name: this.sourceLocationList.filter(x => +x.sales_location_id == +this.objDetailedPO.to_sales_location_id)[0].sales_location_name,
          delivery_location_id: +this.objDetailedPO.to_sales_location_id,
          payment_terms: this.objDetailedPO.payment_terms,
          credit_days: this.objDetailedPO.credit_days,
          remarks: this.objDetailedPO.remarks,
          new_supplier: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].
            indexOf(this.objDetailedPO.supplier_code) === -1 ? 1 : 0,
          entered_by: +this._localStorage.intGlobalUserId(),
          file_path: null,
          details: details,
        }])
      }
      this._detailedPurchaseOrderService.addDetailedPODetails(obj).subscribe(
        (result: any) => {
          if (result) {
            this._confirmationDialog.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New supplier order has been created", "Detailed Purchase Order");
            this.componentVisibility = !this.componentVisibility;
            this.loadDetailedPurchaseOrderList();
          }
        });
    }
  }

  ////////////////////////////// Delete Detailed PO List////////////////////////////

  public onClickDelete(index: any): any {
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this._dialogRef.componentInstance.confirmMessage =
      "Do you want to cancel the detailed purchase order?"
    this._dialogRef.componentInstance.componentName = "Purchase Order";
    return this._dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.openReasonDialog(index);
      }
      this._dialogRef = null;
    });
  }

  public openReasonDialog(index: number): void {
    debugger;
    let dialogRef = this._matDialog.open(ReasonComponent, {
      width: "75vw",
      panelClass: "custom-dialog-container",
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== null) {
        this.deleteDetailedPurchaseOrder(index, result);
      }
      this._dialogRef = null;
    });
  }

  private deleteDetailedPurchaseOrder(row: any, Reason: string): void {
    debugger;
    let objData = [{
      po_id: +this.dataSource.data[row].po_id,
      company_section_id: +this._localStorage.getCompanySectionId(),
      cancelled_by: +this._localStorage.intGlobalUserId(),
      cancelled_reason: Reason,
    }];
    let objSave = {
      CancelDetailedPO: JSON.stringify(objData)
    }
    debugger;
    this._detailedPurchaseOrderService.deleteDetailedPODetails(objSave).subscribe(
      (result: any) => {
        if (result !== null) {
          this._confirmationDialog.openAlertDialog("Added to cancelled list", "Detailed Purchase Order");
          this.loadDetailedPurchaseOrderList()
        }
      });
    this._dialogRef = null;
  }

  /////////////////////////////////  Fetch Detailed Purchase Order ///////////////////

  public fetchDetailedPODetails(index: number,isLocation? : boolean): void {
    debugger;
    this.objAction.isView = false;
    isLocation = true;
    this.componentVisibility = !this.componentVisibility;
    let objLoad = {
      FetchDetailedPO: JSON.stringify([{
        po_id: +this.dataSource.data[index].po_id,
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._detailedPurchaseOrderService.fetchDetailedPODetails(objLoad).subscribe((result: any) => {
      debugger;
      if (JSON.parse(result) !== null) {
        console.log(JSON.parse(result), 'fetch');
        this.objAction.isEditing = true;
        this.objAction.isView = false;
        this.getDetailedPOGirdList = JSON.parse(result);
        this.modifyDetailedPOGirdList = JSON.parse(result);
        this.objDetailedPO = JSON.parse(result)[0];
        this.modifyDetailedPO = JSON.parse(result)[0];
        let poDate1 = new Date(this.objDetailedPO.po_date);
        this.objDetailedPO.po_date = this._datePipe.transform(poDate1, 'dd/MM/yyyy');
        let deliveryDate1 = new Date(this.objDetailedPO.delivery_date);
        this.objDetailedPO.delivery_date = this._datePipe.transform(deliveryDate1, 'dd/MM/yyyy');
        let modifyPoDate1 = new Date(this.modifyDetailedPO.po_date);
        this.modifyDetailedPO.po_date = this._datePipe.transform(modifyPoDate1, 'dd/MM/yyyy');
        let modifyDeliveryDate1 = new Date(this.modifyDetailedPO.delivery_date);
        this.modifyDetailedPO.delivery_date = this._datePipe.transform(modifyDeliveryDate1, 'dd/MM/yyyy');
        for (let i = 0; i < JSON.parse(result).length; i++) {
          this.getDetailedPOGirdList[i].getLocationTableList = JSON.parse(JSON.parse(result)[i].location_details);
          this.modifyDetailedPOGirdList[i].getLocationTableList = JSON.parse(JSON.parse(result)[i].location_details);
        }
      }
    });
  }

  public viewDetailedPODetails(index: number): void {
    debugger;
    this.objAction.isView = true;
    this.componentVisibility = !this.componentVisibility;
    let objLoad = {
      FetchDetailedPO: JSON.stringify([{
        po_id: +this.dataSource.data[index].po_id,
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._detailedPurchaseOrderService.fetchDetailedPODetails(objLoad).subscribe((result: any) => {
      debugger;
      if (JSON.parse(result) !== null) {
        this.objAction.isEditing = false;
        this.objAction.isView = true;
        this.getDetailedPOGirdList = JSON.parse(result);
        this.modifyDetailedPOGirdList = JSON.parse(result);
        this.objDetailedPO = JSON.parse(result)[0];
        this.modifyDetailedPO = JSON.parse(result)[0];
        let poDate1 = new Date(this.objDetailedPO.po_date);
        this.objDetailedPO.po_date = this._datePipe.transform(poDate1, 'dd/MM/yyyy');
        let deliveryDate1 = new Date(this.objDetailedPO.delivery_date);
        this.objDetailedPO.delivery_date = this._datePipe.transform(deliveryDate1, 'dd/MM/yyyy');
        let modifyPoDate1 = new Date(this.modifyDetailedPO.po_date);
        this.modifyDetailedPO.po_date = this._datePipe.transform(modifyPoDate1, 'dd/MM/yyyy');
        let modifyDeliveryDate1 = new Date(this.modifyDetailedPO.delivery_date);
        this.modifyDetailedPO.delivery_date = this._datePipe.transform(modifyDeliveryDate1, 'dd/MM/yyyy');
      }
    });
  }

  checkIsEditingLocationDetails(isEditingLocation : boolean,index : number) : void {
    // if(isEditingLocation === true){
    //   let objData = this.getDetailedPOGirdList[index].getLocationTableList;
    //  this.objAction.isEditing === true;
    //   return objData;
    // }else {
    //   let objData = this.getDetailedPOGirdList[index].getLocationTableList;
    //   this.objAction.isEditing === false;
    //   return objData;
    // }
    }

  public openLocationTableDialog(index: number, element: any,isLocation? : boolean): void {
    debugger;
    let dialogRef = this._matDialog.open(DetailedPurchaseOrderLocationComponent, {
      width: "500px",
      // data: { index: index, objData:this.getDetailedPOGirdList[index].getLocationTableList,isEditingLocation :this.isEditingLocation , 
      data: { index: index, objData:'',
      isEditing: this.objAction.isEditing},
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      this.inputEls = this.qty.toArray();
      this.inputEls[index].nativeElement.focus();
      if (result) {
        debugger;
        element.qty = result.Total;
        this.getTotalQtyAmount(element);
        element.getLocationTableList = result.locationList;
        this.getNetQuantityTotal();
        this.getNetTotalTotal();
      }
    });
  }

  public getTotalQtyAmount(element: any): void {
    debugger;
    let costPrice: number = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.cost_price) === -1 ?
      element.cost_price : 0;
    let qty: number = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.qty) === -1 ?
      element.qty : 0;
    element.amount = +qty * +costPrice;
  }

  ////////////////////////// Clear Functionalities /////////////////////////////////////////

  public onClear(exitFlag: boolean): void {
    debugger;
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objDetailedPO) !== (!this.objAction.isEditing ? JSON.stringify(this.unChangedDetailedPO) :
      JSON.stringify(this.modifyDetailedPO)) || JSON.stringify(this.getDetailedPOGirdList) !== (!this.objAction.isEditing ?
        JSON.stringify(this.unChangedDetailedPOGirdList) : JSON.stringify(this.modifyDetailedPOGirdList))) {
      return true;
    } else {
      return false;
    }
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Purchase Order";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  private resetScreen(): void {
    debugger;
    if (this.objAction.isEditing) {
      this.contextMenuModel.From_Row = '';
      this.contextMenuModel.To_Row = '';
      this.objDetailedPO = JSON.parse(JSON.stringify(this.modifyDetailedPO));
      this.modifyDetailedPO = JSON.parse(JSON.stringify(this.modifyDetailedPO));
      this.getDetailedPOGirdList = JSON.parse(JSON.stringify(this.modifyDetailedPOGirdList));
      this.modifyDetailedPOGirdList = JSON.parse(JSON.stringify(this.modifyDetailedPOGirdList));
    } else {
      this.objDetailedPO = JSON.parse(JSON.stringify(this.unChangedDetailedPO));
      this.getDetailedPOGirdList = JSON.parse(JSON.stringify(this.unChangedDetailedPOGirdList));
      this.contextMenuModel.From_Row = '';
      this.contextMenuModel.To_Row = '';
    }
  }

  ///////////////////////////////// Exports /////////////////////////////////////////////////

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'PO No': this.dataSource.data[i].po_id,
          'Date': this.dataSource.data[i].po_date,
          'Supplier Name': this.dataSource.data[i].supplier_name,
          "Brand": this.dataSource.data[i].brand,
          "Total Amount": this.dataSource.data[i].net_amount,
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Detailed Purchase Order",
        datetime
      );
    } else
      this._confirmationDialog.openAlertDialog("No record found, Load the data first", "Detailed Purchase Order");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.po_no);
        tempObj.push(this._datePipe.transform(e.po_date, 'dd/MM/yyyy'));
        tempObj.push(e.supplier_name);
        tempObj.push(e.brand);
        tempObj.push(e.net_amount);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['PO No', 'Date', 'Supplier Name', 'Brand', 'Total Amount']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Detailed Purchase Order' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No record found, Load the data first", "Detailed Purchase Order");
  }

  public onClearView(): void {
    this.objAction.isView = true;
    this.componentVisibility = !this.componentVisibility;
  }

  public onListClick(): void {
    this.objAction.isView = true;
    this.componentVisibility = !this.componentVisibility;
  }

  public newClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.objAction.isEditing = false;
    this.objAction.isView = false;
    this.resetScreen();
  }

}
