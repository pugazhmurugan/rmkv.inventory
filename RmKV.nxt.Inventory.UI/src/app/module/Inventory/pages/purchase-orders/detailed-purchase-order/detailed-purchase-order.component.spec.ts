import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedPurchaseOrderComponent } from './detailed-purchase-order.component';

describe('DetailedPurchaseOrderComponent', () => {
  let component: DetailedPurchaseOrderComponent;
  let fixture: ComponentFixture<DetailedPurchaseOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedPurchaseOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedPurchaseOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
