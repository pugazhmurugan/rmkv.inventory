import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { element } from 'protractor';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { EditMode } from 'src/app/common/models/common-model';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ReasonComponent } from 'src/app/common/shared/reason/reason.component';
import { SingleFileAttachComponent } from 'src/app/common/shared/single-file-attach/single-file-attach.component';
import { PurchaseOrdersService } from '../../../services/purchase-orders/purchase-orders/purchase-orders.service';
import { PurchaseOrderMailComponent } from '../purchase-order-mail/purchase-order-mail.component';
import { PurchaseOrderSizeComponent } from '../purchase-order-size/purchase-order-size.component';
import { SupplierBankDetailsComponent } from '../supplier-bank-details/supplier-bank-details.component';
declare var jsPDF: any;
import * as jspdf from 'jspdf';
import * as html2canvas from 'html2canvas';
@Component({
  selector: 'app-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss'],
  providers: [DatePipe]
})
export class PurchaseOrderComponent implements OnInit {

  displayGrid: string;
  loggedSection: string = '';
  getCitiesList: any[] = [];
  getGroupList: any[] = [];
  getSupplierList: any[] = [];
  getUOMList: any[] = [];
  getPurchaseList: any[] = [];
  sourceLocationList: any[] = [];
  poNoList: string;

  cityControl = new FormControl();
  groupControl = new FormControl();
  supplierControl = new FormControl();
  supplierLoadControl = new FormControl();
  locationControl = new FormControl();
  filteredCities: Observable<any[]>;
  filteredGroups: Observable<any[]>;
  filteredSupplier: Observable<any[]>;
  filteredLoadSupplier: Observable<any[]>;
  filteredLocation: Observable<any[]>;
  OldDocumentPathForPO: any = '';
  OldDocumentPathForBank: any = '';
  document: any;
  tempFile: any = '';
  formData: any;
  isFileSelected: boolean = false;
  selectedDocument: File | any = {};
  mulFormData: any;
  selectedGridFiles: File[] | any[] = [];
  selectedFiles: File | any = {};
  selectedBankDocument: File | any = {};
  unChangedFiles: File[] | any[] = [];
  mulDocument: any = null;
  mulTempFile: any = null;
  fileIndex: number[] = [];

  hideFlag: any = {
    ProductFlag: true,
    ImageFlag: true,
    PreFlag: true,
    HSNFlag: true,
    BrandFlag: true,
    SleveeFlag: true,
    ColorFlag: true,
    NoDesignFlag: true,
    NoSetsFlag: true,
    NoColorsFlag: true,
    // PiecesFlag: true,
    QtyFlag: true,
    MeterFlag: true,
    RateFlag: true,
    AmountFlag: true
  }

  componentVisibility: boolean = true;
  isView: boolean = false;
  displayedColumns = ["Serial_No", "PO_No", "Po_Date", "Supplier_Name", "Supplier_Group", "Supplier_City", "Raised_By", "Dispatch_to", "Discount", "Quality", "Amount", "Action"];
  Date: string = this._datePipe.transform(new Date());
  pono: any = "RF/2021/E2454";
  dataSource = new MatTableDataSource([]);
  supplierCityList: any = [];
  supplierGroupList: any = [];
  SupplierBankDetails: any = {};
  purchaseOrderList: any = [];
  modeOfDispatchList: any = [];
  @ViewChild('file', null) fileInput: ElementRef;
  isCancelled: boolean = false;
  removedFileIndex: number[] = [];
  removedFiles: File | any = {};
  oldDocument: any;
  isAttach: boolean = false;
  supplierCtrl: FormControl;
  inputEls: any[];

  objAttachView = {
    file_path: ''
  }

  unChangedAttachView = {
    file_path: ''
  }


  getSizeTableList: any = [];
  SupplierGroup: any = [];

  objPurchaseOrder: any = {
    po_id: 0,
    po_no: '',
    purchase_order_no: 0,
    id: 0,
    date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    purchase_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    delivery_schedule: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    file_path: '',
    city: '',
    supplier_group_id: 0,
    supplier_group_name: '',
    account_code: '',
    account_name: '',
    gstn_no: '',
    address1: '',
    address2: '',
    address3: '',
    email: '',
    agent_name: '',
    agent_address1: '',
    agent_address2: '',
    agent_address3: '',
    agent_email: '',
    phones: '',
    pincode: '',
    uom_id: 0,
    uom_description: '',
    sales_location_id: 0,
    return_type: 0,
    reference_no: '',
    raised_by: '',
    discount: '',
    payment_terms: '',
    remarks: '',
    bank_name: '',
    ifsc_code: '',
    supplier_account_no: '',
    branch_name: '',
    bank_file_path: '',
    isBankFile: false,
    isCommonFile: false
  }

  unChangedPurchaseOrder: any = {
    po_id: 0,
    po_no: '',
    purchase_order_no: 0,
    id: 0,
    date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    purchase_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    delivery_schedule: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    file_path: '',
    city: '',
    supplier_group_id: 0,
    supplier_group_name: '',
    account_code: '',
    account_name: '',
    gstn_no: '',
    address1: '',
    address2: '',
    address3: '',
    email: '',
    agent_name: '',
    agent_address1: '',
    agent_address2: '',
    agent_address3: '',
    agent_email: '',
    phones: '',
    pincode: '',
    uom_id: 0,
    uom_description: '',
    sales_location_id: 0,
    return_type: 0,
    reference_no: '',
    raised_by: '',
    discount: '',
    payment_terms: '',
    remarks: '',
    bank_name: '',
    ifsc_code: '',
    supplier_account_no: '',
    branch_name: '',
    bank_file_path: '',
    isBankFile: false,
    isCommonFile: false
  }

  modifyPurchaseOrder: any = {
    po_id: 0,
    po_no: '',
    purchase_order_no: 0,
    id: 0,
    date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    purchase_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    delivery_schedule: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    file_path: '',
    city: '',
    supplier_group_id: 0,
    supplier_group_name: '',
    account_code: '',
    account_name: '',
    gstn_no: '',
    address1: '',
    address2: '',
    address3: '',
    email: '',
    agent_name: '',
    agent_address1: '',
    agent_address2: '',
    agent_address3: '',
    agent_email: '',
    phones: '',
    pincode: '',
    uom_id: 0,
    uom_description: '',
    sales_location_id: 0,
    return_type: 0,
    reference_no: '',
    raised_by: '',
    discount: '',
    payment_terms: '',
    remarks: '',
    bank_name: '',
    ifsc_code: '',
    supplier_account_no: '',
    branch_name: '',
    bank_file_path: '',
    isBankFile: false,
    isCommonFile: false
  }

  objLoadPurchase: any = {
    from_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    to_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    city: '',
    account_name: '',
    sales_location_name: '',
    sales_location_id: 0,
    raised_by: ''
  }

  unChangedLoadPurchase: any = {
    from_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    to_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    city: '',
    account_name: '',
    sales_location_name: '',
    sales_location_id: 0,
    raised_by: ''
  }

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  getTotal: any = {
    quantity_total: 0,
    amount_total: 0
  }

  emailPurchaseOrder: any = {
    po_no: 0,
    po_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    reference_no: '',
    company_name: '',
    company_address1: '',
    company_address2: '',
    company_address3: '',
    company_phone: '',
    company_gst_no: '',
    despatch_name: '',
    payment_terms: '',
    mode_of_despatch: '',
    raised_by: '',
    file_path: '',
    remarks: '',
    delivery_schedule: '',
    agent_name: '',
    discount: '',
    item_description: '',
    hsncode: '',
    quantity: '',
    supplier_name: '',
    address: '',
    email_id: ''
  }

  objEmailPurchaseOrder: any = {
    po_no: 0,
    po_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    reference_no: '',
    company_name: '',
    company_address1: '',
    company_address2: '',
    company_address3: '',
    company_phone: '',
    company_gst_no: '',
    despatch_name: '',
    payment_terms: '',
    mode_of_despatch: '',
    raised_by: '',
    file_path: '',
    remarks: '',
    delivery_schedule: '',
    agent_name: '',
    discount: '',
    item_description: '',
    hsncode: '',
    quantity: '',
    supplier_name: '',
    address: '',
    email_id: ''
  }

  objPurchaseOrderGridList: any = {
    product_name: "",
    hsn_code: "",
    image_path: '',
    brand: '',
    sleeve: '',
    color: '',
    no_of_design: "",
    no_of_sets: "",
    colors: "",
    qty: "",
    pieces: '',
    // mts: '',
    rate: "",
    amount: "",
    item_description: '',
    hsncode: '',
    quantity: '',
    getSizeTableList: []
  };


  getPurchaseOrderGridList: any[] = [{
    product_name: "",
    hsn_code: "",
    image_path: '',
    brand: '',
    sleeve: '',
    color: '',
    no_of_design: "",
    no_of_sets: "",
    colors: "",
    qty: "",
    pieces: '',
    // mts: '',
    rate: "",
    amount: "",
    item_description: '',
    hsncode: '',
    quantity: '',
    getSizeTableList: []
  }];

  unChangedPurchaseOrderGridList: any[] = [{
    product_name: "",
    hsn_code: "",
    image_path: '',
    brand: '',
    sleeve: '',
    color: '',
    no_of_design: "",
    no_of_sets: "",
    colors: "",
    qty: "",
    pieces: '',
    // mts: '',
    rate: "",
    amount: "",
    item_description: '',
    hsncode: '',
    quantity: '',
    getSizeTableList: []
  }];

  modifyPurchaseOrderGridList: any[] = [{
    product_name: "",
    hsn_code: "",
    image_path: '',
    brand: '',
    sleeve: '',
    color: '',
    no_of_design: "",
    no_of_sets: "",
    colors: "",
    qty: "",
    pieces: '',
    // mts: '',
    rate: "",
    amount: "",
    item_description: '',
    hsncode: '',
    quantity: '',
    getSizeTableList: []
  }];

  columns: any[] = [
    { display: "sno", editable: false },
    { display: "productName", editable: true },
    { display: "image", editable: true },
    { display: "pre", editable: false },
    { display: "hsnCode", editable: true },
    { display: "brand", editable: true },
    { display: "slevee", editable: true },
    { display: "color", editable: true },
    { display: "design", editable: true },
    { display: "sets", editable: true },
    { display: "colors", editable: true },
    { display: "qty", editable: true },
    { display: "mts", editable: true },
    { display: "rate", editable: true },
    { display: "amount", editable: false },
    { display: "action", editable: true }
  ]

  @ViewChildren("productName") productName: ElementRef | any;
  @ViewChildren("image") image: ElementRef | any;
  @ViewChildren("pre") pre: ElementRef | any;
  @ViewChildren("hsnCode") hsnCode: ElementRef | any;
  @ViewChildren("brand") brand: ElementRef | any;
  @ViewChildren("slevee") slevee: ElementRef | any;
  @ViewChildren("color") color: ElementRef | any;
  @ViewChildren("design") design: ElementRef | any;
  @ViewChildren("sets") sets: ElementRef | any;
  @ViewChildren("colors") colors: ElementRef | any;
  @ViewChildren("qty") qty: ElementRef | any;
  @ViewChildren("mts") mts: ElementRef | any;
  @ViewChildren("rate") rate: ElementRef | any;
  @ViewChildren("amount") amount: ElementRef | any;
  @ViewChildren("action") action: ElementRef | any;


  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        let response = this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.getPurchaseOrderGridList);
        if (response && this.emptyFieldAlert(rowIndex))
          this.addNewPurchaseGridEntry();
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.getPurchaseOrderGridList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.getPurchaseOrderGridList);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.getPurchaseOrderGridList);
        break;
    }
  }

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    private _datePipe: DatePipe,
    public _purchaseOrdersService: PurchaseOrdersService,
    public _dialog: MatDialog,
    private _excelService: ExcelService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _commonService: CommonService,
    public _gridKeyEvents: GridKeyEvents,
    public _keyPressEvents: KeyPressEvents,
    public _matDialog: MatDialog,
    public _minmax: MinMaxDate,
    public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
  ) {
    this.getSupplierCities();
    this.getSupplierGroupList();
    this.getSupplierByGroupIdList();
    this.getModeOfDispatchDetails();
    this.getSourceLocations();
    this.objLoadPurchase.from_date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
    this.startDate1 = new Date(new Date().setDate((new Date().getDate() - 2)));
  }

  ngOnInit() {

  }

  public onListClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.isView = false;
    this.objAction.isView = false;
    this.loadPurchaseOrderListDetails();
  }

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  public dateValidation: DateValidation = new DateValidation();
  startDate1: any = new Date();
  endDate1: any = new Date();
  deliveryDate1: any = new Date();

  public validateStartDate(): void {
    let date = this.dateValidation.validateDate(this.objLoadPurchase.from_date, this.startDate1, this.minDate, this.maxDate);
    this.objLoadPurchase.from_date = date[0];
    this.startDate1 = date[1];
  }

  public validateEndDate(): void {
    let date = this.dateValidation.validateDate(this.objLoadPurchase.to_date, this.endDate1, this._datePipe.transform(this.startDate1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoadPurchase.to_date = date[0];
    this.endDate1 = date[1];
  }

  public validateDeliveryDate(): void {
    let date = this.dateValidation.validateDate(this.objPurchaseOrder.delivery_schedule, this.deliveryDate1, this.minDate, this.maxDate);
    this.objPurchaseOrder.delivery_schedule = date[0];
    this.deliveryDate1 = date[1];
  }

  public addNewPurchaseGridForNOS(uomChange?: boolean): void {
    debugger;
    this.getPurchaseOrderGridList.length = 0;
    this.getPurchaseOrderGridList.push(Object.assign({}, this.objPurchaseOrderGridList));
    this.getSectionBasedProductName(uomChange);
    setTimeout(() => {
      let inputEls = this.productName.toArray();
      inputEls[inputEls.length - 1].nativeElement.focus();
    }, 100);
  }

  public addNewPurchaseGridEntry(): void {
    debugger;
    this.getPurchaseOrderGridList.push(Object.assign({}, this.objPurchaseOrderGridList));
    if (+this._localStorage.getCompanySectionId() === 4) {
      this.loggedSection = 'Enterprises';
      for (let i = 0; i < this.getPurchaseOrderGridList.length; i++) {
        this.getPurchaseOrderGridList[i].product_name = "Sarees";
        this.getPurchaseOrderGridList[i].hsn_code = "5407";
      }
    }
    setTimeout(() => {
      let inputEls = this.productName.toArray();
      inputEls[inputEls.length - 1].nativeElement.focus();
    }, 100);
  }

  private emptyFieldAlert(index: number): boolean {
    if (this.getPurchaseOrderGridList[index].product_name == "") {
      let input = this.productName.toArray();
      input[index].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter product name", "Purchase Order");
      return false;
    } else if (this.getPurchaseOrderGridList[index].product_name !== "" && this.getPurchaseOrderGridList[index].rate === "") {
      let input = this.rate.toArray();
      input[index].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter rate", "Purchase Order");
      return false;
    } else {
      return true;
    }
  }

  private clearSupplierListFunction(value: any): void {
    if (value.city == "") {
      this.objPurchaseOrder.supplier_group_name = "";
      this.objPurchaseOrder.account_name = "";
      this.objPurchaseOrder.account_code = "";
      this.objPurchaseOrder.address1 = "";
      this.objPurchaseOrder.gstn_no = "";
      this.objPurchaseOrder.email = "";
      this.objPurchaseOrder.agent_name = "";
      this.objPurchaseOrder.agent_email = "";
      this.objPurchaseOrder.phones = "";
    } else if (value.supplier_group_name == "") {
      this.objPurchaseOrder.account_name = "";
      this.objPurchaseOrder.account_code = "";
      this.objPurchaseOrder.address1 = "";
      this.objPurchaseOrder.gstn_no = "";
      this.objPurchaseOrder.email = "";
      this.objPurchaseOrder.agent_name = "";
      this.objPurchaseOrder.agent_email = "";
      this.objPurchaseOrder.phones = "";
    } else if (value.account_name == "") {
      this.objPurchaseOrder.address1 = "";
      this.objPurchaseOrder.gstn_no = "";
      this.objPurchaseOrder.email = "";
      this.objPurchaseOrder.agent_name = "";
      this.objPurchaseOrder.agent_email = "";
      this.objPurchaseOrder.phones = "";
    }
  }

  //////////////////// Get Dispatch No List ///////////////////////////

  private getSourceLocations(): void {
    let objAllWorkingLocation = {
      GetDispatchName: JSON.stringify([{

      }])
    };
    this._purchaseOrdersService.getDispatchNameListDetails(objAllWorkingLocation).subscribe(
      (res: any) => {
        this.sourceLocationList = JSON.parse(res);
        console.log(this.sourceLocationList, 'loc')
        this.filteredLocation = this.locationControl.valueChanges
          .pipe(startWith(''), map(value => typeof value === 'string' ? value : value.sales_location_name),
            map(name => name ? this._filterLocations(name) : this.sourceLocationList.slice()));
      }
    )
    this.sourceLocationList.splice(0, 1);
  }

  private _filterLocations(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.sourceLocationList.filter(option => option.sales_location_name.toLowerCase().indexOf(filterValue) === 0);
  }

  public displayLocationFn(locationId?: number): string {
    debugger;
    if (locationId) {
      const supplierGroup = this.sourceLocationList.find(f => f.sales_location_id === locationId);
      return supplierGroup && supplierGroup.sales_location_name ? supplierGroup.sales_location_name : '';
    }
  }

  //////////////////// Get City List ////////////////////

  private getSupplierCities(): void {
    debugger;
    this._purchaseOrdersService.getSupplierCitiesList().subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0) {
        debugger;
        this.getCitiesList = JSON.parse(result);
        this.filteredCities = this.cityControl.valueChanges
          .pipe(startWith(''), map(value => typeof value === 'string' ? value : value.city),
            map(name => name ? this._filterCity(name) : this.getCitiesList.slice()));
      }
    });
  }

  private _filterCity(city: string): any[] {
    const filterValue = city.toLowerCase();
    return this.getCitiesList.filter(option => option.city.toLowerCase().indexOf(filterValue) === 0);
  }

  public displayCitiesFn(city?: string): string {
    if (city) {
      const supplier = this.getCitiesList.find(f => f.city === city);
      return supplier && supplier.city ? supplier.city : '';

    }
  }



  ///////////////// Get Group List /////////////////////////////

  public getSupplierGroupList(isModify?: boolean, supplierGroupName?: string): void {
    debugger;
    if (!this.objAction.isEditing && this.objPurchaseOrder.city === '') {
      this.objPurchaseOrder.supplier_group_name = '';
      this.objPurchaseOrder.account_name = '';
      this.getGroupList = [];
      this.getSupplierList = [];
    }
    let obj = {
      SupplierGroup: JSON.stringify([{
        city: this.objPurchaseOrder.city
      }])
    }
    this._purchaseOrdersService.getSupplierGroupList(obj).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0) {
        debugger;
        this.getGroupList = JSON.parse(result);
        console.log(this.getGroupList, 'group')
        this.filteredGroups = this.groupControl.valueChanges
          .pipe(startWith(''), map(value => typeof value === 'string' ? value : value.supplier_group_name),
            map(name => name ? this._filter(name) : this.getGroupList.slice()));

        if (isModify) {
          this.filteredGroups = this.groupControl.valueChanges
            .pipe(startWith(supplierGroupName), map(value => typeof value === 'string' ? value : value.supplier_group_name),
              map(name => name ? this._filter(name) : this.getGroupList.slice()));
        }
      }
    });
  }

  private _filter(supplier_group_name: string): any[] {
    const filterValue = supplier_group_name.toLowerCase();
    return this.getGroupList.filter(option => option.supplier_group_name.toLowerCase().indexOf(filterValue) === 0);
  }

  public displayGroupFn(supplier_group_name?: any): string {
    if (supplier_group_name) {
      const supplierGroup = this.getGroupList.find(f => f.supplier_group_name === supplier_group_name);
      return supplierGroup && supplierGroup.supplier_group_name ? supplierGroup.supplier_group_name : '';
    }
  }

  public onEnterSupplierGroup(): void {
    debugger;
    if (!this.objAction.isEditing) {
      let supplierGroupList: any = {
        supplier_group_name: '',
        supplier_group_id: 0,
      }
      supplierGroupList = this.getGroupList.find(list => list.supplier_group_name.toLowerCase().trim() ===
        this.objPurchaseOrder.supplier_group_name.toString().trim().toLowerCase());
      this.objPurchaseOrder.supplier_group_id = supplierGroupList && supplierGroupList.supplier_group_id ? supplierGroupList.supplier_group_id : 0;
    }
  }

  /////////////////  Get Supplier By Group Id //////////////////

  public getSupplierByGroupIdList(isModify?: boolean, accountName?: string): void {
    debugger;
    if (!this.objAction.isEditing) {
      this.getSupplierList = [];
    }
    let obj = {
      SupplierByGroupId: JSON.stringify([{
        supplier_group_id: +this.objPurchaseOrder.supplier_group_id,
        city: this.objPurchaseOrder.city
      }])
    }
    this._purchaseOrdersService.getSupplierByGroupIdList(obj).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0) {
        debugger;
        this.getSupplierList = JSON.parse(result);
        this.filteredSupplier = this.supplierControl.valueChanges
          .pipe(startWith(''), map(value => typeof value === 'string' ? value : value.account_name.toString().trim()),
            map(name => name ? this._filterSupplier(name) : this.getSupplierList.slice()));

        this.filteredLoadSupplier = this.supplierLoadControl.valueChanges
          .pipe(startWith(''), map(value => typeof value === 'string' ? value : value.account_name.toString().trim()),
            map(name => name ? this._filterLoadSupplier(name) : this.getSupplierList.slice()));

        if (isModify) {
          this.filteredSupplier = this.supplierControl.valueChanges
            .pipe(startWith(accountName), map(value => typeof value === 'string' ? value : value.account_name.toString().trim()),
              map(name => name ? this._filterSupplier(name) : this.getSupplierList.slice()));
        }
      }
    });
  }

  private _filterSupplier(account_name: string): any[] {
    const filterValue = account_name.toString().trim().toLowerCase();
    return this.getSupplierList.filter(option => option.account_name.toString().trim().toLowerCase().indexOf(filterValue) === 0);
  }

  public displaySupplierFn(account_name?: string): string {
    if (account_name) {
      const supplier = this.getSupplierList.find(f => f.account_name.toString().trim() === account_name.toString().trim());
      return supplier && supplier.account_name.toString().trim() ? supplier.account_name.toString().trim() : '';
    }
  }

  private _filterLoadSupplier(account_name: string): any[] {
    const filterValue = account_name.toString().trim().toLowerCase();
    return this.getSupplierList.filter(option => option.account_name.toString().trim().toLowerCase().indexOf(filterValue) === 0);
  }

  public displayLoadSupplierFn(account_name?: string): string {
    debugger;
    if (account_name) {
      const supplier = this.getSupplierList.find(f => f.account_name.toString().trim() === account_name.toString().trim());
      return supplier && supplier.account_name.toString().trim() ? supplier.account_name.toString().trim() : '';
    }
  }

  public onEnterSupplierDetails(): void {
    debugger;
    if (!this.objAction.isEditing) {
      let supplierList: any = {
        account_code: '',
        account_name: '',
        gstn_no: '',
        address1: '',
        address2: '',
        address3: '',
        email: '',
        agent_address1: '',
        agent_address2: '',
        agent_address3: '',
        agent_email: '',
        phones: '',
        pincode: '',
      }
      supplierList = this.getSupplierList.find(list => list.account_name.toLowerCase().trim() ===
        this.objPurchaseOrder.account_name.toString().trim().toLowerCase());
      this.objPurchaseOrder.account_code = supplierList && supplierList.account_code ? supplierList.account_code : '';
      this.objPurchaseOrder.gstn_no = supplierList && supplierList.gstn_no ? supplierList.gstn_no : '';
      let Supplieraddress1: string = supplierList && supplierList.address1 ? supplierList.address1 : '';
      let Supplieraddress2: string = supplierList && supplierList.address2 ? supplierList.address2 : '';
      let Supplieraddress3: string = supplierList && supplierList.address3 ? supplierList.address3 : '';
      this.objPurchaseOrder.address1 = supplierList && supplierList.account_code !== '' ?
        Supplieraddress1 + '\n' + Supplieraddress2 + '\n' + Supplieraddress3 : '';
      this.objPurchaseOrder.email = supplierList && supplierList.email ? supplierList.email : '';
      this.objPurchaseOrder.agent_address1 = supplierList && supplierList.agent_address1 ? supplierList.agent_address1 : '';
      this.objPurchaseOrder.agent_address2 = supplierList && supplierList.agent_address2 ? supplierList.agent_address2 : '';
      this.objPurchaseOrder.agent_address3 = supplierList && supplierList.agent_address3 ? supplierList.agent_address3 : '';
      this.objPurchaseOrder.agent_email = supplierList && supplierList.agent_email ? supplierList.agent_email : '';
      this.objPurchaseOrder.phones = supplierList && supplierList.phones ? supplierList.phones : '';
      this.objPurchaseOrder.pincode = supplierList && supplierList.pincode ? supplierList.pincode : '';
    }
  }

  /////////////////// Get Mode Of Dispatch //////////////////////

  private getModeOfDispatchDetails(): void {
    debugger;
    let obj = {
      ModeOfDispatch: JSON.stringify([{

      }])
    }
    this._purchaseOrdersService.getModeOfDispatchList(obj).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0) {
        debugger;
        this.modeOfDispatchList = JSON.parse(result);
      }
    });
  }

  /////////////////// Get UOM Master List //////////////////////

  private getUOMMasterListDetails(uomChange?: boolean): void {
    debugger;
    let obj = {
      section_id: +this._localStorage.getCompanySectionId()
    }
    this._purchaseOrdersService.getUOMMasterListDetails(obj).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0) {
        debugger;
        let tempArr = JSON.parse(result);
        if (this.loggedSection === 'Enterprises') {
          this.getUOMList = tempArr.splice(tempArr.findIndex(x => +x.uom_id === 1), 1);
          if (!uomChange) {
            this.objPurchaseOrder.uom_id = JSON.parse(JSON.stringify(this.getUOMList[0].uom_id));
            this.unChangedPurchaseOrder.uom_id = JSON.parse(JSON.stringify(this.getUOMList[0].uom_id));
          }
          this.checkUOM();
        } else if (this.loggedSection === 'Kids') {
          this.getUOMList = tempArr.splice(tempArr.findIndex(x => +x.uom_id === 5), 1);
          if (!uomChange) {
            this.objPurchaseOrder.uom_id = JSON.parse(JSON.stringify(this.getUOMList[0].uom_id));
            this.unChangedPurchaseOrder.uom_id = JSON.parse(JSON.stringify(this.getUOMList[0].uom_id));
          }
          this.checkUOM();
        } else if (this.loggedSection === 'Visvams') {
          this.getUOMList = tempArr.splice(tempArr.findIndex(x => +x.uom_id === 2), 1);
          this.getUOMList.push(tempArr.splice(tempArr.findIndex(x => +x.uom_id === 1), 1)[0]);
          if (!uomChange) {
            this.objPurchaseOrder.uom_id = JSON.parse(JSON.stringify(this.getUOMList[0].uom_id));
            this.unChangedPurchaseOrder.uom_id = JSON.parse(JSON.stringify(this.getUOMList[0].uom_id));
          }
          this.checkUOM();
        } else if (this.loggedSection === 'Aremkay') {
          this.getUOMList = tempArr.splice(tempArr.findIndex(x => +x.uom_id === 1), 1);
          this.getUOMList.push(tempArr.splice(tempArr.findIndex(x => +x.uom_id === 2), 1)[0]);
          if (!uomChange) {
            this.objPurchaseOrder.uom_id = this.getUOMList[0].uom_id;
            this.unChangedPurchaseOrder.uom_id = this.getUOMList[0].uom_id;
          }
          this.checkUOM();
        } else if (this.loggedSection === 'Mens Garments') {
          this.getUOMList = tempArr.splice(tempArr.findIndex(x => +x.uom_id === 5), 1);
          if (!uomChange) {
            this.objPurchaseOrder.uom_id = this.getUOMList[0].uom_id;
            this.unChangedPurchaseOrder.uom_id = this.getUOMList[0].uom_id;
          }
          this.checkUOM();
        } else if (this.loggedSection === 'Silks') {
          this.getUOMList = tempArr.splice(tempArr.findIndex(x => +x.uom_id === 1), 1);
          if (!uomChange) {
            this.objPurchaseOrder.uom_id = this.getUOMList[0].uom_id;
            this.unChangedPurchaseOrder.uom_id = this.getUOMList[0].uom_id;
          }
          this.checkUOM();
        } else if (this.loggedSection === 'Best Choice') {
          this.getUOMList = tempArr.splice(tempArr.findIndex(x => +x.uom_id === 5), 1);
          if (!uomChange) {
            this.objPurchaseOrder.uom_id = this.getUOMList[0].uom_id;
            this.unChangedPurchaseOrder.uom_id = this.getUOMList[0].uom_id;
          }
          this.checkUOM();
        } else if (this.loggedSection === 'Textiles') {
          this.getUOMList = tempArr;
          if (!uomChange) {
            this.objPurchaseOrder.uom_id = this.getUOMList[0].uom_id;
            this.unChangedPurchaseOrder.uom_id = this.getUOMList[0].uom_id;
          }
          this.checkUOM();
        }
      }
    });
  }

  /////////////////// Get PO No //////////////////////

  private loadPoNoDetails(): void {
    debugger;
    let obj = {
      GetPONo: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._purchaseOrdersService.getPONoDetails(obj).subscribe((result: any) => {
      if (result && result.length > 0) {
        debugger;
        console.log(result, 'po')
        this.poNoList = result;
        this.objPurchaseOrder.po_no = this.poNoList;
        this.unChangedPurchaseOrder.po_no = this.poNoList;
      }
    });
  }

  public onClearQuantityBasedData(element: any): void {
    if (element.qty === 0 || element.qty === '') {
      element.no_of_design = '';
      element.colors = '';
      element.no_of_sets = '';
    }
  }

  private setEditableTrueOrFalse(columnName: string, editable: boolean): void {
    let i = this.columns.findIndex(x => x.display === columnName)
    this.columns[i].editable = editable;
  }

  public getSectionBasedProductName(uomChange?: boolean): void {
    debugger;
    if (+this._localStorage.getCompanySectionId() === 4 || +this._localStorage.getCompanySectionId() === 13 ||
      +this._localStorage.getCompanySectionId() === 22 || +this._localStorage.getCompanySectionId() === 35 ||
      +this._localStorage.getCompanySectionId() === 76 || +this._localStorage.getCompanySectionId() === 89 ||
      +this._localStorage.getCompanySectionId() === 102 || +this._localStorage.getCompanySectionId() === 120) {
      this.loggedSection = 'Enterprises';
      this.getUOMMasterListDetails(uomChange);
      for (let i = 0; i < this.getPurchaseOrderGridList.length; i++) {
        this.getPurchaseOrderGridList[i].product_name = "Sarees";
        this.getPurchaseOrderGridList[i].hsn_code = "5407";
      }
    } else if (+this._localStorage.getCompanySectionId() === 34 || +this._localStorage.getCompanySectionId() === 21 ||
      +this._localStorage.getCompanySectionId() === 75 || +this._localStorage.getCompanySectionId() === 88 ||
      +this._localStorage.getCompanySectionId() === 101 || +this._localStorage.getCompanySectionId() === 115 ||
      +this._localStorage.getCompanySectionId() === 119 || +this._localStorage.getCompanySectionId() === 12 ||
      +this._localStorage.getCompanySectionId() === 3) {
      this.loggedSection = 'Kids';
      this.getUOMMasterListDetails(uomChange);

    } else if (+this._localStorage.getCompanySectionId() === 125 || +this._localStorage.getCompanySectionId() === 27 ||
      +this._localStorage.getCompanySectionId() === 40 || +this._localStorage.getCompanySectionId() === 81 ||
      +this._localStorage.getCompanySectionId() === 107 || +this._localStorage.getCompanySectionId() === 9 ||
      +this._localStorage.getCompanySectionId() === 18 || +this._localStorage.getCompanySectionId() === 94) {
      this.loggedSection = 'Visvams';
      this.getUOMMasterListDetails(uomChange);

    } else if (+this._localStorage.getCompanySectionId() === 19 || +this._localStorage.getCompanySectionId() === 73 ||
      +this._localStorage.getCompanySectionId() === 86 || +this._localStorage.getCompanySectionId() === 99 ||
      +this._localStorage.getCompanySectionId() === 117 || +this._localStorage.getCompanySectionId() === 1 ||
      +this._localStorage.getCompanySectionId() === 32 || +this._localStorage.getCompanySectionId() === 10 ||
      +this._localStorage.getCompanySectionId() === 8) {
      this.loggedSection = 'Aremkay';
      this.getUOMMasterListDetails(uomChange);

    } else if (+this._localStorage.getCompanySectionId() === 150 || +this._localStorage.getCompanySectionId() === 130 ||
      +this._localStorage.getCompanySectionId() === 140 || +this._localStorage.getCompanySectionId() === 145 ||
      +this._localStorage.getCompanySectionId() === 155 || +this._localStorage.getCompanySectionId() === 160 ||
      +this._localStorage.getCompanySectionId() === 165 || +this._localStorage.getCompanySectionId() === 135) {
      this.loggedSection = 'Mens Garments';
      this.getUOMMasterListDetails(uomChange);

    } else if (+this._localStorage.getCompanySectionId() === 15 || +this._localStorage.getCompanySectionId() === 6 ||
      +this._localStorage.getCompanySectionId() === 56 || +this._localStorage.getCompanySectionId() === 60 ||
      +this._localStorage.getCompanySectionId() === 68 || +this._localStorage.getCompanySectionId() === 82 ||
      +this._localStorage.getCompanySectionId() === 126 || +this._localStorage.getCompanySectionId() === 108 ||
      +this._localStorage.getCompanySectionId() === 95) {
      this.loggedSection = 'Silks';
      this.getUOMMasterListDetails(uomChange);

    } else if (+this._localStorage.getCompanySectionId() === 33 || +this._localStorage.getCompanySectionId() === 11 ||
      +this._localStorage.getCompanySectionId() === 20 || +this._localStorage.getCompanySectionId() === 74 ||
      +this._localStorage.getCompanySectionId() === 87 || +this._localStorage.getCompanySectionId() === 100 ||
      +this._localStorage.getCompanySectionId() === 118 || +this._localStorage.getCompanySectionId() === 2) {
      this.loggedSection = 'Best Choice';
      this.getUOMMasterListDetails(uomChange);

    } else if (+this._localStorage.getCompanySectionId() === 39 || +this._localStorage.getCompanySectionId() === 26 ||
      +this._localStorage.getCompanySectionId() === 80 || +this._localStorage.getCompanySectionId() === 124 ||
      +this._localStorage.getCompanySectionId() === 93 || +this._localStorage.getCompanySectionId() === 106 ||
      +this._localStorage.getCompanySectionId() === 8 || +this._localStorage.getCompanySectionId() === 116
      || +this._localStorage.getCompanySectionId() === 17) {
      this.loggedSection = 'Textiles';
      this.getUOMMasterListDetails(uomChange);
    }
  }

  public checkUOM(): void {
    debugger;
    if (+ this.objPurchaseOrder.uom_id === 1 && this.loggedSection === 'Enterprises') {
      this.hideHeaderBasedOnNosSarees();

    } else if (+this.objPurchaseOrder.uom_id === 5 && this.loggedSection === 'Kids') {
      this.hideHeaderBasedOnSizesKids();

    } else if (+this.objPurchaseOrder.uom_id === 2 && this.loggedSection === 'Visvams') {
      this.hideHeaderBasedOnMtsVisvamsandAremkay();

    } else if (+this.objPurchaseOrder.uom_id === 1 && this.loggedSection === 'Visvams') {
      this.hideHeaderBasedOnNosVisvamsandAremkay();

    } else if (+this.objPurchaseOrder.uom_id === 1 && this.loggedSection === 'Aremkay') {
      this.hideHeaderBasedOnNosVisvamsandAremkay();

    } else if (+this.objPurchaseOrder.uom_id === 2 && this.loggedSection === 'Aremkay') {
      this.hideHeaderBasedOnMtsVisvamsandAremkay();

    } else if (+this.objPurchaseOrder.uom_id === 1 && this.loggedSection === 'Textiles') {
      this.hideHeaderBasedOnNosVisvamsandAremkay();

    } else if (+this.objPurchaseOrder.uom_id === 2 && this.loggedSection === 'Textiles') {
      this.hideHeaderBasedOnMtsVisvamsandAremkay();

    } else if (+this.objPurchaseOrder.uom_id === 5 && this.loggedSection === 'Textiles') {
      this.hideHeaderBasedOnSizesKids();

    } else if (+this.objPurchaseOrder.uom_id === 5 && this.loggedSection === 'Mens Garments') {
      this.hideHeaderBasedOnSizesKids();

    } else if (+this.objPurchaseOrder.uom_id === 1 && this.loggedSection === 'Silks') {
      this.hideHeaderBasedOnNosVisvamsandAremkay();

    } else if (+this.objPurchaseOrder.uom_id === 5 && this.loggedSection === 'Best Choice') {
      this.hideHeaderBasedOnSizesKids();
    }
  }

  private hideHeaderBasedOnMtsVisvamsandAremkay(): void {
    this.hideFlag.NoSetsFlag = false;
    this.hideFlag.NoDesignFlag = false;
    this.hideFlag.NoColorsFlag = false;
    // this.hideFlag.PiecesFlag = false;
    this.hideFlag.SleveeFlag = false;
    this.hideFlag.BrandFlag = false;
    this.hideFlag.ColorFlag = false;
    this.hideFlag.QtyFlag = false;

    this.hideFlag.ImageFlag = true;
    this.hideFlag.PreFlag = true;
    this.hideFlag.ProductFlag = true;
    this.hideFlag.HSNFlag = true;
    this.hideFlag.MeterFlag = true;
    this.hideFlag.RateFlag = true;
    this.hideFlag.AmountFlag = true;
    this.checkEditableTrueFalse();
  }

  private hideHeaderBasedOnNosSarees(): void {
    this.hideFlag.SleveeFlag = false;
    this.hideFlag.BrandFlag = false;
    this.hideFlag.ColorFlag = false;
    // this.hideFlag.PiecesFlag = false;
    this.hideFlag.MeterFlag = false;

    this.hideFlag.ProductFlag = true;
    this.hideFlag.HSNFlag = true;
    this.hideFlag.NoDesignFlag = true;
    this.hideFlag.NoSetsFlag = true;
    this.hideFlag.NoColorsFlag = true;
    this.hideFlag.QtyFlag = true;
    this.hideFlag.RateFlag = true;
    this.hideFlag.AmountFlag = true;
    this.hideFlag.ImageFlag = true;
    this.hideFlag.PreFlag = true;
    this.checkEditableTrueFalse();
  }

  public hideHeaderBasedOnSizesKids(): void {
    this.hideFlag.NoSetsFlag = false;
    this.hideFlag.NoDesignFlag = false;
    this.hideFlag.NoColorsFlag = false;
    // this.hideFlag.PiecesFlag = false;
    this.hideFlag.MeterFlag = false;

    this.hideFlag.ImageFlag = true;
    this.hideFlag.PreFlag = true;
    this.hideFlag.SleveeFlag = true;
    this.hideFlag.BrandFlag = true;
    this.hideFlag.ColorFlag = true;
    this.hideFlag.ProductFlag = true;
    this.hideFlag.HSNFlag = true;
    this.hideFlag.QtyFlag = true;
    this.hideFlag.RateFlag = true;
    this.hideFlag.AmountFlag = true;
    this.checkEditableTrueFalse();
  }

  private hideHeaderBasedOnNosVisvamsandAremkay(): void {
    this.hideFlag.SleveeFlag = false;
    this.hideFlag.BrandFlag = false;
    this.hideFlag.ColorFlag = false;
    // this.hideFlag.PiecesFlag = false;
    this.hideFlag.MeterFlag = false;
    this.hideFlag.NoDesignFlag = false;
    this.hideFlag.NoSetsFlag = false;
    this.hideFlag.NoColorsFlag = false;

    this.hideFlag.ProductFlag = true;
    this.hideFlag.ImageFlag = true;
    this.hideFlag.PreFlag = true;
    this.hideFlag.HSNFlag = true;
    this.hideFlag.QtyFlag = true;
    this.hideFlag.RateFlag = true;
    this.hideFlag.AmountFlag = true;
    this.checkEditableTrueFalse();
  }

  public checkValidQuantity(element: any): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.no_of_design) !== -1 ||
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.no_of_sets) !== -1 ||
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.colors) !== -1 &&
      this.loggedSection === 'Enterprises') {
      return false;
    }
    else return true;
  }

  public checkIsEmptyQuantity(element: any): boolean {
    if (
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.colors) === -1 ||
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.no_of_sets) === -1 ||
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.no_of_design) === -1 ||
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.qty) !== -1 &&
      this.loggedSection === 'Enterprises') {
      return false;
    }
    else return true;
  }

  public checkEditableTrueFalse(): any {
    debugger;
    if (this.loggedSection === 'Enterprises' && +this.objPurchaseOrder.uom_id === 1) {
      for (let i = 0; i < this.getPurchaseOrderGridList.length; i++) {
        if (this.getPurchaseOrderGridList[i].no_of_design !== '' && this.getPurchaseOrderGridList[i].no_of_sets !== ''
          && this.getPurchaseOrderGridList[i].colors !== '' && this.getPurchaseOrderGridList[i].qty !== '') {
          this.setEditableTrueOrFalse('productName', true);
          this.setEditableTrueOrFalse('image', true);
          this.setEditableTrueOrFalse('hsnCode', true);
          this.setEditableTrueOrFalse('design', true);
          this.setEditableTrueOrFalse('sets', true);
          this.setEditableTrueOrFalse('colors', true);
          this.setEditableTrueOrFalse('rate', true);
          this.setEditableTrueOrFalse('action', true);

          this.setEditableTrueOrFalse('brand', false);
          this.setEditableTrueOrFalse('slevee', false);
          this.setEditableTrueOrFalse('color', false);
          this.setEditableTrueOrFalse('mts', false);
          this.setEditableTrueOrFalse('amount', false);
          this.setEditableTrueOrFalse('qty', false);
        } else if (this.getPurchaseOrderGridList[i].no_of_design === '' && this.getPurchaseOrderGridList[i].no_of_sets === ''
          && this.getPurchaseOrderGridList[i].colors === '' && this.getPurchaseOrderGridList[i].qty !== '') {
          this.setEditableTrueOrFalse('productName', true);
          this.setEditableTrueOrFalse('image', true);
          this.setEditableTrueOrFalse('hsnCode', true);
          this.setEditableTrueOrFalse('qty', true);
          this.setEditableTrueOrFalse('rate', true);
          this.setEditableTrueOrFalse('action', true);

          this.setEditableTrueOrFalse('brand', false);
          this.setEditableTrueOrFalse('slevee', false);
          this.setEditableTrueOrFalse('color', false);
          this.setEditableTrueOrFalse('amount', false);
          this.setEditableTrueOrFalse('design', false);
          this.setEditableTrueOrFalse('sets', false);
          this.setEditableTrueOrFalse('colors', false);
        } else {
          this.setEditableTrueOrFalse('productName', true);
          this.setEditableTrueOrFalse('image', true);
          this.setEditableTrueOrFalse('hsnCode', true);
          this.setEditableTrueOrFalse('design', true);
          this.setEditableTrueOrFalse('sets', true);
          this.setEditableTrueOrFalse('colors', true);
          this.setEditableTrueOrFalse('qty', true);
          this.setEditableTrueOrFalse('rate', true);
          this.setEditableTrueOrFalse('amount', true);

          this.setEditableTrueOrFalse('brand', false);
          this.setEditableTrueOrFalse('slevee', false);
          this.setEditableTrueOrFalse('color', false);
          this.setEditableTrueOrFalse('mts', false);
        }
      }
    } if (this.loggedSection === 'Visvams' && +this.objPurchaseOrder.uom_id === 1) {
      this.setEditableTrueOrFalse('productName', true);
      this.setEditableTrueOrFalse('image', true);
      this.setEditableTrueOrFalse('hsnCode', true);
      this.setEditableTrueOrFalse('qty', true);
      this.setEditableTrueOrFalse('rate', true);
      this.setEditableTrueOrFalse('action', true);

      this.setEditableTrueOrFalse('brand', false);
      this.setEditableTrueOrFalse('slevee', false);
      this.setEditableTrueOrFalse('color', false);
      this.setEditableTrueOrFalse('mts', false);
      this.setEditableTrueOrFalse('design', false);
      this.setEditableTrueOrFalse('sets', false);
      this.setEditableTrueOrFalse('colors', false);
      this.setEditableTrueOrFalse('amount', false);
      this.setEditableTrueOrFalse('mts', false);

    } if (this.loggedSection === 'Visvams' && +this.objPurchaseOrder.uom_id === 2) {
      this.setEditableTrueOrFalse('productName', true);
      this.setEditableTrueOrFalse('image', true);
      this.setEditableTrueOrFalse('hsnCode', true);
      this.setEditableTrueOrFalse('mts', true);
      this.setEditableTrueOrFalse('rate', true);
      this.setEditableTrueOrFalse('action', true);

      this.setEditableTrueOrFalse('brand', false);
      this.setEditableTrueOrFalse('slevee', false);
      this.setEditableTrueOrFalse('color', false);
      this.setEditableTrueOrFalse('design', false);
      this.setEditableTrueOrFalse('sets', false);
      this.setEditableTrueOrFalse('colors', false);
      this.setEditableTrueOrFalse('amount', false);
      this.setEditableTrueOrFalse('qty', false);
    } if (this.loggedSection === 'Aremkay' && +this.objPurchaseOrder.uom_id === 1) {
      this.setEditableTrueOrFalse('productName', true);
      this.setEditableTrueOrFalse('image', true);
      this.setEditableTrueOrFalse('hsnCode', true);
      this.setEditableTrueOrFalse('qty', true);
      this.setEditableTrueOrFalse('rate', true);
      this.setEditableTrueOrFalse('action', true);

      this.setEditableTrueOrFalse('brand', false);
      this.setEditableTrueOrFalse('slevee', false);
      this.setEditableTrueOrFalse('color', false);
      this.setEditableTrueOrFalse('mts', false);
      this.setEditableTrueOrFalse('design', false);
      this.setEditableTrueOrFalse('sets', false);
      this.setEditableTrueOrFalse('colors', false);
      this.setEditableTrueOrFalse('amount', false);
      this.setEditableTrueOrFalse('mts', false);

    } if (this.loggedSection === 'Aremkay' && +this.objPurchaseOrder.uom_id === 2) {
      this.setEditableTrueOrFalse('productName', true);
      this.setEditableTrueOrFalse('image', true);
      this.setEditableTrueOrFalse('hsnCode', true);
      this.setEditableTrueOrFalse('mts', true);
      this.setEditableTrueOrFalse('rate', true);
      this.setEditableTrueOrFalse('action', true);

      this.setEditableTrueOrFalse('brand', false);
      this.setEditableTrueOrFalse('slevee', false);
      this.setEditableTrueOrFalse('color', false);
      this.setEditableTrueOrFalse('design', false);
      this.setEditableTrueOrFalse('sets', false);
      this.setEditableTrueOrFalse('colors', false);
      this.setEditableTrueOrFalse('amount', false);
      this.setEditableTrueOrFalse('qty', false);

    } if (this.loggedSection === 'Silks' && +this.objPurchaseOrder.uom_id === 1) {
      this.setEditableTrueOrFalse('productName', true);
      this.setEditableTrueOrFalse('image', true);
      this.setEditableTrueOrFalse('hsnCode', true);
      this.setEditableTrueOrFalse('qty', true);
      this.setEditableTrueOrFalse('rate', true);
      this.setEditableTrueOrFalse('action', true);

      this.setEditableTrueOrFalse('brand', false);
      this.setEditableTrueOrFalse('slevee', false);
      this.setEditableTrueOrFalse('color', false);
      this.setEditableTrueOrFalse('design', false);
      this.setEditableTrueOrFalse('sets', false);
      this.setEditableTrueOrFalse('colors', false);
      this.setEditableTrueOrFalse('amount', false);
      this.setEditableTrueOrFalse('mts', false);

    } if (this.loggedSection === 'Kids' && +this.objPurchaseOrder.uom_id === 5) {
      this.setEditableTrueOrFalse('productName', true);
      this.setEditableTrueOrFalse('image', true);
      this.setEditableTrueOrFalse('hsnCode', true);
      this.setEditableTrueOrFalse('brand', true);
      this.setEditableTrueOrFalse('slevee', true);
      this.setEditableTrueOrFalse('color', true);
      this.setEditableTrueOrFalse('qty', true);
      this.setEditableTrueOrFalse('rate', true);
      this.setEditableTrueOrFalse('action', true);

      this.setEditableTrueOrFalse('design', false);
      this.setEditableTrueOrFalse('sets', false);
      this.setEditableTrueOrFalse('colors', false);
      this.setEditableTrueOrFalse('amount', false);
      this.setEditableTrueOrFalse('mts', false);

    } if (this.loggedSection === 'Textiles' && +this.objPurchaseOrder.uom_id === 1) {
      this.setEditableTrueOrFalse('productName', true);
      this.setEditableTrueOrFalse('image', true);
      this.setEditableTrueOrFalse('hsnCode', true);
      this.setEditableTrueOrFalse('qty', true);
      this.setEditableTrueOrFalse('rate', true);
      this.setEditableTrueOrFalse('action', true);

      this.setEditableTrueOrFalse('brand', false);
      this.setEditableTrueOrFalse('slevee', false);
      this.setEditableTrueOrFalse('color', false);
      this.setEditableTrueOrFalse('mts', false);
      this.setEditableTrueOrFalse('design', false);
      this.setEditableTrueOrFalse('sets', false);
      this.setEditableTrueOrFalse('colors', false);
      this.setEditableTrueOrFalse('amount', false);
      this.setEditableTrueOrFalse('mts', false);

    } if (this.loggedSection === 'Textiles' && +this.objPurchaseOrder.uom_id === 2) {
      this.setEditableTrueOrFalse('productName', true);
      this.setEditableTrueOrFalse('image', true);
      this.setEditableTrueOrFalse('hsnCode', true);
      this.setEditableTrueOrFalse('mts', true);
      this.setEditableTrueOrFalse('rate', true);
      this.setEditableTrueOrFalse('action', true);

      this.setEditableTrueOrFalse('brand', false);
      this.setEditableTrueOrFalse('slevee', false);
      this.setEditableTrueOrFalse('color', false);
      this.setEditableTrueOrFalse('design', false);
      this.setEditableTrueOrFalse('sets', false);
      this.setEditableTrueOrFalse('colors', false);
      this.setEditableTrueOrFalse('amount', false);
      this.setEditableTrueOrFalse('qty', false);

    } if (this.loggedSection === 'Textiles' && +this.objPurchaseOrder.uom_id === 3) {
      this.setEditableTrueOrFalse('productName', true);
      this.setEditableTrueOrFalse('image', true);
      this.setEditableTrueOrFalse('hsnCode', true);
      this.setEditableTrueOrFalse('brand', true);
      this.setEditableTrueOrFalse('slevee', true);
      this.setEditableTrueOrFalse('color', true);
      this.setEditableTrueOrFalse('qty', true);
      this.setEditableTrueOrFalse('rate', true);
      this.setEditableTrueOrFalse('action', true);

      this.setEditableTrueOrFalse('design', false);
      this.setEditableTrueOrFalse('sets', false);
      this.setEditableTrueOrFalse('colors', false);
      this.setEditableTrueOrFalse('amount', false);
      this.setEditableTrueOrFalse('mts', false);

    } if (this.loggedSection === 'Mens Garments' && +this.objPurchaseOrder.uom_id === 5) {
      this.setEditableTrueOrFalse('productName', true);
      this.setEditableTrueOrFalse('image', true);
      this.setEditableTrueOrFalse('hsnCode', true);
      this.setEditableTrueOrFalse('brand', true);
      this.setEditableTrueOrFalse('slevee', true);
      this.setEditableTrueOrFalse('color', true);
      this.setEditableTrueOrFalse('qty', true);
      this.setEditableTrueOrFalse('rate', true);
      this.setEditableTrueOrFalse('action', true);

      this.setEditableTrueOrFalse('design', false);
      this.setEditableTrueOrFalse('sets', false);
      this.setEditableTrueOrFalse('colors', false);
      this.setEditableTrueOrFalse('amount', false);
      this.setEditableTrueOrFalse('mts', false);

    } if (this.loggedSection === 'Best Choice' && +this.objPurchaseOrder.uom_id === 5) {
      this.setEditableTrueOrFalse('productName', true);
      this.setEditableTrueOrFalse('image', true);
      this.setEditableTrueOrFalse('hsnCode', true);
      this.setEditableTrueOrFalse('brand', true);
      this.setEditableTrueOrFalse('slevee', true);
      this.setEditableTrueOrFalse('color', true);
      this.setEditableTrueOrFalse('qty', true);
      this.setEditableTrueOrFalse('rate', true);
      this.setEditableTrueOrFalse('action', true);

      this.setEditableTrueOrFalse('design', false);
      this.setEditableTrueOrFalse('sets', false);
      this.setEditableTrueOrFalse('colors', false);
      this.setEditableTrueOrFalse('amount', false);
      this.setEditableTrueOrFalse('mts', false);

    }
  }

  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, i: number): boolean {
    debugger;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][i][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    }
  }

  public onlyAllowNumbers(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    // let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    }
  }

  public deletePurchaseGrid(index) {
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this._dialogRef.componentInstance.confirmMessage =
      "Do you want to remove the product name?"
    this._dialogRef.componentInstance.componentName = "Purchase Order";
    return this._dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getPurchaseOrderGridList.splice(index, 1);
      }
      this._dialogRef = null;
    });
  }

  private beforeLoadValidate(): boolean {
    if (this.objLoadPurchase.from_date.toString() === '') {
      document.getElementById("startDate").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter from date", "Purchase Order");
      return false;
    } else if (this.objLoadPurchase.from_date.toString().length !== 10) {
      document.getElementById("startDate").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid from date", "Purchase Order");
      this.objLoadPurchase.from_date = '';
      return false;
    } else if (this.objLoadPurchase.to_date.toString() === '') {
      document.getElementById("endDate").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter to date", "Purchase Order");
      return false;
    } else if (this.objLoadPurchase.to_date.toString().length !== 10) {
      document.getElementById("endDate").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid to date", "Purchase Order");
      this.objLoadPurchase.to_date = '';
      return false;
    }
    else return true;
  }

  public matConfig(tableRecords: any[]): void {
    if (tableRecords) {
      this.dataSource = new MatTableDataSource(tableRecords);
    } else this.dataSource = new MatTableDataSource([]);
  }

  public getTotalQty(element: any): void {
    let designTotal: number = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.no_of_design) === -1 ? element.no_of_design : 0;
    let setTotal: number = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.no_of_sets) === -1 ? +element.no_of_sets : 0;
    let colorsTotal: number = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.colors) === -1 ? +element.colors : 0
    if (+ designTotal !== 0 && +setTotal !== 0 && +colorsTotal !== 0)
      element.qty = +designTotal * +setTotal * +colorsTotal;
    else if (+designTotal !== 0 && +setTotal !== 0 && +colorsTotal === 0)
      element.qty = +designTotal * +setTotal;
    else if (+designTotal !== 0 && +setTotal === 0 && +colorsTotal === 0)
      element.qty = +designTotal
    else if (+designTotal === 0 && +setTotal !== 0 && +colorsTotal !== 0)
      element.qty = +setTotal * +colorsTotal;
    else if (+designTotal === 0 && +setTotal === 0 && +colorsTotal !== 0)
      element.qty = +colorsTotal;
    else if (+designTotal === 0 && +setTotal !== 0 && +colorsTotal === 0)
      element.qty = +setTotal;
    else if (+designTotal !== 0 && +setTotal === 0 && +colorsTotal !== 0)
      element.qty = +designTotal * +colorsTotal;
    else if (+designTotal === 0 && +setTotal === 0 && +colorsTotal === 0)
      element.qty = 0;
    this.getTotalRateAmount(element);
  }

  public getTotalRateAmount(element: any): void {
    debugger;
    let qty: number = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.qty) === -1 ?
      element.qty : 0;
    let rateTotal: number = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.rate) === -1 ?
      element.rate : 0;
    element.amount = +qty * +rateTotal;
  }

  public checkbeforeSaveEmptyGrid(): boolean {
    for (let i = 0; i < this.getPurchaseOrderGridList.length; i++) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.getPurchaseOrderGridList[i].product_name) === -1
        || [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.getPurchaseOrderGridList[i].rate) === -1
        || [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.getPurchaseOrderGridList[i].amount) === -1) {
        return true
      } else return false;
    }
  }

  public setTotalCalculation(tableRecords: any): void {
    debugger;
    this.getTotal = {
      quantity_total: 0,
      amount_total: 0,
    }
    for (let i = 0; i < tableRecords.length; i++) {
      this.getTotal.quantity_total += +(tableRecords[i].quantity);
      this.getTotal.amount_total += +(tableRecords[i].amount)
    }
  }


  public uploadMultipleFiles(event: any, i: number): void {
    debugger;
    let fileToUpload = event.target.files[0] as any;
    this.mulFormData = new FormData();
    this.mulFormData.append('file', fileToUpload, fileToUpload.name);
    // this.mulFormData.append('fileRootPath', this._localStorage.getPurchasesDocumentPath());
    this.mulFormData.append('fileRootPath', '\\\\192.9.202.39\\\\Rmkv_Nxt_Doc\\\\Invoices');
    this._purchaseOrdersService.UploadTempFile(this.mulFormData).subscribe(
      (result: any) => {
        this.getPurchaseOrderGridList[i].temp_path = result.document_Path;
        debugger;
        if (event.target.length != 0) {
          this.fileIndex.push(i);
          this.selectedGridFiles[i] = event.target.files[0];
          this.mulDocument = event.target.files;
        }
        else {
          this.fileInput[i].nativeElement.files = this.mulDocument;
          this.selectedGridFiles[i] = this.mulDocument[0];
        }
        this.getPurchaseOrderGridList[i].image_path = "";
      });
  }

  public beforeSaveValidate(): boolean {
    if (this.objPurchaseOrder.city.toString().trim() === '') {
      document.getElementById("city").focus();
      this._confirmationDialogComponent.openAlertDialog("Select supplier city", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.supplier_group_name.toString().trim() === '') {
      document.getElementById("group").focus();
      this._confirmationDialogComponent.openAlertDialog("Select supplier group", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.account_name.toString().trim() === '') {
      document.getElementById("name").focus();
      this._confirmationDialogComponent.openAlertDialog("Select supplier name", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.address1.toString().trim() === '') {
      document.getElementById("textarea").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter supplier address", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.gstn_no.toString().trim() === '') {
      document.getElementById("gstnNo").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter supplier gstn no", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.email.toString().trim() === '') {
      document.getElementById("supplierMail").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter supplier mail id", "Purchase Order");
      return false;
    } else if (!this.objPurchaseOrder.email.toString().includes('@')) {
      document.getElementById('supplierMail').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid supplier mail id", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.email.toString().includes('@') && !this.objPurchaseOrder.email.toString().includes('.')) {
      document.getElementById('supplierMail').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid supplier mail id", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.agent_name.toString().trim() === '') {
      document.getElementById("agentName").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter agent name", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.agent_email.toString().trim() === '') {
      document.getElementById("agentMail").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter agent mail id", "Purchase Order");
      return false;
    } else if (!this.objPurchaseOrder.agent_email.toString().includes('@')) {
      document.getElementById('agentMail').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid agent mail id", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.agent_email.toString().includes('@') && !this.objPurchaseOrder.agent_email.toString().includes('.')) {
      document.getElementById('agentMail').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid agent mail id", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.phones.toString().trim() === '') {
      document.getElementById("supplierMbl").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter mobile no", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.phones.length.toString().trim() < 10 || this.objPurchaseOrder.phones.length.toString().trim() > 12) {
      document.getElementById("supplierMbl").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid mobile no", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.sales_location_id === 0) {
      document.getElementById("location").focus();
      this._confirmationDialogComponent.openAlertDialog("Select despatch no", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.return_type === 0) {
      document.getElementById("returnType").focus();
      this._confirmationDialogComponent.openAlertDialog("Select return type", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.uom_id === 0) {
      document.getElementById("nos").focus();
      this._confirmationDialogComponent.openAlertDialog("Select Nos", "Purchase Order");
      return false;
    } else if (this.getPurchaseOrderGridList.length === 0) {
      this._confirmationDialogComponent.openAlertDialog("No record to save", "Purchase Order");
      return false;
    }else if (this.objPurchaseOrder.delivery_schedule.toString() === '') {
      document.getElementById("toDate").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter delivery schedule", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.delivery_schedule.toString().length !== 10) {
      document.getElementById("toDate").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid delivery schedule", "Purchase Order");
      this.objPurchaseOrder.delivery_schedule = '';
      return false;
    } else if (this.objPurchaseOrder.reference_no.toString().trim() === '') {
      document.getElementById("refNo").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter reference no", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.id === 0) {
      document.getElementById("dispatch").focus();
      this._confirmationDialogComponent.openAlertDialog("Select mode of dispatch", "Purchase Order");
      return false;
    } else if (this.objPurchaseOrder.raised_by.toString().trim() === '') {
      document.getElementById("raised").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter raised by", "Purchase Order");
      return false;
    }
    else return true;
  }

  public checkGridListEmptyRow(index: number): boolean {
    if (this.getPurchaseOrderGridList.length === 0 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.getPurchaseOrderGridList[index].product_name.toString().trim()) === -1) {
      let input = this.productName.toArray();
      input[index].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter product details", "Purchase Order");
      return true;
    } else if (this.getPurchaseOrderGridList.length === 0 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.getPurchaseOrderGridList[index].product.toString().trim()) === -1) {
      let input = this.hsnCode.toArray();
      input[index].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter product details", "Purchase Order");
      return true;
    } else if (this.getPurchaseOrderGridList.length === 0 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.getPurchaseOrderGridList[index].qty.toString().trim()) === -1) {
      let input = this.qty.toArray();
      input[index].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter product details", "Purchase Order");
      return true;
    } else if (this.getPurchaseOrderGridList.length === 0 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.getPurchaseOrderGridList[index].rate.toString().trim()) === -1) {
      let input = this.rate.toArray();
      input[index].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter product details", "Purchase Order");
      return true;
    } else return false;
  }

  public onSavePurchaseOrders(): void {
    debugger;
    let details: any = [];
    let formData = new FormData();
    if (this.beforeSaveValidate()) {
      for (let i = 0; i < this.getPurchaseOrderGridList.length; i++) {
        if (this.checkGridListEmptyRow(i)) {
          return;
        } else if (this.getPurchaseOrderGridList.length !== 1 && this.getPurchaseOrderGridList[i].product_name.toString().trim() === '') {
          this.getPurchaseOrderGridList.splice(i, 0);
        } else {
          details.push({
            serial_no: i + 1,
            item_description: this.getPurchaseOrderGridList[i].product_name,
            image_path: '',
            hsncode: this.getPurchaseOrderGridList[i].hsn_code,
            brand: this.getPurchaseOrderGridList[i].brand,
            sleeve: this.getPurchaseOrderGridList[i].sleeve,
            color: this.getPurchaseOrderGridList[i].color,
            no_of_design: +this.getPurchaseOrderGridList[i].no_of_design,
            no_of_sets: +this.getPurchaseOrderGridList[i].no_of_sets,
            no_of_colors: +this.getPurchaseOrderGridList[i].colors,
            quantity: +this.getPurchaseOrderGridList[i].qty,
            pcs: 1,
            rate: +this.getPurchaseOrderGridList[i].rate,
            amount: +this.getPurchaseOrderGridList[i].amount,
            sizedetails: this.getPurchaseOrderGridList[i].getSizeTableList ? this.getSizeDetails(i) : null
          });
        }
      }
      let obj = {
        po_id: this.objAction.isEditing ? +this.objPurchaseOrder.po_id : 0,
        po_no: this.objPurchaseOrder.po_no,
        company_section_id: +this._localStorage.getCompanySectionId(),
        po_date: this.objAction.isEditing ? this._datePipe.transform(this.objPurchaseOrder.date, 'dd/MM/yyyy') : this.objPurchaseOrder.date,
        supplier_code: this.objPurchaseOrder.account_code,
        supplier_name: this.objPurchaseOrder.account_name,
        supplier_group: this.objPurchaseOrder.supplier_group_name,
        city: this.objPurchaseOrder.city,
        address: this.objPurchaseOrder.address1,
        gstin_no: this.objPurchaseOrder.gstn_no.toString().trim(),
        email_id: this.objPurchaseOrder.email.toString().trim(),
        phone_no: this.objPurchaseOrder.phones.toString().trim(),
        reference_no: this.objPurchaseOrder.reference_no.toString().trim(),
        despatch_name: this.sourceLocationList.filter(x => +x.sales_location_id == +this.objPurchaseOrder.sales_location_id)[0].sales_location_name,
        despatch_location_id: this.objPurchaseOrder.sales_location_id,
        delivery_schedule: this.objPurchaseOrder.delivery_schedule,
        mode_of_despatch: this.objPurchaseOrder.id,
        payment_terms: this.objPurchaseOrder.payment_terms,
        raised_by: this.objPurchaseOrder.raised_by,
        uom: this.getUOMList.filter(x => +x.uom_id == +this.objPurchaseOrder.uom_id)[0].uom_description,
        remarks: this.objPurchaseOrder.remarks,
        agent_name: this.objPurchaseOrder.agent_name,
        agent_email_id: this.objPurchaseOrder.agent_email,
        agent_email_status: this.objPurchaseOrder.agent_email !== null ? true : false,
        bank_name: this.objPurchaseOrder.bank_name,
        ifsc_code: this.objPurchaseOrder.ifsc_code,
        supplier_account_no: this.objPurchaseOrder.supplier_account_no,
        branch_name: this.objPurchaseOrder.branch_name,
        new_supplier: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].
          indexOf(this.objPurchaseOrder.branch_name) !== -1 ? 1 : 0,
        supplier_status: false,
        return_type: this.objPurchaseOrder.return_type,
        file_path: [{ file_path: this.objPurchaseOrder.file_path }],
        bank_file_path: [{ bank_file_path: this.objPurchaseOrder.bank_file_path }],
        entered_by: +this._localStorage.intGlobalUserId(),
        details: details,
        OldDocumentPathForPO: this.OldDocumentPathForPO,
        OldDocumentPathForBank: this.OldDocumentPathForBank,
        isBankFile: this.objPurchaseOrder.isBankFile,
        isCommonFile: this.objPurchaseOrder.isCommonFile
      }
      if (this.selectedGridFiles !== null) {
        for (let i = 0; i < this.selectedGridFiles.length; i++) {
          formData.append('files', this.selectedGridFiles[i]);
        }
      }
      if (this.selectedBankDocument) {
        formData.append('files', this.selectedBankDocument);
      }
      if (this.selectedFiles) {
        formData.append('files', this.selectedFiles);
      }
      formData.append('fileIndex', JSON.stringify(this.fileIndex));
      formData.append('SavePurchaseOrder', JSON.stringify(obj));
      //formData.append('fileRootPath', this._localStorage.getPurchasesDocumentPath());
      formData.append('fileRootPath', '\\\\192.9.202.39\\\\Rmkv_Nxt_Doc\\\\Invoices');
      this._purchaseOrdersService.addPurchaseOrdersDetails(formData).subscribe(
        (result: any) => {
          if (result) {
            this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New purchase order has been created", "Purchase Order");
            this.componentVisibility = !this.componentVisibility;
            this.loadPurchaseOrderListDetails();
          }
          this.selectedFiles = null;
          this.selectedGridFiles = [];
          this.document = null;
        });
    }
  }

  private getSizeDetails(i: number): any[] {
    let sizedetails: any[] = [];
    // if(this.loggedSection ==='Kids' || this.loggedSection ==='Best Choice' || this.loggedSection ==='Mens Garments'
    //  && this.getPurchaseOrderGridList[i].getSizeTableList !== null){
    for (let j = 0; j < this.getPurchaseOrderGridList[i].getSizeTableList.length; j++) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.getPurchaseOrderGridList[i].getSizeTableList[j].qty) === -1) {
        sizedetails.push({
          po_serial_no: i + 1,
          size_id: +this.getPurchaseOrderGridList[i].getSizeTableList[j].size_id,
          sizes: this.getPurchaseOrderGridList[i].getSizeTableList[j].sizes,
          quantity: this.getPurchaseOrderGridList[i].getSizeTableList[j].qty
        });
      }
    } return sizedetails;
  }
  // }

  public fetchPurchaseOrderDetails(index: number): void {
    debugger;
    this.objAction.isEditing = true;
    this.objAction.isView = false;
    this.componentVisibility = !this.componentVisibility;
    let objLoad = {
      FetchPurchaseOrder: JSON.stringify([{
        po_id: this.dataSource.data[index].po_id,
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._purchaseOrdersService.fetchPurchaseOrderDetails(objLoad).subscribe((result: any) => {
      debugger;
      if (JSON.parse(result) !== null) {
        console.log(JSON.parse(result), 'fetch');
        this.objAction.isEditing = true;
        this.getSupplierGroupList();
        this.onEnterSupplierGroup();
        this.getSupplierByGroupIdList();
        this.addNewPurchaseGridForNOS();
        this.getPurchaseOrderGridList = JSON.parse(result);
        this.modifyPurchaseOrderGridList = JSON.parse(result);
        this.objPurchaseOrder = JSON.parse(result)[0];
        this.modifyPurchaseOrder = JSON.parse(result)[0];
      }

    });
  }

  public viewPurchaseOrderDetails(index: number): void {
    debugger;
    this.componentVisibility = !this.componentVisibility;
    let objLoad = {
      FetchPurchaseOrder: JSON.stringify([{
        po_id: this.dataSource.data[index].po_id,
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._purchaseOrdersService.fetchPurchaseOrderDetails(objLoad).subscribe((result: any) => {
      debugger;
      if (JSON.parse(result) !== null) {
        this.objAction.isView = true;
        this.getSupplierGroupList();
        this.onEnterSupplierGroup();
        this.getSupplierByGroupIdList();
        this.addNewPurchaseGridForNOS();
        this.getPurchaseOrderGridList = JSON.parse(result);
        this.objPurchaseOrder = JSON.parse(result)[0];
        this.modifyPurchaseOrder = JSON.parse(result)[0];
      }
    });
  }

  public onClearView(): void {
    this.objAction.isView = true;
    this.componentVisibility = !this.componentVisibility;
    this.isView = false;
    this.loadPurchaseOrderListDetails();
  }

  public onClickDelete(index: any): any {
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this._dialogRef.componentInstance.confirmMessage =
      "Do you want to cancel the purchase order?"
    this._dialogRef.componentInstance.componentName = "Purchase Order";
    return this._dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.openReasonDialog(index);
      }
      this._dialogRef = null;
    });
  }

  public openReasonDialog(index: number): void {
    debugger;
    let dialogRef = this._matDialog.open(ReasonComponent, {
      width: "75vw",
      panelClass: "custom-dialog-container",
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== null) {
        this.deletePurchaseOrder(index, result);
      }
      this._dialogRef = null;
    });
  }

  private deletePurchaseOrder(row: any, Reason: string): void {
    debugger;
    let objData = [{
      po_id: this.dataSource.data[row].po_id,
      company_section_id: +this._localStorage.getCompanySectionId(),
      cancelled_by: +this._localStorage.intGlobalUserId(),
      cancelled_reason: Reason,
    }];
    let objSave = {
      CancelPurchaseOrder: JSON.stringify(objData)
    }
    debugger;
    this._purchaseOrdersService.deletePurchaseOrdersDetails(objSave).subscribe(
      (result: any) => {
        if (result !== null) {
          this._confirmationDialogComponent.openAlertDialog("Added to cancelled list", "Purchase Order");
          this.loadPurchaseOrderListDetails()
        }
      });
    this._dialogRef = null;
  }

  public openAttachFilesPopup(): void {
    let dialogRef = this._matDialog.open(SingleFileAttachComponent, {
      panelClass: "custom-dialog-container",
      width: '53vw',
      data: {
        objAttach: JSON.stringify(this.objAttachView),
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      debugger;
      if (result) {
        this.fileIndex = [];
        this.selectedFiles = result.selectedFiles;
        this.objAttachView = JSON.parse(JSON.stringify(result.objAttachView));
        if (this.objPurchaseOrder.file_path !== this.objAttachView.file_path) {
          this.objPurchaseOrder.file_path = '';
        }
        if (this.selectedFiles !== null)
          this.objPurchaseOrder.isCommonFile = true;
        else this.objPurchaseOrder.isCommonFile = false;
      }
    });
  }

  public openNewBankDialog(): void {
    let dialogRef = this._dialog.open(SupplierBankDetailsComponent, {
      width: "500px",
      data: {
        isEditing: this.objAction.isEditing,
        obj: this.objPurchaseOrder
      },
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        debugger;
        this.objPurchaseOrder.bank_name = result.bank_name;
        this.objPurchaseOrder.branch_name = result.branch_name;
        this.objPurchaseOrder.supplier_account_no = result.supplier_account_no;
        this.objPurchaseOrder.ifsc_code = result.ifsc_code;
        this.objPurchaseOrder.bank_file_path = result.bank_file_path;
        this.selectedBankDocument = result.selectedFiles;
        if (this.selectedBankDocument !== null) {
          this.objPurchaseOrder.isBankFile = true;
          this.objPurchaseOrder.bank_file_path = '';
        } else
          this.objPurchaseOrder.isBankFile = false;
      }
    });
  }

  public openSizeTableDialog(index: number, element: any): void {
    let dialogRef = this._dialog.open(PurchaseOrderSizeComponent, {
      width: "500px",
      data: { index: index, element, isEditing: this.objAction.isEditing },
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      this.inputEls = this.rate.toArray();
      this.inputEls[index].nativeElement.focus();
      if (result) {
        debugger;
        element.qty = result.Total;
        this.getTotalRateAmount(element);
        element.getSizeTableList = result.sizeList;
        for (var count in element.getSizeTableList) {
          element.getSizeTableList[count].size_name = result.size_name;
        }
      }
    });
  }

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'PO No': this.dataSource.data[i].po_id,
          'PO Date': this._datePipe.transform(this.dataSource.data[i].po_date, 'dd/MM/yyyy'),
          "Name": this.dataSource.data[i].supplier_name,
          "Group": this.dataSource.data[i].supplier_group_name,
          "City": this.dataSource.data[i].city,
          "Raised By": this.dataSource.data[i].raised_by,
          "Dispatch To": this.dataSource.data[i].despatch_name,
          "Discount%": this.dataSource.data[i].discount,
          "Quality": this.dataSource.data[i].quality,
          "Amount": this.dataSource.data[i].amount
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Purchase Order",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Purchase Order");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.po_id);
        tempObj.push(this._datePipe.transform(e.po_date, 'dd/MM/yyyy'));
        tempObj.push(e.supplier_name);
        tempObj.push(e.supplier_group_name);
        tempObj.push(e.city);
        tempObj.push(e.raised_by);
        tempObj.push(e.despatch_name);
        tempObj.push(e.discount),
          tempObj.push(e.quality),
          tempObj.push(e.amount)
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['PO No', 'PO Date', 'Name', 'Group', 'City', 'Raised By', 'Dispatch To', 'Discount%', 'Quality', 'Amount']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Purchase Order' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Purchase Order");
  }

  public newClick(): void {
    debugger;
    this.componentVisibility = !this.componentVisibility;
    this.isView = false;
    this.objAction.isView = false;
    this.objAction.isEditing = false;
    this.resetScreen();
    this.loadPoNoDetails();
    this.getSectionBasedProductName();
  }

  private trimStringFields(): void {
    this.objPurchaseOrder.city = this.objPurchaseOrder.city.toString().trim();
    this.modifyPurchaseOrder.city = this.modifyPurchaseOrder.city.toString().trim();
    this.unChangedPurchaseOrder.city = this.unChangedPurchaseOrder.city.toString().trim();

    this.objPurchaseOrder.supplier_group_name = this.objPurchaseOrder.supplier_group_name.toString().trim();
    this.modifyPurchaseOrder.supplier_group_name = this.modifyPurchaseOrder.supplier_group_name.toString().trim();
    this.unChangedPurchaseOrder.supplier_group_name = this.unChangedPurchaseOrder.supplier_group_name.toString().trim();

    this.objPurchaseOrder.account_name = this.objPurchaseOrder.account_name.toString().trim();
    this.modifyPurchaseOrder.account_name = this.modifyPurchaseOrder.account_name.toString().trim();
    this.unChangedPurchaseOrder.account_name = this.unChangedPurchaseOrder.account_name.toString().trim();

    this.objPurchaseOrder.address1 = this.objPurchaseOrder.address1.toString().trim();
    this.modifyPurchaseOrder.address1 = this.modifyPurchaseOrder.address1.toString().trim();
    this.unChangedPurchaseOrder.address1 = this.unChangedPurchaseOrder.address1.toString().trim();

    this.objPurchaseOrder.gstn_no = this.objPurchaseOrder.gstn_no.toString().trim();
    this.modifyPurchaseOrder.gstn_no = this.modifyPurchaseOrder.gstn_no.toString().trim();
    this.unChangedPurchaseOrder.gstn_no = this.unChangedPurchaseOrder.gstn_no.toString().trim();

    this.objPurchaseOrder.email = this.objPurchaseOrder.email.toString().trim();
    this.modifyPurchaseOrder.email = this.modifyPurchaseOrder.email.toString().trim();
    this.unChangedPurchaseOrder.email = this.unChangedPurchaseOrder.email.toString().trim();

    this.objPurchaseOrder.agent_name = this.objPurchaseOrder.agent_name.toString().trim();
    this.modifyPurchaseOrder.agent_name = this.modifyPurchaseOrder.agent_name.toString().trim();
    this.unChangedPurchaseOrder.agent_name = this.unChangedPurchaseOrder.agent_name.toString().trim();

    this.objPurchaseOrder.agent_email = this.objPurchaseOrder.agent_email.toString().trim();
    this.modifyPurchaseOrder.agent_email = this.modifyPurchaseOrder.agent_email.toString().trim();
    this.unChangedPurchaseOrder.agent_email = this.unChangedPurchaseOrder.agent_email.toString().trim();

    this.objPurchaseOrder.phones = this.objPurchaseOrder.phones.toString().trim();
    this.modifyPurchaseOrder.phones = this.modifyPurchaseOrder.phones.toString().trim();
    this.unChangedPurchaseOrder.phones = this.unChangedPurchaseOrder.phones.toString().trim();

    this.objPurchaseOrder.reference_no = this.objPurchaseOrder.reference_no.toString().trim();
    this.modifyPurchaseOrder.reference_no = this.modifyPurchaseOrder.reference_no.toString().trim();
    this.unChangedPurchaseOrder.reference_no = this.unChangedPurchaseOrder.reference_no.toString().trim();

    this.objPurchaseOrder.raised_by = this.objPurchaseOrder.raised_by.toString().trim();
    this.modifyPurchaseOrder.raised_by = this.modifyPurchaseOrder.raised_by.toString().trim();
    this.unChangedPurchaseOrder.raised_by = this.unChangedPurchaseOrder.raised_by.toString().trim();

    this.objPurchaseOrder.payment_terms = this.objPurchaseOrder.payment_terms.toString().trim();
    this.modifyPurchaseOrder.payment_terms = this.modifyPurchaseOrder.payment_terms.toString().trim();
    this.unChangedPurchaseOrder.payment_terms = this.unChangedPurchaseOrder.payment_terms.toString().trim();

    this.objPurchaseOrder.remarks = this.objPurchaseOrder.remarks.toString().trim();
    this.modifyPurchaseOrder.remarks = this.modifyPurchaseOrder.remarks.toString().trim();
    this.unChangedPurchaseOrder.remarks = this.unChangedPurchaseOrder.remarks.toString().trim();

    this.getPurchaseOrderGridList.forEach(x => {
      x.product_name = x.product_name.toString().trim();
      x.hsn_code = x.hsn_code.toString().trim();
      x.hsn_code = x.brand.toString().trim();
      x.hsn_code = x.sleeve.toString().trim();
      x.hsn_code = x.color.toString().trim();
    });
    this.modifyPurchaseOrderGridList.forEach(x => {
      x.product_name = x.product_name.toString().trim();
      x.hsn_code = x.hsn_code.toString().trim();
      x.hsn_code = x.brand.toString().trim();
      x.hsn_code = x.sleeve.toString().trim();
      x.hsn_code = x.color.toString().trim();
    })
    this.unChangedPurchaseOrderGridList.forEach(x => {
      x.product_name = x.product_name.toString().trim();
      x.hsn_code = x.hsn_code.toString().trim();
      x.hsn_code = x.brand.toString().trim();
      x.hsn_code = x.sleeve.toString().trim();
      x.hsn_code = x.color.toString().trim();
    })
  }

  public onClear(exitFlag: boolean): void {
    this.trimStringFields();
    debugger;
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objPurchaseOrder) !== (!this.objAction.isEditing ? JSON.stringify(this.unChangedPurchaseOrder) :
      JSON.stringify(this.modifyPurchaseOrder)) || JSON.stringify(this.getPurchaseOrderGridList) !==
      (!this.objAction.isEditing ? JSON.stringify(this.unChangedPurchaseOrderGridList) : JSON.stringify(this.modifyPurchaseOrderGridList))) {
      return true;
    } else {
      return false;
    }
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Purchase Order";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  private resetScreen(): void {
    debugger;
    if (this.objAction.isEditing) {
      this.objPurchaseOrder = JSON.parse(JSON.stringify(this.modifyPurchaseOrder));
      this.modifyPurchaseOrder = JSON.parse(JSON.stringify(this.modifyPurchaseOrder));
      this.getPurchaseOrderGridList = JSON.parse(JSON.stringify(this.modifyPurchaseOrderGridList));
      this.modifyPurchaseOrderGridList = JSON.parse(JSON.stringify(this.modifyPurchaseOrderGridList));
    } else {
      this.objPurchaseOrder = JSON.parse(JSON.stringify(this.unChangedPurchaseOrder));
      this.getPurchaseOrderGridList = JSON.parse(JSON.stringify(this.unChangedPurchaseOrderGridList));
      this.document = undefined;
      this.selectedDocument = null;
      this.selectedGridFiles = null;
    }
  }

  public setTwoNumberDecimal($event: KeyboardEvent | any): void {
    debugger
    if ($event.target.value === "") {
      $event.target.value = "0.00";
    } else {
      $event.target.value = parseFloat($event.target.value).toFixed(2);
    }
  }

  public loadPurchaseOrderListDetails(): void {
    if (this.beforeLoadValidate()) {
      let obj = {
        GetPurchaseOrder: JSON.stringify([{
          from_date: this.objLoadPurchase.from_date,
          to_date: this.objLoadPurchase.to_date,
          company_section_id: +this._localStorage.getCompanySectionId(),
          supplier_name: this.objLoadPurchase.account_name,
          supplier_city: this.objLoadPurchase.city,
          raised_by: this.objLoadPurchase.raised_by,
          despatch_to: this.objLoadPurchase.sales_location_id !== '' ? this.objLoadPurchase.sales_location_id : 0
        }])
      }
      this._purchaseOrdersService.getPurchaseOrderList(obj).subscribe((result: any) => {
        if (JSON.parse(result) !== null) {
          this.getPurchaseList = JSON.parse(result);
          this.matConfig(JSON.parse(result));
          this.setTotalCalculation(JSON.parse(result));
          console.log(this.getPurchaseList, 'list')
          this.loadPurchaseOrderEmailDetails(false);
        } else this._confirmationDialogComponent.openAlertDialog("No record found", "Purchase Order")
      })
    }
  }
  
  public loadPurchaseOrderEmailDetails(isEmail : boolean): void {
    debugger;
    let obj = {
      GetPOEmail: JSON.stringify([{
        po_id: this.dataSource.data[0].po_id,
        company_section_id: +this.dataSource.data[0].company_section_id,
        email_id: this.dataSource.data[0].email_id,
        bcc: 'teamleader@rmkv.com',
        cc: this.dataSource.data[0].agent_email_id,
      }])
    }
    this._purchaseOrdersService.getMailPurchaseOrderDetails(obj).subscribe((result: any) => {
      if (JSON.parse(result) !== null) {
        console.log(JSON.parse(result) ,'emailid')
        if(isEmail === true){
          this.isView = true;
          this.componentVisibility = false;
        }
        this.addNewPurchaseGridForNOS();
        this.getPurchaseOrderGridList = JSON.parse(result);
        this.objEmailPurchaseOrder = JSON.parse(result)[0];
      }
    })
  }

  public openMailDialog(): void {
    debugger;
    const emailPO = Object.assign(this.objEmailPurchaseOrder);
    let dialogRef = this._dialog.open(PurchaseOrderMailComponent, {
      width: "450px",
      data: {
        emailPO
      },
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this._confirmationDialogComponent.openAlertDialog('Mail has been sent', 'Purchase Order');
      } else if (result === false)
        this._confirmationDialogComponent.openAlertDialog('Failure sending mail', 'Purchase Order');
    });
  }
  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('printView').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>RmKV Purchase</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
  public capturePdf() {
    debugger;
    let element = 'printView';
    if (element !== '') {
      var data = document.getElementById(element);
      html2canvas(document.getElementById(element), {
        scale: 4
      }).then(canvas => {
        let pdf = new jspdf('p', 'mm', 'a4');
        var width = pdf.internal.pageSize.getWidth();
        var height = pdf.internal.pageSize.getHeight();
        let imgFile = canvas;
        //  pdf.addPage();
        pdf.addImage(imgFile, "JPEG", 5, 5, 200, 150);
        pdf.save('test' + ".pdf");
      });
    }
  }
}

export class GroupList {
  supplier_group_id: number;
  supplier_group_name: string;
}

export class SupplierList {
  account_code: string;
  account_name: string;
  gstn_no: string;
  address1: string;
  address2: string
  address3: string;
  email: string;
  agent_address1: string;
  agent_address2: string;
  agent_address3: string;
  agent_email: string;
  phones: string;
  pincode: string;
}


export interface PeriodicElement {
  Serial_No: number;
  PO_No: string;
  Invoice_No: string;
  Invoice_Date: string;
  Supplier_Name: string;
  Invoice_Amount: string;
  Actual_Amount: string;
  Action: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { Serial_No: 1, PO_No: 'RS/2021/S01033', Invoice_No: '4687', Invoice_Date: '20/11/2020', Supplier_Name: '', Invoice_Amount: '54308.00', Actual_Amount: '54308.00', Action: 'Action', }
];