import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseOrderSizeComponent } from './purchase-order-size.component';

describe('PurchaseOrderSizeComponent', () => {
  let component: PurchaseOrderSizeComponent;
  let fixture: ComponentFixture<PurchaseOrderSizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseOrderSizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseOrderSizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
