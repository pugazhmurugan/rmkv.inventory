import { DecimalPipe } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { PurchaseOrdersService } from '../../../services/purchase-orders/purchase-orders/purchase-orders.service';

@Component({
  selector: 'app-purchase-order-size',
  templateUrl: './purchase-order-size.component.html',
  styleUrls: ['./purchase-order-size.component.scss'],
  providers: [DecimalPipe]
})
export class PurchaseOrderSizeComponent implements OnInit {
  getSizePurchaseList: any[] = [];
  getPOSizeBasedSizesList: any[] = [];
  index: number;
  isEditing: boolean = false;

  objSize: any = {
    size_name: ''
  }

  getTotal: any = {
    qty: 0
  }

  columns: any[] = [
    { display: "size", editable: true },
    { display: "qty", editable: true },
  ]

  @ViewChildren("size") size: ElementRef | any;
  @ViewChildren("qty") qty: ElementRef | any;

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
       this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.getPOSizeBasedSizesList);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.getPOSizeBasedSizesList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.getPOSizeBasedSizesList);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.getPOSizeBasedSizesList);
        break;
    }
  }

  constructor(
    public _router: Router,
    public _dialogRef: MatDialogRef<PurchaseOrderSizeComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _localStorage: LocalStorage,
    public _purchaseOrdersService: PurchaseOrdersService,
    public _keyPressEvents: KeyPressEvents,
    public _gridKeyEvents: GridKeyEvents,
  ) {
    this._dialogRef.disableClose = true;
    this.isEditing = JSON.parse(JSON.stringify(_data.isEditing));
  }

  ngOnInit() {
    this.getPurchaseOrderSizeName();
  }

  public getPurchaseOrderSizeName(): void {
    debugger;
    let obj = {
      GetPoSizeName: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._purchaseOrdersService.getPOSizeNameDetails(obj).subscribe((result: any) => {
      if (JSON.parse(result) !== null) {
        this.getSizePurchaseList = JSON.parse(result);
        this.objSize.size_name = this.getSizePurchaseList[0].size_name;
        this.getPOSizeNameBasedSizes();
      } else this._confirmationDialogComponent.openAlertDialog("No record found", "Purchase Order Size")
    })
  }

  public getPOSizeNameBasedSizes(): void {
    debugger;
    let obj = {
      GetPoSizeNameBasedSizes: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        size_name: this.objSize.size_name
      }])
    }
    this._purchaseOrdersService.getPOSizeNameBasedSizesDetails(obj).subscribe((result: any) => {
      if (JSON.parse(result) !== null) {
        this.getPOSizeBasedSizesList = JSON.parse(result);
        console.log(this.getPOSizeBasedSizesList, 'size')
        if (this.isEditing) {
          for (let i = 0; i < this.getPOSizeBasedSizesList.length; i++) {
            for (let k = 0; k < this._data.element.length; k++) {
              if (this._data.element[k].size_id === this.getPOSizeBasedSizesList[i].size_id) {
                this.getPOSizeBasedSizesList[i].qty = this._data.element[k].qty;
              }
            }
          }
        }
      }
    })
  }

  public setTotalCalculation() {
    var total = 0;
    for (var i in this.getPOSizeBasedSizesList) {
      if (this.getPOSizeBasedSizesList[i].qty) {
        total += (this.getPOSizeBasedSizesList[i].qty * 1);
      }
    }
    if (!isNaN(total) && (total != undefined))
      return total;
    else { return 0 }
  }

  public checkPoSizeName(): boolean {
    if (this.objSize.size_name !== "Set") {
      return true
    } else return false;
  }

  public addSizeDetails() {
    debugger;
    const poSizes = {
      sizeList: this.getPOSizeBasedSizesList,
      Total: this.setTotalCalculation(),
      po_no: this.index + 1,
      size_name: this.objSize.size_name
    }
    this._dialogRef.close(poSizes);
  }

  public onClear(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }



}
