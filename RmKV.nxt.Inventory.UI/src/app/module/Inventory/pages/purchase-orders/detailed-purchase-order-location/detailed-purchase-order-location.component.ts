import { Component, ElementRef, Inject, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { DetailedPurchaseOrderService } from '../../../services/purchase-orders/detailed-purchase-order/detailed-purchase-order.service';
import { PurchaseOrdersService } from '../../../services/purchase-orders/purchase-orders/purchase-orders.service';

@Component({
  selector: 'app-detailed-purchase-order-location',
  templateUrl: './detailed-purchase-order-location.component.html',
  styleUrls: ['./detailed-purchase-order-location.component.scss']
})
export class DetailedPurchaseOrderLocationComponent implements OnInit {

  getPurchaseLocationList: any[] = [];
  index: number;
  isEditing: boolean = false;
  isEditingLocation: boolean;
  getTotal: any = {
    qty: 0
  }

  columns: any[] = [
    { display: "location", editable: true },
    { display: "qty", editable: true },
  ]

  @ViewChildren("location") location: ElementRef | any;
  @ViewChildren("qty") qty: ElementRef | any;

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.getPurchaseLocationList);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.getPurchaseLocationList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.getPurchaseLocationList);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.getPurchaseLocationList);
        break;
    }
  }

  constructor(
    public _router: Router,
    public _dialogRef: MatDialogRef<DetailedPurchaseOrderLocationComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _localStorage: LocalStorage,
    public _purchaseOrdersService: PurchaseOrdersService,
    public _detailedPurchaseOrderService: DetailedPurchaseOrderService,
    public _keyPressEvents: KeyPressEvents,
    public _gridKeyEvents: GridKeyEvents,
  ) {
    debugger;
    this._dialogRef.disableClose = true;
    this.isEditing = JSON.parse(JSON.stringify(_data.isEditing));
    this.getPurchaseLocationList = JSON.parse(JSON.stringify(_data.objData));
    // this.isEditingLocation = JSON.parse(JSON.stringify(_data.isEditingLocation));
  }

  ngOnInit() {
    this.getPOLocations();
  }

  public getPOLocations(): void {
    debugger;
    let obj = {
      GetLocationsForDPO: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._detailedPurchaseOrderService.getLocationsDetailedPODetails(obj).subscribe((result: any) => {
      if (JSON.parse(result) !== null) {
        this.getPurchaseLocationList = JSON.parse(result);
        console.log(this.getPurchaseLocationList, 'loc')
        if (this.isEditing ) {
          for (let i = 0; i < this.getPurchaseLocationList.length; i++) {
            for (let k = 0; k < this._data.objData.length; k++) {
              if (this._data.objData[k].sales_location_id === this.getPurchaseLocationList[i].sales_location_id) {
                this.getPurchaseLocationList[i].qty = this._data.objData[k].quantity;
              }
            }
          }
        } 
      }
    })
  }

  public setTotalCalculation() {
    var total = 0;
    for (var i in this.getPurchaseLocationList) {
      if (this.getPurchaseLocationList[i].qty) {
        total += (this.getPurchaseLocationList[i].qty * 1);
      }
    }
    if (!isNaN(total) && (total != undefined))
      return total;
    else { return 0 }
  }

  public addLocationsDetails() {
    debugger;
    const poLocations = {
      locationList: this.getPurchaseLocationList,
      Total: this.setTotalCalculation(),
      po_no: this.index + 1,
    }
    this._dialogRef.close(poLocations);
  }

  public onClear(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }
}
