import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedPurchaseOrderLocationComponent } from './detailed-purchase-order-location.component';

describe('DetailedPurchaseOrderLocationComponent', () => {
  let component: DetailedPurchaseOrderLocationComponent;
  let fixture: ComponentFixture<DetailedPurchaseOrderLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedPurchaseOrderLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedPurchaseOrderLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
