import { DatePipe } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { EditMode, Rights } from 'src/app/common/models/common-model';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { SingleFileAttachComponent } from 'src/app/common/shared/single-file-attach/single-file-attach.component';
import { PurchaseOrdersService } from '../../../services/purchase-orders/purchase-orders/purchase-orders.service';

@Component({
  selector: 'app-supplier-bank-details',
  templateUrl: './supplier-bank-details.component.html',
  styleUrls: ['./supplier-bank-details.component.scss'],
  providers : [DatePipe]
})
export class SupplierBankDetailsComponent implements OnInit {
  
  objSupplierBank: any = {
    bank_name: "",
    ifsc_code: "",
    branch_name: "",
    supplier_account_no : '',
    file_path: "",
  }

  unChangedSupplierBank: any = {
    bank_name: "",
    ifsc_code: "",
    branch_name: "",
    supplier_account_no : '',
    file_path: "",
  }

  @ViewChild('file', null) fileInput: ElementRef;
  fileIndex: number[] = [];
  isCancelled: boolean = false;
  removedFileIndex: number[] = [];
  removedFiles: File | any = {};
  selectedFiles: File | any = {};
  oldDocument: any;
  isAttach: boolean = false;
  selectedDocument: File | any = {};
  document: any = null;
  tempFile: any = null;
  OldDocumentPath: any = null;
  formData: any;
  componentVisibility: boolean = true;
  isEditing: boolean;

  objAttachView = {
    file_path: '',
  }

  unChangedAttachView = {
    file_path: '',
  }

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  rights: Rights = {
    Add: false,
    Update: false,
    Delete: false
  };

  constructor(
    public _router: Router,
    public _dialogRef: MatDialogRef<SupplierBankDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _purchaseOrdersService: PurchaseOrdersService,
    public _localStorage : LocalStorage,
    public _keyPressEvents : KeyPressEvents
  ) { 
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.isEditing = this._data.isEditing;
    this.objSupplierBank = JSON.parse(JSON.stringify(this._data.obj));
  }


  public addSupplierDetails(): boolean {
    debugger;
    if (this.objSupplierBank.bank_name.toString().trim() === '') {
      document.getElementById('bank').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter bank name", "Supplier Bank Details");
      return false;
    }else if(this.objSupplierBank.supplier_account_no.toString().trim() === ''){
      document.getElementById("accountNo").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter account no","Supplier Bank Details");
      return false;
    }else if (this.objSupplierBank.ifsc_code.toString().trim() === '') {
      document.getElementById('ifsc').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter ifsc code", "Supplier Bank Details");
      return false;
    } else if (this.objSupplierBank.branch_name.toString().trim() === '') {
      document.getElementById('branch').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter branch name", "Supplier Bank Details");
      return false;
    } else return true
  }


  public addBanKDetails() : void {
    if(this.addSupplierDetails())
    {
      let objData : any = {
       selectedFiles: this.selectedFiles,
       bank_file_path : this.objSupplierBank.bank_file_path,
       bank_name : this.objSupplierBank.bank_name,
       supplier_account_no : this.objSupplierBank.supplier_account_no,
       ifsc_code : this.objSupplierBank.ifsc_code,
       branch_name : this.objSupplierBank.branch_name
      };
       this._dialogRef.close(objData);
     }
  }


  public openAttachFilesPopup(): void {
    let objAttachView = {
      file_path: this.objSupplierBank.file_path
    }
    let dialogRef = this._matDialog.open(SingleFileAttachComponent, {
      panelClass: "custom-dialog-container",
      width: '59vw',
      data: {
        objAttach: JSON.stringify(objAttachView),
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      debugger;
      if (result) {
        this.fileIndex = [];
        this.selectedFiles = result.selectedFiles;
        this.objAttachView = JSON.parse(JSON.stringify(result.objAttachView));
        if (this.objSupplierBank.file_path !== this.objAttachView.file_path)
          this.objSupplierBank.file_path = '';
        debugger;
      }
    });
  }

  public resetScreen() : void {
     this.objSupplierBank = JSON.parse(JSON.stringify(this.unChangedSupplierBank));
     this.document = null;
     this.selectedDocument = null;
     this.tempFile = null;
  }

  public onClear(): void {
    // this._router.navigate(["/Purchases/PurchaseOrder"]);
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }
}
