import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { Rights } from 'src/app/common/models/common-model';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { PurchaseOrdersService } from '../../../services/purchase-orders/purchase-orders/purchase-orders.service';
declare var jsPDF: any;

@Component({
  selector: 'app-po-analysis',
  templateUrl: './po-analysis.component.html',
  styleUrls: ['./po-analysis.component.scss'],
  providers: [DatePipe]
})
export class PoAnalysisComponent implements OnInit {

  objLoad = {
    From_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    To_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
  }

  rights: Rights = {
    Add: true,
    Update: true,
    Delete: true,
    Approve: true,
    View: true
  };

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  model1: any = new Date();
  model2: any = new Date();

  public dateValidation: DateValidation = new DateValidation();

  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "PO_No", "Date", "Place", "Supplier_Name", "PO_Qty", "Received_Qty"];
  poList: any;

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    private _datePipe: DatePipe,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _excelService: ExcelService,
    public _purchaseOrdersService: PurchaseOrdersService,
    public _commonService: CommonService
  ) {
    this.model1.setDate(this.model2.getDate() - 2);
    this.objLoad.From_Date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
  }

  ngOnInit() {
    this.getRights();
  }

  /****************************************** Validations **********************************************/

  private getRights(): void {
    var rights = this._localStorage.getMenuRights("/POAnalysis");
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "Delete" && rights[x].Status == true) {
        this.rights.Delete = true;
      }
      if (rights[x].Right_Name == "Approve" && rights[x].Status == true) {
        this.rights.Approve = true;
      }
      if (rights[x].Right_Name == "View" && rights[x].Status == true) {
        this.rights.View = true;
      }
    }
  }

  public validateFromDate() {
    let date = this.dateValidation.validateDate(this.objLoad.From_Date, this.model1, this.minDate, this.maxDate);
    this.objLoad.From_Date = date[0];
    this.model1 = date[1];
  }

  public validateToDate() {
    let date = this.dateValidation.validateDate(this.objLoad.To_Date, this.model2, this._datePipe.transform(this.model1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoad.To_Date = date[0];
    this.model2 = date[1];
  }

  private beforeloadValidate(): boolean {
    if (!this.objLoad.From_Date.toString().trim()) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter from date", "PO Analysis");
      return false;
    } else if (this.objLoad.From_Date.length != 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid from date", "PO Analysis");
      return false;
    } else if (!this.objLoad.To_Date.toString().trim()) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter to date", "PO Analysis");
      return false;
    } else if (this.objLoad.To_Date.length != 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid to date", "PO Analysis");
      return false;
    } else if (this.model2 < this.model1) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("To date can't be less than from date", "PO Analysis");
      return false;
    } else
      return true;
  }

  /****************************************** CRUD Operations **********************************************/

  public getPOAnalysis(): void {
    if (this.beforeloadValidate()) {
      this.matTableConfig([]);
      let objData = {
        POAnalysis: JSON.stringify([{
          fromdate: this.objLoad.From_Date,
          todate: this.objLoad.To_Date,
          company_section_id: +this._localStorage.getCompanySectionId(),
        }]),
      }
      this._purchaseOrdersService.getPOAnalysis(objData).subscribe((result: any) => {
        if (result) {
          this.poList = result;
          console.log(this.poList, 'polist');
          this.matTableConfig(result);
        } else
          this._confirmationDialogComponent.openAlertDialog('No records found', 'PO Analysis');
      });
    }
  }

  public matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }

  public supplierNameFilter(searchValue: string, event: KeyboardEvent | any): void {
    searchValue = searchValue.toString().trim() ? searchValue.toString().toLocaleLowerCase() : "";
    let filteredPoList = [];
    if (event.keyCode === 8 || event.keyCode === 46) {
      filteredPoList = this.poList.filter((item: any) =>
        item.supplier_name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    } else {
      filteredPoList = this.dataSource.data.filter((item: any) =>
        item.supplier_name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    }
    this.matTableConfig(filteredPoList);
  }

  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'PO No': this.dataSource.data[i].po_no,
          'Date': this.dataSource.data[i].po_date,
          "Place": this.dataSource.data[i].city,
          'Supplier Name': this.dataSource.data[i].supplier_name,
          "PO Qty": this.dataSource.data[i].po_qty,
          "Received Qty": this.dataSource.data[i].po_received_qty,
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "PO Analysis",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "PO Analysis");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.po_no);
        tempObj.push(this._datePipe.transform(e.po_date, 'dd/MM/yyyy'));
        tempObj.push(e.city);
        tempObj.push(e.supplier_name);
        tempObj.push(e.po_qty);
        tempObj.push(e.po_received_qty);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['PO No', 'Date', 'Place', 'Supplier Name', 'PO Qty', 'Received Qty']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('PO Analysis' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "PO Analysis");
  }

}
