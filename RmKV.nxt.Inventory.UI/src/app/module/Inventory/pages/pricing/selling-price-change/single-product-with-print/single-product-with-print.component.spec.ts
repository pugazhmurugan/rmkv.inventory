import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleProductWithPrintComponent } from './single-product-with-print.component';

describe('SingleProductWithPrintComponent', () => {
  let component: SingleProductWithPrintComponent;
  let fixture: ComponentFixture<SingleProductWithPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleProductWithPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleProductWithPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
