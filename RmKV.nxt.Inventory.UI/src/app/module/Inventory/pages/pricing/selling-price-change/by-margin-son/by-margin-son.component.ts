import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-by-margin-son',
  templateUrl: './by-margin-son.component.html',
  styleUrls: ['./by-margin-son.component.scss']
})
export class ByMarginSonComponent implements OnInit {

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
