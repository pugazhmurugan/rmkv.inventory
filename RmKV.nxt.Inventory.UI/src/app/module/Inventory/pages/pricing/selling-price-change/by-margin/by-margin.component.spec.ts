import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByMarginComponent } from './by-margin.component';

describe('ByMarginComponent', () => {
  let component: ByMarginComponent;
  let fixture: ComponentFixture<ByMarginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByMarginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByMarginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
