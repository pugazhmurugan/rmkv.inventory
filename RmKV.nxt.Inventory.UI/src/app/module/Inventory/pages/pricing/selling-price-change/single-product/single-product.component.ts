import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ForCheckBynoData } from 'src/app/module/Inventory/model/common.model';
import { SingleProductService } from 'src/app/module/Inventory/services/pricing/selling-price-change/single-product/single-product.service';
declare var jsPDF: any;

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.scss'],
  providers: [DatePipe]
})
export class SingleProductComponent implements OnInit {
  objSingleProductLoad: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    company_section_name: this._localStorage.getCompanySectionName()
  }

  objUnchangedSingleProductLoad: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    company_section_name: this._localStorage.getCompanySectionName()
  }

  ForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  UnChangedForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  Inv_Byno: string = "";
  Byno_Serial: string = "";
  Byno_Prod_Serial: string = "";
  keyUpEvent: any;
  singleProductList: any[] = [];

  columns: any[] = [
    { display: 'newPrice', editable: true },
  ];

  @ViewChildren('byno') byNo: ElementRef | any;
  @ViewChildren('newPrice') newPrice: ElementRef | any;

  constructor(
    public _router: Router,
    public _localStorage: LocalStorage,
    private _datePipe: DatePipe,
    private _excelService: ExcelService,
    private _gridKeyEvents: GridKeyEvents,
    private _keyPressEvents: KeyPressEvents,
    private _singleProductService: SingleProductService,
    private _confirmationDialog: ConfirmationDialogComponent,
  ) { }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.singleProductList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.singleProductList);
        break;
    }
  }

  ngOnInit() {
    setTimeout(() => {
      let input = this.byNo.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100);

  }

  /*************************************************** CRUD Operations ***************************************************/

  public getSingleProductDetails(): void {
    if (this.beforeLoadValidate()) {
      let objGet: any = {
        SingleProduct: JSON.stringify([{
          inv_byno: this.ForCheckBynoData.Inv_Byno.toString().trim().toUpperCase(),
          byno_serial: this.ForCheckBynoData.Byno_Serial.toString().trim().toUpperCase(),
          byno_prod_serial: this.ForCheckBynoData.Byno_Prod_Serial.toString().trim().toUpperCase(),
          company_section_id: this._localStorage.getCompanySectionId()
        }])
      }
      this._singleProductService.getSingleProduct(objGet).subscribe((result: any) => {
        if (result) {
          // console.log(result, 'Single Product Details List');
          this.singleProductList = JSON.parse(JSON.stringify(result));
          this.afterLoadSingleProductCosts();
        } else {
          this.objSingleProductLoad.byno = '';
          this._confirmationDialog.openAlertDialog('No records found', 'Single Product');
        }
      });
    }
  }

  private afterLoadSingleProductCosts(): void {
    this.singleProductList.forEach(x => {
      x.new_selling_price = x.selling_price;
    });
    setTimeout(() => {
      let input = this.newPrice.toArray();
      input[0].nativeElement.focus();
    }, 100);
    // this.objSingleProductLoad.byno = '';
  }

  public addSingleProduct(): void {
    if (this.beforeSaveValidate()) {
      debugger;
      let tempArr: any = [];
      for (let i = 0; i < this.singleProductList.length; i++) {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.singleProductList[i].new_selling_price) === -1 &&
          +this.singleProductList[i].new_selling_price !== +this.singleProductList[i].selling_price) {
          tempArr.push({
            inv_byno: this.singleProductList[i].inv_byno,
            byno_serial: this.singleProductList[i].byno_serial,
            byno_prod_serial: this.singleProductList[i].byno_prod_serial,
            old_selling_price: +this.singleProductList[i].selling_price,
            new_selling_price: +this.singleProductList[i].new_selling_price,
            selling_price: +this.singleProductList[i].selling_price,
            entered_by: this._localStorage.intGlobalUserId(),
            company_section_id: this._localStorage.getCompanySectionId()
          });
        }
      }
      let objAdd: any = {
        SingleProduct: JSON.stringify(tempArr)
      }
      debugger;
      this._singleProductService.addSingleProduct(objAdd).subscribe((result: boolean) => {
        if (result) {
          this.resetScreen();
          this._confirmationDialog.openAlertDialog('Saved successfully', 'Single Product');
        }
      })
    }
  }

  /**************************************************** Validations ****************************************************/

  private beforeLoadValidate(): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objSingleProductLoad.byno) !== -1) {
      document.getElementById('invByNo').focus();
      this._confirmationDialog.openAlertDialog('Enter byno', 'Single Product');
      return false;
    } else return true;
  }

  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  private beforeSaveValidate(): boolean {
    debugger;
    if (this.singleProductList.length === 0) {
      this._confirmationDialog.openAlertDialog('No records to save, Load the data first', 'Single Product');
      return false;
    } if (!this.checkAtleastPriceChanges()) {
      this._confirmationDialog.openAlertDialog('No changes to save', 'Single Product');
      return false;
    } if (!this.checkValidPrice()) {
      this._confirmationDialog.openAlertDialog('No changes to save', 'Single Product');
      return false;
    } else return true;
  }

  private checkAtleastPriceChanges(): boolean {
    let i = this.singleProductList.findIndex(x => +x.new_selling_price !== +x.selling_price);
    return i === -1 ? false : true;
  }

  private checkValidPrice(): boolean {
    for (let i = 0; i < this.singleProductList.length; i++) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.singleProductList[i].new_selling_price) !== -1)
        return false;
    } return true;
  }

  private resetScreen(): void {
    this.singleProductList = [];
    this.ForCheckBynoData = JSON.parse(JSON.stringify(this.UnChangedForCheckBynoData));
    this.objSingleProductLoad = JSON.parse(JSON.stringify(this.objUnchangedSingleProductLoad));
  }

  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.singleProductList.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.singleProductList.length; i++) {
        json[i] = Object.assign({
          "ByNo": this.singleProductList[i].byno,
          "Product Name": this.singleProductList[i].product_name,
          "Counter": this.singleProductList[i].counter_code,
          "Current Price": this.singleProductList[i].selling_price,
          "New Price": this.singleProductList[i].new_selling_price
        });
      }
      this._excelService.exportAsExcelFile(json, "Single_Product", datetime);
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "Single Product");
  }

  public exportToPdf(): void {
    if (this.singleProductList.length != 0) {
      var prepare = [];
      this.singleProductList.forEach(e => {
        var tempObj = [];
        tempObj.push(e.byno);
        tempObj.push(e.product_name);
        tempObj.push(e.counter_code);
        tempObj.push(e.selling_price);
        tempObj.push(e.new_selling_price);
        prepare.push(tempObj);
      });

      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["ByNo", "Product Name", "Counter", "Current Price", "New Price"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('Single_Product' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "Single Product");
  }


  /***************************************************** ByNo Validations *****************************************************/

  public onKeyPress(keyUpEvent: any) {
    debugger;
    this.singleProductList = [];
    if (keyUpEvent.keyCode === 13 && this.objSingleProductLoad.byno === '') {
      document.getElementById("byno").focus();
      this._confirmationDialog.openAlertDialog('Enter byno', 'Single Product');
    } else if (keyUpEvent.keyCode === 13 && this.objSingleProductLoad.byno.length === 8) {
      this.objSingleProductLoad.byno = this.objSingleProductLoad.byno.toString().trim().toUpperCase();
      this.ForCheckBynoData.Inv_Byno = this.objSingleProductLoad.byno.toString().trim().toUpperCase();
      this.ForCheckBynoData.Byno_Serial = "";
      this.ForCheckBynoData.Byno_Prod_Serial = "";
      this.getSingleProductDetails();
    } else if (keyUpEvent.keyCode === 13 && this.objSingleProductLoad.byno.length == 16 && !this.objSingleProductLoad.byno.includes("/")) {
      this.byNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objSingleProductLoad.byno.length == 28 && !this.objSingleProductLoad.byno.includes("/")) {
      this.OldByNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objSingleProductLoad.byno.length == 30 && !this.objSingleProductLoad.byno.includes("/")) {
      this.byNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objSingleProductLoad.byno.includes("/")) {
      debugger
      this.splitByNumber();
    } else if (this.objSingleProductLoad.byno.length >= 28 && keyUpEvent.keyCode === 13) {
      this.autoScannerCall();
    } else {
      if (keyUpEvent.keyCode === 13 && !this.objSingleProductLoad.byno.includes("/") && this.objSingleProductLoad.byno.length > 16) {
        document.getElementById('byno').focus();
        this._confirmationDialog.openAlertDialog("Invalid Byno", "Single Product");
      } else if (keyUpEvent.keyCode === 13 && !this.checkValid()) {
        document.getElementById('byno').focus();
        this._confirmationDialog.openAlertDialog("Invalid Byno", "Single Product");
        this.objSingleProductLoad.byno = '';
      }
    }
  }

  private checkValid(): boolean {
    debugger;
    let index = this.singleProductList.findIndex(x => x.byno.toString().trim().toUpperCase() === this.objSingleProductLoad.inv_byno.toString().trim().toUpperCase());
    if (index !== -1)
      return true;
    else
      return false;
  }

  private byNoWithoutSlash() {
    debugger;
    this.objSingleProductLoad.byno = this.objSingleProductLoad.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objSingleProductLoad.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objSingleProductLoad.byno.substr(9, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objSingleProductLoad.byno.substr(13, 3);
    this.getSingleProductDetails();
    // this.openReceivedByNo(this.ForCheckBynoData);
  }

  private OldByNoWithoutSlash() {
    debugger;
    this.objSingleProductLoad.byno = this.objSingleProductLoad.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objSingleProductLoad.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objSingleProductLoad.byno.substr(8, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objSingleProductLoad.byno.substr(11, 3);
    // console.log(this.ForCheckBynoData);
    this.getSingleProductDetails();
    // this.openReceivedByNo(this.ForCheckBynoData);
  }

  private autoScannerCall() {
    debugger;
    this.ForCheckBynoData.Inv_Byno = this.objSingleProductLoad.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = this.objSingleProductLoad.byno.substr(8, 4);
    this.ForCheckBynoData.Byno_Prod_Serial = this.objSingleProductLoad.byno.substr(11, 4);
    // this.objSingleProductLoad.byno = '';
    this.getSingleProductDetails();
    // this.openReceivedByNo(this.ForCheckBynoData);
  }

  private splitByNumber() {
    debugger;
    this.objSingleProductLoad.byno = this.objSingleProductLoad.byno.toString().trim().toUpperCase();
    var x = this.objSingleProductLoad.byno.split("/");
    this.Inv_Byno = x[0];
    this.Byno_Serial = x[1];
    this.Byno_Prod_Serial = [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(x[2]) !== -1 ? "" : x[2];
    if (this.Byno_Serial.length == 2) {
      this.Byno_Serial = "00".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 2) {
      this.Byno_Prod_Serial = "00".concat(this.Byno_Prod_Serial);
    }
    if (this.Byno_Serial.length == 1) {
      this.Byno_Serial = "000".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 1) {
      this.Byno_Prod_Serial = "000".concat(this.Byno_Prod_Serial);
    } if (this.Byno_Serial.length == 0) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Invalid Byno', 'Single Product');
      return;
    } if (this.Byno_Prod_Serial.length == 0) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Invalid Byno', 'Single Product');
      return;
    }
    this.ForCheckBynoData.Inv_Byno = this.Inv_Byno.toString().trim();
    this.ForCheckBynoData.Byno_Serial = this.Byno_Serial.toString().trim();
    this.ForCheckBynoData.Byno_Prod_Serial = this.Byno_Prod_Serial.toString().trim();
    this.getSingleProductDetails();
    // this.openReceivedByNo(this.ForCheckBynoData);
  }
}
