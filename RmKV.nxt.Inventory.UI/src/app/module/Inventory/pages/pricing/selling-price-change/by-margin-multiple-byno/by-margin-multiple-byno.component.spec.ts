import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByMarginMultipleBynoComponent } from './by-margin-multiple-byno.component';

describe('ByMarginMultipleBynoComponent', () => {
  let component: ByMarginMultipleBynoComponent;
  let fixture: ComponentFixture<ByMarginMultipleBynoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByMarginMultipleBynoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByMarginMultipleBynoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
