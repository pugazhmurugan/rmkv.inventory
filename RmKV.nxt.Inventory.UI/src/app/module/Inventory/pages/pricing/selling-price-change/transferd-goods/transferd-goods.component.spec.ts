import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferdGoodsComponent } from './transferd-goods.component';

describe('TransferdGoodsComponent', () => {
  let component: TransferdGoodsComponent;
  let fixture: ComponentFixture<TransferdGoodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferdGoodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferdGoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
