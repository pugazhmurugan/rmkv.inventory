import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ForCheckBynoData } from 'src/app/module/Inventory/model/common.model';
import { ByAmountService } from 'src/app/module/Inventory/services/pricing/selling-price-change/single-product/double-rate/by-amount/by-amount.service';
import { SingleProductService } from 'src/app/module/Inventory/services/pricing/selling-price-change/single-product/single-product.service';

@Component({
  selector: 'app-by-margin-multiple-byno',
  templateUrl: './by-margin-multiple-byno.component.html',
  styleUrls: ['./by-margin-multiple-byno.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class ByMarginMultipleBynoComponent implements OnInit {
  objDblRateByMarginLoad: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    margin: '',
    profit_percentage: 0,
    company_section_name: this._localStorage.getCompanySectionName()
  }

  objUnChangedDblRateByAmtLoad: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    margin: '',
    profit_percentage: 0,
    company_section_name: this._localStorage.getCompanySectionName()
  }

  ForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  UnChangedForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  Inv_Byno: string = "";
  Byno_Serial: string = "";
  Byno_Prod_Serial: string = "";
  keyUpEvent: any;
  doubleRateByMarginList: any[] = [];

  columns: any[] = [
    { display: 'marginGrid', editable: true },
    { display: 'newPrice', editable: true },
  ];

  @ViewChildren('byno') byNo: ElementRef | any;
  @ViewChildren('marginGrid') marginGrid: ElementRef | any;
  @ViewChildren('newPrice') newPrice: ElementRef | any;

  constructor(
    public _router: Router,
    public _localStorage: LocalStorage,
    private _matDialog: MatDialog,
    private _datePipe: DatePipe,
    private _decimalPipe: DecimalPipe,
    private _excelService: ExcelService,
    private _gridKeyEvents: GridKeyEvents,
    private _keyPressEvents: KeyPressEvents,
    private _byAmountService: ByAmountService,
    private _singleProductService: SingleProductService,
    private _confirmationDialog: ConfirmationDialogComponent,
  ) { }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.doubleRateByMarginList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.doubleRateByMarginList);
        break;
    }
  }

  ngOnInit() {
    setTimeout(() => {
      let input = this.byNo.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100);
  }

  /*************************************************** CRUD Operations ***************************************************/
  public getDblRateByMarginList(): void {
    if (this.beforeLoadValidate()) {
      let objGet: any = {
        SingleProduct: JSON.stringify([{
          inv_byno: this.ForCheckBynoData.Inv_Byno.toString().trim().toUpperCase(),
          byno_serial: this.ForCheckBynoData.Byno_Serial.toString().trim().toUpperCase(),
          byno_prod_serial: this.ForCheckBynoData.Byno_Prod_Serial.toString().trim().toUpperCase(),
          company_section_id: this._localStorage.getCompanySectionId()
        }])
      }
      this._singleProductService.getSingleProduct(objGet).subscribe((result: any) => {
        if (result) {
          console.log(result, 'By Margin(Multiple Byno) Details List');
          this.doubleRateByMarginList = JSON.parse(JSON.stringify(result));
          this.afterLoadSingleProductCosts();
        } else {
          this.objDblRateByMarginLoad.byno = '';
          this._confirmationDialog.openAlertDialog('No records found', 'By Margin(Multiple Byno)');
        }
      });
    }
  }

  private afterLoadSingleProductCosts(): void {
    this.doubleRateByMarginList.forEach(x => {
      x.margin = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(x.margin) !== -1 ? '' : x.margin;
      x.double_margin = '';
      x.new_selling_price = 0;
      x.selling_price = x.selling_price + '.00'
    });
    setTimeout(() => {
      let input = this.marginGrid.toArray();
      input[0].nativeElement.focus();
    }, 100);
    // this.objSingleProductLoad.byno = '';
  }

  public checkValidMargin(margin: string, isGrid?: boolean, i?: number): void {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(margin) === -1) {
      let objCheck: any = {
        Margin: JSON.stringify([{
          price_code: margin.toString().trim().toUpperCase(),
          company_section_id: this._localStorage.getCompanySectionId()
        }])
      }
      this._byAmountService.checkMargin(objCheck).subscribe((result: boolean) => {
        if (isGrid) {
          debugger;
          if (!result)
            this.openInvalidAlertDialog('Invalid Margin', i, 'By Margin(Multiple Byno)', 'marginGrid');
          else this.getMarginCalculation(margin, true, i);
        }
        else if (!result)
          this.openAlertDialog('Invalid Margin', 'By Margin(Multiple Byno)', 'margin');
      });
    }
  }

  public getMarginCalculation(margin: string, isGrid?: boolean, i?: number): void {
    if (this.beforeLoadValidate() && this.beforeCalculateValidate()) {
      let objGet: any = {
        Margin: JSON.stringify([{
          price_code: margin.toString().trim().toUpperCase(),
          company_section_id: this._localStorage.getCompanySectionId()
        }])
      }
      this._byAmountService.getMarginCalculation(objGet).subscribe((result: any) => {
        if (result) {
          debugger;
          if (!isGrid) {
            this.objDblRateByMarginLoad.margin = result[0].price_code;
            this.objDblRateByMarginLoad.profit_percentage = result[0].profit_percentage;
            this.setMargin(result[0]);
          } else {
            this.setMarginForSingleRow(result[0], i);
          }
        }
        // console.log(result, 'Calculation Result');
      });
    }
  }

  private setMargin(result: any): void {
    debugger;
    this.doubleRateByMarginList.forEach(x => {
      // x.margin = this.objDblRateByMarginLoad.margin;
      x.profit_percentage = result.profit_percentage;
      x.double_margin = result.price_code;
      this.calculateMarginValue(x);
    });
  }

  private setMarginForSingleRow(result: any, i: number): void {
    this.doubleRateByMarginList[i].profit_percentage = result.profit_percentage;
    this.doubleRateByMarginList[i].double_margin = result.price_code;
    this.calculateMarginValue(this.doubleRateByMarginList[i]);
  }

  private calculateMarginValue(element: any): void {
    debugger;
    element.new_selling_price = Math.round(+element.cost_price + (+element.cost_price * (+element.profit_percentage / 100)));
    this.convertToDecimal(element);
  }

  private convertToDecimal(element: any): void {
    element.selling_price = [null, undefined, "", '0.00'].indexOf(element.selling_price.toString().trim()) === -1 ? (this._decimalPipe.transform(+element.selling_price, '1.2-2')).replace(/,/g, '') : '0.00';
    element.new_selling_price = [null, undefined, "", '0.00'].indexOf(element.new_selling_price.toString().trim()) === -1 ? (this._decimalPipe.transform(+element.new_selling_price, '1.2-2')).replace(/,/g, '') : '0.00';

  }

  public addByMarginDoubleRate(): void {
    if (this.beforeSaveValidate()) {
      debugger;
      let tempArr: any = [];
      for (let i = 0; i < this.doubleRateByMarginList.length; i++) {
        if (([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.doubleRateByMarginList[i].new_selling_price) === -1 &&
          +this.doubleRateByMarginList[i].new_selling_price !== +this.doubleRateByMarginList[i].selling_price)) {
          tempArr.push({
            inv_byno: this.doubleRateByMarginList[i].inv_byno,
            byno_serial: this.doubleRateByMarginList[i].byno_serial,
            byno_prod_serial: this.doubleRateByMarginList[i].byno_prod_serial,
            old_selling_price: +this.doubleRateByMarginList[i].selling_price,
            new_selling_price: +this.doubleRateByMarginList[i].selling_price,
            selling_price: +this.doubleRateByMarginList[i].selling_price,
            entered_by: this._localStorage.intGlobalUserId(),
            company_section_id: this._localStorage.getCompanySectionId(),
            double_rate: +this.doubleRateByMarginList[i].new_selling_price,
            discount_rate: +this.doubleRateByMarginList[i].selling_price,
            add_margin: 0,
            less_margin: 0,
            aadi_pricecode: this.doubleRateByMarginList[i].double_margin.toString().trim().toUpperCase()
          });
        }
      }
      let objAdd: any = {
        ByPercentage: JSON.stringify(tempArr)
      }
      debugger;
      this._byAmountService.addByPercentage(objAdd).subscribe((result: boolean) => {
        if (result) {
          this.openPrintAlertDialog('Saved successfully', tempArr);
        }
      })
    }
  }

  /**************************************************** Validations ****************************************************/

  private beforeLoadValidate(): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objDblRateByMarginLoad.byno) !== -1) {
      document.getElementById('invByNo').focus();
      this._confirmationDialog.openAlertDialog('Enter byno', 'By Margin(Multiple Byno)');
      return false;
    } else return true;
  }

  private beforeCalculateValidate(): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objDblRateByMarginLoad.margin) !== -1) {
      this.openAlertDialog('Invalid Margin', 'By Margin(Multiple Byno)', 'margin');
      return false;
    } else return true;
  }

  private openAlertDialog(value: string, componentName: string, focus: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        document.getElementById(focus).focus();
      _dialogRef = null;
    });
  }

  public onlyAllowUpperCases(event: KeyboardEvent | any, i?: number): void {
    if (event.keyCode >= 65 && event.keyCode <= 90)
      event.target.value = event.target.value.toString().trim().toUpperCase();
    if (event.keyCode === 13)
      this.checkValidMargin(this.doubleRateByMarginList[i].double_margin, true, i)
  }

  private openInvalidAlertDialog(value: string, i: number, componentName: string, focus: any) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this[focus].toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  private beforeSaveValidate(): boolean {
    debugger;
    if (this.doubleRateByMarginList.length === 0) {
      this._confirmationDialog.openAlertDialog('No records to save, Load the data first', 'By Margin(Multiple Byno)');
      return false;
    } if (!this.checkAtleastPriceChanges()) {
      this._confirmationDialog.openAlertDialog('No changes to save', 'By Margin(Multiple Byno)');
      return false;
    } if (!this.checkValidPrice()) {
      this._confirmationDialog.openAlertDialog('No changes to save', 'By Margin(Multiple Byno)');
      return false;
    } else return true;
  }

  private checkAtleastPriceChanges(): boolean {
    let i = this.doubleRateByMarginList.findIndex(x => +x.new_selling_price !== +x.selling_price);
    return i !== -1 ? true : false;
  }

  private checkValidPrice(): boolean {
    for (let i = 0; i < this.doubleRateByMarginList.length; i++) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.doubleRateByMarginList[i].new_selling_price) !== -1)
        return false;
    } return true;
  }

  openPrintAlertDialog(meassge: any, data: any[]) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = meassge;
    _dialogRef.componentInstance.componentName = "By Margin(Multiple Byno)";
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.openPrintConfirmationDialog(data);
      _dialogRef = null;
    });
  }

  public openPrintConfirmationDialog(data: any[]): any {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    _dialogRef.componentInstance.confirmMessage = "Do you want to print this record ?"
    _dialogRef.componentInstance.componentName = "By Margin(Multiple Byno)";
    return _dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.print(data);
      else {
        this.resetScreen();
      }

      _dialogRef = null;
    });
  }

  private print(data: any[]) {
    this.resetScreen();
  }

  private resetScreen(): void {
    this.doubleRateByMarginList = [];
    this.ForCheckBynoData = JSON.parse(JSON.stringify(this.UnChangedForCheckBynoData));
    this.objDblRateByMarginLoad = JSON.parse(JSON.stringify(this.objUnChangedDblRateByAmtLoad));
  }

  /***************************************************** ByNo Validations *****************************************************/

  public onKeyPress(keyUpEvent: any) {
    debugger;
    this.doubleRateByMarginList = [];
    if (keyUpEvent.keyCode === 13 && this.objDblRateByMarginLoad.byno === '') {
      document.getElementById("byno").focus();
      this._confirmationDialog.openAlertDialog('Enter byno', 'By Margin(Multiple Byno)');
    } else if (keyUpEvent.keyCode === 13 && this.objDblRateByMarginLoad.byno.length === 8) {
      this.objDblRateByMarginLoad.byno = this.objDblRateByMarginLoad.byno.toString().trim().toUpperCase();
      this.ForCheckBynoData.Inv_Byno = this.objDblRateByMarginLoad.byno.toString().trim().toUpperCase();
      this.ForCheckBynoData.Byno_Serial = "";
      this.ForCheckBynoData.Byno_Prod_Serial = "";
      this.getDblRateByMarginList();
    } else if (keyUpEvent.keyCode === 13 && this.objDblRateByMarginLoad.byno.length == 16 && !this.objDblRateByMarginLoad.byno.includes("/")) {
      this.byNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objDblRateByMarginLoad.byno.length == 28 && !this.objDblRateByMarginLoad.byno.includes("/")) {
      this.OldByNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objDblRateByMarginLoad.byno.length == 30 && !this.objDblRateByMarginLoad.byno.includes("/")) {
      this.byNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objDblRateByMarginLoad.byno.includes("/")) {
      debugger
      this.splitByNumber();
    } else if (this.objDblRateByMarginLoad.byno.length >= 28 && keyUpEvent.keyCode === 13) {
      this.autoScannerCall();
    } else {
      if (keyUpEvent.keyCode === 13 && !this.objDblRateByMarginLoad.byno.includes("/") && this.objDblRateByMarginLoad.byno.length > 16) {
        document.getElementById('byno').focus();
        this._confirmationDialog.openAlertDialog("Invalid Byno", "By Margin(Multiple Byno)");
      } else if (keyUpEvent.keyCode === 13 && !this.checkValid()) {
        document.getElementById('byno').focus();
        this._confirmationDialog.openAlertDialog("Invalid Byno", "By Margin(Multiple Byno)");
        this.objDblRateByMarginLoad.byno = '';
      }
    }
  }

  private checkValid(): boolean {
    debugger;
    let index = this.doubleRateByMarginList.findIndex(x => x.byno.toString().trim().toUpperCase() === this.objDblRateByMarginLoad.inv_byno.toString().trim().toUpperCase());
    if (index !== -1)
      return true;
    else
      return false;
  }

  private byNoWithoutSlash() {
    debugger;
    this.objDblRateByMarginLoad.byno = this.objDblRateByMarginLoad.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objDblRateByMarginLoad.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objDblRateByMarginLoad.byno.substr(9, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objDblRateByMarginLoad.byno.substr(13, 3);
    this.getDblRateByMarginList();
  }

  private OldByNoWithoutSlash() {
    debugger;
    this.objDblRateByMarginLoad.byno = this.objDblRateByMarginLoad.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objDblRateByMarginLoad.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objDblRateByMarginLoad.byno.substr(8, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objDblRateByMarginLoad.byno.substr(11, 3);
    // console.log(this.ForCheckBynoData);
    this.getDblRateByMarginList();
  }

  private autoScannerCall() {
    debugger;
    this.ForCheckBynoData.Inv_Byno = this.objDblRateByMarginLoad.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = this.objDblRateByMarginLoad.byno.substr(8, 4);
    this.ForCheckBynoData.Byno_Prod_Serial = this.objDblRateByMarginLoad.byno.substr(11, 4);
    // this.objSingleProductLoad.byno = '';
    this.getDblRateByMarginList();
  }

  private splitByNumber() {
    debugger;
    this.objDblRateByMarginLoad.byno = this.objDblRateByMarginLoad.byno.toString().trim().toUpperCase();
    var x = this.objDblRateByMarginLoad.byno.split("/");
    this.Inv_Byno = x[0];
    this.Byno_Serial = x[1];
    this.Byno_Prod_Serial = [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(x[2]) !== -1 ? "" : x[2];
    if (this.Byno_Serial.length == 2) {
      this.Byno_Serial = "00".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 2) {
      this.Byno_Prod_Serial = "00".concat(this.Byno_Prod_Serial);
    }
    if (this.Byno_Serial.length == 1) {
      this.Byno_Serial = "000".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 1) {
      this.Byno_Prod_Serial = "000".concat(this.Byno_Prod_Serial);
    } if (this.Byno_Serial.length == 0) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Invalid Byno', 'By Margin(Multiple Byno)');
      return;
    } if (this.Byno_Prod_Serial.length == 0) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Invalid Byno', 'By Margin(Multiple Byno)');
      return;
    }
    this.ForCheckBynoData.Inv_Byno = this.Inv_Byno.toString().trim();
    this.ForCheckBynoData.Byno_Serial = this.Byno_Serial.toString().trim();
    this.ForCheckBynoData.Byno_Prod_Serial = this.Byno_Prod_Serial.toString().trim();
    this.getDblRateByMarginList();
  }

}
