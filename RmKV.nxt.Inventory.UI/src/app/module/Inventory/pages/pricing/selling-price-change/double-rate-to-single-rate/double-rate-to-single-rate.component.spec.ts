import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoubleRateToSingleRateComponent } from './double-rate-to-single-rate.component';

describe('DoubleRateToSingleRateComponent', () => {
  let component: DoubleRateToSingleRateComponent;
  let fixture: ComponentFixture<DoubleRateToSingleRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoubleRateToSingleRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoubleRateToSingleRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
