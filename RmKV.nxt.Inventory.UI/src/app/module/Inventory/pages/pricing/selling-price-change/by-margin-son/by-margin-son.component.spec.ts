import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByMarginSonComponent } from './by-margin-son.component';

describe('ByMarginSonComponent', () => {
  let component: ByMarginSonComponent;
  let fixture: ComponentFixture<ByMarginSonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByMarginSonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByMarginSonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
