import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByMarginCodeComponent } from './by-margin-code.component';

describe('ByMarginCodeComponent', () => {
  let component: ByMarginCodeComponent;
  let fixture: ComponentFixture<ByMarginCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByMarginCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByMarginCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
