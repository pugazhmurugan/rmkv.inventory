import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ForCheckBynoData } from 'src/app/module/Inventory/model/common.model';
import { ByAmountService } from 'src/app/module/Inventory/services/pricing/selling-price-change/single-product/double-rate/by-amount/by-amount.service';
import { SingleProductService } from 'src/app/module/Inventory/services/pricing/selling-price-change/single-product/single-product.service';
declare var jsPDF: any;

@Component({
  selector: 'app-by-percentage',
  templateUrl: './by-percentage.component.html',
  styleUrls: ['./by-percentage.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class ByPercentageComponent implements OnInit {
  objByPercentageLoad: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    double_percent: 0,
    discount_percent: 0,
    company_section_name: this._localStorage.getCompanySectionName()
  }

  objUnChangedByPercentageLoad: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    double_percent: 0,
    discount_percent: 0,
    company_section_name: this._localStorage.getCompanySectionName()
  }

  ForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  UnChangedForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  Inv_Byno: string = "";
  Byno_Serial: string = "";
  Byno_Prod_Serial: string = "";
  keyUpEvent: any;
  SellingPriceByPercentList: any[] = [];

  columns: any[] = [
    { display: 'doublePercent', editable: true },
    { display: 'doubleRate', editable: true },
    { display: 'discountPercent', editable: true },
    { display: 'discountRate', editable: true },
  ];

  @ViewChildren('byno') byNo: ElementRef | any;
  @ViewChildren('doublePercent') doublePercent: ElementRef | any;
  @ViewChildren('doubleRate') doubleRate: ElementRef | any;
  @ViewChildren('discountPercent') discountPercent: ElementRef | any;
  @ViewChildren('discountRate') discountRate: ElementRef | any;

  constructor(
    public _router: Router,
    public _localStorage: LocalStorage,
    private _matDialog: MatDialog,
    private _datePipe: DatePipe,
    private _decimalPipe: DecimalPipe,
    private _excelService: ExcelService,
    private _gridKeyEvents: GridKeyEvents,
    private _keyPressEvents: KeyPressEvents,
    private _byAmountService: ByAmountService,
    private _singleProductService: SingleProductService,
    private _confirmationDialog: ConfirmationDialogComponent,
  ) { }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.SellingPriceByPercentList);
        // this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.SellingPriceByPercentList);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.SellingPriceByPercentList);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.SellingPriceByPercentList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.SellingPriceByPercentList);
        break;
    }
  }

  ngOnInit() {
    setTimeout(() => {
      let input = this.byNo.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100);

  }


   /***************************************************** ByNo Validations *****************************************************/

   public onKeyPress(keyUpEvent: any) {
    debugger;
    this.SellingPriceByPercentList = [];
    if (keyUpEvent.keyCode === 13 && this.objByPercentageLoad.byno === '') {
      document.getElementById("byno").focus();
      this._confirmationDialog.openAlertDialog('Enter byno', 'By Percentage SP');
    } else if (keyUpEvent.keyCode === 13 && this.objByPercentageLoad.byno.length === 8) {
      this.objByPercentageLoad.byno = this.objByPercentageLoad.byno.toString().trim().toUpperCase();
      this.ForCheckBynoData.Inv_Byno = this.objByPercentageLoad.byno.toString().trim().toUpperCase();
      this.ForCheckBynoData.Byno_Serial = "";
      this.ForCheckBynoData.Byno_Prod_Serial = "";
      this.getDblRateByPercenttList();
    } else if (keyUpEvent.keyCode === 13 && this.objByPercentageLoad.byno.length == 16 && !this.objByPercentageLoad.byno.includes("/")) {
      this.byNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objByPercentageLoad.byno.length == 28 && !this.objByPercentageLoad.byno.includes("/")) {
      this.OldByNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objByPercentageLoad.byno.length == 30 && !this.objByPercentageLoad.byno.includes("/")) {
      this.byNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objByPercentageLoad.byno.includes("/")) {
      debugger
      this.splitByNumber();
    } else if (this.objByPercentageLoad.byno.length >= 28 && keyUpEvent.keyCode === 13) {
      this.autoScannerCall();
    } else {
      if (keyUpEvent.keyCode === 13 && !this.objByPercentageLoad.byno.includes("/") && this.objByPercentageLoad.byno.length > 16) {
        document.getElementById('byno').focus();
        this._confirmationDialog.openAlertDialog("Invalid Byno", "By Percentage SP");
      } else if (keyUpEvent.keyCode === 13 && !this.checkValid()) {
        document.getElementById('byno').focus();
        this._confirmationDialog.openAlertDialog("Invalid Byno", "By Percentage SP");
        this.objByPercentageLoad.byno = '';
      }
    }
  }

  private checkValid(): boolean {
    debugger;
    let index = this.SellingPriceByPercentList.findIndex(x => x.byno.toString().trim().toUpperCase() === this.objByPercentageLoad.inv_byno.toString().trim().toUpperCase());
    if (index !== -1)
      return true;
    else
      return false;
  }

  private byNoWithoutSlash() {
    debugger;
    this.objByPercentageLoad.byno = this.objByPercentageLoad.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objByPercentageLoad.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objByPercentageLoad.byno.substr(9, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objByPercentageLoad.byno.substr(13, 3);
    this.getDblRateByPercenttList();
  }

  private OldByNoWithoutSlash() {
    debugger;
    this.objByPercentageLoad.byno = this.objByPercentageLoad.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objByPercentageLoad.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objByPercentageLoad.byno.substr(8, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objByPercentageLoad.byno.substr(11, 3);
    this.getDblRateByPercenttList();
  }

  private autoScannerCall() {
    debugger;
    this.ForCheckBynoData.Inv_Byno = this.objByPercentageLoad.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = this.objByPercentageLoad.byno.substr(8, 4);
    this.ForCheckBynoData.Byno_Prod_Serial = this.objByPercentageLoad.byno.substr(11, 4);
    this.getDblRateByPercenttList();
  }

  private splitByNumber() {
    debugger;
    this.objByPercentageLoad.byno = this.objByPercentageLoad.byno.toString().trim().toUpperCase();
    var x = this.objByPercentageLoad.byno.split("/");
    this.Inv_Byno = x[0];
    this.Byno_Serial = x[1];
    this.Byno_Prod_Serial = [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(x[2]) !== -1 ? "" : x[2];
    if (this.Byno_Serial.length == 2) {
      this.Byno_Serial = "00".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 2) {
      this.Byno_Prod_Serial = "00".concat(this.Byno_Prod_Serial);
    }
    if (this.Byno_Serial.length == 1) {
      this.Byno_Serial = "000".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 1) {
      this.Byno_Prod_Serial = "000".concat(this.Byno_Prod_Serial);
    } if (this.Byno_Serial.length == 0) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Invalid Byno', 'By Percentage SP');
      return;
    } if (this.Byno_Prod_Serial.length == 0) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Invalid Byno', 'By Percentage SP');
      return;
    }
    this.ForCheckBynoData.Inv_Byno = this.Inv_Byno.toString().trim();
    this.ForCheckBynoData.Byno_Serial = this.Byno_Serial.toString().trim();
    this.ForCheckBynoData.Byno_Prod_Serial = this.Byno_Prod_Serial.toString().trim();
    this.getDblRateByPercenttList();
  }

    /*************************************************** CRUD Operations ***************************************************/
    public getDblRateByPercenttList(): void {
      if (this.beforeLoadValidate()) {
        let objGet: any = {
          SingleProduct: JSON.stringify([{
            inv_byno: this.ForCheckBynoData.Inv_Byno.toString().trim().toUpperCase(),
            byno_serial: this.ForCheckBynoData.Byno_Serial.toString().trim().toUpperCase(),
            byno_prod_serial: this.ForCheckBynoData.Byno_Prod_Serial.toString().trim().toUpperCase(),
            company_section_id: this._localStorage.getCompanySectionId()
          }])
        }
        this._singleProductService.getSingleProduct(objGet).subscribe((result: any) => {
          if (result) {
            this.SellingPriceByPercentList = JSON.parse(JSON.stringify(result));
            this.afterLoadSingleProductCosts();
          } else {
            this.objByPercentageLoad.byno = '';
            this._confirmationDialog.openAlertDialog('No records found', 'By Percentage SP');
          }
        });
      }
    }
  
    private afterLoadSingleProductCosts(): void {
      this.SellingPriceByPercentList.forEach((x, i) => {
        x.new_selling_price = x.selling_price;
        x.double_percent = 0;
        x.double_rate = x.selling_price;
        x.discount_percent = 0;
        x.discount_rate = x.selling_price;
        this.convertToDecimal(i);
      });
      setTimeout(() => {
        let input = this.doublePercent.toArray();
        input[0].nativeElement.focus();
      }, 100);
    }
  
    public addDblRateByPercentage(): void {
      if (this.beforeSaveValidate()) {
        debugger;
        let tempArr: any = [];
        for (let i = 0; i < this.SellingPriceByPercentList.length; i++) {
          if (([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.SellingPriceByPercentList[i].double_rate) === -1 &&
            +this.SellingPriceByPercentList[i].double_rate !== +this.SellingPriceByPercentList[i].selling_price) || ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.SellingPriceByPercentList[i].discount_rate) === -1 &&
              +this.SellingPriceByPercentList[i].discount_rate !== +this.SellingPriceByPercentList[i].selling_price)) {
            tempArr.push({
              inv_byno: this.SellingPriceByPercentList[i].inv_byno,
              byno_serial: this.SellingPriceByPercentList[i].byno_serial,
              byno_prod_serial: this.SellingPriceByPercentList[i].byno_prod_serial,
              old_selling_price: +this.SellingPriceByPercentList[i].selling_price,
              new_selling_price: +this.SellingPriceByPercentList[i].discount_rate,
              selling_price: +this.SellingPriceByPercentList[i].selling_price,
              entered_by: this._localStorage.intGlobalUserId(),
              company_section_id: this._localStorage.getCompanySectionId(),
              double_rate: +this.SellingPriceByPercentList[i].double_rate,
              discount_rate: +this.SellingPriceByPercentList[i].discount_rate,
              add_margin: +this.SellingPriceByPercentList[i].double_percent,
              less_margin: +this.SellingPriceByPercentList[i].discount_percent
            });
          }
        }
        let objAdd: any = {
          ByPercentage: JSON.stringify(tempArr)
        }
        debugger;
        this._byAmountService.addByPercentage(objAdd).subscribe((result: boolean) => {
          if (result) {
            this.openAlertDialog('Saved successfully', tempArr);
          }
        })
      }
    }
  
    public onChangeOfGlobalDoubleRate(): void {
      this.SellingPriceByPercentList.forEach((x, i) => {
        debugger;
        x.double_percent = +this.objByPercentageLoad.double_percent;
        x.discount_percent = +this.objByPercentageLoad.discount_percent;
        this.calculateDoubleRate(i);
      });
    }
  
    public calculateDoubleRate(i: number): void {
      debugger;
      this.SellingPriceByPercentList[i].double_rate = Math.round(+this.SellingPriceByPercentList[i].selling_price + (+this.SellingPriceByPercentList[i].selling_price * (+this.SellingPriceByPercentList[i].double_percent / 100)));
      this.calculateDiscountRate(i);
    }
  
    public calculateDiscountRate(i: number, doubleRate?: boolean): void {
      debugger;
      this.SellingPriceByPercentList[i].discount_rate = Math.round(+this.SellingPriceByPercentList[i].double_rate - (+this.SellingPriceByPercentList[i].double_rate * (+this.SellingPriceByPercentList[i].discount_percent / 100)));
      this.convertToDecimal(i, doubleRate);
    }
  
    private convertToDecimal(i: number, doubleRate?: boolean): void {
      let element = this.SellingPriceByPercentList[i];
      this.SellingPriceByPercentList[i].selling_price = [null, undefined, "", '0.00'].indexOf(element.selling_price.toString().trim()) === -1 ? (this._decimalPipe.transform(+element.selling_price, '1.2-2')).replace(/,/g, '') : '0.00';
      if (!doubleRate)
        this.SellingPriceByPercentList[i].double_rate = [null, undefined, "", '0.00'].indexOf(element.double_rate.toString().trim()) === -1 ? (this._decimalPipe.transform(+element.double_rate, '1.2-2')).replace(/,/g, '') : '0.00';
      this.SellingPriceByPercentList[i].discount_rate = [null, undefined, "", '0.00'].indexOf(element.discount_rate.toString().trim()) === -1 ? (this._decimalPipe.transform(+element.discount_rate, '1.2-2')).replace(/,/g, '') : '0.00';
    }
    /**************************************************** Validations ****************************************************/
  
    private beforeLoadValidate(): boolean {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objByPercentageLoad.byno) !== -1) {
        document.getElementById('invByNo').focus();
        this._confirmationDialog.openAlertDialog('Enter byno', 'By Percentage SP');
        return false;
      } else return true;
    }
  
    public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
      debugger;
      let key = key1 + '[' + index + ']' + '.' + key2;
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[0-9]*$/;
        if (pattern.test(event.key)) {
          this[key] = "";
          event.target.value = "";
        }
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      } else {
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      }
    }
  
    public onlyAllowInputNumbers(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
      debugger;
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
      } else {
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
      }
    }
  
    public onlyAllowNumbers(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
      debugger;
      let key = key1 + '[' + index + ']' + '.' + key2;
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[0-9]*$/;
        if (pattern.test(event.key)) {
          this[key] = "";
          event.target.value = "";
        }
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
      } else {
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
      }
    }
  
    private beforeSaveValidate(): boolean {
      debugger;
      if (this.SellingPriceByPercentList.length === 0) {
        this._confirmationDialog.openAlertDialog('No records to save, Load the data first', 'By Percentage SP');
        return false;
      } if (!this.checkAtleastPriceChanges()) {
        this._confirmationDialog.openAlertDialog('No changes to save', 'By Percentage SP');
        return false;
      } if (!this.checkValidPrice()) {
        this._confirmationDialog.openAlertDialog('No changes to save', 'By Percentage SP');
        return false;
      } else return true;
    }
  
    private checkAtleastPriceChanges(): boolean {
      let i = this.SellingPriceByPercentList.findIndex(x => +x.double_rate !== +x.selling_price);
      let k = this.SellingPriceByPercentList.findIndex(x => +x.discount_rate !== +x.selling_price);
      return i !== -1 || k !== -1 ? true : false;
    }
  
    private checkValidPrice(): boolean {
      for (let i = 0; i < this.SellingPriceByPercentList.length; i++) {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.SellingPriceByPercentList[i].double_rate) !== -1 &&
          [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.SellingPriceByPercentList[i].discount_rate) !== -1)
          return false;
      } return true;
    }
  
    private resetScreen(): void {
      this.SellingPriceByPercentList = [];
      this.ForCheckBynoData = JSON.parse(JSON.stringify(this.UnChangedForCheckBynoData));
      this.objByPercentageLoad = JSON.parse(JSON.stringify(this.objUnChangedByPercentageLoad));
    }
  
    openAlertDialog(meassge: any, data: any[]) {
      let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
        panelClass: "custom-dialog-container",
        data: { confirmationDialog: 0 }
      });
      _dialogRef.componentInstance.alertMessage = meassge;
      _dialogRef.componentInstance.componentName = "By Percentage SP";
      _dialogRef.afterClosed().subscribe(result => {
        if (result)
          this.openPrintConfirmationDialog(data);
        _dialogRef = null;
      });
    }
  
    public openPrintConfirmationDialog(data: any[]): any {
      let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
        panelClass: "custom-dialog-container",
        data: { confirmationDialog: 1 }
      });
      _dialogRef.componentInstance.confirmMessage = "Do you want to print this record ?"
      _dialogRef.componentInstance.componentName = "By Percentage SP";
      return _dialogRef.afterClosed().subscribe(result => {
        if (result)
          this.print(data);
        else {
          this.resetScreen();
        }
  
        _dialogRef = null;
      });
    }
  
    private print(data: any[]) {
      this.resetScreen();
    }


       /*************************************************** Exports ************************************************/

   public exportToExcel(): void {
    if (this.SellingPriceByPercentList.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.SellingPriceByPercentList.length; i++) {
        json[i] = Object.assign({
          "ByNo": this.SellingPriceByPercentList[i].byno,
          "Product Name": this.SellingPriceByPercentList[i].product_name,
          "Counter": this.SellingPriceByPercentList[i].counter_code,
          "Current Price": this.SellingPriceByPercentList[i].selling_price,
          "Add Margin SingleRate %": this.SellingPriceByPercentList[i].double_percent,
          "Add Margin SingleRate Amount": this.SellingPriceByPercentList[i].double_rate,
          "Less Margin SingleRate %": this.SellingPriceByPercentList[i].discount_percent,
          "Less Margin SingleRate Amount": this.SellingPriceByPercentList[i].discount_rate,
        });
      }
      this._excelService.exportAsExcelFile(json, "By Percentage SP", datetime);
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "By Percentage SP");
  }

  public exportToPdf(): void {
    if (this.SellingPriceByPercentList.length != 0) {
      var prepare = [];
      this.SellingPriceByPercentList.forEach(e => {
        var tempObj = [];
        tempObj.push(e.byno);
        tempObj.push(e.product_name);
        tempObj.push(e.counter_code);
        tempObj.push(e.selling_price);
        tempObj.push(e.double_percent);
        tempObj.push(e.double_rate);
        tempObj.push(e.discount_percent);
        tempObj.push(e.discount_rate);
        prepare.push(tempObj);
      });

      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["ByNo", "Product Name", "Counter", "Current Price", "Add Margin SglRte %","Add Margin SglRte Amt","Less Margin SglRte %","Less Margin SglRte Amt"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('By Percentage SP' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "By Percentage SP");
  }


}
