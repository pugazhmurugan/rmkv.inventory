import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByPercentageComponent } from './by-percentage.component';

describe('ByPercentageComponent', () => {
  let component: ByPercentageComponent;
  let fixture: ComponentFixture<ByPercentageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByPercentageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByPercentageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
