import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByMarginDoubleRateComponent } from './by-margin-double-rate.component';

describe('ByMarginDoubleRateComponent', () => {
  let component: ByMarginDoubleRateComponent;
  let fixture: ComponentFixture<ByMarginDoubleRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByMarginDoubleRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByMarginDoubleRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
