import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByAmountComponent } from './by-amount.component';

describe('ByAmountComponent', () => {
  let component: ByAmountComponent;
  let fixture: ComponentFixture<ByAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByAmountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
