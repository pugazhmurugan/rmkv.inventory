import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BynoWiseDiscountComponent } from './byno-wise-discount.component';

describe('BynoWiseDiscountComponent', () => {
  let component: BynoWiseDiscountComponent;
  let fixture: ComponentFixture<BynoWiseDiscountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BynoWiseDiscountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BynoWiseDiscountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
