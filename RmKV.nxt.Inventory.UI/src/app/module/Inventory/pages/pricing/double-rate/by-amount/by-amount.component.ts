import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ForCheckBynoData } from 'src/app/module/Inventory/model/common.model';
import { ByAmountService } from 'src/app/module/Inventory/services/pricing/selling-price-change/single-product/double-rate/by-amount/by-amount.service';
import { SingleProductService } from 'src/app/module/Inventory/services/pricing/selling-price-change/single-product/single-product.service';
declare var jsPDF: any;

@Component({
  selector: 'app-by-amount',
  templateUrl: './by-amount.component.html',
  styleUrls: ['./by-amount.component.scss'],
  providers: [DatePipe]
})
export class ByAmountComponent implements OnInit {
  objDblRateByAmtLoad: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    company_section_name: this._localStorage.getCompanySectionName()
  }

  objUnChangedDblRateByAmtLoad: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    company_section_name: this._localStorage.getCompanySectionName()
  }

  ForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  UnChangedForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  Inv_Byno: string = "";
  Byno_Serial: string = "";
  Byno_Prod_Serial: string = "";
  keyUpEvent: any;
  doubleRateByAmountList: any[] = [];

  columns: any[] = [
    { display: 'newPrice', editable: true },
  ];

  @ViewChildren('byno') byNo: ElementRef | any;
  @ViewChildren('newPrice') newPrice: ElementRef | any;

  constructor(
    public _router: Router,
    public _localStorage: LocalStorage,
    private _matDialog: MatDialog,
    private _datePipe: DatePipe,
    private _excelService: ExcelService,
    private _gridKeyEvents: GridKeyEvents,
    private _keyPressEvents: KeyPressEvents,
    private _byAmountService: ByAmountService,
    private _singleProductService: SingleProductService,
    private _confirmationDialog: ConfirmationDialogComponent,
  ) { }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.doubleRateByAmountList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.doubleRateByAmountList);
        break;
    }
  }

  ngOnInit() {
    setTimeout(() => {
      let input = this.byNo.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100);
  }

  /*************************************************** CRUD Operations ***************************************************/
  public getDblRateByAmounttList(): void {
    if (this.beforeLoadValidate()) {
      let objGet: any = {
        SingleProduct: JSON.stringify([{
          inv_byno: this.ForCheckBynoData.Inv_Byno.toString().trim().toUpperCase(),
          byno_serial: this.ForCheckBynoData.Byno_Serial.toString().trim().toUpperCase(),
          byno_prod_serial: this.ForCheckBynoData.Byno_Prod_Serial.toString().trim().toUpperCase(),
          company_section_id: this._localStorage.getCompanySectionId()
        }])
      }
      this._singleProductService.getSingleProduct(objGet).subscribe((result: any) => {
        if (result) {
          // console.log(result, 'By Amount Details List');
          this.doubleRateByAmountList = JSON.parse(JSON.stringify(result));
          this.afterLoadSingleProductCosts();
        } else {
          this.objDblRateByAmtLoad.byno = '';
          this._confirmationDialog.openAlertDialog('No records found', 'By Amount');
        }
      });
    }
  }

  private afterLoadSingleProductCosts(): void {
    this.doubleRateByAmountList.forEach(x => {
      x.new_selling_price = x.selling_price;
    });
    setTimeout(() => {
      let input = this.newPrice.toArray();
      input[0].nativeElement.focus();
    }, 100);
    // this.objSingleProductLoad.byno = '';
  }

  public addDblRateByAmount(): void {
    if (this.beforeSaveValidate()) {
      debugger;
      let tempArr: any = [];
      for (let i = 0; i < this.doubleRateByAmountList.length; i++) {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.doubleRateByAmountList[i].new_selling_price) === -1 &&
          +this.doubleRateByAmountList[i].new_selling_price !== +this.doubleRateByAmountList[i].selling_price) {
          tempArr.push({
            inv_byno: this.doubleRateByAmountList[i].inv_byno,
            byno_serial: this.doubleRateByAmountList[i].byno_serial,
            byno_prod_serial: this.doubleRateByAmountList[i].byno_prod_serial,
            old_selling_price: +this.doubleRateByAmountList[i].selling_price,
            new_selling_price: +this.doubleRateByAmountList[i].new_selling_price,
            selling_price: +this.doubleRateByAmountList[i].selling_price,
            entered_by: this._localStorage.intGlobalUserId(),
            company_section_id: this._localStorage.getCompanySectionId(),
            double_rate: +this.doubleRateByAmountList[i].selling_price,
            discount_rate: +this.doubleRateByAmountList[i].new_selling_price
          });
        }
      }
      let objAdd: any = {
        ByAmount: JSON.stringify(tempArr)
      }
      debugger;
      this._byAmountService.addByAmount(objAdd).subscribe((result: boolean) => {
        if (result) {
          // this.resetScreen();
          this.openAlertDialog('Saved successfully', tempArr);
        }
      })
    }
  }

  /**************************************************** Validations ****************************************************/

  private beforeLoadValidate(): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objDblRateByAmtLoad.byno) !== -1) {
      document.getElementById('invByNo').focus();
      this._confirmationDialog.openAlertDialog('Enter byno', 'By Amount');
      return false;
    } else return true;
  }

  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  private beforeSaveValidate(): boolean {
    debugger;
    if (this.doubleRateByAmountList.length === 0) {
      this._confirmationDialog.openAlertDialog('No records to save, Load the data first', 'By Amount');
      return false;
    } if (!this.checkAtleastPriceChanges()) {
      this._confirmationDialog.openAlertDialog('No changes to save', 'By Amount');
      return false;
    } if (!this.checkValidPrice()) {
      this._confirmationDialog.openAlertDialog('No changes to save', 'By Amount');
      return false;
    } else return true;
  }

  private checkAtleastPriceChanges(): boolean {
    let i = this.doubleRateByAmountList.findIndex(x => +x.new_selling_price !== +x.selling_price);
    return i === -1 ? false : true;
  }

  private checkValidPrice(): boolean {
    for (let i = 0; i < this.doubleRateByAmountList.length; i++) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.doubleRateByAmountList[i].new_selling_price) !== -1)
        return false;
    } return true;
  }

  private resetScreen(): void {
    this.doubleRateByAmountList = [];
    this.ForCheckBynoData = JSON.parse(JSON.stringify(this.UnChangedForCheckBynoData));
    this.objDblRateByAmtLoad = JSON.parse(JSON.stringify(this.objUnChangedDblRateByAmtLoad));
  }

  openAlertDialog(meassge: any, data: any[]) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = meassge;
    _dialogRef.componentInstance.componentName = "By Amount";
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.openPrintConfirmationDialog(data);
      _dialogRef = null;
    });
  }

  public openPrintConfirmationDialog(data: any[]): any {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    _dialogRef.componentInstance.confirmMessage = "Do you want to print this record ?"
    _dialogRef.componentInstance.componentName = "By Amount";
    return _dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.print(data);
      else {
        this.resetScreen();
      }

      _dialogRef = null;
    });
  }

  private print(data: any[]) {
    this.resetScreen();
  }

   /*************************************************** Exports ************************************************/

   public exportToExcel(): void {
    if (this.doubleRateByAmountList.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.doubleRateByAmountList.length; i++) {
        json[i] = Object.assign({
          "ByNo": this.doubleRateByAmountList[i].byno,
          "Product Name": this.doubleRateByAmountList[i].product_name,
          "Counter": this.doubleRateByAmountList[i].counter_code,
          "Current Rate": this.doubleRateByAmountList[i].selling_price,
          "Discount Rate": this.doubleRateByAmountList[i].new_selling_price
        });
      }
      this._excelService.exportAsExcelFile(json, "By_Amount", datetime);
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "By Amount");
  }

  public exportToPdf(): void {
    if (this.doubleRateByAmountList.length != 0) {
      var prepare = [];
      this.doubleRateByAmountList.forEach(e => {
        var tempObj = [];
        tempObj.push(e.byno);
        tempObj.push(e.product_name);
        tempObj.push(e.counter_code);
        tempObj.push(e.selling_price);
        tempObj.push(e.new_selling_price);
        prepare.push(tempObj);
      });

      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["ByNo", "Product Name", "Counter", "Current Rate", "Discount Rate"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('By_Amount' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "By Amount");
  }

  /***************************************************** ByNo Validations *****************************************************/

  public onKeyPress(keyUpEvent: any) {
    debugger;
    this.doubleRateByAmountList = [];
    if (keyUpEvent.keyCode === 13 && this.objDblRateByAmtLoad.byno === '') {
      document.getElementById("byno").focus();
      this._confirmationDialog.openAlertDialog('Enter byno', 'By Amount');
    } else if (keyUpEvent.keyCode === 13 && this.objDblRateByAmtLoad.byno.length === 8) {
      this.objDblRateByAmtLoad.byno = this.objDblRateByAmtLoad.byno.toString().trim().toUpperCase();
      this.ForCheckBynoData.Inv_Byno = this.objDblRateByAmtLoad.byno.toString().trim().toUpperCase();
      this.ForCheckBynoData.Byno_Serial = "";
      this.ForCheckBynoData.Byno_Prod_Serial = "";
      this.getDblRateByAmounttList();
    } else if (keyUpEvent.keyCode === 13 && this.objDblRateByAmtLoad.byno.length == 16 && !this.objDblRateByAmtLoad.byno.includes("/")) {
      this.byNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objDblRateByAmtLoad.byno.length == 28 && !this.objDblRateByAmtLoad.byno.includes("/")) {
      this.OldByNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objDblRateByAmtLoad.byno.length == 30 && !this.objDblRateByAmtLoad.byno.includes("/")) {
      this.byNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objDblRateByAmtLoad.byno.includes("/")) {
      debugger
      this.splitByNumber();
    } else if (this.objDblRateByAmtLoad.byno.length >= 28 && keyUpEvent.keyCode === 13) {
      this.autoScannerCall();
    } else {
      if (keyUpEvent.keyCode === 13 && !this.objDblRateByAmtLoad.byno.includes("/") && this.objDblRateByAmtLoad.byno.length > 16) {
        document.getElementById('byno').focus();
        this._confirmationDialog.openAlertDialog("Invalid Byno", "By Amount");
      } else if (keyUpEvent.keyCode === 13 && !this.checkValid()) {
        document.getElementById('byno').focus();
        this._confirmationDialog.openAlertDialog("Invalid Byno", "By Amount");
        this.objDblRateByAmtLoad.byno = '';
      }
    }
  }

  private checkValid(): boolean {
    debugger;
    let index = this.doubleRateByAmountList.findIndex(x => x.byno.toString().trim().toUpperCase() === this.objDblRateByAmtLoad.inv_byno.toString().trim().toUpperCase());
    if (index !== -1)
      return true;
    else
      return false;
  }

  private byNoWithoutSlash() {
    debugger;
    this.objDblRateByAmtLoad.byno = this.objDblRateByAmtLoad.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objDblRateByAmtLoad.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objDblRateByAmtLoad.byno.substr(9, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objDblRateByAmtLoad.byno.substr(13, 3);
    this.getDblRateByAmounttList();
  }

  private OldByNoWithoutSlash() {
    debugger;
    this.objDblRateByAmtLoad.byno = this.objDblRateByAmtLoad.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objDblRateByAmtLoad.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objDblRateByAmtLoad.byno.substr(8, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objDblRateByAmtLoad.byno.substr(11, 3);
    // console.log(this.ForCheckBynoData);
    this.getDblRateByAmounttList();
  }

  private autoScannerCall() {
    debugger;
    this.ForCheckBynoData.Inv_Byno = this.objDblRateByAmtLoad.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = this.objDblRateByAmtLoad.byno.substr(8, 4);
    this.ForCheckBynoData.Byno_Prod_Serial = this.objDblRateByAmtLoad.byno.substr(11, 4);
    // this.objSingleProductLoad.byno = '';
    this.getDblRateByAmounttList();
  }

  private splitByNumber() {
    debugger;
    this.objDblRateByAmtLoad.byno = this.objDblRateByAmtLoad.byno.toString().trim().toUpperCase();
    var x = this.objDblRateByAmtLoad.byno.split("/");
    this.Inv_Byno = x[0];
    this.Byno_Serial = x[1];
    this.Byno_Prod_Serial = [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(x[2]) !== -1 ? "" : x[2];
    if (this.Byno_Serial.length == 2) {
      this.Byno_Serial = "00".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 2) {
      this.Byno_Prod_Serial = "00".concat(this.Byno_Prod_Serial);
    }
    if (this.Byno_Serial.length == 1) {
      this.Byno_Serial = "000".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 1) {
      this.Byno_Prod_Serial = "000".concat(this.Byno_Prod_Serial);
    } if (this.Byno_Serial.length == 0) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Invalid Byno', 'By Amount');
      return;
    } if (this.Byno_Prod_Serial.length == 0) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Invalid Byno', 'By Amount');
      return;
    }
    this.ForCheckBynoData.Inv_Byno = this.Inv_Byno.toString().trim();
    this.ForCheckBynoData.Byno_Serial = this.Byno_Serial.toString().trim();
    this.ForCheckBynoData.Byno_Prod_Serial = this.Byno_Prod_Serial.toString().trim();
    this.getDblRateByAmounttList();
  }

}
