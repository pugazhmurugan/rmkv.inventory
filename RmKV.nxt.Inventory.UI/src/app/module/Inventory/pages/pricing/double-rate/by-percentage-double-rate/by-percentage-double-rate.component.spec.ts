import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByPercentageDoubleRateComponent } from './by-percentage-double-rate.component';

describe('ByPercentageDoubleRateComponent', () => {
  let component: ByPercentageDoubleRateComponent;
  let fixture: ComponentFixture<ByPercentageDoubleRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByPercentageDoubleRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByPercentageDoubleRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
