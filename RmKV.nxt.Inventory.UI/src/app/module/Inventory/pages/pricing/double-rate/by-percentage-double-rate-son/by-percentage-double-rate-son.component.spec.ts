import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByPercentageDoubleRateSonComponent } from './by-percentage-double-rate-son.component';

describe('ByPercentageDoubleRateSonComponent', () => {
  let component: ByPercentageDoubleRateSonComponent;
  let fixture: ComponentFixture<ByPercentageDoubleRateSonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByPercentageDoubleRateSonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByPercentageDoubleRateSonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
