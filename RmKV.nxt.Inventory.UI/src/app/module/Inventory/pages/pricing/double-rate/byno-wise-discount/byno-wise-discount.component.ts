import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ForCheckBynoData } from 'src/app/module/Inventory/model/common.model';
import { ByAmountService } from 'src/app/module/Inventory/services/pricing/selling-price-change/single-product/double-rate/by-amount/by-amount.service';
import { SingleProductService } from 'src/app/module/Inventory/services/pricing/selling-price-change/single-product/single-product.service';
declare var jsPDF: any;

@Component({
  selector: 'app-byno-wise-discount',
  templateUrl: './byno-wise-discount.component.html',
  styleUrls: ['./byno-wise-discount.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class BynoWiseDiscountComponent implements OnInit {
  objDblRateByDiscount: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    discount_percent: 0,
    company_section_name: this._localStorage.getCompanySectionName()
  }

  objUnChangedDblRateByDiscount: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    discount_percent: 0,
    company_section_name: this._localStorage.getCompanySectionName()
  }

  bynoWiseChangeList: any = []

  objBynoWiseChange: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    product_name: '',
    counter_code: '',
    selling_price: 0,
    discount_percent: 0,
    discount_rate: 0
  }

  ForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  UnChangedForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  Inv_Byno: string = "";
  Byno_Serial: string = "";
  Byno_Prod_Serial: string = "";
  keyUpEvent: any;

  columns: any[] = [
    { display: 'sNo', editable: false },
    { display: 'byno', editable: true },
    { display: 'discountPercent', editable: true },
    { display: 'discountAmount', editable: true },
    { display: 'action', editable: true },
  ];

  @ViewChildren('discountPercent') discountPercent: ElementRef | any;
  @ViewChildren('discountAmount') discountAmount: ElementRef | any;
  @ViewChildren('action') action: ElementRef | any;
  @ViewChildren('byno') byno: ElementRef | any;

  constructor(
    public _router: Router,
    public _localStorage: LocalStorage,
    private _matDialog: MatDialog,
    private _datePipe: DatePipe,
    private _decimalPipe: DecimalPipe,
    private _excelService: ExcelService,
    private _gridKeyEvents: GridKeyEvents,
    private _keyPressEvents: KeyPressEvents,
    private _byAmountService: ByAmountService,
    private _singleProductService: SingleProductService,
    private _confirmationDialog: ConfirmationDialogComponent,
  ) {
    this.addNewRow();
  }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        let response = this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.bynoWiseChangeList);
        if (response)
          this.addEmptyRow(rowIndex);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.bynoWiseChangeList);
        break;

      case 38: // Arrow Up
        debugger;
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right\
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.bynoWiseChangeList);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.bynoWiseChangeList);
        break;
    }
  }

  ngOnInit() {
  }

  /*************************************************** CRUD Operations ***************************************************/

  public getDblRateByMarginList(i: number): void {
    // if (this.beforeLoadValidate()) {
    let objGet: any = {
      SingleProduct: JSON.stringify([{
        inv_byno: this.ForCheckBynoData.Inv_Byno.toString().trim().toUpperCase(),
        byno_serial: this.ForCheckBynoData.Byno_Serial.toString().trim().toUpperCase(),
        byno_prod_serial: this.ForCheckBynoData.Byno_Prod_Serial.toString().trim().toUpperCase(),
        company_section_id: this._localStorage.getCompanySectionId()
      }])
    }
    this._singleProductService.getSingleProduct(objGet).subscribe((result: any) => {
      if (result) {
        console.log(result, 'By Margin Details List');
        this.bynoWiseChangeList[i] = JSON.parse(JSON.stringify(result))[0];
        this.bynoWiseChangeList[i].discount_percent = this.objDblRateByDiscount.discount_percent;
        this.bynoWiseChangeList[i].discount_rate = Math.round((+this.bynoWiseChangeList[i].selling_price) - (+this.bynoWiseChangeList[i].selling_price * (this.objDblRateByDiscount.discount_percent / 100)));
        this.checkExistByno(i);

      }
    });
    // }
  }

  public addNewRow(): void {
    this.bynoWiseChangeList.push(Object.assign({}, this.objBynoWiseChange));
    setTimeout(() => {
      let input = this.byno.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100);
  }

  public addEmptyRow(i: number): void {
    if (this.beforeAddNewRowValidate(i)) {
      this.bynoWiseChangeList.push(Object.assign({}, this.objBynoWiseChange));
      setTimeout(() => {
        let input = this.byno.toArray();
        input[input.length - 1].nativeElement.focus();
      }, 100);
    }
  }

  private beforeAddNewRowValidate(i: number): boolean {
    if (this.bynoWiseChangeList.length > 0 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.bynoWiseChangeList[i].byno.toString().trim()) !== -1) {
      this.openAlertDialogByno(i, 'Enter byno');
      return false;
    } if (this.bynoWiseChangeList.length > 0 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.bynoWiseChangeList[i].byno.toString().trim()) === -1 &&
      this.bynoWiseChangeList.length > 0 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.bynoWiseChangeList[i].product_name.toString().trim()) !== -1 &&
      this.bynoWiseChangeList.length > 0 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.bynoWiseChangeList[i].selling_price.toString().trim()) !== -1) {
      this.openAlertDialogByno(i, 'Invalid byno');
      return false;
    } else return true;
  }

  public removeRows(i: number): void {
    this.openConfirmationDialog('Are you sure want to remove this byno ' + this.bynoWiseChangeList[i].byno + ' from the list', 'Bynowise Discount', i);
  }

  public calculateDiscountRate(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      if (this.bynoWiseChangeList.length > 0) {
        this.bynoWiseChangeList.forEach((x, i) => {
          if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(x.selling_price) === -1) {
            x.discount_percent = +this.objDblRateByDiscount.discount_percent;
            x.discount_rate = +x.selling_price - Math.round((+x.selling_price * (+x.discount_percent / 100)));
          }
        });
      }
    }
  }

  public calculateDiscountRatePerRow(data: any): void {
    debugger;
    if (this.bynoWiseChangeList.length > 0) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(data.selling_price) === -1) {
        // data.discount_percent = +data.discount_percent;
        data.discount_rate = +data.selling_price - Math.round((+data.selling_price * (+data.discount_percent / 100)));
      }
    }
  }

  public addBynoWiseDisount(): void {
    if (this.beforeSaveValidate()) {
      debugger;
      let tempArr: any = [];
      for (let i = 0; i < this.bynoWiseChangeList.length; i++) {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.bynoWiseChangeList[i].byno) === -1 &&
          [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.bynoWiseChangeList[i].product_name) === -1 &&
          [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.bynoWiseChangeList[i].selling_price) === -1 &&
          [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.bynoWiseChangeList[i].discount_rate) === -1) {
          tempArr.push({
            inv_byno: this.bynoWiseChangeList[i].inv_byno,
            byno_serial: this.bynoWiseChangeList[i].byno_serial,
            byno_prod_serial: this.bynoWiseChangeList[i].byno_prod_serial,
            old_selling_price: +this.bynoWiseChangeList[i].selling_price,
            new_selling_price: +this.bynoWiseChangeList[i].discount_rate,
            selling_price: +this.bynoWiseChangeList[i].selling_price,
            entered_by: this._localStorage.intGlobalUserId(),
            company_section_id: this._localStorage.getCompanySectionId(),
            double_rate: +this.bynoWiseChangeList[i].selling_price,
            discount_rate: +this.bynoWiseChangeList[i].discount_rate,
            add_margin: 0,
            less_margin: +this.bynoWiseChangeList[i].discount_percent,
            aadi_pricecode: ''
          });
        }
      }
      let objAdd: any = {
        ByPercentage: JSON.stringify(tempArr)
      }
      this._byAmountService.addByPercentage(objAdd).subscribe((result: boolean) => {
        if (result)
          this.openPrintAlertDialog('Saved successfully', tempArr);
      })
    }
  }

  public openConfirmationDialog(message: string, componentName: string, i: number): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = componentName;
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deleteRow(i);
      dialogRef = null;
    });
  }

  private deleteRow(i: number) {
    this.bynoWiseChangeList.splice(i, 1);
    setTimeout(() => {
      let inputEls = this.byno.toArray();
      inputEls[i].nativeElement.focus();
    }, 100);
  }

  /****************************************************** Validations ********************************************************/

  private beforeSaveValidate(): boolean {
    if (this.bynoWiseChangeList.length === 1 && [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.bynoWiseChangeList[0].byno) !== -1 &&
      [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.bynoWiseChangeList[0].product_name) !== -1 &&
      [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.bynoWiseChangeList[0].selling_price) !== -1) {
      this.openAlertDialogByno(0, 'No records to save');
      return false;
    } if (!this.checkValidDiscount()) {
      this._confirmationDialog.openAlertDialog('Enter atleast one discount percent / amount', 'Byno wise Discount');
      return false;
    } else return true;
  }

  private checkValidDiscount(): boolean {
    let i = this.bynoWiseChangeList.findIndex(x => +x.discount_percent !== 0);
    return i !== -1 ? true : false;
  }

  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public onlyAllowInputDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  openPrintAlertDialog(meassge: any, data: any[]) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = meassge;
    _dialogRef.componentInstance.componentName = "By Amount";
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.openPrintConfirmationDialog(data);
      _dialogRef = null;
    });
  }

  public openPrintConfirmationDialog(data: any[]): any {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    _dialogRef.componentInstance.confirmMessage = "Do you want to print this record ?"
    _dialogRef.componentInstance.componentName = "Byno wise discount";
    return _dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.print(data);
      else {
        this.resetScreen();
      }

      _dialogRef = null;
    });
  }

  private print(data: any[]) {
    this.resetScreen();
  }

  private resetScreen(): void {
    this.bynoWiseChangeList = [];
    this.ForCheckBynoData = JSON.parse(JSON.stringify(this.UnChangedForCheckBynoData));
    this.objDblRateByDiscount = JSON.parse(JSON.stringify(this.objUnChangedDblRateByDiscount));
    this.addNewRow();
  }

  /***************************************************** ByNo Validations *****************************************************/

  public onKeyPress(keyUpEvent: any, i: number) {
    debugger;
    this.resetRow(i, true);
    if (keyUpEvent.keyCode === 13 && this.bynoWiseChangeList[i].byno === '') {
      this.openAlertDialogByno(i, 'Enter byno');
      // this._confirmationDialog.openAlertDialog('Enter byno', 'By Amount');
    } else if (keyUpEvent.keyCode === 13 && this.bynoWiseChangeList[i].byno.length == 16 && !this.bynoWiseChangeList[i].byno.includes("/")) {
      this.byNoWithoutSlash(i);
    } else if (keyUpEvent.keyCode === 13 && this.bynoWiseChangeList[i].byno.length == 28 && !this.bynoWiseChangeList[i].byno.includes("/")) {
      this.OldByNoWithoutSlash(i);
    } else if (keyUpEvent.keyCode === 13 && this.bynoWiseChangeList[i].byno.length == 30 && !this.bynoWiseChangeList[i].byno.includes("/")) {
      this.byNoWithoutSlash(i);
    } else if (keyUpEvent.keyCode === 13 && this.bynoWiseChangeList[i].byno.toString().trim().includes("/")) {
      debugger
      this.splitByNumber(i);
    } else if (this.bynoWiseChangeList[i].byno.length >= 28 && keyUpEvent.keyCode === 13) {
      this.autoScannerCall(i);
    } else {
      if (keyUpEvent.keyCode === 13 && (this.bynoWiseChangeList[i].byno.toString().trim().includes('/') && this.bynoWiseChangeList[i].byno.split('/').length !== 3)) {
        this.openAlertDialogByno(i, 'Invalid byno');
        this.bynoWiseChangeList[i].byno = '';
      } else if (keyUpEvent.keyCode === 13 && !this.bynoWiseChangeList[i].byno.toString().trim().includes("/") && this.bynoWiseChangeList[i].byno.length > 16) {
        this.openAlertDialogByno(i, 'Invalid byno');
      } else if (keyUpEvent.keyCode === 13 && !this.bynoWiseChangeList[i].byno.toString().trim().includes('/')) {
        this.openAlertDialogByno(i, 'Invalid byno');
        this.bynoWiseChangeList[i].byno = '';
      }
    }
  }

  private byNoWithoutSlash(i: number) {
    debugger;
    this.bynoWiseChangeList[i].byno = this.bynoWiseChangeList[i].byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.bynoWiseChangeList[i].byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.bynoWiseChangeList[i].byno.substr(9, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.bynoWiseChangeList[i].byno.substr(13, 3);
    this.getDblRateByMarginList(i);
  }

  private OldByNoWithoutSlash(i: number) {
    debugger;
    this.bynoWiseChangeList[i].byno = this.bynoWiseChangeList[i].byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.bynoWiseChangeList[i].byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.bynoWiseChangeList[i].byno.substr(8, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.bynoWiseChangeList[i].byno.substr(11, 3);
    // console.log(this.ForCheckBynoData);
    this.getDblRateByMarginList(i);
  }

  private autoScannerCall(i: number) {
    debugger;
    this.ForCheckBynoData.Inv_Byno = this.bynoWiseChangeList[i].byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = this.bynoWiseChangeList[i].byno.substr(8, 4);
    this.ForCheckBynoData.Byno_Prod_Serial = this.bynoWiseChangeList[i].byno.substr(11, 4);
    // this.objSingleProductLoad.byno = '';
    this.getDblRateByMarginList(i);
  }

  private splitByNumber(i: number) {
    debugger;
    this.bynoWiseChangeList[i].byno = this.bynoWiseChangeList[i].byno.toString().trim().toUpperCase();
    var x = this.bynoWiseChangeList[i].byno.split("/");
    this.Inv_Byno = x[0];
    this.Byno_Serial = x[1];
    this.Byno_Prod_Serial = [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(x[2]) !== -1 ? "" : x[2];
    if (this.Byno_Serial.length == 2) {
      this.Byno_Serial = "00".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 2) {
      this.Byno_Prod_Serial = "00".concat(this.Byno_Prod_Serial);
    }
    if (this.Byno_Serial.length == 1) {
      this.Byno_Serial = "000".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 1) {
      this.Byno_Prod_Serial = "000".concat(this.Byno_Prod_Serial);
    } if (this.Byno_Serial.length == 0) {
      this.openAlertDialogByno(i, 'Invalid byno');
      return;
    } if (this.Byno_Prod_Serial.length == 0) {
      this.openAlertDialogByno(i, 'Invalid byno');
      return;
    }
    this.ForCheckBynoData.Inv_Byno = this.Inv_Byno.toString().trim();
    this.ForCheckBynoData.Byno_Serial = this.Byno_Serial.toString().trim();
    this.ForCheckBynoData.Byno_Prod_Serial = this.Byno_Prod_Serial.toString().trim();
    this.getDblRateByMarginList(i);
  }

  private checkExistByno(i: number) {
    let already = false;
    let index = 0;
    let byno = '';
    for (let j = 0; (j < this.bynoWiseChangeList.length && j !== i); j++) {
      if (this.bynoWiseChangeList[j].byno === this.bynoWiseChangeList[i].byno) {
        already = true;
        index = j + 1;
        byno = this.bynoWiseChangeList[i].byno;
        break;
      }
    }
    if (already) {
      // this.inputEls = this.byno.toArray();
      // this.inputEls[i].nativeElement.focus();
      this.resetRow(i);
      this.openAlertDialogByno(i, "This Byno " + " " + byno + " Already Available In Row No" + " " + index + "");
    } else this.addNewRow();
  }

  private resetRow(i: number, isByNo?: boolean): void {
    if (!isByNo)
      this.bynoWiseChangeList[i].byno = '';
    this.bynoWiseChangeList[i].product_name = '';
    this.bynoWiseChangeList[i].counter_code = '';
    this.bynoWiseChangeList[i].selling_price = 0;
    this.bynoWiseChangeList[i].discount_percent = 0;
    this.bynoWiseChangeList[i].discount_rate = 0;
  }
  private openAlertDialogByno(i: number, message: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = message;
    _dialogRef.componentInstance.componentName = "Byno";
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this.byno.toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.bynoWiseChangeList.length > 0 && [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.bynoWiseChangeList[0].byno) === -1 &&
      [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.bynoWiseChangeList[0].product_name) === -1 && [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.bynoWiseChangeList[0].selling_price) === -1) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.bynoWiseChangeList.length; i++) {
        if ([null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.bynoWiseChangeList[i].byno) === -1 &&
          [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.bynoWiseChangeList[i].product_name) === -1 &&
          [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.bynoWiseChangeList[i].selling_price) === -1) {
          json[i] = Object.assign({
            "ByNo": this.bynoWiseChangeList[i].byno,
            "Product Name": this.bynoWiseChangeList[i].product_name,
            "Counter": this.bynoWiseChangeList[i].counter_code,
            "Current Price": this.bynoWiseChangeList[i].selling_price,
            "Discount %": this.bynoWiseChangeList[i].discount_percent,
            "Discount Amount": this.bynoWiseChangeList[i].discount_rate,
          });
        }
      }
      this._excelService.exportAsExcelFile(json, "Byno_wise_discount", datetime);
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "Byno wise discount");
  }

  public exportToPdf(): void {
    if (this.bynoWiseChangeList.length > 0 && [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.bynoWiseChangeList[0].byno) === -1 &&
      [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.bynoWiseChangeList[0].product_name) === -1 && [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.bynoWiseChangeList[0].selling_price) === -1) {
      var prepare = [];
      this.bynoWiseChangeList.forEach(e => {
        if ([null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(e.byno) === -1 &&
          [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(e.product_name) === -1 &&
          [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(e.selling_price) === -1) {
          var tempObj = [];
          tempObj.push(e.byno);
          tempObj.push(e.product_name);
          tempObj.push(e.counter_code);
          tempObj.push(e.selling_price);
          tempObj.push(e.discount_percent);
          tempObj.push(e.discount_rate);
          prepare.push(tempObj);
        }
      });

      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["ByNo", "Product Name", "Counter", "Current Price", "Discount %", "Discount Amount"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('Byno_wise_discount' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "Byno wise discount");
  }
}
