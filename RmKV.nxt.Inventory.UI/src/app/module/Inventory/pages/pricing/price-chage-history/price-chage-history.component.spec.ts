import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceChageHistoryComponent } from './price-chage-history.component';

describe('PriceChageHistoryComponent', () => {
  let component: PriceChageHistoryComponent;
  let fixture: ComponentFixture<PriceChageHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceChageHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceChageHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
