import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { SingleProductService } from '../../../services/pricing/selling-price-change/single-product/single-product.service';

@Component({
  selector: 'app-price-chage-history',
  templateUrl: './price-chage-history.component.html',
  styleUrls: ['./price-chage-history.component.scss'],
  providers: [DatePipe]
})
export class PriceChageHistoryComponent implements OnInit {

  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
   
  }

  fromDate1: any = new Date();
  toDate1: any = new Date();
  sectionsList: any;
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  constructor(public _localStorage: LocalStorage, 
    public _pricechangereportdervice: SingleProductService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _commonService: CommonService,
    private _datePipe: DatePipe,
    public _router: Router) {
      this.load.FromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
     }

  ngOnInit() {
    this.getSections();
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }
  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }

  private getSections(): void {
    debugger;
    let objSections = {
      GetAllCompanySection: JSON.stringify([{
        warehouse_id: this._localStorage.getGlobalWarehouseId(),
        sales_location_id: this._localStorage.getGlobalSalesLocationId()
      }])
    }
    this._commonService.getWarehouseBasedCompanySection(objSections).subscribe((result: string) => {
      if (result && JSON.parse(result))
        this.sectionsList = JSON.parse(result);
      // this.load.company_section_id = this.sectionsList[0].company_section_id;

    });
  }

  public generateReport(): void {
    debugger;
          let objForm = {
          SingleProduct: JSON.stringify([{
          from_date: this.load.FromDate,
          to_date: this.load.ToDate,
          company_section_id: +this._localStorage.getCompanySectionId(),
          // warehouse_id: +this._localStorage.getGlobalWarehouseId(),

          // wh_section_id: this.sectionsList.filter(x => +x.company_section_id == +this._localStorage.getCompanySectionId())[0].wh_section_id,

        }])
      }                               
      this._pricechangereportdervice.getPriceChangeReport(objForm).subscribe((result: any) => {
        if (result.size != 0) {
          let dataType = result.type;
          const file = new Blob([result], { type: dataType });
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL);
        } else
          this._confirmationDialogComponent.openAlertDialog('No records found', 'Price Change History')
      });
  }

}
