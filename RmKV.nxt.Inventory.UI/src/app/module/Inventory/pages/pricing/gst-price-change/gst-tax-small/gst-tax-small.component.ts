import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-gst-tax-small',
  templateUrl: './gst-tax-small.component.html',
  styleUrls: ['./gst-tax-small.component.scss']
})
export class GstTaxSmallComponent implements OnInit {

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
