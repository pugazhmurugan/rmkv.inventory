import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-gsttax',
  templateUrl: './gsttax.component.html',
  styleUrls: ['./gsttax.component.scss']
})
export class GSTTaxComponent implements OnInit {

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }
  ngOnInit() {
  }

}
