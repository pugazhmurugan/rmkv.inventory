import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GstTaxMaterialComponent } from './gst-tax-material.component';

describe('GstTaxMaterialComponent', () => {
  let component: GstTaxMaterialComponent;
  let fixture: ComponentFixture<GstTaxMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GstTaxMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GstTaxMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
