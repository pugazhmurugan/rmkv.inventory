import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GstTaxSmallComponent } from './gst-tax-small.component';

describe('GstTaxSmallComponent', () => {
  let component: GstTaxSmallComponent;
  let fixture: ComponentFixture<GstTaxSmallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GstTaxSmallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GstTaxSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
