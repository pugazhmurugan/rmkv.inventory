import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-gst-tax-material',
  templateUrl: './gst-tax-material.component.html',
  styleUrls: ['./gst-tax-material.component.scss']
})
export class GstTaxMaterialComponent implements OnInit {

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
