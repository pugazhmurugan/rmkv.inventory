import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GSTTaxComponent } from './gsttax.component';

describe('GSTTaxComponent', () => {
  let component: GSTTaxComponent;
  let fixture: ComponentFixture<GSTTaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GSTTaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GSTTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
