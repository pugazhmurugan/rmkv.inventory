import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ForCheckBynoData } from '../../../model/common.model';
import { MRPChangeService } from '../../../services/pricing/selling-price-change/single-product/mrp-change/mrpchange.service';
import { SingleProductService } from '../../../services/pricing/selling-price-change/single-product/single-product.service';

@Component({
  selector: 'app-mrp-change',
  templateUrl: './mrp-change.component.html',
  styleUrls: ['./mrp-change.component.scss']
})
export class MrpChangeComponent implements OnInit {

  objMrpChange: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    product_name: '',
    uom_description: '',
    prod_pcs: 0,
    prod_qty: 0,
    mrp_amount: 0,
    hsncode: '',
    tax: 0,
    remarks: ''
  }

  objUnChangedMrpChange: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    product_name: '',
    uom_description: '',
    prod_pcs: 0,
    prod_qty: 0,
    mrp_amount: 0,
    hsncode: '',
    tax: 0,
    remarks: ''
  }

  ForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  UnChangedForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  Inv_Byno: string = "";
  Byno_Serial: string = "";
  Byno_Prod_Serial: string = "";
  keyUpEvent: any;

  constructor(
    public _router: Router,
    public _localStorage: LocalStorage,
    private _matDialog: MatDialog,
    private _keyPressEvents: KeyPressEvents,
    private _mrpChangesService: MRPChangeService,
    private _singleProductService: SingleProductService,
    private _confirmationDialog: ConfirmationDialogComponent,
  ) { }

  ngOnInit() {
  }

  /*************************************************** CRUD Operations ***************************************************/
  public getMRPDetailsForByno(): void {
    if (this.beforeLoadValidate()) {
      let objGet: any = {
        SingleProduct: JSON.stringify([{
          inv_byno: this.ForCheckBynoData.Inv_Byno.toString().trim().toUpperCase(),
          byno_serial: this.ForCheckBynoData.Byno_Serial.toString().trim().toUpperCase(),
          byno_prod_serial: this.ForCheckBynoData.Byno_Prod_Serial.toString().trim().toUpperCase(),
          company_section_id: this._localStorage.getCompanySectionId()
        }])
      }
      this._singleProductService.getSingleProduct(objGet).subscribe((result: any) => {
        if (result) {
          console.log(result, 'MRP Change Details List');
          this.objMrpChange = JSON.parse(JSON.stringify(result))[0];
          this.objMrpChange.remarks = '';
          this.objMrpChange.mrp_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objMrpChange.mrp_amount) !== -1 ? 0 : this.objMrpChange.mrp_amount;
        } else {
          this.objMrpChange.byno = '';
          this._confirmationDialog.openAlertDialog('No record found', 'MRP Change');
        }
      });
    }
  }

  public addMRPChanges(): void {
    if (this.beforeSaveValidate()) {
      let objAdd = {
        MRPChange: JSON.stringify([{
          inv_byno: this.objMrpChange.inv_byno,
          byno_serial: this.objMrpChange.byno_serial,
          type: 'M',
          mrp: +this.objMrpChange.mrp_amount,
          remarks: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objMrpChange.remarks.toString().trim()) !== -1 ? '' : this.objMrpChange.remarks.toString().trim(),
          entered_by: +this._localStorage.intGlobalUserId(),
          company_section_id: +this._localStorage.getCompanySectionId()
        }])
      }
      this._mrpChangesService.addMRPChanges(objAdd).subscribe((result: boolean) => {
        if (result) {
          this._confirmationDialog.openAlertDialog('MRP changed for ' + this.objMrpChange.byno + ' successfully', 'MRP Change');
          this.resetScreen();
        }
      })
    }
  }

  /**************************************************** Validations ****************************************************/

  private beforeLoadValidate(): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objMrpChange.byno) !== -1) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Enter byno', 'MRP Change');
      return false;
    } else return true;
  }

  private beforeSaveValidate(): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objMrpChange.byno) !== -1) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Enter byno', 'MRP Change');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objMrpChange.byno) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objMrpChange.product_name) !== -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objMrpChange.hsn_code) !== -1) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Invalid byno', 'MRP Change');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.objMrpChange.mrp_amount) !== -1) {
      document.getElementById('mrp').focus();
      this._confirmationDialog.openAlertDialog('Enter mrp', 'MRP Change');
      return false;
    } if (+this.objMrpChange.mrp_amount < 0) {
      document.getElementById('mrp').focus();
      this._confirmationDialog.openAlertDialog('Invalid mrp', 'MRP Change');
      return false;
    } else return true;
  }

  private resetRow(isByNo?: boolean): void {
    if (!isByNo)
      this.objMrpChange.byno = '';
    this.objMrpChange.product_name = '';
    this.objMrpChange.uom_description = '';
    this.objMrpChange.prod_pcs = 0;
    this.objMrpChange.prod_qty = 0;
    this.objMrpChange.mrp_amount = 0;
    this.objMrpChange.tax = 0;
    this.objMrpChange.hsncode = '';
  }

  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  private resetScreen(): void {
    this.objMrpChange = JSON.parse(JSON.stringify(this.objUnChangedMrpChange));
  }

  private checkAnyChangesMade(): boolean {
    this.objMrpChange.byno = this.objMrpChange.byno.toString().trim();
    this.objMrpChange.remarks = this.objMrpChange.remarks.toString().trim();
    if (JSON.stringify(this.objMrpChange) !== JSON.stringify(this.objUnChangedMrpChange))
      return true;
    else return false;
  }

  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade()) {
      this.openConfirmationDialog(exitFlag ? 'Changes will be lost, are you sure?' : 'Are you sure, want to clear all the changes?', exitFlag);
    } else if (exitFlag)
      this._router.navigate([this._localStorage.getMenuPath()]);
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = 'MRP Changes';
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this._router.navigate([this._localStorage.getMenuPath()]) : this.resetScreen();
      dialogRef = null;
    });
  }

  /***************************************************** ByNo Validations *****************************************************/

  public onKeyPress(keyUpEvent: any) {
    debugger;
    this.resetRow(true);
    if (keyUpEvent.keyCode === 13 && this.objMrpChange.byno === '') {
      document.getElementById("byno").focus();
      this._confirmationDialog.openAlertDialog('Enter byno', 'MRP Change');
    } else if (keyUpEvent.keyCode === 13 && this.objMrpChange.byno.length == 16 && !this.objMrpChange.byno.includes("/")) {
      this.byNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objMrpChange.byno.length == 28 && !this.objMrpChange.byno.includes("/")) {
      this.OldByNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objMrpChange.byno.length == 30 && !this.objMrpChange.byno.includes("/")) {
      this.byNoWithoutSlash();
    } else if (keyUpEvent.keyCode === 13 && this.objMrpChange.byno.includes("/")) {
      debugger
      this.splitByNumber();
    } else if (this.objMrpChange.byno.length >= 28 && keyUpEvent.keyCode === 13) {
      this.autoScannerCall();
    } else {
      if (keyUpEvent.keyCode === 13 && !this.objMrpChange.byno.includes("/") && this.objMrpChange.byno.length > 16) {
        document.getElementById('byno').focus();
        this._confirmationDialog.openAlertDialog("Invalid Byno", "MRP Change");
      }
      //  else if (keyUpEvent.keyCode === 13 && !this.checkValid()) {
      //   document.getElementById('byno').focus();
      //   this._confirmationDialog.openAlertDialog("Invalid Byno", "MRP Change");
      //   this.objMrpChange.byno = '';
      // }
    }
  }
  private byNoWithoutSlash() {
    debugger;
    this.objMrpChange.byno = this.objMrpChange.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objMrpChange.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objMrpChange.byno.substr(9, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objMrpChange.byno.substr(13, 3);
    this.getMRPDetailsForByno();
  }

  private OldByNoWithoutSlash() {
    debugger;
    this.objMrpChange.byno = this.objMrpChange.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objMrpChange.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objMrpChange.byno.substr(8, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objMrpChange.byno.substr(11, 3);
    // console.log(this.ForCheckBynoData);
    this.getMRPDetailsForByno();
  }

  private autoScannerCall() {
    debugger;
    this.ForCheckBynoData.Inv_Byno = this.objMrpChange.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = this.objMrpChange.byno.substr(8, 4);
    this.ForCheckBynoData.Byno_Prod_Serial = this.objMrpChange.byno.substr(11, 4);
    // this.objSingleProductLoad.byno = '';
    this.getMRPDetailsForByno();
  }

  private splitByNumber() {
    debugger;
    this.objMrpChange.byno = this.objMrpChange.byno.toString().trim().toUpperCase();
    var x = this.objMrpChange.byno.split("/");
    this.Inv_Byno = x[0];
    this.Byno_Serial = x[1];
    this.Byno_Prod_Serial = [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(x[2]) !== -1 ? "" : x[2];
    if (this.Byno_Serial.length == 2) {
      this.Byno_Serial = "00".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 2) {
      this.Byno_Prod_Serial = "00".concat(this.Byno_Prod_Serial);
    }
    if (this.Byno_Serial.length == 1) {
      this.Byno_Serial = "000".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 1) {
      this.Byno_Prod_Serial = "000".concat(this.Byno_Prod_Serial);
    } if (this.Byno_Serial.length == 0) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Invalid Byno', 'MRP Change');
      return;
    } if (this.Byno_Prod_Serial.length == 0) {
      document.getElementById('byno').focus();
      this._confirmationDialog.openAlertDialog('Invalid Byno', 'MRP Change');
      return;
    }
    this.ForCheckBynoData.Inv_Byno = this.Inv_Byno.toString().trim();
    this.ForCheckBynoData.Byno_Serial = this.Byno_Serial.toString().trim();
    this.ForCheckBynoData.Byno_Prod_Serial = this.Byno_Prod_Serial.toString().trim();
    this.getMRPDetailsForByno();
  }

}
