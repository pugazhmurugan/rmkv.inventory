import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MrpChangeComponent } from './mrp-change.component';

describe('MrpChangeComponent', () => {
  let component: MrpChangeComponent;
  let fixture: ComponentFixture<MrpChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MrpChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MrpChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
