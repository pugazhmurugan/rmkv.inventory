import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostPriceModificationComponent } from './cost-price-modification.component';

describe('CostPriceModificationComponent', () => {
  let component: CostPriceModificationComponent;
  let fixture: ComponentFixture<CostPriceModificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostPriceModificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostPriceModificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
