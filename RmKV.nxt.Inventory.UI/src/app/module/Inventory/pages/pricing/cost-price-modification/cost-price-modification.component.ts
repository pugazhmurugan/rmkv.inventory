import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-cost-price-modification',
  templateUrl: './cost-price-modification.component.html',
  styleUrls: ['./cost-price-modification.component.scss']
})
export class CostPriceModificationComponent implements OnInit {

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
