import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatGridTileHeaderCssMatStyler } from '@angular/material';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { PurchaseReturnDcService } from '../../../services/purchase-return/purchase-return-dc/purchase-return-dc.service';
declare var jsPDF: any;

@Component({
  selector: 'app-purchase-return-dc',
  templateUrl: './purchase-return-dc.component.html',
  styleUrls: ['./purchase-return-dc.component.scss'],
  providers: [DatePipe]
})
export class PurchaseReturnDcComponent implements OnInit {

  loadPurchaseReturnDC: any = {
    from_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    to_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  objPurchaseReturnDC: any = {
    date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    supplier_gstn: '',
    city: '',
    dc: '',
    supplier_invoice_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    address1: '',
    trading_drcr_no: ''
  }

  unChangedPurchaseReturnDC: any = {
    date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    supplier_code: '',
    supplier_name: '',
    supplier_address: '',
    supplier_gstn: '',
    city: '',
    dc: '',
    supplier_invoice_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    address1: '',
    trading_drcr_no: ''
  }
  validDcNo: boolean = false;
  loadPurchaseReturnDCList: any[] = [];
  componentVisibility: boolean = true;
  getBynoGridList: any[] = [];
  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    private _datePipe: DatePipe,
    public _commonService: CommonService,
    private _confirmationDialog: ConfirmationDialogComponent,
    private _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    private _matDialog: MatDialog,
    public _excelService: ExcelService,
    public _keyPressEvents: KeyPressEvents,
    public _purchaseReturnDCService: PurchaseReturnDcService) {
    this.loadPurchaseReturnDC.from_date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 2)));
  }

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  public dateValidation: DateValidation = new DateValidation();
  fromDate1: any = new Date();
  toDate1: any = new Date();
  dcDate1: any = new Date();

  public validateFromDate(): void {
    let date = this.dateValidation.validateDate(this.loadPurchaseReturnDC.from_date, this.fromDate1, this.minDate, this.maxDate);
    this.loadPurchaseReturnDC.from_date = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.loadPurchaseReturnDC.to_date, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.loadPurchaseReturnDC.to_date = date[0];
    this.toDate1 = date[1];
  }

  ngOnInit() {

  }

  public onViewClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.resetScreen();
  }

  private emptyFromToDate(): boolean {
    if (this.loadPurchaseReturnDC.from_date.toString() === "") {
      document.getElementById("fromDate").focus();
      this._confirmationDialog.openAlertDialog('Enter from date', 'Purchase Return(vapas)DC');
      return false;
    } else if (this.loadPurchaseReturnDC.from_date.toString().length !== 10) {
      document.getElementById("fromDate").focus();
      this._confirmationDialog.openAlertDialog('Enter valid from date', 'Purchase Return(vapas)DC');
      this.loadPurchaseReturnDC.from_date = "";
      return false;
    } else if (this.loadPurchaseReturnDC.to_date.toString() === "") {
      document.getElementById("toDate").focus();
      this._confirmationDialog.openAlertDialog('Enter to date', 'Purchase Return(vapas)DC');
      return false;
    } else if (this.loadPurchaseReturnDC.to_date.toString().length !== 10) {
      document.getElementById("toDate").focus();
      this._confirmationDialog.openAlertDialog('Enter valid to date', 'Purchase Return(vapas)DC');
      this.loadPurchaseReturnDC.to_date = "";
      return false;
    } else if (this.toDate1 < this.fromDate1) {
      document.getElementById("toDate").focus();
      this._confirmationDialog.openAlertDialog('To date cannot be less than from date', 'Purchase Return(vapas)DC');
      this.toDate1 = this.fromDate1;
      return false;
    } else {
      return true;
    }
  }

  public loadPurchaseReturnDCDetails(): void {
    debugger;
    if (this.emptyFromToDate()) {
      let objLoad = {
        LoadReturnDC: JSON.stringify([{
          company_section_id: +this._localStorage.getCompanySectionId(),
          from_date: this.loadPurchaseReturnDC.from_date,
          to_date: this.loadPurchaseReturnDC.to_date
        }])
      }
      this._purchaseReturnDCService.getPurchaseReturnDCList(objLoad).subscribe((result: any) => {
        debugger;
        if (result !== null) {
          this.loadPurchaseReturnDCList = result;
          console.log(this.loadPurchaseReturnDCList, 'load')
        } else
          this._confirmationDialog.openAlertDialog("No record found", "Purchase Return(vapas)DC");
      });
    }
  }

  public viewPurchaseReturnDCDetails(index: number): void {
    debugger;
    this.componentVisibility = !this.componentVisibility;
    if (this.emptyFromToDate()) {
      let objLoad = {
        FetchReturnDC: JSON.stringify([{
          company_section_id: +this._localStorage.getCompanySectionId(),
          purchase_return_date: this.loadPurchaseReturnDCList[index].purchase_return_date,
          supplier_code: this.loadPurchaseReturnDCList[index].supplier_code,
          dc_no: this.loadPurchaseReturnDCList[index].dc_no
        }])
      }
      this._purchaseReturnDCService.fetchPurchaseReturnDCDetails(objLoad).subscribe((result: any) => {
        debugger;
        if (result !== null) {
          this.objPurchaseReturnDC = result[0];
          this.getBynoGridList = result;
          this.checkIsValidDCNO();
          console.log(result, 'viewData');
        }
      });
    }
  }

  public checkIsValidDCNO(): void {
    this.validDcNo = this.objPurchaseReturnDC.trading_drcr_no.toString().trim() === '' ? false : true;
  }

  public GeneratePurchaseReturnDCNo(): void {
    debugger;
    var date = '';
    var code = '';
    for (let i = 0; i < this.loadPurchaseReturnDCList.length; i++) {
      date = this.loadPurchaseReturnDCList[i].purchase_return_date;
      code = this.loadPurchaseReturnDCList[i].supplier_code;
    }
    let objData = {
      GenerateDCNo: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        purchase_return_date: date,
        supplier_code: code
      }]),
    }
    this._purchaseReturnDCService.generatePurchaseReturnDCNo(objData).subscribe((result: any) => {
      if (result) {
        this.openAlertDialog("New dc no is generated", "Purchase Return(vapas)DC");
      }
    });
  }

  openAlertDialog(value: string, componentName?: string) {
    //  this._matDialog.closeAll();
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    this._dialogRef.componentInstance.alertMessage = value;
    this._dialogRef.componentInstance.componentName = componentName;
    this._dialogRef.afterClosed().subscribe(result => {
      this._dialogRef = null;
    });
  }

  public onClearView(): void {
    this.componentVisibility = !this.componentVisibility;
  }

  public onChangeFromToDate() : any  {
    this.loadPurchaseReturnDCList = [];
  }

  private resetScreen() : void {
    this.objPurchaseReturnDC = JSON.parse(JSON.stringify(this.unChangedPurchaseReturnDC));
    this.getBynoGridList = [];
    this.validDcNo = !this.validDcNo;
  }

  public exportToPdf(): void {
    if (this.loadPurchaseReturnDCList.length != 0) {
      var prepare = [];
      this.loadPurchaseReturnDCList.forEach(e => {
        var tempObj = [];
        tempObj.push(e.purchase_return_date, e.supplier_code, e.supplier_name, e.supplier_city, e.notecount,
          e.total_pieces,e.credit_amount,e.debit_amount, e.dc_no);
        prepare.push(tempObj);
      });
      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [['Date', 'Supplier Code', 'Supplier Name', 'Supplier City', 'No of Notes', 'Total Pieces',  'Credit Amount',  'Debit Amount', 'DC No']],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('Purchase Return(vapas)DC' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No record found, Load the data first", "Purchase Return(vapas)DC");
  }

  public exportToExcel(): void {
    debugger
    if (this.loadPurchaseReturnDCList.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const commissionsArr = [];
      for (let i = 0; i < this.loadPurchaseReturnDCList.length; i++) {
        commissionsArr[i] = Object.assign({
          'Date': this.loadPurchaseReturnDCList[i].purchase_return_date,
          'Supplier Code': this.loadPurchaseReturnDCList[i].supplier_code,
          'Supplier Name': this.loadPurchaseReturnDCList[i].supplier_name,
          'Supplier City': this.loadPurchaseReturnDCList[i].supplier_city,
          'No of Notes': this.loadPurchaseReturnDCList[i].notecount,
          'Total Pieces': this.loadPurchaseReturnDCList[i].total_pieces,
          'Credit Amount': this.loadPurchaseReturnDCList[i].credit_amount,
          'Debit Amount': this.loadPurchaseReturnDCList[i].debit_amount,
          'DC No': this.loadPurchaseReturnDCList[i].dc_no
        });
      }
      this._excelService.exportAsExcelFile(commissionsArr, "Purchase Return(vapas)DC", datetime);
    } else
      this._confirmationDialog.openAlertDialog("No record found, Load the data first", "Purchase Return(vapas)DC");
  }

}
