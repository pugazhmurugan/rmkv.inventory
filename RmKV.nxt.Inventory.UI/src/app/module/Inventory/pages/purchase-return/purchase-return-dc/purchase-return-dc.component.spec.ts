import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseReturnDcComponent } from './purchase-return-dc.component';

describe('PurchaseReturnDcComponent', () => {
  let component: PurchaseReturnDcComponent;
  let fixture: ComponentFixture<PurchaseReturnDcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseReturnDcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseReturnDcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
