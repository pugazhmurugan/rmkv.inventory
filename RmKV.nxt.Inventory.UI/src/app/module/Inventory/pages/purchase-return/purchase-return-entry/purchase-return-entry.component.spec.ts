import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseReturnEntryComponent } from './purchase-return-entry.component';

describe('PurchaseReturnEntryComponent', () => {
  let component: PurchaseReturnEntryComponent;
  let fixture: ComponentFixture<PurchaseReturnEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseReturnEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseReturnEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
