import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { EditMode } from 'src/app/common/models/common-model';
import { EditableGridComponent } from 'src/app/common/pages/editable-grid/editable-grid.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { IndexedDBCommonFN } from 'src/app/common/shared/IndexedDB/indexed_db_common';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { PurchaseReturnEntryService } from '../../../services/purchase-return/purchase-return-entry/purchase-return-entry.service';
declare var jsPDF: any;
@Component({
  selector: 'app-purchase-return-entry',
  templateUrl: './purchase-return-entry.component.html',
  styleUrls: ['./purchase-return-entry.component.scss'],
  providers: [DatePipe]
})
export class PurchaseReturnEntryComponent implements OnInit {
  componentVisibility: boolean = true;

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };
  Date: any = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  objLoad: any = {
    section: this._localStorage.getCompanySectionName(),
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  objPurchaseReturnEntry: any = {
    Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
  }

  fromDate1: any = new Date();
  toDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();
  @ViewChild(MatSort, null) sort: MatSort;
  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "purchase_return_date", "byno", "product_code", "product_name", "uom_descrition", "qty", "pcs", "Action"];
  public commonIndexed: IndexedDBCommonFN = new IndexedDBCommonFN(this.dbService, this._localStorage);

  @ViewChild(EditableGridComponent, null) byno_data;

  constructor(public _localStorage: LocalStorage, private _purchaseReturnEntryService: PurchaseReturnEntryService,
    public _router: Router, private _datePipe: DatePipe, public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _matDialog: MatDialog, private dbService: NgxIndexedDBService,public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _confirmationDialogComponent: ConfirmationDialogComponent,public _excelService: ExcelService,) {
    this.showTable = false;
    this.objLoad.FromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
  }

  ngOnInit() {
    this.showTable = true;
  }

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  showTable: boolean = false;

  objEditableGrid = {
    tabledata: [],
    obj: {
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0
      , uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: 0.00, hsncode: '', gst: 0, unique_byno_prod_serial: null
    },
    flags: { isProductCode: true, isProductName: true, isUOM: true, isSellingPrice: false, isDisable: false },
    schema_name: 'purchaseReturn'
  }

  objGodown = [{
    byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
    uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
    isDelete: false, valid: false
  }]

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.objLoad.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.objLoad.FromDate = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.objLoad.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoad.ToDate = date[0];
    this.toDate1 = date[1];
  }

  /************************************************ CRUD OPERATION ************************************************/

  GetPurchaseReturnEntry(): void {
    if (this.beforeobjLoadValidate()) {
      this.matTableConfig([]);
      let objData = {
        PurchaseReturnEntry: JSON.stringify([{
          from_date: this.objLoad.FromDate,
          to_date: this.objLoad.ToDate,
          company_section_id: +this._localStorage.getCompanySectionId(),
        }]),
      }
      this._purchaseReturnEntryService.GetPurchaseReturn(objData).subscribe((result: any) => {
        debugger;
        if (result) {
          console.log(result,"purchase list")
          this.matTableConfig(result);
        } else
          this._confirmationDialogComponent.openAlertDialog('No records found', 'Purchase Return Entry');
      });
    }
  }

  public matTableConfig(tableRecords: any[]): void {
    if (tableRecords) {
      this.dataSource = new MatTableDataSource(tableRecords);
      this.dataSource.sort = this.sort;
    }
    else {
      this.dataSource = new MatTableDataSource([]);
      this.dataSource.sort = this.sort;
    }
  }

  public addPurchaseReturn(): void {
    if (this.beforeSaveValidate()) {
      debugger
      for (let i = 0; i < this.objEditableGrid.tabledata.length; i++) {
        if (this.objEditableGrid.tabledata[i].product_code == '') {
          this.objEditableGrid.tabledata.splice(i, 1);
        }
      }
      let objDetails = [];
      for (let i = 0; i < this.byno_data.objEditableGrid.tabledata.length; i++) {
        objDetails.push(Object.assign({
          serial_no: i + 1,
          byno: this.byno_data.objEditableGrid.tabledata[i].byno,
          inv_byno: this.byno_data.objEditableGrid.tabledata[i].inv_byno,
          byno_serial: this.byno_data.objEditableGrid.tabledata[i].byno_serial,
          byno_prod_serial: this.byno_data.objEditableGrid.tabledata[i].byno_prod_serial,
          // uom_id: this.byno_data.objEditableGrid.tabledata[i].uom_id,
          // uom_descrition: this.byno_data.objEditableGrid.tabledata[i].uom_descrition,
          return_qty: this.byno_data.objEditableGrid.tabledata[i].prod_qty,
          return_pcs: this.byno_data.objEditableGrid.tabledata[i].prod_pcs,
          return_quantity: this.byno_data.objEditableGrid.tabledata[i].prod_qty,
          // selling_price: this.byno_data.objEditableGrid.tabledata[i].selling_price,
          // hsncode: this.byno_data.objEditableGrid.tabledata[i].hsncode,
          // gst: this.byno_data.objEditableGrid.tabledata[i].gst,
          // unique_byno_prod_serial: this.byno_data.objEditableGrid.tabledata[i].unique_byno_prod_serial,
        }));
      }
      let objData = {
        PurchaseReturnEntry: JSON.stringify([{
          return_date: this.objPurchaseReturnEntry.Date,
          // transfer_id: +this.objGodownToCounter.Transfer_No,
          // wh_section_id: +this._localStorage.getWhSectionId(),
          // destination_id: +this.objGodownToCounter.To_Location,
          // remarks: this.objGodownToCounter.Remarks.toString().trim(),
          entered_by: +this._localStorage.intGlobalUserId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          details: objDetails,

        }]),
      }
      this._purchaseReturnEntryService.AddPurchaseReturn(objData).subscribe((result: any) => {
        if (result) {
          this.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New Transfer No Is :" + " " + result + "");
          this.GetPurchaseReturnEntry();
          this.componentVisibility = !this.componentVisibility;
        }
      });
    }
  }

  public onClickDelete(index: any): any {
    this.dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this.dialogRef.componentInstance.confirmMessage =
      "Do you want to cancel of '" + this.dataSource.data[index].inv_byno + "'";
    this.dialogRef.componentInstance.componentName = "Purchase Return Entry";
    return this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.delete(index);
      }
      this.dialogRef = null;
    });
  }

  private delete(row: any): void {
    debugger;
    let tempSaveArr = [];
    let objData = {
      return_date: this.objPurchaseReturnEntry.Date,
        inv_byno: this.dataSource.data[row].inv_byno,
        byno_serial:this.dataSource.data[row].byno_serial,
        byno_prod_serial: this.dataSource.data[row].byno_prod_serial,
        company_section_id: +this._localStorage.getCompanySectionId(),
    }
    tempSaveArr.push(objData);
    let objCancel = {
      PurchaseReturnEntry: JSON.stringify(tempSaveArr)
    }
    this._purchaseReturnEntryService.CancelPurchaseReturn(objCancel).subscribe(
      (result: any) => {
        if (result.error) {
          this._confirmationDialogComponent.openAlertDialog(result.error, "Invoice Register");
        } else if (result) {
          this._confirmationDialogComponent.openAlertDialog("Cancelled", "Invoice Register");
        } else {
          this._confirmationDialogComponent.openAlertDialog(result.error, "Invoice Register");
        }
        this.GetPurchaseReturnEntry();
      });
    this.dialogRef = null;
  }

  /************************************************ Validations ************************************************/

  private beforeSaveValidate(): boolean {
    if (this.byno_data.objEditableGrid.tabledata[0].product_code == '' && this.byno_data.objEditableGrid.tabledata[0].byno == '') {
      let input = this.byno_data.byno.toArray();
      input[0].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Byno", "Purchase Return Entry");
      return false;
    } else
      return true;
  }

  openAlertDialog(value: string, componentName?: string) {
    //  this._matDialog.closeAll();
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    this._dialogRef.componentInstance.alertMessage = value;
    this._dialogRef.componentInstance.componentName = componentName;
    this._dialogRef.afterClosed().subscribe(result => {
      this._dialogRef = null;
    });
  }

  private beforeobjLoadValidate(): boolean {
    debugger;
    if ([null, undefined, 0, ""].indexOf(this.objLoad.FromDate) !== -1) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Select from date", "Purchase Return Entry");
      return false;
    } else if (this.objLoad.FromDate.length != 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Invalid from date", "Purchase Return Entry");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objLoad.ToDate) !== -1) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Select todate", "Purchase Return Entry");
      return false;
    } else if (this.objLoad.ToDate.length != 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Invalid todate", "Purchase Return Entry");
      return false;
    } else {
      return true;
    }
  }

  resetScreen(exitFlag) {
    if (!this.objEditableGrid.flags.isDisable) {
      if (JSON.stringify(this.byno_data.objEditableGrid.tabledata) != JSON.stringify(this.objGodown)) {
        this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag)
      } else if (exitFlag)
        this.onListClick();
    } else
      this.onListClick();
  }

  private openConfirmationDialog(message: string, exitFlag): void {
    debugger;
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Purchase Return Entry";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.onClear();
    });
  }
  public newClick(): void {
    this.objEditableGrid.flags.isDisable = false;
    this.objEditableGrid.tabledata = [];
    this.componentVisibility = !this.componentVisibility;
    this.objAction.isEditing = false;
  }

  public onListClick(): void {
    this.componentVisibility = !this.componentVisibility;
  }

  public onClear(): void {
    this.byno_data.objEditableGrid.tabledata = [];
    this.byno_data.objEditableGrid.tabledata.push({
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
      uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
      isDelete: false, valid: false
    });
    this.objAction.isEditing = false;
    this.commonIndexed.clearCommonIndexedData(this.objEditableGrid.schema_name);

  }

   /*************************************************** Exports ************************************************/

   public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Scanned Date": this._datePipe.transform(this.dataSource.data[i].purchase_return_date, 'dd/MM/yyyy'),
          "Byno": this.dataSource.data[i].byno,
          "Product Code": this.dataSource.data[i].product_code,
          "Product Name": this.dataSource.data[i].product_name,
          "UOM": this.dataSource.data[i].uom_descrition,
          "Quantity": this.dataSource.data[i].qty,
          "Pieces": this.dataSource.data[i].pcs,
        });
      }
      this._excelService.exportAsExcelFile(json, "Purchase Return Entry", datetime);
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found", "Purchase Return Entry");
  }
  
  exportToPdf(): void {
  
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(this._datePipe.transform(e.purchase_return_date, 'dd/MM/yyyy'));
        tempObj.push(e.byno);
        tempObj.push(e.product_code);
        tempObj.push(e.product_name);
        tempObj.push(e.uom_descrition);
        tempObj.push(e.qty);
        tempObj.push(e.pcs);
        prepare.push(tempObj);
      });
  
      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Scanned Date","Byno","Product Code","Product Name", "UOM","Quantity","Pieces"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
  
      doc.save('Purchase Return Entry' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Purchase Return Entry");
  }

}
