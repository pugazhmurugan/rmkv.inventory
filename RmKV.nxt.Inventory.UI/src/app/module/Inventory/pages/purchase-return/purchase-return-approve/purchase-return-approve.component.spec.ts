import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseReturnApproveComponent } from './purchase-return-approve.component';

describe('PurchaseReturnApproveComponent', () => {
  let component: PurchaseReturnApproveComponent;
  let fixture: ComponentFixture<PurchaseReturnApproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseReturnApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseReturnApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
