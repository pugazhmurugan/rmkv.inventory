import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatDialogRef, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { PurchaseReturnApproveService } from '../../../services/purchase-return/purchase-return-entry/purchase-return-approve/purchase-return-approve.service';
declare var jsPDF: any;

@Component({
  selector: 'app-purchase-return-approve',
  templateUrl: './purchase-return-approve.component.html',
  styleUrls: ['./purchase-return-approve.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class PurchaseReturnApproveComponent implements OnInit {

  allSelected: boolean;
  componentVisibility: boolean = true;

  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Return_Date", "Byno", "Supplier_Name", "Supplier_City", "Product_code", "Quantity", "Pieces", "Action"];

  objReturnLoad = {
    From_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    To_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    SupplierCityList: [],
    SupplierGroupList: [],
    SupplierNameList: [],
    citySelectAll: false,
    groupSelectAll: false,
    nameSelectAll: false,
  }

  public dateValidation: DateValidation = new DateValidation();
  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  model1: any = new Date();
  model2: any = new Date();

  objLoad = {
    Section: this._localStorage.getCompanySectionName(),
    Section_Id: +this._localStorage.getCompanySectionId(),
    Warehouse_Id: +this._localStorage.getGlobalWarehouseId(),
    Supplier_Code: ''
  }

  supplierList: any[];
  purchaseReturnApprovedList: any[];
  approvalList: any[];
  SelectApprovalList: any[];

  @ViewChildren("group") group: ElementRef | any;
  @ViewChildren("name") name: ElementRef | any;
  @ViewChildren("city") city: ElementRef | any;
  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    private _datePipe: DatePipe,
    public _excelService: ExcelService,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    private _purchaseReturnApproveService: PurchaseReturnApproveService,
    private _confirmationDialog: ConfirmationDialogComponent,
    private _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _decimalPipe: DecimalPipe
  ) { }

  public validateFromDate() {
    let date = this.dateValidation.validateDate(this.objReturnLoad.From_Date, this.model1, this.minDate, this.maxDate);
    this.objReturnLoad.From_Date = date[0];
    this.model1 = date[1];
  }

  public validateToDate() {
    let date = this.dateValidation.validateDate(this.objReturnLoad.To_Date, this.model2, this._datePipe.transform(this.model1, 'dd/MM/yyyy'), this.maxDate);
    this.objReturnLoad.To_Date = date[0];
    this.model2 = date[1];
  }

  ngOnInit() {
    this.getSupplier();
    this.model1.setDate(this.model2.getDate() - 2);
    this.objReturnLoad.From_Date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
    this.getSupplierCity();
  }

  private getSupplier(): void {
    let objSupplier = {
      PRApprove: JSON.stringify([{
        warehouse_id: +this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._purchaseReturnApproveService.getApprovedSupplier(objSupplier).subscribe((result: any[]) => {
      if (result)
        this.supplierList = result;
      result.length == 1 ? this.objLoad.Supplier_Code = this.supplierList[0].supplier_code : '';
      this.getPurchaseReturnApprovedList();
    });
  }

  public getPurchaseReturnApprovedList(): void {
    if (this.objLoad.Supplier_Code !== '') {
      this.matTableConfig([]);
      let objData = {
        PRApprove: JSON.stringify([{
          supplier_code: this.objLoad.Supplier_Code,
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
        }]),
      }
      this._purchaseReturnApproveService.getPurchaseRetunApprovedList(objData).subscribe((result: any) => {
        if (result) {
          this.purchaseReturnApprovedList = result;
          this.matTableConfig(result);
        } else
          this._confirmationDialogComponent.openAlertDialog('No records found', 'Purchase Return Approve');
      });
    }
  }

  public matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }

  public getQuantity(): number {
    if (this.dataSource.data !== undefined)
      return this.dataSource.data.map(t => +t.return_quantity).reduce((acc, value) => acc + value, 0);
  }

  public getPcs(): number {
    if (this.dataSource.data !== undefined)
      return this.dataSource.data.map(t => +t.return_pcs).reduce((acc, value) => acc + value, 0);
  }

  public onRemovePruchaseReturnList(data: any): any {
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this._dialogRef.componentInstance.confirmMessage =
      "Do you want to remove this row?"
    this._dialogRef.componentInstance.componentName = "Purchase Return Approve";
    return this._dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.removePurchaseReturnList(data);
      }
      this._dialogRef = null;
    });
  }

  public removePurchaseReturnList(data: any): void {
    let objData = {
      PRApprove: JSON.stringify([{
        approval_date: this._datePipe.transform(data.approved_date, 'dd/MM/yyyy'),
        inv_byno: data.inv_byno,
        byno_serial: data.byno_serial,
        byno_prod_serial: data.byno_prod_serial,
        warehouse_id: +this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
      }]),
    }
    this._purchaseReturnApproveService.removPurchaseReturn(objData).subscribe((result: any) => {
      debugger
      if (result) {
        this._confirmationDialog.openAlertDialog("Removed", "Purchase Return Approve");
        this.getPurchaseReturnApprovedList();
      }
    });
  }

  public getSupplierCity(): void {
    if (this.objReturnLoad.From_Date.length == 10 && this.objReturnLoad.To_Date.length == 10) {
      let objSupplier = {
        PRApprove: JSON.stringify([{
          from_date: this.objReturnLoad.From_Date,
          to_date: this.objReturnLoad.To_Date,
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
        }])
      }
      this._purchaseReturnApproveService.getSupplierCityForApprove(objSupplier).subscribe((result: any[]) => {
        if (result)
          this.objReturnLoad.SupplierCityList = result;
      });
    }
  }

  public getSupplierGroup(): void {
    if (this.getSelectedCities().length !== 0) {
      let objSupplier = {
        PRApprove: JSON.stringify([{
          from_date: this.objReturnLoad.From_Date,
          to_date: this.objReturnLoad.To_Date,
          supplier_city: this.getSelectedCities(),
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
        }])
      }
      this._purchaseReturnApproveService.getSupplierGroupNameForApprove(objSupplier).subscribe((result: any[]) => {
        if (result)
          for (let i = 0; i < result.length; i++) {
            this.objReturnLoad.SupplierGroupList.push({
              is_selected: false,
              supplier_group: result[i].supplier_group,
              supplier_group_id: result[i].supplier_group_id
            })
          }
      });
    }
  }

  public getSupplierName(): void {
    if (this.getSelectedCities().length !== 0 && this.getSelectedGroups().length !== 0) {
      let objSupplier = {
        PRApprove: JSON.stringify([{
          from_date: this.objReturnLoad.From_Date,
          to_date: this.objReturnLoad.To_Date,
          supplier_city: this.getSelectedCities(),
          supplier_group_id: this.getSelectedGroups(),
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
        }])
      }
      this._purchaseReturnApproveService.getSupplierNameForApprove(objSupplier).subscribe((result: any[]) => {
        if (result)
          for (let i = 0; i < result.length; i++) {
            this.objReturnLoad.SupplierNameList.push({
              is_selected: false,
              supplier_account_name: result[i].supplier_account_name,
              supplier_code: result[i].supplier_code,
              supplier_name: result[i].supplier_name
            })
          }
      });
    }
  }

  public onSelectCityAll() {
    for (let i = 0; i < this.objReturnLoad.SupplierCityList.length; i++) {
      this.objReturnLoad.SupplierCityList[i].is_selected = this.objReturnLoad.citySelectAll;
    }
    this.onChangeSupplierCity();
    this.getSupplierGroup()
  }

  public onSelectGroupAll() {
    for (let i = 0; i < this.objReturnLoad.SupplierGroupList.length; i++) {
      this.objReturnLoad.SupplierGroupList[i].is_selected = this.objReturnLoad.groupSelectAll;
    }
    this.onChangeSupplierGroup();
    this.getSupplierName()
  }

  public onSelectNameAll() {
    for (let i = 0; i < this.objReturnLoad.SupplierNameList.length; i++) {
      this.objReturnLoad.SupplierNameList[i].is_selected = this.objReturnLoad.nameSelectAll;
    }
    this.onChangeSupplierName();
    this.getAprovalList()
  }

  public citySelectAll() {
    this.objReturnLoad.citySelectAll = this.objReturnLoad.SupplierCityList.every(function (item: any) {
      return item.isSelected == true;
    })
  }

  public cityunSelectAll() {
    let cityArr = [];
    this.objReturnLoad.SupplierCityList.forEach(x => {
      if (x.is_selected) {
        cityArr.push({
          supplier_city: x.supplier_city
        });
      }
    });
    if (this.objReturnLoad.SupplierCityList.length == cityArr.length) {
      this.objReturnLoad.citySelectAll = true;
    }
  }

  public groupunSelectAll() {
    let groupArr = [];
    this.objReturnLoad.SupplierGroupList.forEach(x => {
      if (x.is_selected) {
        groupArr.push({
          supplier_group_id: x.supplier_group_id
        });
      }
    });
    if (this.objReturnLoad.SupplierGroupList.length == groupArr.length) {
      this.objReturnLoad.groupSelectAll = true;
    }
  }

  public nameunSelectAll() {
    let supplierArr: any = []
    this.objReturnLoad.SupplierNameList.forEach(x => {
      if (x.is_selected) {
        supplierArr.push({
          supplier_code: x.supplier_code
        });
      }
    });
    if (this.objReturnLoad.SupplierNameList.length == supplierArr.length) {
      this.objReturnLoad.nameSelectAll = true;
    }
    this.approvalList = [];
  }

  public groupSelectAll() {
    this.objReturnLoad.groupSelectAll = this.objReturnLoad.SupplierGroupList.every(function (item: any) {
      return item.isSelected == true;
    })
  }

  public nameSelectAll() {
    this.objReturnLoad.nameSelectAll = this.objReturnLoad.SupplierNameList.every(function (item: any) {
      return item.isSelected == true;
    })
    this.nameunSelectAll();
  }

  private getSelectedCities(): any {
    let cityArr: any = [];
    this.objReturnLoad.SupplierCityList.forEach(x => {
      if (x.is_selected) {
        cityArr.push({
          supplier_city: x.supplier_city
        });
      }
    });
    return cityArr;
  }

  private getSelectedGroups(): any {
    let groupArr: any = []
    this.objReturnLoad.SupplierGroupList.forEach(x => {
      if (x.is_selected) {
        groupArr.push({
          supplier_group_id: x.supplier_group_id
        });
      }
    });
    return groupArr;
  }

  private getSelectedSuppliers(): any {
    let supplierArr: any = []
    this.objReturnLoad.SupplierNameList.forEach(x => {
      if (x.is_selected) {
        supplierArr.push({
          supplier_code: x.supplier_code
        });
      }
    });
    return supplierArr;
  }

  public getAprovalList(): void {
    if (this.getSelectedCities().length !== 0 && this.getSelectedGroups().length !== 0 && this.getSelectedSuppliers().length !== 0) {
      let objSupplier = {
        PRApprove: JSON.stringify([{
          from_date: this.objReturnLoad.From_Date,
          to_date: this.objReturnLoad.To_Date,
          supplier_city: this.getSelectedCities(),
          supplier_group_id: this.getSelectedGroups(),
          supplier_code: this.getSelectedSuppliers(),
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
        }])
      }
      this._purchaseReturnApproveService.geSupplierDetailsForApprove(objSupplier).subscribe((result: any[]) => {
        if (result) {
          this.approvalList = result;
          console.log(result, 'dsfdt List');
          this.approvalList.forEach(x => {
            x.cost_price = x.cost_price + '.00'
          });
        }
        else
          this._confirmationDialogComponent.openAlertDialog('No records found', 'Purchase Return Approve');
      });
    }
  }

  public getApprovalQty(): number {
    return this.approvalList.map(t => +t.quantity).reduce((acc, value) => acc + value, 0);
  }

  public getApprovalCostPrice(): string {
    let price = this.approvalList.map(t => +t.cost_price).reduce((acc, value) => acc + value, 0).toString();
    return price + '.00'
  }

  public checkUncheckAll() {
    for (var i = 0; i < this.approvalList.length; i++) {
      this.approvalList[i].isSelected = this.allSelected;
    }
    this.getCheckedItemList();
  }

  public isAllSelected() {
    this.allSelected = this.approvalList.every(function (item: any) {
      return item.isSelected == true;
    })
    this.getCheckedItemList();
  }

  private getCheckedItemList() {
    let tempApproveList = [];
    for (var i = 0; i < this.approvalList.length; i++) {
      if (this.approvalList[i].isSelected)
        tempApproveList.push(this.approvalList[i]);
    }
    this.SelectApprovalList = tempApproveList;
    console.log(this.SelectApprovalList, 'AP');
  }

  private beforeApproveValidate(): boolean {

    let index = this.objReturnLoad.SupplierCityList.findIndex(x => x.is_selected !== false)
    let index1 = this.objReturnLoad.SupplierGroupList.findIndex(x => x.is_selected !== false)
    let index2 = this.objReturnLoad.SupplierNameList.findIndex(x => x.is_selected !== false)

    if (index == -1) {
      this._confirmationDialog.openAlertDialog("select supplier city", "Purchase Return Approve");
    } else if (index1 == -1) {
      this._confirmationDialog.openAlertDialog("select supplier group", "Purchase Return Approve");
    } else if (index2 == -1) {
      this._confirmationDialog.openAlertDialog("select supplier name", "Purchase Return Approve");
    } else if (this.SelectApprovalList == undefined || this.SelectApprovalList.length == 0) {
      this._confirmationDialog.openAlertDialog("Select Byno", "Purchase Return Approve");
      return false;
    } else {
      return true;
    }

  }

  public onApproveByno(): any {
    if (this.beforeApproveValidate()) {
      this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
        panelClass: "custom-dialog-container",
        data: { confirmationDialog: 1 }
      });
      this._dialogRef.componentInstance.confirmMessage =
        "Do you want to approve  byno ?"
      this._dialogRef.componentInstance.componentName = "Purchase Return Approve";
      return this._dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.approveByno();
        }
        this._dialogRef = null;
      });
    }
  }

  public approveByno(): void {
    let tempApproveList = [];
    for (let i = 0; i < this.SelectApprovalList.length; i++) {
      tempApproveList.push(Object.assign({
        serial_no: i + 1,
        inv_byno: this.SelectApprovalList[i].inv_byno,
        byno_serial: this.SelectApprovalList[i].byno_serial,
        byno_prod_serial: this.SelectApprovalList[i].byno_prod_serial,
        return_qty: this.SelectApprovalList[i].qty,
        return_pcs: this.SelectApprovalList[i].pcs,
        return_quantity: this.SelectApprovalList[i].quantity,
        payment_decision_status: this.SelectApprovalList[i].payment_decision_status,
      }))
    }
    let objData = {
      PRApprove: JSON.stringify([{
        details: tempApproveList,
        warehouse_id: +this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        entered_by: +this._localStorage.intGlobalUserId()
      }]),
    }
    this._purchaseReturnApproveService.approvePurchaseReturnList(objData).subscribe((result: any) => {
      debugger
      if (result) {
        this._confirmationDialog.openAlertDialog("Approved", "Purchase Return Approve");
        this.onClear();
      }
    });
  }

  newClick(): void {
    this.onClear();
    this.componentVisibility = !this.componentVisibility;
  }

  onList(): void {
    this.componentVisibility = !this.componentVisibility;
    this.objReturnLoad.SupplierGroupList = [];
    this.objReturnLoad.SupplierNameList = [];
    this.approvalList = [];
  }

  onClear(): void {
    this.objReturnLoad.SupplierGroupList = [];
    this.objReturnLoad.SupplierNameList = [];
    this.approvalList = [];
    this.objReturnLoad.citySelectAll = false;
    this.objReturnLoad.groupSelectAll = false;
    this.objReturnLoad.nameSelectAll = false;
    this.allSelected = false;
    for (let i = 0; i < this.objReturnLoad.SupplierCityList.length; i++) {
      this.objReturnLoad.SupplierCityList[i].is_selected = false;
    }
  }

  onChangeSupplierCity() {
    this.objReturnLoad.SupplierGroupList = [];
    this.objReturnLoad.SupplierNameList = [];
    this.approvalList = [];
    this.objReturnLoad.groupSelectAll = false;
  }

  onChangeSupplierGroup() {
    this.objReturnLoad.SupplierNameList = [];
    this.approvalList = [];
    this.objReturnLoad.nameSelectAll = false;
  }

  onChangeSupplierName() {
    this.approvalList = [];
  }

  resetScreen(exitFlag) {
    let index = 1;
    for (let i = 0; i < this.objReturnLoad.SupplierCityList.length; i++) {
      if (this.objReturnLoad.SupplierCityList[i].is_selected == true) {
        index = 2;
      }
    }
    if (index == 2) {
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag)
      return
    } else if (exitFlag)
      this.onListClick();
  }

  onListClick() {
    this.componentVisibility = !this.componentVisibility;
  }

  private openConfirmationDialog(message: string, exitFlag): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Purchase Return Approve";
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        exitFlag ? this.onListClick() : this.onClear();
      }
    });
  }

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Approved Date': this.dataSource.data[i].approved_date,
          'Byno': this.dataSource.data[i].byno,
          'Supplier Name': this.dataSource.data[i].supplier_name,
          "Supplier City": this.dataSource.data[i].supplier_city,
          "Product Code": this.dataSource.data[i].product_code,
          "Quantity": this.dataSource.data[i].return_quantity,
          "Pcs": this.dataSource.data[i].return_pcs,
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Purchase Return Approve",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Purchase Return Approve");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(this._datePipe.transform(e.approved_date, 'dd/MM/yyyy'));
        tempObj.push(e.byno);
        tempObj.push(e.supplier_name);
        tempObj.push(e.supplier_city);
        tempObj.push(e.product_code);
        tempObj.push(e.return_quantity);
        tempObj.push(e.return_pcs);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Approved Date', 'Byno', 'Supplier Name', 'Supplier City', 'Product Code', 'Quantity', 'Pcs']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Purchase Return Approve' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Purchase Return Approve");
  }

}
