import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierwiseCreditNoteComponent } from './supplierwise-credit-note.component';

describe('SupplierwiseCreditNoteComponent', () => {
  let component: SupplierwiseCreditNoteComponent;
  let fixture: ComponentFixture<SupplierwiseCreditNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierwiseCreditNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierwiseCreditNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
