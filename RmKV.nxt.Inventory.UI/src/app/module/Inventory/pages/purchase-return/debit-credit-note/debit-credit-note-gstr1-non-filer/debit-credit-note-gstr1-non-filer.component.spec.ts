import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebitCreditNoteGstr1NonFilerComponent } from './debit-credit-note-gstr1-non-filer.component';

describe('DebitCreditNoteGstr1NonFilerComponent', () => {
  let component: DebitCreditNoteGstr1NonFilerComponent;
  let fixture: ComponentFixture<DebitCreditNoteGstr1NonFilerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebitCreditNoteGstr1NonFilerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebitCreditNoteGstr1NonFilerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
