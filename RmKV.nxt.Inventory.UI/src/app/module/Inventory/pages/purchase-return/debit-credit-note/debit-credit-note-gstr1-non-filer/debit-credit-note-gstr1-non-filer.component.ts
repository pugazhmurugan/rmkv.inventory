import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-debit-credit-note-gstr1-non-filer',
  templateUrl: './debit-credit-note-gstr1-non-filer.component.html',
  styleUrls: ['./debit-credit-note-gstr1-non-filer.component.scss'],
  providers: [DatePipe]
})
export class DebitCreditNoteGstr1NonFilerComponent implements OnInit {

  constructor(public _localStorage: LocalStorage,
    public _router: Router, private _datePipe: DatePipe,)  { }

  Date: any = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  ngOnInit() {
  }

}
