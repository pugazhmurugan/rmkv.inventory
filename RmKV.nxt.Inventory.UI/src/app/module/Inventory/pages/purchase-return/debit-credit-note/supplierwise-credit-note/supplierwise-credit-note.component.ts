import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-supplierwise-credit-note',
  templateUrl: './supplierwise-credit-note.component.html',
  styleUrls: ['./supplierwise-credit-note.component.scss'],
  providers: [DatePipe]
})
export class SupplierwiseCreditNoteComponent implements OnInit {

 
  constructor(public _localStorage: LocalStorage,
    public _router: Router, private _datePipe: DatePipe,)  { }

  Date: any = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  public dateValidation: DateValidation = new DateValidation();
  
  ngOnInit() {
  }

}
