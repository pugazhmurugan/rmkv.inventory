import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-packing-slip',
  templateUrl: './packing-slip.component.html',
  styleUrls: ['./packing-slip.component.scss']
})
export class PackingSlipComponent implements OnInit {

  constructor(public _localStorage: LocalStorage,
    public _router: Router,) { }

  ngOnInit() {
  }

}
