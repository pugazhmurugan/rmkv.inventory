import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseReturnNoteReportsComponent } from './purchase-return-note-reports.component';

describe('PurchaseReturnNoteReportsComponent', () => {
  let component: PurchaseReturnNoteReportsComponent;
  let fixture: ComponentFixture<PurchaseReturnNoteReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseReturnNoteReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseReturnNoteReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
