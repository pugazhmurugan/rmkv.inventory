import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradingDrCrReportsComponent } from './trading-dr-cr-reports.component';

describe('TradingDrCrReportsComponent', () => {
  let component: TradingDrCrReportsComponent;
  let fixture: ComponentFixture<TradingDrCrReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradingDrCrReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradingDrCrReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
