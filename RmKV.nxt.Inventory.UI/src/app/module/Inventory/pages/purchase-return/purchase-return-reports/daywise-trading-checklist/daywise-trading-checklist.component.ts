import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-daywise-trading-checklist',
  templateUrl: './daywise-trading-checklist.component.html',
  styleUrls: ['./daywise-trading-checklist.component.scss'],
  providers: [DatePipe]
})
export class DaywiseTradingChecklistComponent implements OnInit {

  constructor(public _localStorage: LocalStorage,
    public _router: Router, private _datePipe: DatePipe,) { }

  Date: any = new Date();

  ngOnInit() {
  }

}
