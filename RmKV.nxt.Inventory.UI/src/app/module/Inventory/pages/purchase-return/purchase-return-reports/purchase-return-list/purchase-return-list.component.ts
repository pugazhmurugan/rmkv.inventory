import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-purchase-return-list',
  templateUrl: './purchase-return-list.component.html',
  styleUrls: ['./purchase-return-list.component.scss']
})
export class PurchaseReturnListComponent implements OnInit {

  constructor(public _minMaxDate: MinMaxDate,public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
