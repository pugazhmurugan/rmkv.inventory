import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseReturnDcPrintComponent } from './purchase-return-dc-print.component';

describe('PurchaseReturnDcPrintComponent', () => {
  let component: PurchaseReturnDcPrintComponent;
  let fixture: ComponentFixture<PurchaseReturnDcPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseReturnDcPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseReturnDcPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
