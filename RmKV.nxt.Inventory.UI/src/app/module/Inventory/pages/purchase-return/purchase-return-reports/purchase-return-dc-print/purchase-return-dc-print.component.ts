import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-purchase-return-dc-print',
  templateUrl: './purchase-return-dc-print.component.html',
  styleUrls: ['./purchase-return-dc-print.component.scss']
})
export class PurchaseReturnDcPrintComponent implements OnInit {

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
