import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-trading-dr-cr-reports',
  templateUrl: './trading-dr-cr-reports.component.html',
  styleUrls: ['./trading-dr-cr-reports.component.scss']
})
export class TradingDrCrReportsComponent implements OnInit {

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
