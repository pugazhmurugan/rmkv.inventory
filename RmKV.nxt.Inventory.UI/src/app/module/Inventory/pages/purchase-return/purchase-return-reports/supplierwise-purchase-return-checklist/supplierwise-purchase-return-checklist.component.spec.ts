import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierwisePurchaseReturnChecklistComponent } from './supplierwise-purchase-return-checklist.component';

describe('SupplierwisePurchaseReturnChecklistComponent', () => {
  let component: SupplierwisePurchaseReturnChecklistComponent;
  let fixture: ComponentFixture<SupplierwisePurchaseReturnChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierwisePurchaseReturnChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierwisePurchaseReturnChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
