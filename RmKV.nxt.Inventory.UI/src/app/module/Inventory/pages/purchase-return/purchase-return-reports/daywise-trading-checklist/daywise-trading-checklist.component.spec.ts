import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaywiseTradingChecklistComponent } from './daywise-trading-checklist.component';

describe('DaywiseTradingChecklistComponent', () => {
  let component: DaywiseTradingChecklistComponent;
  let fixture: ComponentFixture<DaywiseTradingChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaywiseTradingChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaywiseTradingChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
