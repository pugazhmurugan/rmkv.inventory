import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-purchase-return-note-reports',
  templateUrl: './purchase-return-note-reports.component.html',
  styleUrls: ['./purchase-return-note-reports.component.scss']
})
export class PurchaseReturnNoteReportsComponent implements OnInit {


  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
