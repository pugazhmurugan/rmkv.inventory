import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Rights } from 'src/app/common/models/common-model';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { PurchaseReturnNoteService } from '../../../services/purchase-return/purchase-return-entry/purchase-return-note/purchase-return-note.service';
declare var jsPDF: any;

@Component({
  selector: 'app-purchase-return-note',
  templateUrl: './purchase-return-note.component.html',
  styleUrls: ['./purchase-return-note.component.scss'],
  providers: [DatePipe]
})
export class PurchaseReturnNoteComponent implements OnInit {

  componentVisibility: boolean = true;
  Date: any = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  purchaseReturnNoteLoad: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  objReturnNote: any = {
    inv_byno: 0,
    supplier_invoice_no: '',
    freight_charges: 0,
    packing_charges: 0,
    credit_amount: 0,
    debit_amount: 0,
    remarks: ''
  }

  objUnChangedReturnNote: any = {
    inv_byno: 0,
    supplier_invoice_no: '',
    freight_charges: 0,
    packing_charges: 0,
    credit_amount: 0,
    debit_amount: 0,
    remarks: ''
  }
  byNosList: any = [];
  byNoDetailsList: any = [];

  fromDate1: any = new Date();
  toDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Return_Date", "Byno", "Supplier_code", "Product_Name", "Supplier_Invoice_No", "Supplier_Inv_Date", "Remarks", "Action"];
  rights: Rights = {
    Add: true,
    Update: true,
    View: true
  };

  columns: any[] = [
    { display: 'remarks', editable: true },
    { display: 'selected', editable: true }
  ];

  @ViewChildren('remarks') remarks: ElementRef | any;
  @ViewChildren('selected') selected: ElementRef | any;

  constructor(
    public _router: Router,
    public _localStorage: LocalStorage,
    private _datePipe: DatePipe,
    private _matDialog: MatDialog,
    private _excelService: ExcelService,
    private _gridKeyEvents: GridKeyEvents,
    private _keyPressEvents: KeyPressEvents,
    private _confirmationDialog: ConfirmationDialogComponent,
    private _purchaseReturnNoteService: PurchaseReturnNoteService,
  ) {
    this.purchaseReturnNoteLoad.FromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
    this.getPurchaseReturnByNos();
  }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.byNoDetailsList);
        // }
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.byNoDetailsList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.byNoDetailsList);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.byNoDetailsList);
        break;
    }
  }

  ngOnInit() {
  }

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.purchaseReturnNoteLoad.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.purchaseReturnNoteLoad.FromDate = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.purchaseReturnNoteLoad.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.purchaseReturnNoteLoad.ToDate = date[0];
    this.toDate1 = date[1];
  }

  getRights() {
    var rights = this._localStorage.getMenuRights("/PurchaseReturnVapasNote");
    // console.log('rights', rights);
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "View" && rights[x].Status == true) {
        this.rights.View = true;
      }
    }

  }

  /****************************************** CRUD Operations  *********************************************/

  public getPurchaseReturnNotes(): void {
    debugger;
    if (this.beforeLoadValidate()) {
      let objPurchase = {
        PurchaseReturnNote: JSON.stringify([{
          company_section_id: +this._localStorage.getCompanySectionId(),
          from_date: this.purchaseReturnNoteLoad.FromDate,
          to_date: this.purchaseReturnNoteLoad.ToDate
        }])
      }
      debugger;
      this._purchaseReturnNoteService.getPurchaseReturnNote(objPurchase).subscribe((result: any) => {
        result ? this.matTableConfig(result) : this._confirmationDialog.openAlertDialog('No records found', 'Purchase Return(Vapas) Note');
      });
    }
  }

  private getPurchaseReturnByNos(): void {
    let objPurchase = {
      PurchaseReturnNote: JSON.stringify([{
        company_section_id: this._localStorage.getCompanySectionId()
      }])
    }
    this._purchaseReturnNoteService.getPurchaseReturnByNos(objPurchase).subscribe((result: any) => {
      this.byNosList = result ? JSON.parse(JSON.stringify(result)) : [];
      this.objReturnNote.inv_byno = 0;
    });
  }

  public getPurchaseReturnByNoDetails(): void {
    let objPurchase = {
      PurchaseReturnNote: JSON.stringify([{
        company_section_id: this._localStorage.getCompanySectionId(),
        inv_byno: this.objReturnNote.inv_byno
      }])
    }
    this._purchaseReturnNoteService.getPurchaseRetunByNoDetails(objPurchase).subscribe((result: any) => {
      this.byNoDetailsList = result ? JSON.parse(JSON.stringify(result)) : this._confirmationDialog.openAlertDialog('No records found', 'Purchase Return(Vapas) Note');
      this.byNoDetailsList.forEach(x => {
        x.byNoSelected = false;
      });
    });
  }

  public addPurchaseReturnNote(): void {
    if (this.beforeSaveValidate()) {
      let detailsArr: any = [];
      this.byNoDetailsList.forEach(x => {
        if (x.byNoSelected) {
          detailsArr.push({
            byno_serial: x.byno_serial,
            remarks: x.remarks
          });
        }
      });
      let objAdd: any = {
        PurchaseReturnNote: JSON.stringify([{
          freight_charges: this.objReturnNote.freight_charges,
          packaging_charges: this.objReturnNote.packaging_charges,
          credit_amount: this.objReturnNote.credit_amount,
          debit_amount: this.objReturnNote.debit_amount,
          remarks: this.objReturnNote.remarks.toString().trim(),
          byno_details: detailsArr
        }])
      }
      this._purchaseReturnNoteService.addPurchaseReturnNote(objAdd).subscribe((result: boolean) => {
        if (result) {
          this._confirmationDialog.openAlertDialog('New purchase return(vapas) note record created', 'Purchase Return(Vapas) Note');
          this.componentVisibility = !this.componentVisibility;
        }
      });
    }
  }

  public viewPurchaseReturnNote(i: number): void {
    let objFetch: any = {
      PurchaseReturnNote: JSON.stringify([{

      }])
    }
    this._purchaseReturnNoteService.fetchPurchaseReturnNote(objFetch).subscribe((result: any) => {
      if (result) {

      }
    })
  }

  public newClick(): void {
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade()) {
      this.openConfirmationDialog(exitFlag ? 'Changes will be lost, are you sure?' : 'Do you want to clear all the fields', exitFlag);
    } else if (exitFlag)
      this.listClick();
  }

  public listClick(): void {
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  private matTableConfig(tableRecords?: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }

  /********************************************** Validations **********************************************/

  private beforeLoadValidate(): boolean {
    if (this.purchaseReturnNoteLoad.FromDate.toString().trim().length !== 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialog.openAlertDialog('Invalid from date', 'Purchase Return(Vapas) Note');
      return false;
    } if (this.purchaseReturnNoteLoad.ToDate.toString().trim().length !== 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialog.openAlertDialog('Invalid to date', 'Purchase Return(Vapas) Note');
      return false;
    } if (this.fromDate1 > this.toDate1) {
      document.getElementById('toDate').focus();
      this._confirmationDialog.openAlertDialog('To date must be greater than from date', 'Purchase Return(Vapas) Note');
      return false;
    } else return true;
  }

  public beforeSaveValidate(): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objReturnNote.inv_byno) !== -1) {
      document.getElementById('byNo').focus();
      this._confirmationDialog.openAlertDialog('Select byno', 'Purchase Return(Vapas) Note');
      return false;
    } if (this.byNoDetailsList.length === 0) {
      this._confirmationDialog.openAlertDialog('Load byno details', 'Purchase Return(Vapas) Note');
      return false;
    } if (!this.checkIsValidByNoDetailsList()) {
      this._confirmationDialog.openAlertDialog('Select at least one byno serial', 'Purchase Return(Vapas) Note');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objReturnNote.freight_charges) !== -1) {
      document.getElementById('freightCharges').focus();
      this._confirmationDialog.openAlertDialog('Enter freight charges', 'Purchase Return(Vapas) Note');
      return false;
    } if (+this.objReturnNote.freight_charges < 0) {
      this._confirmationDialog.openAlertDialog('Invalid freight charges', 'Purchase Return(Vapas) Note');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objReturnNote.packing_charges) !== -1) {
      document.getElementById('packagingCharges').focus();
      this._confirmationDialog.openAlertDialog('Enter packaging charges', 'Purchase Return(Vapas) Note');
      return false;
    } if (+this.objReturnNote.freight_charges < 0) {
      this._confirmationDialog.openAlertDialog('Invalid packaging charges', 'Purchase Return(Vapas) Note');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objReturnNote.credit_amount) !== -1) {
      document.getElementById('creditAmount').focus();
      this._confirmationDialog.openAlertDialog('Enter credit amount', 'Purchase Return(Vapas) Note');
      return false;
    } if (+this.objReturnNote.freight_charges < 0) {
      this._confirmationDialog.openAlertDialog('Invalid credit amount', 'Purchase Return(Vapas) Note');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objReturnNote.debit_amount) !== -1) {
      document.getElementById('debitAmount').focus();
      this._confirmationDialog.openAlertDialog('Enter debit amount', 'Purchase Return(Vapas) Note');
      return false;
    } if (+this.objReturnNote.freight_charges < 0) {
      this._confirmationDialog.openAlertDialog('Invalid debit amount', 'Purchase Return(Vapas) Note');
      return false;
    } else return true;
  }

  private checkIsValidByNoDetailsList(): boolean {
    if (this.byNoDetailsList.length > 0) {
      let i = this.byNoDetailsList.findIndex(x => x.byNoSelected === true);
      return i === -1 ? false : true;
    }
  }

  public onlyAllwDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Purchase Return(Vapas) Note";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.listClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    return JSON.stringify(this.objReturnNote) !== JSON.stringify(this.objUnChangedReturnNote) || this.byNoDetailsList.length !== 0 ? true : false;
  }

  private resetScreen(): void {
    this.objReturnNote = JSON.parse(JSON.stringify(this.objUnChangedReturnNote));
    this.byNoDetailsList = [];
  }

  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Date": this.dataSource.data[i].note_date,
          "Inv Byno": this.dataSource.data[i].inv_byno,
          "Supplier Code": this.dataSource.data[i].supplier_code,
          "Supplier Name": this.dataSource.data[i].supplier_name,
          "Remarks": this.dataSource.data[i].remarks
        });
      }
      this._excelService.exportAsExcelFile(json, "Purchase_Return(Vapas)_Note", datetime);
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "Purchase Return(Vapas) Note");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.note_date);
        tempObj.push(e.inv_byno);
        tempObj.push(e.supplier_code);
        tempObj.push(e.supplier_name);
        tempObj.push(e.remarks);
        prepare.push(tempObj);
      });

      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Date", "Inv Byno", "Supplier Code", "Supplier Name", "Remarks"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('Purchase_Return(Vapas)_Note' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "Purchase Return(Vapas) Note");
  }
}
