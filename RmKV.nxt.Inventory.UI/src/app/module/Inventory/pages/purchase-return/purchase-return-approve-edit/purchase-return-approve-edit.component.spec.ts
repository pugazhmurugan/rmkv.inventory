import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseReturnApproveEditComponent } from './purchase-return-approve-edit.component';

describe('PurchaseReturnApproveEditComponent', () => {
  let component: PurchaseReturnApproveEditComponent;
  let fixture: ComponentFixture<PurchaseReturnApproveEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseReturnApproveEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseReturnApproveEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
