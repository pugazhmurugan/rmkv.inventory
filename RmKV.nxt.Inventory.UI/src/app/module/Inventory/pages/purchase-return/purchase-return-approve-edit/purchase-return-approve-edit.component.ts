import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-purchase-return-approve-edit',
  templateUrl: './purchase-return-approve-edit.component.html',
  styleUrls: ['./purchase-return-approve-edit.component.scss'],
  providers: [DatePipe]
})
export class PurchaseReturnApproveEditComponent implements OnInit {

  componentVisibility: boolean = true;

  Date: any = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  fromDate1: any = new Date();
  toDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  constructor(public _localStorage: LocalStorage,
    public _router: Router, private _datePipe: DatePipe,) {
    this.load.FromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
  }

  ngOnInit() {
  }

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }

}
