import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { MultipleFilesAttachComponent } from 'src/app/common/shared/multiple-files-attach/multiple-files-attach.component';
import { EditMode } from '../../../model/common.model';
import { PurchaseReturnNoteService } from '../../../services/purchase-return/purchase-return-entry/purchase-return-note/purchase-return-note.service';
import { ReturnedGoodsNotesService } from '../../../services/purchase-return/returned-goods-notes/returned-goods-notes.service';

@Component({
  selector: 'app-returned-goods-notes',
  templateUrl: './returned-goods-notes.component.html',
  styleUrls: ['./returned-goods-notes.component.scss'],
  providers: [DatePipe]
})
export class ReturnedGoodsNotesComponent implements OnInit {

  componentVisibility: number = 1;
  Date: any = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  objLoad: any = {
    From_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    To_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Section: this._localStorage.getCompanySectionName()
  }



  model1: any = new Date();
  model2: any = new Date();
  tempLRDate: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "RGN_No", "RGN_Date", "Supplier_Name", "No_Of_Bales", "LR_No", "LR_Amount", "Action"];

  byNosList: any = [];
  byNoDetailsList: any = [];
  objRGN = {
    Section: this._localStorage.getCompanySectionName(),
    RGN_No: '',
    RGN_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Supplier_Details: [{
      checkFlag: false,
      dc_no: '',
      dc_date: '',
      pcs: '',
      supplier_name: '',
      city: '',
      supplier_code: ''
    }],
    LR_No: '',
    LR_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    LR_Amount: '',
    Paid: '',
    No_of_Bales: '',
    Transporter: 0,
    Mode_Of_Transporter: '',
    Carrier_Name: '',
    Coolie: '',
    Kind_Attention: '',
    Docket_No: '',
    Remarks: '',
    file_path1: '',
    file_path2: '',
    file_path3: '',
    file_path4: ''
  }
  objModifyRGN = {
    Section: this._localStorage.getCompanySectionName(),
    RGN_No: '',
    RGN_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Supplier_Details: [{
      checkFlag: false,
      dc_no: '',
      dc_date: '',
      pcs: '',
      supplier_name: '',
      city: '',
      supplier_code: ''
    }],
    LR_No: '',
    LR_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    LR_Amount: '',
    Paid: '',
    No_of_Bales: '',
    Transporter: 0,
    Mode_Of_Transporter: '',
    Carrier_Name: '',
    Coolie: '',
    Kind_Attention: '',
    Docket_No: '',
    Remarks: '',
    file_path1: '',
    file_path2: '',
    file_path3: '',
    file_path4: ''
  }
  unchangedRGN = {
    Section: this._localStorage.getCompanySectionName(),
    RGN_No: '',
    RGN_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Supplier_Details: [{
      checkFlag: false,
      dc_no: '',
      dc_date: '',
      pcs: '',
      supplier_name: '',
      city: '',
      supplier_code: ''
    }],
    LR_No: '',
    LR_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    LR_Amount: '',
    Paid: '',
    No_of_Bales: '',
    Transporter: 0,
    Mode_Of_Transporter: '',
    Carrier_Name: '',
    Coolie: '',
    Kind_Attention: '',
    Docket_No: '',
    Remarks: '',
    file_path1: '',
    file_path2: '',
    file_path3: '',
    file_path4: ''
  }

  objAttachView: any = {
    Section: this._localStorage.getCompanySectionName(),
    RGN_No: '',
    RGN_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Supplier_Details: [{
      checkFlag: false,
      dc_no: '',
      dc_date: '',
      pcs: '',
      supplier_name: '',
      city: '',
      supplier_code: ''
    }],
    LR_No: '',
    LR_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    LR_Amount: '',
    Paid: '',
    No_of_Bales: '',
    Transporter: 0,
    Mode_Of_Transporter: '',
    Carrier_Name: '',
    Coolie: '',
    Kind_Attention: '',
    Docket_No: '',
    Remarks: '',
    file_path1: '',
    file_path2: '',
    file_path3: '',
    file_path4: ''
  }
  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };
  totaldata: any = [];
  viewFlag: boolean = false;
  SupplierDCDetailsList: any = [];
  @ViewChildren('check') check: ElementRef | any;
  transporterList: any;


  AttachFiles: any = [];
  isFileAttach: number;
  fileIndex: any = [];
  tempFile: any;
  selectedDocument: any;
  document: any;
  selectedFiles: File[] | any[] = [];
  @ViewChild('fileInput', null) fileInput: ElementRef;
  isCancelled: boolean = false;
  removedFileIndex: number[] = [];
  removedFiles: string[] = [];
  objView: any = {
    dc_no: '',
    dc_date: ''
  };
  constructor(
    public _router: Router,
    public _localStorage: LocalStorage,
    private _datePipe: DatePipe,
    private _confirmationDialog: ConfirmationDialogComponent,
    private _RGNService: ReturnedGoodsNotesService,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _keyPressEvents: KeyPressEvents,
  ) {
    this.model1.setDate(this.model2.getDate() - 2);
    this.objLoad.From_Date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
    // this.getPurchaseReturnNotes();
  }

  ngOnInit() {
  }

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.objLoad.FromDate, this.model1, this.minDate, this.maxDate);
    this.objLoad.FromDate = date[0];
    this.model1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.objLoad.ToDate, this.model2, this._datePipe.transform(this.model1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoad.ToDate = date[0];
    this.model2 = date[1];
  }
  public validateLRDate() {
    let date = this.dateValidation.validateDate(this.objRGN.LR_Date, this.tempLRDate, this.minDate, this.maxDate);
    this.objRGN.LR_Date = date[0];
    this.tempLRDate = date[1];
  }

  /****************************************** CRUD Operations  *********************************************/

  public getTransporter(): void {
    this.transporterList = [];
    this.objRGN.Transporter = 0;
    if (this.objRGN.Mode_Of_Transporter == 'Transport') {
      let objPurchase = {
        RGNDetails: JSON.stringify([{
          sales_location_id: +this._localStorage.getGlobalSalesLocationId()
        }])
      }
      this._RGNService.getTransporter(objPurchase).subscribe((result: any) => {
        this.transporterList = result;
      });
    }
  }
  public getDCDetails(i): void {
    let objPurchase = {
      RGNDetails: JSON.stringify([{
        dc_no: this.objRGN.Supplier_Details[i].dc_no,
        dc_date: this.objRGN.Supplier_Details[i].dc_date,
        trading_drcr_no: this.objRGN.Supplier_Details[i].dc_no,
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._RGNService.getDCDetails(objPurchase).subscribe((result: any) => {
      this.SupplierDCDetailsList = result;
      this.objView.dc_no = this.objRGN.Supplier_Details[i].dc_no;
      this.objView.dc_date = this.objRGN.Supplier_Details[i].dc_date;
    });
  }

  public getRGNDCDetails(): void {
    let objPurchase = {
      RGNDetails: JSON.stringify([{
        date: this.objRGN.RGN_Date,
        company_section_id: this._localStorage.getCompanySectionId()
      }])
    }
    this._RGNService.getRGNDCDetails(objPurchase).subscribe((result: any) => {
      this.objRGN.Supplier_Details = [];
      this.unchangedRGN.Supplier_Details = [];

      console.log(result, 'vhcgdv')
      for (let i = 0; i < JSON.parse(JSON.stringify(result)).length; i++) {
        this.objRGN.Supplier_Details.push({
          checkFlag: false,
          dc_no: result[i].dc_no,
          dc_date: result[i].dc_date,
          supplier_name: result[i].supplier_name,
          city: result[i].city,
          pcs: result[i].pcs,
          supplier_code: result[i].supplier_code
        })
        this.unchangedRGN.Supplier_Details.push({
          checkFlag: false,
          dc_no: result[i].dc_no,
          dc_date: result[i].dc_date,
          supplier_name: result[i].supplier_name,
          city: result[i].city,
          pcs: result[i].pcs,
          supplier_code: result[i].supplier_code
        })
      }
    });
  }
  private beforeloadValidate(): boolean {
    if (!this.objLoad.From_Date.toString().trim()) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter from date", "Returned Goods Notes");
      return false;
    } else if (this.objLoad.From_Date.length != 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid from date", "Returned Goods Notes");
      return false;
    } else if (!this.objLoad.To_Date.toString().trim()) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter to date", "Returned Goods Notes");
      return false;
    } else if (this.objLoad.To_Date.length != 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid to date", "Returned Goods Notes");
      return false;
    } else if (this.model2 < this.model1) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("To date can't be less than from date", "Returned Goods Notes");
      return false;
    } else
      return true;
  }
  public getRGNDetails(): void {
    if (this.beforeloadValidate()) {
      this.matTableConfig([]);
      let objPurchase = {
        RGNDetails: JSON.stringify([{
          from_date: this.objLoad.From_Date,
          to_date: this.objLoad.To_Date,
          company_section_id: this._localStorage.getCompanySectionId()
        }])
      }
      this._RGNService.getRGNDetails(objPurchase).subscribe((result: any) => {
        if (result)
          this.matTableConfig(result);
        else
          this._confirmationDialogComponent.openAlertDialog('No record found', 'Returned Goods Notes');
      });
    }
  }
  public fetchRGNDetails(data): void {
    let objPurchase = {
      RGNDetails: JSON.stringify([{
        rgn_date: this._datePipe.transform(data.rgn_date, 'dd/MM/yyyy'),
        rgn_no: data.rgn_no,
        company_section_id: this._localStorage.getCompanySectionId()
      }])
    }
    this._RGNService.fetchRGNDetails(objPurchase).subscribe((result: any) => {
      console.log(result, 'fhbj');
      this.componentVisibility = 2;
      this.objAction.isEditing = true;
      this.assignData(result[0]);
    });
  }
  assignData(data) {
    this.objRGN.RGN_No = data.rgn_no;
    this.objRGN.RGN_Date = this._datePipe.transform(data.rgn_date, 'dd/MM/yyyy');
    this.objRGN.LR_No = data.lr_no;
    this.objRGN.LR_Amount = data.lr_amount;
    this.objRGN.LR_Date = this._datePipe.transform(data.lr_date, 'dd/MM/yyyy');
    this.objRGN.Paid = data.to_pay;
    this.objRGN.No_of_Bales = data.bales;
    this.objRGN.Mode_Of_Transporter = data.mode_of_transfer;
    this.getTransporter();
    this.objRGN.Carrier_Name = data.carrier_name;
    this.objRGN.Coolie = data.coolie;
    this.objRGN.Kind_Attention = data.kind_attention;
    this.objRGN.Docket_No = data.docket_no;
    this.objRGN.Remarks = data.remarks;
    this.objRGN.Supplier_Details = JSON.parse(JSON.stringify(data.dc_details));
    this.objRGN.Transporter = +data.transporter_code;
  }


  newClick(): void {
    this.totaldata = [];
    this.resetScreen();
    this.getRGNDCDetails();
    this.componentVisibility = 2;

  }

  onListClick(): void {
    this.totaldata = [];
    this.componentVisibility = 1;
  }

  public matTableConfig(tableRecords?: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }

  /********************************************** Validations **********************************************/


  private checkAnyChangesMade(): boolean {
    if (JSON.stringify(this.objRGN) !== (!this.objAction.isEditing ? JSON.stringify(this.unchangedRGN) : JSON.stringify(this.objModifyRGN))) {
      return true;
    } else {
      return false;
    }
  }

  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Returned Goods Notes";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }
  private resetScreen(): void {
    this.objRGN = JSON.parse(JSON.stringify(this.unchangedRGN));
    this.objModifyRGN = JSON.parse(JSON.stringify(this.unchangedRGN));
    this.totaldata = [];
  }

  private beforeSaveValidate(): boolean {
    // if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objRGN.LR_No.toString().trim()) !== -1) {
    //   document.getElementById("lrno").focus();
    //   this._confirmationDialogComponent.openAlertDialog('Enter lr no', 'Returned Goods Notes');
    //   return false;
    // } else if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objRGN.LR_Date.toString().trim()) !== -1) {
    //   document.getElementById("fromDate").focus();
    //   this._confirmationDialogComponent.openAlertDialog('Enter lr date', 'Returned Goods Notes');
    //   return false;
    // } else if (this.objRGN.LR_Date.length != 10) {
    //   document.getElementById('fromDate').focus();
    //   this._confirmationDialogComponent.openAlertDialog("Enter valid lr date", "Returned Goods Notes");
    //   return false;
    // } else if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objRGN.LR_Amount.toString().trim()) !== -1) {
    //   document.getElementById("lramount").focus();
    //   this._confirmationDialogComponent.openAlertDialog('Enter lr amount', 'Returned Goods Notes');
    //   return false;
    // } else 
    if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objRGN.Paid.toString().trim()) !== -1) {
      document.getElementById("paid").focus();
      this._confirmationDialogComponent.openAlertDialog('select paid', 'Returned Goods Notes');
      return false;
    } else if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objRGN.No_of_Bales.toString().trim()) !== -1) {
      document.getElementById("bales").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter no of bales', 'Returned Goods Notes');
      return false;
    } else if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objRGN.Mode_Of_Transporter.toString().trim()) !== -1) {
      document.getElementById("rdiogrp").focus();
      this._confirmationDialogComponent.openAlertDialog('select mode of transport', 'Returned Goods Notes');
      return false;
    } else if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objRGN.Transporter.toString().trim()) !== -1 && this.objRGN.Mode_Of_Transporter == 'Transport') {
      document.getElementById("trans").focus();
      this._confirmationDialogComponent.openAlertDialog('select transport', 'Returned Goods Notes');
      return false;
    } else if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objRGN.Carrier_Name.toString().trim()) !== -1 && this.objRGN.Mode_Of_Transporter !== 'Transport') {
      document.getElementById("carrier").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter carrier name', 'Returned Goods Notes');
      return false;
    }
    //else if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objRGN.Coolie.toString().trim()) !== -1) {
    //   document.getElementById("coolie").focus();
    //   this._confirmationDialogComponent.openAlertDialog("Enter coolie", "Returned Goods Notes");
    //   return false;
    // } else if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objRGN.Kind_Attention.toString().trim()) !== -1) {
    //   document.getElementById("kind").focus();
    //   this._confirmationDialogComponent.openAlertDialog("Enter kind attention", "Returned Goods Notes");
    //   return false;
    // } else if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objRGN.Docket_No.toString().trim()) !== -1) {
    //   document.getElementById("dcno").focus();
    //   this._confirmationDialogComponent.openAlertDialog("Enter dc no", "Returned Goods Notes");
    //   return false;
    // } 
    else {
      return true;
    }
  }

  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }
  onAddCheckValue(i) {
    if (this.objRGN.Supplier_Details[i].checkFlag == true) {
      this.totaldata.push({
        dc_no: this.objRGN.Supplier_Details[i].dc_no,
        dc_date: this.objRGN.Supplier_Details[i].dc_date,
        supplier_name: this.objRGN.Supplier_Details[i].supplier_name,
        city: this.objRGN.Supplier_Details[i].city,
        pcs: this.objRGN.Supplier_Details[i].pcs,
        checkFlag: this.objRGN.Supplier_Details[i].checkFlag
      })
    } else if (this.objRGN.Supplier_Details[i].checkFlag == false) {
      let id = this.objRGN.Supplier_Details[i].dc_no;
      for (let i = 0; i < this.totaldata.length; i++)
        if (this.totaldata[i].dc_no == id) {
          this.totaldata.splice(i, 1);
        }
    }
  }
  alreadySeletecedCheck(j) {
    debugger
    if (this.totaldata.length > 1) {
      for (let k = 0; k < this.objRGN.Supplier_Details.length; k++) {
        this.objRGN.Supplier_Details[k].checkFlag = false;
      }
      this.objRGN.Supplier_Details[j].checkFlag = true;
    }
    this.totaldata = [];
    this.totaldata.push(this.objRGN.Supplier_Details[j]);
  }

  openAlertDialog(value: string, componentName?: string, j?: number) {
    //  this._matDialog.closeAll();
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.objRGN.Supplier_Details[j].checkFlag = false;
        let input = this.check.toArray();
        input[j].nativeElement.focus();
        this.onAddCheckValue(j);
      }
      _dialogRef = null;
    });
  }
  public getTransferPcs(): number {
    debugger
    if (this.totaldata.length == 1 && this.totaldata[0].checkFlag == false)
      return 0;
    else if (this.totaldata.length !== 0)
      return this.totaldata.map(t => +t.pcs).reduce((acc, value) => acc + value, 0);
  }

  addRGN() {
    if (this.objRGN.Supplier_Details.length != 0) {
      let index = this.objRGN.Supplier_Details.findIndex(x => x.checkFlag == true)
      if (index == -1) {
        let input = this.check.toArray();
        input[0].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog('Select one row must', 'Returned Goods Notes')
        return
      }
    } else {
      this._confirmationDialogComponent.openAlertDialog('No record to save', 'Returned Goods Notes')
      return
    }
    if (this.beforeSaveValidate()) {
      const formData = new FormData();
      for (var i in this.selectedFiles) {
        formData.append("files", this.selectedFiles[i]);
      }
      let tempTranferData = [];
      if (this.objRGN.Supplier_Details) {
        for (let i = 0; i < this.objRGN.Supplier_Details.length; i++) {
          if (this.objRGN.Supplier_Details[i].checkFlag == true) {
            tempTranferData.push({
              dc_no: this.objRGN.Supplier_Details[i].dc_no,
              dc_date: this.objRGN.Supplier_Details[i].dc_date,
              supplier_name: this.objRGN.Supplier_Details[i].supplier_name,
              supplier_code: this.objRGN.Supplier_Details[i].supplier_code,
              trading_drcr_no: this.objRGN.Supplier_Details[i].dc_no,
              city: this.objRGN.Supplier_Details[i].city,
              pcs: this.objRGN.Supplier_Details[i].pcs
            })
          }
        }
      }
      let objData = {
        // RGNDetails: JSON.stringify([{
        rgn_no: this.objRGN.RGN_No,
        rgn_date: this.objRGN.RGN_Date,
        lr_no: this.objRGN.LR_No,
        lr_date: this.objRGN.LR_Date,
        lr_amount: this.objRGN.LR_Amount == '' ? 0 : this.objRGN.LR_Amount,
        to_pay: this.objRGN.Paid,
        invoice_no: '',//this.objRGN,
        invoice_date: '',//his.objRGN,
        bales: this.objRGN.No_of_Bales,
        dc_no: this.objRGN.Docket_No,
        mode_of_transfer: this.objRGN.Mode_Of_Transporter,
        transporter_code: this.objRGN.Transporter,
        carrier_name: this.objRGN.Carrier_Name,
        docket_no: this.objRGN.Docket_No,
        coolie: this.objRGN.Coolie == '' ? 0 : this.objRGN.Coolie,
        delivered_by: this.objRGN.Kind_Attention,
        kind_attention: this.objRGN.Kind_Attention,
        remarks: this.objRGN.Remarks,
        file_path: [{
          file_path1: this.objRGN.file_path1, file_path2: this.objRGN.file_path2,
          file_path3: this.objRGN.file_path3, file_path4: this.objRGN.file_path4
        }],
        details: tempTranferData,
        entered_by: +this._localStorage.intGlobalUserId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        sales_location_id: +this._localStorage.getGlobalSalesLocationId()
        // }]),
      }
      formData.append('objRGNDetails', JSON.stringify(objData));
      formData.append("fileRootPath", '\\\\192.9.202.39\\\\Rmkv_Nxt_Doc\\\\Invoices');
      formData.append("fileIndex", JSON.stringify(this.fileIndex));
      formData.append('removedFiles', JSON.stringify(this.removedFiles));
      formData.append('removedFileIndex', JSON.stringify(this.removedFileIndex));
      this._RGNService.addRGNDetails(formData).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New RGN record has been created", 'Returned Goods Notes');
          this.componentVisibility = 1;
          this.resetScreen();
          this.getRGNDetails();
        }
      });
    }
  }
  onViewClick(i) {
    this.componentVisibility = 3;
    this.getDCDetails(i);
  }
  public getReturnQty(): number {
    if (this.SupplierDCDetailsList.length !== 0)
      return this.SupplierDCDetailsList.map(t => +t.return_qty).reduce((acc, value) => acc + value, 0);
  }
  public getReturnPcs(): number {
    if (this.SupplierDCDetailsList.length !== 0)
      return this.SupplierDCDetailsList.map(t => +t.return_pcs).reduce((acc, value) => acc + value, 0);
  }

  // public openAttachFilesPopup(): void {
  //   let dialogRef = this._matDialog.open(MultipleFilesAttachComponent, {
  //     panelClass: "custom-dialog-container",
  //     width: '53vw',
  //     data: {
  //       objAttach: JSON.stringify(this.objAttachView),
  //     }
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //     debugger;
  //     if (result) {
  //       this.fileIndex = [];
  //       debugger;
  //       this.selectedFiles = result.selectedFiles;
  //       this.fileIndex = JSON.parse(JSON.stringify(result.fileIndex));
  //       this.removedFileIndex = JSON.parse(JSON.stringify(result.removedFileIndex));
  //       this.removedFiles = JSON.parse(JSON.stringify(result.removedFiles));
  //       this.objAttachView = JSON.parse(JSON.stringify(result.objAttachView));
  //       if (this.objRGN.file_path1 !== this.objAttachView.file_path1)
  //         this.objRGN.file_path1 = '';
  //       if (this.objRGN.file_path2 !== this.objAttachView.file_path2)
  //         this.objRGN.file_path2 = '';
  //       if (this.objRGN.file_path3 !== this.objAttachView.file_path3)
  //         this.objRGN.file_path3 = '';
  //       if (this.objRGN.file_path4 !== this.objAttachView.file_path4)
  //         this.objRGN.file_path4 = '';
  //       debugger;
  //       console.log(this.objRGN, 'RFGN')
  //     }
  //   });
  // }

  public openAttachFilesPopup(): void {
    let objAttachView = {
      file_path1: this.objRGN.file_path1,
      file_path2: this.objRGN.file_path2,
      file_path3: this.objRGN.file_path3,
      file_path4: this.objRGN.file_path4,
    }
    let dialogRef = this._matDialog.open(MultipleFilesAttachComponent, {
      panelClass: "custom-dialog-container",
      width: '53vw',
      data: {
        objAttach: JSON.stringify(objAttachView),
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      debugger;
      if (result) {
        this.fileIndex = [];
        debugger;
        this.selectedFiles = result.selectedFiles;
        // this.isFileAttach = JSON.parse(JSON.stringify(result.isFileAttach));
        this.fileIndex = JSON.parse(JSON.stringify(result.fileIndex));
        this.removedFileIndex = JSON.parse(JSON.stringify(result.removedFileIndex));
        this.removedFiles = JSON.parse(JSON.stringify(result.removedFiles));
        this.objAttachView = JSON.parse(JSON.stringify(result.objAttachView));
        if (this.objRGN.file_path1 !== this.objAttachView.file_path1)
          this.objRGN.file_path1 = '';
        if (this.objRGN.file_path2 !== this.objAttachView.file_path2)
          this.objRGN.file_path2 = '';
        if (this.objRGN.file_path3 !== this.objAttachView.file_path3)
          this.objRGN.file_path3 = '';
        if (this.objRGN.file_path4 !== this.objAttachView.file_path4)
          this.objRGN.file_path4 = '';
      }
    });
  }
}