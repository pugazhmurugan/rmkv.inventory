import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnedGoodsNotesComponent } from './returned-goods-notes.component';

describe('ReturnedGoodsNotesComponent', () => {
  let component: ReturnedGoodsNotesComponent;
  let fixture: ComponentFixture<ReturnedGoodsNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReturnedGoodsNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnedGoodsNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
