import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode, GroupSections } from 'src/app/common/models/common-model';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductGroup } from '../../../model/masters/product-group-model';
import { ProductCategoryService } from '../../../services/masters/product-category/product-category.service';
import { ProductGroupService } from '../../../services/masters/product-group/product-group.service';
declare var jsPDF: any;

@Component({
  selector: 'app-product-groups',
  templateUrl: './product-groups.component.html',
  styleUrls: ['./product-groups.component.scss'],
  providers: [DatePipe]
})
export class ProductGroupsComponent implements OnInit {

  componentVisibility: boolean = true;
  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Group_Name", "Description", "Category", "UOM", "Unique_Byno", "Lifestyle", "Active", "Action"];
  groupSectionsList: GroupSections[] = [];
  unChangedGroupSectionsList: GroupSections[] = [];
  modifyGroupSectionsList: GroupSections[] = [];
  categoriesList: any[] = [];
  productGroupList:any[]=[];

  objLoad: any = {
    group_section_id: this._localStorage.getGlobalGroupSectionId(),
    group_section_name: this._localStorage.getGroupSectionName()
  }

  objAction: EditMode = {
    isEditing: false
  }

  objUnchangedAction: EditMode = {
    isEditing: false
  }

  objProductGroup: ProductGroup = {
    product_group_id: 0,
    product_group_name: '',
    product_group_desc: '',
    product_group_uom: 1,
    unique_byno_prod_serial: true,
    // lsp: false,
    group_section_id: [],
    category_id: 0,
    entered_by: this._localStorage.intGlobalUserId()
  }

  objUnChangedProductGroup: ProductGroup = {
    product_group_id: 0,
    product_group_name: '',
    product_group_desc: '',
    product_group_uom: 1,
    unique_byno_prod_serial: true,
    // lsp: false,
    group_section_id: [],
    category_id: 0,
    entered_by: this._localStorage.intGlobalUserId()
  }

  objModifyProductGroup: ProductGroup = {
    product_group_id: 0,
    product_group_name: '',
    product_group_desc: '',
    product_group_uom: 1,
    unique_byno_prod_serial: true,
    // lsp: false,
    group_section_id: [],
    category_id: 0,
    entered_by: this._localStorage.intGlobalUserId()
  }

  constructor(
    public _router: Router,
    public _matDialog: MatDialog,
    public _localStorage: LocalStorage,
    public keyPressEvents: KeyPressEvents,
    private _datePipe: DatePipe,
    private _excelService: ExcelService,
    private _commonService: CommonService,
    private _categoryService: ProductCategoryService,
    private _productGroupService: ProductGroupService,
    private _confirmationDialog: ConfirmationDialogComponent,
    private _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
  ) {
    this.getGroupSections();
    this.loadProductGroups();
    this.getProductCategories();
  }

  ngOnInit() {
  }

  private loadProductGroups(): void {
    let objGet: any = {
      ProductGroup: JSON.stringify([{
        group_section_id: +this._localStorage.getGlobalGroupSectionId()
      }])
    }
    this._productGroupService.getProductGroups(objGet).subscribe((result: any) => {
      if (result) {
        this.productGroupList = JSON.parse(JSON.stringify(result));
        this.matTableConfig(result);
      } else
        this._confirmationDialog.openAlertDialog('No records found', 'Product Groups');
    });
  }

  public GroupNameFilter(searchValue: string, event: KeyboardEvent | any): void {
    searchValue = searchValue.toString().trim() ? searchValue.toString().toLocaleLowerCase() : "";
    let filterproductGroupList = [];
    if (event.keyCode === 8 || event.keyCode === 46) {
      filterproductGroupList = this.productGroupList.filter((item: any) =>
        item.product_group_name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    } else {
      filterproductGroupList = this.dataSource.data.filter((item: any) =>
        item.product_group_name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    }
    this.matTableConfig(filterproductGroupList);
  }

  private getGroupSections(): void {
    this._commonService.getGroupSections().subscribe((result: GroupSections[]) => {
      if (result) {
        this.groupSectionsList = JSON.parse(JSON.stringify(result));
        this.modifyGroupSectionsList = JSON.parse(JSON.stringify(result));
        this.unChangedGroupSectionsList = JSON.parse(JSON.stringify(result));
        this.groupSectionsList.every(t => t.checked = false);
        this.modifyGroupSectionsList.every(t => t.checked = false);
        this.unChangedGroupSectionsList.every(t => t.checked = false);
      }
    });
  }

  private getProductCategories(): void {
    let objGetLoad: any = {
      group_section_id: +this._localStorage.getGlobalGroupSectionId(),
    }
    this._categoryService.getCategory(objGetLoad).subscribe((result: any) => {
      let tempArr = result;
      this.categoriesList = tempArr.filter(x => x.Active === true);
      console.log(this.categoriesList, "categories list")
    })
  }

  public onChangeSectionSelection(event: any, i: number): void {
    debugger;
    if (event.checked) {
      this.objProductGroup.group_section_id.push(this.groupSectionsList[i].Group_Section_ID);
    } else if (!event.checked) {
      this.objProductGroup.group_section_id.splice(this.objProductGroup.group_section_id.indexOf(this.groupSectionsList[i].Group_Section_ID), 1);
    }
  }

  public addProductGroups(): void {
    this.trimBeforeSave();
    if (this.beforeSaveValidate()) {
      let objSave = {
        ProductGroup: JSON.stringify([this.objProductGroup])
      }
      debugger;
      this._productGroupService.addProductGroups(objSave).subscribe((result: boolean) => {
        if (result) {
          this._confirmationDialog.openAlertDialog(this.objAction.isEditing ? 'Changes have been saved' : 'New product group has been added', 'Product Groups');
          this.loadProductGroups();
          this.onListClick();
        }
      });
    }
  }

  public modifyProductGroups(i: number): void {
    let objFetch = {
      ProductGroup: JSON.stringify([{
        product_group_id: this.dataSource.data[i].product_group_id
      }])
    }
    this._productGroupService.fetchProductGroups(objFetch).subscribe((result: any) => {
      if (result) {
        debugger;
        this.objAction.isEditing = true;
        this.objProductGroup = JSON.parse(JSON.stringify(result[0]));
        this.objModifyProductGroup = JSON.parse(JSON.stringify(result[0]));
        this.setSelectedGroupSections(result[0].group_section_id);
        this.componentVisibility = !this.componentVisibility;
      }
    })
  }

  private setSelectedGroupSections(sections: any): void {
    if (sections) {
      for (let i = 0; i < sections.length; i++) {
        let k = this.groupSectionsList.findIndex(x => x.Group_Section_ID === +sections[i]);
        this.groupSectionsList[k].checked = true;
        this.modifyGroupSectionsList[k].checked = true;
      }
    }
  }

  public onClickDelete(index: number): any {
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this._dialogRef.componentInstance.confirmMessage =
      "Do you want to deactivate this Product Group?"
    this._dialogRef.componentInstance.componentName = "Product Groups";
    return this._dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // this.openReasonDialog(index);
        this.deleteProductGroup(index);
      }
      this._dialogRef = null;
    });
  }

  // public openReasonDialog(index: number): void {
  //   debugger;
  //   let dialogRef = this._matDialog.open(ConfirmationDeleteComponent, {
  //     width: "75vw",
  //     panelClass: "custom-dialog-container",
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result !== null) {
  //       this.deleteProductGroup(index, result);
  //     }
  //     this._dialogRef = null;
  //   });
  // }

  private deleteProductGroup(index: number): void {
    debugger;
    let objDelete = {
      ProductGroup: JSON.stringify([{
        product_group_id: this.dataSource.data[index].product_group_id,
        active: false
      }])
    }
    debugger;
    this._productGroupService.deleteProductGroups(objDelete).subscribe(
      (result: any) => {
        if (result) {
          this.loadProductGroups();
          this._confirmationDialog.openAlertDialog("Removed", "Product Groups");
        }
      });
    this._dialogRef = null;
  }

  public onClickActive(index: any): any {
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this._dialogRef.componentInstance.confirmMessage =
      "Do you want to active this Product Group?"
    this._dialogRef.componentInstance.componentName = "Product Groups";
    return this._dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // this.openReasonDialog(index);
        this.activateProductGroup(index);
      }
      this._dialogRef = null;
    });
  }

  private activateProductGroup(index: number): void {
    debugger;
    let objDelete = {
      ProductGroup: JSON.stringify([{
        product_group_id: this.dataSource.data[index].product_group_id,
        active: true
      }])
    }
    debugger;
    this._productGroupService.deleteProductGroups(objDelete).subscribe(
      (result: any) => {
        if (result) {
          this.loadProductGroups();
          this._confirmationDialog.openAlertDialog("Activated", "Product Groups");
        }
      });
    this._dialogRef = null;
  }

  public newClick(): void {
    this.objAction = JSON.parse(JSON.stringify(this.objUnchangedAction));
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  public onListClick(): void {
    this.objAction = JSON.parse(JSON.stringify(this.objUnchangedAction));
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  private matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }

  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? 'Changes will be lost, are you sure ?' : 'Are you sure, want to clear entered data?', exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  /****************************************** Validations **********************************************/

  public onlyAllowCharacters(event: KeyboardEvent | any): boolean {
    const pattern = /^[a-zA-Z ]*$/;
    if (!pattern.test(event.key)) {
      return false;
    } else
      return true;
  }

  private beforeSaveValidate(): boolean {
    this.trimBeforeSave();
    if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objProductGroup.product_group_name.toString().trim()) !== -1) {
      this.objProductGroup.product_group_name = this.objProductGroup.product_group_name.toString().trim();
      document.getElementById('productGroup').focus();
      this._confirmationDialog.openAlertDialog('Enter product name', 'Product Groups');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(+this.objProductGroup.product_group_uom) !== -1) {
      document.getElementById('uom').focus();
      this._confirmationDialog.openAlertDialog('Select uom', 'Product Groups');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(+this.objProductGroup.group_section_id.length) !== -1) {
      // document.getElementById('category').focus();
      this._confirmationDialog.openAlertDialog('Select atleast one group section name', 'Product Groups');
      return false;
    } else return true;
    // if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(+this.objProductGroup.unique_byno_prod_serial) !== -1) {
    //   document.getElementById('uniqueByNo').focus();
    //   this._confirmationDialog.openAlertDialog('Select unique by no', 'Product Groups');
    //   return false;
    // }  if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objProductGroup.product_group_desc.toString().trim()) !== -1) {
    //   this.objProductGroup.product_group_desc = this.objProductGroup.product_group_desc.toString().trim();
    //   document.getElementById('productDesc').focus();
    //   this._confirmationDialog.openAlertDialog('Enter product description', 'Product Groups');
    //   return false;
    // } if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(+this.objProductGroup.category_id) !== -1) {
    //   document.getElementById('category').focus();
    //   this._confirmationDialog.openAlertDialog('Select category', 'Product Groups');
    //   return false;
    // }
  }

  private trimBeforeSave(): void {
    this.objProductGroup.product_group_name = [null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objProductGroup.product_group_name) !== -1 ? "" : this.objProductGroup.product_group_name.toString().trim();
    this.objModifyProductGroup.product_group_name = [null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objModifyProductGroup.product_group_name) !== -1 ? "" : this.objModifyProductGroup.product_group_name.toString().trim();
    this.objUnChangedProductGroup.product_group_name = [null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objUnChangedProductGroup.product_group_name) !== -1 ? "" : this.objModifyProductGroup.product_group_name.toString().trim();

    this.objProductGroup.product_group_desc = [null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objProductGroup.product_group_desc) !== -1 ? "" : this.objProductGroup.product_group_desc.toString().trim();
    this.objModifyProductGroup.product_group_desc = [null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objModifyProductGroup.product_group_desc) !== -1 ? "" : this.objModifyProductGroup.product_group_desc.toString().trim();
    this.objUnChangedProductGroup.product_group_desc = [null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(this.objUnChangedProductGroup.product_group_desc) !== -1 ? "" : this.objModifyProductGroup.product_group_desc.toString().trim();

    this.objProductGroup.category_id = [null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(+this.objProductGroup.category_id) !== -1 ? 0 : +this.objProductGroup.category_id;
    this.objModifyProductGroup.category_id = [null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(+this.objModifyProductGroup.category_id) !== -1 ? 0 : +this.objModifyProductGroup.category_id;
    this.objUnChangedProductGroup.category_id = [null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(+this.objUnChangedProductGroup.category_id) !== -1 ? 0 : +this.objUnChangedProductGroup.category_id;
  }

  private checkAnyChangesMade(): boolean {
    this.trimBeforeSave();
    if (JSON.stringify(this.objProductGroup) !== (this.objAction.isEditing ? JSON.stringify(this.objModifyProductGroup) : JSON.stringify(this.objUnChangedProductGroup)))
      return true;
    else return false;
  }

  private resetScreen(): void {
    if (this.objAction.isEditing) {
      this.objProductGroup = JSON.parse(JSON.stringify(this.objModifyProductGroup));
      this.objModifyProductGroup = JSON.parse(JSON.stringify(this.objModifyProductGroup));
      this.groupSectionsList = JSON.parse(JSON.stringify(this.modifyGroupSectionsList));
    } else if (!this.objAction.isEditing) {
      this.objProductGroup = JSON.parse(JSON.stringify(this.objUnChangedProductGroup));
      this.objModifyProductGroup = JSON.parse(JSON.stringify(this.objUnChangedProductGroup));
      this.groupSectionsList = JSON.parse(JSON.stringify(this.unChangedGroupSectionsList));
      this.objAction = JSON.parse(JSON.stringify(this.objUnchangedAction));
    }
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Product Groups";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Group Name": this.dataSource.data[i].product_group_name,
          "Description": this.dataSource.data[i].product_group_desc,
          "Category": this.dataSource.data[i].category_name,
          "UOM": this.dataSource.data[i].product_group_uom === 1 ? 'Nos' : 'Mts',
          "Unique ByNo": this.dataSource.data[i].unique_byno_prod_serial ? 'Yes' : 'No',
          // "LifeStyle": this.dataSource.data[i].lsp ? 'Yes' : 'No',
          "Active": this.dataSource.data[i].active ? 'Yes' : 'No'
        });
      }
      this._excelService.exportAsExcelFile(json, "Product_Groups", datetime);
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "Product Groups");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.product_group_name);
        tempObj.push(e.product_group_desc);
        tempObj.push(e.category_name);
        tempObj.push(e.product_group_uom === 1 ? 'Nos' : 'Mts');
        tempObj.push(e.unique_byno_prod_serial ? 'Yes' : 'No');
        // tempObj.push(e.lsp ? 'Yes' : 'No');
        tempObj.push(e.active ? 'Yes' : 'No');
        prepare.push(tempObj);
      });

      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Group Name", "Description", "Category", "UOM", "Unique ByNo", "Active"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('Product_Groups' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No records found, Load the data first", "Product Groups");
  }

}
