import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { AttributeLookupComponent } from 'src/app/common/shared/attribute-lookup/attribute-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductGroupLookupComponent } from 'src/app/common/shared/product-group-lookup/product-group-lookup.component';
import { ProductGroupAttributesService } from '../../../services/masters/product-group-attributes/product-group-attributes.service';
declare var jsPDF: any;

@Component({
  selector: 'app-product-group-attributes',
  templateUrl: './product-group-attributes.component.html',
  styleUrls: ['./product-group-attributes.component.scss'],
  providers: [DatePipe]
})
export class ProductGroupAttributesComponent implements OnInit {

  componentVisibility: boolean = true;
  productGroupList: any[] = [];
  attributeList: any[] = [];
  loadProductGroupList: any[] = [];
  dataSource: any = new MatTableDataSource([]);
  objProductAttributes: any = {
    Group_Section_Name: this._localStorage.getGroupSectionName(),
    product_group_id: 0,
    product_group_name: "",
    attribute_name: "",
    attribute_id: 0,
    attributeValue: [{
      attribute_value_id: 0,
      attribute_value: 0,
      is_selected: false,
    }]
  }

  unChangedProductAttributes: any = {
    Group_Section_Name: this._localStorage.getGroupSectionName(),
    product_group_id: 0,
    product_group_name: "",
    attribute_name: "",
    attribute_id: 0,
    attributeValue: [{
      attribute_value_id: 0,
      attribute_value: 0,
      is_selected: false,
    }]
  }

  objLoad: any = {
    Group_Section_Name: this._localStorage.getGroupSectionName(),
    product_group_id: 0,
    product_group_name: "",
    product_group_desc: "",
    category_id: 0,
    category_name: "",
    product_group_uom: '',
    unique_byno_prod_serial: 0,
    lsp: ''
  }

  displayedColumns = ["Serial_No", "Attribute_Name", "Attribute_Value", "Action"];
  focusFlag: boolean;

  constructor(public _localStorage: LocalStorage,
    public _router: Router,
    private _confirmationDialogComponent: ConfirmationDialogComponent,
    public _matDialog: MatDialog,
    public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _productGroupAttributesService: ProductGroupAttributesService,
    public _datePipe: DatePipe,
    public _excelService: ExcelService) {
    this.getProductGroupList();
    this.getAttributesList();
  }

  ngOnInit() {
  }

  public newClick(): void {
    if (this.beforeNewClick()) {
      this.componentVisibility = !this.componentVisibility;
      this.resetScreen();
    }
  }

  public onListClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.resetLoadScreen();
  }

  private resetLoadScreen(): void {
    this.objLoad.product_group_name = 0;
    this.objLoad.product_group_name = ""
    this.objLoad.product_group_desc = "";
    this.objLoad.category_name = "";
    this.objLoad.product_group_uom = '';
    this.objLoad.lsp = '';
    this.matTableConfig([]);
  }

  ////////////////// Product Group Lookup ///////////////////////

  public openProductGroupLookup(event: any): void {
    debugger;
    if (this.objLoad.product_group_name === "") {
      this.objLoad.product_group_desc = "";
      this.objLoad.category_name = "";
      this.objLoad.product_group_uom = '';
      this.objLoad.lsp = '';
      this.matTableConfig([]);
    }
    if (event.keyCode === 13) {
      this.openProductGroupLookupDialog();
    }
  }

  openAlertDialog(value: string, componentName?: string, focus?: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        document.getElementById(focus).focus();
      _dialogRef = null;
    });
  }


  private openProductGroupLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(ProductGroupLookupComponent, {
      width: "500px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objLoad.product_group_name
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      debugger;
      this.focusFlag = false;
      if (result) {
        if (result.lsp === true) {
          result.lsp = "Yes"
        } else if (result.lsp === false) {
          result.lsp = "No"
        }
        this.objLoad.product_group_id = result.product_group_id;
        this.objLoad.product_group_name = result.product_group_name.toString().trim();
        this.objLoad.product_group_desc = result.product_group_desc;
        this.objLoad.category_id = result.category_id;
        this.objLoad.category_name = result.category_name;
        this.objLoad.product_group_uom = result.product_group_uom;
        this.objLoad.unique_byno_prod_serial = result.unique_byno_prod_serial;
        this.objLoad.lsp = result.lsp;
        this.objProductAttributes.product_group_name = this.objLoad.product_group_name;
        this.objProductAttributes.product_group_id = this.objLoad.product_group_id;
        this.unChangedProductAttributes.product_group_name = this.objLoad.product_group_name;
        this.unChangedProductAttributes.product_group_id = this.objLoad.product_group_id;
        this.loadProductGroupDetails();
      } else {
        this.objLoad.product_group_id = 0;
        this.objLoad.product_group_desc = "";
        this.objLoad.category_id = 0;
        this.objLoad.category_name = '';
        this.objLoad.product_group_uom = '';
        this.objLoad.unique_byno_prod_serial = '';
        this.objLoad.lsp = '';
        this.matTableConfig([]);
      }
    });
  }

  public onEnterProductGroupLookupDetails(): void {
    debugger;
    let productGroupList: any = {
      product_group_id: 0,
      product_group_name: "",
      product_group_desc: "",
      category_id: 0,
      category_name: '',
      product_group_uom: '',
      unique_byno_prod_serial: '',
      lsp: '',
    }
    productGroupList = this.productGroupList.find(emp => emp.product_group_name.toLowerCase().trim() ===
      this.objLoad.product_group_name.toString().trim().toLowerCase());
    this.objLoad.product_group_id = productGroupList && productGroupList.product_group_id ? productGroupList.product_group_id : 0;
    this.objLoad.product_group_name = productGroupList && productGroupList.product_group_name ?
      productGroupList.product_group_name : this.objLoad.product_group_name;
    this.objLoad.product_group_desc = productGroupList && productGroupList.product_group_desc ? productGroupList.product_group_desc : '';
    this.objLoad.category_id = productGroupList && productGroupList.category_id ? productGroupList.category_id : 0;
    this.objLoad.category_name = productGroupList && productGroupList.category_name ? productGroupList.category_name : '';
    this.objLoad.product_group_uom = productGroupList && productGroupList.product_group_uom ? productGroupList.product_group_uom : '';
    this.objLoad.unique_byno_prod_serial = productGroupList && productGroupList.unique_byno_prod_serial ? productGroupList.unique_byno_prod_serial : '';
    this.objLoad.lsp = productGroupList && productGroupList.lsp.toString().trim() ? (productGroupList && productGroupList.lsp.toString().trim() === true ? 'Yes' : 'No') : '';
    this.objLoad.product_group_id !== 0 ? this.loadProductGroupDetails() : this.matTableConfig([]);
  }

  ////////////////// Product Group Lookup End ///////////////////////


  private getProductGroupList(): void {
    debugger;
    let objData: any = [{
      group_section_id: +this._localStorage.getGlobalGroupSectionId(),
    }];
    let objDetails: any = {
      GetProductGroupLookup: JSON.stringify(objData)
    }
    this._productGroupAttributesService.GetProductGroupLookupDetails(objDetails).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0) {
        debugger;
        this.productGroupList = JSON.parse(result);
      }
    });
  }

  private matTableConfig(tableRecords: any[]): void {
    this.dataSource = new MatTableDataSource(tableRecords);
  }

  private beforeNewClick(): boolean {
    if (this.objLoad.product_group_name === '') {
      document.getElementById('groupName').focus();
      this._confirmationDialogComponent.openAlertDialog("Select product group name", "Product Group Attributes");
      return false;
    } else if (this.objLoad.product_group_name !== '' && this.objLoad.product_group_id == 0) {
      document.getElementById('groupName').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid product group name", "Product Group Attributes");
      this.objLoad.product_group_name = '';
      return false;
    } else return true;
  }

  ////////////////// Attribute Lookup ///////////////////////

  private getAttributesList(): void {
    debugger;
    let objData: any = [{
      group_section_id: +this._localStorage.getGlobalGroupSectionId()
    }];
    let objDetails: any = {
      GetAttributeLookup: JSON.stringify(objData)
    }
    this._productGroupAttributesService.getAttributesLookup(objDetails).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0) {
        debugger;
        this.attributeList = JSON.parse(result);
      }
    });
  }

  public openAttributeGroupLookup(event: any): void {
    debugger;
    if (this.objProductAttributes.attribute_name === "") {
      this.objProductAttributes.attribute_id = 0;
      this.checkEmptyAtributeValue();
    }
    if (event.keyCode === 13) {
      this.openAttributeLookupDialog();
    }
  }

  private openAttributeLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AttributeLookupComponent, {
      width: "500px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objProductAttributes.attribute_name
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      debugger;
      this.focusFlag = false;
      if (result) {
        this.objProductAttributes.attribute_id = result.attribute_id;
        this.objProductAttributes.attribute_name = result.attribute_name.toString().trim();
        this.fetchProductGroupDetails();
      } else {
        this.objProductAttributes.attribute_id = 0;
        this.checkEmptyAtributeValue();
      }
    });
  }

  public onEnterAttributeLookupDetails(): void {
    debugger;
    let attributeList: any = {
      attribute_id: 0,
      attribute_name: "",
    }
    attributeList = this.attributeList.find(emp => emp.attribute_name.toLowerCase().trim() ===
      this.objProductAttributes.attribute_name.toString().trim().toLowerCase());
    this.objProductAttributes.attribute_id = attributeList && attributeList.attribute_id ? attributeList.attribute_id : 0;
    this.objProductAttributes.attribute_name = attributeList && attributeList.attribute_name ?
      attributeList.attribute_name : this.objProductAttributes.attribute_name.toString().trim();
    this.objProductAttributes.attribute_id !== 0 ? this.fetchProductGroupDetails() : this.checkEmptyAtributeValue();
  }

  ////////////////// Attribute Lookup End ///////////////////////

  private checkEmptyAtributeValue(): void {
    this.objProductAttributes.attributeValue = [];
    this.unChangedProductAttributes.attributeValue = [];
  }

  public loadProductGroupDetails(): void {
    debugger;
    this.matTableConfig([]);
    let obj = [{
      product_group_id: +this.objLoad.product_group_id
    }]
    let objLoad = {
      ProductGroupAttribute: JSON.stringify(obj)
    }
    this._productGroupAttributesService.GetProductGroupAttributeDetails(objLoad).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0) {
        this.loadProductGroupList = JSON.parse(result);
        this.matTableConfig(this.loadProductGroupList);
      }
    });
  }

  private fetchProductGroupDetails(): void {
    debugger;
    let obj = [{
      product_group_id: +this.objProductAttributes.product_group_id,
      attribute_id: +this.objProductAttributes.attribute_id
    }]
    let objLoad = {
      FetchProductGroupAttribute: JSON.stringify(obj)
    }
    this._productGroupAttributesService.FetchProductGroupAttributeDetails(objLoad).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0) {
        this.objProductAttributes.attributeValue = JSON.parse(result);
        console.log(this.objProductAttributes.attributeValue, 'fetch')
      }
    });
  }

  private beforeSaveValidate(): boolean {
    if (this.objProductAttributes.attribute_name === '') {
      document.getElementById('name').focus();
      this._confirmationDialogComponent.openAlertDialog("Select attribute name", "Product Group Attributes");
      return false;
    } else if (this.objProductAttributes.attribute_name !== '' && this.objProductAttributes.attribute_id == 0) {
      document.getElementById('name').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid attribute name", "Product Group Attributes");
      this.objProductAttributes.attribute_name = '';
      return false;
    } else if (this.objProductAttributes.attributeValue.length === 0) {
      this._confirmationDialogComponent.openAlertDialog("No record to save", "Product Group Attributes");
      return false;
    } else return true;
  }

  private beforeSaveProductGroupAttributeValidate(tempArray: any, saveData: any): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(tempArray.length) !== -1) {
      this._confirmationDialogComponent.openAlertDialog('Select atleast one attribute value', 'Product Group Attributes');
      return false;
    } else if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(saveData.length) !== -1) {
      this._confirmationDialogComponent.openAlertDialog('Select atleast one attribute value', 'Product Group Attributes');
      return false;
    } else return true;
  }

  public onSaveModuleDetails(): void {
    debugger;
    let tempArray: any = [];
    if (this.beforeSaveValidate()) {
      for (let i = 0; i < this.objProductAttributes.attributeValue.length; i++) {
         if (this.objProductAttributes.attributeValue[i].is_selected === true) {
          tempArray.push({
            attribute_value_id: this.objProductAttributes.attributeValue[i].attribute_value_id,
          });
        }
      }
      let saveData = [{
        product_group_id: +this.objProductAttributes.product_group_id,
        attribute_id : +this.objProductAttributes.attribute_id,
        attribute_value_data: tempArray
      }]
      let objSave = {
        AddGroupAttributes: JSON.stringify(saveData)
      }
     if (this.beforeSaveProductGroupAttributeValidate(saveData,tempArray)) {
      this._productGroupAttributesService.addProductGroupAttributes(objSave).subscribe(
        (result: any) => {
          if (result) {
            this._confirmationDialogComponent.openAlertDialog("All records has been saved", "Product Group Attributes");
            this.componentVisibility = !this.componentVisibility;
            this.resetScreen();
            this.resetLoadScreen();
          }
        });
    }
  }
  }


  //////////////////////////////////////////////////////////////////////

  private resetScreen(): void {
    this.objProductAttributes.attribute_id = 0;
    this.objProductAttributes.attribute_name = "";
    this.objProductAttributes.attributeValue = [];
    this.unChangedProductAttributes.attributeValue = [];
  }

  public onClickDelete(index: any): any {
    debugger;
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this._dialogRef.componentInstance.confirmMessage =
      "Do you want to remove this attribute name?"
    this._dialogRef.componentInstance.componentName = "Product Group Attributes";
    return this._dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.removeProductGroupAttributes(index);
      }
      this._dialogRef = null;
    });
  }

  private removeProductGroupAttributes(index: any): void {
    debugger;
    let objData = [{
      product_group_id: +this.dataSource.data[index].product_group_id,
      attribute_value_id: +this.dataSource.data[index].attribute_value_id
    }];
    let objRemoveDetails: any = {
      RemoveGroupAttributes: JSON.stringify(objData)
    }
    this._productGroupAttributesService.removeProductGroupAttributes(objRemoveDetails).subscribe(
      (result: any) => {
        if (result === 1) {
          this._confirmationDialogComponent.openAlertDialog("Removed", "Product Group Attributes");
          this.loadProductGroupDetails();
        }
      });
    this._dialogRef = null;
  }

  private trimStringFields(): void {
    this.objProductAttributes.attribute_name = this.objProductAttributes.attribute_name.toString().trim();
    this.unChangedProductAttributes.attribute_name = this.unChangedProductAttributes.attribute_name.toString().trim();
  }

  public onClear(exitFlag: boolean): void {
    this.trimStringFields();
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objProductAttributes) !== JSON.stringify(this.unChangedProductAttributes)) {
      return true;
    } else {
      return false;
    }
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Product Group Attributes";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  public exportToExcel(): void {
    debugger;
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          AttributeName: this.dataSource.data[i].attribute_name,
          AttributeValue: this.dataSource.data[i].attribute_value
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Product Group Attributes",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Product Group Attributes");
  }

  public exportToPdf(): void {
    debugger
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.loadProductGroupList.forEach(e => {
        var tempObj = [];
        tempObj.push(e.attribute_name, e.attribute_value);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Attribute Name', 'Attribute Value']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Product Group Attributes' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Product Group Attributes");
  }
}
