import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductGroupAttributesComponent } from './product-group-attributes.component';

describe('ProductGroupAttributesComponent', () => {
  let component: ProductGroupAttributesComponent;
  let fixture: ComponentFixture<ProductGroupAttributesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductGroupAttributesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductGroupAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
