import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatRadioChange, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';

import { EditMode, GroupSections, Rights } from 'src/app/common/models/common-model';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductAttributes } from '../../../model/masters/product-attributes.model';
import { ProductAttributesService } from '../../../services/masters/product-attributes/product-attributes.service';
import { DatePipe } from '@angular/common';
import { CommonService } from 'src/app/common/services/common/common.service';
declare var jsPDF: any;
@Component({
  selector: 'app-product-attributes',
  templateUrl: './product-attributes.component.html',
  styleUrls: ['./product-attributes.component.scss'],
  providers: [DatePipe]
})
export class ProductAttributesComponent implements OnInit {

  componentVisibility: boolean = true;

  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Attribute_Name", "Action"];

  Group_Section_Name: string = this._localStorage.getGroupSectionName();

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    // isView: false
  };

  objUnchangedAction: EditMode = {
    isEditing: false
  }

  groupSectionsList: GroupSections[] = [];
  unChangedGroupSectionsList: GroupSections[] = [];
  modifyGroupSectionsList: GroupSections[] = [];

  objProductAttributes: ProductAttributes = {
    Group_Section_Id:[],
    Attribute_Id: 0,
    Attribute_Name: "",
    Active: true
  }

  unChangedProductAttributes: ProductAttributes = {
    Group_Section_Id:[],
    Attribute_Id: 0,
    Attribute_Name: "",
    Active: true
  }

  objModifyProductAttributes: ProductAttributes = {
    Group_Section_Id:[],
    Attribute_Id: 0,
    Attribute_Name: "",
    Active: true
  }

  objProductLoad: any = {
    TimeLine: 1
  }

  rights: Rights = {
    Add: false,
    Update: false,
    Delete: false
  };

  keysOfProductAttributes: any[] = [
    { isCheck: true, key: "Attribute_Name" },
    { isCheck: false, key: "Active" },
  ];

  unChangedKeysOfProductAttributes: any[] = [
    { isCheck: true, key: "Attribute_Name" },
    { isCheck: false, key: "Active" },
  ];

  productAttributeList: ProductAttributes[];
  filterproductAttributeList: any[];
  searchString: string;

  @ViewChild(MatSort, null) sort: MatSort;

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    public _datePipe: DatePipe,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _matDialog: MatDialog,
    public _keyPressEvents: KeyPressEvents,
    public _productAttributeService: ProductAttributesService,
    public _excelService: ExcelService,
    private _commonService: CommonService,) { }

  ngOnInit() {
    this.getProductAttributes();
    this.getGroupSections();
    // this.getRights();
  }

  private getRights(): void {
    var rights = this._localStorage.getMenuRights("/ProductAttributes");
    console.log('rights', rights);
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "Delete" && rights[x].Status == true) {
        this.rights.Delete = true;
      }
    }
  }

  public newClick(): void {
    // if (this.rights.Add) {
    this.objAction.isEditing = false;
    this.objAction.isView = false;
    this.objAction = JSON.parse(JSON.stringify(this.unChangedProductAttributes));
    this.groupSectionsList = JSON.parse(JSON.stringify(this.unChangedGroupSectionsList));
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
    // }
  }

  public onListClick(): void {
    this.getProductAttributes();
    this.objAction = JSON.parse(JSON.stringify(this.unChangedAction));
    this.groupSectionsList = JSON.parse(JSON.stringify(this.unChangedGroupSectionsList));
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  private getProductAttributes(): void {
    let objData = {
      Group_Section_Id: +this._localStorage.getGlobalGroupSectionId(),
    }
    this._productAttributeService.getProductAttributes(objData).subscribe((result: any[]) => {
      this.productAttributeList = result;
      this.productAttributeList !== null ? this.loadActiveRecords() : this._confirmationDialogComponent.openAlertDialog("No record found", "Product Attributes");
    });
  }

  private getGroupSections(): void {
    this._commonService.getGroupSections().subscribe((result: GroupSections[]) => {
      if (result) {
        this.groupSectionsList = JSON.parse(JSON.stringify(result));
        this.modifyGroupSectionsList = JSON.parse(JSON.stringify(result));
        this.unChangedGroupSectionsList = JSON.parse(JSON.stringify(result));
        this.groupSectionsList.every(t => t.checked = false);
        this.modifyGroupSectionsList.every(t => t.checked = false);
        this.unChangedGroupSectionsList.every(t => t.checked = false);
      }
    });
  }

  private loadActiveRecords(): void {
    debugger;
    this.objProductLoad.TimeLine = 1;
    this.filterproductAttributeList = [];
    this.productAttributeList.forEach(ele => {
      if (ele.Active === true)
        this.filterproductAttributeList.push(ele);
    });
    this.matTableConfig(this.filterproductAttributeList);
  }

  private matTableConfig(tableRecords: any[]): void {
    this.dataSource = new MatTableDataSource(tableRecords);
    this.dataSource.sort = this.sort;
  }

  public onAttributeValueClick(productAttributes): void {
    localStorage.setItem("ProductAttributes", btoa(JSON.stringify(productAttributes)));
    this._router.navigate(["/Inventory/ProductAttributeValues"]);
  }

  public addProductAttributes(): void {
    if (this.beforeSaveValidate()) {
      debugger
      let data = [{
        attribute_id: this.objProductAttributes.Attribute_Id,
        attribute_name: this.objProductAttributes.Attribute_Name.toString().trim(),
        active: this.objProductAttributes.Active,
        group_section_id: this.objProductAttributes.Group_Section_Id,
      }]
      let objAttribute = {
        Product_Attribute_List: JSON.stringify(data)
      }
      this._productAttributeService.addProductAttributes(objAttribute).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New product attribute record has been created", "Product Attributes");
        }
        if (this.objAction.isEditing === true) {
          if (this.objProductAttributes.Active == this.objModifyProductAttributes.Active) {
            this.resetScreen();
            this.getProductAttributes();
            this.componentVisibility = !this.componentVisibility;
          }
          this.objModifyProductAttributes.Active = !this.objModifyProductAttributes.Active;
        } else {
          this.resetScreen();
          this.getProductAttributes();
          this.componentVisibility = !this.componentVisibility;
        }
      });
    }
  }

  // public modifyOrViewProductAttributes(product: ProductAttributes, isEditing: boolean): void {
  //   this.objAction = {
  //     isEditing: isEditing ? true : false,
  //     isView: isEditing ? false : true,
  //   }
  //   this.objProductAttributes = Object.assign({}, product);
  //   this.objModifyProductAttributes = Object.assign({}, product);
  //       this.setSelectedGroupSections(result[0].group_section_id);
  //   this.keysOfProductAttributes[1].isCheck = true;
  //   this.componentVisibility = !this.componentVisibility;
  // }

  public modifyOrViewProductAttributes(i: number): void {
    let objFetch = {
      Product_Attribute_List: JSON.stringify([{
        attribute_id: this.dataSource.data[i].Attribute_Id,
        group_section_id: this._localStorage.getGlobalGroupSectionId(),
      }])
    }
    this._productAttributeService.modifyProductAttributes(objFetch).subscribe((result: any) => {
      if (result) {
        debugger;
        this.objAction.isEditing = true;
        this.objProductAttributes = JSON.parse(JSON.stringify(result[0]));
        this.objModifyProductAttributes = JSON.parse(JSON.stringify(result[0]));
        this.setSelectedGroupSections(result[0].Group_Section_Id);
        this.componentVisibility = !this.componentVisibility;
      }
    })
  }

  public onChangeSectionSelection(event: any, i: number): void {
    debugger;
    if (event.checked) {
      this.objProductAttributes.Group_Section_Id.push(this.groupSectionsList[i].Group_Section_ID);
    } else if (!event.checked) {
      this.objProductAttributes.Group_Section_Id.splice(this.objProductAttributes.Group_Section_Id.indexOf(this.groupSectionsList[i].Group_Section_ID), 1);
    }
  }

  private setSelectedGroupSections(sections: any): void {
    debugger;
    if (sections) {
      for (let i = 0; i < sections.length; i++) {
        let k = this.groupSectionsList.findIndex(x => x.Group_Section_ID === +sections[i]);
        this.groupSectionsList[k].checked = true;
        this.modifyGroupSectionsList[k].checked = true;
      }
    }
  }

  /****************************************** Validations **********************************************/

  private beforeSaveValidate(): boolean {
    if (!this.objProductAttributes.Attribute_Name.toString().trim()) {
      document.getElementById('attributeName').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Attribute name", "Product Attributes");
      return false;
    }
    if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(+this.objProductAttributes.Group_Section_Id.length) !== -1) {
      // document.getElementById('category').focus();
      this._confirmationDialogComponent.openAlertDialog('Select atleast one section name', 'Product Attributes');
      return false;
    }
    else {
      return true;
    }
  }

  public radioChange(event: MatRadioChange): void {
    this.filterproductAttributeList = [];
    if (event.value === 1) {
      this.productAttributeList.forEach(element => {
        if (+element.Active === 1) {
          this.filterproductAttributeList.push(element);
        }
      });
    } else if (event.value === 0) {
      this.productAttributeList.forEach(element => {
        if (+element.Active === 0) {
          this.filterproductAttributeList.push(element);
        }
      });
    } else {
      this.productAttributeList.forEach(element => {
        this.filterproductAttributeList.push(element);
      });
    } this.matTableConfig(this.filterproductAttributeList);
  }

  public changeFlagConfirmation(): void {
    if (this.objAction.isEditing === true) {
      this.openFlagConfirmationDialogs(this.objProductAttributes.Active == false ? "Do you want to Deactivate this record?" : "Do you want to make this record active?")
    }
  }

  private openFlagConfirmationDialogs(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Product Attributes";
    dialogRef.afterClosed().subscribe(result => {
      result ? this.addProductAttributes() : this.objProductAttributes.Active = this.objProductAttributes.Active ? false : true;
      dialogRef = null;
    });
  }

  public removeSpecialCharacters(event: any) {
    this.objProductAttributes.Attribute_Name = event.target.value.replace(/[^a-zA-Z ]/g, "");
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objProductAttributes) !== (this.objAction.isEditing ? JSON.stringify(this.objModifyProductAttributes) : JSON.stringify(this.unChangedProductAttributes)))
      return true;
    else return false;
  }

  // private checkAnyChangesMade(): boolean {
  //   for (let i = 0; i < this.keysOfProductAttributes.length; i++) {
  //     if (this.keysOfProductAttributes[i].isCheck)
  //       if (this.objProductAttributes[this.keysOfProductAttributes[i].key] !==
  //         (!this.objAction.isEditing ? this.unChangedProductAttributes[this.keysOfProductAttributes[i].key]
  //           : this.objModifyProductAttributes[this.keysOfProductAttributes[i].key]))
  //         return true;
  //   }
  //   return false;
  // }

  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Product Attributes";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  private resetScreen(): void {
    // this.objAction = Object.assign({}, this.unChangedAction);
    // this.objProductAttributes = Object.assign({}, this.unChangedProductAttributes);
    // this.objModifyProductAttributes = Object.assign({}, this.unChangedProductAttributes);
    // this.groupSectionsList = JSON.parse(JSON.stringify(this.modifyGroupSectionsList));
    // this.keysOfProductAttributes = this.unChangedKeysOfProductAttributes;
    this.objProductAttributes = JSON.parse(JSON.stringify(this.unChangedProductAttributes));
    this.objModifyProductAttributes = JSON.parse(JSON.stringify(this.unChangedProductAttributes));
    this.groupSectionsList = JSON.parse(JSON.stringify(this.unChangedGroupSectionsList));
    this.objAction = JSON.parse(JSON.stringify(this.objUnchangedAction));
  }

  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Attribute Name': this.dataSource.data[i].Attribute_Name,
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Product Attributes",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Product Attributes");
  }

  exportToPdf(): void {
    debugger
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.productAttributeList.forEach(e => {
        var tempObj = [];
        tempObj.push(e.Attribute_Name);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Attribute Name']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Product Attributes' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Product Attributes");
  }

}
