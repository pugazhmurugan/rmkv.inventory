import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionMaterialRequirementsComponent } from './production-material-requirements.component';

describe('ProductionMaterialRequirementsComponent', () => {
  let component: ProductionMaterialRequirementsComponent;
  let fixture: ComponentFixture<ProductionMaterialRequirementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionMaterialRequirementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionMaterialRequirementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
