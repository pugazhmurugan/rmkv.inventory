import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog, MatDialogRef, MatRadioChange, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode } from 'src/app/common/models/common-model';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { MaterialLookupComponent } from 'src/app/common/shared/material-lookup/material-lookup.component';
import { ProductionMaterialRequirementService } from '../../../services/masters/Production-Material-Requirements/production-material-requirement.service';
import { ProductionMaterialsService } from '../../../services/masters/production-materials/production-materials.service';
import { ProductionProductsService } from '../../../services/masters/Production-Product/production-products.service';
declare var jsPDF: any;

@Component({
  selector: 'app-production-material-requirements',
  templateUrl: './production-material-requirements.component.html',
  styleUrls: ['./production-material-requirements.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class ProductionMaterialRequirementsComponent implements OnInit {



  columns: any[] = [
    { display: 'Sno', editable: false },
    { display: 'materialName', editable: true },
    { display: 'uom', editable: false },
    { display: 'requiredQuantity', editable: true },
    { display: 'action', editable: true },
  ];

  @ViewChildren('materialName') materialName: ElementRef | any;
  @ViewChildren('uom') uom: ElementRef | any;
  @ViewChildren('requiredQuantity') requiredQuantity: ElementRef | any;
  @ViewChildren('action') action: ElementRef | any;
  @ViewChild(MatSort, null) sort: MatSort;

  getProductList: any[] = [];
  getProductSizeList: any[] = [];
  componentVisibility: boolean = true;
  focusFlag: any;
  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "prod_name", "Size", "Action"];
  searchString: string = "";
  filterProductNameList: any[] = [];
  filterProductionList: any[] = [];
  materialLookupList: any = [];

  objProdMaterial: any = {
    material_id: 0,
    material_name: '',
    uom_description: '',
    req_quantity: '',
    prod_id: '',
    prod_name: '',
    prod_size: '',
    mat_req_id: '',
    active: true,
  }

  objModifyProdMaterial: any = {
    material_id: 0,
    material_name: '',
    uom_description: '',
    req_quantity: '',
    prod_id: '',
    prod_name: '',
    prod_size: '',
    mat_req_id: '',
    active: true,
  }

  objUnchangedProdMaterial: any = {
    material_id: 0,
    material_name: '',
    uom_description: '',
    req_quantity: '',
    prod_id: '',
    prod_name: '',
    prod_size: '',
    mat_req_id: '',
    active: true,
  }

  objProductionMaterialList: any = {
    material_id: 0,
    material_name: '',
    uom_description: '',
    req_quantity: '',
    materialdata: '',
  }

  getProductionMaterialList: any[] = [{
    material_id: 0,
    material_name: '',
    uom_description: '',
    req_quantity: '',
    materialdata: '',
  }]

  getModifyProductionMaterialList: any[] = [{
    material_id: 0,
    material_name: '',
    uom_description: '',
    req_quantity: '',
    materialdata: '',
  }]

  getUnchangedProductionMaterialList: any[] = [{
    material_id: 0,
    material_name: '',
    uom_description: '',
    req_quantity: '',
    materialdata: '',
  }]

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  objUnchangedAction: EditMode = {
    isEditing: false,
    isView: false
  };
  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    public _keyPressEvents: KeyPressEvents,
    public _matDialog: MatDialog, private _datePipe: DatePipe,
    public _excelService: ExcelService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _gridKeyEvents: GridKeyEvents,
    public _productionMaterialRequirementService: ProductionMaterialRequirementService,
    public _productionMaterialService: ProductionMaterialsService,
    public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _productionProductsService: ProductionProductsService
  ) {
    this.getProductionProduct();
  }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        let response = this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.getProductionMaterialList);
        if (response && this.checkEmptyFieldForGrid(rowIndex))
          this.addNewRowToGrid();
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.getProductionMaterialList);
        break;

      case 38: // Arrow Up
        debugger;
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right\
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.getProductionMaterialList);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.getProductionMaterialList);
        break;
    }
  }

  ngOnInit() {
    this.getProductionMaterialRequirements();
    this.addNewRowToGrid();
    this.getMaterialNameList();
  }
  public matTableConfig(tableRecords: any[]): void {
    this.dataSource = new MatTableDataSource(tableRecords);
    this.dataSource.sort = this.sort;
  }

  /*************************************************** Functionalities ************************************************/

  public searchRights(searchValue: string): void {
    searchValue = searchValue.toString().trim();
    searchValue = searchValue ? searchValue.toString().toLocaleLowerCase() : "";
    this.searchString = searchValue;
    let filterProductNameList = this.filterProductNameList.filter(element =>
      element.prod_name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    this.matTableConfig(filterProductNameList);
  }

  public newClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.resetScreen();
  }

  public getProductionProduct(): void {
    this.getProductionMaterialList.length = 0;
    this._productionProductsService.getProductionProduct().subscribe((result: any) => {
      if (result) {
        this.getProductList = JSON.parse(JSON.stringify(result));
      }
    });
  }

  public getProductionProductSize(flag?: boolean, isModify?: boolean): any {
    if (flag === true) {
      this.getProductionMaterialList.length = 0;
      this.getProductionMaterialList.push(Object.assign({}, this.objProductionMaterialList));
      this.objProdMaterial.prod_size = '';
    }
    let objData = {
      ProdProduct: JSON.stringify([{
        prod_id: this.objProdMaterial.prod_id,
      }])
    }
    this._productionProductsService.FetchProductionProduct(objData).subscribe((result: any) => {
      if (result) {
        this.getProductSizeList = JSON.parse(JSON.stringify(result))[0].sizedata;
        if (isModify)
          this.objProdMaterial.prod_size = this.getProductSizeList[this.getProductSizeList.findIndex(x => x.prod_size.toString().trim() === this.objProdMaterial.prod_size.toString().trim())].prod_size;
      }
    });
  }

  // public radioChange(event: MatRadioChange): void {
  //   debugger;
  //   this.filterProductionList = [];
  //   if (event.value === 1) {
  //     this.filterProductNameList.forEach(element => {
  //       if (element.active === true) {
  //         this.filterProductionList.push(element);
  //       }
  //     });
  //     this.matTableConfig(this.filterProductionList);
  //   } else if (event.value === 0) {
  //     this.filterProductNameList.forEach(element => {
  //       if (element.active === false) {
  //         this.filterProductionList.push(element);
  //       }
  //     });
  //     this.matTableConfig(this.filterProductionList);
  //   } else {
  //     this.filterProductNameList.forEach(element => {
  //       this.filterProductionList.push(element);
  //     });
  //     this.searchRights(this.searchString)
  //   }
  // }

  private onListClick(): void {
    this.objAction = JSON.parse(JSON.stringify(this.objUnchangedAction));
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    //  this.trimStringFields();
    if (JSON.stringify(this.objProdMaterial) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.objModifyProdMaterial) :
      JSON.stringify(this.objUnchangedProdMaterial)) ||
      JSON.stringify(this.getProductionMaterialList) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.getModifyProductionMaterialList) :
        JSON.stringify(this.getUnchangedProductionMaterialList))) {
      return true;
    } else {
      return false;
    }
  }

  // private trimStringFields(): void {
  //   debugger;
  //   // this.objProdMaterial.prod_name = this.objProdMaterial.prod_name.toString().trim();
  //   // this.objModifyProdMaterial.prod_name = this.objModifyProdMaterial.prod_name.toString().trim();
  //   // this.objUnchangedProdMaterial.prod_name = this.objUnchangedProdMaterial.prod_name.toString().trim();
  //   this.getProductionMaterialList.forEach(x => {
  //     x.material_name = x.material_name.toString().trim()
  //   });
  //   this.getModifyProductionMaterialList.forEach(x => {
  //     x.material_name = x.material_name.toString().trim()
  //   });
  //   this.getUnchangedProductionMaterialList.forEach(x => {
  //     x.material_name = x.material_name.toString().trim()
  //   });
  // }



  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Production Material Requirements";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  public addNewRowToGrid(): void {
    debugger;
    this.getProductionMaterialList.push(Object.assign({}, this.objProductionMaterialList));
    setTimeout(() => {
      let input = this.materialName.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100);
  }

  public deleteRowFromGrid(index) {
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this._dialogRef.componentInstance.confirmMessage =
      "Do you want to remove this row?"
    this._dialogRef.componentInstance.componentName = "Photo Shoot Collection Entry";
    return this._dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getProductionMaterialList.splice(index, 1);
      }
      this._dialogRef = null;
    });
  }

  // public changeFlagConfirmation(): void {
  //   if (this.objAction.isEditing === true) {
  //     this.openFlagConfirmationDialogs(this.objProdMaterial.active == false ? "Do you want to Deactivate this records?" : "Do you want to make this record active?")
  //   }
  // }

  // public openFlagConfirmationDialogs(message: string, exitFlag?: boolean): void {
  //   let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
  //     panelClass: "custom-dialog-container",
  //     data: { confirmationDialog: 1 }
  //   });
  //   dialogRef.componentInstance.confirmMessage = message;
  //   dialogRef.componentInstance.componentName = "Product Category";
  //   dialogRef.afterClosed().subscribe(result => {
  //     result ? this.saveProductionMaterial() : this.objProdMaterial.active = this.objProdMaterial.active ? false : true;
  //     dialogRef = null;
  //   });
  // }

  public validateMaterialName(i: number): boolean {
    let index = this.materialLookupList.findIndex(element => element.material_name.toString().trim() === this.getProductionMaterialList[i].material_name.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  private getMaterialNameList(): void {
    debugger;
    this._productionMaterialService.getProductionMaterialDetails().subscribe((result: any) => {
      if (result) {
        debugger;
        this.materialLookupList = result.filter(x => x.active === true);
      }
    });
  }

  public checkValidMaterialName(event: KeyboardEvent,i: number, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.getProductionMaterialList[i].material_name.toString().trim() !== ''
      && this.getProductionMaterialList[i].material_id === 0) {
      this.openAlertDialog("Invalid material name", "Production Material Requirements", focus,i);
    }
  }

  private openAlertDialog(value: string, componentName: string, focus: string,i: number) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this[focus].toArray();
        input[i].nativeElement.focus();  
      }
       
      _dialogRef = null;
    });
  }

  public openMaterialLookup(event: KeyboardEvent | any, i: number): void {
    debugger;

    if (event.keyCode === 13 && !this.validateMaterialName(i)) {
      this.openMaterialLookupDialog(i);
    }

  }

  private openMaterialLookupDialog(i: number): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(MaterialLookupComponent, {
      width: "550px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.getProductionMaterialList[i].material_name,
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.getProductionMaterialList[i].material_id = result.material_id;
        this.getProductionMaterialList[i].material_name = result.material_name.toString().trim();
        this.getProductionMaterialList[i].uom_description = result.uom_description.toString().trim();
        this.checkExistMaterialName(i)
      } else {
        this.getProductionMaterialList[i].material_id = '';
        this.getProductionMaterialList[i].material_name = '';
        this.getProductionMaterialList[i].uom_description = '';
      }
    });
  }

  public onEnterMaterialLookupDetails(i: number): void {
    debugger;
    let materialLookupList: any = {
      material_id: 0,
      material_name: "",
      uom_description: "",
    };
    materialLookupList = this.materialLookupList.find(act => act.material_name.toLowerCase().trim() === this.getProductionMaterialList[i].material_name.toString().trim().toLowerCase());
    this.getProductionMaterialList[i].material_id = materialLookupList && materialLookupList.material_id ? materialLookupList.material_id : 0;
    this.getProductionMaterialList[i].material_name = materialLookupList && materialLookupList.material_name ? materialLookupList.material_name.toString().trim() : this.getProductionMaterialList[i].material_name.toString().trim();
    this.getProductionMaterialList[i].uom_description = materialLookupList && materialLookupList.uom_description ? materialLookupList.uom_description : '';
    this.checkExistMaterialName(i);
  }

  private checkExistMaterialName(index: number): void {
    debugger;
    let alreadyFlag = false;
    let materialName = '';
    for (let i = 0; i < this.getProductionMaterialList.length; i++) {
      if (i !== index && this.getProductionMaterialList[i].material_name === this.getProductionMaterialList[index].material_name) {
        materialName = this.getProductionMaterialList[i].material_name;
        alreadyFlag = true;
        break;
      }
    } if (alreadyFlag) {
      this._confirmationDialogComponent.openAlertDialog(materialName+ ' ' + 'Already exists', 'Production Material Requirements');
      this.getProductionMaterialList[index].material_name = '';
      this.getProductionMaterialList[index].uom_description = '';
    }
  }

  public saveProductionMaterial(): void {
    debugger;
    if (this.beforeSaveValidate()) {
      let details: any = [];
      for (let i = 0; i < this.getProductionMaterialList.length; i++) {
        if (this.getProductionMaterialList.length === 1 && this.getProductionMaterialList[i].material_name === '') {
          let input = this.materialName.toArray();
          input[0].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog("No record to save", "Photo Shoot Collection Entry");
          return;
        } else if (this.getProductionMaterialList.length !== 0 && this.getProductionMaterialList[i].material_name === '') {
          this.getProductionMaterialList.splice(i, 1);
          continue;
        } if (!this.checkEmptyFieldForGrid(i)) {
          return;
        }
        let element = this.getProductionMaterialList[i];
        details.push({
          serial_no: i + 1,
          material_id: +element.material_id,
          // uom_description: element.uom_description,
          req_quantity: +element.req_quantity,
        })
      }

      let obj: any = {
        ProdMaterialRequirements: JSON.stringify([{
          prod_id: +this.objProdMaterial.prod_id,
          prod_size: this.objProdMaterial.prod_size,
          entered_by: this._localStorage.intGlobalUserId(),
          active: this.objProdMaterial.active,
          details: details,
        }])
      }
      this._productionMaterialRequirementService.saveProductionMaterialRequirements(obj).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? 'Updated Successfully' : 'Saved Successfully', 'Photo Shoot Collection');
          this.getProductionMaterialRequirements();
          this.resetScreen();
          this.componentVisibility = !this.componentVisibility;
        }
      })
    }
  }

  private getProductionMaterialRequirements(): void {
    debugger;
    this.matTableConfig([]);
    this._productionMaterialRequirementService.getProductionMaterialRequirements().subscribe((result: any) => {
      if (JSON.parse(JSON.stringify(result))) {
        debugger;
        this.matTableConfig(JSON.parse(JSON.stringify(result)));
        this.filterProductNameList = JSON.parse(JSON.stringify(result));
      }
    });
  }



  public modifyProductionMaterialRequirements(isEditing: boolean, index: number, isView?: boolean, flag?: boolean): any {
    if (flag === false) {
      this.getProductionMaterialList.length = 0;
      this.getProductionMaterialList.push(Object.assign({}, this.objProductionMaterialList));
    }
    this.objAction = {
      isEditing: isEditing ? true : false,
      isView: isView ? true : false
    }
    let objData = {
      ProdMaterialRequirements: JSON.stringify([{
        prod_id: flag ? +this.dataSource.data[index].prod_id : this.objProdMaterial.prod_id,
        prod_size: flag ? this.dataSource.data[index].prod_size : this.objProdMaterial.prod_size
      }])
    }
    this._productionMaterialRequirementService.fetchProductionMaterialRequirements(objData).subscribe((result: any) => {
      debugger;
      if (JSON.parse(result)) {
        console.log(result, 'fetch')
        // flag ?  this.getProductionProductSize() : this.objProdMaterial.prod_size = '';
        this.objProdMaterial = JSON.parse(result)[0];
        this.objModifyProdMaterial = JSON.parse(result)[0];
        this.getProductionMaterialList = JSON.parse(result);
        this.getModifyProductionMaterialList = JSON.parse(result);
        this.getProductionProductSize(false, true);
        if (isEditing || isView) {
          this.componentVisibility = !this.componentVisibility;
        }
      }
    });
  }

  /*************************************************** Validations ************************************************/

  private beforeSaveValidate(): boolean {
    if (!this.objProdMaterial.prod_id.toString().trim()) {
      document.getElementById('productName').focus();
      this._confirmationDialogComponent.openAlertDialog("Select product name", "Production Material Requirements");
      return false;
    } else if (!this.objProdMaterial.prod_size.toString().trim()) {
      document.getElementById('productSize').focus();
      this._confirmationDialogComponent.openAlertDialog("Select product size", "Production Material Requirements");
      return false;
    } else
      return true;
  }

  public checkEmptyFieldForGrid(index: number): boolean {
    if (!this.getProductionMaterialList[index].material_name.toString().trim()) {
      let input = this.materialName.toArray();
      input[index].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter material name", "Production Material Requirements");
      return false;
    } else if (!+this.getProductionMaterialList[index].req_quantity) {
      let input = this.requiredQuantity.toArray();
      input[index].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter quantity", "Production Material Requirements");
      return false;
    } else return true;
  }

  public beforeEnterMaterialNameValidation(): boolean {
    if (this.objProdMaterial.prod_id === '') {
      document.getElementById("productName").focus();
      this._confirmationDialogComponent.openAlertDialog("Select product name", "Production Material Requirements");
      return true;
    } else if (this.objProdMaterial.prod_size === '') {
      document.getElementById("productSize").focus();
      this._confirmationDialogComponent.openAlertDialog("Select product size", "Production Material Requirements");
      return true;
    } else return false;
  }
  public beforeEnterQuantityValidation(index: number): boolean {
    if (this.getProductionMaterialList[index].material_name === '') {
      document.getElementById('materialName').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter material name", "Production Material Requirements");
      return true;
    } else return false;
  }
  public checkProductName(): boolean {
    if (this.objProdMaterial.prod_id === '') {
      document.getElementById("productName").focus();
      this._confirmationDialogComponent.openAlertDialog("Select product name", "Production Material Requirements");
      return true;
    } else return false
  }

  private checkValidDetails(rowIndex: number, colIndex: number): boolean {
    debugger;
    if (this.columns[colIndex].display === 'materialName') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(this.getProductionMaterialList[rowIndex].material_name) !== -1) {
        this.openInvalidAlertDialog('Enter product group', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else if (this.columns[colIndex].display === 'requiredQuantity') {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this.getProductionMaterialList[rowIndex].req_quantity) !== -1) {
        this.openInvalidAlertDialog('Enter quantity', rowIndex, 'Invoices', this.columns[colIndex].display);
        return false;
      } else return true;
    } else return true;
  }

  private openInvalidAlertDialog(value: string, i: number, componentName: string, focus: any) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this[focus].toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  public onlyAllwDecimalForQuantity(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public onlyAllwNumbersForQuantity(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    }
  }

  public checkViewData(index: number): boolean {
    if (this.getProductionMaterialList[index].material_name === '') {
      return true;
    } if (this.getProductionMaterialList[index].req_quantity === '') {
      return true;
    } if (this.objAction.isView === true) {
      return true;
    } else return false;
  }

  private resetScreen(): void {
    debugger;
    if (this.objAction.isEditing) {
      this.objProdMaterial = JSON.parse(JSON.stringify(this.objModifyProdMaterial));
      this.objModifyProdMaterial = JSON.parse(JSON.stringify(this.objModifyProdMaterial));
      this.getProductionMaterialList = JSON.parse(JSON.stringify(this.getModifyProductionMaterialList));
      this.getModifyProductionMaterialList = JSON.parse(JSON.stringify(this.getModifyProductionMaterialList));
    } else {
      this.objProdMaterial = JSON.parse(JSON.stringify(this.objUnchangedProdMaterial));
      this.getProductionMaterialList = JSON.parse(JSON.stringify(this.getUnchangedProductionMaterialList));
      this.getProductSizeList = [];
    }
  }



  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Product Name": this.dataSource.data[i].prod_name,
          "Product Size": this.dataSource.data[i].prod_size,
        });
      }
      this._excelService.exportAsExcelFile(json, "Production_Material_Requirements", datetime);
    } else
      this._confirmationDialogComponent.openAlertDialog("No records found, Load the data first", "Production Material Requirements");
  }
  public exportToPdf(): void {
    if (this.dataSource.data.length > 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.prod_name);
        tempObj.push(e.prod_size);
        prepare.push(tempObj);
      });

      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Product Name", "Product Size"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('Production_Material_Requirements' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No records found, Load the data first", "Production Material Requirements");
  }
}
