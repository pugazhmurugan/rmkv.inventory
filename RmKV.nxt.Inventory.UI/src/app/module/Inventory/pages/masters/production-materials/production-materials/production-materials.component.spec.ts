import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionMaterialsComponent } from './production-materials.component';

describe('ProductionMaterialsComponent', () => {
  let component: ProductionMaterialsComponent;
  let fixture: ComponentFixture<ProductionMaterialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionMaterialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionMaterialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
