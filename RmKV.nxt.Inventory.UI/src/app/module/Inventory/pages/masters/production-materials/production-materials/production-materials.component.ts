import { Component, OnInit } from '@angular/core';
import { SalesLocation, EditMode } from 'src/app/common/models/common-model';
import { ProductionMaterialsService } from 'src/app/module/Inventory/services/masters/production-materials/production-materials.service';
import { MatTableDataSource, MatDialog, MatRadioChange } from '@angular/material';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { Router } from '@angular/router';
import * as _moment from "moment";
import { DatePipe } from '@angular/common';
import { ProdMaterials } from 'src/app/module/Inventory/model/masters/production-materials.model';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
declare var jsPDF: any;
@Component({
  selector: 'app-production-materials',
  templateUrl: './production-materials.component.html',
  styleUrls: ['./production-materials.component.scss'],
  providers: [DatePipe]
})

export class ProductionMaterialsComponent implements OnInit {

  displayedColumns = ["Serial_No", "Material_Name", "UOM", "Action"];
  workingLocationList: SalesLocation[] = [];
  componentVisibility: boolean = true;
  dataSource: any = new MatTableDataSource([]);
  materialList: ProdMaterials[] = [];
  filterMaterialList: ProdMaterials[] = [];
  selectedRowIndex: number = 0;


  objMaterials: ProdMaterials = {
    material_id: 0,
    material_name: "",
    material_uom: 0,
    active: true
  }

  unChangedMaterials: ProdMaterials = {
    material_id: 0,
    material_name: "",
    material_uom: 0,
    active: true
  }

  objModifyMaterial: ProdMaterials = {
    material_id: 0,
    material_name: "",
    material_uom: 0,
    active: true
  }
  objAction: EditMode = {
    isEditing: false,
    isView: false
  };
  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };
  objFilter: any = {
    SearchMaterial: "",
    SelectedLocation: 0
  }

  constructor(
    private _prodMaterialService: ProductionMaterialsService,
    private _confirmationDialogComponent: ConfirmationDialogComponent,
    public _router: Router,
    public _matDialog: MatDialog,
    public _keyPressEvents: KeyPressEvents,
    public _excelService: ExcelService,
    public _localStorage: LocalStorage,
    public _datePipe: DatePipe
  ) {
    this.getMaterialDetails();
  }

  ngOnInit() {

  }


  private getMaterialDetails(): void {
    this._prodMaterialService.getProductionMaterialDetails().subscribe((result: ProdMaterials[]) => {
      debugger;
      this.materialList = result;
      this.filterMaterialList = result;
      this.materialList.length !== 0 ? this.matTableConfig(this.materialList) : this._confirmationDialogComponent.openAlertDialog("No records found", "Materials");
      debugger;
    });
  }


  private matTableConfig(tableRecords: ProdMaterials[]): void {
    this.dataSource = new MatTableDataSource(tableRecords);
  }

  private beforeSaveValidate(): boolean {
    if (!this.objMaterials.material_name.toString().trim() || this.objMaterials.material_name == "-") {
      document.getElementById('materialName').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter material name", "Production Materials");
      return false;
    } else if ([null, 'null', undefined, 'undefined', NaN, 'naN', 0, '0', ''].indexOf(this.objMaterials.material_uom) !== -1) {
      document.getElementById('uom_id').focus();
      this._confirmationDialogComponent.openAlertDialog("Select material uom", "Production Materials");
      return false;
    } else {
      return true;
    }
  }

  removeSpecialCharacters(event: any) {
    this.objMaterials.material_name = event.target.value.replace(/[^a-zA-Z ]/g, "");
  }


  public addProductMaterials(): void {

    if (this.beforeSaveValidate()) {
      debugger;
      let objData = {
        ProductMaterial: JSON.stringify([{
          material_id: this.objAction.isEditing ? +this.objMaterials.material_id : 0,
          material_name: this.objMaterials.material_name.toString().trim(),
          material_uom: this.objMaterials.material_uom,
          active: this.objMaterials.active,
          entered_by: this._localStorage.intGlobalUserId()
          // company_section_id: +this._localStorage.getCompanySectionId()
        }]),
      }
      this._prodMaterialService.addProductionMaterialDetails(objData).subscribe((result: any) => {
        if (result)
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New material record has been created", "Production Materials");
        if (this.objAction.isEditing === true) {
          if (this.objMaterials.active == this.objModifyMaterial.active) {
            this.resetScreen();
            this.getMaterialDetails();
            this.componentVisibility = !this.componentVisibility;
          }
          this.objModifyMaterial.active = !this.objModifyMaterial.active;
        } else {
          this.resetScreen();
          this.getMaterialDetails();
          this.componentVisibility = !this.componentVisibility;
        }
      });
    }
  }

  private resetScreen(): void {
    if(this.objAction.isEditing) {
     // this.objAction = JSON.parse(JSON.stringify(this.unChangedAction));
      this.objModifyMaterial = JSON.parse(JSON.stringify(this.objModifyMaterial));
      this.objMaterials = JSON.parse(JSON.stringify(this.objModifyMaterial));
      } else if(!this.objAction.isEditing) {
      this.objAction = JSON.parse(JSON.stringify(this.unChangedAction));
      this.objModifyMaterial = JSON.parse(JSON.stringify(this.unChangedMaterials));
      this.objMaterials = JSON.parse(JSON.stringify(this.unChangedMaterials));
      }
  }

  public modifyOrViewMaterial(materialdetails: ProdMaterials, isEditing: boolean): void {
    this.objAction = {
      isEditing: isEditing ? true : false,
      isView: isEditing ? false : true,
    }
    this.objMaterials = JSON.parse(JSON.stringify(materialdetails));
    this.objModifyMaterial = JSON.parse(JSON.stringify(materialdetails));
    this.componentVisibility = !this.componentVisibility;
  }

  public onClearView(): void {
    this.componentVisibility = !this.componentVisibility;
    this.objAction.isEditing = false;
  }

  public onClearExit(): void {
    this.componentVisibility = !this.componentVisibility;
    this.objAction.isView = false;
  }

  fetchProductionMaterials(data: any): void {
    let objData = {
      ProductMaterial: JSON.stringify([{
        material_id: this.objMaterials.material_id
      }]),
    }
    this._prodMaterialService.fetchProductionMaterialDetails(objData).subscribe((result: ProdMaterials[]) => {
      if (result) {
        this.objAction.isEditing = true;
        this.objMaterials = JSON.parse(JSON.stringify(result));
        this.componentVisibility = !this.componentVisibility;

      }
    });
  }

  public changeFlagConfirmation(): void {
    if (this.objAction.isEditing === true)
      this.openFlagConfirmationDialog(this.objMaterials.active ? "Do you want to make this record active?" : "Do you want to deactive this record?");
  }

  public newClick(): void {
    this.objAction = JSON.parse(JSON.stringify(this.unChangedAction));
      this.objModifyMaterial = JSON.parse(JSON.stringify(this.unChangedMaterials));
      this.objMaterials = JSON.parse(JSON.stringify(this.unChangedMaterials));
    this.componentVisibility = !this.componentVisibility;
  }

  private onListClick(): void {
    this.getMaterialDetails();
    this.componentVisibility = !this.componentVisibility;
  }

  public radioChange(event: MatRadioChange): void {
    this.filterMaterialList = [];
    if (event.value === 1) {
      this.materialList.forEach(element => {
        if (+element.active === 1)
          this.filterMaterialList.push(element);
      });
    } else if (event.value === 0) {
      this.materialList.forEach(element => {
        if (+element.active === 0)
          this.filterMaterialList.push(element);
      });
    }
    else {
      this.materialList.forEach(element => {
        this.filterMaterialList.push(element);
      });
    }
    this.searchMaterialName(this.objFilter.SearchMaterial);
  }

  public searchMaterialName(searchValue: string): void {
    searchValue = searchValue.toString().trim();
    searchValue = searchValue ? searchValue.toString().toLocaleLowerCase() : "";
    this.objFilter.SearchMaterial = searchValue;
    let filteredMaterialList = this.filterMaterialList.filter(element =>
      element.material_name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    this.matTableConfig(filteredMaterialList);
  }



  private checkAnyChangesMade(): boolean {
    if (JSON.stringify(this.objMaterials) !== (!this.objAction.isEditing ? JSON.stringify(this.unChangedMaterials) : JSON.stringify(this.objModifyMaterial))) {
      return true;
    } else {
      return false;
    }
  }
  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Materials";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  private openFlagConfirmationDialog(message: string): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Materials";
    dialogRef.afterClosed().subscribe(result => {
      result ? this.addProductMaterials() : this.objMaterials.active = this.objMaterials.active ? false : true;
      dialogRef = null;
    });
  }

  public onlyAllowCharacters(event: KeyboardEvent | any) {
    const pattern = /^[a-zA-Z -]*$/;
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^a-zA-Z -]/g, "");
    }
  }

  public highlightRow(index: number): void {
    this.selectedRowIndex = index;
  }


  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Material Name': this.dataSource.data[i].material_name,
          'UOM Description': this.dataSource.data[i].uom_description
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Production_Materials ",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Production Materials");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.material_name);
        tempObj.push(e.uom_description);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Material Name', 'UOM Description']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Production_Materials ' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Production Materials");
  }

}
