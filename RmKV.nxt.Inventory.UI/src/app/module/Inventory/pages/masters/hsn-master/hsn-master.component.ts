import { Component, OnInit, ViewChildren, ElementRef, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatDialogRef, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { EditMode } from 'src/app/common/models/common-model';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { HsnMasterService } from '../../../services/masters/hsn-master/hsn-master.service';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { DatePipe } from '@angular/common';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { ChangeDetectorRef } from '@angular/core';
declare var jsPDF: any;

@Component({
  selector: 'app-hsn-master',
  templateUrl: './hsn-master.component.html',
  styleUrls: ['./hsn-master.component.scss'],
  providers: [DatePipe]
})
export class HsnMasterComponent implements OnInit {

  componentVisibility: boolean = true;
  isTaxRates: boolean = false;
  searchString: string = "";

  hsnTaxList: any[] =    [{
    Hsn_Tax_Rate_Id: "",
    Effective_From: "",
    Range_From: 0,
    Range_To: 0,
    Gst_Rate: 0,
  }]

  objHSN:any = {
    Hsn:"",
    Variable_Rate:true,
    Hsn_Description:"",
    Active:true,
  }

  objmodifyHSN:any = {
    Hsn:"",
    Variable_Rate:true,
    Hsn_Description:"",
    Active:true,
  }

  objUnchangedHSN:any = {
    Hsn:"",
    Variable_Rate:true,
    Hsn_Description:"",
    Active:true,
  }

  objHSNTax: any = {
    hsn_tax_rate_id:0,

  }

  hsnMasterList:any[] = [];
  filterhsnMasterList:any[]=[];
  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  checked = true;
 
  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "HSN", "Description", "Variable_Rate", "Current_GST", "Action"];

  @ViewChildren("effectivemonth") effectivemonth: ElementRef | any;
  @ViewChildren("rangefrom") rangefrom: ElementRef | any;
  @ViewChildren("rangeto") rangeto: ElementRef | any;
  @ViewChildren("gstrate") gstrate: ElementRef | any;

  constructor(public _localStorage: LocalStorage,private _datePipe: DatePipe,
    private _confirmationDialogComponent: ConfirmationDialogComponent,
    public _router: Router,private _hsnMasterService:HsnMasterService,
    public _matDialog: MatDialog,public _keyPressEvents: KeyPressEvents,
    public dialogRef: MatDialogRef<ConfirmationDialogComponent>,public _excelService: ExcelService,
    private changeDetectorRefs: ChangeDetectorRef,)
     {
      this.getHSNMasterList(); 
    }

  ngOnInit() {
  }

   /****************************************** CRUD Operations **********************************************/


  public taxRatesClick(HsnMaster):void {
    // sessionStorage.setItem("HSNTaxRates", this.objHSN);
    localStorage.setItem("HsnMaster", btoa(JSON.stringify(HsnMaster)));
    this._router.navigate(["/Inventory/HSNTaxRates"]);
  }

  public saveHSNMaster(): void {
    debugger;
     if (this.beforeSaveValidate()) {
      let tempSaveArr = [];

      let objData = {
        hsn: this.objHSN.Hsn.toString().trim(),
        hsn_description: this.objHSN.Hsn_Description.toString().trim(),
        variable_rate: this.objHSN.Variable_Rate,
        entered_by:this._localStorage.intGlobalUserId()
      }
      tempSaveArr.push(objData);
      let objSave = {
        SaveHSN: JSON.stringify(tempSaveArr)
      }

      this._hsnMasterService.saveHSN(objSave).subscribe((result: any[]) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New HSN record has been created", "HSN Master");       
           this.getHSNMasterList();
          this.componentVisibility = !this.componentVisibility;
        }
      });
    }
    }

  private getHSNMasterList(): void {
    this._hsnMasterService.getHSNMaster().subscribe((result: any[]) => {
      this.hsnMasterList = result;
      this.filterhsnMasterList = result;
      this.hsnMasterList !== null ? this.matTableConfig(this.hsnMasterList) : this._confirmationDialogComponent.openAlertDialog("No record found", "HSN Master");
    });
  }

  private matTableConfig(tableRecords: any[]): void {
    this.dataSource = new MatTableDataSource(tableRecords);    
  }

  public modifyHSN(isEditing: boolean, index: number): void {
    this.objAction = {
      isEditing: isEditing ? true : false
    }
    let objModify: any = [{
      hsn: this.dataSource.data[index].Hsn,
    }]
    let objFetch = {
      FetchHSN: JSON.stringify(objModify)
    }

    this._hsnMasterService.fetchHSNMaster(objFetch).subscribe((result: any) => {
      if (result) {
        debugger;
        this.objHSN = JSON.parse(JSON.stringify(result[0]));
        this.objmodifyHSN = JSON.parse(JSON.stringify(result[0]));
       this.componentVisibility = !this.componentVisibility;
      }
    });
  }

  private deleteHSN(row: any): void {
    debugger;
    let tempSaveArr = [];
    let objData = {
      hsn: this.dataSource.data[row].Hsn,
      active: false,
    }
    tempSaveArr.push(objData);
    let objCancel = {
      CancelHSN: JSON.stringify(tempSaveArr)
    }
    this._hsnMasterService.RemoveHsnMaster(objCancel).subscribe(
      (result: any) => {
        if (result.error) {
          this._confirmationDialogComponent.openAlertDialog(result.error, "HSN Master");
        } else if (result) {
          this._confirmationDialogComponent.openAlertDialog("DeActivated", "HSN Master");
        } else {
          this._confirmationDialogComponent.openAlertDialog(result.error, "HSN Master");
        }
        this.getHSNMasterList();
      });
    this.dialogRef = null;
  }

  private deleteHSNInactive(row: any): void {
    debugger;
    let tempSaveArr = [];
    let objData = {
      hsn: this.dataSource.data[row].Hsn,
      active: true,
    }
    tempSaveArr.push(objData);
    let objCancel = {
      CancelHSN: JSON.stringify(tempSaveArr)
    }
    this._hsnMasterService.RemoveHsnMaster(objCancel).subscribe(
      (result: any) => {
        if (result.error) {
          this._confirmationDialogComponent.openAlertDialog(result.error, "HSN Master");
        } else if (result) {
          this._confirmationDialogComponent.openAlertDialog("Activated", "HSN Master");
        } else {
          this._confirmationDialogComponent.openAlertDialog(result.error, "HSN Master");
        }
        this.getHSNMasterList();
      });
    this.dialogRef = null;
  }

/****************************************** Validations **********************************************/

  public onClickDelete(index: any): any {
    this.dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this.dialogRef.componentInstance.confirmMessage =
      "Do you want to deactivate of '" + this.dataSource.data[index].Hsn + "'";
    this.dialogRef.componentInstance.componentName = "HSN Master";
    return this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteHSN(index);
      }
      this.dialogRef = null;
    });
  }

  public onClickActive(index: any): any {
    this.dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this.dialogRef.componentInstance.confirmMessage =
      "Do you want to activate of '" + this.dataSource.data[index].Hsn + "'";
    this.dialogRef.componentInstance.componentName = "HSN Master";
    return this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteHSNInactive(index);
      }
      this.dialogRef = null;
    });
  }

  public searchRights(searchValue: string): void {
    searchValue = searchValue.toString().trim();
    searchValue = searchValue ? searchValue.toString().toLocaleLowerCase() : "";
    this.searchString = searchValue;
    let filterhsnMasterList = this.filterhsnMasterList.filter(element =>
      element.Hsn.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    this.matTableConfig(filterhsnMasterList);
  }

  private beforeSaveValidate(): boolean {
    debugger;
    if (!this.objHSN.Hsn.toString().trim()) {
      document.getElementById("hsncode").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter HSN Code", "HSN Master");
      return false;
    }else return true;

  }

  public newClick(): void {
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }
  
  private resetScreen(): void {
    this.objAction = Object.assign({}, this.unChangedAction);
    this.objHSN = Object.assign({}, this.objUnchangedHSN);
    this.objmodifyHSN = Object.assign({}, this.objUnchangedHSN);
  }
  
  public onListClick(): void {
    this.getHSNMasterList();
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  public onClear(exitFlag: boolean): void {
    debugger;
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "HSN Master";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }
  
  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objHSN) !== ((this.objAction.isEditing) ? JSON.stringify(this.objmodifyHSN)
      : JSON.stringify(this.objUnchangedHSN)))
      return true;
    else
      return false;
  }

   /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "HSN": this.dataSource.data[i].Hsn,
          "HSN Description": this.dataSource.data[i].Hsn_Description,
          "Variable Rate": this.dataSource.data[i].Variable_Rate ? 'Yes' : 'No',
        });
      }
      this._excelService.exportAsExcelFile(json, "HSN Master", datetime);
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found", "HSN Master");
  }
  
  exportToPdf(): void {
  
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.Hsn);
        tempObj.push(e.Hsn_Description);
        tempObj.push(e.Variable_Rate ? 'Yes' : 'No');
        prepare.push(tempObj);
      });
  
      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["HSN","Hsn Description","Variable Rate"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
  
      doc.save('HSN Master' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "HSN Master");
  }
  


}
 