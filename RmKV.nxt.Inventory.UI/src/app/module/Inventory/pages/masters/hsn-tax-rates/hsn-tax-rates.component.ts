import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog, MatDialogRef } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { HsnMasterService } from '../../../services/masters/hsn-master/hsn-master.service';
import { EditMode } from 'src/app/common/models/common-model';
import { DatePipe } from '@angular/common';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
declare var jsPDF: any;

@Component({
  selector: 'app-hsn-tax-rates',
  templateUrl: './hsn-tax-rates.component.html',
  styleUrls: ['./hsn-tax-rates.component.scss'],
  providers:[DatePipe]
})
export class HsnTaxRatesComponent implements OnInit {

  componentVisibility: boolean = true;


  objHSNTax: any = {
    Hsn_Tax_Rate_Id: "",
    Effective_From: new Date(),
    Range_From: 0,
    Rage_To: 0,
    Gst_Rate: 0,
  }

  objmodifyHSNTax: any = {
    Hsn_Tax_Rate_Id: "",
    Effective_From: new Date(),
    Range_From: 0,
    Rage_To: 0,
    Gst_Rate: 0,
  }

  objUnchangedHSNTax: any = {
    Hsn_Tax_Rate_Id: "",
    Effective_From: new Date(),
    Range_From: 0,
    Rage_To: 0,
    Gst_Rate: 0,
  }

  objHSN:any = {
    Hsn:"",
    Variable_Rate:true,
    Hsn_Description:"",
    Active:true,
  }

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

    dataSource: any = new MatTableDataSource([]);
    displayedColumns = ["Serial_No", "Effective_From", "Range_From", "Range_To", "GST", "Action"];
    hsnMasterList:any[] = [];
    filterhsnMasterList:any[]=[];

  constructor(public _localStorage: LocalStorage,
    private _route : ActivatedRoute,
    private _datePipe : DatePipe,
    private _confirmationDialogComponent: ConfirmationDialogComponent,
    public _router: Router,private _hsnMasterService:HsnMasterService,
    public _matDialog: MatDialog,public _keyPressEvents: KeyPressEvents,
    public dialogRef: MatDialogRef<ConfirmationDialogComponent>,public _excelService: ExcelService,) {
      // this.objHSN = this._route.snapshot.params;
      // this.getHSNMasterList();
     }

  ngOnInit() {
     let localData = JSON.parse(atob(localStorage.getItem("HsnMaster")));
  this.assignProductAttributeDatas(localData);
  }

  private assignProductAttributeDatas(records): void {
    this.objHSN.Hsn = records.Hsn,
      this.objHSN.Variable_Rate = records.Variable_Rate,
      this.objHSN.Hsn_Description = records.Hsn_Description,
      this.getHSNMasterList();
  }

  public onExit(): void {
    this._router.navigate(["/Inventory/HSNMaster"]);
  }

   /*************************************************** CRUD Operations ************************************************/


  private getHSNMasterList(): void {
debugger;
    let tempSaveArr = [];

    let objData = [{
      hsn:this.objHSN.Hsn,
    }]
    // tempSaveArr.push(objData);
    let objSave = {
      FetchHSNTaxRate: JSON.stringify(objData)
    }

    this._hsnMasterService.fetchHSNTaxRate(objSave).subscribe((result: any[]) => {
      debugger;
      if(result == null|| result == []){
        this.hsnMasterList = result;
        this.filterhsnMasterList = result;
        this._confirmationDialogComponent.openAlertDialog("No record found", "HSN Tax"); 
      }
      else{
        this.hsnMasterList = result;
        this.filterhsnMasterList = result;
        this.matTableConfig(result);
      }
      });
  }

  private matTableConfig(tableRecords: any[]): void {
    this.dataSource = new MatTableDataSource(tableRecords);
  }

  public saveHSNTax(): void {
    debugger;
     if (this.beforeSaveValidate()) {
      let tempSaveArr = [];

      let objData = [{
        hsn_tax_rate_id: this.objAction.isEditing ? this.objHSNTax.Hsn_Tax_Rate_Id : "",
        hsn: this.objHSN.Hsn.toString().trim(),
        effective_from: this._datePipe.transform(this.objHSNTax.Effective_From, 'dd/MM/yyyy'),
        range_from: this.objHSNTax.Range_From,
        rage_to:this.objHSNTax.Rage_To,
        gst_rate:this.objHSNTax.Gst_Rate
      }]
      // tempSaveArr.push(objData);
      let objSave = {
        SaveHSNTax: JSON.stringify(objData)
      }
      this._hsnMasterService.saveHSNTax(objSave).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New HSN record has been created", "HSN Tax");       
           this.getHSNMasterList();
          this.componentVisibility = !this.componentVisibility;
        }
      });
    }
   }


  public modifyHSN(isEditing: boolean, index: number): void {
    this.objAction = {
      isEditing: isEditing ? true : false
    }
    let objModify: any = [{
      hsn_tax_rate_id: this.dataSource.data[index].Hsn_Tax_Rate_Id,
    }]
    let objFetch = {
      FetchHSNTaxRateIndividual: JSON.stringify(objModify)
    }

    this._hsnMasterService.fetchHSNTaxRateIndividual(objFetch).subscribe((result: any) => {
      if (result) {
        this.objHSNTax = JSON.parse(JSON.stringify(result[0]));
        this.objmodifyHSNTax = JSON.parse(JSON.stringify(result[0]));
       this.componentVisibility = !this.componentVisibility;
      }
    });
  }

  private deleteHSN(row: any): void {
    debugger;
    let tempSaveArr = [];
    let objData = {
      hsn_tax_rate_id: this.dataSource.data[row].Hsn_Tax_Rate_Id,
    }
    tempSaveArr.push(objData);
    let objCancel = {
      RemoveHSNTaxRate: JSON.stringify(tempSaveArr)
    }
    this._hsnMasterService.RemoveHsnTaxRate(objCancel).subscribe(
      (result: any) => {
         if (result === 1) {
          this._confirmationDialogComponent.openAlertDialog("Removed", "HSN Tax Rates");
          this.getHSNMasterList();
        } else {
          this._confirmationDialogComponent.openAlertDialog(result.error, "HSN Tax Rates");
        }
        
      });
    this.dialogRef = null;
  }

   /*************************************************** Validations ************************************************/


  public checkGSTRate() : boolean {
    if(this.objHSNTax.Gst_Rate > 50) {
      document.getElementById("gstrate").focus();
      this._confirmationDialogComponent.openAlertDialog("Maximum GST Rate is 50%" , "HSN Tax Rates");
      return false;
    }else return true;
  }
  
  public onlyAllowNumber(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    let key = key1 + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    }
  }

  private beforeSaveValidate(): boolean {
     if ([null, undefined, "", 0, '0'].indexOf(this.objHSNTax.Range_From) !== -1 || !this.objHSNTax.Range_From.toString().trim()) {
     
      document.getElementById("rangeFrom").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Range From", "HSN TaxRate");
      return false;
    } else if ([null, undefined, "", 0, '0'].indexOf(this.objHSNTax.Rage_To) !== -1 || !this.objHSNTax.Rage_To.toString().trim()) {
      
      document.getElementById("rangeto").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Range To", "HSN TaxRate");
      return false;
    }
    else if ([null, undefined, "", 0, '0'].indexOf(this.objHSNTax.Gst_Rate) !== -1 || !this.objHSNTax.Gst_Rate.toString().trim()) {
      
      document.getElementById("gstrate").focus();
      this._confirmationDialogComponent.openAlertDialog("Enter GST Rate", "HSN TaxRate");
      return false;
    }
    else {
      return true;
    }
  }

  public newClick(): void {
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }
  
  private resetScreen(): void {
    this.objAction = Object.assign({}, this.unChangedAction);
    this.objHSNTax = Object.assign({}, this.objUnchangedHSNTax);
    this.objmodifyHSNTax = Object.assign({}, this.objUnchangedHSNTax);
  }
  
  public onListClick(): void {
    this.getHSNMasterList();
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  public onClear(exitFlag: boolean): void {
    debugger;
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "HSN Tax";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  public onClickDelete(index: any): any {
    this.dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this.dialogRef.componentInstance.confirmMessage =
      "Do you want to remove hsn tax rate?"
    this.dialogRef.componentInstance.componentName = "HSN Tax Rate";
    return this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteHSN(index);
      }
      this.dialogRef = null;
    });
  }
  
  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objHSNTax) !== ((this.objAction.isEditing) ? JSON.stringify(this.objmodifyHSNTax)
      : JSON.stringify(this.objUnchangedHSNTax)))
      return true;
    else
      return false;
  }

   /*************************************************** Exports ************************************************/


  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Effective From": this._datePipe.transform(this.dataSource.data[i].Effective_From, "dd-MM-yyyy"),
          "Range From": this.dataSource.data[i].Range_From,
          "Rage To": this.dataSource.data[i].Rage_To,
          "Gst Rate": this.dataSource.data[i].Gst_Rate,
        });
      }
      this._excelService.exportAsExcelFile(json, "HSN Tax Rate", datetime);
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found", "HSN Tax Rate");
  }
  
  exportToPdf(): void {
  
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.hsnMasterList.forEach(e => {
        var tempObj = [];
        tempObj.push(this._datePipe.transform(e.Effective_From, "dd-MM-yyyy"));
        tempObj.push(e.Range_From);
        tempObj.push(e.Rage_To);
        tempObj.push(e.Gst_Rate);
        prepare.push(tempObj);
      });
  
      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Effective From","Range From","Range To","Gst Rate"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
  
      doc.save('HSN Tax Rate' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "HSN Tax Rate");
  }
  

}
