import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HsnTaxRatesComponent } from './hsn-tax-rates.component';

describe('HsnTaxRatesComponent', () => {
  let component: HsnTaxRatesComponent;
  let fixture: ComponentFixture<HsnTaxRatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HsnTaxRatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HsnTaxRatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
