
import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode, Rights } from 'src/app/common/models/common-model';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { HsnLookupComponent } from 'src/app/common/shared/hsn-lookup/hsn-lookup.component';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductGroupLookupComponent } from 'src/app/common/shared/product-group-lookup/product-group-lookup.component';
import { HsnMasterService } from '../../../services/masters/hsn-master/hsn-master.service';
import { ProductGroupAttributesService } from '../../../services/masters/product-group-attributes/product-group-attributes.service';
import { ProductMasterService } from '../../../services/masters/product-master/product-master.service';
declare var jsPDF: any;
@Component({
  selector: 'app-product-master',
  templateUrl: './product-master.component.html',
  styleUrls: ['./product-master.component.scss'],
  providers: [DatePipe]
})
export class ProductMasterComponent implements OnInit {

  objLoadProductMaster = {
    product_group_id: 0,
    product_group_name: "",
    product_group_desc: "",
    category_id: 0,
    category_name: "",
    product_group_uom: "",
    lsp: ""
  }

  objProductMaster = {
    product_group_id: 0,
    product_group_name: "",
    product_group_desc: "",
    category_id: 0,
    category_name: "",
    product_group_uom: "",
    lsp: "",
    product_code: '',
    active: true,
    product_name: '',
    product_billing_desc: '',
    product_attributes: [],
    counter_data: [],
    product_hsn: [{
      Hsn: '',
      Hsn_Description: ''
    }],
  }

  objModifyProductMaster = {
    product_group_id: 0,
    product_group_name: "",
    product_group_desc: "",
    category_id: 0,
    category_name: "",
    product_group_uom: "",
    lsp: "",
    product_code: '',
    active: true,
    product_name: '',
    product_billing_desc: '',
    product_attributes: [],
    counter_data: [],
    product_hsn: [{
      Hsn: '',
      Hsn_Description: ''
    }],
  }

  unchangedProductMaster = {
    product_group_id: 0,
    product_group_name: "",
    product_group_desc: "",
    category_id: 0,
    category_name: "",
    product_group_uom: "",
    lsp: "",
    product_code: '',
    active: true,
    product_name: '',
    product_billing_desc: '',
    product_attributes: [],
    counter_data: [],
    product_hsn: [{
      Hsn: '',
      Hsn_Description: ''
    }],
  }

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  rights: Rights = {
    Add: false,
    Update: false,
    Delete: false
  };

  componentVisibility: boolean = true;
  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Product_Code", "Product_Name", "Billing_Name", "Active", "Action"];
  productGroupList: any[];
  focusFlag: boolean;
  productMasterList: any[];
  HSNList: any[];
  productAtrributes: any = '';
  @ViewChildren("hsn") hsn: ElementRef | any;
  tempAttribute: any[];

  constructor(public _localStorage: LocalStorage,
    public _router: Router, public _productMaster: ProductMasterService,
    public _matDialog: MatDialog, private _datePipe: DatePipe,
    private _excelService: ExcelService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _productGroupAttributesService: ProductGroupAttributesService,
    public _hsnMasterService: HsnMasterService) {
    this.getProductGroupList();
    this.getHSNList();
  }

  ngOnInit() {
    //console.log(this.objProductMaster.product_hsn, 'hsn')
    this.getRights();
  }

  private getRights(): void {
    var rights = this._localStorage.getMenuRights("/ProductMaster");
    console.log('rights', rights);
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "Delete" && rights[x].Status == true) {
        this.rights.Delete = true;
      }
    }
  }

  public newClick(): void {
    if (this.rights.Add && this.objLoadProductMaster.product_group_id !== 0) {
      this.objAction.isEditing = false;
      this.objAction.isView = false;
      this.componentVisibility = !this.componentVisibility;
      debugger
      this.resetScreen();
      this.addAssaignData();
      this.getProductAttributes();
      this.getProductCounter();
    } else {
      document.getElementById('groupName').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter product group", "Product Master");
      this.objProductMaster.product_code = "";
      // return false;
    }
  }

  addAssaignData() {
    this.objProductMaster.product_group_id = this.objLoadProductMaster.product_group_id;
    this.objProductMaster.product_group_desc = this.objLoadProductMaster.product_group_desc
    this.objProductMaster.product_group_name = this.objLoadProductMaster.product_group_name
    this.objProductMaster.product_group_uom = this.objLoadProductMaster.product_group_uom
    this.objProductMaster.lsp = this.objLoadProductMaster.lsp
    this.objProductMaster.category_id = this.objLoadProductMaster.category_id
    this.objProductMaster.category_name = this.objLoadProductMaster.category_name
    this.objModifyProductMaster.product_group_id = this.objLoadProductMaster.product_group_id;
    this.objModifyProductMaster.product_group_desc = this.objLoadProductMaster.product_group_desc
    this.objModifyProductMaster.product_group_name = this.objLoadProductMaster.product_group_name
    this.objModifyProductMaster.product_group_uom = this.objLoadProductMaster.product_group_uom
    this.objModifyProductMaster.lsp = this.objLoadProductMaster.lsp
    this.objModifyProductMaster.category_id = this.objLoadProductMaster.category_id
    this.objModifyProductMaster.category_name = this.objLoadProductMaster.category_name
    this.unchangedProductMaster.product_group_id = this.objLoadProductMaster.product_group_id;
    this.unchangedProductMaster.product_group_desc = this.objLoadProductMaster.product_group_desc
    this.unchangedProductMaster.product_group_name = this.objLoadProductMaster.product_group_name
    this.unchangedProductMaster.product_group_uom = this.objLoadProductMaster.product_group_uom
    this.unchangedProductMaster.lsp = this.objLoadProductMaster.lsp
    this.unchangedProductMaster.category_id = this.objLoadProductMaster.category_id
    this.unchangedProductMaster.category_name = this.objLoadProductMaster.category_name
  }

  private resetScreen(): void {
    if (this.objAction.isEditing == true) {
      this.objModifyProductMaster = JSON.parse(JSON.stringify(this.objModifyProductMaster));
      this.objProductMaster = JSON.parse(JSON.stringify(this.objModifyProductMaster));
      this.productAtrributes = '';
      this.getProductAttributes();
      this.getProductCounter();
      if (this.objProductMaster.product_hsn == null) {
        this.objProductMaster.product_hsn = [];
        this.objProductMaster.product_hsn.push({
          Hsn: '',
          Hsn_Description: ''
        })
      }
    } else {
      this.objModifyProductMaster = JSON.parse(JSON.stringify(this.unchangedProductMaster));
      this.objProductMaster = JSON.parse(JSON.stringify(this.unchangedProductMaster));
      this.productAtrributes = '';
      this.getProductAttributes();
      this.getProductCounter();
      if (this.objProductMaster.product_hsn == null) {
        this.objProductMaster.product_hsn = [];
        this.objProductMaster.product_hsn.push({
          Hsn: '',
          Hsn_Description: ''
        })
      }
    }
  }

  public onListClick(): void {
    this.getProductMasters();
    this.componentVisibility = !this.componentVisibility;
  }

  private checkAnyChangesMade(): boolean {
    if (JSON.stringify(this.objProductMaster) !== (!this.objAction.isEditing ? JSON.stringify(this.unchangedProductMaster) : JSON.stringify(this.objModifyProductMaster))) {
      return true;
    } else {
      return false;
    }
  }

  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Product Master";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  private getProductMasters(): void {
    this.matTableConfig([]);
    let objData = {
      ProductMaster: JSON.stringify([{
        group_section_id: +this._localStorage.getGlobalGroupSectionId(),
        product_group_id: +this.objLoadProductMaster.product_group_id
      }]),
    }
    this._productMaster.getProductMasters(objData).subscribe((result: any) => {
      if (result) {
        this.productMasterList = result;
        this.matTableConfig(this.productMasterList);
      } else
        this._confirmationDialogComponent.openAlertDialog('No records found', 'Product Master');
    });
    // console.log(this.productMasterList,)
  }

  private getProductAttributes(): void {
    let objData = {
      ProductMaster: JSON.stringify([{
        product_group_id: +this.objLoadProductMaster.product_group_id
      }]),
    }
    this._productMaster.getProductAttributes(objData).subscribe((result: any) => {
      if (result) {
        console.log(result)
        this.objProductMaster.product_attributes = [];
        this.objProductMaster.product_attributes.push(result[0]);
        //console.log(this.objProductMaster.product_attributes, 'attributes')
      }
    });
  }

  private getProductCounter(): void {
    let objData = {
      ProductMaster: JSON.stringify([{
        product_group_id: this._localStorage.getGlobalGroupSectionId(),//+this.objLoadProductMaster.product_group_id
      }]),
    }
    this._productMaster.getProductCounter(objData).subscribe((result: any) => {
      if (result) {
        console.log(result)
        this.objProductMaster.counter_data = result;
        // this.objProductMaster.counter_data.push(result[0]);
        // console.log(this.objProductMaster.counter_data, 'conter')
      }
    });
  }

  private matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }

  public openProductGroupLookup(event: any): void {

    if (this.objLoadProductMaster.product_group_name === "") {
      this.objLoadProductMaster.product_group_desc = "";
      this.objLoadProductMaster.category_name = "";
      this.objLoadProductMaster.product_group_uom = '';
      this.objLoadProductMaster.lsp = '';
    }
    if (event.keyCode === 13) {
      this.openProductGroupLookupDialog();
    }
  }

  openAlertDialog(value: string, componentName?: string, focus?: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        document.getElementById(focus).focus();
      _dialogRef = null;
    });
  }

  private getProductGroupList(): void {

    let objDetails: any = {
      GetProductGroupLookup: JSON.stringify([{
        group_section_id: +this._localStorage.getGlobalSectionId()
      }])
    }
    this._productGroupAttributesService.GetProductGroupLookupDetails(objDetails).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0) {

        this.productGroupList = JSON.parse(result);
        //console.log(this.productGroupList, 'RESULT')
      }
    });
  }
  public checkValidProduct(event: KeyboardEvent): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objLoadProductMaster.product_group_name != '' && this.objLoadProductMaster.product_group_id == 0) {
      this.openAlertDialogCode("Invalid product group", "Product Master");
    }
  }

  openAlertDialogCode(value: string, componentName?: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        document.getElementById('groupName').focus();
      _dialogRef = null;
    });
  }
  private openProductGroupLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(ProductGroupLookupComponent, {
      width: "500px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objLoadProductMaster.product_group_name
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {

      this.focusFlag = false;
      debugger;
      if (result) {
        this.objLoadProductMaster.product_group_id = result.product_group_id;
        this.objLoadProductMaster.product_group_name = result.product_group_name;
        this.objLoadProductMaster.product_group_desc = result.product_group_desc;
        this.objLoadProductMaster.category_id = result.category_id;
        this.objLoadProductMaster.category_name = result.category_name;
        // this.objLoadProductMaster.product_group_uom = result.product_group_uom;
        if (+result.product_group_uom === 1)
          this.objLoadProductMaster.product_group_uom = 'Nos';
        else if (+result.product_group_uom === 2)
          this.objLoadProductMaster.product_group_uom = 'Mts';
        this.objLoadProductMaster.lsp = result.lsp == true ? 'Yes' : 'No';
        if (this.objLoadProductMaster.product_group_id)
          this.getProductMasters();
      } else {
        this.objLoadProductMaster.product_group_id = 0;
        this.objLoadProductMaster.product_group_desc = "";
        this.objLoadProductMaster.category_id = 0;
        this.objLoadProductMaster.category_name = '';
        this.objLoadProductMaster.product_group_uom = '';
        this.objLoadProductMaster.lsp = '';
      }
    });
  }

  public onEnterProductGroupLookupDetails(): void {

    let productGroupList: any = {
      product_group_id: 0,
      product_group_name: "",
      product_group_desc: "",
      category_id: 0,
      category_name: '',
      product_group_uom: '',
      unique_byno_prod_serial: '',
      lsp: '',
    }
    productGroupList = this.productGroupList.find(emp => emp.product_group_name.toLowerCase().trim() ===
      this.objLoadProductMaster.product_group_name.toString().trim().toLowerCase());
    this.objLoadProductMaster.product_group_id = productGroupList && productGroupList.product_group_id ? productGroupList.product_group_id : 0;
    this.objLoadProductMaster.product_group_name = productGroupList && productGroupList.product_group_name ?
      productGroupList.product_group_name : this.objLoadProductMaster.product_group_name;
    this.objLoadProductMaster.product_group_desc = productGroupList && productGroupList.product_group_desc ? productGroupList.product_group_desc : '';
    this.objLoadProductMaster.category_id = productGroupList && productGroupList.category_id ? productGroupList.category_id : 0;
    this.objLoadProductMaster.category_name = productGroupList && productGroupList.category_name ? productGroupList.category_name : '';
    this.objLoadProductMaster.product_group_uom = productGroupList && productGroupList.product_group_uom ? productGroupList.product_group_uom : '';
    if (+this.objLoadProductMaster.product_group_uom === 1)
      this.objLoadProductMaster.product_group_uom = 'Nos';
    else if (+this.objLoadProductMaster.product_group_uom === 2)
      this.objLoadProductMaster.product_group_uom = 'Mts';
    this.objLoadProductMaster.lsp = productGroupList && productGroupList.lsp ? productGroupList.lsp ? 'Yes' : 'No' : '';
    if (this.objLoadProductMaster.product_group_id)
      this.getProductMasters();
    else
      this.productMasterList = [];
  }

  private beforeSaveValidate(): boolean {
    if (!this.objProductMaster.product_code.toString().trim()) {
      document.getElementById('productCode').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter product code", "Product Master");
      this.objProductMaster.product_code = "";
      return false;
    } else if (!this.objProductMaster.product_name.toString().trim()) {
      document.getElementById('productName').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter product name", "Product Master");
      this.objProductMaster.product_name = "";
      return false;
    } else if (!this.objProductMaster.product_billing_desc.toString().trim()) {
      document.getElementById('billing Name').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter billing name", "Product Master");
      return false;
    } else
      return true;
  }

  public addProductMasters(): void {
    if (this.beforeSaveValidate()) {
      let tempHsn = [];
      for (let i = 0; i < this.objProductMaster.product_hsn.length; i++) {
        tempHsn.push(this.objProductMaster.product_hsn[i].Hsn)
      }
      let tempCounter = [];
      for (let i = 0; i < this.objProductMaster.counter_data.length; i++) {
        if (this.objProductMaster && this.objProductMaster.counter_data && this.objProductMaster.counter_data[i] && this.objProductMaster.counter_data[i].counter_data)
          tempCounter.push(
            (this.objProductMaster && this.objProductMaster.counter_data && this.objProductMaster.counter_data[i].company_section_id) ? this.objProductMaster.counter_data[i].company_section_id : '',
            (this.objProductMaster && this.objProductMaster.counter_data && this.objProductMaster.counter_data[i].counter_id) ? this.objProductMaster.counter_data[i].counter_id : ''
          )
      }
      let objData = {
        ProductMaster: JSON.stringify([{
          product_code: this.objProductMaster.product_code.toString().trim(),
          product_name: this.objProductMaster.product_name.toString().trim(),
          product_billing_desc: this.objProductMaster.product_billing_desc.toString().trim(),
          product_group_id: this.objLoadProductMaster.product_group_id,
          group_section_id: this._localStorage.getGlobalGroupSectionId(),
          product_attributes: this.tempAttribute,
          product_hsn: tempHsn,
          active: this.objProductMaster.active,
          product_counter_data: tempCounter,
          entered_by: this._localStorage.intGlobalUserId()
        }]),
      }
      this._productMaster.addProductMasters(objData).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New product record has been created", "Product Master");
          this.componentVisibility = !this.componentVisibility;
          this.resetScreen();
        }
      });
    }
  }

  public fetchProductMaster(product_code: any): void {
    let objData = {
      ProductMaster: JSON.stringify([{
        product_code: product_code
      }]),
    }
    this._productMaster.fetchProductMasters(objData).subscribe((result: any) => {
      //  console.log(result, 'result')
      this.objAction.isEditing = true;
      this.objAction.isView = false;
      this.getProductAttributes();
      this.getProductCounter();
      this.objProductMaster = JSON.parse(JSON.stringify(result[0]));
      this.objModifyProductMaster = JSON.parse(JSON.stringify(result[0]));
      //this.productAtrributes = this.objProductMaster.product_attributes[0].value_data[0].attribute_value_id;
      if (this.objProductMaster.product_hsn == null) {
        this.objProductMaster.product_hsn = [];
        this.objProductMaster.product_hsn.push({
          Hsn: '',
          Hsn_Description: ''
        })
      } else {
        for (let i = 0; i < JSON.parse(JSON.stringify(result[0])).product_hsn.length; i++) {
          this.objProductMaster.product_hsn[i].Hsn = JSON.parse(JSON.stringify(result[0])).product_hsn[i].hsn;
          this.objProductMaster.product_hsn[i].Hsn_Description = JSON.parse(JSON.stringify(result[0])).product_hsn[i].hsn_description;
        }
      }
      this.addAssaignData();
      this.componentVisibility = !this.componentVisibility;
    });
  }

  private deleteProductMasters(product_code: string): void {
    let objData = {
      ProductMaster: JSON.stringify([{
        product_code: product_code,
        active: false,
        entered_by: this._localStorage.intGlobalUserId()
      }]),
    }
    this._productMaster.deleteProductMasters(objData).subscribe((result: any) => {
      if (!result) {
        this._confirmationDialogComponent.openAlertDialog("Delete successfully", "Product Master");
        this.getProductMasters();
      }
    });
  }

  public openDeleteConfirmationDialog(product_code: string): void {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    _dialogRef.componentInstance.confirmMessage = 'Do you want remove this record?';
    _dialogRef.componentInstance.componentName = "Product Master";
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deleteProductMasters(product_code);
      _dialogRef = null;
    });
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  public openHSNLookup(event: any, i): void {

    if (this.objProductMaster.product_hsn[i].Hsn.toString().trim() === "") {
      this.objProductMaster.product_hsn[i].Hsn_Description = '';
    }
    if (event.keyCode === 13) {
      this.openHSNLookupDialog(i);
    }
  }
  public checkValidHsn(event: KeyboardEvent, i): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objProductMaster.product_hsn[i].Hsn != '' && this.objProductMaster.product_hsn[i].Hsn_Description == '') {
      this.openAlertDialogHsn("Invalid hsn", i, "Product Master");
    }
  }

  openAlertDialogHsn(value: string, i, componentName?: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        setTimeout(() => {
          let inputEls = this.hsn.toArray();
          inputEls[i].nativeElement.focus();
        }, 100);
      }
      _dialogRef = null;
    });
  }
  private getHSNList(): void {
    this._hsnMasterService.getHSNMaster().subscribe((result: any[]) => {
      // console.log(result, "list")
      this.HSNList = result;
    });
  }

  private checkAlreadyEntered(index: number): void {
    let alreadyFlag = false;
    let right = '';
    for (let i = 0; i < this.objProductMaster.product_hsn.length; i++) {
      if (i !== index && this.objProductMaster.product_hsn[i].Hsn === this.objProductMaster.product_hsn[index].Hsn) {
        right = this.objProductMaster.product_hsn[i].Hsn;
        alreadyFlag = true;
        break;
      }
    } if (alreadyFlag) {
      this._confirmationDialogComponent.openAlertDialog("This hsn '" + right + "' already exists on current list", "Product Master");
      this.objProductMaster.product_hsn[index].Hsn = '';
      this.objProductMaster.product_hsn[index].Hsn_Description = "";
    }
  }

  private openHSNLookupDialog(i): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(HsnLookupComponent, {
      width: "650px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objProductMaster.product_hsn[i].Hsn
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {

      this.focusFlag = false;
      //console.log(result, 'rrgjhbr')
      if (result) {
        this.objProductMaster.product_hsn[i].Hsn = result.Hsn;
        this.objProductMaster.product_hsn[i].Hsn_Description = result.Hsn_Description;
        this.checkAlreadyEntered(i);
      } else {
        this.objProductMaster.product_hsn[i].Hsn = '';
        this.objProductMaster.product_hsn[i].Hsn_Description = '';
      }
    });
  }

  public onEnterHSNLookupDetails(i): void {
    let objHSN = this.HSNList.find((rights: any) => rights.Hsn.toLowerCase().trim() === this.objProductMaster.product_hsn[i].Hsn.toString().trim().toLowerCase());
    this.objProductMaster.product_hsn[i].Hsn = objHSN && objHSN.Hsn ? objHSN.Hsn : this.objProductMaster.product_hsn[i].Hsn;
    this.objProductMaster.product_hsn[i].Hsn_Description = objHSN && objHSN.Hsn_Description ? objHSN.Hsn_Description : '';
    this.checkAlreadyEntered(i);
  }

  public openRemoveConfirmationDialog(i): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = "Do you want remove this record ?";
    dialogRef.componentInstance.componentName = "Product Master";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.objProductMaster.product_hsn.splice(i, 1);
      dialogRef = null;
    });
  }

  addNewRow() {
    this.objProductMaster.product_hsn.push({
      Hsn: '',
      Hsn_Description: ''
    })
    setTimeout(() => {
      let inputEls = this.hsn.toArray();
      inputEls[inputEls.length - 1].nativeElement.focus();
    }, 100);
  }
  attibuteChange() {
    debugger
    this.tempAttribute = [];
    let valueLength = this.objProductMaster.product_attributes[0].value_data.length;
    for (let i = 0; i < valueLength; i++) {
      if (this.objProductMaster.product_attributes[0].value_data[i].attribute_value_id == +this.productAtrributes) {
        this.tempAttribute.push(this.objProductMaster.product_attributes[0].value_data[i])
      }
    }
  }
  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Product Code": this.dataSource.data[i].product_code,
          "Product Name": this.dataSource.data[i].product_name,
          "Billing Name": this.dataSource.data[i].product_billing_desc,
          "Active": this.dataSource.data[i].active ? 'Yes' : 'No'
        });
      }
      this._excelService.exportAsExcelFile(json, "Product_Master", datetime);
    } else
      this._confirmationDialogComponent.openAlertDialog("No records found, Load the data first", "Product Master");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.product_code);
        tempObj.push(e.product_name);
        tempObj.push(e.product_billing_desc);
        tempObj.push(e.active ? 'Yes' : 'No');
        prepare.push(tempObj);
      });
      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Product Code", "Product Name", "Billing Name", "Active"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('Product_Master' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No records found, Load the data first", "Product Master");
  }

}
