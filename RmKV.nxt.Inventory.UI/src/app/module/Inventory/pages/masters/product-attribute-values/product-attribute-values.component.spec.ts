import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductAttributeValuesComponent } from './product-attribute-values.component';

describe('ProductAttributeValuesComponent', () => {
  let component: ProductAttributeValuesComponent;
  let fixture: ComponentFixture<ProductAttributeValuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductAttributeValuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductAttributeValuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
