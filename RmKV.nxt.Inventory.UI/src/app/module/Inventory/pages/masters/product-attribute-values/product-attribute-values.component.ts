import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatRadioChange, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode, Rights } from 'src/app/common/models/common-model';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductAttributesService } from '../../../services/masters/product-attributes/product-attributes.service';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { ProductAttributes, ProductAttributeValues } from '../../../model/masters/product-attributes.model';
declare var jsPDF: any;

@Component({
  selector: 'app-product-attribute-values',
  templateUrl: './product-attribute-values.component.html',
  styleUrls: ['./product-attribute-values.component.scss'],
  providers: [DatePipe]
})
export class ProductAttributeValuesComponent implements OnInit {

  componentVisibility: boolean = true;
  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Attribute_Value", "Action"];
  Group_Section_Name: string = this._localStorage.getGroupSectionName();

  objProductAttributes : ProductAttributes = {
    Attribute_Id: 0,
    Attribute_Name: "",
    Active: true
  }

  objProductAttributeValue : ProductAttributeValues = {
    Attribute_Value_Id: 0,
    Attribute_Value: "",
    Active: true
  }

  unchangedProductAttributeValue : ProductAttributeValues = {
    Attribute_Value_Id: 0,
    Attribute_Value: "",
    Active: true
  }

  objModifyProductAttributeValue : ProductAttributeValues = {
    Attribute_Value_Id: 0,
    Attribute_Value: "",
    Active: true
  }

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  productAttributeValueList: ProductAttributeValues[];
  filterProductAttributeValueList: ProductAttributeValues[];

  @ViewChild(MatSort, null) sort: MatSort;
  
  rights: Rights = {
    Add: false,
    Update: false,
    Delete: false
  };

  objProductLoad: any = {
    TimeLine: 1
  }

  keysOfProductAttributes: any[] = [
    { isCheck: true, key: "Attribute_Value" },
    { isCheck: false, key: "Active" },
  ];

  unChangedKeysOfProductAttributes: any[] = [
    { isCheck: true, key: "Attribute_Value" },
    { isCheck: false, key: "Active" },
  ];

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    public _datePipe: DatePipe,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _productAttributeValueService: ProductAttributesService,
    public _keyPressEvents: KeyPressEvents,
    public _matDialog: MatDialog,
    public _excelService: ExcelService,
    public _productAttributeService: ProductAttributesService,) { }

  ngOnInit() {
    let localData = JSON.parse(atob(localStorage.getItem("ProductAttributes")));
    this.assignProductAttributeDatas(localData);
     // this.getRights();
  }

  private getRights(): void {
    var rights = this._localStorage.getMenuRights("/ProductAttributeValues");
    console.log('rights', rights);
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "Delete" && rights[x].Status == true) {
        this.rights.Delete = true;
      }
    }
  }

  private getProductAttributeValue(): void {
    this.matTableConfig([]);
    let data = [{
      attribute_id: this.objProductAttributes.Attribute_Id,
      group_section_id: +this._localStorage.getGlobalGroupSectionId(),
    }]
    let objData = {
      Product_Attribute_List: JSON.stringify(data)
    }
    this._productAttributeValueService.getProductAttributeValues(objData).subscribe((result: any[]) => {
      this.productAttributeValueList = result;
      console.log(result,"listvalue")
      this.productAttributeValueList !== null ? this.loadActiveRecords() : this._confirmationDialogComponent.openAlertDialog("No record found", "Product Attribute Value");
    });
  }

  private loadActiveRecords(): void {
    debugger;
    this.objProductLoad.TimeLine = 1;
    this.filterProductAttributeValueList=[];
    this.productAttributeValueList.forEach(ele => { 
      if (ele.Active === true)
        this.filterProductAttributeValueList.push(ele);
    });
    this.matTableConfig(this.filterProductAttributeValueList);
  } 

  private matTableConfig(tableRecords: any[]): void {
    this.dataSource = new MatTableDataSource(tableRecords);
    this.dataSource.sort = this.sort;
  }

  public addProductValues(): void {
    if (this.beforeSaveValidate()) {
      debugger
      let data = [{
        attribute_value_id: this.objProductAttributeValue.Attribute_Value_Id,
        attribute_id: this.objProductAttributes.Attribute_Id,
        active: this.objProductAttributeValue.Active,
        attribute_value: this.objProductAttributeValue.Attribute_Value.toString().trim(),
        group_section_id: +this._localStorage.getGlobalGroupSectionId(),
      }]
      let objProductAttributeValue = {
        Product_Attribute_List: JSON.stringify(data)
      }
      this._productAttributeValueService.addProductAttributeValues(objProductAttributeValue).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New module record has been created", "Product Attribute Value");
        }
        if (this.objAction.isEditing === true) {
          if (this.objProductAttributeValue.Active == this.objModifyProductAttributeValue.Active) {
            this.resetScreen();
            this.getProductAttributeValue();
            this.componentVisibility = !this.componentVisibility;
          }
          this.objModifyProductAttributeValue.Active = !this.objModifyProductAttributeValue.Active;
        } else {
          this.resetScreen();
          this.getProductAttributeValue();
          this.componentVisibility = !this.componentVisibility;
        }
      });
    }
  }

  // public modifyOrViewProductAttributeValue(product : ProductAttributeValues, isEditing: boolean): void {
  //   this.objAction = {
  //     isEditing: isEditing ? true : false,
  //     isView: isEditing ? false : true,
  //   }
  //   this.objProductAttributeValue = Object.assign({}, product);
  //   this.objModifyProductAttributeValue = Object.assign({}, product);
  //   this.keysOfProductAttributes[1].isCheck = true;
  //   this.componentVisibility = !this.componentVisibility;
  // }

  public modifyOrViewProductAttributeValue(i: number): void {
    let objFetch = {
      Product_Attribute_List: JSON.stringify([{
        attribute_value_id: this.dataSource.data[i].Attribute_Value_Id,
        attribute_id: this.dataSource.data[i].Attribute_Id,
        group_section_id: this._localStorage.getGlobalGroupSectionId(),
      }])
    }
    this._productAttributeService.modifyProductAttributeValues(objFetch).subscribe((result: any) => {
      if (result) {
        debugger;
        this.objAction.isEditing = true;
        this.objProductAttributeValue = JSON.parse(JSON.stringify(result[0]));
        this.objModifyProductAttributeValue = JSON.parse(JSON.stringify(result[0]));
        console.log(result,"fetchvalue")
        this.componentVisibility = !this.componentVisibility;
      }
    })
  }

  public newClick(): void {
    // if (this.rights.Add) {
    this.resetScreen();
    this.objAction.isEditing = false;
    this.componentVisibility = !this.componentVisibility;
    // }
  }

  public onListClick(): void {
    this.getProductAttributeValue();
    this.componentVisibility = !this.componentVisibility;
  }

  public onExit(): void {
    this._router.navigate(["/Inventory/ProductAttributes"]);
  }

  private assignProductAttributeDatas(records): void {
    this.objProductAttributes.Attribute_Id = records.Attribute_Id,
      this.objProductAttributes.Attribute_Name = records.Attribute_Name,
      this.getProductAttributeValue();
  }

      /****************************************** Validations **********************************************/

  private beforeSaveValidate(): boolean {
    if (!this.objProductAttributeValue.Attribute_Value.toString().trim()) {
      document.getElementById('attributeValue').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Attribute Value", "Product Attribute Value");
      return false;
    } else {
      return true;
    }
  }

  public radioChange(event: MatRadioChange): void {
    this.filterProductAttributeValueList=[];
    if (event.value === 1) {
      this.productAttributeValueList.forEach(element => {
        if (+element.Active === 1) {
          this.filterProductAttributeValueList.push(element);
        }
      });
    } else if (event.value === 0) {
      this.productAttributeValueList.forEach(element => {
        if (+element.Active === 0) {
          this.filterProductAttributeValueList.push(element);
        }
      });
    } else {
      this.productAttributeValueList.forEach(element => {
        this.filterProductAttributeValueList.push(element);
      });
    }this.matTableConfig(this.filterProductAttributeValueList);
  }

  public changeFlagConfirmation(): void {
    if (this.objAction.isEditing === true) {
      this.openFlagConfirmationDialogs(this.objProductAttributeValue.Active == false ? "Do you want to Deactivate this record?" : "Do you want to make this record active?")
    }
  }

  private openFlagConfirmationDialogs(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Product Attribute Value";
    dialogRef.afterClosed().subscribe(result => {
      result ? this.addProductValues() : this.objProductAttributeValue.Active = this.objProductAttributeValue.Active ? false : true;
      dialogRef = null;
    });
  }

  // private checkAnyChangesMade(): boolean {
  //   for (let i = 0; i < this.keysOfProductAttributes.length; i++) {
  //     if (this.keysOfProductAttributes[i].isCheck)
  //       if (this.objProductAttributeValue[this.keysOfProductAttributes[i].key] !==
  //         (!this.objAction.isEditing ? this.unchangedProductAttributeValue[this.keysOfProductAttributes[i].key]
  //           : this.objModifyProductAttributeValue[this.keysOfProductAttributes[i].key]))
  //         return true;
  //   }
  //   return false;
  // }

  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objProductAttributeValue) !== (this.objAction.isEditing ? JSON.stringify(this.objModifyProductAttributeValue) : JSON.stringify(this.unchangedProductAttributeValue)))
      return true;
    else return false;
  }

  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Product Attribute Value";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  private resetScreen(): void {
    this.objModifyProductAttributeValue = Object.assign({}, this.unchangedProductAttributeValue);
    this.objProductAttributeValue = Object.assign({}, this.unchangedProductAttributeValue);
    this.keysOfProductAttributes = this.unChangedKeysOfProductAttributes;
  }


    /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Attribute Value': this.dataSource.data[i].Attribute_Value,
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Product Attribute Value",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Product Attribute Value");
  }

  exportToPdf(): void {
    debugger
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.productAttributeValueList.forEach(e => {
        var tempObj = [];
        tempObj.push(e.Attribute_Value);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Attribute Value']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Product Attribute Value' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Product Attribute Value");
  }

}

