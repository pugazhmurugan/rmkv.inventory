import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionProductsComponent } from './production-products.component';

describe('ProductionProductsComponent', () => {
  let component: ProductionProductsComponent;
  let fixture: ComponentFixture<ProductionProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
