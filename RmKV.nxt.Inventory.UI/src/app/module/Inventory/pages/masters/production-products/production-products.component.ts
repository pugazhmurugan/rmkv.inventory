import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatRadioChange, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode, Rights } from 'src/app/common/models/common-model';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductionProductsService } from '../../../services/masters/Production-Product/production-products.service';
declare var jsPDF: any;
@Component({
  selector: 'app-production-products',
  templateUrl: './production-products.component.html',
  styleUrls: ['./production-products.component.scss'],
  providers: [DatePipe]
})
export class ProductionProductsComponent implements OnInit {
  componentVisibility: boolean = true;

  objProdProducts: any = {
    prod_id: 0,
    prod_name: '',
    special_product: false,
    active:true,
  }

  objModifyProdProducts: any = {
    prod_id: 0,
    prod_name: '',
    special_product: false,
    active:true,
  }

  objUnchangedProdProducts: any = {
    prod_id: 0,
    prod_name: '',
    special_product: false,
    active:true,
  }

  productSizesList: any[] = [{
    prod_size: ''
  }]

  modifyProductSizesList: any[] = [{
    prod_size: ''
  }]

  unChangeProductSizesList: any[] = [{
    prod_size: ''
  }]

  objProductSizesList: any = {
    prod_size: ''
  }
  objUnChangeProductSizesList: any = {
    prod_size: ''
  }

  objProdProductsLoad: any = {
    TimeLine: 2
  }
  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };
  columns: any[] = [
    { display: 'SNo', editable: false },
    { display: 'productionSize', editable: true },
    { display: 'action', editable: true }
  ];
  @ViewChildren('productionSize') productionSize: ElementRef | any;
  @ViewChildren('action') action: ElementRef | any;
  searchString: string;

  filterCategoriesList: any[];

  filterprodNameList: any;


  constructor(
    public _router: Router,
    public _localStorage: LocalStorage,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    private _datePipe: DatePipe,
    public _excelService: ExcelService,
    public _keyPressEvents: KeyPressEvents,
    public _productionProductsService: ProductionProductsService,
    public _gridKeyEvents: GridKeyEvents
  ) {

  }
  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.productSizesList);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.productSizesList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this.productSizesList);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.productSizesList);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.productSizesList);
        break;
    }
  }
  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Prod_Name", "Special_Product", "Action"];

  ngOnInit() {
    this.getProductionProduct();
  }

  newClick() {
    this.componentVisibility = !this.componentVisibility;
    this.addNewRowToProductionProductsDetails();
    this.resetScreen();
  }

  public addNewRowToProductionProductsDetails(): void {
    debugger
    this.productSizesList.push(Object.assign({}, this.objProductSizesList));
    setTimeout(() => {
      let input = this.productionSize.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100);
  }

  public removeRowFromProductionProductsDetails(i: number): void {
    this.productSizesList.splice(i, 1);
  }
  
  private matTableConfig(tableRecords: any[]): void {
    this.dataSource = new MatTableDataSource(tableRecords);
  }

  public saveProductionProduct(): void {
    debugger;
    if (this.beforeSaveValidate()) {
      debugger;
      let obj: any = {
        ProdProduct: JSON.stringify([{
          prod_id: this.objAction.isEditing ? +this.objProdProducts.prod_id : 0,
          prod_name: this.objProdProducts.prod_name,
          special_product: this.objProdProducts.special_product,
          active:this.objProdProducts.active,
          details: this.getProductDetailsToAdd(),
          entered_by: this._localStorage.intGlobalUserId()
        }])
      }
      this._productionProductsService.saveProductionProduct(obj).subscribe((result: any) => {
        if (result === 1)
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New production Products record has been created", "production Products");
        this.onListClick();
        this.getProductionProduct();
      })
    }
  }

  private getProductDetailsToAdd(): any[] {
    let tempArr: any[] = [];
    for (let i = 0; i < this.productSizesList.length; i++) {
      let element = this.productSizesList[i];
      tempArr.push({
        prod_size: element.prod_size.toString().trim()
      });
    }
    return tempArr;
  }
  public changeFlagConfirmation(): void {
    debugger;
    if (this.objAction.isEditing === true) {
      this.openFlagConfirmationDialogs(this.objProdProducts.Active  ? "Do you want to Deactivate this records?" : "Do you want to make this record active?")
    }
  }
  public openFlagConfirmationDialogs(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Production Product";
    dialogRef.afterClosed().subscribe(result => {
      result ? this.saveProductionProduct() : this.objProdProducts.Active = this.objProdProducts.Active ? false : true;
      dialogRef = null;
    });
  }
  
  public modifyProductionProducts(isEditing: boolean, i: number, isView?: boolean): any {
    if (isEditing)
      this.objAction.isEditing = true;
    else if (isView)
      this.objAction.isView = true;
    let objData = {
      ProdProduct: JSON.stringify([{
        prod_id: this.dataSource.data[i].prod_id,
      }])
    }
    this._productionProductsService.FetchProductionProduct(objData).subscribe((result: any) => {
      if (result) {
        console.log(result,'fetch')
        this.objProdProducts = JSON.parse(JSON.stringify(result))[0];
        this.objModifyProdProducts = JSON.parse(JSON.stringify(result))[0];
        this.productSizesList = JSON.parse(JSON.stringify(result))[0].sizedata;
        this.modifyProductSizesList = JSON.parse(JSON.stringify(result))[0].sizedata;
      
      }
      
      this.componentVisibility = !this.componentVisibility;
    });
  }

  public getProductionProduct(): void {
    this.matTableConfig([]);
    this._productionProductsService.getProductionProduct().subscribe((result: any) => {
      if (result) {
        this.matTableConfig(result);
        this.filterprodNameList = JSON.parse(JSON.stringify(result));
      }
    });
  }

  public radioChange(event: MatRadioChange): void {
    debugger;
    this.filterCategoriesList = [];
    if (event.value === 1) {
      this.filterprodNameList.forEach(element => {
        if (element.active === true) {
          this.filterCategoriesList.push(element);
        }
      });
      this.matTableConfig(this.filterCategoriesList);
    } else if (event.value === 0) {
      this.filterprodNameList.forEach(element => {
        if (element.active === false) {
          this.filterCategoriesList.push(element); }
      });
      this.matTableConfig(this.filterCategoriesList);
    } else {
      this.filterprodNameList.forEach(element => {
        this.filterCategoriesList.push(element);
      });
      this.searchRights(this.searchString)
    }
  }

  public searchRights(searchValue: string): void {
    searchValue = searchValue ? searchValue.toString().trim().toLocaleLowerCase() : "";
    this.searchString = JSON.parse(JSON.stringify(searchValue.toString().trim()));
    let filterprodNameList = this.filterCategoriesList.filter(element =>
      element.prod_name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    this.matTableConfig(filterprodNameList);
  }
  
  
  /***************************Validations***********************************************/
  private beforeSaveValidate(): boolean {
    this.trimStringFields();
    debugger;
    if ([null, 'null', undefined, 'undfefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objProdProducts.prod_name) !== -1) {
      this.openAlertDialog('Enter Product Name', 'Production Product', 'product name');
      return false;
    } if (this.productSizesList.length === 1 && [null, 'null', undefined, 'undefined', 0, '0', ''].indexOf(this.productSizesList[0].prod_size) !== -1) {
      this._confirmationDialogComponent.openAlertDialog('No records to save', 'Production Product');
      return;
    } if (!this.beforeSaveValidateProductionProductDetails()) {
      return false;
    } return true;
  }
  private trimStringFields(): void {
    this.objProdProducts.prod_name =this.objProdProducts.prod_name.toString().trim();
    this.objModifyProdProducts.prod_name=this.objModifyProdProducts.prod_name.toString().trim();
    this.objUnchangedProdProducts.prod_name= this.objUnchangedProdProducts.prod_name.toString().trim();
    this.productSizesList.forEach(x => {
      x.prod_size = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(x.prod_size) === -1 ? x.prod_size.toString().trim() : '';   
    });
    this.modifyProductSizesList.forEach(x => {
      x.prod_size = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(x.prod_size) === -1 ? x.prod_size.toString().trim() : '';   
    });
    this.unChangeProductSizesList.forEach(x => {
      x.prod_size = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(x.prod_size) === -1 ? x.prod_size.toString().trim() : '';   
    });

  }
 


  private beforeSaveValidateProductionProductDetails(): boolean {
    for (let i = 0; i < this.productSizesList.length; i++) {
      let element = this.productSizesList[i];
      if (this.productSizesList[i].prod_size === '') {
        this.productSizesList.splice(i, 1);
        continue;
      } if ([null, 'null', undefined, 'undfefined', NaN, 'NaN', 0, '0', ''].indexOf(element.prod_size) !== -1) {
        this.openInvalidAlertDialog('Enter Product Size', i, 'Production Product', 'prod_size');
        return false;
      }
    } return true
  }

  public onlyAllowUpperCases(event: KeyboardEvent | any): void {
    if (event.keyCode >= 65 && event.keyCode <= 90)
      event.target.value = event.target.value.toString().trim().toUpperCase();
  }

  private openAlertDialog(value: string, componentName: string, focus: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        document.getElementById(focus).focus();
      _dialogRef = null;
    });
  }

  private openInvalidAlertDialog(value: string, i: number, componentName: string, focus: any) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this[focus].toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  private resetScreen(): void {
    debugger;
    if (this.objAction.isEditing) {
      this.objProdProducts = JSON.parse(JSON.stringify(this.objModifyProdProducts));
      this.productSizesList = JSON.parse(JSON.stringify(this.modifyProductSizesList));
    } else {
      this.objProdProducts = Object.assign({}, this.objUnchangedProdProducts);
      this.objModifyProdProducts = Object.assign({}, this.objUnchangedProdProducts);
      this.objProductSizesList = JSON.parse(JSON.stringify(this.objUnChangeProductSizesList));
      this.productSizesList = [];
      this.addNewRowToProductionProductsDetails();
    }
  }

  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost, Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }
  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Production Product";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  private onListClick(): void {
    this.objAction = JSON.parse(JSON.stringify(this.unChangedAction));
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  private checkAnyChangesMade(): boolean {
    if (JSON.stringify(this.objProdProducts) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.objModifyProdProducts)
      : JSON.stringify(this.objUnchangedProdProducts)) ||
      JSON.stringify(this.productSizesList) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.modifyProductSizesList)
        : JSON.stringify(this.unChangeProductSizesList)))
      return true;
    else
      return false;
  }

  public onlyAllowAlphanumeric(event: KeyboardEvent | any) {
    const pattern = /^[a-zA-Z0-9 ]*$/;
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^a-zA-Z0-9 ]/g, "");
      return false;
    } else return true;
  }

  /*************************************************** Exports ************************************************/
  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Product name": this.dataSource.data[i].prod_name,
          "Special Product": this.dataSource.data[i].special_product,
        });
      }
      this._excelService.exportAsExcelFile(json, "Production Product", datetime);
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found", "Production Product");
  }

  exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.prod_name);
        tempObj.push(e.special_product);
        prepare.push(tempObj);
      });
      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Product name", "Special Product"]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Production Product' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Production Product");
  }
}
