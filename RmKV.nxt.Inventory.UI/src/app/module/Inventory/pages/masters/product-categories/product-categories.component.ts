import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatDialogRef, MatRadioChange, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductCategoryService } from '../../../services/masters/product-category/product-category.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { EditMode } from 'src/app/common/models/common-model';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { DatePipe } from '@angular/common';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { ProductCategory, ProductCategoryList } from '../../../model/masters/product-category.model';
declare var jsPDF: any;

@Component({
  selector: 'app-product-categories',
  templateUrl: './product-categories.component.html',
  styleUrls: ['./product-categories.component.scss'],
  providers: [DatePipe]
})

export class ProductCategoriesComponent implements OnInit {

  componentVisibility: boolean = true;
  Group_Section_Name: string = this._localStorage.getGroupSectionName();
  objCategories:ProductCategory ={
    Category_Id:0,
    Group_Section_Name: this._localStorage.getGroupSectionName(),
    Category_Name:"",
    Group_Section_Id: 0,
    Active:true,
  }

  objCategoriesModify:ProductCategory ={
    Category_Id:0,
    Group_Section_Name: this._localStorage.getGroupSectionName(),
    Category_Name:"",
    Group_Section_Id: 0, 
    Active:true,
  }


  objCategoriesUnchanged:ProductCategory ={
    Category_Id:0,
    Group_Section_Name: this._localStorage.getGroupSectionName(),
    Category_Name:"",
    Group_Section_Id: 0,
    Active:true,
  }

  objCategoriesLoad: any = {
    TimeLine: 1
  }

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  CategoryList: ProductCategoryList[] = [];
  filterCategoriesList: ProductCategoryList[] = [];

  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Category_Name", "Action"];

  @ViewChild(MatSort, null) sort: MatSort;
  
  constructor(public _localStorage: LocalStorage,
    public _router: Router,private _productCategoryService:ProductCategoryService,
    private _confirmationDialogComponent: ConfirmationDialogComponent,
    private _datePipe: DatePipe,
    public _matDialog: MatDialog,public _keyPressEvents: KeyPressEvents,
    public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _excelService: ExcelService,) { }

  ngOnInit() {
    this.categoryLoad();
  }

 /* ============================================================= Functionalities  ==========================================
 */
  public categoryLoad(): void {
   
      let objGetLoad: any = {
        group_section_id: +this._localStorage.getGlobalGroupSectionId(),
      }
      this._productCategoryService.getCategory(objGetLoad).subscribe((result: any) => {
        this.filterCategoriesList = [];
        this.CategoryList = result;
        this.filterCategoriesList = result
        this.CategoryList.length > 0 ? this.loadActiveRecords() : this._confirmationDialogComponent.openAlertDialog("No record found", "Product Categories");

      })
    }

    private loadActiveRecords(): void {
      debugger;
      this.objCategoriesLoad.TimeLine = 1;
      // this.filterCategoriesList = [];
      this.matTableConfig(this.CategoryList.filter(x => x.Active === true));
    }

    public matTableConfig(tableRecords: any[]): void {
      this.dataSource = new MatTableDataSource(tableRecords);
      this.dataSource.sort = this.sort;
    }

    public radioChange(event: MatRadioChange): void {
      this.filterCategoriesList = [];
      if (event.value === 1) {
        this.CategoryList.forEach(element => {
          if (element.Active === true) {
            this.filterCategoriesList.push(element);
          }
        });
        this.matTableConfig(this.filterCategoriesList);
      } else if (event.value === 0) {
        this.CategoryList.forEach(element => {
          if (element.Active === false) {
            this.filterCategoriesList.push(element);
          }
        });
        this.matTableConfig(this.filterCategoriesList);
      } else {
        this.CategoryList.forEach(element => {
          this.filterCategoriesList.push(element);
        });
        this.matTableConfig(this.filterCategoriesList);
      }
    }

    public saveCategories(): void {
      debugger;
       if (this.beforeSaveValidate()) {
        let tempSaveArr = [];
        let objData = {
          category_id: this.objAction.isEditing ? +this.objCategories.Category_Id : 0,
          category_name: this.objCategories.Category_Name.toString().trim(),
          group_section_id: this._localStorage.getGlobalGroupSectionId(),
          active: this.objCategories.Active,
        }
        tempSaveArr.push(objData);
        let objSave = {
          SaveCategories: JSON.stringify(tempSaveArr)
        }
  
        this._productCategoryService.saveCategory(objSave).subscribe((result: any) => {
          if (result == 1) {
            this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "Product Category record has been created", "Product Category");
          }
          if (this.objAction.isEditing === true) {
            if (this.objCategories.Active == this.objCategoriesModify.Active) {
              this.resetScreen();
              this.categoryLoad();
              this.componentVisibility = !this.componentVisibility;
            }
            this.objCategoriesModify.Active = !this.objCategoriesModify.Active;
          } else {
            this.resetScreen();
            this.categoryLoad();
            this.componentVisibility = !this.componentVisibility;
          }
    
        });
      }
    }

    
  public modifyCategories(isEditing: boolean, index: number): void {
    this.objAction = {
      isEditing: isEditing ? true : false
    }
    let objModify: any = [{
      category_id: this.dataSource.data[index].Category_Id,
      group_section_id: this._localStorage.getGlobalGroupSectionId(),
    }]
    let objFetch = {
      FetchCategory: JSON.stringify(objModify)
    }

    this._productCategoryService.FetchCategory(objFetch).subscribe((result: any) => {
      if (result) {
        debugger;
        this.objCategories = JSON.parse(JSON.stringify(result[0]));
        this.objCategoriesModify = JSON.parse(JSON.stringify(result[0]));
       this.componentVisibility = !this.componentVisibility;
      }
    });
  }

  private deleteCategory(row: any): void {
    debugger;
    let tempSaveArr = [];
    let objData = {
      category_id: this.dataSource.data[row].Category_Id,
      group_section_id: this._localStorage.getGlobalGroupSectionId(),
    }
    tempSaveArr.push(objData);
    let objCancel = {
      CancelCategory: JSON.stringify(tempSaveArr)
    }
    this._productCategoryService.RemoveCategory(objCancel).subscribe(
      (result: any) => {
        if (result.error) {
          this._confirmationDialogComponent.openAlertDialog(result.error, "Product Category");
        } else if (result) {
          this._confirmationDialogComponent.openAlertDialog("Removed", "Product Category");
        } else {
          this._confirmationDialogComponent.openAlertDialog(result.error, "Product Category");
        }
        this.categoryLoad();
      });
    this.dialogRef = null;
  }

/* ============================================================= Validations  ==========================================
 */
public newClick(): void {
  this.resetScreen();
  this.componentVisibility = !this.componentVisibility;
}

private resetScreen(): void {
  this.objAction = Object.assign({}, this.unChangedAction);
  this.objCategories = Object.assign({}, this.objCategoriesUnchanged);
  this.objCategoriesModify = Object.assign({}, this.objCategoriesUnchanged);
}

private onListClick(): void {
  this.categoryLoad();
  this.resetScreen();
  this.componentVisibility = !this.componentVisibility;
}

public changeFlagConfirmation(): void {
  if (this.objAction.isEditing === true) {
    this.openFlagConfirmationDialogs(this.objCategories.Active == false ? "Do you want to Deactivate this records?" : "Do you want to make this record active?")
  }
}

public openFlagConfirmationDialogs(message: string, exitFlag?: boolean): void {
  let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
    panelClass: "custom-dialog-container",
    data: { confirmationDialog: 1 }
  });
  dialogRef.componentInstance.confirmMessage = message;
  dialogRef.componentInstance.componentName = "Product Category";
  dialogRef.afterClosed().subscribe(result => {
    result ? this.saveCategories() : this.objCategories.Active = this.objCategories.Active ? false : true;
    dialogRef = null;
  });
}


public onClickDelete(index: any): any {
  this.dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
    panelClass: "custom-dialog-container",
    data: { confirmationDialog: 1 }
  });
  this.dialogRef.componentInstance.confirmMessage =
    "Do you want to remove the Categories of '" + this.dataSource.data[index].Category_Name + "'";
  this.dialogRef.componentInstance.componentName = "Product Categories";
  return this.dialogRef.afterClosed().subscribe(result => {
    if (result) {
      this.deleteCategory(index);
    }
    this.dialogRef = null;
  });
}

private openConfirmationDialog(message: string, exitFlag?: boolean): void {
  let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
    panelClass: "custom-dialog-container",
    data: { confirmationDialog: 1 }
  });
  dialogRef.componentInstance.confirmMessage = message;
  dialogRef.componentInstance.componentName = "Product Category";
  dialogRef.afterClosed().subscribe(result => {
    if (result)
      exitFlag ? this.onListClick() : this.resetScreen();
    dialogRef = null;
  });
}

public onClear(exitFlag: boolean): void {
  debugger;
  if (this.checkAnyChangesMade())
    this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
  else if (exitFlag)
    this.onListClick();
}

private checkAnyChangesMade(): boolean {
  debugger;
  if (JSON.stringify(this.objCategories) !== ((this.objAction.isEditing) ? JSON.stringify(this.objCategoriesModify)
    : JSON.stringify(this.objCategoriesUnchanged)))
    return true;
  else
    return false;
}

private beforeSaveValidate(): boolean {
  debugger;
  if (!this.objCategories.Category_Name.toString().trim()) {
    document.getElementById("categoryName").focus();
    this._confirmationDialogComponent.openAlertDialog("Enter Category Name", "Category");
    return false;
  }
  else
    return true;
}

removeSpecialCharacters(event: any) {
  this.objCategories.Category_Name = event.target.value.replace(/[^a-zA-Z ]/g, "");
}

 /*************************************************** Exports ************************************************/

public exportToExcel(): void {
  if (this.dataSource.data.length > 0) {
    let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
    let json = [];
    for (let i = 0; i < this.dataSource.data.length; i++) {
      json[i] = Object.assign({
        "category Name": this.dataSource.data[i].Category_Name,
      });
    }
    this._excelService.exportAsExcelFile(json, "Product Categories", datetime);
  } else
    this._confirmationDialogComponent.openAlertDialog("No record found", "Product Categories");
}

exportToPdf(): void {

  if (this.dataSource.data.length != 0) {
    var prepare = [];
    this.dataSource.data.forEach(e => {
      var tempObj = [];
      tempObj.push(e.Category_Name);
      prepare.push(tempObj);
    });

    const doc = new jsPDF('l', 'pt', "a4");
    doc.autoTable({
      head: [["Category Name",]],
      body: prepare,
      styles: { overflow: 'linebreak', columnWidth: 'wrap' },
      columnStyles: { text: { columnWidth: 'auto' } },
      didParseCell: function (table) {
        if (table.section === 'head') {
          table.cell.styles.textColor = '#FFFFFF';
          table.cell.styles.fillColor = '#d32f2f';
        }
      }
    });

    doc.save('Product Categories' + '.pdf');
  } else
    this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Product Categories");
}

  

}
