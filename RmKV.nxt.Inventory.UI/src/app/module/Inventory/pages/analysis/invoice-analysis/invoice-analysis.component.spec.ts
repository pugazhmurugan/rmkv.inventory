import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceAnalysisComponent } from './invoice-analysis.component';

describe('InvoiceAnalysisComponent', () => {
  let component: InvoiceAnalysisComponent;
  let fixture: ComponentFixture<InvoiceAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
