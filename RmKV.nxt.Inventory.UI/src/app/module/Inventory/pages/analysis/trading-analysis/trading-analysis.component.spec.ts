import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradingAnalysisComponent } from './trading-analysis.component';

describe('TradingAnalysisComponent', () => {
  let component: TradingAnalysisComponent;
  let fixture: ComponentFixture<TradingAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradingAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradingAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
