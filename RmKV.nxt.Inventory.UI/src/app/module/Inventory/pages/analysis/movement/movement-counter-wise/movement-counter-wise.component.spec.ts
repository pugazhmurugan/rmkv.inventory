import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementCounterWiseComponent } from './movement-counter-wise.component';

describe('MovementCounterWiseComponent', () => {
  let component: MovementCounterWiseComponent;
  let fixture: ComponentFixture<MovementCounterWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementCounterWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovementCounterWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
