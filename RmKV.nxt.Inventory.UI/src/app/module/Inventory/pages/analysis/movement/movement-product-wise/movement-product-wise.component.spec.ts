import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementProductWiseComponent } from './movement-product-wise.component';

describe('MovementProductWiseComponent', () => {
  let component: MovementProductWiseComponent;
  let fixture: ComponentFixture<MovementProductWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementProductWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovementProductWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
