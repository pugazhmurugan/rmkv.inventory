import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BynoHistoryComponent } from './byno-history.component';

describe('BynoHistoryComponent', () => {
  let component: BynoHistoryComponent;
  let fixture: ComponentFixture<BynoHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BynoHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BynoHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
