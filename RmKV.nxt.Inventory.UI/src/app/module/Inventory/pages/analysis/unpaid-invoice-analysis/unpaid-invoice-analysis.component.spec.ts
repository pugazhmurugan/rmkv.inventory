import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnpaidInvoiceAnalysisComponent } from './unpaid-invoice-analysis.component';

describe('UnpaidInvoiceAnalysisComponent', () => {
  let component: UnpaidInvoiceAnalysisComponent;
  let fixture: ComponentFixture<UnpaidInvoiceAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnpaidInvoiceAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnpaidInvoiceAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
