import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsTransferAnalysisComponent } from './goods-transfer-analysis.component';

describe('GoodsTransferAnalysisComponent', () => {
  let component: GoodsTransferAnalysisComponent;
  let fixture: ComponentFixture<GoodsTransferAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsTransferAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsTransferAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
