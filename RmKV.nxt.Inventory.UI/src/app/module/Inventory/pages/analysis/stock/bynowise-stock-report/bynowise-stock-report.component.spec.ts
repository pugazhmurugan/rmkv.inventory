import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BynowiseStockReportComponent } from './bynowise-stock-report.component';

describe('BynowiseStockReportComponent', () => {
  let component: BynowiseStockReportComponent;
  let fixture: ComponentFixture<BynowiseStockReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BynowiseStockReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BynowiseStockReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
