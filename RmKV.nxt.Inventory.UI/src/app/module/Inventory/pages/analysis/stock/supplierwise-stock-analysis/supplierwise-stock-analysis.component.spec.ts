import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierwiseStockAnalysisComponent } from './supplierwise-stock-analysis.component';

describe('SupplierwiseStockAnalysisComponent', () => {
  let component: SupplierwiseStockAnalysisComponent;
  let fixture: ComponentFixture<SupplierwiseStockAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierwiseStockAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierwiseStockAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
