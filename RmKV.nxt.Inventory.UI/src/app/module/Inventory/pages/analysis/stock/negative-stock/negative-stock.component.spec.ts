import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegativeStockComponent } from './negative-stock.component';

describe('NegativeStockComponent', () => {
  let component: NegativeStockComponent;
  let fixture: ComponentFixture<NegativeStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegativeStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegativeStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
