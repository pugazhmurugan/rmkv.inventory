import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockAnalysisQtyComponent } from './stock-analysis-qty.component';

describe('StockAnalysisQtyComponent', () => {
  let component: StockAnalysisQtyComponent;
  let fixture: ComponentFixture<StockAnalysisQtyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockAnalysisQtyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockAnalysisQtyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
