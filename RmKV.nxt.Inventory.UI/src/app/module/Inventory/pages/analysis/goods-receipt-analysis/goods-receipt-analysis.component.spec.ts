import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsReceiptAnalysisComponent } from './goods-receipt-analysis.component';

describe('GoodsReceiptAnalysisComponent', () => {
  let component: GoodsReceiptAnalysisComponent;
  let fixture: ComponentFixture<GoodsReceiptAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsReceiptAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsReceiptAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
