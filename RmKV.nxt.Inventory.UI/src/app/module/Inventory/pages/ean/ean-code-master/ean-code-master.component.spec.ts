import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EanCodeMasterComponent } from './ean-code-master.component';

describe('EanCodeMasterComponent', () => {
  let component: EanCodeMasterComponent;
  let fixture: ComponentFixture<EanCodeMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EanCodeMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EanCodeMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
