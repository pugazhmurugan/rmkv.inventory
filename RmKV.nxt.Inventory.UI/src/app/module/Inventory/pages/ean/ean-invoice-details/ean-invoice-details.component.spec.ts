import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EanInvoiceDetailsComponent } from './ean-invoice-details.component';

describe('EanInvoiceDetailsComponent', () => {
  let component: EanInvoiceDetailsComponent;
  let fixture: ComponentFixture<EanInvoiceDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EanInvoiceDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EanInvoiceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
