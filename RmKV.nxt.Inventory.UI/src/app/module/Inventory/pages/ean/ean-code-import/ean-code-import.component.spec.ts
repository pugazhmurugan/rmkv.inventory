import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EanCodeImportComponent } from './ean-code-import.component';

describe('EanCodeImportComponent', () => {
  let component: EanCodeImportComponent;
  let fixture: ComponentFixture<EanCodeImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EanCodeImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EanCodeImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
