import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatioVsStockVsRefilComponent } from './ratio-vs-stock-vs-refil.component';

describe('RatioVsStockVsRefilComponent', () => {
  let component: RatioVsStockVsRefilComponent;
  let fixture: ComponentFixture<RatioVsStockVsRefilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatioVsStockVsRefilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatioVsStockVsRefilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
