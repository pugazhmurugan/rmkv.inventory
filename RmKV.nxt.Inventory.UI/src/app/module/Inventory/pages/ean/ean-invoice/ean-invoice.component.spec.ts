import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EanInvoiceComponent } from './ean-invoice.component';

describe('EanInvoiceComponent', () => {
  let component: EanInvoiceComponent;
  let fixture: ComponentFixture<EanInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EanInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EanInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
