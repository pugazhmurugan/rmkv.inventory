import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoreMasterComponent } from './core-master.component';

describe('CoreMasterComponent', () => {
  let component: CoreMasterComponent;
  let fixture: ComponentFixture<CoreMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoreMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoreMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
