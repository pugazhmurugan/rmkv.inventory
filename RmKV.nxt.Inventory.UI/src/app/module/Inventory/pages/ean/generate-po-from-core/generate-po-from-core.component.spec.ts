import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneratePoFromCoreComponent } from './generate-po-from-core.component';

describe('GeneratePoFromCoreComponent', () => {
  let component: GeneratePoFromCoreComponent;
  let fixture: ComponentFixture<GeneratePoFromCoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneratePoFromCoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneratePoFromCoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
