import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoreRatioMasterComponent } from './core-ratio-master.component';

describe('CoreRatioMasterComponent', () => {
  let component: CoreRatioMasterComponent;
  let fixture: ComponentFixture<CoreRatioMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoreRatioMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoreRatioMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
