import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetCoreInfoComponent } from './get-core-info.component';

describe('GetCoreInfoComponent', () => {
  let component: GetCoreInfoComponent;
  let fixture: ComponentFixture<GetCoreInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetCoreInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetCoreInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
