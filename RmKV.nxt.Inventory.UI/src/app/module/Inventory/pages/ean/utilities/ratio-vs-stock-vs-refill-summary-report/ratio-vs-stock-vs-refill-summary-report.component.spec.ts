import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatioVsStockVsRefillSummaryReportComponent } from './ratio-vs-stock-vs-refill-summary-report.component';

describe('RatioVsStockVsRefillSummaryReportComponent', () => {
  let component: RatioVsStockVsRefillSummaryReportComponent;
  let fixture: ComponentFixture<RatioVsStockVsRefillSummaryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatioVsStockVsRefillSummaryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatioVsStockVsRefillSummaryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
