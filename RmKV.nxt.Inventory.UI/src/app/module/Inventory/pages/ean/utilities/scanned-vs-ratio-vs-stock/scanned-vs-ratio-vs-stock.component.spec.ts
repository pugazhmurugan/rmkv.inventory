import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScannedVsRatioVsStockComponent } from './scanned-vs-ratio-vs-stock.component';

describe('ScannedVsRatioVsStockComponent', () => {
  let component: ScannedVsRatioVsStockComponent;
  let fixture: ComponentFixture<ScannedVsRatioVsStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScannedVsRatioVsStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScannedVsRatioVsStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
