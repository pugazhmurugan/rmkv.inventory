import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorewiseStockReportComponent } from './corewise-stock-report.component';

describe('CorewiseStockReportComponent', () => {
  let component: CorewiseStockReportComponent;
  let fixture: ComponentFixture<CorewiseStockReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorewiseStockReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorewiseStockReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
