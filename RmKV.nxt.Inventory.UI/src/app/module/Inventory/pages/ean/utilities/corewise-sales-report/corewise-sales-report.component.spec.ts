import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorewiseSalesReportComponent } from './corewise-sales-report.component';

describe('CorewiseSalesReportComponent', () => {
  let component: CorewiseSalesReportComponent;
  let fixture: ComponentFixture<CorewiseSalesReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorewiseSalesReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorewiseSalesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
