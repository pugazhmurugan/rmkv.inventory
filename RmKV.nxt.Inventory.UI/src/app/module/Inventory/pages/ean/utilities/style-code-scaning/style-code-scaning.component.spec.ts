import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleCodeScaningComponent } from './style-code-scaning.component';

describe('StyleCodeScaningComponent', () => {
  let component: StyleCodeScaningComponent;
  let fixture: ComponentFixture<StyleCodeScaningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleCodeScaningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleCodeScaningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
