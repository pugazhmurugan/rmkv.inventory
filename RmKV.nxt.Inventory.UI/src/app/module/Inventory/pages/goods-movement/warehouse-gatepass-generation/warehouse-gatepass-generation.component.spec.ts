import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseGatepassGenerationComponent } from './warehouse-gatepass-generation.component';

describe('WarehouseGatepassGenerationComponent', () => {
  let component: WarehouseGatepassGenerationComponent;
  let fixture: ComponentFixture<WarehouseGatepassGenerationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseGatepassGenerationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseGatepassGenerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
