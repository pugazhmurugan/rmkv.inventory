import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { EditMode, Rights } from 'src/app/common/models/common-model';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { GoodsTransferService } from '../../../services/goods-movement/goods-transfers/goods-transfer.service';
import { CounterToGodownService } from '../../../services/goods-movement/internal-transfers/counter-to-godown/counter-to-godown.service';
import { GodownToCounterService } from '../../../services/goods-movement/internal-transfers/godown-to-counter/godown-to-counter.service';
import { WarehouseGatepassService } from '../../../services/goods-movement/warehouse-gatepass/warehouse-gatepass.service';
declare var jsPDF: any;

@Component({
  selector: 'app-warehouse-gatepass-generation',
  templateUrl: './warehouse-gatepass-generation.component.html',
  styleUrls: ['./warehouse-gatepass-generation.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class WarehouseGatepassGenerationComponent implements OnInit {
  componentVisibility: boolean = true;
  displayedColumns = ["Serial_No", "GatePass_No", "GatePass_Date", "Transfer_Type", "From_Location", "To_Location", "Pieces", "No_Of_Bales", "Action"];

  objLoad = {
    Section: this._localStorage.getCompanySectionName(),
    From_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    To_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
  }
  objWarehouseGatepass = {
    Type: '',
    GoodsTransfer_Type: '',
    Section: this._localStorage.getCompanySectionName(),
    GatePass_No: '',
    GatePass_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    To_Location: '',
    To_Warehouse: '',
    Transfer_Details: [],
    To: '',
    DC_No: '',
    No_Of_Bales: '',
    Remarks: ''
  }
  unChangedWarehouseGatepass = {
    Type: '',
    GoodsTransfer_Type: '',
    Section: this._localStorage.getCompanySectionName(),
    GatePass_No: '',
    GatePass_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    To_Location: '',
    To_Warehouse: '',
    Transfer_Details: [],
    To: '',
    DC_No: '',
    No_Of_Bales: '',
    Remarks: ''
  }
  objModifyWarehouseGatepass = {
    Type: '',
    GoodsTransfer_Type: '',
    Section: this._localStorage.getCompanySectionName(),
    GatePass_No: '',
    GatePass_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    To_Location: '',
    To_Warehouse: '',
    Transfer_Details: [],
    To: '',
    DC_No: '',
    No_Of_Bales: '',
    Remarks: ''
  }
  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };
  rights: Rights = {
    Add: true,
    Update: true,
    Delete: true,
    Approve: true,
    View: true
  };
  dataSource: any = new MatTableDataSource([]);
  locationList: any;
  public dateValidation: DateValidation = new DateValidation();

  model1: any = new Date();
  model2: any = new Date();
  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  showTable: boolean = false;
  typeList: any;
  goodsTransferTypeList: any;
  transferList: any;
  toWarehouseList: any;
  totaldata: any = [];

  @ViewChildren("check") check: ElementRef | any;
  GatePass: any;
  constructor(public _minMaxDate: MinMaxDate, public _router: Router, private dbService: NgxIndexedDBService,
    public _localStorage: LocalStorage, private _datePipe: DatePipe, public _CommonService: CommonService,
    public _confirmationDialogComponent: ConfirmationDialogComponent, public _matDialog: MatDialog,
    public _warehouseGatepass: WarehouseGatepassService, public _GoodsTransfer: GoodsTransferService,
    public _decimalPipe: DecimalPipe, public _keyPressEvents: KeyPressEvents, public _excelService: ExcelService) {
    this.model1.setDate(this.model2.getDate() - 2);
    this.objLoad.From_Date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
  }

  ngOnInit() {
    this.getWarehouseGatePassType();
    this.getRights();
  }
  private getRights(): void {
    var rights = this._localStorage.getMenuRights("/WHGatepassGeneration");
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "Delete" && rights[x].Status == true) {
        this.rights.Delete = true;
      }
      if (rights[x].Right_Name == "Approve" && rights[x].Status == true) {
        this.rights.Approve = true;
      }
      if (rights[x].Right_Name == "View" && rights[x].Status == true) {
        this.rights.View = true;
      }
    }
  }
  public getGoodsTransferType(): void {
    if (this.objAction.isEditing == false && this.objAction.isView == false) {
      this.objWarehouseGatepass.GoodsTransfer_Type = '';
    }
    if (this.objWarehouseGatepass.Type == 'T') {
      let objData = {
        GoodsTransfer: JSON.stringify([{
          virtual: this._localStorage.getVirtual()
        }]),
      }
      this._GoodsTransfer.getGoodsTransferType(objData).subscribe((result: any) => {
        if (result) {
          this.goodsTransferTypeList = result;
          if (this.objAction.isEditing == false && this.objAction.isView == false)
            result.length == 1 ? this.objWarehouseGatepass.GoodsTransfer_Type = this.goodsTransferTypeList[0].goods_transfer_flag : '';
        }
      });
    }
  }


  validateFromDate() {
    let date = this.dateValidation.validateDate(this.objLoad.From_Date, this.model1, this.minDate, this.maxDate);
    this.objLoad.From_Date = date[0];
    this.model1 = date[1];
  }
  validateToDate() {
    let date = this.dateValidation.validateDate(this.objLoad.To_Date, this.model2, this._datePipe.transform(this.model1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoad.To_Date = date[0];
    this.model2 = date[1];
  }

  newClick(): void {
    this.objAction.isEditing = false;
    this.objAction.isView = false;
    this.componentVisibility = !this.componentVisibility;
    this.resetScreen();
  }


  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objWarehouseGatepass) !== ((this.objAction.isEditing) ? JSON.stringify(this.objModifyWarehouseGatepass) : JSON.stringify(this.unChangedWarehouseGatepass)))
      return true;
    else
      return false;
  }
  public onClear(exitFlag: boolean): void {
    if (!this.objAction.isView) {
      if (this.checkAnyChangesMade())
        this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
      else if (exitFlag)
        this.onListClick();
    } else
      this.onListClick();
  }
  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Warehouse GatePass Generation";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }
  private resetScreen(): void {
    this.objAction = Object.assign({}, this.unChangedAction);
    this.objWarehouseGatepass = Object.assign({}, this.unChangedWarehouseGatepass);
    this.objModifyWarehouseGatepass = Object.assign({}, this.unChangedWarehouseGatepass);
    this.totaldata = [];
  }
  public onListClick(): void {
    this.componentVisibility = !this.componentVisibility;
  }
  private beforeSaveValidate(): boolean {
    if (!this.objWarehouseGatepass.Type.toString().trim()) {
      document.getElementById('type').focus();
      this._confirmationDialogComponent.openAlertDialog("Select type", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.Type = "";
      return false;
    } else if (!this.objWarehouseGatepass.Section.toString().trim()) {
      document.getElementById('section').focus();
      this._confirmationDialogComponent.openAlertDialog("Select section", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.Section = "";
      return false;
    } else if (!this.objWarehouseGatepass.To_Location.toString().trim()) {
      document.getElementById('toLocation').focus();
      this._confirmationDialogComponent.openAlertDialog("Select to location", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.To_Location = '';
      return false;
    } else if (!this.objWarehouseGatepass.To_Warehouse.toString().trim()) {
      document.getElementById('toWarehouse').focus();
      this._confirmationDialogComponent.openAlertDialog("Select to warehouse", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.To_Warehouse = "";
      return false;
    } else
      return true;
  }
  private getWarehouseGatePassType(): void {
    let objData = {
      WarehouseGatePass: JSON.stringify([{
        virtual: this._localStorage.getVirtual()
      }]),
    }
    this._warehouseGatepass.getWarehouseGatePassType(objData).subscribe((result: any) => {
      if (result) {
        this.typeList = result;
        if (this.objAction.isEditing == false || this.objAction.isView == false)
          result.length == 1 ? this.objWarehouseGatepass.Type = this.typeList[0].transfer_flag : '';
      }
    });
  }
  public getGatepassTowarehouse(): void {
    if (this.objAction.isEditing == false && this.objAction.isView == false) {
      this.toWarehouseList = [];
      this.objWarehouseGatepass.To_Warehouse = '';
    }
    let objData = {
      WarehouseGatePass: JSON.stringify([{
        transfer_flag: this.objWarehouseGatepass.Type,
        goods_transfer_flag: this.objWarehouseGatepass.GoodsTransfer_Type,
        sales_location_id: this.objWarehouseGatepass.To_Location,
        current_warehouse_id : +this._localStorage.getGlobalWarehouseId()
      }]),
    }
    this._warehouseGatepass.getGatepassTowarehouse(objData).subscribe((result: any) => {
      if (result) {
        this.toWarehouseList = result;
        if (this.objAction.isEditing == false || this.objAction.isView == false)
          result.length == 1 ? this.objWarehouseGatepass.To_Warehouse = this.toWarehouseList[0].warehouse_id : '';
        if (this.objWarehouseGatepass.To_Warehouse !== '' && this.objAction.isEditing == false && this.objAction.isView == false)
          this.getTransferDetails();
      }
    });
  }
  public getGatepassToLocation(): void {
    if (this.objAction.isEditing == false && this.objAction.isView == false) {
      this.locationList = [];
      this.toWarehouseList = [];
      this.objWarehouseGatepass.To_Location = '';
      this.objWarehouseGatepass.To_Warehouse = '';
    }
    let objData = {
      WarehouseGatePass: JSON.stringify([{
        transfer_flag: this.objWarehouseGatepass.Type,
        goods_transfer_flag: this.objWarehouseGatepass.GoodsTransfer_Type,
        sales_location_id: this._localStorage.getGlobalSalesLocationId()
      }]),
    }
    this._warehouseGatepass.getGatepassToLocation(objData).subscribe((result: any) => {
      if (result) {
        this.locationList = result;
        if (this.objAction.isEditing == false && this.objAction.isView == false) {
          result.length == 1 ? this.objWarehouseGatepass.To_Location = this.locationList[0].sales_location_id : '';
        }
        if (this.objWarehouseGatepass.To_Location !== '')
          this.getGatepassTowarehouse();
      }
    });
  }
  public getGatepassNo(): void {
    let objData = {
      WarehouseGatePass: JSON.stringify([{
        transfer_type: this.objWarehouseGatepass.Type,
        wh_section_id: +this._localStorage.getWhSectionId(),
        sales_location_id: +this._localStorage.getGlobalSalesLocationId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._warehouseGatepass.getGatepassNo(objData).subscribe((result: any) => {
      if (result) {
        this.objWarehouseGatepass.GatePass_No = result[0].gatepass_no;
        this.GatePass = result[0].gatepass;
      }
    });
  }


  addWarehouseGatepass() {
    if (this.beforeSaveValidate()) {
      let tempTranferData = [];
      for (let i = 0; i < this.objWarehouseGatepass.Transfer_Details.length; i++) {
        if (this.objWarehouseGatepass.Transfer_Details[i].checkFlag == true) {
          tempTranferData.push({
            transfer_id: this.objWarehouseGatepass.Transfer_Details[i].transfer_id,
            transfer_pcs: this.objWarehouseGatepass.Transfer_Details[i].transfer_pcs,
            transfer_value: this.objWarehouseGatepass.Transfer_Details[i].sale_value,
            transfer_date: this._datePipe.transform(this.objWarehouseGatepass.Transfer_Details[i].transfer_date, 'dd/MM/yyyy'),
            acknowledged: null,
            acknowledged_by: null,
            acknowledged_on: null,
          })
        }
      }
      if (this.objWarehouseGatepass.Transfer_Details.length !== 0 && tempTranferData.length == 0) {
        let inputEls = this.check.toArray();
        inputEls[0].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog("Select any one row ", "Warehouse GatePass Generation");
        return;
      } else if (tempTranferData.length == 0) {
        this._confirmationDialogComponent.openAlertDialog("No record to save", "Warehouse GatePass Generation");
        return;
      } else if (!this.objWarehouseGatepass.No_Of_Bales.toString().trim()) {
        document.getElementById('noofbales').focus();
        this._confirmationDialogComponent.openAlertDialog("Enter no of bales ", "Warehouse GatePass Generation");
        this.objWarehouseGatepass.No_Of_Bales = "";
        return;
      }
      // else if (tempTranferData.length !== +this.objWarehouseGatepass.No_Of_Bales) {
      //   document.getElementById('noofbales').focus();
      //   this._confirmationDialogComponent.openAlertDialog("No of bales can't be less than transfer", "Warehouse GatePass Generation");
      //   return;
      // }
      let tempBaleData = [];
      for (let i = 0; i < +this.objWarehouseGatepass.No_Of_Bales; i++) {
        tempBaleData.push({
          bale_no: i + 1,
          acknowledged: null,
          acknowledged_by: null,
          acknowledged_on: null
        })
      }
      let objData = {
        WarehouseGatePass: JSON.stringify([{
          wh_section_id: +this._localStorage.getWhSectionId(),
          transfer_type: this.objWarehouseGatepass.Type,
          gatepass_no: this.objAction.isEditing ? +this.GatePass : 0,
          goods_transfer_type: this.objWarehouseGatepass.GoodsTransfer_Type,
          destination_id: this.objWarehouseGatepass.To_Warehouse,
          destination_name: this.objWarehouseGatepass.To,
          no_of_bags: +this.objWarehouseGatepass.No_Of_Bales,
          no_of_pcs: +this.objWarehouseGatepass.No_Of_Bales,
          gatepass_title: this.objWarehouseGatepass.GatePass_No,
          dc_no: this.objWarehouseGatepass.DC_No,
          remarks: this.objWarehouseGatepass.Remarks.toString().trim(),
          entered_by: this._localStorage.intGlobalUserId(),
          gatepass_transfer_details: tempTranferData,
          gatepass_bales_details: tempBaleData,
          company_section_id: this._localStorage.getCompanySectionId(),
          gatepass_date: this.objWarehouseGatepass.GatePass_Date
        }]),
      }
      this._warehouseGatepass.addWarehouseGatePass(objData).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New warehouse gatePass record has been created", 'Warehouse GatePass Generation');
          this.componentVisibility = !this.componentVisibility;
          this.resetScreen();
          this.getWarehouseGatePass();
        }
      });
    }
  }

  private beforeloadValidate(): boolean {
    if (!this.objLoad.From_Date.toString().trim()) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter from date", "Warehouse GatePass Generation");
      return false;
    } else if (this.objLoad.From_Date.length != 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid from date", "Warehouse GatePass Generation");
      return false;
    } else if (!this.objLoad.To_Date.toString().trim()) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter to date", "Warehouse GatePass Generation");
      return false;
    } else if (this.objLoad.To_Date.length != 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid to date", "Warehouse GatePass Generation");
      return false;
    } else if (this.model2 < this.model1) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("To date can't be less than from date", "Warehouse GatePass Generation");
      return false;
    } else
      return true;
  }
  public matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }
  getWarehouseGatePass(): void {
    if (this.beforeloadValidate()) {
      this.matTableConfig([]);
      let objData = {
        WarehouseGatePass: JSON.stringify([{
          from_date: this.objLoad.From_Date,
          to_date: this.objLoad.To_Date,
          wh_section_id: +this._localStorage.getWhSectionId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          virtual: false
        }]),
      }
      this._warehouseGatepass.getWarehouseGatePass(objData).subscribe((result: any) => {
        if (result) {
          this.matTableConfig(result);
        } else
          this._confirmationDialogComponent.openAlertDialog('No records found', 'Warehouse GatePass Generation');
      });
    }
  }
  getTransferDetails(): void {
    if (this.beforeLoadTowarehouse()) {
      this.objWarehouseGatepass.Transfer_Details = [];
      let objData = {
        WarehouseGatePass: JSON.stringify([{
          transfer_flag: this.objWarehouseGatepass.Type,
          goods_transfer_flag: this.objWarehouseGatepass.GoodsTransfer_Type,
          wh_section_id: +this._localStorage.getWhSectionId(),
          to_destination_id: +this.objWarehouseGatepass.To_Warehouse,
          company_section_id: +this._localStorage.getCompanySectionId(),
        }]),
      }
      this._warehouseGatepass.getTransferDetails(objData).subscribe((result: any) => {
        if (result) {
          debugger
          for (let i = 0; i < result.length; i++) {
            this.objWarehouseGatepass.Transfer_Details.push({
              checkFlag: false,
              sale_value: result[i].sale_value,
              transfer_date: result[i].transfer_date,
              transfer_id: result[i].transfer_id,
              transfer_pcs: result[i].transfer_pcs
            })
          }
          // this.objWarehouseGatepass.Transfer_Details = result;
        } else
          this._confirmationDialogComponent.openAlertDialog('No records found', 'Warehouse GatePass Generation');
      });
    }
  }
  public openDeleteConfirmationDialog(data: any): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = 'Do you want cancel this record ?';
    dialogRef.componentInstance.componentName = 'Warehouse GatePass Generation';
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.delete(data);
      dialogRef = null;
    });
  }

  public delete(data: any) {
    let objData = {
      WarehouseGatePass: JSON.stringify([{
        wh_section_id: +this._localStorage.getWhSectionId(),
        gatepass_no: data.gatepass_no,
        company_section_id: +this._localStorage.getCompanySectionId(),
        cancel: false,
        entered_by: +this._localStorage.intGlobalUserId(),
        cancelled_reason: ''
      }]),
    }
    this._warehouseGatepass.cancelWarehouseGatePass(objData).subscribe((result: any) => {
      if (result) {
        this._confirmationDialogComponent.openAlertDialog("Cancel successfully", "Warehouse GatePass Generation");
        this.getWarehouseGatePass();
      }
    });
  }

  fetchWarehouseGatepass(data: any): void {
    let objData = {
      WarehouseGatePass: JSON.stringify([{
        gatepass_no: data.gatepass_no,
        wh_section_id: this._localStorage.getWhSectionId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._warehouseGatepass.fetchWarehouseGatePass(objData).subscribe((result: any) => {
      if (result) {
        this.objAction.isEditing = true;
        this.objAction.isView = false;
        let masterData = JSON.parse(result.records1);
        let transferData = JSON.parse(result.records2);
        let balesdata = JSON.parse(result.records3);
        if (masterData !== null) {
          this.objWarehouseGatepass.Type = masterData[0].transfer_flag;
          this.getGatepassToLocation();
          this.objWarehouseGatepass.GoodsTransfer_Type = masterData[0].goods_transfer_flag !== null ? masterData[0].goods_transfer_flag : '';
          this.getGoodsTransferType();
          this.objWarehouseGatepass.Section = masterData[0].company_section_name;
          this.objWarehouseGatepass.GatePass_No = masterData[0].gatepass_title;
          this.objWarehouseGatepass.GatePass_Date = this._datePipe.transform(masterData[0].gatepass_date, 'dd/MM/yyyy');
          this.objWarehouseGatepass.To_Location = masterData[0].sales_location_id;
          this.getGatepassTowarehouse();
          this.objWarehouseGatepass.To_Warehouse = masterData[0].destination_id;
          this.objWarehouseGatepass.To = masterData[0].destination_name;
          this.objWarehouseGatepass.DC_No = masterData[0].dc_no;
          this.objWarehouseGatepass.No_Of_Bales = balesdata.length;
          this.objWarehouseGatepass.Remarks = masterData[0].remarks;
          this.GatePass = masterData[0].gatepass_no;
          this.componentVisibility = !this.componentVisibility;
          if (transferData != null) {
            this.objWarehouseGatepass.Transfer_Details = [];
            this.totaldata = [];
            for (let i = 0; i < transferData.length; i++) {
              this.objWarehouseGatepass.Transfer_Details.push({
                checkFlag: true,
                sale_value: transferData[i].transfer_value,
                transfer_date: transferData[i].transfer_date,
                transfer_id: transferData[i].transfer_id,
                transfer_pcs: transferData[i].transfer_pcs
              })
              this.totaldata.push({
                checkFlag: true,
                sale_value: transferData[i].transfer_value,
                transfer_date: transferData[i].transfer_date,
                transfer_id: transferData[i].transfer_id,
                transfer_pcs: transferData[i].transfer_pcs,
                transfer_value: transferData[i].transfer_value,
              })
            }
          }
        }
        this.objModifyWarehouseGatepass = JSON.parse(JSON.stringify(this.objWarehouseGatepass));
      }
    });
  }

  viewWarehouseGatepass(data: any): void {
    let objData = {
      WarehouseGatePass: JSON.stringify([{
        gatepass_no: data.gatepass_no,
        wh_section_id: this._localStorage.getWhSectionId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._warehouseGatepass.fetchWarehouseGatePass(objData).subscribe((result: any) => {
      if (result) {
        this.objAction.isEditing = false;
        this.objAction.isView = true;
        let masterData = JSON.parse(result.records1);
        let transferData = JSON.parse(result.records2);
        let balesdata = JSON.parse(result.records3);
        if (masterData !== null) {
          this.objWarehouseGatepass.Type = masterData[0].transfer_flag;
          this.getGatepassToLocation();
          this.objWarehouseGatepass.GoodsTransfer_Type = masterData[0].goods_transfer_flag !== null ? masterData[0].goods_transfer_flag : '';
          this.getGoodsTransferType();
          this.objWarehouseGatepass.Section = masterData[0].company_section_name;
          this.objWarehouseGatepass.GatePass_No = masterData[0].gatepass_title;
          this.objWarehouseGatepass.GatePass_Date = this._datePipe.transform(masterData[0].gatepass_date, 'dd/MM/yyyy');
          this.objWarehouseGatepass.To_Location = masterData[0].sales_location_id;
          this.getGatepassTowarehouse();
          this.objWarehouseGatepass.To_Warehouse = masterData[0].destination_id;
          this.objWarehouseGatepass.To = masterData[0].destination_name;
          this.objWarehouseGatepass.DC_No = masterData[0].dc_no;
          this.objWarehouseGatepass.No_Of_Bales = balesdata.length;
          this.objWarehouseGatepass.Remarks = masterData[0].remarks;
          this.componentVisibility = !this.componentVisibility;
          if (transferData != null) {
            this.objWarehouseGatepass.Transfer_Details = [];
            this.totaldata = [];
            for (let i = 0; i < transferData.length; i++) {
              this.objWarehouseGatepass.Transfer_Details.push({
                checkFlag: true,
                sale_value: transferData[i].transfer_value,
                transfer_date: transferData[i].transfer_date,
                transfer_id: transferData[i].transfer_id,
                transfer_pcs: transferData[i].transfer_pcs
              })
              this.totaldata.push({
                checkFlag: true,
                sale_value: transferData[i].transfer_value,
                transfer_date: transferData[i].transfer_date,
                transfer_id: transferData[i].transfer_id,
                transfer_pcs: transferData[i].transfer_pcs,
                transfer_value: transferData[i].transfer_value,
              })
            }
          }
        }
        this.objModifyWarehouseGatepass = JSON.parse(JSON.stringify(this.objWarehouseGatepass));
      }
    });
  }

  beforeSelectToLocation() {
    if (!this.objWarehouseGatepass.Type.toString().trim()) {
      document.getElementById('type').focus();
      this._confirmationDialogComponent.openAlertDialog("Select type", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.Type = "";
      return false;
    } else if (this.objWarehouseGatepass.Type == 'T' && !this.objWarehouseGatepass.GoodsTransfer_Type.toString().trim()) {
      document.getElementById('goodsTransferType').focus();
      this._confirmationDialogComponent.openAlertDialog("Select goods transfer type", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.GoodsTransfer_Type = '';
      return false;
    } else
      return true;
  }
  beforeSelectTowarehouse() {
    if (!this.objWarehouseGatepass.Type.toString().trim()) {
      document.getElementById('type').focus();
      this._confirmationDialogComponent.openAlertDialog("Select type", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.Type = "";
      return false;
    } else if (this.objWarehouseGatepass.Type == 'T' && !this.objWarehouseGatepass.GoodsTransfer_Type.toString().trim()) {
      document.getElementById('goodsTransferType').focus();
      this._confirmationDialogComponent.openAlertDialog("Select goods transfer type", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.GoodsTransfer_Type = '';
      return false;
    } else if (!this.objWarehouseGatepass.To_Location.toString().trim()) {
      document.getElementById('toLocation').focus();
      this._confirmationDialogComponent.openAlertDialog("Select to location", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.To_Location = "";
      return false;
    } else
      return true;
  }
  beforeLoadTowarehouse() {
    if (!this.objWarehouseGatepass.Type.toString().trim()) {
      document.getElementById('type').focus();
      this._confirmationDialogComponent.openAlertDialog("Select type", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.Type = "";
      return false;
    } else if (this.objWarehouseGatepass.Type == 'T' && !this.objWarehouseGatepass.GoodsTransfer_Type.toString().trim()) {
      document.getElementById('goodsTransferType').focus();
      this._confirmationDialogComponent.openAlertDialog("Select goods transfer type", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.GoodsTransfer_Type = '';
      return false;
    } else if (!this.objWarehouseGatepass.To_Location.toString().trim()) {
      document.getElementById('toLocation').focus();
      this._confirmationDialogComponent.openAlertDialog("Select to location", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.To_Location = "";
      return false;
    } else if (!this.objWarehouseGatepass.To_Warehouse.toString().trim()) {
      document.getElementById('toWarehouse').focus();
      this._confirmationDialogComponent.openAlertDialog("Select to warehouse", "Warehouse GatePass Generation");
      this.objWarehouseGatepass.To_Warehouse = "";
      return false;
    } else
      return true;
  }

  onAddCheckValue(i) {
    //for (let i = 0; i < this.objWarehouseGatepass.Transfer_Details.length; i++) {
    debugger
    if (this.objWarehouseGatepass.Transfer_Details[i].checkFlag == true) {
      this.totaldata.push({
        transfer_id: this.objWarehouseGatepass.Transfer_Details[i].transfer_id,
        transfer_pcs: this.objWarehouseGatepass.Transfer_Details[i].transfer_pcs,
        transfer_value: this.objWarehouseGatepass.Transfer_Details[i].sale_value,
        acknowledged: null,
        acknowledged_by: null,
        acknowledged_on: null,
      })
    } else if (this.objWarehouseGatepass.Transfer_Details[i].checkFlag == false) {
      let id = this.objWarehouseGatepass.Transfer_Details[i].transfer_id;
      for (let i = 0; i < this.totaldata.length; i++)
        if (this.totaldata[i].transfer_id == id) {
          this.totaldata.splice(i, 1);
        }
    }
    // }
  }
  public getPieces(): number {
    if (this.dataSource.data !== undefined)
      return this.dataSource.data.map(t => +t.no_of_pcs).reduce((acc, value) => acc + value, 0);
  }

  public getNoOfBales(): any {
    if (this.dataSource.data !== undefined) {
      return this.dataSource.data.map(t => +t.no_of_bags).reduce((acc, value) => acc + value, 0);
    }
  }
  public getTransferPcs(): number {
    if (this.totaldata.length !== 0)
      return this.totaldata.map(t => +t.transfer_pcs).reduce((acc, value) => acc + value, 0);
  }

  public getSaleValue(): any {
    let price = '0.00'
    if (this.totaldata.length !== 0) {
      price = this.totaldata.map(t => +t.transfer_value).reduce((acc, value) => acc + value, 0);
      return this._decimalPipe.transform(price, '1.2-2');
    }
  }

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'GatePass No': this.dataSource.data[i].gatepass_no,
          'GatePass Date': this.dataSource.data[i].gatepass_date,
          'Transfer Type': this.dataSource.data[i].transfer_name,
          "From Section": this.dataSource.data[i].company_section_name,
          "To Location": this.dataSource.data[i].destination_wh_name,
          "Pieces": this.dataSource.data[i].no_of_pcs,
          "No of Bales": this.dataSource.data[i].no_of_bags,
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Warehouse_GatePass_Generation",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Warehouse GatePass Generation");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.gatepass_title);
        tempObj.push(this._datePipe.transform(e.gatepass_date, 'dd/MM/yyyy'));
        tempObj.push(e.transfer_name);
        tempObj.push(e.company_section_name);
        tempObj.push(e.destination_wh_name);
        tempObj.push(e.no_of_pcs);
        tempObj.push(e.no_of_bags);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['GatePass No', 'GatePass Date', 'Transfer Type', 'From Section', 'To Location', 'Pieces', 'No Of Bales']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Warehouse_GatePass_Generation ' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Warehouse GatePass Generation");
  }


}


