import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BynoSelectWorkorderInvoiceComponent } from './byno-select-workorder-invoice.component';

describe('BynoSelectWorkorderInvoiceComponent', () => {
  let component: BynoSelectWorkorderInvoiceComponent;
  let fixture: ComponentFixture<BynoSelectWorkorderInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BynoSelectWorkorderInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BynoSelectWorkorderInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
