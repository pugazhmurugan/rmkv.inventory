import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkOrderReceiptComponent } from './work-order-receipt.component';

describe('WorkOrderReceiptComponent', () => {
  let component: WorkOrderReceiptComponent;
  let fixture: ComponentFixture<WorkOrderReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkOrderReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkOrderReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
