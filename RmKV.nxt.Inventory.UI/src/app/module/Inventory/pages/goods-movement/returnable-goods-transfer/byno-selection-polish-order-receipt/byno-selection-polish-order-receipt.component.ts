import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { PolishReceiptGridList } from 'src/app/module/Inventory/model/goodsMovement/returnableGoodsTransfer/polish-order-receipt';
import { PolishOrderReceiptService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer/polish-order-receipt/polish-order-receipt.service';
import { BynoSelectionComponent } from '../byno-selection/byno-selection.component';

@Component({
  selector: 'app-byno-selection-polish-order-receipt',
  templateUrl: './byno-selection-polish-order-receipt.component.html',
  styleUrls: ['./byno-selection-polish-order-receipt.component.scss']
})
export class BynoSelectionPolishOrderReceiptComponent implements OnInit {

  polishReceiptDetailsList: PolishReceiptGridList[] = [
  ]
  polishOrderByNoList: PolishReceiptGridList[] = [];
  selectedParticipant: any = [{
    issue_no: '',
    byno: '',
    description: '',
    stone_type: '',
    stone_qty: '',
  }];

  BynoSelectList: any = {
    issue_no: '',
    byno: '',
    description: '',
    stone_type: '',
    stone_qty: '',
    checked: false,
  };

  polish_issue_details: PolishReceiptGridList[] = []
  transferTypeList: PolishReceiptGridList[] = []
  selectedList: PolishReceiptGridList[] = [];
  selectedRowIndex: number = 0;

  constructor(
    public _router: Router, 
    private _polishReceiptService: PolishOrderReceiptService,
    public _dialogRef: MatDialogRef<BynoSelectionComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    private _keyPressEvents: KeyPressEvents,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _localStorage: LocalStorage
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getByNoList();
  }

  private getByNoList(): void {
    debugger;
    let WorkOrder = {
      PolishOrderReceipt: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        transfer_type: this._data.data.transfer_type,
        wh_section_id: +this._data.data.wh_section_id,
        issue_no: this._data.data.issue_no,
        issue_date : this._data.issue_date
      }])
    }
    this._polishReceiptService.getPendingPolishIssueDetails(WorkOrder).subscribe((result: any[]) => {
      if (result) {
        debugger;
        this.polishOrderByNoList = JSON.parse(JSON.stringify(result));
        console.log(JSON.parse(JSON.stringify(result)),'bynoList')
        this.polishReceiptDetailsList.forEach(x => {
          x.checked = false;
        });
        console.log(this._data, "_data");
        // console.log(this.polishOrderByNoList, "workOrderByNoList");
        // console.log(this._data.workOrderIssueDetailsList, "WorkOrderIssueDetailsList");

        if (this._data.isEditing == true) {
          debugger;
          this._data.editPolishOrderIssueDetails.forEach(x => {
            x.checked = false;
          });
          for (var i = 0; i < this._data.editPolishOrderIssueDetails.length; i++) {
            if (this._data.editPolishOrderIssueDetails[i].wh_section_id == this._data.data.wh_section_id &&
              this._data.editPolishOrderIssueDetails[i].transfer_type == this._data.data.transfer_type &&
              this._data.editPolishOrderIssueDetails[i].issue_no == this._data.data.issue_no) {
              this.polishOrderByNoList.push(this._data.editPolishOrderIssueDetails[i]);
              console.log(this._data.editPolishOrderIssueDetails, "work_order_wh_section_id")
            }
          }
        }

        for (var i = 0; i < this._data.polishOrderIssueDetails.length; i++) {
          for (var j = 0; j < this.polishOrderByNoList.length; j++) {
            if (this._data.polishOrderIssueDetails[i].wh_section_id == this.polishOrderByNoList[j].wh_section_id
              && this._data.polishOrderIssueDetails[i].transfer_type == this.polishOrderByNoList[j].transfer_type
              && this._data.polishOrderIssueDetails[i].issue_no == this.polishOrderByNoList[j].issue_no
              && this._data.polishOrderIssueDetails[i].serial_no == this.polishOrderByNoList[j].serial_no) {
              this.polishOrderByNoList[j].checked = true;
            }
          }
        }

      }
      else
        this._confirmationDialogComponent.openAlertDialog('No records found', 'Byno Selection');
    });
  }

  public determineEnterKey(event: KeyboardEvent | any,): void {
    debugger;
    if (event.keyCode === 13) {
      this.selectedParticipant = this.polishOrderByNoList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    debugger;
    for (let i = 0; i < this.polishOrderByNoList.length; i++) {
      if (this.polishOrderByNoList[i].checked == true) {
        this.selectedList.push(this.polishOrderByNoList[i]);
      }
      else if (this.polishOrderByNoList[i].checked == false)
        this._dialogRef.disableClose = false;
      this._dialogRef.close(this.selectedList);
    }
  }

  public selectedRowIndexList(data: any, i: number): void {
    debugger;
    this.polishOrderByNoList[i].checked = data.checked ? true : false;
  }

  public checkAll(data: any) {
    this.polishReceiptDetailsList = [];
    if (data.checked == true) {
      debugger;
      for (var i = 0; i < this.polishReceiptDetailsList.length; i++) {
        this.polishReceiptDetailsList[i].checked = true;
        this.isAllSelected(this.polishReceiptDetailsList[i]);
      }
      data.checked = true;
    }
    else if (data.checked == false) {
      for (var i = 0; i < this.polishReceiptDetailsList.length; i++) {
        this.polishReceiptDetailsList[i].checked = false;
        this.isAllSelected(this.polishReceiptDetailsList[i]);
      }
      data.checked = false;
    }
  }

  public isAllSelected(data) {
    debugger;
    console.log(data.checked, 'event');

    if (data.checked == true) {
      for (var i = 0; i < data.polish_issue_details.length; i++) {
        this.polishReceiptDetailsList.push(data.polish_issue_details[i]);
      }
      console.log(this.polishReceiptDetailsList)
    }
    else if (data.checked == false) {
      let data_array = [];
      for (var i = 0; i < this.polishReceiptDetailsList.length; i++) {
        if (+data.issue_no !== +this.polishReceiptDetailsList[i].issue_no) {
          data_array.push(this.polishReceiptDetailsList[i]);
        }
      }
      console.log(data_array, "data_array");
      this.polishReceiptDetailsList = [];
      this.polishReceiptDetailsList = data_array;
    }
    data.checked = this.polishReceiptDetailsList.every(function (item: any) {
      return item.checked == true;
    })
  }

  public onClear(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

  // public dialogOK(): void {
  //   this._dialogRef.disableClose = false;
  //   this._dialogRef.close();
  // }
}


