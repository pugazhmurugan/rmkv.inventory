import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BynoSelectionPolishOrderReceiptComponent } from './byno-selection-polish-order-receipt.component';

describe('BynoSelectionPolishOrderReceiptComponent', () => {
  let component: BynoSelectionPolishOrderReceiptComponent;
  let fixture: ComponentFixture<BynoSelectionPolishOrderReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BynoSelectionPolishOrderReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BynoSelectionPolishOrderReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
