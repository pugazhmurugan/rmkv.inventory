import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { EditMode } from 'src/app/common/models/common-model';
import { AccountsLookupService } from 'src/app/common/services/accounts-lookup/accounts-lookup.service';
import { CommonService } from 'src/app/common/services/common/common.service';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { SaveWorksOrderIssue, WorksOrderLoad } from 'src/app/module/Inventory/model/goodsMovement/returnableGoodsTransfer/work-order-issue.model';
import { CounterToGodownService } from 'src/app/module/Inventory/services/goods-movement/internal-transfers/counter-to-godown/counter-to-godown.service';
import { GodownToCounterService } from 'src/app/module/Inventory/services/goods-movement/internal-transfers/godown-to-counter/godown-to-counter.service';
import { WorkOrderIssueService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer/customer-work-order/work-order-issue/work-order-issue.service';
import { BynoSelectionWorkOrderIssueComponent } from '../byno-selection-work-order-issue/byno-selection-work-order-issue.component';
import { BynoSelectionComponent } from '../byno-selection/byno-selection.component';
declare var jsPDF: any;

@Component({
  selector: 'app-work-order-issue',
  templateUrl: './work-order-issue.component.html',
  styleUrls: ['./work-order-issue.component.scss'],
  providers: [DatePipe]
})
export class WorkOrderIssueComponent implements OnInit {
  componentVisibility: boolean = true;
  displayedColumns = ["Serial_No", "wo_issue_id", "wo_issue_date", "account_name", "quantity", "Action"];
  masterSelected: boolean;
  load: any = {
    fromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    toDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    supplier_code: "",
    supplier_name: "",
    transfer_type: "",
  }

  objWorkOrderIssue: SaveWorksOrderIssue = {
    wo_issue_date: new Date(),
    supplier_code: "",
    supplier_name: "",
    transfer_type: "",
    remarks: "",
    wo_issue_id: 0,
  }

  objModifyWorkOrderIssue: SaveWorksOrderIssue = {
    wo_issue_date: new Date(),
    supplier_code: "",
    supplier_name: "",
    transfer_type: "",
    remarks: "",
    wo_issue_id: 0,
  }

  UnchangedWorkOrderIssue: SaveWorksOrderIssue = {
    wo_issue_date: new Date(),
    supplier_code: "",
    supplier_name: "",
    transfer_type: "",
    remarks: "",
    wo_issue_id: 0,
  }

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  workOrderIssueDetailsList: any = []
  selectedWorkorderList: any = [];
  dataSource: any = new MatTableDataSource([]);
  workOrderNoList: any = [];
  WorkOrderIssueEntryGridList: any[] = [];
  newSupplierLookupList: any = [];
  newSupplierLookupLists: any = [];
  loadSupplierLookupList: any = [];
  focusFlag: boolean = false;


  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  showTable: boolean = false;
  @ViewChildren("check") check: ElementRef | any;
  SelectApprovalList: any;
  workOrderList: any[] = [];
  editWorkOrderIssueDetailsList: any = [];
  edit_data: any;

  constructor
  (
    public _minMaxDate: MinMaxDate,
    public _router: Router,
    public _dialog: MatDialog,
    private _confirmationDialog: ConfirmationDialogComponent,
    public _localStorage: LocalStorage,
    private _datePipe: DatePipe,
    public _CommonService: CommonService,
    private _accountsLookupService: AccountsLookupService,
    private _workOrderIssueService: WorkOrderIssueService,
    public _godowntocounter: GodownToCounterService,
    public _godownstocounter: CounterToGodownService,
    private _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _excelService: ExcelService,
  ) 
  {
    this.load.fromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 2)));
    this.getSupplierLookupList();
    this.getSupplierLookupLists();
    // this.getWorkOrderIssueEntryGridList();
  }

  ngOnInit() {
    this.getTransferTypeList()
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.fromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.fromDate = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.toDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.toDate = date[0];
    this.toDate1 = date[1];
  }


  public newClick(): void {
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
    this.objAction.isEditing = false;
  }

  private resetScreen(): void {
    this.objAction = Object.assign({}, this.unChangedAction);
    this.objWorkOrderIssue = Object.assign({}, this.UnchangedWorkOrderIssue);
    this.objModifyWorkOrderIssue = Object.assign({}, this.UnchangedWorkOrderIssue);
    this.workOrderNoList = [];
    this.workOrderIssueDetailsList = [];
    this.masterSelected = false;
  }

  public clearIsBynoList(): void {
    this.workOrderNoList = [];
    this.workOrderIssueDetailsList = [];
  }

  /************************************************ CRUD OPERATION ************************************************/
  public getWorkOrderIssueList(): void {
    debugger;
    if (this.beforeLoadValidate()) {
      let WorkOrderLoad = {
        WorkOrderIssue: JSON.stringify([{
          from_date: this.load.fromDate,
          to_date: this.load.toDate,
          supplier_code: this.load.supplier_code,
          company_section_id: +this._localStorage.getCompanySectionId(),
          transfer_type: this.load.transfer_type,
          wh_section_id: this._localStorage.getWhSectionId(),
        }])
      }
      this._workOrderIssueService.getWorkOrderIssueList(WorkOrderLoad).subscribe((result: any[]) => {
        this.workOrderList = result;
        this.workOrderList != null ? this.matTableConfig(this.workOrderList) : this._confirmationDialogComponent.openAlertDialog("No record found", "work Order Issue List");

      });
    }
  }

  public matTableConfig(tableRecords: any[]): void {
    this.dataSource = new MatTableDataSource(tableRecords);
  }

  public getQuantity(): number {
    if (this.dataSource.data !== undefined)
      return this.dataSource.data.map(t => +t.quantity).reduce((acc, value) => acc + value, 0);
  }

  public getWorkOrderIssueEntryGridList(value: number): void {
    if (this.beforeLoadSaveValidate()) {
      debugger;
      let WorkOrder = {
        WorkOrderIssue: JSON.stringify([{
          destination_id: this._localStorage.getWhSectionId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          transfer_type: this.objWorkOrderIssue.transfer_type,
        }])
      }
      this._workOrderIssueService.getWorkOrderIssueEntryGridList(WorkOrder).subscribe((result: any[]) => {
        if (result) {
          debugger;
          if (value == 0) {
            this.workOrderIssueDetailsList = [];
          }
          this.workOrderNoList = JSON.parse(JSON.stringify(result));
          this.workOrderNoList.forEach(ele => {
            ele.isSelected = false;
          });
          console.log(result, "WorkOrderIssueEntryGridList")
        }
        else
          this._confirmationDialog.openAlertDialog('No records found', 'Work Order Issue');
      });
    }
  }

  public getTransferTypeList(): void {
    this._workOrderIssueService.getTransferTypeList().subscribe((result: any[]) => {
      if (result) {
        this.WorkOrderIssueEntryGridList = result;
        this.objWorkOrderIssue.transfer_type = this.WorkOrderIssueEntryGridList[0].transfer_type;
        result.length == 1 ? this.objWorkOrderIssue.transfer_type = result[0].transfer_flag : '';
      }
      // else {
      //   this._confirmationDialog.openAlertDialog('No records found', 'Work Order Issue');
      // }
    });
  }

  public addWorkOrderIssue(): void {
    if (this.beforeSaveValidate()) {
      debugger
      let objDetails = [];
      for (let i = 0; i < this.workOrderIssueDetailsList.length; i++) {
        objDetails.push(({
          serial_no: i + 1,
          byno: this.workOrderIssueDetailsList[i].byno,
          work_order_wh_section_id: this.workOrderIssueDetailsList[i].wh_section_id,
          work_order_transfer_type: this.workOrderIssueDetailsList[i].transfer_type,
          byno_prod_serial: this.workOrderIssueDetailsList[i].byno_prod_serial,
          work_order_no: this.workOrderIssueDetailsList[i].work_order_no,
          work_order_serial_no: this.workOrderIssueDetailsList[i].serial_no,
        }));
      }
      let objData = {
        WorkOrderIssue: JSON.stringify([{
          wh_section_id: this._localStorage.getWhSectionId(),
          transfer_type: this.objWorkOrderIssue.transfer_type,
          wo_issue_id: this.objAction.isEditing ? this.objWorkOrderIssue.wo_issue_id : 0,
          wo_issue_date: this._datePipe.transform(this.objWorkOrderIssue.wo_issue_date, 'dd/MM/yyyy'),
          remarks: this.objWorkOrderIssue.remarks,
          supplier_code: this.objWorkOrderIssue.supplier_code,
          entered_by: +this._localStorage.intGlobalUserId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          details: objDetails,
        }]),
      }
      console.log(objDetails, 'objDetails');
      this._workOrderIssueService.saveWorkOrderissue(objData).subscribe((result: any[]) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New Work Order Issue record has been created", "Work Order Issue");

          this.componentVisibility = !this.componentVisibility;
          this.getWorkOrderIssueList();
        }
      });
    }
  }


  public modifyWorkOrderIssue(isEditing: boolean, index: number): void {
    debugger;
    this.objAction = {
      isEditing: isEditing ? true : false
    }

    let objFetch = {
      WorkOrderIssue: JSON.stringify([{
        wh_section_id: this._localStorage.getWhSectionId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        transfer_type: this.load.transfer_type,
        wo_issue_id: this.dataSource.data[index].wo_issue_id,
      }])
    }
    this._workOrderIssueService.fetchWorkOrderissue(objFetch).subscribe((result: any) => {
      if (result) {
        debugger;
        this.objWorkOrderIssue = JSON.parse(result.records1)[0];
        this.objModifyWorkOrderIssue = JSON.parse(result.records1)[0];
        this.getWorkOrderIssueEntryGridList(1);
        this.workOrderIssueDetailsList = JSON.parse(result.records2);
        this.editWorkOrderIssueDetailsList = JSON.parse(result.records2);
        // console.log(this.workOrderIssueDetailsList, "fetchIssue")

        // console.log(result, "fetch")
        // console.log(this.objWorkOrderIssue.supplier_code, "code")
        // console.log(this.objModifyWorkOrderIssue.supplier_code, "code")
        this.componentVisibility = !this.componentVisibility;
      }
    })
  }


  public checkAll() {
    this.workOrderIssueDetailsList = [];
    if (this.masterSelected == true) {
      debugger;
      for (var i = 0; i < this.workOrderNoList.length; i++) {
        this.workOrderNoList[i].isSelected = true;
        this.isAllSelected(this.workOrderNoList[i]);
      }
      this.masterSelected = true;
    }
    else if (this.masterSelected == false) {
      for (var i = 0; i < this.workOrderNoList.length; i++) {
        this.workOrderNoList[i].isSelected = false;
        this.isAllSelected(this.workOrderNoList[i]);
      }
      this.masterSelected = false;
    }
  }

  private getCheckedItemList() {
    debugger;
    let tempWorkorderIssueList = [];
    for (var i = 0; i < this.workOrderNoList.length; i++) {
      if (this.workOrderNoList[i].isSelected)
        tempWorkorderIssueList.push(this.workOrderNoList[i]);
    }
    this.selectedWorkorderList = tempWorkorderIssueList;
    this.isAllSelected(i)
  }

  // public isAllSelected() {
  //   debugger;
  //   this.masterSelected = this.workOrderNoList.every(function (item: any) {
  //     return item.isSelected == true;
  //   })
  //   this.getCheckedItemList();
  // }

  public isAllSelected(data) {
    debugger;
    console.log(data.isSelected, 'event');

    if (data.isSelected == true) {
      for (var i = 0; i < data.work_order_details.length; i++) {
        this.workOrderIssueDetailsList.push(data.work_order_details[i]);
      }
      console.log(this.workOrderIssueDetailsList)
    }
    else if (data.isSelected == false) {
      let data_array = [];
      for (var i = 0; i < this.workOrderIssueDetailsList.length; i++) {
        if (+data.work_order_no !== +this.workOrderIssueDetailsList[i].work_order_no) {
          data_array.push(this.workOrderIssueDetailsList[i]);
        }
      }
      // console.log(data_array, "data_array")
      this.workOrderIssueDetailsList = [];
      this.workOrderIssueDetailsList = data_array;
    }
    this.masterSelected = this.workOrderNoList.every(function (item: any) {
      return item.isSelected == true;
    })
  }

  /************************************************ Validations ************************************************/
  public onClear(exitFlag: boolean): void {

    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objWorkOrderIssue) !== ((this.objAction.isEditing) ? JSON.stringify(this.objModifyWorkOrderIssue)
      : JSON.stringify(this.UnchangedWorkOrderIssue)))
      return true;
    else
      return false;
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Work Order Issue";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  public onListClick(): void {
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  public validateTransferType(): boolean {
    if ([null, undefined, 0, ""].indexOf(this.objWorkOrderIssue.transfer_type) !== -1 && !this.objWorkOrderIssue.transfer_type.toString().trim()) {
      document.getElementById('target').focus();
      this._confirmationDialogComponent.openAlertDialog("Select Transfer Type", "Work Order Issue");
      return false;
    } else return true;
  }

  private beforeSaveValidate(): boolean {
    debugger;
    if ([null, undefined, 0, ""].indexOf(this.objWorkOrderIssue.supplier_code) !== -1 || !this.objWorkOrderIssue.supplier_code.toString().trim()) {
      document.getElementById('supplierCode').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Supplier Code", "Work Order Issue");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objWorkOrderIssue.transfer_type) !== -1 || !this.objWorkOrderIssue.transfer_type.toString().trim()) {
      document.getElementById('target').focus();
      this._confirmationDialogComponent.openAlertDialog("Select Transfer Type", "Work Order Issue");
      return false;
    } else if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(+this.workOrderNoList) !== -1) {
      this._confirmationDialogComponent.openAlertDialog("Load Transfer type List", "Work Order Issue");
      return false;
    } else if ([null, 'null', undefined, 'undefined', NaN, 0, '0', ''].indexOf(+this.workOrderIssueDetailsList) !== -1) {
      this._confirmationDialogComponent.openAlertDialog("Select any one Byno ", "Work Order Issue");
      return;
    } else {
      return true;
    }
  }

  public beforeLoadSaveValidate(): boolean {
    if ([null, undefined, 0, ""].indexOf(this.objWorkOrderIssue.supplier_code) !== -1 || !this.objWorkOrderIssue.supplier_code.toString().trim()) {
      document.getElementById('supplierCode').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Supplier Code", "Work Order Issue");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objWorkOrderIssue.transfer_type) !== -1 || !this.objWorkOrderIssue.transfer_type.toString().trim()) {
      document.getElementById('target').focus();
      this._confirmationDialogComponent.openAlertDialog("Select Transfer Type", "Work Order Issue");
      return;
    } else {
      return true;
    }
  }

  private beforeLoadValidate(): boolean {
    debugger;
    if ([null, undefined, 0, ""].indexOf(this.load.fromDate) !== -1) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Select from date", "Work Order Issue");
      return false;
    } else if (this.load.fromDate.length != 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Invalid from date", "Work Order Issue");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.load.toDate) !== -1) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Select todate", "Work Order Issue");
      return false;
    } else if (this.load.toDate.length != 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Invalid todate", "Work Order Issue");
      return false;
    } if ([null, undefined, 0, ""].indexOf(this.load.supplier_code) !== -1 || !this.load.supplier_code.toString().trim()) {
      document.getElementById('supplierCodeName').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Supplier Code", "Work Order Issue");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.load.transfer_type) !== -1 || !this.load.transfer_type.toString().trim()) {
      document.getElementById('Transfer').focus();
      this._confirmationDialogComponent.openAlertDialog("Select Transfer Type", "Work Order Issue");
      return false;
    }
    else {
      return true;
    }
  }

  /************************************************ Lookup Functionalities ************************************************/
  private getSupplierLookupList(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._accountsLookupService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      debugger;
      if (result) {
        this.newSupplierLookupList = JSON.parse(result);
        this.loadSupplierLookupList = JSON.parse(result);
        this.newSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
        this.loadSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
      }
    });
  }

  private getSupplierLookupLists(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._accountsLookupService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      debugger;
      if (result) {
        this.newSupplierLookupLists = JSON.parse(result);
        this.loadSupplierLookupList = JSON.parse(result);
        this.newSupplierLookupLists.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
        this.loadSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
      }
    });
  }


  public openNewSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierName()) {
      this.openNewSupplierLookupDialog();
    }
  }

  private openNewSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "650px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.load.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.load.supplier_code = result.account_code.toString().trim();
        this.load.supplier_name = result.account_name.toString().trim();

      } else {
        this.load.supplier_code = '';
        this.load.supplier_name = '';
      }
    });
  }

  public validateSupplierName(): boolean {
    let index = this.newSupplierLookupLists.findIndex(element => element.account_code.toString().trim() === this.load.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }





  public checkValidSupplierCode(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.load.supplier_code.toString().trim() !== '' && this.load.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier code", "WorkOrder Issue", focus);
    }
  }


  public onEnterNewSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      email: "",
      address1: "",
      address2: "",
      address3: "",
      gstn_no: ""
    };

    supplierLookupList = this.newSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.load.supplier_code.toString().trim().toLowerCase());
    this.load.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.load.supplier_code.toString().trim();
    this.load.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';

  }

  // Entry Table Lookup

  public openNewSupplierLookups(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierCode()) {
      this.openNewSupplierLookupDialogs();
    }
  }

  private openNewSupplierLookupDialogs(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "650px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objWorkOrderIssue.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objWorkOrderIssue.supplier_code = result.account_code.toString().trim();
        this.objWorkOrderIssue.supplier_name = result.account_name.toString().trim();

      } else {
        this.objWorkOrderIssue.supplier_code = '';
        this.objWorkOrderIssue.supplier_name = '';
      }
    });
  }

  public validateSupplierCode(): boolean {
    let index = this.newSupplierLookupLists.findIndex(element => element.account_code === this.objWorkOrderIssue.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public checkValidSupplierName(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objWorkOrderIssue.supplier_code.toString().trim() !== '' && this.objWorkOrderIssue.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier code", "WorkOrder Issue", focus);
    }
  }

  public onEnterNewSupplierLookupDetail(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      email: "",
      address1: "",
      address2: "",
      address3: "",
      gstn_no: ""
    };

    supplierLookupList = this.newSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objWorkOrderIssue.supplier_code.toString().trim().toLowerCase());
    this.objWorkOrderIssue.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objWorkOrderIssue.supplier_code.toString().trim();
    this.objWorkOrderIssue.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';

  }

  public openBynoDialog(data): void {
    this.edit_data = data;
    let dialogRef = this._dialog.open(BynoSelectionWorkOrderIssueComponent, {
      width: "975px",
      data: {
        data: data,
        workOrderIssueDetailsList: this.workOrderIssueDetailsList,
        isEditing: this.objAction.isEditing,
        editWorkOrderIssueDetailsList: this.editWorkOrderIssueDetailsList
      },
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      debugger;
      if (result) {
        let data_array = [];
        console.log(result, 'value');

        var array = [];
        var oldArray = [];
        oldArray = this.workOrderIssueDetailsList;
        if (result.length > 0) {
          for (var i = 0; i < oldArray.length; i++) {
            if (oldArray[i].wh_section_id == result[0].wh_section_id && oldArray[i].transfer_type == result[0].transfer_type &&
              oldArray[i].work_order_no != result[0].work_order_no) {
              array.push(oldArray[i]);
            }
          }
          this.workOrderIssueDetailsList = JSON.parse(JSON.stringify(array));
        }
        else {
          var array = [];
          var oldArray = [];
          console.log(this.edit_data, 'this.edit_data')
          oldArray = this.workOrderIssueDetailsList;
          for (var i = 0; i < oldArray.length; i++) {
            if (
              oldArray[i].wh_section_id == this.edit_data.wh_section_id &&
              oldArray[i].transfer_type == this.edit_data.transfer_type &&
              oldArray[i].work_order_no != this.edit_data.work_order_no) {
              array.push(oldArray[i]);
            }
          }
          this.workOrderIssueDetailsList = JSON.parse(JSON.stringify(array));
        }
        for (let i = 0; i < result.length; i++) {
          this.workOrderIssueDetailsList.push(result[i]);
          // if (this.checkIsBynoExists(result[i].work_order_no, result[i].byno))
          //   this._confirmationDialog.openAlertDialog('Byno ' + result[i].byno + ' already exists', '');
          // else
          //   if (result[i].checked == true) {
          //     //alert(1)
          //     this.workOrderIssueDetailsList.push(result[i]);
          //   } else (result[i].checked == false)
          // // alert(2)
        }

      }
    });
  }

  private checkIsBynoExists(workOrderNo: number, byno: string): boolean {
    let i = this.workOrderIssueDetailsList.findIndex(x => +x.work_order_no === +workOrderNo && x.byno.toString().trim() === byno.toString().trim())
    return i !== -1 ? true : false;
  }

  openAlertDialog(value: string, componentName?: string, focus?: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        document.getElementById(focus).focus();
        _dialogRef = null;
      }
      _dialogRef = null;
    });
  }


  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Issue No": this.dataSource.data[i].wo_issue_id,
          "Issue Date": this._datePipe.transform(this.dataSource.data[i].wo_issue_date, 'dd/MM/yyyy'),
          "Supplier Name": this.dataSource.data[i].account_name,
          "Quantity": this.dataSource.data[i].quantity,
        });
      }
      this._excelService.exportAsExcelFile(json, "WorkOrder Issue", datetime);
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found", "WorkOrder Issue");
  }

  exportToPdf(): void {

    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.wo_issue_id);
        tempObj.push(this._datePipe.transform(e.wo_issue_date, 'dd/MM/yyyy'));
        tempObj.push(e.account_name);
        tempObj.push(e.quantity);
        prepare.push(tempObj);
      });

      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Issue No", "Issue Date", "Supplier Name", "Quantity",]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('WorkOrder Issue' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "WorkOrder Issue");
  }


}
