import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { data } from 'jquery';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { WorkOrderIssueService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer/customer-work-order/work-order-issue/work-order-issue.service';
import { BynoSelectionComponent } from '../byno-selection/byno-selection.component';

@Component({
  selector: 'app-byno-select-workorder-invoice',
  templateUrl: './byno-select-workorder-invoice.component.html',
  styleUrls: ['./byno-select-workorder-invoice.component.scss']
})
export class BynoSelectWorkorderInvoiceComponent implements OnInit {
  workOrderIssueDetailsList: any = [];
  workOrderByNoList: any = [];
  work_order_details: any = [];
  transferTypeList: any = [];
  masterSelected: boolean;
  objWorkOrderIssue: any = {
    transfer_type: ""
  }
  checkedList: any[];

  selectedList: any = [];
  selectedRowIndex: any;
  selectedParticipant: any;
  constructor(
    public _router: Router, private _workOrderIssueService: WorkOrderIssueService,
    public _dialogRef: MatDialogRef<BynoSelectionComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog, private _confirmationDialog: ConfirmationDialogComponent,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _localStorage: LocalStorage
  ) {
    this.masterSelected = false;
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {  
    this.getTransferTypeList()
    this.byNoList()
    //   console.log(this._data,'pop') 
    //  // this.getTransferTypeList() 
    //   this.getWorkOrderReceiptPendindIsssueDetails();  
    // }
    //   private getWorkOrderReceiptPendindIsssueDetails(): void {
    //     let WorkOrder = {
    //       WorkOrderIssue: JSON.stringify([{
    //         company_section_id:  +this._localStorage.getCompanySectionId(),
    //         wh_section_id: this._data.wh_section_id,
    //         transfer_type:this._data.transfer_type,
    //         wo_issue_id: this._data.wo_issue_id     
    //       }])
    //     }

    //     this._workOrderIssueService.getWorkOrderReceiptDetailsList(WorkOrder).subscribe((result: any[]) => {
    //       if (result) {
    //         debugger;
    //         console.log(result, "WorkOrderIssueEntryGridList")
    //         this.work_order_details = JSON.parse(JSON.stringify(result));
    //       }
    //       else
    //       this._confirmationDialog.openAlertDialog('No records found', 'Work Order Receipt Details'); 
    //     });
    //   }
    //   public onClear(): void {
    //     this._dialogRef.disableClose = false;
    //     this._dialogRef.close(null);
    //   }
    //   public onSelect(): void {
    //     debugger;
    //     this._dialogRef.disableClose = false;    
    //     this.masterSelected = this.work_order_details.every(function (item: any) {
    //       return item.isSelected == true;     
    //     })
    //     this.getCheckedItemList();    
    //     this._dialogRef.close(this.work_order_details);   
    //   }


    //   private getCheckedItemList(): void {
    //     debugger;
    //     this.checkedList = [];
    //     for (var i = 0; i < this.work_order_details.length; i++) {
    //       if (this.work_order_details[i].isSelected)
    //         this.checkedList.push(this.work_order_details[i]);
    //     }    
    //     this.work_order_details = this.checkedList;   
    //   }
    //   private getTransferTypeList(): void {
    //     this.work_order_details = this._data.data;
    //     // this.work_order_details.forEach(x => {
    //     //   x.isSelected = false;
    //     // });

    //   }

    //  public selectedRowIndexList(data: any, i: number): void {
    //     debugger;
    //     this.work_order_details[i].isSelected = data.isSelected ? true : false;
    //   }

    //   public dialogOK(): void {
    //     debugger;
    //     for (let i = 0; i < this.work_order_details.length; i++) {
    //       if (this.work_order_details[i].isSelected == true)
    //         this.selectedList.push(this.work_order_details[i]);
    //     }
    //     this._dialogRef.disableClose = false;
    //     this._dialogRef.close(this.selectedList);
    //   }
  }

  public onClear(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

  public getTransferTypeList(): void {
    debugger;
    // this.workOrderByNoList = this._data.data;
    // this.workOrderIssueDetailsList.forEach(x => {
    //   x.checked = false;
    // });
  }

  private byNoList(): void {
    debugger
    let WorkOrder = {
      WorkOrderIssue: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        wh_section_id: +this._data.data.work_issue_details[0].wh_section_id,
        transfer_type: this._data.data.work_issue_details[0].transfer_type,
        wo_issue_id: this._data.data.work_issue_details[0].wo_issue_id,
      }])
    }
    this._workOrderIssueService.getWorkOrderReceiptDetailsList(WorkOrder).subscribe((result: any[]) => {
      if (result) {
        debugger;
        this.workOrderByNoList = JSON.parse(JSON.stringify(result));
        this.workOrderIssueDetailsList.forEach(x => {
          x.checked = false;
        });
        console.log(this._data, "_data");
        console.log(this.workOrderByNoList, "workOrderByNoList");
        console.log(this._data.workOrderIssueDetailsList, "WorkOrderIssueDetailsList");

        if (this._data.isEditing == true) {
          debugger;
          this._data.editWorkOrderIssueDetailsList.forEach(x => {
            x.checked = false;
          });
          for (var i = 0; i < this._data.editWorkOrderIssueDetailsList.length; i++) {
            if (this._data.editWorkOrderIssueDetailsList[i].work_order_wh_section_id == this._data.data.wh_section_id &&
              this._data.editWorkOrderIssueDetailsList[i].work_order_transfer_type == this._data.data.transfer_type &&
              this._data.editWorkOrderIssueDetailsList[i].wo_issue_id == this._data.data.wo_issue_id) {
              this.workOrderByNoList.push(this._data.editWorkOrderIssueDetailsList[i]);
              console.log(this._data.editWorkOrderIssueDetailsList, "work_order_wh_section_id")
            }
          }
        }

        for (var i = 0; i < this._data.workOrderIssueDetailsList.length; i++) {
          for (var j = 0; j < this.workOrderByNoList.length; j++) {
            if (this._data.workOrderIssueDetailsList[i].wh_section_id == this.workOrderByNoList[j].wh_section_id
              && this._data.workOrderIssueDetailsList[i].transfer_type == this.workOrderByNoList[j].transfer_type
              && this._data.workOrderIssueDetailsList[i].wo_issue_id == this.workOrderByNoList[j].wo_issue_id
              && this._data.workOrderIssueDetailsList[i].serial_no == this.workOrderByNoList[j].serial_no) {
              this.workOrderByNoList[j].checked = true;
            }
          }
        }

      }
      else
        this._confirmationDialogComponent.openAlertDialog('No records found', 'Byno Selection');
    });
  }

  //  private checkIsBynoExists(workOrderNo: number, byno: string): boolean {
  //     let i = this.workOrderByNoList.findIndex(x => +x.work_order_no === +workOrderNo && x.byno.toString().trim() === byno.toString().trim())
  //     return i !== -1 ? true : false;
  //   }


  public determineEnterKey(event: KeyboardEvent | any,): void {
    debugger;
    if (event.keyCode === 13) {
      this.selectedParticipant = this.workOrderByNoList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    debugger;
    for (let i = 0; i < this.workOrderByNoList.length; i++) {
      if (this.workOrderByNoList[i].checked == true) {
        this.selectedList.push(this.workOrderByNoList[i]);
      }
      else if (this.workOrderByNoList[i].checked == false)
        //  {
        //     let data_array = [];
        //       if (this._data.work_order_details[i].work_order_no !== +this.workOrderIssueDetailsList[i].work_order_no ) {
        //         data_array.push(this.workOrderIssueDetailsList[i]);
        //       }

        //     console.log(data_array, "data_array")
        //     this.workOrderIssueDetailsList = [];
        //     this.workOrderIssueDetailsList = data_array;
        //   }
        this._dialogRef.disableClose = false;
      this._dialogRef.close(this.selectedList);
    }
  }

  public selectedRowIndexList(data: any, i: number): void {
    debugger;
    this.workOrderByNoList[i].checked = data.checked ? true : false;
  }

  // public checkAll() {
  //   for (let i = 0; i < this.workOrderIssueDetailsList.length; i++) {
  //     if (this.BynoSelectList.checked == true) {
  //       this.workOrderIssueDetailsList[i].checked = false;
  //     } else {
  //       this.workOrderIssueDetailsList[i].checked = true;
  //     }
  //   }
  // }
  public checkAll(data) {
    this.workOrderIssueDetailsList = [];
    if (data.checked == true) {
      debugger;
      for (var i = 0; i < this.workOrderIssueDetailsList.length; i++) {
        this.workOrderIssueDetailsList[i].checked = true;
        this.isAllSelected(this.workOrderIssueDetailsList[i]);
      }
      data.checked = true;
    }
    else if (data.checked == false) {
      for (var i = 0; i < this.workOrderIssueDetailsList.length; i++) {
        this.workOrderIssueDetailsList[i].checked = false;
        this.isAllSelected(this.workOrderIssueDetailsList[i]);
      }
      data.checked = false;
    }
  }

  public isAllSelected(data) {
    debugger;
    console.log(data.checked, 'event');

    if (data.checked == true) {
      for (var i = 0; i < data.work_order_details.length; i++) {
        this.workOrderIssueDetailsList.push(data.work_order_details[i]);
      }
      console.log(this.workOrderIssueDetailsList)
    }
    else if (data.checked == false) {
      let data_array = [];
      for (var i = 0; i < this.workOrderIssueDetailsList.length; i++) {
        if (+data.work_order_no !== +this.workOrderIssueDetailsList[i].work_order_no) {
          data_array.push(this.workOrderIssueDetailsList[i]);
        }
      }
      console.log(data_array, "data_array")
      this.workOrderIssueDetailsList = [];
      this.workOrderIssueDetailsList = data_array;
    }
    data.checked = this.workOrderIssueDetailsList.every(function (item: any) {
      return item.checked == true;
    })
  }

  // @HostListener("window:keyup", ["$event"])
  // keyEvent(event: KeyboardEvent | any) {
  //   if (event.key === "ArrowDown" || event.key === "ArrowUp") {
  //     if (document.getElementById("row" + this.selectedRowIndex)) {
  //       document.getElementById("row" + this.selectedRowIndex).focus();
  //     }
  //   }
  //   this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.workOrderIssueDetailsList);
  //   if (this.workOrderIssueDetailsList)
  //     this.selectedParticipant = this.workOrderIssueDetailsList[this.selectedRowIndex];
  // }

  // public onDoubleClick(selectedParticipantRecord: any): void {
  //   this.selectedParticipant = selectedParticipantRecord;
  //   this._dialogRef.close(this.selectedParticipant);
  // }

  // public onClick(selectedParticipantRecord: any, event: KeyboardEvent): void {
  //   if (event.keyCode === 13) {
  //     this.selectedParticipant = selectedParticipantRecord;
  //     this._dialogRef.close(this.selectedParticipant);
  //   }

  // }



}
