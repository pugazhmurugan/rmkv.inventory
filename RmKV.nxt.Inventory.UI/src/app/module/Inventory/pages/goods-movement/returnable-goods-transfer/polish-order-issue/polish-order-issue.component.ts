import { DatePipe } from '@angular/common';
import { ElementRef } from '@angular/core';
import { Component, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatDialogRef, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode } from 'src/app/common/models/common-model';
import { AccountsLookupService } from 'src/app/common/services/accounts-lookup/accounts-lookup.service';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ReasonComponent } from 'src/app/common/shared/reason/reason.component';
import { ForCheckBynoData } from 'src/app/module/Inventory/model/common.model';
import { PolishOrderIssueService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer/polish-order-issue/polish-order-issue.service';
import { SingleProductService } from 'src/app/module/Inventory/services/pricing/selling-price-change/single-product/single-product.service';
declare var jsPDF: any;
@Component({
  selector: 'app-polish-order-issue',
  templateUrl: './polish-order-issue.component.html',
  styleUrls: ['./polish-order-issue.component.scss'],
  providers: [DatePipe]
})
export class PolishOrderIssueComponent implements OnInit {

  componentVisibility: boolean = true;
  inputEls: any = [];
  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  Date: any = new Date();
  objLoadPolishOrderIssue: any = {
    fromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    toDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    status: "All",
    supplier_code: "",
    supplier_name: "",
  }

  objPolishOrderIssue: any = {
    issue_no: 0,
    issue_date: new Date(),
    section_Name: this._localStorage.getCompanySectionName(),
    supplier_code: "",
    issue_type: "Stone",
    remarks: "",
    IssueDetails: [
      {
        serial_no: 0,
        byno: '',
        inv_byno: "",
        byno_serial: "",
        byno_prod_serial: "",
        issued_qty: 0,
        stone_type: "",
        stone_qty: 0,
        charges: 0,
      }
    ]
  };

  objModifyPolishOrderIssue: any = {
    issue_no: "",
    issue_date: new Date(),
    section_Name: this._localStorage.getCompanySectionName(),
    supplier_code: "",
    issue_type: "Stone",
    remarks: "",
    IssueDetails: [
      {
        serial_no: 0,
        inv_byno: "",
        byno_serial: "",
        byno_prod_serial: "",
        issued_qty: 0,
        stone_type: "",
        stone_qty: 0,
        charges: 0,
      }
    ]
  }

  UnchangedPolishOrderIssue: any = {
    issue_no: "",
    issue_date: new Date(),
    section_Name: this._localStorage.getCompanySectionName(),
    supplier_code: "",
    issue_type: "Stone",
    remarks: "",
    IssueDetails: [
      {
        serial_no: 0,
        inv_byno: "",
        byno_serial: "",
        byno_prod_serial: "",
        issued_qty: 0,
        stone_type: "",
        stone_qty: 0,
        charges: 0,
      }
    ]
  }

  UnModifyPolishOrderIssue: any = {
    issue_no: "",
    issue_date: new Date(),
    section_Name: this._localStorage.getCompanySectionName(),
    supplier_code: "",
    issue_type: "Stone",
    remarks: "",
    IssueDetails: [
      {
        serial_no: 0,
        inv_byno: "",
        byno_serial: "",
        byno_prod_serial: "",
        issued_qty: 0,
        stone_type: "",
        stone_qty: 0,
        charges: 0,
      }
    ]
  }

  ForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  objBynoWiseChange: any = {
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    issued_qty: 0,
    product_name: '',
    counter_code: '',
    selling_price: 0,
    discount_percent: 0,
    discount_rate: 0,
    valid: false
  }

  Inv_Byno: string = "";
  Byno_Serial: string = "";
  Byno_Prod_Serial: string = "";
  keyUpEvent: any;

  fromDate1: any = new Date();
  toDate1: any = new Date();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  public dateValidation: DateValidation = new DateValidation();

  dataSource: any = new MatTableDataSource([]);
  // dataSource = ELEMENT_DATA;
  displayedColumns = ["Serial_No", "Issue_No", "Issue_Date", "Issue_Type", "Remarks", "Status", "Action"];

  focusFlag: boolean = false;
  newSupplierLookupList: any = [];
  newSupplierLookupLists: any = [];
  objobjLoadPolishOrderIssuePolishOrderIssueSupplierLookupList: any = [];
  polishOrderIssueList: any[] = [];

  receiptColumns: any[] = [
    { display: "byno", editable: true },
    { display: "stoneType", editable: true },
    { display: "stoneQty", editable: true },
    { display: "charges", editable: true },
    { display: "action", editable: true },
  ]

  @ViewChildren('byno') byno: ElementRef | any;
  @ViewChildren('stoneType') stoneType: ElementRef | any;
  @ViewChildren('stoneQty') stoneQty: ElementRef | any;
  @ViewChildren('charges') charges: ElementRef | any;
  @ViewChildren("action") action: ElementRef | any;

  polishorderbynolist: any = [];

  constructor(public _localStorage: LocalStorage,
    private _accountsLookupService: AccountsLookupService,
    public _router: Router,
    private _datePipe: DatePipe,
    public _dialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    private _polishOrderIssueService: PolishOrderIssueService,
    private _singleProductService: SingleProductService,
    public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    private _matDialog: MatDialog,
    private _keyPressEvents: KeyPressEvents,
    public _excelService: ExcelService,
    public _gridKeyEvents: GridKeyEvents,
  ) {
    this.objLoadPolishOrderIssue.fromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 2)));
  }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    debugger
    switch (event.keyCode) {
      case 13: // Enter Key
        let response = this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.receiptColumns, this, this.objPolishOrderIssue.IssueDetails);
        debugger;
        if (response)
          this.addNewRow();
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.receiptColumns, this, this.objPolishOrderIssue.IssueDetails);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.receiptColumns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.receiptColumns, this, this.objPolishOrderIssue.IssueDetails);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.receiptColumns, this, this.objPolishOrderIssue.IssueDetails);
        break;
    }
  }

  ngOnInit() {
    this.getSupplierLookupList();
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.objLoadPolishOrderIssue.fromDate, this.fromDate1, this.minDate, this.maxDate);
    this.objLoadPolishOrderIssue.fromDate = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.objLoadPolishOrderIssue.toDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoadPolishOrderIssue.toDate = date[0];
    this.toDate1 = date[1];
  }

  public newClick(): void {
    this.objAction = JSON.parse(JSON.stringify(this.unChangedAction));
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  private resetScreen(): void {
    if (this.objAction.isEditing) {
      this.objPolishOrderIssue = JSON.parse(JSON.stringify(this.objModifyPolishOrderIssue));
      this.objModifyPolishOrderIssue = JSON.parse(JSON.stringify(this.objModifyPolishOrderIssue));
    } else {
      this.objAction = JSON.parse(JSON.stringify(this.unChangedAction));
      this.objPolishOrderIssue = JSON.parse(JSON.stringify(this.UnchangedPolishOrderIssue));
      this.objModifyPolishOrderIssue = JSON.parse(JSON.stringify(this.UnchangedPolishOrderIssue));
    }
  }

  listClick(): void {
    this.objAction = JSON.parse(JSON.stringify(this.unChangedAction));
    this.componentVisibility = !this.componentVisibility;
  }

  /************************************************ Cured Operations ************************************************/
  public getPolishOderIssue(): void {
    if (this.beforeLoadValidate()) {
      let PolishOrder = {
        PolishOrderIssue: JSON.stringify([{
          from_date: this.objLoadPolishOrderIssue.fromDate,
          to_date: this.objLoadPolishOrderIssue.toDate,
          wh_section_id: this._localStorage.getWhSectionId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          supplier_code: this.objLoadPolishOrderIssue.supplier_code,
          status: this.objLoadPolishOrderIssue.status,
        }])
      }
      this._polishOrderIssueService.getPolishOrderIssueList(PolishOrder).subscribe((result: any[]) => {
        this.polishOrderIssueList = result;
        console.log(this.polishOrderIssueList, "Stone & Polish Issue")
        this.polishOrderIssueList != null ? this.matTableConfig(this.polishOrderIssueList) : this._confirmationDialogComponent.openAlertDialog("No record found", "Stone & Polish Issue List");

      });
    }
  }

  public matTableConfig(tableRecords: any[]): void {
    this.dataSource = new MatTableDataSource(tableRecords);
  }

  public addPolishOrderIssue(): void {
    debugger;
    if(this.beforeSaveValidate()) {
    let IssuesDetails = [];
    for (let i = 0; i < this.objPolishOrderIssue.IssueDetails.length; i++) {
      debugger;

      if (!this.objPolishOrderIssue.IssueDetails[i].inv_byno.toString().trim())
      // !this.objPolishOrderIssue.IssueDetails[i].stone_type.toString().trim() ||
      //   !this.objPolishOrderIssue.IssueDetails[i].Issued_Qty.toString().trim()
      //   || !this.objPolishOrderIssue.IssueDetails[i].charges.toString().trim()
      //  )
         {
        this.objPolishOrderIssue.IssueDetails.splice(i, 1);
        i--;
      } else
      IssuesDetails.push({
        serial_no: i + 1,
        inv_byno: this.objPolishOrderIssue.IssueDetails[i].inv_byno,
        byno_serial: this.objPolishOrderIssue.IssueDetails[i].byno_serial,
        byno_prod_serial: this.objPolishOrderIssue.IssueDetails[i].byno_prod_serial,
        issued_qty: this.objAction.isEditing ? this.objPolishOrderIssue.IssueDetails[i].issued_qty : this.objPolishOrderIssue.IssueDetails[i].issued_qty,
        stone_type: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objPolishOrderIssue.IssueDetails[i].stone_type) === -1 ? this.objPolishOrderIssue.IssueDetails[i].stone_type : 0,
        stone_qty: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objPolishOrderIssue.IssueDetails[i].stone_qty) === -1 ? this.objPolishOrderIssue.IssueDetails[i].stone_qty : 0,
        charges: this.objPolishOrderIssue.IssueDetails[i].charges,
      })
    }

    let polishOrderIssue = {
      PolishOrderIssue: JSON.stringify([{
        issue_no: this.objAction.isEditing ? this.objPolishOrderIssue.issue_no : 0,
        wh_section_id: this._localStorage.getWhSectionId(),
        issue_date: this._datePipe.transform(this.objPolishOrderIssue.issue_date, 'dd/MM/yyyy'),
        supplier_code: this.objPolishOrderIssue.supplier_code,
        remarks: this.objPolishOrderIssue.remarks,
        entered_by: this._localStorage.intGlobalUserId(),
        issue_type: this.objPolishOrderIssue.issue_type,
        issue_details: JSON.parse(JSON.stringify(IssuesDetails)),
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._polishOrderIssueService.addPolishOrderIssue(polishOrderIssue).subscribe((result: any[]) => {
      debugger;
      if (result) {
        this.openUniformAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New Stone & Polish Issue record has been created", "Stone & Polish Issue", false);

        this.getPolishOderIssue();
      }
    });
  }
}

  // public addPolishOrderIssue(): void {
  //   debugger;
  //   let IssuesDetails = [];
  //   for (let i = 0; i < this.objPolishOrderIssue.IssueDetails.length; i++) {
  //     if ([null, undefined, 0, ""].indexOf(this.objPolishOrderIssue.supplier_code) !== -1 || !this.objPolishOrderIssue.supplier_code.toString().trim()) {
  //       document.getElementById('supplierCode').focus();
  //       this._confirmationDialogComponent.openAlertDialog("Enter issue to", "Stone & Polish Issue");
  //       return
  //     } else if ([null, undefined, "", 0, '0'].indexOf(this.objPolishOrderIssue.IssueDetails[i].byno) !== -1 || !this.objPolishOrderIssue.IssueDetails[i].byno.toString().trim()) {
  //       let input = this.byno.toArray();
  //       input[i].nativeElement.focus();
  //       this._confirmationDialogComponent.openAlertDialog("Enter byno", "Stone & Polish Issue");
  //       return
  //     } else if (this.objPolishOrderIssue.issue_type === 'Stone' && [null, undefined, "", 0, '0'].indexOf(this.objPolishOrderIssue.IssueDetails[i].stone_type) !== -1) {
  //       let input = this.stoneType.toArray();
  //       input[i].nativeElement.focus();
  //       this._confirmationDialogComponent.openAlertDialog("Enter stoneType", "Stone & Polish Issue");
  //       return
  //     } else if (this.objPolishOrderIssue.issue_type === 'Stone' && [null, undefined, "", 0, '0'].indexOf(this.objPolishOrderIssue.IssueDetails[i].stone_qty) !== -1) {
  //       let input = this.stoneQty.toArray();
  //       input[i].nativeElement.focus();
  //       this._confirmationDialogComponent.openAlertDialog("Enter stone Qty", "Stone & Polish Issue");
  //       return
  //     } else if ([null, undefined, "", 0, '0'].indexOf(this.objPolishOrderIssue.IssueDetails[i].charges) !== -1 || !this.objPolishOrderIssue.IssueDetails[i].charges.toString().trim()) {
  //       let input = this.charges.toArray();
  //       input[i].nativeElement.focus();
  //       this._confirmationDialogComponent.openAlertDialog("Enter charges", "Stone & Polish Issue");
  //       return
  //     }
  //     let currentlyExist = 0;
  //     debugger;
  //     IssuesDetails.push({
  //       serial_no: i + 1,
  //       inv_byno: this.objPolishOrderIssue.IssueDetails[i].inv_byno,
  //       byno_serial: this.objPolishOrderIssue.IssueDetails[i].byno_serial,
  //       byno_prod_serial: this.objPolishOrderIssue.IssueDetails[i].byno_prod_serial,
  //       issued_qty: this.objAction.isEditing ? this.objPolishOrderIssue.IssueDetails[i].issued_qty : this.objPolishOrderIssue.IssueDetails[i].issued_qty,
  //       stone_type: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objPolishOrderIssue.IssueDetails[i].stone_type) === -1 ? this.objPolishOrderIssue.IssueDetails[i].stone_type : "",
  //       stone_qty: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objPolishOrderIssue.IssueDetails[i].stone_qty) === -1 ? this.objPolishOrderIssue.IssueDetails[i].stone_qty : 0,
  //       charges: this.objPolishOrderIssue.IssueDetails[i].charges,
  //     })
  //   }

  //   let polishOrderIssue = {
  //     PolishOrderIssue: JSON.stringify([{
  //       issue_no: this.objAction.isEditing ? this.objPolishOrderIssue.issue_no : 0,
  //       wh_section_id: this._localStorage.getWhSectionId(),
  //       issue_date: this._datePipe.transform(this.objPolishOrderIssue.issue_date, 'dd/MM/yyyy'),
  //       supplier_code: this.objPolishOrderIssue.supplier_code,
  //       remarks: this.objPolishOrderIssue.remarks,
  //       entered_by: this._localStorage.intGlobalUserId(),
  //       issue_type: this.objPolishOrderIssue.issue_type,
  //       issue_details: JSON.parse(JSON.stringify(IssuesDetails)),
  //       company_section_id: +this._localStorage.getCompanySectionId(),
  //     }])
  //   }
  //   this._polishOrderIssueService.addPolishOrderIssue(polishOrderIssue).subscribe((result: any[]) => {
  //     debugger;
  //     if (result) {
  //       this.openUniformAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New Stone & Polish Issue record has been created", "Stone & Polish Issue", false);

  //       this.getPolishOderIssue();
  //     }
  //   });
  // }

  public modifyPolishOrderIssue(isEditing: boolean, index: number, isView?: boolean): void {
    debugger;
    this.objAction = {
      isEditing: isEditing ? true : false,
      isView: isView ? true : false
    }

    let objFetch = {
      PolishOrderIssue: JSON.stringify([{
        wh_section_id: this._localStorage.getWhSectionId(),
        issue_no: this.dataSource.data[index].issue_no,
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._polishOrderIssueService.fetchPolishOrderIssue(objFetch).subscribe((result: any) => {
      if (result) {
        debugger;
        this.objAction.isEditing = true;
        this.objPolishOrderIssue = JSON.parse(JSON.stringify(result));
        this.objModifyPolishOrderIssue = JSON.parse(JSON.stringify(result));
        this.objPolishOrderIssue = JSON.parse(result.records1)[0];
        this.objModifyPolishOrderIssue = JSON.parse(result.records1)[0];
        this.objPolishOrderIssue.IssueDetails = JSON.parse(result.records2);
        this.objModifyPolishOrderIssue.IssueDetails = JSON.parse(result.records2);
        this.objPolishOrderIssue.IssueDetails.charges = JSON.parse(result.records2)[0].charges;
        this.objModifyPolishOrderIssue.IssueDetails.charges = JSON.parse(result.records2)[0].charges;
        this.objPolishOrderIssue.section_Name = this._localStorage.getCompanySectionName();
        this.objModifyPolishOrderIssue.section_Name = this._localStorage.getCompanySectionName();
        console.log(this.objPolishOrderIssue, "fetch")
        this.componentVisibility = !this.componentVisibility;
      }
    })
  }

  private deletePolishOrderIssueList(row: any, Reason: string): void {
    debugger;
    let tempSaveArr = [];
    let objData = {
      wh_section_id: this._localStorage.getWhSectionId(),
      issue_no: this.dataSource.data[row].issue_no,
      cancel: false,
      entered_by: +this._localStorage.intGlobalUserId(),
      cancelled_reason: Reason,
      company_section_id: +this._localStorage.getCompanySectionId(),
    }
    tempSaveArr.push(objData);
    let objCancel = {
      PolishOrderIssue: JSON.stringify(tempSaveArr)
    }
    this._polishOrderIssueService.deletePolishOrderIssue(objCancel).subscribe(
      (result: any) => {
        if (result.error) {
          this._confirmationDialogComponent.openAlertDialog(result.error, "Stone & Polish Issue");
        } else if (result) {
          this._confirmationDialogComponent.openAlertDialog("Cancelled", "Stone & Polish Issue");
        } else {
          this._confirmationDialogComponent.openAlertDialog(result.error, "Stone & Polish Issue");
        }
        this.getPolishOderIssue();
      });
    this.dialogRef = null;
  }


  private getPolishOrderBynoList(i: number): void {
    debugger;
    let objGet: any = {
      SingleProduct: JSON.stringify([{
        inv_byno: this.ForCheckBynoData.Inv_Byno.toString().trim().toUpperCase(),
        byno_serial: this.ForCheckBynoData.Byno_Serial.toString().trim().toUpperCase(),
        byno_prod_serial: this.ForCheckBynoData.Byno_Prod_Serial.toString().trim().toUpperCase(),
        company_section_id: this._localStorage.getCompanySectionId()
      }])
    }
    this._singleProductService.getSingleProduct(objGet).subscribe((result: any) => {
      debugger;
      if (result) {
        this.objPolishOrderIssue.IssueDetails[i].selling_price = JSON.parse(JSON.stringify(result))[0].selling_price;
        this.objPolishOrderIssue.IssueDetails[i].inv_byno = JSON.parse(JSON.stringify(result))[0].inv_byno;
        this.objPolishOrderIssue.IssueDetails[i].byno_serial = JSON.parse(JSON.stringify(result))[0].byno_serial;
        this.objPolishOrderIssue.IssueDetails[i].byno_prod_serial = JSON.parse(JSON.stringify(result))[0].byno_prod_serial;
        this.objPolishOrderIssue.IssueDetails[i].quantity = JSON.parse(JSON.stringify(result))[0].quantity;
        this.objPolishOrderIssue.IssueDetails[i].image_path = JSON.parse(JSON.stringify(result))[0].image_path;
        this.objPolishOrderIssue.IssueDetails[i].valid = true;
        console.log(this.objPolishOrderIssue, 'By Margin Details List');
        this.checkExistByno(i);

      }
    });
  }

  /**********************************************Byno Validation****************************************************** */

  // public onKeyPress(keyUpEvent: any, i: number) {
  //   debugger;
  //   this.resetRow(i, true);
  //   if (keyUpEvent.keyCode === 13 && this.objPolishOrderIssue.IssueDetails[i].byno === '') {
  //     this.openAlertDialogByno(i, 'Enter byno');
  //     // this._confirmationDialog.openAlertDialog('Enter byno', 'By Amount');
  //   } else if (keyUpEvent.keyCode === 13 && this.objPolishOrderIssue.IssueDetails[i].byno.length == 16 && !this.objPolishOrderIssue.IssueDetails[i].byno.includes("/")) {
  //     this.byNoWithoutSlash(i);
  //   } else if (keyUpEvent.keyCode === 13 && this.objPolishOrderIssue.IssueDetails[i].byno.length == 28 && !this.objPolishOrderIssue.IssueDetails[i].byno.includes("/")) {
  //     this.OldByNoWithoutSlash(i);
  //   } else if (keyUpEvent.keyCode === 13 && this.objPolishOrderIssue.IssueDetails[i].byno.length == 30 && !this.objPolishOrderIssue.IssueDetails[i].byno.includes("/")) {
  //     this.byNoWithoutSlash(i);
  //   } else if (keyUpEvent.keyCode === 13 && this.objPolishOrderIssue.IssueDetails[i].byno.toString().trim().includes("/")) {
  //     debugger
  //     this.splitByNumber(i);
  //   } else if (this.objPolishOrderIssue.IssueDetails[i].byno.length >= 28 && keyUpEvent.keyCode === 13) {
  //     this.autoScannerCall(i);
  //   } else {
  //     if (keyUpEvent.keyCode === 13 && (this.objPolishOrderIssue.IssueDetails[i].byno.toString().trim().includes('/') && this.objPolishOrderIssue.IssueDetails[i].byno.split('/').length !== 3)) {
  //       this.openAlertDialogByno(i, 'Invalid byno');
  //       this.objPolishOrderIssue.IssueDetails[i].byno = '';
  //     } else if (keyUpEvent.keyCode === 13 && !this.objPolishOrderIssue.IssueDetails[i].byno.toString().trim().includes("/") && this.objPolishOrderIssue.IssueDetails[i].byno.length > 16) {
  //       this.openAlertDialogByno(i, 'Invalid byno');
  //     } else if (keyUpEvent.keyCode === 13 && !this.objPolishOrderIssue.IssueDetails[i].byno.toString().trim().includes('/')) {
  //       this.openAlertDialogByno(i, 'Invalid byno');
  //       this.objPolishOrderIssue.IssueDetails[i].byno = '';
  //     }
  //   }
  // }

  public onKeyPress(keyUpEvent: any, i: number) {
    this.focusFlag = keyUpEvent.keyCode == 13 ? true : false;
    if (keyUpEvent.keyCode == 46) {
    } else if (keyUpEvent.keyCode === 13 &&
      this.objPolishOrderIssue.IssueDetails[i].byno.length == 16) {
      this.byNoWithoutSlash(i);
    } else if (keyUpEvent.keyCode === 13 &&
      this.objPolishOrderIssue.IssueDetails[i].byno.length == 28) {
      this.OldByNoWithoutSlash(i);
    } else if (keyUpEvent.keyCode === 13 &&
      this.objPolishOrderIssue.IssueDetails[i].byno.length == 30) {
      this.byNoWithoutSlash(i);
    } else if (keyUpEvent.keyCode === 13 && this.objPolishOrderIssue.IssueDetails[i].byno.includes("/")) {
      this.splitByNumber(i);
    } else if (this.objPolishOrderIssue.IssueDetails[i].byno.length >= 28 && keyUpEvent.keyCode === 13) {
      this.autoScannerCall(i);
    }  else if (keyUpEvent.keyCode == 8) {
      this.resetRow(i);
    } else {
      if (keyUpEvent.keyCode === 13 && !this.objPolishOrderIssue.IssueDetails[i].byno.toString().trim()) {
        this.openAlertDialogByno(i, "Enter Byno");
      } else if (keyUpEvent.keyCode === 13 && !this.objPolishOrderIssue.IssueDetails[i].byno.includes("/") && this.objPolishOrderIssue.IssueDetails[i].byno.length > 16) {
        this._confirmationDialogComponent.openAlertDialog("Invalid Byno", "Byno");
      } else if (keyUpEvent.keyCode === 13) {
        this.objPolishOrderIssue.IssueDetails[i].byno != "" ? this._confirmationDialogComponent.openAlertDialog("Invalid Byno", "Byno") : "";
        this.resetRow(i);
      }
    }
  }

  private checkExistByno(i: number) {
    let already = false;
    let index = 0;
    let byno = '';
    for (let j = 0; (j < this.objPolishOrderIssue.IssueDetails.length); j++) {
      if (j !== i && this.objPolishOrderIssue.IssueDetails[j].byno === this.objPolishOrderIssue.IssueDetails[i].byno) {
        already = true;
        index = j + 1;
        byno = this.objPolishOrderIssue.IssueDetails[i].byno;
        break;
      }
    }
    if (already) {
      // this.inputEls = this.byno.toArray();
      // this.inputEls[i].nativeElement.focus();
      this.resetRow(i);
      this.openAlertDialogByno(i, "This Byno " + " " + byno + " Already Available In Row No" + " " + index + "");
    }
    // else this.addNewRow();
  }

  private resetRow(i: number): void {
    // this.objPolishOrderIssue.IssueDetails[i].byno = '';
    this.objPolishOrderIssue.IssueDetails[i].product_name = '';
    this.objPolishOrderIssue.IssueDetails[i].counter_code = '';
    this.objPolishOrderIssue.IssueDetails[i].selling_price = 0;
    this.objPolishOrderIssue.IssueDetails[i].discount_percent = 0;
    this.objPolishOrderIssue.IssueDetails[i].discount_rate = 0;
    this.objPolishOrderIssue.IssueDetails[i].valid = false;
  }

  public addNewRow(): void {
    this.objPolishOrderIssue.IssueDetails.push(Object.assign({}, this.objBynoWiseChange));
    setTimeout(() => {
      let input = this.byno.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100);
  }

  private autoScannerCall(i: number) {
    debugger;
    this.ForCheckBynoData.Inv_Byno = this.objPolishOrderIssue.IssueDetails[i].byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = this.objPolishOrderIssue.IssueDetails[i].byno.substr(8, 4);
    this.ForCheckBynoData.Byno_Prod_Serial = this.objPolishOrderIssue.IssueDetails[i].byno.substr(11, 4);
    // this.objSingleProductobjLoadPolishOrderIssue.byno = '';
    this.getPolishOrderBynoList(i);
  }

  private splitByNumber(i: number) {
    debugger;
    this.objPolishOrderIssue.IssueDetails[i].byno = this.objPolishOrderIssue.IssueDetails[i].byno.toString().trim().toUpperCase();
    var x = this.objPolishOrderIssue.IssueDetails[i].byno.split("/");
    this.Inv_Byno = x[0];
    this.Byno_Serial = x[1];
    this.Byno_Prod_Serial = [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(x[2]) !== -1 ? "" : x[2];
    if (this.Byno_Serial.length == 2) {
      this.Byno_Serial = "00".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 2) {
      this.Byno_Prod_Serial = "00".concat(this.Byno_Prod_Serial);
    }
    if (this.Byno_Serial.length == 1) {
      this.Byno_Serial = "000".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 1) {
      this.Byno_Prod_Serial = "000".concat(this.Byno_Prod_Serial);
    } if (this.Byno_Serial.length == 0) {
      this.openAlertDialogByno(i, 'Invalid byno');
      return;
    } if (this.Byno_Prod_Serial.length == 0) {
      this.openAlertDialogByno(i, 'Invalid byno');
      return;
    }
    this.ForCheckBynoData.Inv_Byno = this.Inv_Byno.toString().trim();
    this.ForCheckBynoData.Byno_Serial = this.Byno_Serial.toString().trim();
    this.ForCheckBynoData.Byno_Prod_Serial = this.Byno_Prod_Serial.toString().trim();
    this.getPolishOrderBynoList(i);
  }

  private byNoWithoutSlash(i: number) {
    debugger;
    this.objPolishOrderIssue.IssueDetails[i].byno = this.objPolishOrderIssue.IssueDetails[i].byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objPolishOrderIssue.IssueDetails[i].byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objPolishOrderIssue.IssueDetails[i].byno.substr(9, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objPolishOrderIssue.IssueDetails[i].byno.substr(13, 3);
    this.getPolishOrderBynoList(i);
  }

  private OldByNoWithoutSlash(i: number) {
    debugger;
    this.objPolishOrderIssue.IssueDetails[i].byno = this.objPolishOrderIssue.IssueDetails[i].byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objPolishOrderIssue.IssueDetails[i].byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objPolishOrderIssue.IssueDetails[i].byno.substr(8, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objPolishOrderIssue.IssueDetails[i].byno.substr(11, 3);
    // console.log(this.ForCheckBynoData);
    this.getPolishOrderBynoList(i);
  }

  private openAlertDialogByno(i: number, message: string) {
    let _dialogRef = this._dialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = message;
    _dialogRef.componentInstance.componentName = "Byno";
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this.byno.toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  public removeRows(i: number): void {
    this.openConfirmationDialog('Are you sure want to remove this byno ' + this.objPolishOrderIssue.IssueDetails[i].byno + ' from the list', 'Stone & Polish Issue', i);
  }

  public openConfirmationDialog(message: string, componentName: string, i: number): void {
    let dialogRef = this._dialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = componentName;
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deleteRow(i);
      dialogRef = null;
    });
  }

  private deleteRow(i: number) {
    this.objPolishOrderIssue.IssueDetails.splice(i, 1);
    setTimeout(() => {
      let inputEls = this.byno.toArray();
      inputEls[i].nativeElement.focus();
    }, 100);
  }

  public addEmptyRow(i: number): void {
    if (this.beforeAddNewRowValidate(i)) {
      this.objPolishOrderIssue.IssueDetails.push(Object.assign({}, this.objBynoWiseChange));
      setTimeout(() => {
        let input = this.byno.toArray();
        input[input.length - 1].nativeElement.focus();
      }, 100);
    }
  }

  private beforeAddNewRowValidate(i: number): boolean {
    if (this.objPolishOrderIssue.IssueDetails.length > 0 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objPolishOrderIssue.IssueDetails[0].byno.toString().trim()) !== -1) {
      this.openAlertDialogByno(i, 'Enter byno');
      return false;
    } if (this.objPolishOrderIssue.IssueDetails.length > 0 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objPolishOrderIssue.IssueDetails[0].byno.toString().trim()) === -1 &&
      this.objPolishOrderIssue.IssueDetails.length > 0 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.objPolishOrderIssue.IssueDetails[0].selling_price.toString().trim()) !== -1) {
      this.openAlertDialogByno(i, 'Invalid byno');
      return false;
    }
    else if (this.objPolishOrderIssue.issue_type === 'Stone' && [null, undefined, "", 0, '0'].indexOf(this.objPolishOrderIssue.IssueDetails[0].stone_type) !== -1) {
      let input = this.stoneType.toArray();
      input[i].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter stoneType", "Stone & Polish Issue");
      return false
    } else if (this.objPolishOrderIssue.issue_type === 'Stone' && [null, undefined, "", 0, '0'].indexOf(this.objPolishOrderIssue.IssueDetails[0].stone_qty) !== -1) {
      let input = this.stoneQty.toArray();
      input[i].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter stone Qty", "Stone & Polish Issue");
      return false
    } else if ([null, undefined, "", 0, '0'].indexOf(this.objPolishOrderIssue.IssueDetails[0].charges) !== -1 || !this.objPolishOrderIssue.IssueDetails[0].charges.toString().trim()) {
      let input = this.charges.toArray();
      input[i].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter charges", "Stone & Polish Issue");
      return false
    } else return true;
  }
  /************************************************ Validations ************************************************/

  private beforeLoadValidate(): boolean {
    debugger;
    if ([null, undefined, 0, ""].indexOf(this.objLoadPolishOrderIssue.fromDate) !== -1) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Select from date", "Stone & Polish Issue");
      return false;
    } else if (this.objLoadPolishOrderIssue.fromDate.length != 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Invalid from date", "Stone & Polish Issue");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objLoadPolishOrderIssue.toDate) !== -1) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Select todate", "Stone & Polish Issue");
      return false;
    } else if (this.objLoadPolishOrderIssue.toDate.length != 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Invalid todate", "Stone & Polish Issue");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objLoadPolishOrderIssue.status) !== -1 || !this.objLoadPolishOrderIssue.status.toString().trim()) {
      document.getElementById('statusSelection').focus();
      this._confirmationDialogComponent.openAlertDialog("Select status", "Stone & Polish Issue");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objLoadPolishOrderIssue.supplier_code) !== -1 || !this.objLoadPolishOrderIssue.supplier_code.toString().trim()) {
      document.getElementById('supplierCodeName').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Supplier Code", "Stone & Polish Issue");
      return false;
    }
    else {
      return true;
    }
  }

    private beforeSaveValidate(): boolean {
    debugger;
    if ([null, undefined, 0, ""].indexOf(this.objPolishOrderIssue.supplier_code) !== -1 || !this.objPolishOrderIssue.supplier_code.toString().trim()) {
      document.getElementById('supplierCode').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter issue to", "Work Order Issue");
      return false;
    } else if ([null, undefined, "", 0, '0'].indexOf(this.objPolishOrderIssue.IssueDetails[0].byno) !== -1 || !this.objPolishOrderIssue.IssueDetails[0].byno.toString().trim()) {
      // let input = this.byno.toArray();
      // input[0].nativeElement.focus();
      document.getElementById('byno').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter byno", "Work Order Issue");
      return false;
    } else if ([null, undefined, "", 0, '0'].indexOf(this.objPolishOrderIssue.IssueDetails[0].selling_price) !== -1) {
      // let input = this.byno.toArray();
      // input[0].nativeElement.focus();
      document.getElementById('byno').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid byno", "Work Order Issue");
      return false;
    }
     else if (this.objPolishOrderIssue.issue_type === 'Stone' && [null, undefined, "", 0, '0'].indexOf(this.objPolishOrderIssue.IssueDetails[0].stone_type) !== -1) {
      // let input = this.stoneType.toArray();
      // input[0].nativeElement.focus();
      document.getElementById('stoneType').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter stoneType", "Work Order Issue");
      return false;
    } else if (this.objPolishOrderIssue.issue_type === 'Stone' && [null, undefined, "", 0, '0',NaN,'NaN'].indexOf(this.objPolishOrderIssue.IssueDetails[0].stone_qty) !== -1) {
      // let input = this.stoneQty.toArray();
      // input[0].nativeElement.focus();
      document.getElementById('stoneQty').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter stone Qty", "Work Order Issue");
      return false;
    } else if ([null, undefined, "", 0, '0'].indexOf(this.objPolishOrderIssue.IssueDetails[0].charges) !== -1) {
      let input = this.charges.toArray();
      input[0].nativeElement.focus();
      // document.getElementById('charges').focus;
      this._confirmationDialogComponent.openAlertDialog("Enter charges", "Work Order Issue");
      return false;
    } else {
      return true;
    }
  }

  public clearStone(): void {
    for (let i = 0; i < this.objPolishOrderIssue.IssueDetails.length; i++) {
      this.objPolishOrderIssue.IssueDetails[i].stone_type = '';
      this.objPolishOrderIssue.IssueDetails[i].stone_qty = 0;
    }
  }

  private openUniformAlertDialog(message: string, componentName?: string, isRemove?: boolean) {
    let _dialogRef = this._dialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = message;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result && !isRemove)
        this.listClick();
      else
        this.getPolishOderIssue();
      _dialogRef = null;
    });
  }

  public onClickDelete(index: any): any {
    this.dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this.dialogRef.componentInstance.confirmMessage =
      "Do you want to cancel of '" + this.dataSource.data[index].issue_no + "'";
    this.dialogRef.componentInstance.componentName = "Stone & Polish Issue";
    return this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.openReasonDialog(index);
      }
      this.dialogRef = null;
    });
  }

  public openReasonDialog(index: number): void {
    debugger;
    let dialogRef = this._matDialog.open(ReasonComponent, {
      width: "75vw",
      panelClass: "custom-dialog-container",
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== null) {
        this.deletePolishOrderIssueList(index, result);
      }
      this.dialogRef = null;
    });
  }

  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade())
      this.openConfirmationDialogs(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private openConfirmationDialogs(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Stone & Polish Issue";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  public onListClick(): void {
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objPolishOrderIssue) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.objModifyPolishOrderIssue)
      : JSON.stringify(this.UnchangedPolishOrderIssue)))
      return true;
    else
      return false;
  }

  public onlyAllwDecimalForInvoiceDetails(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  /************************************************ Lookup Functionalities ************************************************/
  private getSupplierLookupList(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._accountsLookupService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      debugger;
      if (result) {
        this.newSupplierLookupList = JSON.parse(result);
        this.objobjLoadPolishOrderIssuePolishOrderIssueSupplierLookupList = JSON.parse(result);
        this.newSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
        this.objobjLoadPolishOrderIssuePolishOrderIssueSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
      }
    });
  }

  public openNewSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierName()) {
      this.openNewSupplierLookupDialog();
    }
  }

  private openNewSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._dialog.open(AccountsLookupComponent, {
      width: "650px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objLoadPolishOrderIssue.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objLoadPolishOrderIssue.supplier_code = result.account_code.toString().trim();
        this.objLoadPolishOrderIssue.supplier_name = result.account_name.toString().trim();

      } else {
        this.objLoadPolishOrderIssue.supplier_code = '';
        this.objLoadPolishOrderIssue.supplier_name = '';
      }
    });
  }

  public validateSupplierName(): boolean {
    let index = this.newSupplierLookupLists.findIndex(element => element.account_code.toString().trim() === this.objLoadPolishOrderIssue.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public onEnterNewSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      email: "",
      address1: "",
      address2: "",
      address3: "",
      gstn_no: ""
    };

    supplierLookupList = this.newSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objLoadPolishOrderIssue.supplier_code.toString().trim().toLowerCase());
    this.objLoadPolishOrderIssue.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objLoadPolishOrderIssue.supplier_code.toString().trim();
    this.objLoadPolishOrderIssue.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';

  }

  public checkValidSupplierCode(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objLoadPolishOrderIssue.supplier_code.toString().trim() !== '' && this.objLoadPolishOrderIssue.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier code", "Stone & Polish Issue", focus);
    }
  }

  openAlertDialog(value: string, componentName?: string, focus?: string) {
    let _dialogRef = this._dialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        document.getElementById(focus).focus();
        _dialogRef = null;
      }
      _dialogRef = null;
    });
  }


  /************************************************ Entry Table Lookup Functionalities ************************************************/

  public openNewSupplierLookups(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierCode()) {
      this.openNewSupplierLookupDialogs();
    }
  }

  private openNewSupplierLookupDialogs(): void {
    this.focusFlag = true;
    const dialogRef = this._dialog.open(AccountsLookupComponent, {
      width: "650px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objPolishOrderIssue.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objPolishOrderIssue.supplier_code = result.account_code.toString().trim();
        this.objPolishOrderIssue.supplier_name = result.account_name.toString().trim();

      } else {
        this.objPolishOrderIssue.supplier_code = '';
        this.objPolishOrderIssue.supplier_name = '';
      }
    });
  }

  public validateSupplierCode(): boolean {
    let index = this.newSupplierLookupLists.findIndex(element => element.account_code === this.objPolishOrderIssue.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public onEnterNewSupplierLookupDetail(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      email: "",
      address1: "",
      address2: "",
      address3: "",
      gstn_no: ""
    };

    supplierLookupList = this.newSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objPolishOrderIssue.supplier_code.toString().trim().toLowerCase());
    this.objPolishOrderIssue.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objPolishOrderIssue.supplier_code.toString().trim();
    this.objPolishOrderIssue.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';

  }

  public checkValidSupplierName(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objPolishOrderIssue.supplier_code.toString().trim() !== '' && this.objPolishOrderIssue.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid issue to", "Stone & Polish Issue", focus);
    }
  }

  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length > 0) {
      let datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      let json = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        json[i] = Object.assign({
          "Issue No": this.dataSource.data[i].issue_no,
          "Issue Date": this._datePipe.transform(this.dataSource.data[i].issue_date, 'dd/MM/yyyy'),
          "Issue Type": this.dataSource.data[i].issue_type,
          "Remarks": this.dataSource.data[i].remarks,
          "Status": this.dataSource.data[i].active ? 'Yes' : 'No',
        });
      }
      this._excelService.exportAsExcelFile(json, "Stone & Polish Issue", datetime);
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found", "Stone & Polish Issue");
  }

  exportToPdf(): void {

    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.issue_no);
        tempObj.push(this._datePipe.transform(e.issue_date, 'dd/MM/yyyy'));
        tempObj.push(e.issue_type);
        tempObj.push(e.remarks);
        tempObj.push(e.active ? 'Yes' : 'No');
        prepare.push(tempObj);
      });

      const doc = new jsPDF('l', 'pt', "a4");
      doc.autoTable({
        head: [["Issue No", "Issue Date", "Issue Type", "Remarks", "Status",]],
        body: prepare,
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        columnStyles: { text: { columnWidth: 'auto' } },
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });

      doc.save('Stone & Polish Issue' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Stone & Polish Issue");
  }


}
