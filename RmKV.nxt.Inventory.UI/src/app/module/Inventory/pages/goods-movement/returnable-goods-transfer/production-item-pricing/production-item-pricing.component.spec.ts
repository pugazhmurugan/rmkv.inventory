import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionItemPricingComponent } from './production-item-pricing.component';

describe('ProductionItemPricingComponent', () => {
  let component: ProductionItemPricingComponent;
  let fixture: ComponentFixture<ProductionItemPricingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionItemPricingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionItemPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
