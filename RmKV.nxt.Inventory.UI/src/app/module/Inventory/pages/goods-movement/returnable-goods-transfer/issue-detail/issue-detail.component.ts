import { DatePipe } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, ViewChildren } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { EstimatedProductLookupComponent } from 'src/app/common/shared/estimated-product-lookup/estimated-product-lookup.component';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductionReceiptService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer/production-receipt/production-receipt.service';

@Component({
  selector: 'app-issue-detail',
  templateUrl: './issue-detail.component.html',
  styleUrls: ['./issue-detail.component.scss'],
  providers: [DatePipe,]
})
export class IssueDetailComponent implements OnInit {

  focusFlag: boolean = false;
  productsList: any[] = [];
  // productDetailsList: EstimatedProductLookup[] = [];
  // objProductDetails: EstimatedProductLookup = {
  //   product_id: 0,
  //   product_name: '',
  //   product_size: '',
  //   estimated_qty: 0,
  //   received_so_far: 0
  // }
  productionDetailsList: any[] = [];
  objIssueNos: any = {
    issue_no: '',
    issue_date: '',
    transfer_type: '',
    wh_section_id: this._localStorage.getWhSectionId()
  }

  @ViewChildren('productName') productName: ElementRef | any;

  constructor(
    public _router: Router,
    public _datePipe: DatePipe,
    public _dialogRef: MatDialogRef<IssueDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    private _productionReceiptService: ProductionReceiptService,
    public _localStorage: LocalStorage
  ) {
    this._dialogRef.disableClose = true;
    this.objIssueNos = JSON.parse(JSON.stringify(this._data.objIssueNo));
    this.objIssueNos.issue_date = this._datePipe.transform(this.objIssueNos.issue_date, 'dd/MM/yyyy');
    this.getProductionIssueDetails();
    this.loadProductLookupList();
  }

  ngOnInit() {
    // this.addNewRow();
  }

  public onClear(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

  private loadProductLookupList(): void {
    let objLoad: any = {
      ProductionReceipt: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        wh_section_id: +this.objIssueNos.issue_wh_section_id,
        issue_no: +this.objIssueNos.issue_no
      }])
    }
    this._productionReceiptService.getProductionReceiptProductLookup(objLoad).subscribe((result: any[]) => {
      if (result) {
        this.productsList = JSON.parse(JSON.stringify(result));
      }
    });
  }

  private getProductionIssueDetails(): void {
    let objLoad: any = {
      ProductionReceipt: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        wh_section_id: +this.objIssueNos.issue_wh_section_id,
        issue_no: +this.objIssueNos.issue_no
      }])
    }
    this._productionReceiptService.getPendingProductionIssueDetails(objLoad).subscribe((result: any[]) => {
      if (result) {
        this.productionDetailsList = JSON.parse(JSON.stringify(result));
      }
    });
  }
  // public openProductLookup(event: any, i: number): void {
  //   debugger;
  //   if (event.keyCode === 13 && !this.validateProductName(i)) {
  //     this.openProductLookupDialog(i);
  //   }
  // }

  // private openProductLookupDialog(i: number): void {
  //   this.focusFlag = true;
  //   const dialogRef = this._matDialog.open(EstimatedProductLookupComponent, {
  //     width: "700px",
  //     panelClass: "custom-dialog-container",
  //     data: {
  //       searchString: this.productDetailsList[i].product_name,
  //       objIssueNo: JSON.parse(JSON.stringify(this.objIssueNos))
  //     }
  //   });
  //   dialogRef.afterClosed().subscribe((result: any) => {
  //     this.focusFlag = false;
  //     if (result) {
  //       this.productDetailsList[i].product_id = result.product_id;
  //       this.productDetailsList[i].product_name = result.product_name;
  //       this.productDetailsList[i].product_size = result.product_size;
  //       this.productDetailsList[i].estimated_qty = result.estimated_qty;
  //       this.productDetailsList[i].received_so_far = result.received_so_far;
  //     } else {
  //       this.productDetailsList[i].product_id = 0;
  //       this.productDetailsList[i].product_name = '';
  //       this.productDetailsList[i].product_size = '';
  //       this.productDetailsList[i].estimated_qty = 0;
  //       this.productDetailsList[i].received_so_far = 0;
  //     }
  //   });
  // }

  // public onEnterProductLookupDetails(i: number): void {
  //   let productGroupList: any = {
  //     product_id: 0,
  //     product_name: "",
  //     product_size: '',
  //     estimated_qty: 0,
  //     received_so_far: 0
  //   }
  //   productGroupList = this.productsList.find(ele => ele.product_name.toString().toLowerCase().trim() ===
  //     this.productDetailsList[i].product_name.toString().trim().toLowerCase());
  //   this.productDetailsList[i].product_name = productGroupList && productGroupList.product_name ? productGroupList.product_name : this.productDetailsList[i].product_name;
  //   this.productDetailsList[i].product_id = productGroupList && productGroupList.product_id ? productGroupList.product_id : 0;
  //   this.productDetailsList[i].product_size = productGroupList && productGroupList.product_size ? productGroupList.product_size : '';
  //   this.productDetailsList[i].estimated_qty = productGroupList && productGroupList.estimated_qty ? productGroupList.estimated_qty : 0;
  //   this.productDetailsList[i].received_so_far = productGroupList && productGroupList.estimareceived_so_farted_qty ? productGroupList.received_so_far : 0;
  // }

  // public validateProductName(i: number): boolean {
  //   let index = this.productsList.findIndex(element => element.product_name.toString().trim() === this.productDetailsList[i].product_name.toString().trim());
  //   if (index === -1)
  //     return false;
  //   else return true;
  // }

  // public checkValidProductName(i: number, event: KeyboardEvent): void {
  //   if (event.keyCode !== 13 && !this.focusFlag && this.productDetailsList[i].product_name.toString().trim() !== '' && +this.productDetailsList[i].product_id === 0)
  //     this.openInvalidAlertDialog("Invalid product group", i, "Invoices", 'productName');
  // }

  // public addNewRow(): void {
  //   this.productDetailsList.push(Object.assign({}, this.objProductDetails));
  //   setTimeout(() => {
  //     let input = this.productName.toArray();
  //     input[input.length - 1].nativeElement.focus();
  //   }, 100);
  // }

  // public removeRows(i: number): void {
  //   this.productDetailsList.splice(i, 1);
  //   setTimeout(() => {
  //     let input = this.productName.toArray();
  //     input[i].nativeElement.focus();
  //   }, 100);
  // }

  // private openInvalidAlertDialog(value: string, i: number, componentName: string, focus: any) {
  //   let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
  //     panelClass: "custom-dialog-container",
  //     data: { confirmationDialog: 0 }
  //   });
  //   _dialogRef.componentInstance.alertMessage = value;
  //   _dialogRef.componentInstance.componentName = componentName;
  //   _dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       let input = this[focus].toArray();
  //       input[i].nativeElement.focus();
  //     }
  //     _dialogRef = null;
  //   });
  // }

}

export class EstimatedProductLookup {
  product_id: number;
  product_name: string;
  product_size: string;
  estimated_qty: number;
  received_so_far: number;
}