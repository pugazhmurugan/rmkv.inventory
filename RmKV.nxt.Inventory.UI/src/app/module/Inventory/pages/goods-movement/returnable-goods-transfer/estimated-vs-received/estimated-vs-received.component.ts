import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductionIssueService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer/production-issue/production-issue.service';

@Component({
  selector: 'app-estimated-vs-received',
  templateUrl: './estimated-vs-received.component.html',
  styleUrls: ['./estimated-vs-received.component.scss']
})
export class EstimatedVsReceivedComponent implements OnInit {

  objProductSize = {
    estimated: [{
      prod_id: 0,
      prod_name: '',
      prod_size: '',
      estimated_qty: ''
    }],
    received: [{
      prod_id: 0,
      prod_name: '',
      prod_size: '',
      received_quantity: ''
    }],
    receipt_details: [{
      receipt_no: '',
      receipt_date: '',
      prod_name: '',
      prod_size: '',
      received_quantity: '',
      conversion_charges: '',
      cost_image: '',
    }]
  }
  constructor(
    public _router: Router,
    public _dialogRef: MatDialogRef<EstimatedVsReceivedComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _localStorage: LocalStorage,
    public _productionIssueService: ProductionIssueService,
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getProductionIssueEstimatedVsReceived(this._data.issueNo);
  }
  public getProductionIssueEstimatedVsReceived(issueNo): void {
    let objData = {
      ProductionIssue: JSON.stringify([{
        wh_section_id: this._localStorage.getWhSectionId(),
        issue_no: issueNo,
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._productionIssueService.getProductionIssueEstimatedVsReceived(objData).subscribe((result: any) => {
      if (result) {
        console.log(result, 'Test');
        this.objProductSize.estimated = JSON.parse(result.records1);
        this.objProductSize.received = JSON.parse(result.records2);
        this.objProductSize.receipt_details = JSON.parse(result.records3);
      }
    });
  }

  public onClear(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }


}
