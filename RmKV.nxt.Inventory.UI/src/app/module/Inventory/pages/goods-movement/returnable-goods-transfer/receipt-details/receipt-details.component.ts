import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-receipt-details',
  templateUrl: './receipt-details.component.html',
  styleUrls: ['./receipt-details.component.scss']
})
export class ReceiptDetailsComponent implements OnInit {

  constructor(
    public _router: Router,
    public _dialogRef: MatDialogRef<ReceiptDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _localStorage: LocalStorage
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
  }

  public onClear(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

}
