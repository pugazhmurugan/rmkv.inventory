import { TemplateDefinitionBuilder } from '@angular/compiler/src/render3/view/template';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { Observable, Subject } from 'rxjs';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ImageLookupComponent } from 'src/app/common/shared/image-lookup/image-lookup.component';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-imagecapture',
  templateUrl: './imagecapture.component.html',
  styleUrls: ['./imagecapture.component.scss']
})
export class ImagecaptureComponent implements OnInit {

  selectedDocument: any | File;
  document: any;

  constructor(
    public _dialogRef: MatDialogRef<ImageLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    private _commonService: CommonService,
    public _localStorage: LocalStorage
  ) { this._dialogRef.disableClose = true; }

  /************************************************** WebCam Part **********************************************/
  tempImgArr: any[] = [];
  Image: any;

  //WebCam Part
  public showWebcam = true;
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  public deviceId: string;
  public videoOptions: MediaTrackConstraints = {};
  public errors: WebcamInitError[] = [];

  // latest snapshot
  public webcamImage: WebcamImage = null;

  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();

  // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  private nextWebcam: Subject<boolean | string> = new Subject<boolean | string>();

  public ngOnInit(): void {
    WebcamUtil.getAvailableVideoInputs()
      .then((mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
      });
  }

  public triggerSnapshot(): void {
    this.trigger.next();
  }

  public toggleWebcam(): void {
    this.showWebcam = !this.showWebcam;
  }

  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
  }

  public showNextWebcam(directionOrDeviceId: boolean | string): void {
    this.nextWebcam.next(directionOrDeviceId);
  }

  public handleImage(webcamImage: WebcamImage): void {
    console.info('received webcam image', webcamImage);
    this.webcamImage = webcamImage;
  }

  public cameraWasSwitched(deviceId: string): void {
    console.log('active device: ' + deviceId);
    this.deviceId = deviceId;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean | string> {
    return this.nextWebcam.asObservable();
  }

  public onAttachClick(): void {
    debugger;
    debugger;
    let arrayBuff: ArrayBuffer = new ArrayBuffer(this.webcamImage.imageAsBase64.length);
    let byteArr: Uint8Array = new Uint8Array(arrayBuff);
    for (let i = 0; i < this.webcamImage.imageAsBase64.length; i++) {
      byteArr[i] = this.webcamImage.imageAsBase64.charCodeAt(i);
    }
    debugger;
    let img: any = new Blob([byteArr], { type: "image/png" });
    // this.uploadDocument(img);
    // this.webcamImage = null;
    // console.log(img, 'Temp Img Array');
  }

  public dialogOK(tempFile: any): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close();
  }

  public uploadDocument(): void {
    debugger;
    let arrayBuff: ArrayBuffer = new ArrayBuffer(this.webcamImage.imageAsBase64.length);
    let byteArr: Uint8Array = new Uint8Array(arrayBuff);
    for (let i = 0; i < this.webcamImage.imageAsBase64.length; i++) {
      byteArr[i] = this.webcamImage.imageAsBase64.charCodeAt(i);
    }
    debugger;
    let image: any = new Blob([byteArr], { type: "image/png" });
    let tempFile: string = '';
    let fileToUpload = image as any;
    let formData = new FormData();
    formData.append('file', fileToUpload, 'capturedImg');
    formData.append('fileRootPath', '\\\\192.9.202.39\\\\Rmkv_Nxt_Doc\\\\Invoices');
    this._commonService.uploadTempFile(formData).subscribe(
      (result: any) => {
        debugger;
        tempFile = result.document_Path;
        if (image.length != 0) {
          this.selectedDocument = image;
          this.document = image;
        } else {
          // this.fileInput.nativeElement.files = this.document;
          this.selectedDocument = this.document;
        }
      });
    let objAttach: any = {
      uploadedFileName: tempFile,
      selectedDocument: this.selectedDocument
    }
    this._dialogRef.close(objAttach);
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

}
