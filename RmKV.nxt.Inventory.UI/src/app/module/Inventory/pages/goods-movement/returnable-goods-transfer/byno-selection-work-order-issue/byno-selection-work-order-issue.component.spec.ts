import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BynoSelectionWorkOrderIssueComponent } from './byno-selection-work-order-issue.component';

describe('BynoSelectionWorkOrderIssueComponent', () => {
  let component: BynoSelectionWorkOrderIssueComponent;
  let fixture: ComponentFixture<BynoSelectionWorkOrderIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BynoSelectionWorkOrderIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BynoSelectionWorkOrderIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
