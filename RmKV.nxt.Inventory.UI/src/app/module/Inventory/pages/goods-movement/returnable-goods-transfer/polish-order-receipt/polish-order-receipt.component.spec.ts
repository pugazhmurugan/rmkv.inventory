import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolishOrderReceiptComponent } from './polish-order-receipt.component';

describe('PolishOrderReceiptComponent', () => {
  let component: PolishOrderReceiptComponent;
  let fixture: ComponentFixture<PolishOrderReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolishOrderReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolishOrderReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
