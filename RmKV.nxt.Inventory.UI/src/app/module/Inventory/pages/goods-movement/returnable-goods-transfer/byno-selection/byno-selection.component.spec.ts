import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BynoSelectionComponent } from './byno-selection.component';

describe('BynoSelectionComponent', () => {
  let component: BynoSelectionComponent;
  let fixture: ComponentFixture<BynoSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BynoSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BynoSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
