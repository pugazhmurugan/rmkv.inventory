import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstimatedVsReceivedComponent } from './estimated-vs-received.component';

describe('EstimatedVsReceivedComponent', () => {
  let component: EstimatedVsReceivedComponent;
  let fixture: ComponentFixture<EstimatedVsReceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstimatedVsReceivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstimatedVsReceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
