import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolishOrderIssueComponent } from './polish-order-issue.component';

describe('PolishOrderIssueComponent', () => {
  let component: PolishOrderIssueComponent;
  let fixture: ComponentFixture<PolishOrderIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolishOrderIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolishOrderIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
