import { DatePipe } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode, Rights } from 'src/app/common/models/common-model';
import { AccountsLookupService } from 'src/app/common/services/accounts-lookup/accounts-lookup.service';
import { CommonService } from 'src/app/common/services/common/common.service';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductionMaterialLookupComponent } from 'src/app/common/shared/production-material-lookup/production-material-lookup.component';
import { ProductionProductLookupComponent } from 'src/app/common/shared/production-product-lookup/production-product-lookup.component';
import { ForCheckBynoData } from 'src/app/module/Inventory/model/common.model';
import { ProductionIssueService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer/production-issue/production-issue.service';
import { ProductionMaterialsService } from 'src/app/module/Inventory/services/masters/production-materials/production-materials.service';
import { ProductionProductsService } from 'src/app/module/Inventory/services/masters/Production-Product/production-products.service';
import { EstimatedVsReceivedComponent } from '../estimated-vs-received/estimated-vs-received.component';
declare var jsPDF: any;

@Component({
  selector: 'app-production-issue',
  templateUrl: './production-issue.component.html',
  styleUrls: ['./production-issue.component.scss'],
  providers: [DatePipe]
})
export class ProductionIssueComponent implements OnInit {

  /****************************************** Declaration **********************************************/

  componentVisibility: boolean = true;
  isReconciliation: boolean = false;

  objLoad: any = {
    from_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    to_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    status: 'All',
    supplier_code: '',
    supplier_name: '',
    wh_section_id: +this._localStorage.getWhSectionId(),
    company_section_id: +this._localStorage.getCompanySectionId(),
  }

  objProductionIssue: any = {
    issue_no: 0,
    transfer_type: '',
    issue_date: new Date(),
    section: this._localStorage.getCompanySectionName(),
    supplier_code: '',
    supplier_name: '',
    remarks: '',
    entered_by: +this._localStorage.intGlobalUserId(),
    wh_section_id: +this._localStorage.getWhSectionId(),
    company_section_id: +this._localStorage.getCompanySectionId(),
    issue_details: [{
      material_id: 0,
      material_name: '',
      byno: '',
      inv_byno: '',
      byno_serial: '',
      byno_prod_serial: '',
      damaged: true,
      length: '',
      issued_qty: '',
      estimated_loss: ''
    }],
    issue_estimation_details: [{
      prod_id: 0,
      product_name: '',
      prod_size: '',
      estimated_qty: ''
    }],
  }

  objModifyProductionIssue: any = {
    issue_no: 0,
    transfer_type: '',
    issue_date: new Date(),
    section: this._localStorage.getCompanySectionName(),
    supplier_code: '',
    supplier_name: '',
    remarks: '',
    entered_by: +this._localStorage.intGlobalUserId(),
    wh_section_id: +this._localStorage.getWhSectionId(),
    company_section_id: +this._localStorage.getCompanySectionId(),
    issue_details: [{
      material_id: 0,
      material_name: '',
      byno: '',
      inv_byno: '',
      byno_serial: '',
      byno_prod_serial: '',
      damaged: true,
      length: '',
      issued_qty: '',
      estimated_loss: ''
    }],
    issue_estimation_details: [{
      prod_id: 0,
      product_name: '',
      prod_size: '',
      estimated_qty: ''
    }],
  }

  objUnChangedProductionIssue: any = {
    issue_no: 0,
    transfer_type: '',
    issue_date: new Date(),
    section: this._localStorage.getCompanySectionName(),
    supplier_code: '',
    supplier_name: '',
    remarks: '',
    entered_by: +this._localStorage.intGlobalUserId(),
    wh_section_id: +this._localStorage.getWhSectionId(),
    company_section_id: +this._localStorage.getCompanySectionId(),
    issue_details: [{
      material_id: 0,
      material_name: '',
      byno: '',
      inv_byno: '',
      byno_serial: '',
      byno_prod_serial: '',
      damaged: true,
      length: '',
      issued_qty: '',
      estimated_loss: ''
    }],
    issue_estimation_details: [{
      prod_id: 0,
      product_name: '',
      prod_size: '',
      estimated_qty: ''
    }],
  }
  objReconciliation: any = {
    issue_no: 0,
    transfer_type: '',
    issue_date: '',
    section: this._localStorage.getCompanySectionName(),
    supplier_code: '',
    supplier_name: '',
    remarks: '',
    entered_by: +this._localStorage.intGlobalUserId(),
    wh_section_id: +this._localStorage.getWhSectionId(),
    company_section_id: +this._localStorage.getCompanySectionId(),
    closed: '',
    reconciliation_details: [{
      material_id: 0,
      material_name: '',
      byno: '',
      inv_byno: '',
      byno_serial: '',
      byno_prod_serial: '',
      damaged: true,
      length: '',
      issued_qty: '',
      consumed_qty: '',
      returned_qty: '',
      remarks: '',
      estimated_loss: ''
    }]
  }
  unchangedReconciliation: any = {
    issue_no: 0,
    transfer_type: '',
    issue_date: '',
    section: this._localStorage.getCompanySectionName(),
    supplier_code: '',
    supplier_name: '',
    remarks: '',
    entered_by: +this._localStorage.intGlobalUserId(),
    wh_section_id: +this._localStorage.getWhSectionId(),
    company_section_id: +this._localStorage.getCompanySectionId(),
    closed: '',
    reconciliation_details: [{
      material_id: 0,
      material_name: '',
      byno: '',
      inv_byno: '',
      byno_serial: '',
      byno_prod_serial: '',
      damaged: true,
      length: '',
      issued_qty: '',
      consumed_qty: '',
      returned_qty: '',
      remarks: '',
      estimated_loss: ''
    }]
  }

  objIssueDetails: any = {
    material_id: 0,
    material_name: '',
    byno: '',
    inv_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    damaged: true,
    length: '',
    issued_qty: '',
    estimated_loss: ''
  }

  objEstimationDetails: any = {
    prod_id: 0,
    product_name: '',
    prod_size: '',
    estimated_qty: ''
  }

  ForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  UnChangedForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  objAction: EditMode = {
    isEditing: false,
    isView: false,
  }

  objUnchangedAction: EditMode = {
    isEditing: false,
    isView: false,
  }

  rights: Rights = {
    Add: true,
    Update: true,
    Delete: true,
    Approve: true,
    View: true
  };

  objAttachView: any = {
    file_path1: '',
    file_path2: '',
    file_path3: '',
    file_path4: ''
  }

  unchangedAttach: any = {
    file_path1: '',
    file_path2: '',
    file_path3: '',
    file_path4: ''
  }

  selectedFiles: File[] | any[] = [];
  fileIndex: number[] = [];
  isCancelled: boolean = false;
  isExceedCount: boolean = false;
  removedFileIndex: number[] = [];
  removedFiles: string[] = [];

  Inv_Byno: string = "";
  Byno_Serial: string = "";
  Byno_Prod_Serial: string = "";
  keyUpEvent: any;

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  model1: any = new Date();
  model2: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  productionIssueList: any = [];
  newSupplierLookupList: any = [];
  loadSupplierLookupList: any = [];

  focusFlag: boolean = false;

  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Issue_No", "Issue_Date", "Issue_Type", "Remarks", "Status", "Action"];

  columns: any[] = [
    { display: 'sno', editable: false },
    { display: 'materialName', editable: true },
    { display: 'byno', editable: true },
    { display: "sellingPrice", editable: false },
    { display: "damaged", editable: false },
    { display: "length", editable: true },
    { display: "issuedQty", editable: true },
    { display: "estimatedLoss", editable: true },
    { display: "pre", editable: false },
    { display: "action", editable: true }
  ];

  @ViewChildren('materialName') materialName: ElementRef | any;
  @ViewChildren('byno') byno: ElementRef | any;
  @ViewChildren('sellingPrice') sellingPrice: ElementRef | any;
  @ViewChildren('damaged') damaged: ElementRef | any;
  @ViewChildren('length') length: ElementRef | any;
  @ViewChildren('issuedQty') issuedQty: ElementRef | any;
  @ViewChildren('estimatedLoss') estimatedLoss: ElementRef | any;
  @ViewChildren('pre') pre: ElementRef | any;
  @ViewChildren('action') action: ElementRef | any;

  column: any[] = [
    { display: 'productName', editable: true },
    { display: 'productSize', editable: false },
    { display: 'estimatedQty', editable: true },
    { display: 'action', editable: false }
  ];

  @ViewChildren('productName') productName: ElementRef | any;
  @ViewChildren('estimatedQty') estimatedQty: ElementRef | any;
  ProductNameList: any;
  materialNameList: any;
  objBynoList: any;
  closeFlag: boolean = false;

  /****************************************** Constructor adn ngOnInit **********************************************/

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    private _datePipe: DatePipe,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _excelService: ExcelService,
    public _productionIssueService: ProductionIssueService,
    public _commonService: CommonService,
    private _gridKeyEvents: GridKeyEvents,
    public _dialog: MatDialog,
    public _keyPressEvents: KeyPressEvents,
    private _accountsLookupService: AccountsLookupService,
    public _productionProductsService: ProductionProductsService,
    private _prodMaterialService: ProductionMaterialsService,
  ) {
    this.model1.setDate(this.model2.getDate() - 2);
    this.objLoad.from_date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
  }

  ngOnInit() {
    this.getRights();
    this.getSupplierLookupList();
    this.getProductNameList();
    this.getMaterialNameList();
  }

  /****************************************** Validations **********************************************/

  public validateFromDate() {
    let date = this.dateValidation.validateDate(this.objLoad.from_date, this.model1, this.minDate, this.maxDate);
    this.objLoad.from_date = date[0];
    this.model1 = date[1];
  }

  public validateToDate() {
    let date = this.dateValidation.validateDate(this.objLoad.to_date, this.model2, this._datePipe.transform(this.model1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoad.to_date = date[0];
    this.model2 = date[1];
  }

  private getRights(): void {
    var rights = this._localStorage.getMenuRights("/ProductionIssue");
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "Delete" && rights[x].Status == true) {
        this.rights.Delete = true;
      }
      if (rights[x].Right_Name == "Approve" && rights[x].Status == true) {
        this.rights.Approve = true;
      }
      if (rights[x].Right_Name == "View" && rights[x].Status == true) {
        this.rights.View = true;
      }
    }
  }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        let response = this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.objProductionIssue.issue_details);
        if (response)
          this.addNewRowToMaterialList(rowIndex);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.objProductionIssue.issue_details);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.objProductionIssue.issue_details);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.objProductionIssue.issue_details);
        break;
    }
  }
  public onKeyFocusGlobalForProducts(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        let response = this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.column, this, this.objProductionIssue.issue_estimation_details);
        if (response)
          this.addNewRowToEstimationList(rowIndex);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.column, this, this.objProductionIssue.issue_estimation_details);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.column, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.column, this, this.objProductionIssue.issue_estimation_details);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.column, this, this.objProductionIssue.issue_estimation_details);
        break;
    }
  }
  public beforeLoadValidate(): boolean {
    if (!this.objLoad.from_date.toString().trim()) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter from date", "Work Order ReceiptList");
      return false;
    } else if (this.objLoad.from_date.length != 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid from date", "Work Order ReceiptList");
      return false;
    } else if (!this.objLoad.to_date.toString().trim()) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter to date", "Work Order ReceiptList");
      return false;
    } else if (this.objLoad.to_date.length != 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid to date", "Work Order ReceiptList");
      return false;
    } else if (this.model2 < this.model1) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("To date can't be less than from date", "Work Order ReceiptList");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objLoad.supplier_code) !== -1 || !this.objLoad.supplier_code.toString().trim()) {
      document.getElementById('supplierCode').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Supplier Code", "Production Issue");
      return false;
    } else {
      return true;
    }
  }

  private beforeSaveValidate(): boolean {
    debugger;
    if ([null, undefined, 0, ""].indexOf(this.objProductionIssue.supplier_code) !== -1 || !this.objProductionIssue.supplier_code.toString().trim()) {
      document.getElementById('supplierCode').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Supplier Code", "Production Issue");
      return false;
    } else {
      return true;
    }
  }

  public onlyAllwDecimalForProductionIssue(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  /****************************************** CRUD Operations **********************************************/

  public getProductionIssue(): void {
    if (this.beforeLoadValidate()) {
      this.matTableConfig([]);
      let objData = {
        ProductionIssue: JSON.stringify([{
          from_date: this.objLoad.from_date,
          to_date: this.objLoad.to_date,
          status: this.objLoad.status,
          supplier_code: this.objLoad.supplier_code,
          wh_section_id: +this._localStorage.getWhSectionId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
        }]),
      }
      this._productionIssueService.getProductionIssue(objData).subscribe((result: any) => {
        if (result) {
          this.productionIssueList = result;
          console.log(this.productionIssueList, 'productionIssueList');
          this.matTableConfig(this.productionIssueList);
        } else
          this._confirmationDialogComponent.openAlertDialog('No records found', 'Production Issue');
      });
    }
  }

  public matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }



  fetchProductionIssue(data: any): void {
    let objData = {
      ProductionIssue: JSON.stringify([{
        issue_no: data.issue_no,
        wh_section_id: +this._localStorage.getWhSectionId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._productionIssueService.fetchProductionIssue(objData).subscribe((result: any) => {
      if (result) {
        this.objAction.isEditing = true;
        this.objAction.isView = false;
        this.componentVisibility = !this.componentVisibility;
        let tempArr = JSON.parse(result.records1);
        this.objProductionIssue = tempArr[0];
        this.objProductionIssue.issue_details = JSON.parse(result.records2);
        this.objProductionIssue.issue_estimation_details = JSON.parse(result.records3);
        this.objProductionIssue.section = this._localStorage.getCompanySectionName();
        this.objModifyProductionIssue = JSON.parse(JSON.stringify(this.objProductionIssue));
      }
    });
  }

  viewProductionIssue(data: any): void {
    let objData = {
      ProductionIssue: JSON.stringify([{
        issue_no: data.issue_no,
        wh_section_id: +this._localStorage.getWhSectionId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._productionIssueService.fetchProductionIssue(objData).subscribe((result: any) => {
      if (result) {
        this.objAction.isEditing = false;
        this.objAction.isView = true;
        this.componentVisibility = !this.componentVisibility;
        let tempArr = JSON.parse(result.records1);
        this.objProductionIssue = tempArr[0];
        this.objProductionIssue.issue_details = JSON.parse(result.records2);
        this.objProductionIssue.issue_estimation_details = JSON.parse(result.records3);
        this.objProductionIssue.section = this._localStorage.getCompanySectionName();
        this.objModifyProductionIssue = JSON.parse(JSON.stringify(this.objProductionIssue));
      }
    });
  }

  public openCancelConfirmationDialog(data: any): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = 'Do you want cancel this record ?';
    dialogRef.componentInstance.componentName = 'Production Issue';
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.cancelProductionIssue(data);
      dialogRef = null;
    });
  }

  public cancelProductionIssue(data: any) {
    let objData = {
      ProductionIssue: JSON.stringify([{
        issue_no: data.issue_no,
        wh_section_id: this._localStorage.getWhSectionId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        cancel: false,
        cancelled_reason: '',
        entered_by: +this._localStorage.intGlobalUserId()
      }]),
    }
    this._productionIssueService.cancelProductionIssue(objData).subscribe((result: any) => {
      if (result) {
        this._confirmationDialogComponent.openAlertDialog("Cancel successfully", "Production Issue");
        this.getProductionIssue();
      }
    });
  }

  /****************************************** Click Functions **********************************************/

  public newClick(): void {
    if (this.rights.Add) {
      this.objAction.isEditing = false;
      this.objAction.isView = false;
      this.objProductionIssue.issue_details = [];
      this.objProductionIssue.issue_estimation_details = [];
      this.objProductionIssue.issue_details.push(Object.assign({}, this.objIssueDetails));
      this.objProductionIssue.issue_estimation_details.push(Object.assign({}, this.objEstimationDetails));
      this.componentVisibility = !this.componentVisibility;
      this.isReconciliation = false;
      this.resetScreen();
    }
  }

  onClear(exitFlag: any) {
    if (!this.objAction.isView) {
      if (this.checkAnyChangesMade())
        this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
      else if (exitFlag)
        this.onListClick();
    } else
      this.onListClick();
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objProductionIssue) !== (this.objAction.isEditing ? JSON.stringify(this.objModifyProductionIssue) : JSON.stringify(this.objUnChangedProductionIssue)))
      return true;
    else
      return false;
  }

  private openConfirmationDialog(message: string, exitFlag: any): void {
    debugger;
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Production Issue";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
    });
  }

  public onListClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.isReconciliation = false;
    this.resetScreen();
  }

  public resetScreen(): void {
    this.objAction.isEditing = false;
    this.objProductionIssue = JSON.parse(JSON.stringify(this.objUnChangedProductionIssue));
  }

  public onClearReconciliation() {
    this.isReconciliation = false;
    this.componentVisibility = !this.componentVisibility;
  }

  public reconciliationClick(issueNo) {
    this.isReconciliation = true;
    this.componentVisibility = false;
    this.getProductionIssueReconciliation(issueNo);
  }

  public addNewRowToMaterialList(i: number): void {
    debugger
    if (this.objProductionIssue.issue_details[i].material_name !== '' && this.objProductionIssue.issue_details[i].byno !== '' && this.objProductionIssue.issue_details[i].selling_price !== '' && this.objProductionIssue.issue_details[i].length !== '' && this.objProductionIssue.issue_details[i].issued_qty !== '' && this.objProductionIssue.issue_details[i].estimated_loss !== '') {
      this.objProductionIssue.issue_details.push(Object.assign({}, this.objIssueDetails));
      setTimeout(() => {
        let input = this.materialName.toArray();
        input[input.length - 1].nativeElement.focus();
      }, 100);
    }
  }

  public removeRowFromMaterialList(i: number): void {
    this.openRemoveConfirmationDialog('Are you sure want to remove row ' + (i + 1) + ' from the list', 'Production Issue', i);
  }

  public openRemoveConfirmationDialog(message: string, componentName: string, i: number): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = componentName;
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deleteRow(i);
      dialogRef = null;
    });
  }

  private deleteRow(i: number) {
    this.objProductionIssue.issue_details.splice(i, 1);
    if (i === this.objProductionIssue.issue_details.length) {
      setTimeout(() => {
        let inputEls = this.materialName.toArray();
        inputEls[i - 1].nativeElement.focus();
      }, 100);
    } else {
      setTimeout(() => {
        let inputEls = this.materialName.toArray();
        inputEls[i].nativeElement.focus();
      }, 100);
    }
  }

  public addNewRowToEstimationList(i: number): void {
    debugger
    if (this.objProductionIssue.issue_estimation_details[i].product_name !== '' && this.objProductionIssue.issue_estimation_details[i].prod_size !== '' && this.objProductionIssue.issue_estimation_details[i].estimated_qty !== '') {
      this.objProductionIssue.issue_estimation_details.push(Object.assign({}, this.objEstimationDetails));
      setTimeout(() => {
        let input = this.productName.toArray();
        input[input.length - 1].nativeElement.focus();
      }, 100);
    }
  }

  public removeRowFromEstimationList(i: number): void {
    this.openRemoveEstimationConfirmationDialog('Are you sure want to remove row ' + (i + 1) + ' from the list', 'Production Issue', i);
  }

  public openRemoveEstimationConfirmationDialog(message: string, componentName: string, i: number): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = componentName;
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deleteEstimationRow(i);
      dialogRef = null;
    });
  }

  private deleteEstimationRow(i: number) {
    this.objProductionIssue.issue_estimation_details.splice(i, 1);
    if (i === this.objProductionIssue.issue_estimation_details.length) {
      setTimeout(() => {
        let inputEls = this.productName.toArray();
        inputEls[i - 1].nativeElement.focus();
      }, 100);
    } else {
      setTimeout(() => {
        let inputEls = this.productName.toArray();
        inputEls[i].nativeElement.focus();
      }, 100);
    }
  }

  public onKeyPress(keyUpEvent: any, i: number) {
    debugger;
   // this.resetRow(i, true);
    if (keyUpEvent.keyCode === 13 && this.objProductionIssue.issue_details[i].byno === '') {
      this.openAlertDialogByno(i, 'Enter byno');
    } else if (keyUpEvent.keyCode === 13 && this.objProductionIssue.issue_details[i].byno.length == 16 && !this.objProductionIssue.issue_details[i].byno.includes("/")) {
      this.byNoWithoutSlash(i);
    } else if (keyUpEvent.keyCode === 13 && this.objProductionIssue.issue_details[i].byno.length == 28 && !this.objProductionIssue.issue_details[i].byno.includes("/")) {
      this.OldByNoWithoutSlash(i);
    } else if (keyUpEvent.keyCode === 13 && this.objProductionIssue.issue_details[i].byno.length == 30 && !this.objProductionIssue.issue_details[i].byno.includes("/")) {
      this.byNoWithoutSlash(i);
    } else if (keyUpEvent.keyCode === 13 && this.objProductionIssue.issue_details[i].byno.toString().trim().includes("/")) {
      debugger
      this.splitByNumber(i);
    } else if (this.objProductionIssue.issue_details[i].byno.length >= 28 && keyUpEvent.keyCode === 13) {
      this.autoScannerCall(i);
    } else {
      if (keyUpEvent.keyCode === 13 && (this.objProductionIssue.issue_details[i].byno.toString().trim().includes('/') && this.objProductionIssue.issue_details[i].byno.split('/').length !== 3)) {
        this.openAlertDialogByno(i, 'Invalid byno');
        this.objProductionIssue.issue_details[i].byno = '';
      } else if (keyUpEvent.keyCode === 13 && !this.objProductionIssue.issue_details[i].byno.toString().trim().includes("/") && this.objProductionIssue.issue_details[i].byno.length > 16) {
        this.openAlertDialogByno(i, 'Invalid byno');
      } else if (keyUpEvent.keyCode === 13 && !this.objProductionIssue.issue_details[i].byno.toString().trim().includes('/')) {
        this.openAlertDialogByno(i, 'Invalid byno');
        this.objProductionIssue.issue_details[i].byno = '';
      }
    }
  }

  private byNoWithoutSlash(i: number) {
    debugger;
    this.objProductionIssue.issue_details[i].byno = this.objProductionIssue.issue_details[i].byno.toString().trim().toUpperCase();
    // this.ForCheckBynoData.Inv_Byno = this.objProductionIssue.issue_details[i].byno.substr(0, 8);
    // this.ForCheckBynoData.Byno_Serial = "0" + this.objProductionIssue.issue_details[i].byno.substr(9, 3);
    // this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objProductionIssue.issue_details[i].byno.substr(13, 3);
    this.objProductionIssue.issue_details[i].inv_byno = this.objProductionIssue.issue_details[i].byno.substr(0, 8);
    this.objProductionIssue.issue_details[i].byno_serial = "0" + this.objProductionIssue.issue_details[i].byno.substr(9, 3);
    this.objProductionIssue.issue_details[i].byno_prod_serial = "0" + this.objProductionIssue.issue_details[i].byno.substr(13, 3);
    this.getBynoList(i);
  }

  private OldByNoWithoutSlash(i: number) {
    debugger;
    this.objProductionIssue.issue_details[i].byno = this.objProductionIssue.issue_details[i].byno.toString().trim().toUpperCase();
    // this.ForCheckBynoData.Inv_Byno = this.objProductionIssue.issue_details[i].byno.substr(0, 8);
    // this.ForCheckBynoData.Byno_Serial = "0" + this.objProductionIssue.issue_details[i].byno.substr(8, 3);
    // this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objProductionIssue.issue_details[i].byno.substr(11, 3);
    this.objProductionIssue.issue_details[i].inv_byno = this.objProductionIssue.issue_details[i].byno.substr(0, 8);
    this.objProductionIssue.issue_details[i].byno_serial = "0" + this.objProductionIssue.issue_details[i].byno.substr(8, 3);
    this.objProductionIssue.issue_details[i].byno_prod_serial = "0" + this.objProductionIssue.issue_details[i].byno.substr(11, 3);

    this.getBynoList(i);
  }

  private autoScannerCall(i: number) {
    debugger;
    // this.ForCheckBynoData.Inv_Byno = this.objProductionIssue.issue_details[i].byno.substr(0, 8);
    // this.ForCheckBynoData.Byno_Serial = this.objProductionIssue.issue_details[i].byno.substr(8, 4);
    // this.ForCheckBynoData.Byno_Prod_Serial = this.objProductionIssue.issue_details[i].byno.substr(11, 4);
    this.objProductionIssue.issue_details[i].inv_byno = this.objProductionIssue.issue_details[i].byno.substr(0, 8);
    this.objProductionIssue.issue_details[i].byno_serial = this.objProductionIssue.issue_details[i].byno.substr(8, 4);
    this.objProductionIssue.issue_details[i].byno_prod_serial = this.objProductionIssue.issue_details[i].byno.substr(8, 4);

    this.getBynoList(i);
  }

  private splitByNumber(i: number) {
    debugger;
    this.objProductionIssue.issue_details[i].byno = this.objProductionIssue.issue_details[i].byno.toString().trim().toUpperCase();
    var x = this.objProductionIssue.issue_details[i].byno.split("/");
    this.Inv_Byno = x[0];
    this.Byno_Serial = x[1];
    this.Byno_Prod_Serial = [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(x[2]) !== -1 ? "" : x[2];
    if (this.Byno_Serial.length == 2) {
      this.Byno_Serial = "00".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 2) {
      this.Byno_Prod_Serial = "00".concat(this.Byno_Prod_Serial);
    }
    if (this.Byno_Serial.length == 1) {
      this.Byno_Serial = "000".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 1) {
      this.Byno_Prod_Serial = "000".concat(this.Byno_Prod_Serial);
    } if (this.Byno_Serial.length == 0) {
      this.openAlertDialogByno(i, 'Invalid byno');
      return;
    } if (this.Byno_Prod_Serial.length == 0) {
      this.openAlertDialogByno(i, 'Invalid byno');
      return;
    }
    // this.ForCheckBynoData.Inv_Byno = this.Inv_Byno.toString().trim();
    // this.ForCheckBynoData.Byno_Serial = this.Byno_Serial.toString().trim();
    // this.ForCheckBynoData.Byno_Prod_Serial = this.Byno_Prod_Serial.toString().trim();
    this.objProductionIssue.issue_details[i].inv_byno = this.Inv_Byno.toString().trim();
    this.objProductionIssue.issue_details[i].byno_serial = this.Byno_Serial.toString().trim();
    this.objProductionIssue.issue_details[i].byno_prod_serial = this.Byno_Prod_Serial.toString().trim();

    this.getBynoList(i);
  }

  private resetRow(i: number, isByNo?: boolean): void {
    if (!isByNo)
      this.objProductionIssue.issue_details[i].byno = '';
    this.objProductionIssue.issue_details[i].selling_price = '';
  }

  private checkExistByno(i: number) {
    let already = false;
    let index = 0;
    let byno = '';
    for (let j = 0; (j < this.objProductionIssue.issue_details.length && j !== i); j++) {
      if (this.objProductionIssue.issue_details[j].byno === this.objProductionIssue.issue_details[i].byno) {
        already = true;
        index = j + 1;
        byno = this.objProductionIssue.issue_details[i].byno;
        break;
      }
    }
    if (already) {
      this.resetRow(i);
      this.openAlertDialogByno(i, "This Byno " + " " + byno + " Already Available In Row No" + " " + index + "");
    }
  }

  private openAlertDialogByno(i: number, message: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = message;
    _dialogRef.componentInstance.componentName = "Byno";
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this.byno.toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  public getBynoList(i: number): void {
    let objGet: any = {
      ByNoData: JSON.stringify([{
        inv_byno: this.objProductionIssue.issue_details[i].inv_byno.toString().trim().toUpperCase(),
        byno_serial: this.objProductionIssue.issue_details[i].byno_serial.toString().trim().toUpperCase(),
        byno_prod_serial: this.objProductionIssue.issue_details[i].byno_prod_serial.toString().trim().toUpperCase(),
        company_section_id: this._localStorage.getCompanySectionId()
      }])
    }
    this._commonService.getByNoDetails(objGet).subscribe((result: any) => {
      if (result !== 'null' && JSON.parse(result).length > 0) {
        let data = JSON.parse(result);
        if (data[0].byno !== null) {
          this.objBynoList = data[0];
          this.objProductionIssue.issue_details[i].selling_price = this.objBynoList.selling_price + '.00';
          this.objProductionIssue.issue_details[i].image_path = this.objBynoList.image_path;
          console.log(result, 'Byno List');
          this.checkExistByno(i);
        } else {
          this.openAlertDialogByno(i, "Invalid ByNo");
          this.resetRow(i, true);
        }
      } else {
        this.openAlertDialogByno(i, "Invalid ByNo");
        this.resetRow(i, true);
      }
    });
  }

  /****************************************** Lookup Functions **********************************************/

  private getSupplierLookupList(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._accountsLookupService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      debugger;
      if (result) {
        this.newSupplierLookupList = JSON.parse(result);
        this.loadSupplierLookupList = JSON.parse(result);
        this.newSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
        this.loadSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
      }
    });
  }

  public openSupplierLookupList(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierName()) {
      this.openSupplierLookupListDialog();
    }
  }

  private openSupplierLookupListDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "750px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objLoad.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objLoad.supplier_code = result.account_code.toString().trim();
        this.objLoad.supplier_name = result.account_name.toString().trim();
      } else {
        this.objLoad.supplier_code = '';
        this.objLoad.supplier_name = '';
      }
    });
  }

  public validateSupplierName(): boolean {
    let index = this.loadSupplierLookupList.findIndex(element => element.account_code.toString().trim() === this.objLoad.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public checkValidSupplierCode(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objLoad.supplier_code.toString().trim() !== '' && this.objLoad.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier code", "Production Issue", focus);
    }
  }

  public onEnterSupplierLookupList(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      email: "",
      address1: "",
      address2: "",
      address3: "",
      gstn_no: ""
    };
    supplierLookupList = this.loadSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objLoad.supplier_code.toString().trim().toLowerCase());
    this.objLoad.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objLoad.supplier_code.toString().trim();
    this.objLoad.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
  }

  public openSupplierLookupNew(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateNewSupplierName()) {
      this.openSupplierLookupNewDialog();
    }
  }

  private openSupplierLookupNewDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "750px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objProductionIssue.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objProductionIssue.supplier_code = result.account_code.toString().trim();
        this.objProductionIssue.supplier_name = result.account_name.toString().trim();
      } else {
        this.objProductionIssue.supplier_code = '';
        this.objProductionIssue.supplier_name = '';
      }
    });
  }

  public validateNewSupplierName(): boolean {
    let index = this.newSupplierLookupList.findIndex(element => element.account_code.toString().trim() === this.objProductionIssue.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public checkValidNewSupplierCode(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objProductionIssue.supplier_code.toString().trim() !== '' && this.objProductionIssue.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier code", "Production Issue", focus);
    }
  }

  public onEnterSupplierLookupNew(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      email: "",
      address1: "",
      address2: "",
      address3: "",
      gstn_no: ""
    };
    supplierLookupList = this.newSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objProductionIssue.supplier_code.toString().trim().toLowerCase());
    this.objProductionIssue.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objProductionIssue.supplier_code.toString().trim();
    this.objProductionIssue.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
  }

  openAlertDialog(value: string, componentName?: string, focus?: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        document.getElementById(focus).focus();
        _dialogRef = null;
      }
      _dialogRef = null;
    });
  }

  private getProductNameList(): void {
    this._productionProductsService.getProductionProductLookup().subscribe((result: any) => {
      if (result && result.length > 0) {
        this.ProductNameList = result;
        console.log(this.ProductNameList, 'prod');
      }
    });
  }

  public openProductLookup(event: any, index: any): void {
    if (event.keyCode === 13 && !this.objProductionIssue.issue_estimation_details[index].prod_size) {
      this.openProductLookupDialog(index);
    }
  }

  private openProductLookupDialog(index: number): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(ProductionProductLookupComponent, {
      width: "500px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objProductionIssue.issue_estimation_details[index].product_name
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      debugger;
      this.focusFlag = false;
      if (result) {
        this.objProductionIssue.issue_estimation_details[index].prod_id = result.prod_id;
        this.objProductionIssue.issue_estimation_details[index].product_name = result.prod_name.toString().trim();
        this.objProductionIssue.issue_estimation_details[index].prod_size = result.prod_size;
        this.checkProductAlreadyEntered(index)
      } else {
        this.objProductionIssue.issue_estimation_details[index].prod_id = 0;
        this.objProductionIssue.issue_estimation_details[index].product_name = '';
        this.objProductionIssue.issue_estimation_details[index].prod_size = '';
      }
    });
  }

  private checkProductAlreadyEntered(index: number): void {
    let alreadyFlag = false;
    let productName = '';
    let productSize = '';
    for (let i = 0; i < this.objProductionIssue.issue_estimation_details.length; i++) {
      if (i !== index && this.objProductionIssue.issue_estimation_details[i].product_name === this.objProductionIssue.issue_estimation_details[index].product_name && this.objProductionIssue.issue_estimation_details[i].prod_size === this.objProductionIssue.issue_estimation_details[index].prod_size) {
        productName = this.objProductionIssue.issue_estimation_details[i].product_name;
        productSize = this.objProductionIssue.issue_estimation_details[i].prod_size;
        alreadyFlag = true;
        break;
      }
    } if (alreadyFlag) {
      this._confirmationDialogComponent.openAlertDialog("This product '" + productName + "' already exists on current list", "Production Issue");
      this.objProductionIssue.issue_estimation_details[index].prod_id = 0;
      this.objProductionIssue.issue_estimation_details[index].product_name = "";
      this.objProductionIssue.issue_estimation_details[index].prod_size = "";
    }
  }

  public onEnterProductLookupDetail(i: any): void {
    debugger;
    let productLookupList: any = {
      prod_id: 0,
      product_name: "",
      prod_size: ""
    };
    productLookupList = this.ProductNameList.find(product => product.prod_name.toLowerCase().trim() === this.objProductionIssue.issue_estimation_details[i].product_name.toString().trim().toLowerCase());
    this.objProductionIssue.issue_estimation_details[i].prod_id = productLookupList && productLookupList.prod_id ? productLookupList.prod_id : 0;
    this.objProductionIssue.issue_estimation_details[i].prod_size = productLookupList && productLookupList.prod_size ? productLookupList.prod_size : '';
    this.checkProductAlreadyEntered(i);
  }


  private getMaterialNameList(): void {
    this._prodMaterialService.getProductionMaterialDetails().subscribe((result: any) => {
      if (result && result.length > 0) {
        this.materialNameList = result;
      }
    });
  }

  public openMaterialLookup(event: any, index: any): void {
    debugger;
    if (event.keyCode === 13 && !this.objProductionIssue.issue_details[index].material_name) {
      this.openMaterialLookupDialog(index);
    }
  }

  private openMaterialLookupDialog(index: number): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(ProductionMaterialLookupComponent, {
      width: "500px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objProductionIssue.issue_details[index].material_name
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      debugger;
      this.focusFlag = false;
      if (result) {
        this.objProductionIssue.issue_details[index].material_id = result.material_id;
        this.objProductionIssue.issue_details[index].material_name = result.material_name.toString().trim();
        this.checkMaterialAlreadyEntered(index);
      } else {
        this.objProductionIssue.issue_details[index].material_id = 0;
        this.objProductionIssue.issue_details[index].material_name = '';
      }
    });
  }

  public onEnterMaterialLookupDetail(i: any): void {
    let materialLookupList: any = {
      material_id: 0,
      material_name: "",
    };
    materialLookupList = this.materialNameList.find(material => material.material_name.toLowerCase().trim() === this.objProductionIssue.issue_details[i].material_name.toString().trim().toLowerCase());
    this.objProductionIssue.issue_details[i].material_name = materialLookupList && materialLookupList.material_name ? materialLookupList.material_name : this.objProductionIssue.issue_details[i].material_name;
    this.objProductionIssue.issue_details[i].material_id = materialLookupList && materialLookupList.material_id ? materialLookupList.material_id : 0;
    this.checkMaterialAlreadyEntered(i);
  }

  private checkMaterialAlreadyEntered(index: number): void {
    let alreadyFlag = false;
    let materialName = '';
    for (let i = 0; i < this.objProductionIssue.issue_details.length; i++) {
      if (i !== index && this.objProductionIssue.issue_details[i].material_name === this.objProductionIssue.issue_details[index].material_name) {
        materialName = this.objProductionIssue.issue_details[i].material_name;
        alreadyFlag = true;
        break;
      }
    } if (alreadyFlag) {
      this._confirmationDialogComponent.openAlertDialog("This material '" + materialName + "' already exists on current list", "Production Issue");
      this.objProductionIssue.issue_details[index].material_id = 0;
      this.objProductionIssue.issue_details[index].material_name = "";
    }
  }

  public openEstimatedVSReceiptDialog(issueNo): void {
    let dialogRef = this._dialog.open(EstimatedVsReceivedComponent, {
      width: "900px",
      data: {
        issueNo: issueNo
      },
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    });
  }

  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Issue No': this.dataSource.data[i].issue_no,
          'Issue Date': this.dataSource.data[i].issue_date,
          "Issue Type": this.dataSource.data[i].transfer_type,
          "Remarks": this.dataSource.data[i].remarks,
          'Status': this.dataSource.data[i].active,
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Production Issue",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Production Issue");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.issue_no);
        tempObj.push(this._datePipe.transform(e.issue_date, 'dd/MM/yyyy'));
        tempObj.push(e.transfer_type);
        tempObj.push(e.remarks);
        tempObj.push(e.active);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Issue No', 'Issue Date', 'Issue Type', 'Remarks', 'Status']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Production Issue' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Production Issue");
  }

  public addProductionIssue(): void {
    if (this.beforeSaveValidate()) {
      debugger
      let tempIssueDetails = [];
      let tempIssueEstimationDetails = [];
      if (this.objProductionIssue.issue_details[0].material_name == '') {
        let input = this.materialName.toArray();
        input[0].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog('Enter material name', 'Production Issue')
        return
      } else if (this.objProductionIssue.issue_details[0].byno == '') {
        let input = this.byno.toArray();
        input[0].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog('Enter byno', 'Production Issue')
        return
      }
      for (let i = 0; i < this.objProductionIssue.issue_details.length; i++) {
        if (this.objProductionIssue.issue_details[i].material_name == '' && this.objProductionIssue.issue_details[i].byno == '') {
          this.objProductionIssue.issue_details.splice(i, 1);
        } else if (this.objProductionIssue.issue_details[i].material_name !== '' && this.objProductionIssue.issue_details[i].material_id == 0) {
          let input = this.materialName.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter valid material name', 'Production Issue')
          return
        } else if (this.objProductionIssue.issue_details[i].material_name !== '' && this.objProductionIssue.issue_details[i].byno == '') {
          let input = this.byno.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter byno', 'Production Issue')
          return
        } else if (this.objProductionIssue.issue_details[i].byno !== '' && this.objProductionIssue.issue_details[i].selling_price == '') {
          let input = this.byno.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter valid byno', 'Production Issue')
          return
        } else if (this.objProductionIssue.issue_details[i].material_name !== '' && this.objProductionIssue.issue_details[i].byno !== '' && this.objProductionIssue.issue_details[i].length == '') {
          let input = this.length.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter length', 'Production Issue')
          return
        } else if (this.objProductionIssue.issue_details[i].material_name !== '' && this.objProductionIssue.issue_details[i].byno !== '' && this.objProductionIssue.issue_details[i].issued_qty == '') {
          let input = this.issuedQty.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter issued qty', 'Production Issue')
          return
        } else if (this.objProductionIssue.issue_details[i].material_name !== '' && this.objProductionIssue.issue_details[i].byno !== '' && this.objProductionIssue.issue_details[i].estimated_loss == '') {
          let input = this.estimatedLoss.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter estimated loss', 'Production Issue')
          return
        }
        if (this.objProductionIssue.issue_details[i] !== undefined) {
          tempIssueDetails.push({
            serial_no: i + 1,
            material_id: this.objProductionIssue.issue_details[i].material_id,
            inv_byno: this.objProductionIssue.issue_details[i].inv_byno,
            byno_serial: this.objProductionIssue.issue_details[i].byno_serial,
            byno_prod_serial: this.objProductionIssue.issue_details[i].byno_prod_serial,
            damaged: this.objProductionIssue.issue_details[i].damaged,
            length: this.objProductionIssue.issue_details[i].length,
            issued_qty: this.objProductionIssue.issue_details[i].issued_qty,
            estimated_loss: this.objProductionIssue.issue_details[i].estimated_loss
          });
        }
      }
      if (this.objProductionIssue.issue_estimation_details[0].product_name == '') {
        let input = this.productName.toArray();
        input[0].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog('Enter product name', 'Production Issue')
        return
      } else if (this.objProductionIssue.issue_estimation_details[0].product_name == '') {
        let input = this.byno.toArray();
        input[0].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog('Enter estimatedQty', 'Production Issue')
        return
      }
      for (let i = 0; i < this.objProductionIssue.issue_estimation_details.length; i++) {
        if (this.objProductionIssue.issue_estimation_details[i].product_name == '' && this.objProductionIssue.issue_estimation_details[i].estimated_qty == '') {
          this.objProductionIssue.issue_estimation_details.splice(i, 1);
        } else if (this.objProductionIssue.issue_estimation_details[i].product_name !== '' && this.objProductionIssue.issue_estimation_details[i].prod_size == '') {
          let input = this.productName.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter valid product name', 'Production Issue')
          return
        } else if (this.objProductionIssue.issue_estimation_details[i].product_name !== '' && this.objProductionIssue.issue_estimation_details[i].estimated_qty == '') {
          let input = this.estimatedQty.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter estimatedQty', 'Production Issue')
          return
        }
        if (this.objProductionIssue.issue_estimation_details[i] !== undefined) {
          tempIssueEstimationDetails.push({
            serial_no: i + 1,
            prod_id: this.objProductionIssue.issue_estimation_details[i].prod_id,
            prod_size: this.objProductionIssue.issue_estimation_details[i].prod_size,
            estimated_qty: this.objProductionIssue.issue_estimation_details[i].estimated_qty
          })
        }
      }
      let objData = {
        ProductionIssue: JSON.stringify([{
          wh_section_id: this._localStorage.getWhSectionId(),
          issue_no: this.objProductionIssue.issue_no,
          issue_date: this._datePipe.transform(this.objProductionIssue.issue_date, 'dd/MM/yyyy'),
          supplier_code: this.objProductionIssue.supplier_code,
          remarks: this.objProductionIssue.remarks,
          entered_by: +this._localStorage.intGlobalUserId(),
          issue_details: tempIssueDetails,
          issue_estimation_details: tempIssueEstimationDetails,
          company_section_id: +this._localStorage.getCompanySectionId()
        }]),
      }
      this._productionIssueService.addProductionIssue(objData).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New production issue record has been created", "Production Issue");
          this.componentVisibility = !this.componentVisibility;
          this.objLoad.supplier_code = this.objProductionIssue.supplier_code;
          this.objLoad.supplier_name = this.objProductionIssue.supplier_name;
          this.getProductionIssue();
          this.resetScreen();
        }
      });
    }
  }


  public addReconciliationUpdate(): void {
    // if (this.beforeSaveValidate()) {
    let objData = {
      ProductionIssue: JSON.stringify([{
        wh_section_id: this._localStorage.getWhSectionId(),
        issue_no: this.objReconciliation.issue_no,
        close: this.objReconciliation.closed,
        closing_remarks: this.objReconciliation.remarks,
        entered_by: +this._localStorage.intGlobalUserId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._productionIssueService.addProductionIssueReconciliation(objData).subscribe((result: any) => {
      if (result) {
        this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "This production issue closed", "Production Issue");
        this.isReconciliation = false;
        this.componentVisibility = !this.componentVisibility;
      }
    });
    //}
  }

  public getProductionIssueReconciliation(issueNo): void {
    let objData = {
      ProductionIssue: JSON.stringify([{
        wh_section_id: this._localStorage.getWhSectionId(),
        issue_no: issueNo,
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._productionIssueService.getProductionIssueReconciliation(objData).subscribe((result: any) => {
      if (result) {
        this.objReconciliation = JSON.parse(JSON.stringify(result[0]));
        this.objReconciliation.issue_date = this._datePipe.transform(this.objReconciliation.issue_date, 'dd/MM/yyyy');
        this.objReconciliation.reconciliation_details = JSON.parse(JSON.stringify(result));
        //this.closeFlag = this.objReconciliation.closed;
      }
    });
  }
}
