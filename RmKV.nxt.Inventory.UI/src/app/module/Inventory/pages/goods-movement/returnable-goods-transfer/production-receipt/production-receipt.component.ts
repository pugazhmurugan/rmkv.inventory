import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatDialogRef, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode } from 'src/app/common/models/common-model';
import { AccountsLookupService } from 'src/app/common/services/accounts-lookup/accounts-lookup.service';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ProductionMaterialLookupComponent } from 'src/app/common/shared/production-material-lookup/production-material-lookup.component';
import { ProductionProductLookupComponent } from 'src/app/common/shared/production-product-lookup/production-product-lookup.component';
import { ReasonComponent } from 'src/app/common/shared/reason/reason.component';
import { ProductionMaterials, ProductionReceiptDetails, ProductionReceiptFlags, ProductionReceiptLoad, ProductionReceipts } from 'src/app/module/Inventory/model/goodsMovement/returnableGoodsTransfer/production-receipt-model';
import { ProductionReceiptService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer/production-receipt/production-receipt.service';
import { ProductionMaterialsService } from 'src/app/module/Inventory/services/masters/production-materials/production-materials.service';
import { ProductionProductsService } from 'src/app/module/Inventory/services/masters/Production-Product/production-products.service';
import { InvoicesService } from 'src/app/module/Inventory/services/purchases/invoices/invoices.service';
import { IssueDetailComponent } from '../issue-detail/issue-detail.component';
import { SizeAndProductComponent } from '../size-and-product/size-and-product.component';
declare var jsPDF: any;

@Component({
  selector: 'app-production-receipt',
  templateUrl: './production-receipt.component.html',
  styleUrls: ['./production-receipt.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class ProductionReceiptComponent implements OnInit {

  componentVisibility: boolean = true;
  focusFlag: boolean = false;
  Date: any = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  invSupplierLookupList: any[] = [];
  loadSupplierLookupList: any[] = [];
  productionIssueNosList: any[] = [];
  productionProductsList: any[] = [];
  productionReceiptDetailsList: ProductionReceiptDetails[] = [];
  productionMaterialsList: ProductionMaterials[] = [];

  productNameList: any[] = [];
  materialNameList: any[] = [];

  flags: ProductionReceiptFlags = {
    isIgst: true,
    isComposite: false,
  }

  objModifyFlags: ProductionReceiptFlags = {
    isIgst: true,
    isComposite: false,
  }

  objUnChangedFlags: ProductionReceiptFlags = {
    isIgst: true,
    isComposite: false,
  }

  objAction: EditMode = {
    isEditing: false,
    isView: false
  }

  objUnChangedAction: EditMode = {
    isEditing: false,
    isView: false,
  }

  objProductionMaterials: ProductionMaterials = {
    material_id: 0,
    material_name: '',
    material_qty: 0
  }

  objUnChangedProductionMaterials: ProductionMaterials = {
    material_id: 0,
    material_name: '',
    material_qty: 0
  }

  modifyProductionMaterialsList: ProductionMaterials[] = [{
    material_id: 0,
    material_name: '',
    material_qty: 0,
  }];
  unChangedProductionMaterialsList: ProductionMaterials[] = [{
    material_id: 0,
    material_name: '',
    material_qty: 0,
  }];

  modifyProductionReceiptDetailsList: ProductionReceiptDetails[] = [{
    issue_no: '',
    product_id: 0,
    product_name: '',
    product_size: '',
    estimated_qty: 0,
    received_so_far: 0,
    received_qty: 0,
    conversion_charges: 0,
    hsn_code: '',
    cgst: 0,
    cgst_amount: 0,
    sgst: 0,
    sgst_amount: 0,
    igst: 0,
    igst_amount: 0,
    total_charges: '0.00',
    new_inv_byno: '',
    new_inv_byno_serial: '',
    new_inv_byno_prod_serial: '',
    production_products: [],
  }];

  unChangedProductionReceiptDetailsList: ProductionReceiptDetails[] = [{
    issue_no: '',
    product_id: 0,
    product_name: '',
    product_size: '',
    estimated_qty: 0,
    received_so_far: 0,
    received_qty: 0,
    conversion_charges: 0,
    hsn_code: '',
    cgst: 0,
    cgst_amount: 0,
    sgst: 0,
    sgst_amount: 0,
    igst: 0,
    igst_amount: 0,
    total_charges: '0.00',
    new_inv_byno: '',
    new_inv_byno_serial: '',
    new_inv_byno_prod_serial: '',
    production_products: [],
  }];


  objProductionReceiptDetails: ProductionReceiptDetails = {
    issue_no: '',
    product_id: 0,
    product_name: '',
    product_size: '',
    estimated_qty: 0,
    received_so_far: 0,
    received_qty: 0,
    conversion_charges: 0,
    hsn_code: '',
    cgst: 0,
    cgst_amount: 0,
    sgst: 0,
    sgst_amount: 0,
    igst: 0,
    igst_amount: 0,
    total_charges: '0.00',
    new_inv_byno: '',
    new_inv_byno_serial: '',
    new_inv_byno_prod_serial: '',
    production_products: [],
  }

  objUnChangedProductionReceiptDetails: ProductionReceiptDetails = {
    issue_no: '',
    product_id: 0,
    product_name: '',
    product_size: '',
    estimated_qty: 0,
    received_so_far: 0,
    received_qty: 0,
    conversion_charges: 0,
    hsn_code: '',
    cgst: 0,
    cgst_amount: 0,
    sgst: 0,
    sgst_amount: 0,
    igst: 0,
    igst_amount: 0,
    total_charges: '0.00',
    new_inv_byno: '',
    new_inv_byno_serial: '',
    new_inv_byno_prod_serial: '',
    production_products: [],
  }

  productionReceiptLoad: ProductionReceiptLoad = {
    from_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    to_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    status: 'All',
    supplier_code: '',
    supplier_name: ''
  }

  objProductionReceipt: ProductionReceipts = {
    production_receipt_no: 0,
    inv_supplier_code: '',
    inv_supplier_name: '',
    inv_supplier_gstn_no: '',
    invoice_date: new Date(),
    production_receipt_date: new Date(),
    invoice_no: '',
    taxable_value: '0.00',
    other_amount: '0.00',
    additions: '0.00',
    deductions: '0.00',
    cgst_total: '0.00',
    sgst_total: '0.00',
    igst_total: '0.00',
    round_off_total: '0.00',
    invoice_amount: '0.00',
    remarks: '',
  }

  objModifyProductionReceipt: ProductionReceipts = {
    production_receipt_no: 0, inv_supplier_code: '',
    inv_supplier_name: '',
    inv_supplier_gstn_no: '',
    invoice_date: new Date(),
    production_receipt_date: new Date(),
    invoice_no: '',
    taxable_value: '0.00',
    other_amount: '0.00',
    additions: '0.00',
    deductions: '0.00',
    cgst_total: '0.00',
    sgst_total: '0.00',
    igst_total: '0.00',
    round_off_total: '0.00',
    invoice_amount: '0.00',
    remarks: '',
  }

  objUnChangedProductionReceipt: ProductionReceipts = {
    production_receipt_no: 0, inv_supplier_code: '',
    inv_supplier_name: '',
    inv_supplier_gstn_no: '',
    invoice_date: new Date(),
    production_receipt_date: new Date(),
    invoice_no: '',
    taxable_value: '0.00',
    other_amount: '0.00',
    additions: '0.00',
    deductions: '0.00',
    cgst_total: '0.00',
    sgst_total: '0.00',
    igst_total: '0.00',
    round_off_total: '0.00',
    invoice_amount: '0.00',
    remarks: '',
  }

  receiptColumns: any = [
    { display: 'sNo', editable: false },
    { display: 'issueNo', editable: true },
    { display: 'productName', editable: true },
    { display: "productSize", editable: false },
    { display: "estimatedQty", editable: false },
    { display: "receivedSoFar", editable: false },
    { display: "receivedQty", editable: true },
    { display: "conversionCharges", editable: true },
    { display: "hsnCode", editable: true },
    { display: "igst", editable: false },
    { display: "cgst", editable: false },
    { display: "sgst", editable: false },
    { display: "totalCharges", editable: false },
    { display: "action", editable: true },
  ]

  materialColumns: any = [
    { display: 'sNo', editable: false },
    { display: 'materialName', editable: true },
    { display: 'materialQty', editable: true },
    { display: "matAction", editable: true },
  ]

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  fromDate1: any = new Date();
  toDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();
  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Receipt_No", "Receipt_Date", "Supplier_Name", "Invoice_No", "Invoice_Amount", "Payment_Status", "Status", "Action"];

  @ViewChildren('issueNo') issueNo: ElementRef | any;
  @ViewChildren('productName') productName: ElementRef | any;
  @ViewChildren('productSize') productSize: ElementRef | any;
  @ViewChildren('estimatedQty') estimatedQty: ElementRef | any;
  @ViewChildren('receivedSoFar') receivedSoFar: ElementRef | any;
  @ViewChildren('receivedQty') receivedQty: ElementRef | any;
  @ViewChildren('conversionCharges') conversionCharges: ElementRef | any;
  @ViewChildren('hsnCode') hsnCode: ElementRef | any;
  @ViewChildren('igst') igst: ElementRef | any;
  @ViewChildren('cgst') cgst: ElementRef | any;
  @ViewChildren('sgst') sgst: ElementRef | any;
  @ViewChildren('totalCharges') totalCharges: ElementRef | any;
  @ViewChildren('action') action: ElementRef | any;

  @ViewChildren('materialName') materialName: ElementRef | any;
  @ViewChildren('materialQty') materialQty: ElementRef | any;
  @ViewChildren('matAction') matAction: ElementRef | any;

  @ViewChildren('invSupplier') invSupplier: ElementRef | any;

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    private _datePipe: DatePipe,
    private _matDialog: MatDialog,
    public _dialog: MatDialog,
    private _decimalPipe: DecimalPipe,
    private _keyPressEvents: KeyPressEvents,
    private _gridKeyEvents: GridKeyEvents,
    private _invoicesService: InvoicesService,
    private _prodMaterialService: ProductionMaterialsService,
    private _confirmationDialog: ConfirmationDialogComponent,
    public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    private _accountsLookupService: AccountsLookupService,
    private _productionReceiptService: ProductionReceiptService,
    private _productionProductsService: ProductionProductsService,
    public _excelService: ExcelService
  ) {
    this.productionReceiptLoad.from_date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
    this.getSupplierLookupList();
    this.getProductNameList();
    this.getMaterialNameList();
  }

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number, column: string, listOfColumn: string): void {
    debugger;
    switch (event.keyCode) {
      case 13: // Enter Key
        if (this.checkValidDetails(column, listOfColumn, rowIndex, colIndex)) {
          let response = this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this[column], this, this[listOfColumn]);
          if (response && this.checkValidDetails(column, listOfColumn, rowIndex, colIndex)) {
            if (column === 'receiptColumns')
              this.addNewRowToReceiptDetails();
            else if (column === 'materialColumns')
              this.addNewRowToMaterialDetails();
          }
        }
        break;

      case 37: // Arrow Left
        if (this.checkValidDetails(column, listOfColumn, rowIndex, colIndex))
          this._gridKeyEvents.focusLeft(rowIndex, colIndex, this[column], this, this[listOfColumn]);
        break;

      case 38: // Arrow Up
        if (this.checkValidDetails(column, listOfColumn, rowIndex, colIndex))
          this._gridKeyEvents.focusUp(rowIndex, colIndex, this[column], this);
        break;

      case 39: // Arrow Right
        if (this.checkValidDetails(column, listOfColumn, rowIndex, colIndex))
          this._gridKeyEvents.focusRight(rowIndex, colIndex, this[column], this, this[listOfColumn]);
        break;

      case 40: // Arrow Down
        if (this.checkValidDetails(column, listOfColumn, rowIndex, colIndex))
          this._gridKeyEvents.focusDown(rowIndex, colIndex, this[column], this, this[listOfColumn]);
        break;
    }
  }

  ngOnInit() {
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDateFrom(this.productionReceiptLoad.from_date, this.fromDate1, this.minDate, this.maxDate, this.toDate1);
    this.productionReceiptLoad.from_date = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDateTo(this.productionReceiptLoad.to_date, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate, this.fromDate1);
    this.productionReceiptLoad.to_date = date[0];
    this.toDate1 = date[1];
  }

  /**************************************************** Lookup Functions ********************************************************/

  private getSupplierLookupList(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._accountsLookupService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      debugger;
      if (result) {
        this.loadSupplierLookupList = JSON.parse(result);
        this.invSupplierLookupList = JSON.parse(result);

        this.loadSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
        this.invSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
      }
    });
  }

  private getProductNameList(): void {
    this._productionProductsService.getProductionProductLookup().subscribe((result: any) => {
      if (result && result.length > 0) {
        this.productNameList = result;
        console.log(this.productNameList, 'prod');
      }
    });
  }

  public openLoadSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateLoadSupplierCode()) {
      this.openLoadSupplierLookupDialog();
    }
  }

  private openLoadSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "550px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.productionReceiptLoad.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      this.matTableConfig();
      if (result) {
        debugger;
        this.productionReceiptLoad.supplier_code = result.account_code.toString().trim();
        this.productionReceiptLoad.supplier_name = result.account_name.toString().trim();
      } else {
        this.productionReceiptLoad.supplier_code = '';
        this.productionReceiptLoad.supplier_name = '';
      }
    });
  }

  public onEnterLoadSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
    };
    this.matTableConfig();
    supplierLookupList = this.loadSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.productionReceiptLoad.supplier_code.toString().trim().toLowerCase());
    this.productionReceiptLoad.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.productionReceiptLoad.supplier_code.toString().trim();
    this.productionReceiptLoad.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
  }

  public checkValidLoadSupplierName(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.productionReceiptLoad.supplier_code.toString().trim() !== '' && this.productionReceiptLoad.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier code", "Production Receipt", focus);
    }
  }

  public validateLoadSupplierCode(): boolean {
    let index = this.loadSupplierLookupList.findIndex(element => element.account_code.toString().trim() === this.productionReceiptLoad.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public openInvSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    this.productionIssueNosList = [];
    this.productionReceiptDetailsList = JSON.parse(JSON.stringify(this.unChangedProductionReceiptDetailsList));
    if (event.keyCode === 13 && !this.validateInvSupplierCode()) {
      this.openInvSupplierLookupDialog();
    }
  }

  private openInvSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "750px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objProductionReceipt.inv_supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objProductionReceipt.inv_supplier_code = result.account_code.toString().trim();
        this.objProductionReceipt.inv_supplier_name = result.account_name.toString().trim();
        this.objProductionReceipt.inv_supplier_gstn_no = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(result.gstn_no) !== -1 ? '' : result.gstn_no;
      } else {
        this.objProductionReceipt.inv_supplier_code = '';
        this.objProductionReceipt.inv_supplier_name = '';
        this.objProductionReceipt.inv_supplier_gstn_no = '';
      }
      this.setTypeOfGst();
    });
  }

  public onEnterInvSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      gstn_no: ""
    };
    this.productionIssueNosList = [];
    this.productionReceiptDetailsList = JSON.parse(JSON.stringify(this.unChangedProductionReceiptDetailsList));
    supplierLookupList = this.invSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objProductionReceipt.inv_supplier_code.toString().trim().toLowerCase());
    this.objProductionReceipt.inv_supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objProductionReceipt.inv_supplier_code.toString().trim();
    this.objProductionReceipt.inv_supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
    this.objProductionReceipt.inv_supplier_gstn_no = supplierLookupList && supplierLookupList.gstn_no ? supplierLookupList.gstn_no : '';
    this.setTypeOfGst();
  }

  public checkValidInvSupplierName(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objProductionReceipt.inv_supplier_code.toString().trim() !== '' && this.objProductionReceipt.inv_supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier name", "Stone/Polish Order Receipt", focus);
    }
  }

  public validateInvSupplierCode(): boolean {
    let index = this.invSupplierLookupList.findIndex(element => element.account_code.toString().trim() === this.objProductionReceipt.inv_supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public openProductLookup(event: any, i: any): void {
    if (event.keyCode === 13 && !this.productionReceiptDetailsList[i].product_size) {
      this.openProductLookupDialog(i);
    }
  }

  private openProductLookupDialog(i: number): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(ProductionProductLookupComponent, {
      width: "500px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.productionReceiptDetailsList[i].product_name
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      debugger;
      this.focusFlag = false;
      if (result) {
        this.productionReceiptDetailsList[i].product_id = result.prod_id;
        this.productionReceiptDetailsList[i].product_name = result.prod_name.toString().trim();
        this.productionReceiptDetailsList[i].product_size = result.prod_size;
        this.checkProductAlreadyEntered(i);
        this.getEstimatedProductValues(i);
      } else {
        this.productionReceiptDetailsList[i].product_id = 0;
        this.productionReceiptDetailsList[i].product_name = '';
        this.productionReceiptDetailsList[i].product_size = '';
        this.productionReceiptDetailsList[i].estimated_qty = 0;
        this.productionReceiptDetailsList[i].received_so_far = 0;
      }
    });
  }

  public onEnterProductLookupDetail(i: any): void {
    debugger;
    let productLookupList: any = {
      prod_id: 0,
      prod_name: "",
      prod_size: ""
    };
    productLookupList = this.productNameList.find(product => product.prod_name.toString().toLowerCase().trim() === this.productionReceiptDetailsList[i].product_name.toString().trim().toLowerCase());
    this.productionReceiptDetailsList[i].product_id = productLookupList && productLookupList.prod_id ? productLookupList.prod_id : 0;
    this.productionReceiptDetailsList[i].product_name = productLookupList && productLookupList.prod_name ? productLookupList.prod_name : this.productionReceiptDetailsList[i].product_name;
    this.productionReceiptDetailsList[i].product_size = productLookupList && productLookupList.prod_size ? productLookupList.prod_size : '';
    this.productionReceiptDetailsList[i].estimated_qty = productLookupList && productLookupList.prod_size ? this.productionReceiptDetailsList[i].estimated_qty : 0;
    this.productionReceiptDetailsList[i].received_so_far = productLookupList && productLookupList.prod_size ? this.productionReceiptDetailsList[i].received_so_far : 0;
    this.checkProductAlreadyEntered(i);
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.productionReceiptDetailsList[i].product_name) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.productionReceiptDetailsList[i].product_id) === -1)
      this.getEstimatedProductValues(i);
  }

  private checkProductAlreadyEntered(index: number): void {
    let alreadyFlag = false;
    let productName = '';
    let productSize = '';
    for (let i = 0; i < this.productionReceiptDetailsList.length; i++) {
      if (i !== index && this.productionReceiptDetailsList[i].product_name === this.productionReceiptDetailsList[index].product_name && this.productionReceiptDetailsList[i].product_size === this.productionReceiptDetailsList[index].product_size) {
        productName = this.productionReceiptDetailsList[i].product_name;
        productSize = this.productionReceiptDetailsList[i].product_size;
        alreadyFlag = true;
        break;
      }
    } if (alreadyFlag) {
      this._confirmationDialog.openAlertDialog("This product '" + productName + "' already exists on current list", "Production Receipt");
      this.productionReceiptDetailsList[index].product_id = 0;
      this.productionReceiptDetailsList[index].product_name = "";
      this.productionReceiptDetailsList[index].product_size = "";
    }
  }

  public checkIsvalidProductName(i: number, event: KeyboardEvent): void {
    if (event.keyCode !== 13 && !this.focusFlag && this.productionReceiptDetailsList[i].product_name.toString().trim() !== '' && +this.productionReceiptDetailsList[i].product_id === 0)
      this.openInvalidAlertDialog('Invalid product name', 'Production Receipt', 'productName', i);
  }

  public validateProductName(i: number): boolean {
    let index = this.productNameList.findIndex(element => element.prod_name.toString().trim() === this.productionReceiptDetailsList[i].product_name.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  private getMaterialNameList(): void {
    this._prodMaterialService.getProductionMaterialDetails().subscribe((result: any) => {
      if (result && result.length > 0) {
        this.materialNameList = result;
      }
    });
  }

  public openMaterialLookup(event: any, i: any): void {
    debugger;
    if (event.keyCode === 13 && this.checkValidMaterialName(i)) {
      this.openMaterialLookupDialog(i);
    }
  }

  private openMaterialLookupDialog(i: number): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(ProductionMaterialLookupComponent, {
      width: "500px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.productionMaterialsList[i].material_name
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      debugger;
      this.focusFlag = false;
      if (result) {
        this.productionMaterialsList[i].material_id = result.material_id;
        this.productionMaterialsList[i].material_name = result.material_name.toString().trim();
        this.checkMaterialAlreadyEntered(i);
      } else {
        this.productionMaterialsList[i].material_id = 0;
        this.productionMaterialsList[i].material_name = '';
      }
    });
  }

  private checkValidMaterialName(i: number): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.productionMaterialsList[i].material_name) === -1) {
      let index = this.materialNameList.findIndex(x => x.material_name.toString().trim() === this.productionMaterialsList[i].material_name.toString().trim());
      return index !== -1 ? false : true;
    } else return true;
  }

  public checkIsvalidMaterialName(i: number, event: KeyboardEvent): void {
    if (event.keyCode !== 13 && !this.focusFlag && this.productionMaterialsList[i].material_name.toString().trim() !== '' && +this.productionMaterialsList[i].material_id === 0)
      this.openInvalidAlertDialog('Invalid material name', 'Production Receipt', 'materialName', i);
  }

  public onEnterMaterialLookupDetail(i: any): void {
    let materialLookupList: any = {
      material_id: 0,
      material_name: "",
    };
    materialLookupList = this.materialNameList.find(material => material.material_name.toLowerCase().trim() === this.productionMaterialsList[i].material_name.toString().trim().toLowerCase());
    this.productionMaterialsList[i].material_id = materialLookupList && materialLookupList.material_id ? materialLookupList.material_id : 0;
    this.productionMaterialsList[i].material_name = materialLookupList && materialLookupList.material_name ? materialLookupList.material_name : this.productionMaterialsList[i].material_name;
    this.checkMaterialAlreadyEntered(i);
  }

  private checkMaterialAlreadyEntered(index: number): void {
    let alreadyFlag = false;
    let materialName = '';
    for (let i = 0; i < this.productionMaterialsList.length; i++) {
      if (i !== index && this.productionMaterialsList[i].material_name === this.productionMaterialsList[index].material_name) {
        materialName = this.productionMaterialsList[i].material_name;
        alreadyFlag = true;
        break;
      }
    } if (alreadyFlag) {
      this._confirmationDialog.openAlertDialog("This material '" + materialName + "' already exists on current list", "Production Issue");
      this.productionMaterialsList[index].material_id = 0;
      this.productionMaterialsList[index].material_name = "";
    }
  }

  /***************************************************** CRUD Function ******************************************************/

  public loadProductionReceipts(): void {
    if (this.beforeLoadValidate()) {
      let objLoad: any = {
        ProductionReceipt: JSON.stringify([{
          from_date: this.productionReceiptLoad.from_date,
          to_date: this.productionReceiptLoad.to_date,
          wh_section_id: +this._localStorage.getWhSectionId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          supplier_code: this.productionReceiptLoad.supplier_code.toString().trim(),
          status: this.productionReceiptLoad.status.toString().trim()
        }])
      }
      this._productionReceiptService.getProductionReceipts(objLoad).subscribe((result: any[]) => {
        result ? this.matTableConfig(JSON.parse(JSON.stringify(result))) : this._confirmationDialog.openAlertDialog('No records found', 'Stone/Polish Order Receipt');
      });
    }
  }

  public getPendingProductionIssueNos(value: number): void {
    if (this.beforeLoadIssueNosValidate()) {
      let objLoad: any = {
        ProductionReceipt: JSON.stringify([{
          supplier_code: this.objProductionReceipt.inv_supplier_code.toString().trim(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          wh_section_id: +this._localStorage.getWhSectionId()
        }])
      }
      this._productionReceiptService.getPendingProductionIssueNos(objLoad).subscribe((result: any[]) => {
        if (result) {
          if (value == 0) {
            this.productionIssueNosList = [];
          }
          this.productionIssueNosList = JSON.parse(JSON.stringify(result));
          this.productionIssueNosList.forEach(ele => {
            ele.isSelected = false;
          });
          console.log(result, "Issue Nos List");
        }
        else
          this._confirmationDialog.openAlertDialog('No pending issues found', 'Stone/Polish Order Receipt');
      });
    }
  }

  public getPendingProductionProducts(i: number, isModify?: boolean): void {
    if (this.checkValidIssueNo(i)) {
      let objLoad: any = {
        ProductionReceipt: JSON.stringify([{
          company_section_id: +this._localStorage.getCompanySectionId(),
          wh_section_id: +this.getWhSectionIdByIssueNo(i),
          issue_no: +this.productionReceiptDetailsList[i].issue_no
        }])
      }
      this._productionReceiptService.getProductionReceiptProductLookup(objLoad).subscribe((result: any[]) => {
        if (result) {
          debugger;
          this.productionReceiptDetailsList[i].production_products = JSON.parse(JSON.stringify(result));
          // console.log(this.productionReceiptDetailsList[i].production_products, 'Production products');
          if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.productionReceiptDetailsList[i].product_name) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.productionReceiptDetailsList[i].product_size) === -1)
            this.getEstimatedProductValues(i);
          if (isModify) {
            this.modifyProductionReceiptDetailsList[i].production_products = JSON.parse(JSON.stringify(result));
            if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.productionReceiptDetailsList[i].product_name) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.productionReceiptDetailsList[i].product_size) === -1)
              this.getEstimatedProductValues(i, true);
          }
        }
      });
    }
  }

  private getWhSectionIdByIssueNo(i: number): number {
    if (this.productionIssueNosList.length > 0)
      return this.productionIssueNosList[this.productionIssueNosList.findIndex(x => +x.issue_no === +this.productionReceiptDetailsList[i].issue_no)].issue_wh_section_id;
  }

  private getEstimatedProductValues(i: number, isModify?: boolean): void {
    let element = this.productionReceiptDetailsList[i];
    if (element.production_products.length > 0) {
      this.productionReceiptDetailsList[i].estimated_qty = element.production_products[element.production_products.findIndex(x => +x.issue_no === +element.issue_no &&
        x.product_name.toString().trim() === element.product_name.toString().trim() && x.product_size.toString().trim() === element.product_size.toString().trim())].estimated_qty;
      this.productionReceiptDetailsList[i].received_so_far = element.production_products[element.production_products.findIndex(x => +x.issue_no === +element.issue_no &&
        x.product_name.toString().trim() === element.product_name.toString().trim() && x.product_size.toString().trim() === element.product_size.toString().trim())].received_so_far;
    }
    if (isModify) {
      let modifyElement: any = this.modifyProductionReceiptDetailsList[i];
      modifyElement.estimated_qty = JSON.parse(JSON.stringify(element.estimated_qty));
      modifyElement.received_so_far = JSON.parse(JSON.stringify(element.received_so_far));
    }
  }

  public setTypeOfGst(): void {
    let supplierGstNo: string = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objProductionReceipt.inv_supplier_gstn_no) !== -1 ? '' : this.objProductionReceipt.inv_supplier_gstn_no.substr(0, 2);
    if (+supplierGstNo === +this._localStorage.getStateCode())
      this.flags.isIgst = false;
    else if (+supplierGstNo !== +this._localStorage.getStateCode())
      this.flags.isIgst = true;
    this.resetGstValues();
  }

  private resetGstValues(): void {
    for (let i = 0; i < this.productionReceiptDetailsList.length; i++) {
      this.productionReceiptDetailsList[i].sgst = 0;
      this.productionReceiptDetailsList[i].sgst_amount = 0;
      this.productionReceiptDetailsList[i].cgst = 0;
      this.productionReceiptDetailsList[i].cgst_amount = 0;
      this.productionReceiptDetailsList[i].igst = 0;
      this.productionReceiptDetailsList[i].igst_amount = 0;
      this.productionReceiptDetailsList[i].hsn_code = '';
      this.calculateTotalCharges(i);
    }
  }

  public getGstByHsnCode(i: number): void {
    if (!this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.productionReceiptDetailsList[i].hsn_code) === -1) {
      let objGet = {
        GstByHSN: JSON.stringify([{
          supplier_gstn: this.objProductionReceipt.inv_supplier_gstn_no.toString().trim(),
          company_section_id: this._localStorage.getCompanySectionId(),
          hsn: this.productionReceiptDetailsList[i].hsn_code,
          supplier_inv_date: this._datePipe.transform(this.objProductionReceipt.invoice_date, 'dd/MM/yyyy'),
          cost_price: this.productionReceiptDetailsList[i].conversion_charges
        }])
      }
      this._invoicesService.getGstByHSNCode(objGet).subscribe((result: any) => {
        if (result) {
          debugger;
          this.productionReceiptDetailsList[i].sgst = JSON.parse(JSON.stringify(result))[0].sgst;
          this.productionReceiptDetailsList[i].cgst = JSON.parse(JSON.stringify(result))[0].cgst;
          this.productionReceiptDetailsList[i].igst = JSON.parse(JSON.stringify(result))[0].igst;
          this.calculateTotalCharges(i);
        } else {
          this.productionReceiptDetailsList[i].igst = 0;
          this.productionReceiptDetailsList[i].cgst = 0;
          this.productionReceiptDetailsList[i].sgst = 0;
          this.openInvalidAlertDialog('Invalid hsn code', 'Production Receipt', 'hsnCode', i);
        }
      });
    } else {
      this.productionReceiptDetailsList[i].igst = 0;
      this.productionReceiptDetailsList[i].cgst = 0;
      this.productionReceiptDetailsList[i].sgst = 0;
    }
  }

  /////////////////// Calculations //////////////////

  public calculateTotalCharges(i: number): void {
    let element = this.productionReceiptDetailsList[i];
    let gstTotal: number = this.getGstTotal(i, +element.conversion_charges);
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN'].indexOf(+element.conversion_charges) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN'].indexOf(+gstTotal) === -1) {
      element.total_charges = +element.conversion_charges + +gstTotal;
    }
    this.getTaxableValueTotal();
    if (this.flags.isIgst)
      this.getIgstGrandTotal();
    else if (!this.flags.isIgst) {
      this.getCgstGrandTotal();
      this.getSgstGrandTotal();
    }
    this.getTotalInvoiceAmount();
    this.convertToDecimal(i);
  }

  private getGstTotal(i: number, conversionCharges?: number): number {
    if (this.flags.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.productionReceiptDetailsList[i].igst) === -1) {
        this.productionReceiptDetailsList[i].igst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.productionReceiptDetailsList[i].igst) === -1 ? +conversionCharges * (+this.productionReceiptDetailsList[i].igst / 100) : 0;
        return +this.productionReceiptDetailsList[i].igst_amount;
      }
    } else if (!this.flags.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.productionReceiptDetailsList[i].cgst) === -1 &&
        [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.productionReceiptDetailsList[i].sgst) === -1) {
        this.productionReceiptDetailsList[i].cgst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.productionReceiptDetailsList[i].cgst) === -1 ? +conversionCharges * (+this.productionReceiptDetailsList[i].cgst / 100) : 0;
        this.productionReceiptDetailsList[i].sgst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.productionReceiptDetailsList[i].sgst) === -1 ? +conversionCharges * (+this.productionReceiptDetailsList[i].sgst / 100) : 0;
        return +(+this.productionReceiptDetailsList[i].cgst_amount + +this.productionReceiptDetailsList[i].sgst_amount);
      }
    }
  }

  public getTaxableValueTotal(): void {
    this.objProductionReceipt.taxable_value = this.productionReceiptDetailsList.map(t => (+t.conversion_charges)).reduce((acc, value) => acc + value, 0);
  }

  private getIgstGrandTotal(): void {
    this.objProductionReceipt.igst_total = 0;
    this.objProductionReceipt.igst_total = this.productionReceiptDetailsList.map(t => +t.igst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getCgstGrandTotal(): void {
    this.objProductionReceipt.cgst_total = 0;
    this.objProductionReceipt.cgst_total = this.productionReceiptDetailsList.map(t => +t.cgst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getSgstGrandTotal(): void {
    this.objProductionReceipt.sgst_total = 0;
    this.objProductionReceipt.sgst_total = this.productionReceiptDetailsList.map(t => +t.sgst_amount).reduce((acc, value) => acc + value, 0);
  }

  public getTotalInvoiceAmount(editableColumn?: string): number {
    let invoiceAmount: number = this.productionReceiptDetailsList.map(t => (+t.total_charges)).reduce((acc, value) => acc + value, 0);
    this.objProductionReceipt.invoice_amount = (+invoiceAmount) + +this.objProductionReceipt.additions - +this.objProductionReceipt.deductions;

    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objProductionReceipt.round_off_total) === -1) {
      if (this.objProductionReceipt.round_off_total.toString().includes('.')) {
        if (+(this.objProductionReceipt.round_off_total.toString().split('.')[1]) > 0) {
          this.objProductionReceipt.invoice_amount = +this.objProductionReceipt.invoice_amount + +(this.objProductionReceipt.round_off_total);
        } else if (+(this.objProductionReceipt.round_off_total.toString().split('.')[1]) < 0) {
          this.objProductionReceipt.invoice_amount = +this.objProductionReceipt.invoice_amount - +(this.objProductionReceipt.round_off_total);
        } else if (+(this.objProductionReceipt.round_off_total.toString().split('.')[1]) === 0) {
          this.objProductionReceipt.invoice_amount = +this.objProductionReceipt.invoice_amount + 0;
        }
      } else if (+(this.objProductionReceipt.round_off_total.toString())) {
        this.objProductionReceipt.invoice_amount = +this.objProductionReceipt.invoice_amount + +(this.objProductionReceipt.round_off_total);
      }
    }
    this.convertTotalsToDecimal(editableColumn);
    return +this.objProductionReceipt.invoice_amount;
  }

  private convertToDecimal(i: number): void {
    this.productionReceiptDetailsList[i].cgst = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.productionReceiptDetailsList[i].cgst.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.productionReceiptDetailsList[i].cgst, '1.2-2')).replace(/,/g, '') : "0.00";
    this.productionReceiptDetailsList[i].sgst = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.productionReceiptDetailsList[i].sgst.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.productionReceiptDetailsList[i].sgst, '1.2-2')).replace(/,/g, '') : "0.00";
    this.productionReceiptDetailsList[i].igst = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.productionReceiptDetailsList[i].igst.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.productionReceiptDetailsList[i].igst, '1.2-2')).replace(/,/g, '') : "0.00";
    this.productionReceiptDetailsList[i].total_charges = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.productionReceiptDetailsList[i].total_charges.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.productionReceiptDetailsList[i].total_charges, '1.2-2')).replace(/,/g, '') : "0.00";
    this.convertTotalsToDecimal('');
  }

  public convertTotalsToDecimal(editableColumn: string): void {
    this.objProductionReceipt.taxable_value = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objProductionReceipt.taxable_value.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objProductionReceipt.taxable_value, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objProductionReceipt.invoice_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objProductionReceipt.invoice_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objProductionReceipt.invoice_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objProductionReceipt.cgst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objProductionReceipt.cgst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objProductionReceipt.cgst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objProductionReceipt.sgst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objProductionReceipt.sgst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objProductionReceipt.sgst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objProductionReceipt.igst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objProductionReceipt.igst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objProductionReceipt.igst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'OtherAmount')
      this.objProductionReceipt.other_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objProductionReceipt.other_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objProductionReceipt.other_amount, '1.0-0')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'Additions')
      this.objProductionReceipt.additions = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objProductionReceipt.additions.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objProductionReceipt.additions, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'Deductions')
      this.objProductionReceipt.deductions = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objProductionReceipt.deductions.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objProductionReceipt.deductions, '1.2-2')).replace(/,/g, '') : "0.00";
    // this.objInvoices.round_off_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objInvoices.round_off_total) === -1 ? (this._decimalPipe.transform(+this.objInvoices.round_off_total, '1.0-0')).replace(/,/g, '') : "0.00";
  }

  ///////////////////// End of Calculations //////////////////////

  public addProductionReceipt(): void {
    if (this.beforeSaveValidate()) {
      debugger;
      let objSave: any = {
        ProductionReceipt: JSON.stringify([{
          wh_section_id: this._localStorage.getWhSectionId(),
          receipt_no: this.objProductionReceipt.production_receipt_no,
          receipt_date: this._datePipe.transform(this.objProductionReceipt.production_receipt_date, 'dd/MM/yyyy'),
          inv_supplier_code: this.objProductionReceipt.inv_supplier_code.toString().trim(),
          inv_no: this.objProductionReceipt.invoice_no.toString().trim(),
          inv_date: this._datePipe.transform(this.objProductionReceipt.invoice_date, 'dd/MM/yyyy'),
          composite: this.flags.isComposite,
          taxable_amount: +this.objProductionReceipt.taxable_value,
          other_amount: +this.objProductionReceipt.other_amount,
          misc_addition_amt: +this.objProductionReceipt.additions,
          misc_deduction_amt: +this.objProductionReceipt.deductions,
          cgst_amount: +this.objProductionReceipt.cgst_total,
          sgst_amount: +this.objProductionReceipt.sgst_total,
          igst_amount: +this.objProductionReceipt.igst_total,
          round_off_amount: +this.objProductionReceipt.round_off_total,
          inv_amount: +this.objProductionReceipt.invoice_amount,
          remarks: [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objProductionReceipt.remarks) === -1 ? this.objProductionReceipt.remarks.toString().trim() : '',
          file_path: [{ file_path1: '', file_path2: '', file_path3: '', file_path4: '' }],
          entered_by: +this._localStorage.intGlobalUserId(),
          details: this.getReceiptDetailsToAdd(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          material_details: this.getMaterialDetailsToAdd(),
          // supplier_gstn: this.objProductionReceipt.inv_supplier_gstn_no.toString().trim(),
        }])
      }
      debugger;
      this._productionReceiptService.addProductionReceipts(objSave).subscribe((result: boolean) => {
        if (result) {
          this._confirmationDialog.openAlertDialog(this.objAction.isEditing ? 'Changes have been saved' : 'New production receipt has been created', 'Production Receipt');
          this.afterAddProductionReceipt();
        }
      });
    }
  }

  private afterAddProductionReceipt(): void {
    this.productionReceiptLoad.supplier_code = this.objProductionReceipt.inv_supplier_code.toString().trim();
    this.productionReceiptLoad.supplier_name = this.objProductionReceipt.inv_supplier_name.toString().trim();
    this.productionReceiptLoad.status = 'All';
    this.loadProductionReceipts();
    this.componentVisibility = !this.componentVisibility;
  }

  private getWhSectionId(issueNo: number): number {
    return this.productionIssueNosList[this.productionIssueNosList.findIndex(x => +x.issue_no === +issueNo)].issue_wh_section_id;
  }

  private getReceiptDetailsToAdd(): any[] {
    let tempArr: any[] = [];
    debugger;
    for (let i = 0; i < this.productionReceiptDetailsList.length; i++) {
      let element: any = this.productionReceiptDetailsList[i];
      if (this.checkIsValidRowToSave(element)) {
        tempArr.push({
          serial_no: i + 1,
          issue_wh_section_id: this.getWhSectionId(element.issue_no),
          issue_no: +element.issue_no,
          prod_id: +element.product_id,
          prod_size: element.product_size,
          estimated_quantity: element.estimated_qty,
          received_so_far: +element.received_so_far,
          received_quantity: +element.received_qty,
          conversion_charges: +element.conversion_charges,
          supplier_hsn: +element.hsn_code,
          igst: +element.igst,
          igst_amount: +element.igst_amount,
          cgst: +element.cgst,
          cgst_amount: +element.cgst_amount,
          sgst: +element.sgst,
          sgst_amount: +element.sgst_amount,
          cost_price: 0,
          margin_code: '',
          selling_price: 0,
          total_charges: +element.total_charges,
          new_inv_byno: element.new_inv_byno,
          new_inv_byno_serial: element.new_inv_byno_serial,
          new_inv_byno_prod_serial: element.new_inv_byno_prod_serial

        });
      }
    }
    return tempArr;
  }

  private getMaterialDetailsToAdd(): any[] {
    let tempArr: any[] = [];
    debugger;
    for (let i = 0; i < this.productionMaterialsList.length; i++) {
      let element: any = this.productionMaterialsList[i];
      if (this.checkIsValidMaterialRowToSave(element)) {
        tempArr.push({
          mat_serial_no: i + 1,
          material_id: +element.material_id,
          returned_quantity: +element.material_qty,
        });
      }
    }
    return tempArr;
  }

  public modifyProductionReceipt(i: number, isEditing: boolean, isView: boolean): void {
    this.objAction = {
      isEditing: isEditing ? true : false,
      isView: isView ? true : false
    }
    let objFetch: any = {
      ProductionReceipt: JSON.stringify([{
        wh_section_id: +this.dataSource.data[i].wh_section_id,
        receipt_no: this.dataSource.data[i].receipt_no,
        company_section_id: this.dataSource.data[i].company_section_id
      }])
    }
    this._productionReceiptService.fetchProductionReceipts(objFetch).subscribe((result: any) => {
      if (result && result.returN_VALUE === 1) {
        console.log(result, 'Fetch Production Receipt');
        this.getSupplierLookupList();
        this.getProductNameList();
        this.getMaterialNameList();
        this.afterFetchProductionReceipt(result);
      }
    });
  }

  private afterFetchProductionReceipt(result: any): void {
    let tempObj: any = JSON.parse(result.records1)[0];
    this.flags.isComposite = JSON.parse(result.records1)[0].composite;
    this.objModifyFlags.isComposite = JSON.parse(result.records1)[0].composite;
    let tempReceiptArr: any[] = JSON.parse(result.records2) ? JSON.parse(result.records2) : [];
    let tempMaterialsArr: any[] = JSON.parse(result.records3) ? JSON.parse(result.records3) : [];
    this.objProductionReceipt = JSON.parse(result.records1)[0];
    this.productionReceiptDetailsList = JSON.parse(JSON.stringify(tempReceiptArr));
    this.productionMaterialsList = JSON.parse(JSON.stringify(tempMaterialsArr));
    for (let i = 0; i < this.productionReceiptDetailsList.length; i++) {
      this.getPendingProductionProducts(i, true);
      this.convertToDecimal(i);
    }
    if (this.productionReceiptDetailsList.length === 0)
      this.addNewRowToReceiptDetails();
    if (this.productionMaterialsList.length === 0)
      this.addNewRowToMaterialDetails();
    this.objModifyProductionReceipt = JSON.parse(JSON.stringify(this.objProductionReceipt));
    this.modifyProductionReceiptDetailsList = JSON.parse(JSON.stringify(this.productionReceiptDetailsList));
    this.modifyProductionMaterialsList = JSON.parse(JSON.stringify(this.productionMaterialsList));
    this.getPendingProductionIssueNos(0);
    this.componentVisibility = !this.componentVisibility;

  }

  public onClickDelete(index: any): any {
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this._dialogRef.componentInstance.confirmMessage = "Do you want to cancel this receipt no " + this.dataSource.data[index].receipt_no + " ?"
    this._dialogRef.componentInstance.componentName = "Production Receipt";
    return this._dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.openReasonDialog(index);
      }
      this._dialogRef = null;
    });
  }

  public openReasonDialog(index: number): void {
    let dialogRef = this._matDialog.open(ReasonComponent, {
      width: "75vw",
      panelClass: "custom-dialog-container",
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== null)
        this.removeProductionReceipt(index, result);
      this._dialogRef = null;
    });
  }

  private removeProductionReceipt(index: number, reason: string): void {
    debugger;
    let objSave: any = {
      ProductionReceipt: JSON.stringify([{
        wh_section_id: +this.dataSource.data[index].wh_section_id,
        receipt_no: +this.dataSource.data[index].receipt_no,
        cancel: false,
        entered_by: +this._localStorage.intGlobalUserId(),
        cancelled_reason: reason.toString().trim(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._productionReceiptService.removeProductionReceipts(objSave).subscribe((result: boolean) => {
      if (result) {
        this._confirmationDialog.openAlertDialog("Cancelled", "Production Receipt");
        this.loadProductionReceipts()
      }
    });
    this._dialogRef = null;
  }

  public newClick(): void {
    this.objAction = JSON.parse(JSON.stringify(this.objUnChangedAction));
    this.resetScreen();
    this.addNewRowToReceiptDetails();
    this.componentVisibility = !this.componentVisibility;
    setTimeout(() => {
      let input = this.invSupplier.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100)
  }

  private resetScreen(): void {
    if (this.objAction.isEditing) {
      this.flags = JSON.parse(JSON.stringify(this.objModifyFlags));
      this.productionReceiptDetailsList = JSON.parse(JSON.stringify(this.modifyProductionReceiptDetailsList));
      this.objProductionReceipt = JSON.parse(JSON.stringify(this.objModifyProductionReceipt));
      this.productionIssueNosList = [];
      this.getPendingProductionIssueNos(0);
      this.productionMaterialsList = JSON.parse(JSON.stringify(this.modifyProductionMaterialsList))
    } else if (!this.objAction.isEditing) {
      this.flags = JSON.parse(JSON.stringify(this.objUnChangedFlags));
      this.productionReceiptDetailsList = [];
      this.objProductionReceipt = JSON.parse(JSON.stringify(this.objUnChangedProductionReceipt));
      this.productionIssueNosList = [];
      this.productionMaterialsList = JSON.parse(JSON.stringify(this.unChangedProductionMaterialsList))
    }
  }

  public listClick(): void {
    this.objAction = JSON.parse(JSON.stringify(this.objUnChangedAction));
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade()) {
      this.openConfirmationDialog(exitFlag ? 'Changes will be lost, are you sure?' : 'Do you want to clear all the fields', exitFlag);
    } else if (exitFlag)
      this.listClick();
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Invoices";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.listClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  public addNewRowToReceiptDetails(): void {
    this.productionReceiptDetailsList.push(Object.assign({}, this.objProductionReceiptDetails));
    setTimeout(() => {
      let input = this.issueNo.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100);
  }

  public openRemoveConfirmationDialog(message: string, componentName: string, i: number): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = componentName;
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deleteRowFromReceiptDetails(i);
      dialogRef = null;
    });
  }

  public deleteRowFromReceiptDetails(i: number): void {
    this.productionReceiptDetailsList.splice(i, 1);
    setTimeout(() => {
      let input = this.issueNo.toArray();
      input[i].nativeElement.focus();
    }, 100);
  }

  public addNewRowToMaterialDetails(): void {
    this.productionMaterialsList.push(Object.assign({}, this.objProductionMaterials));
    setTimeout(() => {
      let input = this.materialName.toArray();
      input[input.length - 1].nativeElement.focus();
    }, 100);
  }


  public openRemoveMaterialConfirmationDialog(message: string, componentName: string, i: number): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = componentName;
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deleteRowFromMaterialDetails(i);
      dialogRef = null;
    });
  }

  public deleteRowFromMaterialDetails(i: number): void {
    this.productionMaterialsList.splice(i, 1);
    setTimeout(() => {
      let input = this.materialName.toArray();
      input[i].nativeElement.focus();
    }, 100);
  }

  public matTableConfig(tableRecords?: any[]): void {
    console.log(tableRecords, 'Table Records');
    this.dataSource = tableRecords ? new MatTableDataSource(tableRecords) : new MatTableDataSource([]);
  }

  public openSizeTableDialog(): void {
    let dialogRef = this._dialog.open(SizeAndProductComponent, {
      width: "700px",
      data: {
      },
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    });
  }

  public openIssueDialog(i: number): void {
    let dialogRef = this._dialog.open(IssueDetailComponent, {
      width: "1000px",
      data: {
        objIssueNo: JSON.parse(JSON.stringify(this.productionIssueNosList[i]))
      },
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    });
  }

  /**************************************************** Validations **************************************************/

  private beforeLoadValidate(): boolean {
    if (this.productionReceiptLoad.from_date.toString().trim().length !== 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialog.openAlertDialog('Invalid from date', 'Production Receipt');
      return false;
    } if (this.productionReceiptLoad.to_date.toString().trim().length !== 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialog.openAlertDialog('Invalid to date', 'Production Receipt');
      return false;
    } if (this.fromDate1 > this.toDate1) {
      document.getElementById('toDate').focus();
      this._confirmationDialog.openAlertDialog('From date must be less than to date', 'Production Receipt');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.productionReceiptLoad.status) !== -1) {
      document.getElementById('statuss').focus();
      this._confirmationDialog.openAlertDialog('Select status', 'Production Receipt');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.productionReceiptLoad.supplier_code) !== -1) {
      document.getElementById('loadSupplier').focus();
      this._confirmationDialog.openAlertDialog('Select supplier', 'Production Receipt');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.productionReceiptLoad.supplier_code) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.productionReceiptLoad.supplier_name) !== -1) {
      document.getElementById('loadSupplier');
      this._confirmationDialog.openAlertDialog('Invalid supplier', 'Production Receipt');
      return false;
    } else return true;
  }

  private beforeLoadIssueNosValidate(): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objProductionReceipt.inv_supplier_code) !== -1) {
      document.getElementById('invSupplier').focus();
      this._confirmationDialog.openAlertDialog('Select supplier', 'Stone/Polish Order Receipt');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objProductionReceipt.inv_supplier_code) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objProductionReceipt.inv_supplier_name) !== -1) {
      document.getElementById('invSupplier');
      this._confirmationDialog.openAlertDialog('Invalid supplier', 'Stone/Polish Order Receipt');
      return false;
    } else return true;
  }

  private beforeSaveValidate(): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objProductionReceipt.inv_supplier_code) !== -1) {
      this.openAlertDialog('Enter supplier code', 'Production Receipt', 'invSupplier');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objProductionReceipt.inv_supplier_code) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objProductionReceipt.inv_supplier_name) !== -1) {
      this.openAlertDialog('Invalid supplier code', 'Production Receipt', 'invSupplier');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objProductionReceipt.invoice_no) !== -1) {
      this.openAlertDialog('Enter invoice no', 'Production Receipt', 'invoiceNo');
      return false;
    } if (this.productionIssueNosList.length === 0) {
      this._confirmationDialog.openAlertDialog('Load pending issues', 'Production Receipt');
      return false;
    } if (!this.beforeSaveValidateReceiptDetails()) {
      return false;
    } if (!this.beforeSaveValidateMaterialDetails()) {
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objProductionReceipt.invoice_amount) !== -1) {
      this._confirmationDialog.openAlertDialog('Invoice amount cannot be 0', 'Production Receipt');
      return false
    } else return true;
  }

  private beforeSaveValidateReceiptDetails(): boolean {
    for (let i = 0; i < this.productionReceiptDetailsList.length; i++) {
      let element: any = this.productionReceiptDetailsList[i];
      if (JSON.stringify(element) !== JSON.stringify(this.objProductionReceiptDetails)) {
        if (this.productionReceiptDetailsList.length === 1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.issue_no) !== -1) {
          this.openInvalidAlertDialog('No records to save', 'Production Receipt', 'issueNo', i);
          return false;
        } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(element.issue_no) !== -1) {
          this.openInvalidAlertDialog('Enter issue no', 'Production Receipt', 'issueNo', i);
          return false;
        } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(element.product_name) !== -1) {
          this.openInvalidAlertDialog('Enter product name', 'Production Receipt', 'productName', i);
          return false;
        } if (!this.validateProductName(i)) {
          this.openInvalidAlertDialog('Invalid product name', 'Production Receipt', 'productName', i);
          return false;
        } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(element.received_qty) !== -1) {
          this.openInvalidAlertDialog('Enter received quantity', 'Production Receipt', 'receivedQty', i);
          return false;
        } if (+element.received_qty < 0) {
          this.openInvalidAlertDialog('Invalid received quantity', 'Production Receipt', 'receivedQty', i);
          return false;
        } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(element.conversion_charges) !== -1) {
          this.openInvalidAlertDialog('Enter conversion charges', 'Production Receipt', 'conversionCharges', i);
          return false;
        } if (+element.conversion_charges < 0) {
          this.openInvalidAlertDialog('Invalid conversion charges', 'Production Receipt', 'conversionCharges', i);
          return false;
        } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.hsn_code.toString().trim()) !== -1) {
          this.openInvalidAlertDialog('Enter hsn code', 'Production Receipt', 'hsnCode', i);
          return false;
        } if (!this.flags.isComposite && this.flags.isIgst && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(+element.igst) !== -1) {
          this.openInvalidAlertDialog('Enter igst', 'Production Receipt', 'igst', i);
          return false;
        } if (!this.flags.isComposite && this.flags.isIgst && +element.igst < 0) {
          this.openInvalidAlertDialog('Invalid igst', 'Production Receipt', 'igst', i);
          return false;
        } if (!this.flags.isComposite && !this.flags.isIgst && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(+element.cgst) !== -1) {
          this.openInvalidAlertDialog('Enter cgst', 'Production Receipt', 'cgst', i);
          return false;
        } if (!this.flags.isComposite && !this.flags.isIgst && +element.cgst < 0) {
          this.openInvalidAlertDialog('Invalid cgst', 'Production Receipt', 'cgst', i);
          return false;
        } if (!this.flags.isComposite && !this.flags.isIgst && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0.00', ''].indexOf(+element.sgst) !== -1) {
          this.openInvalidAlertDialog('Enter sgst', 'Production Receipt', 'sgst', i);
          return false;
        } if (!this.flags.isComposite && !this.flags.isIgst && +element.sgst < 0) {
          this.openInvalidAlertDialog('Invalid sgst', 'Production Receipt', 'sgst', i);
          return false;
        }
      } else return true;
    } return true;
  }

  private beforeSaveValidateMaterialDetails(): boolean {
    for (let i = 0; i < this.productionMaterialsList.length; i++) {
      let element: any = this.productionMaterialsList[i];
      if (JSON.stringify(element) !== JSON.stringify(this.objProductionMaterials)) {
        if (this.checkValidMaterialName(i)) {
          this.openInvalidAlertDialog('Invalid material name', 'Production Receipt', 'materialName', i);
          return false;
        } if (+element.material_qty < 0) {
          this.openInvalidAlertDialog('Invalid material quantity', 'Production Receipt', 'materialQty', i);
          return false;
        }
      } else return true;
    } return true;
  }

  private checkValidDetails(column: string, listOfColumn: string, rowIndex: number, colIndex: number): boolean {
    debugger;
    if (column.toString().trim() === 'receiptColumns') {
      if (this[column][colIndex].display === 'issueNo') {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(this[listOfColumn][rowIndex].issue_no) !== -1) {
          this.openInvalidAlertDialog('Enter issue no', 'Production Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } else return true;
      } else if (this[column][colIndex].display === 'productName') {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this[listOfColumn][rowIndex].product_name) !== -1) {
          this.openInvalidAlertDialog('Enter product name', 'Production Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } else return true;
      } else if (this[column][colIndex].display === 'receivedQty') {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this[listOfColumn][rowIndex].received_qty) !== -1) {
          this.openInvalidAlertDialog('Enter received qty', 'Production Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } else return true;
      } else if (this[column][colIndex].display === 'conversionCharges') {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this[listOfColumn][rowIndex].conversion_charges) !== -1) {
          this.openInvalidAlertDialog('Enter conversion charges', 'Production Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } if (+this[listOfColumn][rowIndex].conversion_charges < 0) {
          this.openInvalidAlertDialog('Invalid conversion charges', 'Prodcution Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } else return true;
      } else if (this[column][colIndex].display === 'hsnCode') {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this[listOfColumn][rowIndex].hsn_code) !== -1) {
          this.openInvalidAlertDialog('Enter hsn code', 'Production Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } else return true;
      } else if (!this.flags.isComposite && this.flags.isIgst && this[column][colIndex].display === 'igst') {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this[listOfColumn][rowIndex].igst) !== -1) {
          this.openInvalidAlertDialog('Enter igst', 'Production Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } if (+this[listOfColumn][rowIndex].igst < 0) {
          this.openInvalidAlertDialog('Invalid igst', 'Production Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } else return true;
      } else if (!this.flags.isComposite && !this.flags.isIgst && this[column][colIndex].display === 'sgst') {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this[listOfColumn][rowIndex].sgst) !== -1) {
          this.openInvalidAlertDialog('Enter sgst', 'Production Receipt', this[listOfColumn][rowIndex], rowIndex);
          return false;
        } if (+this[listOfColumn][rowIndex].sgst < 0) {
          this.openInvalidAlertDialog('Invalid sgst', 'Production Receipt', this[listOfColumn][rowIndex], rowIndex);
          return false;
        } else return true;
      } else if (!this.flags.isComposite && !this.flags.isIgst && this[column][colIndex].display === 'cgst') {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this[listOfColumn][rowIndex].cgst) !== -1) {
          this.openInvalidAlertDialog('Enter cgst', 'Production Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } if (+this[listOfColumn][rowIndex].cgst < 0) {
          this.openInvalidAlertDialog('Invalid cgst', 'Production Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } else return true;
      } else if (this[column][colIndex].display === 'totalCharges') {
        if (+this[listOfColumn][rowIndex].total_charges < 0) {
          this.openInvalidAlertDialog('Invalid total charges', 'Production Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } else return true;
      } else return true;
    } else if (column.toString().trim() === 'materialColumns') {
      if (this[column][colIndex].display === 'materialName') {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this[listOfColumn][rowIndex].material_name) !== -1) {
          this.openInvalidAlertDialog('Enter material name', 'Production Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } else return true;
      } else if (this[column][colIndex].display === 'materialQty') {
        if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', '0,00', ''].indexOf(+this[listOfColumn][rowIndex].material_qty) !== -1) {
          this.openInvalidAlertDialog('Enter material qty', 'Production Receipt', this[column][colIndex].display, rowIndex);
          return false;
        } else return true;
      } else return true;
    } else return true;
  }

  private checkIsValidMaterialRowToSave(element: any): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.material_id) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.material_name) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.material_qty) === -1)
      return true;
    else return false;
  }

  private checkIsValidRowToSave(element: any): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.issue_no) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.product_name) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.product_size) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.received_qty) === -1 &&
      [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.conversion_charges) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(element.hsn_code) === -1)
      return true;
    else return false;
  }

  private checkValidIssueNo(i: number): boolean {
    if (this.productionIssueNosList.length > 0 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.productionReceiptDetailsList[i].issue_no) === -1) {
      let index = this.productionIssueNosList.findIndex(x => +x.issue_no === +this.productionReceiptDetailsList[i].issue_no);
      if (index === -1) {
        this.openInvalidAlertDialog('Invalid issue no', 'Production Receipt', 'issueNo', i);
        return false;
      } else return true;
    } else return true;
  }

  private checkAnyChangesMade(): boolean {
    if (this.checkChangesInMaster() || this.checkChangesInReceiptDetails() || this.checkChangesInMaterialDetails())
      return true;
    else return false;
  }

  private checkChangesInMaster(): boolean {
    if (JSON.stringify(this.objProductionReceipt) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.objModifyProductionReceipt) : JSON.stringify(this.objUnChangedProductionReceipt)))
      return true;
    else return false;
  }

  private checkChangesInReceiptDetails(): boolean {
    if (JSON.stringify(this.productionReceiptDetailsList) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.modifyProductionReceiptDetailsList) : JSON.stringify(this.unChangedProductionReceiptDetailsList)))
      return true;
    else return false;
  }

  private checkChangesInMaterialDetails(): boolean {
    if (JSON.stringify(this.productionMaterialsList) !== ((this.objAction.isEditing || this.objAction.isView) ? JSON.stringify(this.modifyProductionMaterialsList) : JSON.stringify(this.unChangedProductionMaterialsList)))
      return true;
    else return false;
  }

  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public onlyAllowDecimalForReceiptDetails(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public onlyAllowNumbersForReceiptDetails(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    }
  }

  public onlyAllowRoundOffDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string) {
    debugger;
    if (event.keyCode !== 45) {
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      } else {
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      }
    } else {
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
      }
    }
  }

  private openAlertDialog(value: string, componentName: string, focus: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        document.getElementById(focus).focus();
      _dialogRef = null;
    });
  }

  private openInvalidAlertDialog(value: string, componentName: string, focus: string, i: number) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this[focus].toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Receipt No': this.dataSource.data[i].receipt_no,
          'Receipt Date': this._datePipe.transform(this.dataSource.data[i].receipt_date, 'dd/MM/yyyy'),
          "Supplier Name": this.dataSource.data[i].inv_supplier_name,
          'Invoice No': this.dataSource.data[i].inv_no,
          "Invoice Amount": this.dataSource.data[i].inv_amount,
          'Payment Status': this.dataSource.data[i].payment_decision_status,
          'Status': this.dataSource.data[i].active ? 'Active' : 'Cancelled',
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Production Receipt",
        datetime
      );
    } else
      this._confirmationDialog.openAlertDialog("No record found, Load the data first", "Production Receipt");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.receipt_no);
        tempObj.push(this._datePipe.transform(e.receipt_date, 'dd/MM/yyyy'));
        tempObj.push(e.inv_supplier_name);
        tempObj.push(e.inv_no);
        tempObj.push(e.inv_amount);
        tempObj.push(e.payment_decision_status);
        tempObj.push(e.active ? 'Active' : 'Cancelled');
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Receipt No', 'Receipt Date', 'Supplier Name', 'Invoice No', 'Invoice Amount', 'Payment Status', 'Status']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Production Receipt' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No record found, Load the data first", "Production Receipt");
  }
}
