import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionReceiptWithImageComponent } from './production-receipt-with-image.component';

describe('ProductionReceiptWithImageComponent', () => {
  let component: ProductionReceiptWithImageComponent;
  let fixture: ComponentFixture<ProductionReceiptWithImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionReceiptWithImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionReceiptWithImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
