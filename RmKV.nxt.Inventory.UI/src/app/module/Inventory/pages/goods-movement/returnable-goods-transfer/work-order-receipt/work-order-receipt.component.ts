import { DatePipe, DecimalPipe } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { EditMode } from 'src/app/common/models/common-model';
import { AccountsLookupService } from 'src/app/common/services/accounts-lookup/accounts-lookup.service';
import { CommonService } from 'src/app/common/services/common/common.service';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { MultipleFilesAttachComponent } from 'src/app/common/shared/multiple-files-attach/multiple-files-attach.component';
import { PendingSupplierLookupComponent } from 'src/app/common/shared/pending-supplier-lookup/pending-supplier-lookup.component';
import { WorkOrderRecepit } from 'src/app/module/Inventory/model/goodsMovement/returnableGoodsTransfer/work-order-issue.model';
import { CounterToGodownService } from 'src/app/module/Inventory/services/goods-movement/internal-transfers/counter-to-godown/counter-to-godown.service';
import { GodownToCounterService } from 'src/app/module/Inventory/services/goods-movement/internal-transfers/godown-to-counter/godown-to-counter.service';
import { WorkOrderIssueService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer/customer-work-order/work-order-issue/work-order-issue.service';
import { InvoicesService } from 'src/app/module/Inventory/services/purchases/invoices/invoices.service';
import { BynoSelectWorkorderInvoiceComponent } from '../byno-select-workorder-invoice/byno-select-workorder-invoice.component';
import { BynoSelectionComponent } from '../byno-selection/byno-selection.component';
declare var jsPDF: any;
@Component({
  selector: 'app-work-order-receipt',
  templateUrl: './work-order-receipt.component.html',
  styleUrls: ['./work-order-receipt.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class WorkOrderReceiptComponent implements OnInit {
  componentVisibility: boolean = true;
  displayedColumns = ["Serial_No", "Issue_No", "Issue_Date", "Supplier_Name", "Quantity", "Action"];
  dataSource: any = new MatTableDataSource([]);
  rows: any = [];
  WorkOrderIssueEntryGridList: any[] = [];
  SelectbarcodePrintingList: any = [];
  newSupplierLookupLists: any = [];
  newInSupplierLookupLists: any = [];
  newSupplierLookupList: any = [];
  workOrderList: any[] = [];
  workOrderNoList: any = [];
  workOrderReceiptiList: any = []
  masterSelected: boolean;
  workOrderReceiptDetailsList: any = []
  workOrderIssueDetailsList: WorkOrderRecepit[] = [];
  unChangedworkOrderIssueDetailsList: WorkOrderRecepit[] = [];
  focusFlag: boolean = false;
  Date: any = new Date();
  @ViewChildren("check") check: ElementRef | any;
  SelectApprovalList: any;
  isComposite: boolean = false;
  objLoad = {
    From_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    To_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    supplier_code: "",
    supplier_name: "",
    transfer_type: ""
  }
  objWorkOrderInvoice: any = {
    wo_receipt_id: 0,
    Date: new Date(),
    received_from: "",
    supplier_name: "",
    transfer_type: "",
    supplier_code: "",
    gstn_no: '',
    Supplier_invoice_no: '',
    Supplier_invoice_Date: new Date(),
    Supplier_invoice_Amount: 0,
    total_taxable_value: "",
    others_amount: "",
    cgst_total: '0.00',
    sgst_total: "0.00",
    igst_total: '0.00',
    additions: '0.00',
    deductions: '0.00',
    round_off_total: "",
    inv_amount: "",
    remarks: "",
    file_path1: "",
    file_path2: "",
    file_path3: "",
    file_path4: "",
  }
  unChangedWorkOrderInvoice: any = {
    wo_receipt_id: 0,
    Date: new Date(),
    received_from: "",
    supplier_name: "",
    transfer_type: "",
    supplier_code: "",
    gstn_no: '',
    Supplier_invoice_no: '',
    Supplier_invoice_Date: new Date(),
    Supplier_invoice_Amount: 0,
    total_taxable_value: "",
    others_amount: "",
    cgst_total: '0.00',
    sgst_total: "0.00",
    igst_total: '0.00',
    additions: '0.00',
    deductions: '0.00',
    round_off_total: "",
    inv_amount: "",
    remarks: "",
    file_path1: "",
    file_path2: "",
    file_path3: "",
    file_path4: "",
  }
  objWorkOrderModifyInvoice: any = {
    Date: new Date(),
    received_from: "",
    supplier_name: "",
    transfer_type: "",
    supplier_code: "",
    gstn_no: '',
    Supplier_invoice_no: '',
    Supplier_invoice_Date: new Date(),
    Supplier_invoice_Amount: 0,
    total_taxable_value: "",
    others_amount: "",
    cgst_total: '0.00',
    sgst_total: "0.00",
    igst_total: '0.00',
    additions: '0.00',
    deductions: '0.00',
    round_off_total: "",
    inv_amount: "",
    remarks: "",
    file_path1: "",
    file_path2: "",
    file_path3: "",
    file_path4: "",
  }

  objWorkOrderlist: any = {
    Date: new Date(),
    received_from: "",
    supplier_name: "",
    gstn_no: '',
    transfer_type: "",
    supplier_code: "",
    Supplier_invoice_no: '',
    Supplier_invoice_Date: new Date(),
    Supplier_invoice_Amount: 0,
    total_taxable_value: "",
    others_amount: "",
    cgst_total: '0.00',
    sgst_total: "0.00",
    igst_total: '0.00',
    round_off_total: "",
    inv_amount: "",
    remarks: "",
    file_path: ""
  }
  objAction: EditMode = {
    isEditing: false,
    isView: false
  };
  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };
  OldDocumentPath: any = '';
  document: any;
  tempFile: any = '';
  formData: any;
  isFileSelected: boolean = false;
  selectedFiles: File[] | any[] = [];
  unChangedFiles: File[] | any[] = [];
  AttachFiles: any = [];
  isFileAttach: number;
  fileIndex: any = [];
  selectedDocument: any;
  @ViewChild('fileInput', null) fileInput: ElementRef;
  isCancelled: boolean = false;
  removedFileIndex: number[] = [];
  removedFiles: string[] = [];
  inputEls: any[];

  isIgst = true;
  model1: any = new Date();
  model2: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();
  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  showTable: boolean = false;
  edit_data: any;
  editWorkOrderIssueDetailsList: any;

  columns: any[] = [
    { display: 'sNo', editable: false },
    { display: 'workNo', editable: false },
    { display: 'byno', editable: false },
    { display: 'bynoImgae', editable: false },
    { display: "desc", editable: false },
    { display: "stonetype", editable: false },
    { display: "stoneqty", editable: false },
    { display: "taxablevalue", editable: false },
    { display: "hsncode", editable: true },
    { display: "cgst", editable: false },
    { display: "sgst", editable: false },
    { display: "igst", editable: false },
    { display: "charges", editable: false },
    { display: "customercharges", editable: true },
  ];

  @ViewChildren('desc') desc: ElementRef | any;
  @ViewChildren('byno') byno: ElementRef | any;
  @ViewChildren('stonetype') stonetype: ElementRef | any;
  @ViewChildren('stoneqty') stoneqty: ElementRef | any;
  @ViewChildren('hsncode') hsncode: ElementRef | any;
  @ViewChildren('sgst') sgst: ElementRef | any;
  @ViewChildren('cgst') cgst: ElementRef | any;
  @ViewChildren('igst') igst: ElementRef | any;
  @ViewChildren('charges') charges: ElementRef | any;
  @ViewChildren('taxablevalue') taxablevalue: ElementRef | any;
  @ViewChildren('customercharges') customercharges: ElementRef | any;


  SupplierLookupList: any;

  constructor(public _minMaxDate: MinMaxDate, public _router: Router, private dbService: NgxIndexedDBService, public _dialog: MatDialog,
    public _localStorage: LocalStorage, private _datePipe: DatePipe, public _CommonService: CommonService,
    public _excelService: ExcelService, private _matDialog: MatDialog,
    public _godowntocounter: GodownToCounterService, public _godownstocounter: CounterToGodownService, private _confirmationDialog: ConfirmationDialogComponent,
    public _confirmationDialogComponent: ConfirmationDialogComponent, private _workOrderIssueService: WorkOrderIssueService,
    public _invoicesService: InvoicesService, public _gridKeyEvents: GridKeyEvents, public _decimalPipe: DecimalPipe, public _keyPressEvents: KeyPressEvents, public _agentMasterService: AccountsLookupService) {
    this.model1.setDate(this.model2.getDate() - 2);
    this.objLoad.From_Date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');

    this.masterSelected = false;
  }

  ngOnInit() {
    this.getTransferTypeList();
    this.loadAccountsDetails();
  }
  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.workOrderIssueDetailsList);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.workOrderIssueDetailsList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.workOrderIssueDetailsList);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.workOrderIssueDetailsList);
        break;
    }
  }

  // onAddRow() {
  //   this.rows.push(this.createItemFormGroup());
  // }
  public dataChanged(event: any) {
    this.objWorkOrderInvoice.cgst_total = event;
  }

  onRemoveRow(rowIndex: number) {
    this.rows.removeAt(rowIndex);
  }
  validateFromDate() {
    let date = this.dateValidation.validateDate(this.objLoad.From_Date, this.model1, this.minDate, this.maxDate);
    this.objLoad.From_Date = date[0];
    this.model1 = date[1];
  }
  validateToDate() {
    let date = this.dateValidation.validateDate(this.objLoad.To_Date, this.model2, this._datePipe.transform(this.model1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoad.To_Date = date[0];
    this.model2 = date[1];
  }

  public newClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.objAction.isEditing = false;
    this.isComposite = false;
    this.resetScreen();
    this.workOrderIssueDetailsList = [];
    this.showTable = true;
  }

  public onClear(exitFlag: boolean): void {
    if (!this.objAction.isView) {
      if (this.checkAnyChangesMade())
        this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
      else if (exitFlag)
        this.onListClick();
    } else
      this.onListClick();
  }
  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    debugger;
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Invoice Register";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }
  public onListClick(): void {
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }
  private resetScreen(): void {
    if (!this.objAction.isEditing) {
      this.objAction = Object.assign({}, this.unChangedAction);
      this.objWorkOrderInvoice = Object.assign({}, this.unChangedWorkOrderInvoice);
      this.workOrderNoList = [];
      this.workOrderIssueDetailsList = [];//JSON.parse(JSON.stringify(this.unChangedworkOrderIssueDetailsList));
      this.masterSelected = false;
      this.getTransferTypeList();
    } else {
      this.objWorkOrderInvoice = Object.assign({}, this.objWorkOrderModifyInvoice);
      this.workOrderIssueDetailsList = JSON.parse(JSON.stringify(this.unChangedworkOrderIssueDetailsList));
    }
  }
  private reset(): void {
    debugger;
    this.objAction = Object.assign({}, this.unChangedAction);
    this.objWorkOrderInvoice = Object.assign({}, this.unChangedWorkOrderInvoice);
    this.workOrderNoList = [];
    this.workOrderIssueDetailsList = [];
    this.masterSelected = false;
    this.getTransferTypeList();
  }
  //*************Quanty************ */
  public calculation(): number {
    if (this.dataSource.data !== undefined)
      return this.dataSource.data.map(t => +t.quantity).reduce((acc: any, value: any) => acc + value, 0);
  }
  public getTransferTypeList(): void {
    debugger;
    this._workOrderIssueService.getTransferTypeList().subscribe((result: any[]) => {
      if (result)
        this.WorkOrderIssueEntryGridList = result;
      else
        this._confirmationDialog.openAlertDialog('No records found', 'Work Order Issue');
    });
  }

  /************************************************ mulitiple File Upload code ************************************************/

  public openAttachFilesPopup(): void {
    debugger;
    let objAttachView = {
      file_path1: "",
      file_path2: "",
      file_path3: "",
      file_path4: "",
    }
    let dialogRef = this._matDialog.open(MultipleFilesAttachComponent, {
      panelClass: "custom-dialog-container",
      width: '53vw',
      data: {
        objAttach: JSON.stringify(objAttachView),
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      debugger;
      if (result) {
      }
    });
  }
  /************************************************ WorkorderReceiptInsert code ************************************************/
  public addWorkOrderReceiptList(): void {
    if (this.beforeGetWorkOrderAddValidation()) {
      let objDetails = [];
      for (let i = 0; i < this.workOrderIssueDetailsList.length; i++) {
        if ([null, undefined, ""].indexOf(this.workOrderIssueDetailsList[i].hsn_code.toString()) !== -1) {
          let input = this.hsncode.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter hsn code', 'Work Order Receipt Details');
          return
        } else if ([null, undefined, ""].indexOf(this.workOrderIssueDetailsList[i].customer_charges.toString()) !== -1) {
          let input = this.customercharges.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter customer charges', 'Work Order Receipt Details');
          return
        }
        objDetails.push(Object.assign({
          serial_no: i + 1,
          issue_wh_section_id: this.workOrderIssueDetailsList[i].wh_section_id,
          issue_transfer_type: this.workOrderIssueDetailsList[i].transfer_type,
          issue_id: this.workOrderIssueDetailsList[i].wo_issue_id,
          issue_serial_no: this.workOrderIssueDetailsList[i].work_order_serial_no,
          charges: this.workOrderIssueDetailsList[i].charges,
          customer_charges: this.workOrderIssueDetailsList[i].customer_charges,
          cgst: this.workOrderIssueDetailsList[i].cgst,
          cgst_amount: this.workOrderIssueDetailsList[i].cgst_amount,
          igst: this.workOrderIssueDetailsList[i].igst,
          igst_amount: this.workOrderIssueDetailsList[i].igst_amount,
          sgst: this.workOrderIssueDetailsList[i].sgst,
          sgst_amount: this.workOrderIssueDetailsList[i].sgst_amount,
          total_charges: this.workOrderIssueDetailsList[i].charges,
          hsn: this.workOrderIssueDetailsList[i].hsn_code,
          margin_code: null
        }));
      }
      let objData = {
        WorkOrderIssue: JSON.stringify([{
          wh_section_id: this._localStorage.getWhSectionId(),
          transfer_type: this.objWorkOrderInvoice.transfer_type,
          wo_receipt_id: this.objAction.isEditing ? this.objWorkOrderInvoice.wo_receipt_id : 0,
          wo_receipt_date: this._datePipe.transform(this.objWorkOrderInvoice.Date, 'dd/MM/yyyy'),
          received_from: this.objWorkOrderInvoice.received_from,
          taxable_amount: this.objWorkOrderInvoice.total_taxable_value,
          other_amount: 0,
          inv_supplier_code: this.objWorkOrderInvoice.received_from,
          inv_no: this.objWorkOrderInvoice.Supplier_invoice_no,
          inv_amount: this.objWorkOrderInvoice.inv_amount,
          igst_percentage: 0,
          cgst_percentage: 0,
          sgst_percentage: 0,
          composite: this.isComposite,
          igst_amount: this.objWorkOrderInvoice.igst_total,
          cgst_amount: this.objWorkOrderInvoice.cgst_total,
          sgst_amount: this.objWorkOrderInvoice.sgst_total,
          misc_addition_amt: this.objWorkOrderInvoice.additions,
          misc_deduction_amt: this.objWorkOrderInvoice.deductions,
          round_off_amount: this.objWorkOrderInvoice.round_off_total,
          file_path: "",
          remarks: this.objWorkOrderInvoice.remarks.toString().trim(),
          entered_by: +this._localStorage.intGlobalUserId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          inv_date: this._datePipe.transform(this.objWorkOrderInvoice.Supplier_invoice_Date, 'dd/MM/yyyy'),
          details: objDetails,
        }])
      }
      this._workOrderIssueService.addWorkOrderinvoice(objData).subscribe((result: any[]) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New Work Order Issue record has been created", "Work Order Issue");
          // this.reset();
          this.componentVisibility = !this.componentVisibility;
          this.objLoad.supplier_code = this.objWorkOrderInvoice.received_from;
          this.objLoad.supplier_name = this.objWorkOrderInvoice.supplier_name;
          this.objLoad.transfer_type = this.objWorkOrderInvoice.transfer_type;
          this.getWorkOrderReceiptList();
        }
      });
    }
  }
  // public addWorkOrderReceiptList(): void {
  //   const formData = new FormData();
  //   for (var i in this.selectedFiles) {
  //     formData.append("files", this.selectedFiles[i]);
  //   }

  //   if (this.beforeGetWorkOrderAddValidation()) {
  //     let objDetails = [];
  //     for (let i = 0; i < this.workOrderIssueDetailsList.length; i++) {
  //       if ([null, undefined, ""].indexOf(this.workOrderIssueDetailsList[i].hsn_code.toString()) !== -1) {
  //         let input = this.hsncode.toArray();
  //         input[i].nativeElement.focus();
  //         this._confirmationDialogComponent.openAlertDialog('Enter hsn code', 'Work Order Receipt Details');
  //         return
  //       } else if ([null, undefined, ""].indexOf(this.workOrderIssueDetailsList[i].customer_charges.toString()) !== -1) {
  //         let input = this.customercharges.toArray();
  //         input[i].nativeElement.focus();
  //         this._confirmationDialogComponent.openAlertDialog('Enter customer charges', 'Work Order Receipt Details');
  //         return
  //       }
  //       objDetails.push(Object.assign({
  //         serial_no: i + 1,
  //         issue_wh_section_id: this.workOrderIssueDetailsList[i].wh_section_id,
  //         issue_transfer_type: this.workOrderIssueDetailsList[i].transfer_type,
  //         issue_id: this.workOrderIssueDetailsList[i].wo_issue_id,
  //         issue_serial_no: this.workOrderIssueDetailsList[i].work_order_serial_no,
  //         charges: this.workOrderIssueDetailsList[i].charges,
  //         customer_charges: this.workOrderIssueDetailsList[i].customer_charges,
  //         cgst: this.workOrderIssueDetailsList[i].cgst,
  //         cgst_amount: this.workOrderIssueDetailsList[i].cgst_amount,
  //         igst: this.workOrderIssueDetailsList[i].igst,
  //         igst_amount: this.workOrderIssueDetailsList[i].igst_amount,
  //         sgst: this.workOrderIssueDetailsList[i].sgst,
  //         sgst_amount: this.workOrderIssueDetailsList[i].sgst_amount,
  //         total_charges: this.workOrderIssueDetailsList[i].charges,
  //         hsn: this.workOrderIssueDetailsList[i].hsn_code,
  //         margin_code: null
  //       }));
  //     }
  //     let objData = {
  //       WorkOrderIssue: JSON.stringify([{
  //         wh_section_id: this._localStorage.getWhSectionId(),
  //         transfer_type: this.objWorkOrderInvoice.transfer_type,
  //         wo_receipt_id: this.objAction.isEditing ? this.objWorkOrderInvoice.wo_receipt_id : 0,
  //         wo_receipt_date: this._datePipe.transform(this.objWorkOrderInvoice.Date, 'dd/MM/yyyy'),
  //         received_from: this.objWorkOrderInvoice.received_from,
  //         taxable_amount: this.objWorkOrderInvoice.total_taxable_value,
  //         other_amount: 0,
  //         inv_supplier_code: this.objWorkOrderInvoice.received_from,
  //         inv_no: this.objWorkOrderInvoice.Supplier_invoice_no,
  //         inv_amount: this.objWorkOrderInvoice.inv_amount,
  //         igst_percentage: 0,
  //         cgst_percentage: 0,
  //         sgst_percentage: 0,
  //         composite: this.isComposite,
  //         igst_amount: this.objWorkOrderInvoice.igst_total,
  //         cgst_amount: this.objWorkOrderInvoice.cgst_total,
  //         sgst_amount: this.objWorkOrderInvoice.sgst_total,
  //         misc_addition_amt: this.objWorkOrderInvoice.additions,
  //         misc_deduction_amt: this.objWorkOrderInvoice.deductions,
  //         round_off_amount: this.objWorkOrderInvoice.round_off_total,
  //         file_path: [{
  //           file_path1: this.objWorkOrderInvoice.file_path1,
  //           file_path2: this.objWorkOrderInvoice.file_path2,
  //           file_path3: this.objWorkOrderInvoice.file_path3,
  //           file_path4: this.objWorkOrderInvoice.file_path4
  //         }],
  //         remarks: this.objWorkOrderInvoice.remarks.toString().trim(),
  //         entered_by: +this._localStorage.intGlobalUserId(),
  //         company_section_id: +this._localStorage.getCompanySectionId(),
  //         inv_date: this._datePipe.transform(this.objWorkOrderInvoice.Supplier_invoice_Date, 'dd/MM/yyyy'),
  //         details: objDetails,
  //       }])
  //     }

  //     formData.append('SaveWorkOrderInvoice', JSON.stringify(objData));
  //     formData.append("fileRootPath", '\\\\192.9.202.39\\\\Rmkv_Nxt_Doc\\\\Invoices');
  //     formData.append("fileIndex", JSON.stringify(this.fileIndex));
  //     formData.append('removedFiles', JSON.stringify(this.removedFiles));
  //     formData.append('removedFileIndex', JSON.stringify(this.removedFileIndex));
  //     this._workOrderIssueService.addWorkOrderinvoice(formData).subscribe((result: any[]) => {
  //       if (result) {
  //         this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New Work Order Issue record has been created", "Work Order Issue");
  //         // this.reset();
  //         this.componentVisibility = !this.componentVisibility;
  //         this.objLoad.supplier_code = this.objWorkOrderInvoice.received_from;
  //         this.objLoad.supplier_name = this.objWorkOrderInvoice.supplier_name;
  //         this.objLoad.transfer_type = this.objWorkOrderInvoice.transfer_type;
  //         this.getWorkOrderReceiptList();
  //       }
  //       this.selectedFiles = null;
  //       this.document = null;
  //     });
  //   }
  // }
  fetchWorkOrderReceipt(wo_receipt_id): void {
    let objData = {
      WorkOrderIssue: JSON.stringify([{
        wo_receipt_id: wo_receipt_id,
        transfer_type: this.objLoad.transfer_type,
        wh_section_id: +this._localStorage.getWhSectionId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._workOrderIssueService.fetchWorkOrderReceipt(objData).subscribe((result: any) => {
      if (result) {
        this.objAction.isEditing = true;
        this.objAction.isView = false;
        let tempArr = JSON.parse(JSON.stringify(result[0]));
        let tempArray = JSON.parse(JSON.stringify(result));
        this.objWorkOrderInvoice.wo_receipt_id = tempArr.wo_receipt_id;
        this.objWorkOrderInvoice.transfer_type = tempArr.transfer_type;
        this.objWorkOrderInvoice.Supplier_invoice_no = tempArr.inv_no;
        this.objWorkOrderInvoice.Supplier_invoice_Date = tempArr.inv_date;
        this.objWorkOrderInvoice.Supplier_invoice_Amount = tempArr.inv_amount;
        this.objWorkOrderInvoice.received_from = tempArr.inv_supplier_code;
        this.objWorkOrderInvoice.supplier_name = tempArr.inv_supplier_name;
        this.objWorkOrderInvoice.gstn_no = tempArr.gstn_no == null ? '' : tempArr.gstn_no;
        this.objWorkOrderInvoice.Date = tempArr.wo_receipt_date;
        this.objWorkOrderInvoice.inv_amount = tempArr.inv_amount;
        this.setTypeOfGst();
        this.getWorkOrderIssueEntryGridList();
        this.objWorkOrderInvoice.total_taxable_value = tempArr.taxable_amount;
        this.objWorkOrderInvoice.others_amount = tempArr.other_amount;
        this.objWorkOrderInvoice.igst_total = tempArr.igst_total;
        this.objWorkOrderInvoice.sgst_total = tempArr.sgst_total;
        this.objWorkOrderInvoice.cgst_total = tempArr.cgst_total;
        this.objWorkOrderInvoice.round_off_total = tempArr.round_off_amount;
        this.objWorkOrderInvoice.remarks = tempArr.remarks;
        for (let i = 0; i < tempArray.length; i++) {
          this.workOrderIssueDetailsList[i] = Object.assign({
            byno: tempArray[i].byno,
            cgst: tempArray[i].cgst,
            charges: tempArray[i].charges,
            checked: true,
            description: tempArray[i].description,
            hsn_code: tempArray[i].hsn,
            igst: tempArray[i].igst,
            image_path: tempArray[i].image_path,
            saree_value: tempArray[i].saree_value,
            serial_no: tempArray[i].serial_no,
            sgst: tempArray[i].sgst,
            stone_qty: tempArray[i].stone_qty,
            stone_type: tempArray[i].stone_type,
            transfer_type: tempArray[i].transfer_type,
            wh_section_id: tempArray[i].wh_section_id,
            wo_issue_id: tempArray[i].wo_issue_id,
            work_order_no: tempArray[i].work_order_no,
            work_order_serial_no: tempArray[i].work_order_serial_no,
            work_order_transfer_type: tempArray[i].work_order_transfer_type,
            work_order_wh_section_id: tempArray[i].work_order_wh_section_id,
            customer_charges: tempArray[i].customer_charges,
            sgst_amount: tempArray[i].sgst_amount,
            cgst_amount: tempArray[i].cgst_amount,
            igst_amount: tempArray[i].igst_amount,
          })
          this.convertToDecimal(i);
        }
        this.unChangedworkOrderIssueDetailsList = JSON.parse(JSON.stringify(this.workOrderIssueDetailsList));
        this.convertTotalsToDecimal('');
        this.objWorkOrderModifyInvoice = JSON.parse(JSON.stringify(this.objWorkOrderInvoice));
        this.componentVisibility = !this.componentVisibility;
      }
    });
  }
  viewWorkOrderReceipt(wo_receipt_id): void {
    let objData = {
      WorkOrderIssue: JSON.stringify([{
        wo_receipt_id: wo_receipt_id,
        transfer_type: this.objLoad.transfer_type,
        wh_section_id: +this._localStorage.getWhSectionId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._workOrderIssueService.fetchWorkOrderReceipt(objData).subscribe((result: any) => {
      if (result) {
        this.objAction.isEditing = false;
        this.objAction.isView = true;
        let tempArr = JSON.parse(JSON.stringify(result[0]));
        let tempArray = JSON.parse(JSON.stringify(result));
        this.objWorkOrderInvoice.wo_receipt_id = tempArr.wo_receipt_id;
        this.objWorkOrderInvoice.transfer_type = tempArr.transfer_type;
        this.objWorkOrderInvoice.Supplier_invoice_no = tempArr.inv_no;
        this.objWorkOrderInvoice.Supplier_invoice_Date = tempArr.inv_date;
        this.objWorkOrderInvoice.Supplier_invoice_Amount = tempArr.inv_amount;
        this.objWorkOrderInvoice.received_from = tempArr.inv_supplier_code;
        this.objWorkOrderInvoice.supplier_name = tempArr.inv_supplier_name;
        this.objWorkOrderInvoice.gstn_no = tempArr.gstn_no == null ? '' : tempArr.gstn_no;
        this.objWorkOrderInvoice.Date = tempArr.wo_receipt_date;
        this.objWorkOrderInvoice.inv_amount = tempArr.inv_amount;
        this.setTypeOfGst();
        this.getWorkOrderIssueEntryGridList();
        this.objWorkOrderInvoice.total_taxable_value = tempArr.taxable_amount;
        this.objWorkOrderInvoice.others_amount = tempArr.other_amount;
        this.objWorkOrderInvoice.igst_total = tempArr.igst_total;
        this.objWorkOrderInvoice.sgst_total = tempArr.sgst_total;
        this.objWorkOrderInvoice.cgst_total = tempArr.cgst_total;
        this.isComposite = tempArr.composite;
        this.objWorkOrderInvoice.additions = tempArr.additions;
        this.objWorkOrderInvoice.deductions = tempArr.deductions;
        this.objWorkOrderInvoice.round_off_total = tempArr.round_off_amount;
        this.objWorkOrderInvoice.remarks = tempArr.remarks;
        for (let i = 0; i < tempArray.length; i++) {
          this.workOrderIssueDetailsList[i] = Object.assign({
            byno: tempArray[i].byno,
            cgst: tempArray[i].cgst,
            charges: tempArray[i].charges,
            checked: true,
            description: tempArray[i].description,
            hsn_code: tempArray[i].hsn,
            igst: tempArray[i].igst,
            image_path: tempArray[i].image_path,
            saree_value: tempArray[i].saree_value,
            serial_no: tempArray[i].serial_no,
            sgst: tempArray[i].sgst,
            stone_qty: tempArray[i].stone_qty,
            stone_type: tempArray[i].stone_type,
            transfer_type: tempArray[i].transfer_type,
            wh_section_id: tempArray[i].wh_section_id,
            wo_issue_id: tempArray[i].wo_issue_id,
            work_order_no: tempArray[i].work_order_no,
            work_order_serial_no: tempArray[i].work_order_serial_no,
            work_order_transfer_type: tempArray[i].work_order_transfer_type,
            work_order_wh_section_id: tempArray[i].work_order_wh_section_id,
            customer_charges: tempArray[i].customer_charges,
            sgst_amount: tempArray[i].sgst_amount,
            cgst_amount: tempArray[i].cgst_amount,
            igst_amount: tempArray[i].igst_amount,
          })
          this.convertToDecimal(i);
        }
        this.unChangedworkOrderIssueDetailsList = JSON.parse(JSON.stringify(this.workOrderIssueDetailsList));
        this.convertTotalsToDecimal('');
        this.objWorkOrderModifyInvoice = JSON.parse(JSON.stringify(this.objWorkOrderInvoice));
        this.componentVisibility = !this.componentVisibility;
      }
    });
  }
  public getWorkOrderReceiptList(): void {
    if (this.beforeLoadValidate()) {
      this.matTableConfig([]);
      let WorkOrderLoad = {
        WorkOrderIssue: JSON.stringify([{
          from_date: this.objLoad.From_Date,
          to_date: this.objLoad.To_Date,
          supplier_code: this.objLoad.supplier_code,
          company_section_id: +this._localStorage.getCompanySectionId(),
          transfer_type: this.objLoad.transfer_type,
          wh_section_id: this._localStorage.getWhSectionId(),
        }])
      }
      this._workOrderIssueService.getWorkOrderReceiptList(WorkOrderLoad).subscribe((result: any[]) => {
        this.workOrderList = result;
        this.workOrderList != null ? this.matTableConfig(this.workOrderList) : this._confirmationDialogComponent.openAlertDialog("No record found", "work Order Issue List");
      });
    }
  }
  public matTableConfig(tableRecords: any[]): void {
    this.dataSource = new MatTableDataSource(tableRecords);
  }
  public setTypeOfGst(): void {
    if (this.objWorkOrderInvoice.gstn_no) {
      if (!this.isComposite) {
        let enteredGst: string = this.objWorkOrderInvoice.gstn_no.substr(0, 2);
        if (+ enteredGst === +this._localStorage.getStateCode()) {
          this.isIgst = false;
          // this.setEditableTrueOrFalse('sgst', true);
          // this.setEditableTrueOrFalse('cgst', true);
          // this.setEditableTrueOrFalse('igst', false);
        } else {
          this.isIgst = true;
          // this.setEditableTrueOrFalse('sgst', false);
          // this.setEditableTrueOrFalse('cgst', false);
          // this.setEditableTrueOrFalse('igst', true);
        }
      } else {
        // this.setEditableTrueOrFalse('sgst', false);
        // this.setEditableTrueOrFalse('cgst', false);
        // this.setEditableTrueOrFalse('igst', false);
      }
    }
  }

  private setEditableTrueOrFalse(columnName: string, editable: boolean): void {
    debugger;
    let i = this.columns.findIndex(x => x.display === columnName)
    this.columns[i].editable = editable;
  }
  /************************************************ lookup Invoicesupplier code ************************************************/


  public openBynoDialog(data): void {
    debugger
    if (!this.objAction.isView) {
      this.edit_data = data;
      let dialogRef = this._dialog.open(BynoSelectWorkorderInvoiceComponent, {
        width: "975px",
        data: {
          data: data,
          workOrderIssueDetailsList: this.workOrderIssueDetailsList,
          isEditing: this.objAction.isEditing,
          editWorkOrderIssueDetailsList: this.editWorkOrderIssueDetailsList
        },
        panelClass: "custom-dialog-container",
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        debugger;
        if (result) {
          console.log(result, 'issue details result');
          var array = [];
          var oldArray = [];
          oldArray = this.workOrderIssueDetailsList;
          if (result.length > 0) {
            for (var i = 0; i < oldArray.length; i++) {
              if (oldArray[i].wh_section_id == result[0].wh_section_id && oldArray[i].transfer_type == result[0].transfer_type &&
                oldArray[i].wo_issue_id != result[0].wo_issue_id) {
                array.push(oldArray[i]);
              }
            }
            this.workOrderIssueDetailsList = JSON.parse(JSON.stringify(array));
          } else {
            var array = [];
            var oldArray = [];
            console.log(this.edit_data, 'edit_data')
            oldArray = this.workOrderIssueDetailsList;
            for (var i = 0; i < oldArray.length; i++) {
              if (
                oldArray[i].wh_section_id == this.edit_data.wh_section_id &&
                oldArray[i].transfer_type == this.edit_data.transfer_type &&
                oldArray[i].wo_issue_id != this.edit_data.wo_issue_id) {
                array.push(oldArray[i]);
              }
            }
            this.workOrderIssueDetailsList = JSON.parse(JSON.stringify(array));
          }
          if (result.length > 0) {
            result.forEach(x => {
              x.charges = 0
            });
          }
          for (let i = 0; i < result.length; i++) {
            this.workOrderIssueDetailsList.push(result[i]);
          }
          for (let i = 0; i < this.workOrderIssueDetailsList.length; i++) {
            this.workOrderIssueDetailsList[i].charges = this.workOrderIssueDetailsList[i].charges ? this.workOrderIssueDetailsList[i].charges : this.workOrderIssueDetailsList[i].saree_value;
            this.workOrderIssueDetailsList[i].sgst = this.workOrderIssueDetailsList[i].sgst ? this.workOrderIssueDetailsList[i].sgst : 0;
            this.workOrderIssueDetailsList[i].cgst = this.workOrderIssueDetailsList[i].cgst ? this.workOrderIssueDetailsList[i].cgst : 0;
            this.workOrderIssueDetailsList[i].igst = this.workOrderIssueDetailsList[i].igst ? this.workOrderIssueDetailsList[i].igst : 0;
            this.workOrderIssueDetailsList[i].sgst_amount = this.workOrderIssueDetailsList[i].sgst_amount ? this.workOrderIssueDetailsList[i].sgst_amount : 0;
            this.workOrderIssueDetailsList[i].cgst_amount = this.workOrderIssueDetailsList[i].cgst_amount ? this.workOrderIssueDetailsList[i].cgst_amount : 0;
            this.workOrderIssueDetailsList[i].igst_amount = this.workOrderIssueDetailsList[i].igst_amount ? this.workOrderIssueDetailsList[i].igst_amount : 0;
            this.workOrderIssueDetailsList[i].hsn_code = this.workOrderIssueDetailsList[i].hsn_code ? this.workOrderIssueDetailsList[i].hsn_code : '';
            this.workOrderIssueDetailsList[i].customer_charges = this.workOrderIssueDetailsList[i].customer_charges ? this.workOrderIssueDetailsList[i].customer_charges : '0.00';
            this.workOrderIssueDetailsList[i].charges = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.workOrderIssueDetailsList[i].charges.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.workOrderIssueDetailsList[i].charges, '1.2-2')).replace(/,/g, '') : "0.00";
            this.workOrderIssueDetailsList[i].saree_value = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.workOrderIssueDetailsList[i].saree_value.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.workOrderIssueDetailsList[i].saree_value, '1.2-2')).replace(/,/g, '') : "0.00";
          }

          this.getTaxValueTotal();
        }
      });
    }
  }

  public gePendindSupplierLookup(): void {
    debugger;
    let objAccounts = {
      WorkOrderIssue: JSON.stringify([{
        wh_section_id: this._localStorage.getWhSectionId(),
        transfer_type: this.objWorkOrderInvoice.transfer_type,
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._agentMasterService.gePendindSupplierLookup(objAccounts).subscribe((result: any) => {
      if (result)
        this.newSupplierLookupList = result;
    });
  }
  public openReceivedFormLookups(event: KeyboardEvent | any): any {
    debugger;
    this.beforPendingSupplierCodeValidation();
    if (event.keyCode === 13 && !this.validateReceivedFormSupplierNames()) {
      this.openReceivedFormLookupDialogs();
    }
  }
  private openReceivedFormLookupDialogs(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(PendingSupplierLookupComponent, {
      width: "650px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objWorkOrderInvoice.received_from,
        data: this.objWorkOrderInvoice.transfer_type
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        debugger;
        this.objWorkOrderInvoice.received_from = result.account_code.toString().trim();
        this.objWorkOrderInvoice.supplier_name = result.account_name.toString().trim();
        this.objWorkOrderInvoice.gstn_no = result.gstn_no == null ? '' : result.gstn_no.toString().trim();
        this.setTypeOfGst();
        this.getWorkOrderIssueEntryGridList();
      } else {
        this.objWorkOrderInvoice.received_from = '';
        this.objWorkOrderInvoice.supplier_name = '';
        this.objWorkOrderInvoice.gstn_no = '';
      }
    });
  }
  public onEnterNewSupplierReceivedFormLookupDetail(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      email: "",
      address1: "",
      address2: "",
      address3: "",
      gstn_no: ""
    };
    supplierLookupList = this.newSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objWorkOrderInvoice.received_from.toString().trim().toLowerCase());
    this.objWorkOrderInvoice.received_from = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code.toString().trim() : this.objWorkOrderInvoice.received_from.toString().trim();
    this.objWorkOrderInvoice.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name.toString().trim() : '';
    this.objWorkOrderInvoice.gstn_no = supplierLookupList && supplierLookupList.gstn_no ? supplierLookupList.gstn_no.toString().trim() : '';
    this.workOrderNoList = [];
    this.workOrderIssueDetailsList = [];
    this.onChangeClear();
    this.setTypeOfGst();
    if (this.objWorkOrderInvoice.supplier_name)
      this.getWorkOrderIssueEntryGridList();
  }

  onChangeClear() {
    this.objWorkOrderInvoice.total_taxable_value = "";
    this.objWorkOrderInvoice.others_amount = "";
    this.objWorkOrderInvoice.cgst_total = "";
    this.objWorkOrderInvoice.sgst_total = "";
    this.objWorkOrderInvoice.igst_total = "";
    this.objWorkOrderInvoice.round_off_total = "";
    this.objWorkOrderInvoice.inv_amount = "";
    this.objWorkOrderInvoice.remarks = "";
  }
  public openNewSupplierLookups(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierNames()) {
      this.openNewSupplierLookupDialogs();
    }

  }
  private openNewSupplierLookupDialogs(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "650px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objWorkOrderInvoice.inv_supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objWorkOrderInvoice.inv_supplier_code = result.account_code.toString().trim();
        this.objWorkOrderInvoice.inv_supplier_name = result.account_name.toString().trim();
        this.objWorkOrderInvoice.gstn_no = result.gstn_no.toString().trim();
      } else {
        this.objWorkOrderInvoice.inv_supplier_code = '';
        this.objWorkOrderInvoice.inv_supplier_name = '';
        this.objWorkOrderInvoice.gstn_no = '';
      }
    });
  }

  public checkValidSupplierCodes(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objWorkOrderInvoice.supplier_code.toString().trim() !== '' && this.objWorkOrderInvoice.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier code", "WorkOrder Receipt", focus);
    }
  }
  public onEnterNewSupplierLookupDetail(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      email: "",
      address1: "",
      address2: "",
      address3: "",
      gstn_no: ""
    };

    supplierLookupList = this.newSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objWorkOrderInvoice.inv_supplier_code.toString().trim().toLowerCase());
    this.objWorkOrderInvoice.inv_supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objWorkOrderInvoice.inv_supplier_code.toString().trim();
    this.objWorkOrderInvoice.inv_supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
    this.objWorkOrderInvoice.gstn_no = supplierLookupList && supplierLookupList.gstn_no ? supplierLookupList.gstn_no : '';
  }
  /************************************************ End lookup Invoicesupplier code ************************************************/
  public openNewSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierName()) {
      this.openNewSupplierLookupDialog();
    }

  }
  private loadAccountsDetails(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._agentMasterService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      if (result) {
        // console.log(result, "lkp")
        this.SupplierLookupList = JSON.parse(result);
      }
    });
  }
  private openNewSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "650px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objLoad.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objLoad.supplier_code = result.account_code.toString().trim();
        this.objLoad.supplier_name = result.account_name.toString().trim();

      } else {
        this.objLoad.supplier_code = '';
        this.objLoad.supplier_name = '';
      }
    });
  }
  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    // let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public checkValidSupplierCode(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objLoad.supplier_code.toString().trim() !== '' && this.objLoad.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier code", "WorkOrder Receipt", focus);
    }
  }
  public onEnterNewSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      email: "",
      address1: "",
      address2: "",
      address3: "",
      gstn_no: ""
    };

    supplierLookupList = this.SupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objLoad.supplier_code.toString().trim().toLowerCase());
    this.objLoad.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objLoad.supplier_code.toString().trim();
    this.objLoad.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';

  }

  /************************************************ WorkOrderReceiptInsertValidations ************************************************/

  public getWorkOrderIssueEntryGridList(): void {
    debugger;
    //if (this.beforeReceivedtValidation()) {
    let WorkOrder = {
      WorkOrderIssue: JSON.stringify([{
        supplier_code: this.objWorkOrderInvoice.received_from,
        wh_section_id: this._localStorage.getWhSectionId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        transfer_type: this.objWorkOrderInvoice.transfer_type,
      }])
    }
    this._workOrderIssueService.getWorkOrderReceiptListPending(WorkOrder).subscribe((result: any[]) => {
      if (result) {
        debugger;
        console.log(result, "WorkOrderIssueEntryGridList")
        this.workOrderNoList = JSON.parse(JSON.stringify(result));
      }
      else
        this._confirmationDialog.openAlertDialog('No records found', 'Work Order Issue');
    });
    // }
  }


  /************************************************ Validations ************************************************/
  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objWorkOrderInvoice) !== ((!this.objAction.isEditing) ? JSON.stringify(this.unChangedWorkOrderInvoice)
      : JSON.stringify(this.objWorkOrderModifyInvoice)))
      return true;
    else
      return false;
  }
  openAlertDialog(value: string, componentName?: string, focus?: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        document.getElementById(focus).focus();
        _dialogRef = null;
      }
      _dialogRef = null;
    });
  }
  public validateReceivedFormSupplierNames(): boolean {
    debugger;
    let index = this.newInSupplierLookupLists.findIndex(element => element.account_code.toString().trim() === this.objWorkOrderInvoice.supplier_code.toString().trim());
    if (index === -1) {
      return false;
    } else return true;
  }
  public beforPendingSupplierCodeValidation(): boolean {
    if ([null, undefined, ""].indexOf(this.objWorkOrderInvoice.transfer_type) !== -1 || !this.objWorkOrderInvoice.transfer_type.toString().trim()) {
      document.getElementById('Transfer_Type').focus();
      this.openTypeAlertDialog("Select Transfer Type ", "Work Order ReceiptList");
      return false;
    } else return true;
  }
  public validateSupplierName(): boolean {
    let index = this.newSupplierLookupLists.findIndex(element => element.account_code.toString().trim() === this.objLoad.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }
  public validateSupplierNames(): boolean {
    let index = this.newInSupplierLookupLists.findIndex(element => element.account_code.toString().trim() === this.objWorkOrderInvoice.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }


  // private getDeductedTotal(i: number): number {
  //   debugger;
  //   if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(+this.workOrderIssueDetailsList[i].cost_price) === -1 &&
  //     [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.workOrderIssueDetailsList[i].discount_amount) === -1 &&
  //     [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.workOrderIssueDetailsList[i].discount_percent) === -1) {
  //     let total: number = +((+this.workOrderIssueDetailsList[i].product_qty * +this.workOrderIssueDetailsList[i].cost_price) - (+this.workOrderIssueDetailsList[i].product_qty * +this.workOrderIssueDetailsList[i].discount_amount));
  //     this.workOrderIssueDetailsList[i].discount_percent_amt = +total * (+this.workOrderIssueDetailsList[i].discount_percent / 100);
  //     return +(+total - +this.workOrderIssueDetailsList[i].discount_percent_amt);
  //   } else return 0;
  // }


  public getGstByHsnCode(i: number): void {
    if (!this.isComposite) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.workOrderIssueDetailsList[i].hsn_code) === -1) {
        let objGet = {
          GstByHSN: JSON.stringify([{
            supplier_gstn: this.objWorkOrderInvoice.gstn_no,
            company_section_id: this._localStorage.getCompanySectionId(),
            hsn: this.workOrderIssueDetailsList[i].hsn_code,
            supplier_inv_date: this._datePipe.transform(this.objWorkOrderInvoice.Supplier_invoice_Date, 'dd/MM/yyyy'),
            cost_price: this.workOrderIssueDetailsList[i].saree_value//this.getDeductedTotal(i)
          }])
        }
        this._invoicesService.getGstByHSNCode(objGet).subscribe((result: any) => {
          if (result) {
            debugger;
            this.workOrderIssueDetailsList[i].sgst = JSON.parse(JSON.stringify(result))[0].sgst;
            this.workOrderIssueDetailsList[i].cgst = JSON.parse(JSON.stringify(result))[0].cgst;
            this.workOrderIssueDetailsList[i].igst = JSON.parse(JSON.stringify(result))[0].igst;
            this.calculateTaxableValue(i);
          } else {
            this.workOrderIssueDetailsList[i].igst = 0;
            this.workOrderIssueDetailsList[i].cgst = 0;
            this.workOrderIssueDetailsList[i].sgst = 0;
            this.openInvalidAlertDialog('Invalid hsn code', i, 'Work Order Invoice', 'hsncode');
          }
        });
      }
    }
  }
  private openInvalidAlertDialog(value: string, i: number, componentName: string, focus: any) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this[focus].toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  public onlyAllwDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }





  //****************************beforeGetWorkOrderIssueEntrytValidation************************** *//

  private beforeReceivedtValidation(): boolean {
    if ([null, undefined, ""].indexOf(this.objWorkOrderInvoice.transfer_type) !== -1 || !this.objWorkOrderInvoice.transfer_type.toString().trim()) {
      document.getElementById('Transfer_Type').focus();
      this.openTypeAlertDialog("Select Transfer Type ", "Work Order ReceiptList");
      return false;
    }
    else if ([null, undefined, 0, ""].indexOf(this.objWorkOrderInvoice.received_from) !== -1 || !this.objWorkOrderInvoice.received_from.toString().trim()) {
      document.getElementById('received_from').focus();
      this.openTypeAlertDialog("Enter Received From", "Work Order ReceiptList");
      return false;
    }
    else if ([null, undefined, 0, ""].indexOf(this.objWorkOrderInvoice.inv_supplier_code) !== -1 || !this.objWorkOrderInvoice.inv_supplier_code.toString().trim()) {
      document.getElementById('inv_supplier_code').focus();
      this.openTypeAlertDialog("Enter Invoice Supplier Code", "Work Order ReceiptList");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objWorkOrderInvoice.Supplier_invoice_no) !== -1 || !this.objWorkOrderInvoice.Supplier_invoice_no.toString().trim()) {
      document.getElementById('Supplier_invoice_no').focus();
      this.openTypeAlertDialog("Enter Supplier Invoice No", "Work Order ReceiptList");
      return false;
    }
    else if ([null, undefined, 0, ""].indexOf(this.objWorkOrderInvoice.Supplier_invoice_Amount) !== -1 || !this.objWorkOrderInvoice.Supplier_invoice_Amount.toString().trim()) {
      document.getElementById('SupplierinvoiceAmount').focus();
      this.openTypeAlertDialog("Enter Supplier Invoice Amount", "Work Order ReceiptList");
      return false;
    } else {
      return true;
    }
  }

  openTypeAlertDialog(value: string, componentName?: string) {
    //  this._matDialog.closeAll();
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // this.objWorkOrderInvoice.transfer_type = '';
      }
      _dialogRef = null;
    });
  }
  private getGstTotal(i: number, deductedTotal?: number): void {
    debugger;
    if (this.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.workOrderIssueDetailsList[i].igst) === -1) {
        this.workOrderIssueDetailsList[i].igst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.workOrderIssueDetailsList[i].igst) === -1 ? deductedTotal * (+this.workOrderIssueDetailsList[i].igst / 100) : 0;
        //  return +this.workOrderIssueDetailsList[i].igst_amount;
      }
    } else if (!this.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.workOrderIssueDetailsList[i].cgst) === -1 &&
        [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.workOrderIssueDetailsList[i].sgst) === -1) {
        this.workOrderIssueDetailsList[i].cgst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.workOrderIssueDetailsList[i].cgst) === -1 ? deductedTotal * (+this.workOrderIssueDetailsList[i].cgst / 100) : 0;
        this.workOrderIssueDetailsList[i].sgst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.workOrderIssueDetailsList[i].sgst) === -1 ? deductedTotal * (+this.workOrderIssueDetailsList[i].sgst / 100) : 0;
        // return +(+this.workOrderIssueDetailsList[i].cgst_amount + +this.workOrderIssueDetailsList[i].sgst_amount);
      }
    }
  }
  public calculateTaxableValue(i: number): void {
    debugger;
    this.getGstTotal(i, +this.workOrderIssueDetailsList[i].saree_value)
    this.workOrderIssueDetailsList[i].charges = +this.workOrderIssueDetailsList[i].saree_value + +(this.workOrderIssueDetailsList[i].igst);
    if (this.isIgst) {
      this.getIgstGrandTotal(i);
      this.getTaxValueTotal();
    }
    else if (!this.isIgst) {
      this.getCgstGrandTotal(i);
      this.getSgstGrandTotal(i);
      this.getTaxValueTotal();
    }
    this.getGrandTotal();
    this.convertToDecimal(i);
  }
  public disableTaxableColumns(): void {
    debugger;
    this.resetGstValues();
    // if (this.isComposite) {
    //   this.setEditableTrueOrFalse('sgst', false);
    //   this.setEditableTrueOrFalse('cgst', false);
    //   this.setEditableTrueOrFalse('igst', false);
    // } else if (!this.isComposite) {
    //   this.setEditableTrueOrFalse('sgst', true);
    //   this.setEditableTrueOrFalse('cgst', true);
    //   this.setEditableTrueOrFalse('igst', true);
    // }
    this.setTypeOfGst();
  }
  private resetGstValues(): void {
    for (let i = 0; i < this.workOrderIssueDetailsList.length; i++) {
      this.workOrderIssueDetailsList[i].sgst = 0;
      this.workOrderIssueDetailsList[i].sgst_amount = 0;
      this.workOrderIssueDetailsList[i].cgst = 0;
      this.workOrderIssueDetailsList[i].cgst_amount = 0;
      this.workOrderIssueDetailsList[i].igst = 0;
      this.workOrderIssueDetailsList[i].igst_amount = 0;
      this.workOrderIssueDetailsList[i].hsn_code = '';
      this.calculateTaxableValue(i);
    }
  }
  public onlyAllowRoundOffDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string) {
    debugger;
    if (event.keyCode !== 45) {
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      } else {
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      }
    } else {
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
      }
    }
  }
  public getGrandTotal(isModify?: string): any {
    debugger
    let invoiceAmount: number = this.workOrderIssueDetailsList.map(t => (+t.charges)).reduce((acc, value) => acc + value, 0);
    this.objWorkOrderInvoice.inv_amount = (+invoiceAmount) + +this.objWorkOrderInvoice.additions - +this.objWorkOrderInvoice.deductions;

    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objWorkOrderInvoice.round_off_total) === -1) {
      if (this.objWorkOrderInvoice.round_off_total.toString().includes('.')) {
        if (+(this.objWorkOrderInvoice.round_off_total.toString().split('.')[1]) > 0) {
          this.objWorkOrderInvoice.inv_amount = +this.objWorkOrderInvoice.inv_amount + +(this.objWorkOrderInvoice.round_off_total);
        } else if (+(this.objWorkOrderInvoice.round_off_total.toString().split('.')[1]) < 0) {
          this.objWorkOrderInvoice.inv_amount = +this.objWorkOrderInvoice.inv_amount - +(this.objWorkOrderInvoice.round_off_total);
        } else if (+(this.objWorkOrderInvoice.round_off_total.toString().split('.')[1]) === 0) {
          this.objWorkOrderInvoice.inv_amount = +this.objWorkOrderInvoice.inv_amount + 0;
        }
      } else if (+(this.objWorkOrderInvoice.round_off_total.toString())) {
        this.objWorkOrderInvoice.inv_amount = +this.objWorkOrderInvoice.inv_amount + +(this.objWorkOrderInvoice.round_off_total);
      }
    }
    // if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objWorkOrderInvoice.others_amount) === -1) {
    //   if (this.objWorkOrderInvoice.others_amount.toString().includes('.')) {
    //     if (+(this.objWorkOrderInvoice.others_amount.toString().split('.')[1]) > 0) {
    //       this.objWorkOrderInvoice.inv_amount = +this.objWorkOrderInvoice.inv_amount + +(this.objWorkOrderInvoice.others_amount);
    //     } else if (+(this.objWorkOrderInvoice.others_amount.toString().split('.')[1]) < 0) {
    //       this.objWorkOrderInvoice.inv_amount = +this.objWorkOrderInvoice.inv_amount - +(this.objWorkOrderInvoice.others_amount);
    //     } else if (+(this.objWorkOrderInvoice.others_amount.toString().split('.')[1]) === 0) {
    //       this.objWorkOrderInvoice.inv_amount = +this.objWorkOrderInvoice.inv_amount + 0;
    //     }
    //   } else if (+(this.objWorkOrderInvoice.others_amount.toString())) {
    //     this.objWorkOrderInvoice.inv_amount = +this.objWorkOrderInvoice.inv_amount + +(this.objWorkOrderInvoice.others_amount);
    //   }
    // }
    this.convertTotalsToDecimal(isModify);
  }

  public convertToDecimal(i: number): void {
    this.workOrderIssueDetailsList[i].charges = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.workOrderIssueDetailsList[i].charges.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.workOrderIssueDetailsList[i].charges, '1.2-2')).replace(/,/g, '') : "0.00";
    this.workOrderIssueDetailsList[i].customer_charges = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.workOrderIssueDetailsList[i].customer_charges.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.workOrderIssueDetailsList[i].customer_charges, '1.2-2')).replace(/,/g, '') : "0.00";
    this.workOrderIssueDetailsList[i].saree_value = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.workOrderIssueDetailsList[i].saree_value.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.workOrderIssueDetailsList[i].saree_value, '1.2-2')).replace(/,/g, '') : "0.00";
    this.convertTotalsToDecimal('');
  }
  public convertTotalsToDecimal(editableColumn?: string): void {
    if (editableColumn !== 'Total')
      this.objWorkOrderInvoice.total_taxable_value = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objWorkOrderInvoice.total_taxable_value.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objWorkOrderInvoice.total_taxable_value, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'Invoice')
      this.objWorkOrderInvoice.inv_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objWorkOrderInvoice.inv_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objWorkOrderInvoice.inv_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'CGST')
      this.objWorkOrderInvoice.cgst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objWorkOrderInvoice.cgst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objWorkOrderInvoice.cgst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'SGST')
      this.objWorkOrderInvoice.sgst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objWorkOrderInvoice.sgst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objWorkOrderInvoice.sgst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'IGST')
      this.objWorkOrderInvoice.igst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objWorkOrderInvoice.igst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objWorkOrderInvoice.igst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'Other')
      this.objWorkOrderInvoice.others_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objWorkOrderInvoice.others_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objWorkOrderInvoice.others_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'Round')
      this.objWorkOrderInvoice.round_off_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objWorkOrderInvoice.round_off_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objWorkOrderInvoice.round_off_total, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'InvoiceAmount')
      this.objWorkOrderInvoice.Supplier_invoice_Amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objWorkOrderInvoice.Supplier_invoice_Amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objWorkOrderInvoice.Supplier_invoice_Amount, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'Additions')
      this.objWorkOrderInvoice.additions = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objWorkOrderInvoice.additions.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objWorkOrderInvoice.additions, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'Deductions')
      this.objWorkOrderInvoice.deductions = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objWorkOrderInvoice.deductions.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objWorkOrderInvoice.deductions, '1.2-2')).replace(/,/g, '') : "0.00";
  }
  public onlyAllwDecimalOther(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    // let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }
  getTaxValueTotal() {
    this.objWorkOrderInvoice.total_taxable_value = 0;
    this.objWorkOrderInvoice.total_taxable_value = this.workOrderIssueDetailsList.map(t => +t.saree_value).reduce((acc, value) => acc + value, 0);
    this.objWorkOrderInvoice.inv_amount = 0;
    this.objWorkOrderInvoice.inv_amount = this.workOrderIssueDetailsList.map(t => +t.charges).reduce((acc, value) => acc + value, 0);
    this.convertTotalsToDecimal('');
  }
  private getIgstGrandTotal(i): void {
    let discount = +this.workOrderIssueDetailsList[i].saree_value * +(this.workOrderIssueDetailsList[i].igst) / 100;
    this.workOrderIssueDetailsList[i].charges = +this.workOrderIssueDetailsList[i].saree_value + discount;
    this.objWorkOrderInvoice.igst_total = 0;
    this.objWorkOrderInvoice.igst_total = this.workOrderIssueDetailsList.map(t => +t.igst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getCgstGrandTotal(i): void {
    let discount = +this.workOrderIssueDetailsList[i].saree_value * +((+this.workOrderIssueDetailsList[i].sgst) + (+this.workOrderIssueDetailsList[i].cgst)) / 100;
    this.workOrderIssueDetailsList[i].charges = +this.workOrderIssueDetailsList[i].saree_value + discount;
    this.objWorkOrderInvoice.cgst_total = 0;
    this.objWorkOrderInvoice.cgst_total = this.workOrderIssueDetailsList.map(t => +t.cgst_amount).reduce((acc, value) => acc + value, 0);
  }
  public addOtherAmount() {
    this.objWorkOrderInvoice.inv_amount = (+this.objWorkOrderInvoice.inv_amount) + (+this.objWorkOrderInvoice.others_amount);
    this.convertTotalsToDecimal('Other');
  }
  private getSgstGrandTotal(i): void {
    //  this.workOrderIssueDetailsList[i].charges = +this.workOrderIssueDetailsList[i].saree_value * +(this.workOrderIssueDetailsList[i].igst) / 100;
    this.objWorkOrderInvoice.sgst_total = 0;
    this.objWorkOrderInvoice.sgst_total = this.workOrderIssueDetailsList.map(t => +t.sgst_amount).reduce((acc, value) => acc + value, 0);
  }

  private beforeGetWorkOrderAddValidation(): boolean {
    debugger;
    if ([null, undefined, 0, ""].indexOf(this.objWorkOrderInvoice.transfer_type) !== -1 || !this.objWorkOrderInvoice.transfer_type.toString().trim()) {
      document.getElementById('Transfer_Type').focus();
      this._confirmationDialogComponent.openAlertDialog("Select Transfer Type", "Work Order ReceiptList");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objWorkOrderInvoice.Supplier_invoice_no) !== -1 || !this.objWorkOrderInvoice.Supplier_invoice_no.toString().trim()) {
      document.getElementById('Supplier_invoice_no').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Supplier Invoice No", "Work Order ReceiptList");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objWorkOrderInvoice.Supplier_invoice_Amount) !== -1 || !this.objWorkOrderInvoice.Supplier_invoice_Amount.toString().trim()) {
      document.getElementById('SupplierinvoiceAmount').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Supplier Invoice Amount", "Work Order ReceiptList");
      return false;
    } else {
      return true;
    }
  }
  private beforeLoadValidate(): boolean {
    debugger;
    if (!this.objLoad.From_Date.toString().trim()) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter from date", "Work Order ReceiptList");
      return false;
    } else if (this.objLoad.From_Date.length != 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid from date", "Work Order ReceiptList");
      return false;
    } else if (!this.objLoad.To_Date.toString().trim()) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter to date", "Work Order ReceiptList");
      return false;
    } else if (this.objLoad.To_Date.length != 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid to date", "Work Order ReceiptList");
      return false;
    } else if (this.model2 < this.model1) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("To date can't be less than from date", "Work Order ReceiptList");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objLoad.supplier_code) !== -1 || !this.objLoad.supplier_code.toString().trim()) {
      document.getElementById('supplierCode').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Supplier Code", "Work Order ReceiptList");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objLoad.supplier_name) !== -1 || !this.objLoad.supplier_name.toString().trim()) {
      document.getElementById('supplierName').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Supplier Name", "Work Order ReceiptList");
      return false;
    } else if ([null, undefined, 0, ""].indexOf(this.objLoad.transfer_type) !== -1 || !this.objLoad.transfer_type.toString().trim()) {
      document.getElementById('Transfer').focus();
      this._confirmationDialogComponent.openAlertDialog("Select Transfer Type", "Work Order ReceiptList");
      return false;
    } else
      return true;
  }
  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Receipt No': this.dataSource.data[i].wo_receipt_id,
          'Receipt Date': this._datePipe.transform(this.dataSource.data[i].wo_receipt_date, 'dd/MM/yyyy'),
          // 'Customer Name': this.dataSource.data[i].Customer_Name,
          "Supplier Name": this.dataSource.data[i].account_name,
          "Quantity": this.dataSource.data[i].quantity,
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Work_Order_ReceiptList",
        datetime
      );
    } else
      this._confirmationDialog.openAlertDialog("No record found, Load the data first", "Work Order ReceiptList");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.wo_receipt_id);
        tempObj.push(this._datePipe.transform(e.wo_receipt_date, 'dd/MM/yyyy'));
        // tempObj.push(e.customer_name);
        tempObj.push(e.account_name);
        tempObj.push(e.quantity);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Receipt No', 'Receipt Date', 'Supplier Name', 'Quantity']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Work_Order_ReceiptList' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No record found, Load the data first", "Work Order ReceiptList");
  }

}
