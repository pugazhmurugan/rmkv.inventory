import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionIssueWithImageComponent } from './production-issue-with-image.component';

describe('ProductionIssueWithImageComponent', () => {
  let component: ProductionIssueWithImageComponent;
  let fixture: ComponentFixture<ProductionIssueWithImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionIssueWithImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionIssueWithImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
