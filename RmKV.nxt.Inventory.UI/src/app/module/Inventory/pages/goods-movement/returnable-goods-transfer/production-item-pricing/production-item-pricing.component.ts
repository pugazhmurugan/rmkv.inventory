import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-production-item-pricing',
  templateUrl: './production-item-pricing.component.html',
  styleUrls: ['./production-item-pricing.component.scss']
})
export class ProductionItemPricingComponent implements OnInit {

  componentVisibility: boolean = true;
  dataSource = ELEMENT_DATA;
  // dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Byno", "Serial", "Supplier_Name", "Product_Name", "Size", "Qty", "Total_Conversion_Charge", "Action"];

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    ) { }

  ngOnInit() {
  }

  public newClick(): void {
    this.componentVisibility = !this.componentVisibility;
  }

  public onClear(): void {
    this.componentVisibility = !this.componentVisibility;
  }

  public listClick(): void {
    this.componentVisibility = !this.componentVisibility;
  }

}

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
];