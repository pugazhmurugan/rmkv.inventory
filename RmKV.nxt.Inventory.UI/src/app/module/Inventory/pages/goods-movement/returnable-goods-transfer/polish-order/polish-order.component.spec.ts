import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolishOrderComponent } from './polish-order.component';

describe('PolishOrderComponent', () => {
  let component: PolishOrderComponent;
  let fixture: ComponentFixture<PolishOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolishOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolishOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
