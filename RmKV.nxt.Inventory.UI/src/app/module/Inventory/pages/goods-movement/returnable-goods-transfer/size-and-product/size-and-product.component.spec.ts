import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SizeAndProductComponent } from './size-and-product.component';

describe('SizeAndProductComponent', () => {
  let component: SizeAndProductComponent;
  let fixture: ComponentFixture<SizeAndProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SizeAndProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SizeAndProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
