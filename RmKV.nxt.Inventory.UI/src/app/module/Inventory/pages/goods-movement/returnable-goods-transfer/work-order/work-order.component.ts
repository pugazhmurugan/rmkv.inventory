import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Rights } from 'src/app/common/models/common-model';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { MultipleFilesAttachComponent } from 'src/app/common/shared/multiple-files-attach/multiple-files-attach.component';
import { CustomerWorkOrderService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer/customer-work-order/customer-work-order.service';
import { ImagecaptureComponent } from '../imagecapture/imagecapture.component';
declare var jsPDF: any;

@Component({
  selector: 'app-work-order',
  templateUrl: './work-order.component.html',
  styleUrls: ['./work-order.component.scss'],
  providers: [DatePipe]
})
export class WorkOrderComponent implements OnInit {

  objLoad = {
    From_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    To_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Store_Location: this._localStorage.getwareHouseName(),
    Section: this._localStorage.getCompanySectionName(),
    Transfer_Type: ''
  }

  objAction: any = {
    isEditing: false,
    isView: false,
    isAttach: false
  };

  unChangedAction: any = {
    isEditing: false,
    isView: false,
    isAttach: false
  };

  objCustomerWorkOrder = {
    Work_Order_No: 0,
    Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Section: this._localStorage.getCompanySectionName(),
    To_Warehouse: '',
    Transfer_Type: '',
    Bill_No: '',
    Amount_To_Be_Paid: 0,
    Delivery_Date: new Date(),
    Bonus_Card_No: '',
    Customer_Id: 0,
    Customer_Name: '',
    Address1: '',
    Address2: '',
    Address3: '',
    Phone_No: '',
    Order_Taken_By: '',
    Byno_List: [],
    File_Path1: '',
    File_Path2: '',
    File_Path3: '',
    File_Path4: '',
    Remarks: ''
  }

  objModifyCustomerWorkOrder = {
    Work_Order_No: 0,
    Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Section: this._localStorage.getCompanySectionName(),
    To_Warehouse: '',
    Transfer_Type: '',
    Bill_No: '',
    Amount_To_Be_Paid: 0,
    Delivery_Date: new Date(),
    Bonus_Card_No: '',
    Customer_Id: 0,
    Customer_Name: '',
    Address1: '',
    Address2: '',
    Address3: '',
    Phone_No: '',
    Order_Taken_By: '',
    Byno_List: [],
    File_Path1: '',
    File_Path2: '',
    File_Path3: '',
    File_Path4: '',
    Remarks: ''
  }

  unchangedCustomerWorkOrder = {
    Work_Order_No: 0,
    Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Section: this._localStorage.getCompanySectionName(),
    To_Warehouse: '',
    Transfer_Type: '',
    Bill_No: '',
    Amount_To_Be_Paid: 0,
    Delivery_Date: new Date(),
    Bonus_Card_No: '',
    Customer_Id: 0,
    Customer_Name: '',
    Address1: '',
    Address2: '',
    Address3: '',
    Phone_No: '',
    Order_Taken_By: '',
    Byno_List: [],
    File_Path1: '',
    File_Path2: '',
    File_Path3: '',
    File_Path4: '',
    Remarks: ''
  }

  objBynoList = {
    byno: '',
    image_path: '',
    description: '',
    saree_value: '',
    stone_type: '',
    stone_qty: '',
    charges: ''
  }

  unchangedBynoList = {
    byno: '',
    image_path: '',
    description: '',
    saree_value: '',
    stone_type: '',
    stone_qty: '',
    charges: ''
  }

  rights: Rights = {
    Add: true,
    Update: true,
    Delete: true,
    Approve: true,
    View: true
  };

  objAttachView = {
    file_path1: '',
    file_path2: '',
    file_path3: '',
    file_path4: ''
  }

  unchangedAttach = {
    file_path1: '',
    file_path2: '',
    file_path3: '',
    file_path4: ''
  }

  selectedFiles: File[] | any[] = [];
  fileIndex: number[] = [];
  isCancelled: boolean = false;
  isExceedCount: boolean = false;
  removedFileIndex: number[] = [];
  removedFiles: string[] = [];

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  model1: any = new Date();
  model2: any = new Date();

  public dateValidation: DateValidation = new DateValidation();

  componentVisibility: boolean = true;

  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Work_Order_No", "Work_Order_Date", "Customer_Name", "Product_Qty", "Product_Value", "Status", "Action"];

  customerWOList: any;
  locationList: any;
  transferTypeList: any;


  columns: any[] = [
    { display: 'sno', editable: false },
    { display: 'byno', editable: true },
    { display: 'image', editable: false },
    { display: "pre", editable: false },
    { display: "description", editable: true },
    { display: "productValue", editable: true },
    { display: "stoneType", editable: true },
    { display: "stoneQty", editable: true },
    { display: "charges", editable: true },
    { display: "action", editable: true }
  ];

  @ViewChildren('byno') byno: ElementRef | any;
  @ViewChildren('image') image: ElementRef | any;
  @ViewChildren('pre') pre: ElementRef | any;
  @ViewChildren('description') description: ElementRef | any;
  @ViewChildren('productValue') productValue: ElementRef | any;
  @ViewChildren('stoneType') stoneType: ElementRef | any;
  @ViewChildren('stoneQty') stoneQty: ElementRef | any;
  @ViewChildren('charges') charges: ElementRef | any;
  @ViewChildren('action') action: ElementRef | any;

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    private _datePipe: DatePipe,
    public _matDialog: MatDialog,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _excelService: ExcelService,
    public _customerWorkOrderService: CustomerWorkOrderService,
    public _commonService: CommonService,
    private _gridKeyEvents: GridKeyEvents,
    public _dialog: MatDialog,
    public _keyPressEvents: KeyPressEvents
  ) {
    this.model1.setDate(this.model2.getDate() - 2);
    this.objLoad.From_Date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
  }

  ngOnInit() {
    this.getRights();
    this.getToWarehouse();
    this.getTransferTypeList();
  }

  /****************************************** Validations **********************************************/

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        let response = this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.objCustomerWorkOrder.Byno_List);
        if (response)
          this.addNewRowToBynoList(rowIndex);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.objCustomerWorkOrder.Byno_List);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.objCustomerWorkOrder.Byno_List);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.objCustomerWorkOrder.Byno_List);
        break;
    }
  }

  private getRights(): void {
    var rights = this._localStorage.getMenuRights("/WorkOrder");
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "Delete" && rights[x].Status == true) {
        this.rights.Delete = true;
      }
      if (rights[x].Right_Name == "Approve" && rights[x].Status == true) {
        this.rights.Approve = true;
      }
      if (rights[x].Right_Name == "View" && rights[x].Status == true) {
        this.rights.View = true;
      }
    }
  }

  public validateFromDate() {
    let date = this.dateValidation.validateDate(this.objLoad.From_Date, this.model1, this.minDate, this.maxDate);
    this.objLoad.From_Date = date[0];
    this.model1 = date[1];
  }

  public validateToDate() {
    let date = this.dateValidation.validateDate(this.objLoad.To_Date, this.model2, this._datePipe.transform(this.model1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoad.To_Date = date[0];
    this.model2 = date[1];
  }

  private beforeloadValidate(): boolean {
    if (!this.objLoad.From_Date.toString().trim()) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter from date", "Customer Work Order");
      return false;
    } else if (this.objLoad.From_Date.length != 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid from date", "Customer Work Order");
      return false;
    } else if (!this.objLoad.To_Date.toString().trim()) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter to date", "Customer Work Order");
      return false;
    } else if (this.objLoad.To_Date.length != 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid to date", "Customer Work Order");
      return false;
    } else if (this.model2 < this.model1) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("To date can't be less than from date", "Customer Work Order");
      return false;
    } else if (!this.objLoad.Transfer_Type.toString().trim()) {
      document.getElementById('type').focus();
      this._confirmationDialogComponent.openAlertDialog("Select Transfer Type", "Customer Work Order");
      return false;
    } else
      return true;
  }

  private beforeSaveValidate(): boolean {
    if (!this.objCustomerWorkOrder.To_Warehouse.toString().trim()) {
      document.getElementById('warehouse').focus();
      this._confirmationDialogComponent.openAlertDialog("Select to warehouse", "Customer Work Order");
      return false;
    } else if (!this.objCustomerWorkOrder.Transfer_Type.toString().trim()) {
      document.getElementById('type').focus();
      this._confirmationDialogComponent.openAlertDialog("Select transfer type", "Customer Work Order");
      return false;
    } else if (!this.objCustomerWorkOrder.Bill_No.toString().trim()) {
      document.getElementById('bill').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter bill no", "Customer Work Order");
      return false;
    } else if (!this.objCustomerWorkOrder.Amount_To_Be_Paid.toString().trim()) {
      document.getElementById('amount').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter amount to be paid", "Customer Work Order");
      return false;
    } else if (!this.objCustomerWorkOrder.Customer_Name.toString().trim()) {
      document.getElementById('customer').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter customer name", "Customer Work Order");
      return false;
    } else if (!this.objCustomerWorkOrder.Phone_No.toString().trim()) {
      document.getElementById('phone').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter phone no", "Customer Work Order");
      return false;
    } else if (!this.objCustomerWorkOrder.Address1.toString().trim()) {
      document.getElementById('add1').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter address 1", "Customer Work Order");
      return false;
    } else if (!this.objCustomerWorkOrder.Address2.toString().trim()) {
      document.getElementById('add2').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter address 2", "Customer Work Order");
      return false;
    } else if (!this.objCustomerWorkOrder.Address3.toString().trim()) {
      document.getElementById('add3').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter address 3", "Customer Work Order");
      return false;
    } else if (!this.objCustomerWorkOrder.Order_Taken_By.toString().trim()) {
      document.getElementById('order').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter order taken by", "Customer Work Order");
      return false;
    } else
      return true;
  }

  public onlyAllwDecimalForCustomerWorkOrder(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  /****************************************** CRUD Operations **********************************************/

  public getCustomerWorkOrder(): void {
    if (this.beforeloadValidate()) {
      this.matTableConfig([]);
      let objData = {
        CustomerWorkOrder: JSON.stringify([{
          from_date: this.objLoad.From_Date,
          to_date: this.objLoad.To_Date,
          transfer_type: this.objLoad.Transfer_Type,
          wh_section_id: +this._localStorage.getWhSectionId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
        }]),
      }
      this._customerWorkOrderService.getCustomerWorkOrder(objData).subscribe((result: any) => {
        if (result) {
          this.customerWOList = result;
          console.log(this.customerWOList, 'customerWOList');
          this.matTableConfig(this.customerWOList);
        } else
          this._confirmationDialogComponent.openAlertDialog('No records found', 'Customer Work Order');
      });
    }
  }

  public getToWarehouse() {
    let objData = {
      GoodCounter: JSON.stringify([{
        company_section_id: this._localStorage.getCompanySectionId(),
        sales_location_id: this._localStorage.getGlobalSalesLocationId()
      }]),
    }
    this._customerWorkOrderService.getToWarehouse(objData).subscribe((result: any) => {
      if (result) {
        this.locationList = result;
        result.length == 1 ? this.objCustomerWorkOrder.To_Warehouse = result[0].warehouse_id : '';
      } else
        this._confirmationDialogComponent.openAlertDialog('No records found', 'Customer Work Order');
    });
  }

  public getTransferTypeList(): void {
    this._customerWorkOrderService.getTransferTypeList().subscribe((result: any) => {
      if (result) {
        this.transferTypeList = result;
        result.length == 1 ? this.objCustomerWorkOrder.Transfer_Type = result[0].transfer_flag : '';
      }
      else {
        this._confirmationDialogComponent.openAlertDialog('No records found', 'Customer Work Order');
      }
    });
  }

  public matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }

  public customerNameFilter(searchValue: string, event: KeyboardEvent | any): void {
    searchValue = searchValue.toString().trim() ? searchValue.toString().toLocaleLowerCase() : "";
    let filteredCustomerList = [];
    if (event.keyCode === 8 || event.keyCode === 46) {
      filteredCustomerList = this.customerWOList.filter((item: any) =>
        item.customer_name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    } else {
      filteredCustomerList = this.dataSource.data.filter((item: any) =>
        item.customer_name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
    }
    this.matTableConfig(filteredCustomerList);
  }

  public addCustomerWorkOrder(): void {
    if (this.beforeSaveValidate()) {
      debugger
      if (this.objCustomerWorkOrder.Byno_List[0].byno == '') {
        let input = this.byno.toArray();
        input[0].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog('Enter byno', 'Customer Work Order')
        return
      }
      for (let i = 0; i < this.objCustomerWorkOrder.Byno_List.length; i++) {
        if (this.objCustomerWorkOrder.Byno_List[i].byno == '') {
          this.objCustomerWorkOrder.Byno_List.splice(i, 1);
        } else if (this.objCustomerWorkOrder.Byno_List[i].byno !== '' && this.objCustomerWorkOrder.Byno_List[i].description == '') {
          let input = this.description.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter description', 'Customer Work Order')
          return
        } else if (this.objCustomerWorkOrder.Byno_List[i].byno !== '' && this.objCustomerWorkOrder.Byno_List[i].product_value == '') {
          let input = this.productValue.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter product value', 'Customer Work Order')
          return
        } else if (this.objCustomerWorkOrder.Byno_List[i].byno !== '' && this.objCustomerWorkOrder.Byno_List[i].stone_type == '') {
          let input = this.stoneType.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter stone type', 'Customer Work Order')
          return
        } else if (this.objCustomerWorkOrder.Byno_List[i].byno !== '' && this.objCustomerWorkOrder.Byno_List[i].stone_qty == '') {
          let input = this.stoneQty.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter stone Qty', 'Customer Work Order')
          return
        } else if (this.objCustomerWorkOrder.Byno_List[i].byno !== '' && this.objCustomerWorkOrder.Byno_List[i].charges == '') {
          let input = this.charges.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog('Enter charges', 'Customer Work Order')
          return
        }
      }
      let formData: any = new FormData();
      for (let i = 0; i < this.selectedFiles.length; i++) {
        formData.append('files', this.selectedFiles[i]);
      }
      formData.append('fileIndex', JSON.stringify(this.fileIndex));
      formData.append('fileRootPath', '\\\\192.9.202.39\\\\Rmkv_Nxt_Doc\\\\Invoices');

      let objDetails = [];
      for (let i = 0; i < this.objCustomerWorkOrder.Byno_List.length; i++) {
        objDetails.push(Object.assign({
          serial_no: i + 1,
          byno: this.objCustomerWorkOrder.Byno_List[i].byno,
          description: this.objCustomerWorkOrder.Byno_List[i].description,
          stone_type: this.objCustomerWorkOrder.Byno_List[i].stone_type,
          stone_qty: this.objCustomerWorkOrder.Byno_List[i].stone_qty,
          image_path: this.objCustomerWorkOrder.Byno_List[i].image_path,
          saree_value: this.objCustomerWorkOrder.Byno_List[i].saree_value,
          charges: this.objCustomerWorkOrder.Byno_List[i].charges,
        }));
      }
      let objData = {
        CustomerWorkOrder: JSON.stringify([{
          wh_section_id: this._localStorage.getWhSectionId(),
          transfer_type: this.objCustomerWorkOrder.Transfer_Type,
          work_order_no: this.objCustomerWorkOrder.Work_Order_No,
          destination_id: +this.objCustomerWorkOrder.To_Warehouse,
          work_order_date: this.objCustomerWorkOrder.Date,
          customer_id: this.objCustomerWorkOrder.Customer_Id,
          customer_name: this.objCustomerWorkOrder.Customer_Name,
          address1: this.objCustomerWorkOrder.Address1,
          address2: this.objCustomerWorkOrder.Address2,
          address3: this.objCustomerWorkOrder.Address3,
          phones: this.objCustomerWorkOrder.Phone_No,
          bill_no: this.objCustomerWorkOrder.Bill_No,
          no_of_sarees: objDetails.length,
          delivery_date: this._datePipe.transform(this.objCustomerWorkOrder.Delivery_Date, 'dd/MM/yyyy'),
          order_taken_by: this.objCustomerWorkOrder.Order_Taken_By,
          remarks: this.objCustomerWorkOrder.Remarks,
          file_path: [{ file_path1: this.objCustomerWorkOrder.File_Path1, file_path2: this.objCustomerWorkOrder.File_Path2, file_path3: this.objCustomerWorkOrder.File_Path3, file_path4: this.objCustomerWorkOrder.File_Path4 }],
          details: objDetails,
          entered_by: +this._localStorage.intGlobalUserId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          total_saree_value: this.getProductValue(),
          total_charges: this.getCharges(),
          amount_to_be_paid: this.objCustomerWorkOrder.Amount_To_Be_Paid
        }]),
      }
      // formData.append('WorkOrderList', objData);
      this._customerWorkOrderService.addCustomerWorkOrder(objData).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New customer work order record has been created", "Customer Work Order");
          this.resetScreen();
        }
      });
    }
  }

  fetchCustomerWorkOrder(work_order_no): void {
    let objData = {
      CustomerWorkOrder: JSON.stringify([{
        work_order_no: work_order_no,
        transfer_type: this.objLoad.Transfer_Type,
        wh_section_id: +this._localStorage.getWhSectionId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._customerWorkOrderService.fetchCustomerWorkOrder(objData).subscribe((result: any) => {
      if (result) {
        this.objAction.isEditing = true;
        this.objAction.isView = false;
        let tempArr = JSON.parse(JSON.stringify(result[0]));
        this.objCustomerWorkOrder.Work_Order_No = tempArr.work_order_no
        this.objCustomerWorkOrder.Date = this._datePipe.transform(tempArr.work_order_date, 'dd/MM/yyyy');
        this.objCustomerWorkOrder.To_Warehouse = tempArr.destination_id;
        this.objCustomerWorkOrder.Transfer_Type = tempArr.transfer_type;
        this.objCustomerWorkOrder.Bill_No = tempArr.bill_no;
        this.objCustomerWorkOrder.Amount_To_Be_Paid = tempArr.amount_to_be_paid;
        this.objCustomerWorkOrder.Delivery_Date = tempArr.delivery_date;
        this.objCustomerWorkOrder.Bonus_Card_No = tempArr.bonuc_card_no;
        this.objCustomerWorkOrder.Customer_Name = tempArr.customer_name;
        this.objCustomerWorkOrder.Phone_No = tempArr.phones;
        this.objCustomerWorkOrder.Address1 = tempArr.address1;
        this.objCustomerWorkOrder.Address2 = tempArr.address2;
        this.objCustomerWorkOrder.Address3 = tempArr.address3;
        this.objCustomerWorkOrder.Order_Taken_By = tempArr.order_taken_by;
        this.objCustomerWorkOrder.Remarks = tempArr.remarks;
        this.objCustomerWorkOrder.Byno_List = JSON.parse(JSON.stringify(tempArr.work_order_details));
        this.objModifyCustomerWorkOrder = JSON.parse(JSON.stringify(this.objCustomerWorkOrder));
        this.componentVisibility = !this.componentVisibility;
      }
    });
  }

  viewCustomerWorkOrder(work_order_no): void {
    let objData = {
      CustomerWorkOrder: JSON.stringify([{
        work_order_no: work_order_no,
        transfer_type: this.objLoad.Transfer_Type,
        wh_section_id: +this._localStorage.getWhSectionId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._customerWorkOrderService.fetchCustomerWorkOrder(objData).subscribe((result: any) => {
      if (result) {
        this.objAction.isEditing = false;
        this.objAction.isView = true;
        let tempArr = JSON.parse(JSON.stringify(result[0]));
        this.objCustomerWorkOrder.Work_Order_No = tempArr.work_order_no
        this.objCustomerWorkOrder.Date = this._datePipe.transform(tempArr.work_order_date, 'dd/MM/yyyy');
        this.objCustomerWorkOrder.To_Warehouse = tempArr.destination_id;
        this.objCustomerWorkOrder.Transfer_Type = tempArr.transfer_type;
        this.objCustomerWorkOrder.Bill_No = tempArr.bill_no;
        this.objCustomerWorkOrder.Amount_To_Be_Paid = tempArr.amount_to_be_paid;
        this.objCustomerWorkOrder.Delivery_Date = tempArr.delivery_date;
        this.objCustomerWorkOrder.Bonus_Card_No = tempArr.bonuc_card_no;
        this.objCustomerWorkOrder.Customer_Name = tempArr.customer_name;
        this.objCustomerWorkOrder.Phone_No = tempArr.phones;
        this.objCustomerWorkOrder.Address1 = tempArr.address1;
        this.objCustomerWorkOrder.Address2 = tempArr.address2;
        this.objCustomerWorkOrder.Address3 = tempArr.address3;
        this.objCustomerWorkOrder.Order_Taken_By = tempArr.order_taken_by;
        this.objCustomerWorkOrder.Remarks = tempArr.remarks;
        this.objCustomerWorkOrder.Byno_List = JSON.parse(JSON.stringify(tempArr.work_order_details))
        this.componentVisibility = !this.componentVisibility;
      }
    });
  }

  public openCancelConfirmationDialog(data: any): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = 'Do you want cancel this record ?';
    dialogRef.componentInstance.componentName = 'Customer Work Order';
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.cancelCustomerWorkOrder(data);
      dialogRef = null;
    });
  }

  public cancelCustomerWorkOrder(data: any) {
    let objData = {
      CustomerWorkOrder: JSON.stringify([{
        work_order_no: data.work_order_no,
        transfer_type: this.objLoad.Transfer_Type,
        wh_section_id: this._localStorage.getWhSectionId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        cancel: false,
        cancelled_reason: '',
        entered_by: +this._localStorage.intGlobalUserId()
      }]),
    }
    this._customerWorkOrderService.cancelCustomerWorkOrder(objData).subscribe((result: any) => {
      if (result) {
        this._confirmationDialogComponent.openAlertDialog("Cancel successfully", "Customer Work Order");
        this.getCustomerWorkOrder();
      }
    });
  }

  public addNewRowToBynoList(i): void {
    debugger
    if (this.objCustomerWorkOrder.Byno_List[i].byno !== '' && this.objCustomerWorkOrder.Byno_List[i].description !== '' && this.objCustomerWorkOrder.Byno_List[i].product_value !== '' && this.objCustomerWorkOrder.Byno_List[i].stone_type !== '' && this.objCustomerWorkOrder.Byno_List[i].stone_qty !== '' && this.objCustomerWorkOrder.Byno_List[i].charges !== '') {
      this.objCustomerWorkOrder.Byno_List.push(Object.assign({}, this.objBynoList));
      setTimeout(() => {
        let input = this.byno.toArray();
        input[input.length - 1].nativeElement.focus();
      }, 100);
    }
  }

  public removeRowFromBynoList(i: number): void {
    this.openRemoveConfirmationDialog('Are you sure want to remove row ' + (i + 1) + ' from the list', 'Customer Work Order', i);
  }

  public openRemoveConfirmationDialog(message: string, componentName: string, i: number): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = componentName;
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deleteRow(i);
      dialogRef = null;
    });
  }

  private deleteRow(i: number) {
    this.objCustomerWorkOrder.Byno_List.splice(i, 1);
    if (i === this.objCustomerWorkOrder.Byno_List.length) {
      setTimeout(() => {
        let inputEls = this.byno.toArray();
        inputEls[i - 1].nativeElement.focus();
      }, 100);
    } else {
      setTimeout(() => {
        let inputEls = this.byno.toArray();
        inputEls[i].nativeElement.focus();
      }, 100);
    }
  }

  onClear(exitFlag) {
    if (!this.objAction.isView) {
      if (this.checkAnyChangesMade())
        this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
      else if (exitFlag)
        this.onListClick();
    } else
      this.onListClick();
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objCustomerWorkOrder) !== (this.objAction.isEditing ? JSON.stringify(this.objModifyCustomerWorkOrder) : JSON.stringify(this.unchangedCustomerWorkOrder)))
      return true;
    else
      return false;
  }

  private openConfirmationDialog(message: string, exitFlag): void {
    debugger;
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Customer Work Order";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
    });
  }

  newClick(): void {
    if (this.rights.Add) {
      this.objAction.isEditing = false;
      this.objAction.isView = false;
      this.objCustomerWorkOrder.Byno_List = [];
      this.unchangedCustomerWorkOrder.Byno_List = [];
      this.objCustomerWorkOrder.Byno_List.push(Object.assign({}, this.objBynoList));
      this.unchangedCustomerWorkOrder.Byno_List.push(Object.assign({}, this.objBynoList));
      this.componentVisibility = !this.componentVisibility;
      this.resetScreen();
    }
  }

  public onListClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.resetScreen();
    this.getCustomerWorkOrder();
  }

  public resetScreen(): void {
    this.objAction.isEditing = false;
    this.objCustomerWorkOrder = JSON.parse(JSON.stringify(this.unchangedCustomerWorkOrder));
    this.objBynoList = JSON.parse(JSON.stringify(this.unchangedBynoList));
  }

  public openAttachFilesPopup(): void {
    let dialogRef = this._matDialog.open(MultipleFilesAttachComponent, {
      panelClass: "custom-dialog-container",
      width: '53vw',
      data: {
        objAttach: JSON.stringify(this.objAttachView),
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      debugger;
      if (result) {
        this.fileIndex = [];
        debugger;
        this.selectedFiles = result.selectedFiles;
        this.fileIndex = JSON.parse(JSON.stringify(result.fileIndex));
        this.removedFileIndex = JSON.parse(JSON.stringify(result.removedFileIndex));
        this.removedFiles = JSON.parse(JSON.stringify(result.removedFiles));
        this.objAttachView = JSON.parse(JSON.stringify(result.objAttachView));
        if (this.objCustomerWorkOrder.File_Path1 !== this.objAttachView.file_path1)
          this.objCustomerWorkOrder.File_Path1 = '';
        if (this.objCustomerWorkOrder.File_Path2 !== this.objAttachView.file_path2)
          this.objCustomerWorkOrder.File_Path2 = '';
        if (this.objCustomerWorkOrder.File_Path3 !== this.objAttachView.file_path3)
          this.objCustomerWorkOrder.File_Path3 = '';
        if (this.objCustomerWorkOrder.File_Path4 !== this.objAttachView.file_path4)
          this.objCustomerWorkOrder.File_Path4 = '';
        debugger;
        this.addCustomerWorkOrderDocuments();
      }
    });
  }

  public addCustomerWorkOrderDocuments(): void {
    let AttachFiles = this.selectedFiles;
    const formData = new FormData();
    for (var i in AttachFiles) {
      formData.append("files", AttachFiles[i]);
    }
    debugger;
    formData.append('CustomerWorkOrder', JSON.stringify(this.objCustomerWorkOrder));
    formData.append("fileRootPath", this._localStorage.getInventoryDocumentPath());
    formData.append("fileIndex", JSON.stringify(this.fileIndex));
    formData.append('removedFiles', JSON.stringify(this.removedFiles));
    formData.append('removedFileIndex', JSON.stringify(this.removedFileIndex));
    this._customerWorkOrderService.addCustomerWorkOrderDocuments(formData).subscribe((result: boolean) => {
      if (result) {
        this._confirmationDialogComponent.openAlertDialog('Attachment saved successfully', 'Customer Work Order');
        this.resetAttachScreen();
        this.objAction.isAttach = false;
      }
    });
  }

  private resetAttachScreen() {
    this.selectedFiles = [];
    this.fileIndex = [];
    this.isCancelled = false;
    this.objAction = JSON.parse(JSON.stringify(this.unChangedAction));
    this.objAttachView = JSON.parse(JSON.stringify(this.unchangedAttach));
  }

  public getProductValue(): number {
    return this.objCustomerWorkOrder.Byno_List.map(t => +t.saree_value).reduce((acc, value) => acc + value, 0);
  }

  public getCharges(): number {
    return this.objCustomerWorkOrder.Byno_List.map(t => +t.charges).reduce((acc, value) => acc + value, 0);
  }

  public openImageCapture(i: number): void {
    let dialogRef = this._dialog.open(ImagecaptureComponent, {
      width: "800px",
      data: {},
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let tempObj = result;
        this.objCustomerWorkOrder.Byno_List[i].image_path = tempObj.uploadedFileName;
        this.selectedFiles.push(tempObj.selectedDocument);
        this.fileIndex.push(i);
      }
    });
  }

  /*************************************************** Exports ************************************************/

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'WO No': this.dataSource.data[i].work_order_no,
          'WO Date': this.dataSource.data[i].work_order_date,
          "Customer Name": this.dataSource.data[i].customer_name,
          "Product Qty": this.dataSource.data[i].no_of_sarees,
          'Product Value': this.dataSource.data[i].total_saree_value,
          "Status": this.dataSource.data[i].status,
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Customer Work Order",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Customer Work Order");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.work_order_no);
        tempObj.push(this._datePipe.transform(e.work_order_date, 'dd/MM/yyyy'));
        tempObj.push(e.customer_name);
        tempObj.push(e.no_of_sarees);
        tempObj.push(e.total_saree_value);
        tempObj.push(e.status);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['WO No', 'WO Date', 'Customer Name', 'Product Qty', 'Product Value', 'Status']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Customer Work Order' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Customer Work Order");
  }

}
