import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { EstimatedVsReceivedComponent } from '../estimated-vs-received/estimated-vs-received.component';
import { ReceiptDetailsComponent } from '../receipt-details/receipt-details.component';
import { SizeDetailsComponent } from '../size-details/size-details.component';

@Component({
  selector: 'app-production-issue-with-image',
  templateUrl: './production-issue-with-image.component.html',
  styleUrls: ['./production-issue-with-image.component.scss'],
  providers: [DatePipe]
})
export class ProductionIssueWithImageComponent implements OnInit {

  componentVisibility: boolean = true;
  isReconciliation: boolean = false;
  Date: any = new Date();
  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  fromDate1: any = new Date();
  toDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  // dataSource: any = new MatTableDataSource([]);
  dataSource = ELEMENT_DATA;
  displayedColumns = ["Serial_No", "Issue_No", "Issue_Date", "Issue_Type", "Remarks", "Status", "Action"];

  constructor(public _localStorage: LocalStorage,
    public _router: Router, private _datePipe: DatePipe,public _dialog: MatDialog) {
    this.load.FromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
  }

  ngOnInit() {
  }

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }

  newClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.isReconciliation = false;
  }

  onClear(): void {
    this.componentVisibility = !this.componentVisibility;
    this.isReconciliation = false;
  }

  listClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.isReconciliation = false;
  }
  public onClearReconciliation() {
    this.isReconciliation = false;
    this.componentVisibility = !this.componentVisibility;
  }
  public reconciliationClick() {  
    this.isReconciliation = true;
    this.componentVisibility = false;
  }

  public openReceiptDialog(): void {
    let dialogRef = this._dialog.open(ReceiptDetailsComponent, {
      width: "950px",
      data: {
      },
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    });
  }

  public openEstimatedVSReceiptDialog(): void {
    let dialogRef = this._dialog.open(EstimatedVsReceivedComponent, {
      width: "800px",
      data: {
      },
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    });
  }

  public openSizeDialog(): void {
    let dialogRef = this._dialog.open(SizeDetailsComponent, {
      width: "500px",
      data: {
      },
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    });
  }

}

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'}
];