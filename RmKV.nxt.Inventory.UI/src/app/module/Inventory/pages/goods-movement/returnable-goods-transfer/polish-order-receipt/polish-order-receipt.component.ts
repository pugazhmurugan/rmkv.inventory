import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog, MatDialogRef, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode } from 'src/app/common/models/common-model';
import { AccountsLookupService } from 'src/app/common/services/accounts-lookup/accounts-lookup.service';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { GridKeyEvents } from 'src/app/common/shared/directives/grid-key-events';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ReasonComponent } from 'src/app/common/shared/reason/reason.component';
import { PolishReceipt, PolishReceiptGridList } from 'src/app/module/Inventory/model/goodsMovement/returnableGoodsTransfer/polish-order-receipt';
import { PolishOrderReceiptService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer/polish-order-receipt/polish-order-receipt.service';
import { InvoicesService } from 'src/app/module/Inventory/services/purchases/invoices/invoices.service';
import { BynoSelectionPolishOrderReceiptComponent } from '../byno-selection-polish-order-receipt/byno-selection-polish-order-receipt.component';
import { SizeAndProductComponent } from '../size-and-product/size-and-product.component';
declare var jsPDF: any;

@Component({
  selector: 'app-polish-order-receipt',
  templateUrl: './polish-order-receipt.component.html',
  styleUrls: ['./polish-order-receipt.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class PolishOrderReceiptComponent implements OnInit {

  componentVisibility: boolean = true;
  isBynoDetails: boolean = false;
  focusFlag: boolean = false;
  loadSupplierLookupList: any[] = [];
  receivedSupplierLookupList: any[] = [];
  invSupplierLookupList: any[] = [];
  edit_data: any;
  polishReceiptLoad: any = {
    from_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    to_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    status: 'All',
    supplier_code: '',
    supplier_name: ''
  }

  flags: any = {
    isIgst: true,
    isComposite: false,
  }

  objPolishReceipts: PolishReceipt = {
    inv_no: '',
    receipt_no: 0,
    polish_order_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    inv_supplier_code: '',
    inv_supplier_name: '',
    inv_supplier_gstn_no: '',
    invoice_no: '',
    invoice_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    // invoice_amount: 0,
    issue_type: "'Stone'",
    taxable_amount: '0.00',
    other_amount: '0.00',
    sgst_amount: '0.00',
    sgst: '0.00',
    sgst_total: 0,
    igst_amount: '0.00',
    igst: '0.00',
    igst_total: 0,
    cgst_amount: '0.00',
    cgst: '0.00',
    cgst_total: 0,
    round_off: '0.00',
    total_invoice_amount: '0.00',
    additions: '0.00',
    deductions: '0.00',
    remarks: '',
    file_path1: '',
    file_path2: '',
    file_path3: '',
    file_path4: ''
  }

  modifyPolishReceipts: PolishReceipt = {
    inv_no: '',
    receipt_no: 0,
    polish_order_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    inv_supplier_code: '',
    inv_supplier_name: '',
    inv_supplier_gstn_no: '',
    invoice_no: '',
    invoice_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    // invoice_amount: 0,
    issue_type: 'Stone',
    taxable_amount: '0.00',
    other_amount: '0.00',
    sgst_amount: '0.00',
    sgst: '0.00',
    sgst_total: 0,
    igst_amount: '0.00',
    igst: '0.00',
    igst_total: 0,
    cgst_amount: '0.00',
    cgst: '0.00',
    cgst_total: 0,
    round_off: '0.00',
    total_invoice_amount: '0.00',
    additions: '0.00',
    deductions: '0.00',
    remarks: '',
    file_path1: '',
    file_path2: '',
    file_path3: '',
    file_path4: ''
  }

  unChangedPolishReceipts: PolishReceipt = {
    inv_no: '',
    receipt_no: 0,
    polish_order_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    inv_supplier_code: '',
    inv_supplier_name: '',
    inv_supplier_gstn_no: '',
    invoice_no: '',
    invoice_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    // invoice_amount: 0,
    issue_type: 'Stone',
    taxable_amount: '0.00',
    other_amount: '0.00',
    sgst_amount: '0.00',
    sgst: '0.00',
    sgst_total: 0,
    igst_amount: '0.00',
    igst: '0.00',
    igst_total: 0,
    cgst_amount: '0.00',
    cgst: '0.00',
    cgst_total: 0,
    round_off: '0.00',
    total_invoice_amount: '0.00',
    additions: '0.00',
    deductions: '0.00',
    remarks: '',
    file_path1: '',
    file_path2: '',
    file_path3: '',
    file_path4: ''
  }


  objAction: EditMode = {
    isEditing: false,
    isView: false
  }

  objUnChangedAction: EditMode = {
    isEditing: false,
    isView: false
  }

  polishReceiptDetailsList: PolishReceiptGridList[] = [{
    issue_no: '',
    byno: '',
    byno_prod_serial: '',
    byno_serial: '',
    charges: 0,
    checked: false,
    description: '',
    image_path: '',
    inv_byno: '',
    issue_date: new Date(),
    issued_qty: '',
    returned: false,
    serial_no: 0,
    stone_qty: 0,
    stone_type: '',
    transfer_type: "I",
    conversion_charges: '',
    hsn_code: '',
    igst: 0,
    igst_amount: 0,
    cgst: 0,
    cgst_amount: 0,
    sgst: 0,
    sgst_amount: 0,
    total_charges: 0
  }];

  modifyPolishReceiptDetailsList: PolishReceiptGridList[] = [{
    issue_no: '',
    byno: '',
    byno_prod_serial: '',
    byno_serial: '',
    charges: 0,
    checked: false,
    description: '',
    image_path: '',
    inv_byno: '',
    issue_date: new Date(),
    issued_qty: '',
    returned: false,
    serial_no: 0,
    stone_qty: 0,
    stone_type: '',
    transfer_type: "I",
    conversion_charges: '',
    hsn_code: '',
    igst: 0,
    igst_amount: 0,
    cgst: 0,
    cgst_amount: 0,
    sgst: 0,
    sgst_amount: 0,
    total_charges: 0
  }];

  unChangedPolishReceiptDetailsList: PolishReceiptGridList[] = [{
    issue_no: '',
    byno: '',
    byno_prod_serial: '',
    byno_serial: '',
    charges: 0,
    checked: false,
    description: '',
    image_path: '',
    inv_byno: '',
    issue_date: new Date(),
    issued_qty: '',
    returned: false,
    serial_no: 0,
    stone_qty: 0,
    stone_type: '',
    transfer_type: "I",
    conversion_charges: '',
    hsn_code: '',
    igst: 0,
    igst_amount: 0,
    cgst: 0,
    cgst_amount: 0,
    sgst: 0,
    sgst_amount: 0,
    total_charges: 0
  }];

  polishReceiptIssueNosList: any[] = [];
  modifyPolishReceiptIssueNosList: any[] = [];
  unChangedPolishReceiptIssueNosList: any[] = [];
  fromDate1: any = new Date();
  toDate1: any = new Date();
  invoiceDate: any = new Date();
  polishDate: any = new Date();

  public dateValidation: DateValidation = new DateValidation();
  OldDocumentPath: any = '';
  document: any;
  tempFile: any = '';
  formData: any;
  isFileSelected: boolean = false;
  selectedFiles: File[] | any[] = [];
  unChangedFiles: File[] | any[] = [];
  AttachFiles: any = [];
  isFileAttach: number;
  fileIndex: any = [];
  selectedDocument: any;
  @ViewChild('fileInput', null) fileInput: ElementRef;
  isCancelled: boolean = false;
  removedFileIndex: number[] = [];
  removedFiles: string[] = [];
  inputEls: any[];

  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Receipt_No", "Receipt_Date", "Supplier_Name", "Quantity", "Action"];

  columns: any[] = [
    { display: "sno", editable: false },
    { display: "issueNo", editable: false },
    { display: "byNo", editable: false },
    { display: "image", editable: false },
    { display: "desc", editable: false },
    { display: "stoneType", editable: false },
    { display: "stoneQty", editable: false },
    { display: "charge", editable: false },
    { display: "conversion", editable: true },
    { display: "hsnCode", editable: true },
    { display: "igst", editable: false },
    { display: "cgst", editable: false },
    { display: "sgst", editable: false },
    { display: "total", editable: false }
  ]

  @ViewChildren("issueNo") issueNo: ElementRef | any;
  @ViewChildren("byNo") byNo: ElementRef | any;
  @ViewChildren("image") image: ElementRef | any;
  @ViewChildren("desc") desc: ElementRef | any;
  @ViewChildren("stoneType") stoneType: ElementRef | any;
  @ViewChildren("stoneQty") stoneQty: ElementRef | any;
  @ViewChildren("charge") charge: ElementRef | any;
  @ViewChildren("conversion") conversion: ElementRef | any;
  @ViewChildren("hsnCode") hsnCode: ElementRef | any;
  @ViewChildren("igst") igst: ElementRef | any;
  @ViewChildren("cgst") cgst: ElementRef | any;
  @ViewChildren("sgst") sgst: ElementRef | any;
  @ViewChildren("total") total: ElementRef | any;


  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {
    switch (event.keyCode) {
      case 13: // Enter Key
        this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.polishReceiptDetailsList);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.polishReceiptDetailsList);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.polishReceiptDetailsList);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.polishReceiptDetailsList);
        break;
    }
  }

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    private _datePipe: DatePipe,
    private _matDialog: MatDialog,
    public _dialog: MatDialog,
    public _keyPressEvents: KeyPressEvents,
    private _confirmationDialog: ConfirmationDialogComponent,
    private _accountsLookupService: AccountsLookupService,
    private _polishOrderReceiptService: PolishOrderReceiptService,
    public _invoicesService: InvoicesService,
    public _decimalPipe: DecimalPipe,
    public _gridKeyEvents: GridKeyEvents,
    public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _excelService: ExcelService
  ) {
    this.polishReceiptLoad.from_date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 7)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 7)));
    this.getSupplierLookupList();
  }

  ngOnInit() {
  }

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.polishReceiptLoad.from_date, this.fromDate1, this.minDate, this.maxDate);
    this.polishReceiptLoad.from_date = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.polishReceiptLoad.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.polishReceiptLoad.to_date = date[0];
    this.toDate1 = date[1];
  }

  public validateInvoiceDate(): void {
    let date = this.dateValidation.validateDate(this.objPolishReceipts.invoice_date, this.invoiceDate, this.minDate, this.maxDate);
    this.objPolishReceipts.invoice_date = date[0];
    this.invoiceDate = date[1];
  }

  public validatePolishDate(): void {
    let date = this.dateValidation.validateDate(this.objPolishReceipts.polish_order_date, this.polishDate, this.minDate, this.maxDate);
    this.objPolishReceipts.polish_order_date = date[0];
    this.polishDate = date[1];
  }

  /************************************************* Lookup Functionalities **************************************************/

  private getSupplierLookupList(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._accountsLookupService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      debugger;
      if (result) {
        this.receivedSupplierLookupList = JSON.parse(result);
        this.loadSupplierLookupList = JSON.parse(result);
        this.invSupplierLookupList = JSON.parse(result);
        this.receivedSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
        this.loadSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
        this.invSupplierLookupList.forEach(x => {
          x.account_code = x.account_code.toString().trim();
          x.account_name = x.account_name.toString().trim();
        });
      }
    });
  }

  // public openReceivedSupplierLookup(event: KeyboardEvent | any): void {
  //   debugger;
  //   if (event.keyCode === 13 && !this.validateReceivedSupplierCode()) {
  //     this.openReceivedSupplierLookupDialog();
  //   }
  // }

  // private openReceivedSupplierLookupDialog(): void {
  //   this.focusFlag = true;
  //   const dialogRef = this._matDialog.open(AccountsLookupComponent, {
  //     width: "550px",
  //     panelClass: "custom-dialog-container",
  //     data: {
  //       searchString: this.objPolishReceipts.received_supplier_code,
  //       For: 1
  //     }
  //   });
  //   dialogRef.afterClosed().subscribe((result: any) => {
  //     this.focusFlag = false;
  //     if (result) {
  //       debugger;
  //       this.objPolishReceipts.received_supplier_code = result.account_code.toString().trim();
  //       this.objPolishReceipts.received_supplier_name = result.account_name.toString().trim();
  //     } else {
  //       this.objPolishReceipts.received_supplier_code = '';
  //       this.objPolishReceipts.received_supplier_name = '';
  //     }
  //   });
  // }

  // public onEnterReceivedSupplierLookupDetails(): void {
  //   debugger;
  //   let supplierLookupList: any = {
  //     account_code: "",
  //     account_name: "",
  //   };
  //   supplierLookupList = this.receivedSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objPolishReceipts.received_supplier_code.toString().trim().toLowerCase());
  //   this.objPolishReceipts.received_supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objPolishReceipts.received_supplier_code.toString().trim();
  //   this.objPolishReceipts.received_supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
  // }

  // public checkValidReceivedSupplierName(event: KeyboardEvent, focus: string): any {
  //   if (event.keyCode !== 13 && !this.focusFlag && this.objPolishReceipts.received_supplier_code.toString().trim() !== '' && this.objPolishReceipts.received_supplier_name.toString().trim() === '') {
  //     this.openAlertDialog("Invalid supplier name", "Stone/Polish Order Receipt", focus);
  //   }
  // }

  // public validateReceivedSupplierCode(): boolean {
  //   let index = this.receivedSupplierLookupList.findIndex(element => element.account_code.toString().trim() === this.objPolishReceipts.received_supplier_code.toString().trim());
  //   if (index === -1)
  //     return false;
  //   else return true;
  // }

  public openInvSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateInvSupplierCode()) {
      this.openInvSupplierLookupDialog();
    }
  }

  private openInvSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "550px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objPolishReceipts.inv_supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objPolishReceipts.inv_supplier_code = result.account_code.toString().trim();
        this.objPolishReceipts.inv_supplier_name = result.account_name.toString().trim();
        this.objPolishReceipts.inv_supplier_gstn_no = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(result.gstn_no) !== -1 ? '' : result.gstn_no;
      } else {
        this.objPolishReceipts.inv_supplier_code = '';
        this.objPolishReceipts.inv_supplier_name = '';
        this.objPolishReceipts.inv_supplier_gstn_no = '';
      }
      this.setTypeOfGst();
    });
  }

  public onEnterInvSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      gstn_no: ""
    };
    supplierLookupList = this.invSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objPolishReceipts.inv_supplier_code.toString().trim().toLowerCase());
    this.objPolishReceipts.inv_supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objPolishReceipts.inv_supplier_code.toString().trim();
    this.objPolishReceipts.inv_supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
    this.objPolishReceipts.inv_supplier_gstn_no = supplierLookupList && supplierLookupList.gstn_no ? supplierLookupList.gstn_no : '';
    this.setTypeOfGst();
  }

  public checkValidInvSupplierName(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objPolishReceipts.inv_supplier_code.toString().trim() !== '' && this.objPolishReceipts.inv_supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier name", "Stone/Polish Order Receipt", focus);
    }
  }

  public validateInvSupplierCode(): boolean {
    let index = this.invSupplierLookupList.findIndex(element => element.account_code.toString().trim() === this.objPolishReceipts.inv_supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }

  public openLoadSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateLoadSupplierCode()) {
      this.openLoadSupplierLookupDialog();
    }
  }

  private openLoadSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "700px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.polishReceiptLoad.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      this.matTableConfig();
      if (result) {
        debugger;
        this.polishReceiptLoad.supplier_code = result.account_code.toString().trim();
        this.polishReceiptLoad.supplier_name = result.account_name.toString().trim();
      } else {
        this.polishReceiptLoad.supplier_code = '';
        this.polishReceiptLoad.supplier_name = '';
      }
    });
  }

  public onEnterLoadSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
    };
    this.matTableConfig();
    supplierLookupList = this.loadSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.polishReceiptLoad.supplier_code.toString().trim().toLowerCase());
    this.polishReceiptLoad.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.polishReceiptLoad.supplier_code.toString().trim();
    this.polishReceiptLoad.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';
  }

  public checkValidLoadSupplierName(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.polishReceiptLoad.supplier_code.toString().trim() !== '' && this.polishReceiptLoad.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier name", "Stone/Polish Order Receipt", focus);
    }
  }

  public validateLoadSupplierCode(): boolean {
    let index = this.loadSupplierLookupList.findIndex(element => element.account_code.toString().trim() === this.polishReceiptLoad.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }


  /***************************************************** CRUD Functions ***************************************************/

  public loadPolishOrderReceipts(): void {
    if (this.beforeLoadValidate()) {
      let objLoad: any = {
        PolishOrderReceipt: JSON.stringify([{
          from_date: this.polishReceiptLoad.from_date,
          to_date: this.polishReceiptLoad.to_date,
          wh_section_id: +this._localStorage.getWhSectionId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          supplier_code: this.polishReceiptLoad.supplier_code.toString().trim(),
          status: this.polishReceiptLoad.status
        }])
      }
      this._polishOrderReceiptService.getPolishOrderReceipts(objLoad).subscribe((result: any[]) => {
        result ? this.matTableConfig(JSON.parse(JSON.stringify(result))) : this._confirmationDialog.openAlertDialog('No records found', 'Stone/Polish Order Receipt');
        console.log(JSON.parse(JSON.stringify(result)), 'load list')
      });
    }
  }

  public onClickDelete(index: any): any {
    this._dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    this._dialogRef.componentInstance.confirmMessage =
      "Do you want to cancel the polish order receipt?"
    this._dialogRef.componentInstance.componentName = "Stone/Polish Order Receipt";
    return this._dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.openReasonDialog(index);
      }
      this._dialogRef = null;
    });
  }

  public openReasonDialog(index: number): void {
    debugger;
    let dialogRef = this._matDialog.open(ReasonComponent, {
      width: "75vw",
      panelClass: "custom-dialog-container",
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== null) {
        this.deletePolishReceipt(index, result);
      }
      this._dialogRef = null;
    });
  }

  private deletePolishReceipt(index: number, Reason: string): void {
    debugger;
    let objData = [{
      wh_section_id: +this._localStorage.getWhSectionId(),
      receipt_no: +this.dataSource.data[index].receipt_no,
      cancel: false,
      entered_by: +this._localStorage.intGlobalUserId(),
      cancelled_reason: Reason,
      company_section_id: +this._localStorage.getCompanySectionId()
    }];
    let objSave = {
      PolishOrderReceipt: JSON.stringify(objData)
    }
    debugger;
    this._polishOrderReceiptService.removePolishOrderReceipts(objSave).subscribe(
      (result: any) => {
        if (result !== null) {
          this._confirmationDialog.openAlertDialog("Added to cancelled list", "Stone/Polish Order Receipt");
          this.loadPolishOrderReceipts()
        }
      });
    this._dialogRef = null;
  }


  public onChangeSupplierDetails(): void {
    this.polishReceiptIssueNosList = [];
    this.polishReceiptDetailsList = [];
    this.objPolishReceipts.taxable_amount = 0;
    this.objPolishReceipts.sgst_amount = '0.00';
    this.objPolishReceipts.igst_amount = '0.00';
    this.objPolishReceipts.cgst_amount = '0.00';
    this.objPolishReceipts.total_invoice_amount = '0.00';
    this.objPolishReceipts.other_amount = '0.00';
    this.objPolishReceipts.round_off = '0.00';
  }

  ///////////////////////////// Calculations ///////////////////////////////////////////

  public getTaxableAmountTotal(): void {
    this.objPolishReceipts.taxable_amount =
      this.polishReceiptDetailsList.map(t => (+t.conversion_charges)).reduce((acc, value) => acc + value, 0);
  }

  public getGstByHsnCode(i: number): void {
    if (!this.flags.isComposite && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.polishReceiptDetailsList[i].hsn_code) === -1) {
      let objGet = {
        GstByHSN: JSON.stringify([{
          supplier_gstn: this.objPolishReceipts.inv_supplier_gstn_no,
          company_section_id: this._localStorage.getCompanySectionId(),
          hsn: this.polishReceiptDetailsList[i].hsn_code,
          supplier_inv_date: this.objPolishReceipts.invoice_date,
          cost_price: this.polishReceiptDetailsList[i].conversion_charges,
        }])
      }
      this._invoicesService.getGstByHSNCode(objGet).subscribe((result: any) => {
        if (result) {
          debugger;
          this.polishReceiptDetailsList[i].sgst = JSON.parse(JSON.stringify(result))[0].sgst;
          this.polishReceiptDetailsList[i].cgst = JSON.parse(JSON.stringify(result))[0].cgst;
          this.polishReceiptDetailsList[i].igst = JSON.parse(JSON.stringify(result))[0].igst;
          this.calculateTotalCharges(i);
        } else {
          this.polishReceiptDetailsList[i].igst = 0;
          this.polishReceiptDetailsList[i].cgst = 0;
          this.polishReceiptDetailsList[i].sgst = 0;
          this.openInvalidAlertDialog('Invalid hsn code', i, 'Polish Order Receipt', 'hsnCode');
        }
      });
    }
  }

  private getGstTotal(i: number, conversionCharges?: number): number {
    debugger;
    if (this.flags.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.polishReceiptDetailsList[i].igst) === -1) {
        this.polishReceiptDetailsList[i].igst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.polishReceiptDetailsList[i].igst) === -1 ? conversionCharges * (+this.polishReceiptDetailsList[i].igst / 100) : 0;
        return +this.polishReceiptDetailsList[i].igst_amount;
      }
    } else if (!this.flags.isIgst) {
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.polishReceiptDetailsList[i].cgst) === -1 &&
        [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(+this.polishReceiptDetailsList[i].sgst) === -1) {
        this.polishReceiptDetailsList[i].cgst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.polishReceiptDetailsList[i].cgst) === -1 ? conversionCharges * (+this.polishReceiptDetailsList[i].cgst / 100) : 0;
        this.polishReceiptDetailsList[i].sgst_amount = [null, undefined, NaN, 0, '0'].indexOf(this.polishReceiptDetailsList[i].sgst) === -1 ? conversionCharges * (+this.polishReceiptDetailsList[i].sgst / 100) : 0;
        return +(+this.polishReceiptDetailsList[i].cgst_amount + +this.polishReceiptDetailsList[i].sgst_amount);
      }
    }
  }

  public calculateTotalCharges(i: number, editableColumn?: string): void {
    debugger;
    let conversionCharges: any = this.polishReceiptDetailsList[i].conversion_charges;
    let gst_total: number = this.getGstTotal(i, conversionCharges);
    if (conversionCharges === 0 || conversionCharges === '') {
      this.polishReceiptDetailsList[i].hsn_code = '';
      this.polishReceiptDetailsList[i].igst = 0;
      this.polishReceiptDetailsList[i].cgst = 0;
      this.polishReceiptDetailsList[i].sgst = 0;
      this.polishReceiptDetailsList[i].total_charges = 0;
      this.objPolishReceipts.other_amount = 0;
      this.objPolishReceipts.round_off = 0;
    } else
      if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(conversionCharges) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(gst_total) === -1) {
        this.polishReceiptDetailsList[i].total_charges = +conversionCharges + +gst_total;
      }
    this.getTaxableAmountTotal();
    if (this.flags.isIgst)
      this.getIgstGrandTotal();
    else if (!this.flags.isIgst) {
      this.getCgstGrandTotal();
      this.getSgstGrandTotal();
    }
    this.getInvoiceGrandTotal(editableColumn);
    this.convertToDecimal(i);
  }

  private getIgstGrandTotal(): void {
    this.objPolishReceipts.igst_total = 0;
    this.objPolishReceipts.igst_total = this.polishReceiptDetailsList.map(t => +t.igst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getCgstGrandTotal(): void {
    this.objPolishReceipts.cgst_total = 0;
    this.objPolishReceipts.cgst_total = this.polishReceiptDetailsList.map(t => +t.cgst_amount).reduce((acc, value) => acc + value, 0);
  }

  private getSgstGrandTotal(): void {
    this.objPolishReceipts.sgst_total = 0;
    this.objPolishReceipts.sgst_total = this.polishReceiptDetailsList.map(t => +t.sgst_amount).reduce((acc, value) => acc + value, 0);
  }

  private convertToDecimal(i: number): void {
    this.polishReceiptDetailsList[i].total_charges =
      [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.polishReceiptDetailsList[i].total_charges.toString().trim()) === -1 ?
        (this._decimalPipe.transform(+this.polishReceiptDetailsList[i].total_charges, '1.2-2')).replace(/,/g, '') : "0.00";
  }

  private openInvalidAlertDialog(value: string, i: number, componentName: string, focus: any) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this[focus].toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }

  public getInvoiceGrandTotal(editableColumn?: string): number {
    debugger;
    let grandTotal: number = this.polishReceiptDetailsList.map(t => (+t.total_charges)).reduce((acc, value) => acc + value, 0);
    this.objPolishReceipts.total_invoice_amount = (+grandTotal) + +this.objPolishReceipts.additions - +this.objPolishReceipts.deductions;
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', ''].indexOf(this.objPolishReceipts.round_off) === -1) {
      if (this.objPolishReceipts.round_off.toString().includes('.')) {
        if (+(this.objPolishReceipts.round_off.toString().split('.')[1]) > 0) {
          this.objPolishReceipts.total_invoice_amount = +this.objPolishReceipts.total_invoice_amount + +(this.objPolishReceipts.round_off);
        } else if (+(this.objPolishReceipts.round_off.toString().split('.')[1]) < 0) {
          this.objPolishReceipts.total_invoice_amount = +this.objPolishReceipts.total_invoice_amount - +(this.objPolishReceipts.round_off);
        } else if (+(this.objPolishReceipts.round_off.toString().split('.')[1]) === 0) {
          this.objPolishReceipts.total_invoice_amount = +this.objPolishReceipts.total_invoice_amount + 0;
        }
      } else if (+(this.objPolishReceipts.round_off.toString())) {
        this.objPolishReceipts.total_invoice_amount = +this.objPolishReceipts.total_invoice_amount +
          +(this.objPolishReceipts.round_off);
      }
    }
    this.convertTotalsToDecimal(editableColumn);
    return +this.objPolishReceipts.total_invoice_amount;
  }

  public convertTotalsToDecimal(editableColumn: string): void {
    this.objPolishReceipts.sgst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objPolishReceipts.sgst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objPolishReceipts.sgst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objPolishReceipts.cgst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objPolishReceipts.cgst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objPolishReceipts.cgst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objPolishReceipts.igst_total = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objPolishReceipts.igst_total.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objPolishReceipts.igst_total, '1.2-2')).replace(/,/g, '') : "0.00";
    this.objPolishReceipts.total_invoice_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objPolishReceipts.total_invoice_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objPolishReceipts.total_invoice_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'OtherAmount')
      this.objPolishReceipts.other_amount = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objPolishReceipts.other_amount.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objPolishReceipts.other_amount, '1.2-2')).replace(/,/g, '') : "0.00";
    if (editableColumn !== 'RoundOff')
      this.objPolishReceipts.round_off = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.objPolishReceipts.round_off.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.objPolishReceipts.round_off, '1.0-0')).replace(/,/g, '') : "0.00";
    // if (editableColumn !== 'TotalInvoiceAmount')
  }

  ///////////////////////////////////////////////////////////////////////////////////////////

  public getPendingPolishIssueNos(value: number, isModify?: boolean, records?: any): void {
    debugger;
    if (this.beforeLoadIssueNosValidate()) {
      let objLoad: any = {
        PolishOrderReceipt: JSON.stringify([{
          supplier_code: this.objPolishReceipts.inv_supplier_code.toString().trim(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          transfer_type: 'I',
          wh_section_id: +this._localStorage.getWhSectionId(),
          issue_type: this.objPolishReceipts.issue_type
        }])
      }
      this._polishOrderReceiptService.getPendingPolishIssueNosList(objLoad).subscribe((result: any[]) => {
        if (result !== null) {
          debugger;
          if (value == 0) {
            this.polishReceiptIssueNosList = [];
          }
          this.polishReceiptIssueNosList = JSON.parse(JSON.stringify(result));
          this.polishReceiptIssueNosList.forEach(ele => {
          ele.isSelected = false;
           
          });
          console.log(JSON.parse(JSON.stringify(result)), "Issue Nos List");
        }else if (isModify && result === null) {
          this.polishReceiptIssueNosList = []
          this.modifyPolishReceiptIssueNosList = []
        }
        else
          this._confirmationDialog.openAlertDialog('No pending issues found', 'Stone/Polish Order Receipt');
      });
    }
  }

  private checkGridListEmptyRow(index: number): boolean {
    debugger;
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', "", 0, '0.00'].indexOf(this.polishReceiptDetailsList[index].conversion_charges) !== -1) {
      this.openAlertDialog('Enter conversion charges', 'Stone/Polish Order Receipt', 'conversion');
      return false;
    } else if ([null, 'null', undefined, 'undefined', NaN, 'NaN', "", 0, '0.00'].indexOf(this.polishReceiptDetailsList[index].hsn_code) !== -1) {
      this.openAlertDialog('Enter hsn code', 'Stone/Polish Order Receipt', 'hsnCode');
      return false;
    } else if ([null, 'null', undefined, 'undefined', NaN, 'NaN', "", 0, '0.00'].indexOf(this.polishReceiptDetailsList[index].total_charges) !== -1) {
      this.openAlertDialog('Enter valid hsn code', 'Stone/Polish Order Receipt', 'hsnCode');
      return false;
    } else return true;
  }

  //////////////////////////////// Save ////////////////////////////////////////////////

  public onSavePolishOrderReceipt(): void {
    debugger;
    let details: any = [];
    let currentlyExist = 0;
    if (this.beforeSaveValidate()) {
      for (let i = 0; i < this.polishReceiptDetailsList.length; i++) {
        if (!this.checkGridListEmptyRow(i)) {
          currentlyExist = 1;
          return;
        } if (currentlyExist !== 1) {
          details.push({
            serial_no: i + 1,
            issue_wh_section_id: +this._localStorage.getWhSectionId(),
            issue_no: +this.polishReceiptDetailsList[i].issue_no,
            issue_serial_no: i + 1,
            inv_byno: this.polishReceiptDetailsList[i].inv_byno,
            byno_serial: this.polishReceiptDetailsList[i].byno_serial,
            byno_prod_serial: this.polishReceiptDetailsList[i].byno_prod_serial,
            stone_type: this.polishReceiptDetailsList[i].stone_type,
            stone_qty: +this.polishReceiptDetailsList[i].stone_qty,
            charges: +this.polishReceiptDetailsList[i].charges,
            conversion_charges: +this.polishReceiptDetailsList[i].conversion_charges,
            hsn: this.polishReceiptDetailsList[i].hsn_code,
            cgst: +this.polishReceiptDetailsList[i].cgst,
            cgst_amount: this.polishReceiptDetailsList[i].cgst_amount !== undefined
              ? this.polishReceiptDetailsList[i].cgst_amount : 0,
            sgst: +this.polishReceiptDetailsList[i].sgst,
            sgst_amount: this.polishReceiptDetailsList[i].sgst_amount !== undefined
              ? this.polishReceiptDetailsList[i].sgst_amount : 0,
            igst: +this.polishReceiptDetailsList[i].igst,
            igst_amount: this.polishReceiptDetailsList[i].igst_amount !== undefined
              ? this.polishReceiptDetailsList[i].igst_amount : 0,
            cost_price: 0,
            margin_code: '',
            selling_price: 0,
            total_charges: +this.polishReceiptDetailsList[i].total_charges
          });
        }
      }
      let obj = {
        PolishOrderReceipt: JSON.stringify([{
          wh_section_id: +this._localStorage.getWhSectionId(),
          transfer_type: 'I',
          receipt_no: +this.objPolishReceipts.receipt_no,
          receipt_date: this.objPolishReceipts.polish_order_date,
          received_from: this.objPolishReceipts.inv_supplier_name,
          inv_supplier_code: this.objPolishReceipts.inv_supplier_code,
          inv_no: +this.objPolishReceipts.inv_no,
          inv_amount: +this.objPolishReceipts.total_invoice_amount,
          taxable_amount: +this.objPolishReceipts.taxable_amount,
          misc_addition_amt : +this.objPolishReceipts.additions,
          misc_deduction_amt : +this.objPolishReceipts.deductions,
          composite : false,
          other_amount: 0,
          cgst_amount: this.objPolishReceipts.cgst_total !== undefined ? +this.objPolishReceipts.cgst_total : 0,
          sgst_amount: this.objPolishReceipts.sgst_total !== undefined ? +this.objPolishReceipts.sgst_total : 0,
          igst_amount: this.objPolishReceipts.igst_total !== undefined ? +this.objPolishReceipts.igst_total : 0,
          round_off_amount: +this.objPolishReceipts.round_off,
          file_path: [{}],
          remarks: this.objPolishReceipts.remarks,
          entered_by: this._localStorage.intGlobalUserId(),
          details: details,
          company_section_id: this._localStorage.getCompanySectionId(),
          inv_date: this.objPolishReceipts.invoice_date
        }])
      }
      this._polishOrderReceiptService.addPolishOrderReceipts(obj).subscribe(
        (result: any) => {
          if (result) {
            this._confirmationDialog.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New polish order has been created", "Stone/Polish Order Receipt");
            this.componentVisibility = !this.componentVisibility;
            this.loadPolishOrderReceipts();
          }
        });
    }
  }

  // public onSavePolishOrderReceipt(): void {
  //   debugger;
  //   let details: any = [];
  //   const formData = new FormData();
  //   for (var i in this.selectedFiles) {
  //     formData.append("files", this.selectedFiles[i]);
  //   }
  //   // if (this.beforeSaveValidate()) {
  //   for (let i = 0; i < this.polishReceiptDetailsList.length; i++) {
  //     for (let j = 0; j < this.polishReceiptIssueNosList.length; j++) {
  //       //   if (this.checkGridListEmptyRow(i)) {
  //       //     return;
  //       //   } else if (this.getPurchaseOrderGridList.length !== 1 && this.getPurchaseOrderGridList[i].product_name.toString().trim() === '') {
  //       //     this.getPurchaseOrderGridList.splice(i, 0);
  //       //   } else
  //       {
  //         details.push({
  //           serial_no: i + 1,
  //           issue_wh_section_id: +this._localStorage.getWhSectionId(),
  //           issue_no: +this.polishReceiptIssueNosList[j].issue_no,
  //           issue_serial_no: j + 1,
  //           inv_byno: this.polishReceiptDetailsList[i].byno,
  //           byno_serial: this.polishReceiptDetailsList[i].byno,
  //           byno_prod_serial: this.polishReceiptDetailsList[i].byno,
  //           stone_type: this.polishReceiptDetailsList[i].stone_type,
  //           stone_qty: +this.polishReceiptDetailsList[i].stone_qty,
  //           charges: +this.polishReceiptDetailsList[i].charges,
  //           conversion_charges: +this.polishReceiptDetailsList[i].conversion_charges,
  //           hsn: this.polishReceiptDetailsList[i].hsn_code,
  //           cgst: +this.polishReceiptDetailsList[i].cgst,
  //           cgst_amount: +this.polishReceiptDetailsList[i].cgst_amount,
  //           sgst: +this.polishReceiptDetailsList[i].sgst,
  //           sgst_amount: +this.polishReceiptDetailsList[i].sgst_amount,
  //           igst: +this.polishReceiptDetailsList[i].igst,
  //           igst_amount: +this.polishReceiptDetailsList[i].igst_amount,
  //           cost_price: '',
  //           margin_code: '',
  //           selling_price: '',
  //           total_charges: +this.polishReceiptDetailsList[i].total_charges
  //         });
  //       }
  //     }
  //   }
  //   let obj = {
  //     wh_section_id: +this._localStorage.getWhSectionId(),
  //     transfer_type: 'I',
  //     receipt_no: '',
  //     receipt_date: this._datePipe.transform(this.objPolishReceipts.polish_order_date, 'dd/MM/yyyy'),
  //     received_from: this.objPolishReceipts.inv_supplier_name,
  //     inv_supplier_code: this.objPolishReceipts.inv_supplier_code,
  //     inv_no: this.objPolishReceipts.inv_no,
  //     inv_amount: +this.objPolishReceipts.invoice_amount,
  //     taxable_amount: +this.objPolishReceipts.taxable_amount,
  //     other_amount: +this.objPolishReceipts.other_amount,
  //     cgst_amount: +this.objPolishReceipts.cgst_total,
  //     sgst_amount: +this.objPolishReceipts.sgst_total,
  //     igst_amount: +this.objPolishReceipts.igst_total,
  //     round_off_amount: +this.objPolishReceipts.round_off,
  //     file_path: [{
  //       file_path1: this.objPolishReceipts.file_path1,
  //       file_path2: this.objPolishReceipts.file_path2,
  //       file_path3: this.objPolishReceipts.file_path3,
  //       file_path4: this.objPolishReceipts.file_path4
  //     }],
  //     remarks: this.objPolishReceipts.remarks,
  //     entered_by: this._localStorage.intGlobalUserId(),
  //     details: details,
  //     company_section_id: this._localStorage.getCompanySectionId(),
  //     inv_date: this.objPolishReceipts.invoice_date
  //   }
  //   formData.append('SavePolishOrder', JSON.stringify(obj));
  //   formData.append("fileRootPath", '\\\\192.9.202.39\\\\Rmkv_Nxt_Doc\\\\Invoices');
  //   formData.append("fileIndex", JSON.stringify(this.fileIndex));
  //   formData.append('removedFiles', JSON.stringify(this.removedFiles));
  //   formData.append('removedFileIndex', JSON.stringify(this.removedFileIndex));
  //   this._polishOrderReceiptService.addPolishOrderReceipts(formData).subscribe(
  //     (result: any) => {
  //       if (result) {
  //         this._confirmationDialog.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New polish order has been created", "Stone/Polish Order Receipt");
  //         this.componentVisibility = !this.componentVisibility;
  //         this.loadPolishOrderReceipts();
  //       }
  //       this.selectedFiles = null;
  //       this.document = null;
  //     });
  // }

  ///////////////////////////////////////////////////////////////////////////////////////

  public openBynoDialog(data: any): void {
    this.edit_data = data;
    let dialogRef = this._dialog.open(BynoSelectionPolishOrderReceiptComponent, {
      width: "700px",
      data: {
        data: data,
        polishOrderIssueDetails: this.polishReceiptDetailsList,
        isEditing: this.objAction.isEditing,
        editPolishOrderIssueDetails: this.modifyPolishReceiptDetailsList
      },
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result: PolishReceiptGridList[]) => {
      debugger;
      setTimeout(() => {
        let input = this.conversion.toArray();
        input[input.length - 1].nativeElement.focus();
      }, 100);
      if (result) {
        console.log(result, 'issue details result');
        var array: PolishReceiptGridList[] = [];
        var oldArray: PolishReceiptGridList[] = [];
        oldArray = this.polishReceiptDetailsList;
        if (result.length > 0) {
          for (var i = 0; i < oldArray.length; i++) {
            if (oldArray[i].wh_section_id == result[0].wh_section_id && oldArray[i].transfer_type == result[0].transfer_type &&
              oldArray[i].issue_no != result[0].issue_no) {
              array.push(oldArray[i]);
            }
          }
          this.polishReceiptDetailsList = JSON.parse(JSON.stringify(array));
        }
        else {
          var array: PolishReceiptGridList[] = [];
          var oldArray: PolishReceiptGridList[] = [];
          console.log(this.edit_data, 'edit_data')
          oldArray = this.polishReceiptDetailsList;
          for (var i = 0; i < oldArray.length; i++) {
            if (
              oldArray[i].wh_section_id == this.edit_data.wh_section_id &&
              oldArray[i].transfer_type == this.edit_data.transfer_type &&
              oldArray[i].issue_no != this.edit_data.issue_no) {
              array.push(oldArray[i]);
            }
          }
          this.polishReceiptDetailsList = JSON.parse(JSON.stringify(array));
        }

        if (this.polishReceiptDetailsList.length > 0) {
          this.polishReceiptDetailsList.forEach(x => {
            x.charges = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(x.charges) !== -1 ? 0 : +x.charges
          });
        }
        for (let i = 0; i < result.length; i++) {
          this.polishReceiptDetailsList.push(result[i]);
        }
        for (let i = 0; i < this.polishReceiptDetailsList.length; i++) {
          this.polishReceiptDetailsList[i].charges = this.polishReceiptDetailsList[i].charges ? this.polishReceiptDetailsList[i].charges : 0;
          this.polishReceiptDetailsList[i].sgst = this.polishReceiptDetailsList[i].sgst ? this.polishReceiptDetailsList[i].sgst : 0;
          this.polishReceiptDetailsList[i].cgst = this.polishReceiptDetailsList[i].cgst ? this.polishReceiptDetailsList[i].cgst : 0;
          this.polishReceiptDetailsList[i].igst = this.polishReceiptDetailsList[i].igst ? this.polishReceiptDetailsList[i].igst : 0;
          this.polishReceiptDetailsList[i].sgst_amount = this.polishReceiptDetailsList[i].sgst_amount ? this.polishReceiptDetailsList[i].sgst_amount : 0;
          this.polishReceiptDetailsList[i].cgst_amount = this.polishReceiptDetailsList[i].cgst_amount ? this.polishReceiptDetailsList[i].cgst_amount : 0;
          this.polishReceiptDetailsList[i].igst_amount = this.polishReceiptDetailsList[i].igst_amount ? this.polishReceiptDetailsList[i].igst_amount : 0;
          this.polishReceiptDetailsList[i].hsn_code = this.polishReceiptDetailsList[i].hsn_code ? this.polishReceiptDetailsList[i].hsn_code : '';
          this.polishReceiptDetailsList[i].conversion_charges = this.polishReceiptDetailsList[i].conversion_charges ? this.polishReceiptDetailsList[i].conversion_charges : '0.00';
          this.polishReceiptDetailsList[i].charges = [null, 'null', undefined, 'undefined', NaN, 'NaN', "", '0.00'].indexOf(this.polishReceiptDetailsList[i].charges.toString().trim()) === -1 ? (this._decimalPipe.transform(+this.polishReceiptDetailsList[i].charges, '1.2-2')).replace(/,/g, '') : "0.00";
        } this.getTaxableAmountTotal();
        // if (!this.objAction.isEditing) {
        //   this.polishReceiptDetailsList.forEach(x => {
        //     x.conversion_charges = 0;
        //     x.cgst = '0.00';
        //     x.cgst_amount = '0.00',
        //       x.sgst = '0.00';
        //     x.sgst_amount = '0.00',
        //       x.igst = '0.00';
        //     x.igst_amount = '0.00',
        //       x.total_charges = '0.00';
        //   });
        // } 
      }
    });
  }

  public newClick(): void {
    this.objAction.isEditing = false;
    this.objAction.isView = false;
    this.resetScreen();
    this.componentVisibility = !this.componentVisibility;
  }

  public listClick(): void {
    this.componentVisibility = !this.componentVisibility;
  }

  public matTableConfig(tableRecords?: any[]): void {
    this.dataSource = tableRecords ? new MatTableDataSource(tableRecords) : new MatTableDataSource([]);
  }


  /****************************************************** Validations *******************************************************/

  private beforeLoadValidate(): boolean {
    if (this.polishReceiptLoad.from_date.toString().trim().length !== 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialog.openAlertDialog('Invalid from date', 'Stone/Polish Order Receipt');
      return false;
    } if (this.polishReceiptLoad.to_date.toString().trim().length !== 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialog.openAlertDialog('Invalid to date', 'Stone/Polish Order Receipt');
      return false;
    } if (this.fromDate1 > this.toDate1) {
      document.getElementById('toDate').focus();
      this._confirmationDialog.openAlertDialog('From date must be less than to date', 'Stone/Polish Order Receipt');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.polishReceiptLoad.status) !== -1) {
      document.getElementById('statuss').focus();
      this._confirmationDialog.openAlertDialog('Select status', 'Stone/Polish Order Receipt');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.polishReceiptLoad.supplier_code) !== -1) {
      document.getElementById('loadSupplier').focus();
      this._confirmationDialog.openAlertDialog('Select supplier', 'Stone/Polish Order Receipt');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.polishReceiptLoad.supplier_code) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.polishReceiptLoad.supplier_name) !== -1) {
      document.getElementById('loadSupplier');
      this._confirmationDialog.openAlertDialog('Invalid supplier', 'Stone/Polish Order Receipt');
      return false;
    } else return true;
  }

  private beforeLoadIssueNosValidate(): boolean {
    if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objPolishReceipts.inv_supplier_code) !== -1) {
      document.getElementById('invSupplier').focus();
      this._confirmationDialog.openAlertDialog('Select supplier', 'Stone/Polish Order Receipt');
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objPolishReceipts.inv_supplier_code) === -1 && [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objPolishReceipts.inv_supplier_name) !== -1) {
      document.getElementById('invSupplier');
      this._confirmationDialog.openAlertDialog('Invalid supplier', 'Stone/Polish Order Receipt');
      return false;
    } else return true;
  }

  private beforeSaveValidate(): boolean {
    if (this.objPolishReceipts.polish_order_date.toString() === '') {
      document.getElementById("polishDate").focus();
      this._confirmationDialog.openAlertDialog("Enter polish order date", "Stone/Polish Order Receipt");
      return false;
    } else if (this.objPolishReceipts.polish_order_date.toString().length !== 10) {
      document.getElementById("polishDate").focus();
      this._confirmationDialog.openAlertDialog("Enter valid polish order date", "Stone/Polish Order Receipt");
      this.objPolishReceipts.polish_order_date = '';
      return false;
    } else if (this.objPolishReceipts.inv_supplier_code.toString().trim() === '') {
      document.getElementById('invSupplier').focus();
      this._confirmationDialog.openAlertDialog('Enter supplier code', 'Stone/Polish Order Receipt');
      return false;
    } else if (this.objPolishReceipts.inv_supplier_code.toString().trim() !== '' && this.objPolishReceipts.inv_supplier_name.toString().trim() === '') {
      document.getElementById('invSupplier').focus();
      this._confirmationDialog.openAlertDialog('Enter valid supplier code', 'Stone/Polish Order Receipt');
      return false;
    } else if (this.objPolishReceipts.inv_no.toString().trim() === '') {
      document.getElementById('invNo').focus();
      this._confirmationDialog.openAlertDialog('Enter inv no', 'Stone/Polish Order Receipt');
      return false;
    } else if (this.objPolishReceipts.invoice_date.toString() === '') {
      document.getElementById("invoiceDate").focus();
      this._confirmationDialog.openAlertDialog("Enter invoice date", "Stone/Polish Order Receipt");
      return false;
    } else if (this.objPolishReceipts.invoice_date.toString().length !== 10) {
      document.getElementById("invoiceDate").focus();
      this._confirmationDialog.openAlertDialog("Enter invoice date", "Stone/Polish Order Receipt");
      this.objPolishReceipts.invoice_date = '';
      return false;
    } if (this.polishReceiptDetailsList.length === 0) {
      this._confirmationDialog.openAlertDialog('No record to save', 'Stone/Polish Order Receipt');
      return false;
    } else return true;
  }

  public onlyAllwDecimalForReceiptDetails(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public onlyAllowDecimalForRoundOff(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string) {
    debugger;
    if (event.keyCode !== 45) {
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      } else {
        return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
      }
    } else {
      if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
        const pattern = /^[-0-9]*$/;
        if (pattern.test(event.key)) {
          this[key1][key2] = "";
          event.target.value = "";
        }
      }
    }
  }

  public onlyAllowDecimalForOther(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  public onlyAllowNumbers(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    }
  }

  public onlyAllwNumbersForReceiptDetails(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string, index: number): boolean {
    debugger;
    let key = key1 + '[' + index + ']' + '.' + key2;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    }
  }

  public onClear(exitFlag: boolean): void {
    debugger;
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  public onListClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.objAction.isEditing = false;
    this.resetScreen();
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    if (JSON.stringify(this.objPolishReceipts) !== (!this.objAction.isEditing ? JSON.stringify(this.unChangedPolishReceipts) :
      JSON.stringify(this.modifyPolishReceipts))) {
      // || JSON.stringify(this.polishReceiptDetailsList) !== 
      // (!this.objAction.isEditing ? JSON.stringify(this.unChangedPolishReceiptDetailsList) :
      // JSON.stringify(this.modifyPolishReceiptDetailsList)) 
      return true;
    } else {
      return false;
    }
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Stone/Polish Order Receipt";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  public setTypeOfGst(): void {
    let supplierGstNo: string = [null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.objPolishReceipts.inv_supplier_gstn_no) !== -1 ? '' : this.objPolishReceipts.inv_supplier_gstn_no.substr(0, 2);
    if (+supplierGstNo === +this._localStorage.getStateCode())
    this.flags.isIgst = false;
  else if (+supplierGstNo !== +this._localStorage.getStateCode())
    this.flags.isIgst = true;
    this.resetGstValues();
  }

  private resetGstValues(): void {
    for (let i = 0; i < this.polishReceiptDetailsList.length; i++) {
      this.polishReceiptDetailsList[i].sgst = 0;
      this.polishReceiptDetailsList[i].sgst_amount = 0;
      this.polishReceiptDetailsList[i].cgst = 0;
      this.polishReceiptDetailsList[i].cgst_amount = 0;
      this.polishReceiptDetailsList[i].igst = 0;
      this.polishReceiptDetailsList[i].igst_amount = 0;
      this.polishReceiptDetailsList[i].hsn_code = '';
      this.calculateTotalCharges(i);
    }
  }

  public fetchPolishOrderReceiptDetails(index: number, isEditing: boolean): void {
    debugger;
    this.objAction = {
      isEditing: isEditing ? true : false,
    }
    this.componentVisibility = !this.componentVisibility;
    let objLoad = {
      FetchPolishOrderReceipt: JSON.stringify([{
        wh_section_id: +this._localStorage.getWhSectionId(),
        receipt_no: this.dataSource.data[index].receipt_no,
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._polishOrderReceiptService.fetchPolishOrderReceipts(objLoad).subscribe((result: any) => {
      debugger;
      if (JSON.parse(result.records1) !== null || JSON.parse(result.records2) !== null) {
        this.objAction.isView = false;
        this.objPolishReceipts = JSON.parse(result.records1)[0];
        this.modifyPolishReceipts = JSON.parse(result.records1)[0];
        this.polishReceiptDetailsList = JSON.parse(result.records2)
        this.modifyPolishReceiptDetailsList = JSON.parse(result.records2)

        this.objPolishReceipts.inv_supplier_code = JSON.parse(result.records1)[0].inv_supplier_code.toString().trim();
        this.modifyPolishReceipts.inv_supplier_code = JSON.parse(result.records1)[0].inv_supplier_code.toString().trim();
        this.objPolishReceipts.inv_supplier_name = JSON.parse(result.records1)[0].inv_supplier_name.toString().trim();
        this.modifyPolishReceipts.inv_supplier_name = JSON.parse(result.records1)[0].inv_supplier_name.toString().trim();
        this.objPolishReceipts.issue_type = JSON.parse(result.records2)[0].issue_type;
        this.modifyPolishReceipts.issue_type = JSON.parse(result.records2)[0].issue_type;
        this.getPendingPolishIssueNos(0, true, result.records2);
        this.objPolishReceipts.inv_supplier_gstn_no = JSON.parse(result.records1)[0].supplier_gstn_no;
        this.modifyPolishReceipts.inv_supplier_gstn_no = JSON.parse(result.records1)[0].supplier_gstn_no;
        console.log(result, 'fetch');
        this.polishReceiptIssueNosList = JSON.parse(result.records2)
        this.modifyPolishReceiptIssueNosList = JSON.parse(result.records2)
        this.objPolishReceipts.total_invoice_amount = JSON.parse(result.records1)[0].inv_amount;
        this.modifyPolishReceipts.total_invoice_amount = JSON.parse(result.records1)[0].inv_amount;
        let date = new Date(this.objPolishReceipts.invoice_date);
        this.objPolishReceipts.invoice_date = this._datePipe.transform(date, 'dd/MM/yyyy');
        let modifyDate1 = new Date(this.modifyPolishReceipts.invoice_date);
        this.modifyPolishReceipts.invoice_date = this._datePipe.transform(modifyDate1, 'dd/MM/yyyy');
        let date1 = new Date(this.objPolishReceipts.polish_order_date);
        this.objPolishReceipts.polish_order_date = this._datePipe.transform(date1, 'dd/MM/yyyy');
        let modifyDate = new Date(this.modifyPolishReceipts.polish_order_date);
        this.modifyPolishReceipts.polish_order_date = this._datePipe.transform(modifyDate, 'dd/MM/yyyy');
      }
    });
  }

  public onClearView(): void {
    this.componentVisibility = !this.componentVisibility;
    this.objAction.isEditing = false;
  }

  public viewPolishOrderReceiptDetails(index: number, isView: boolean): void {
    debugger;
    this.objAction = {
      isView: isView ? true : false,
    }
    this.componentVisibility = !this.componentVisibility;
    let objLoad = {
      FetchPolishOrderReceipt: JSON.stringify([{
        wh_section_id: +this._localStorage.getWhSectionId(),
        receipt_no: this.dataSource.data[index].receipt_no,
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._polishOrderReceiptService.fetchPolishOrderReceipts(objLoad).subscribe((result: any) => {
      debugger;
      if (JSON.parse(result.records1) !== null || JSON.parse(result.records2) !== null) {
        this.objAction.isEditing = false;
        this.objPolishReceipts = JSON.parse(result.records1)[0];
        this.modifyPolishReceipts = JSON.parse(result.records1)[0];
        this.polishReceiptDetailsList = JSON.parse(result.records2)
        this.modifyPolishReceiptDetailsList = JSON.parse(result.records2)

        this.objPolishReceipts.inv_supplier_code = JSON.parse(result.records1)[0].inv_supplier_code.toString().trim();
        this.modifyPolishReceipts.inv_supplier_code = JSON.parse(result.records1)[0].inv_supplier_code.toString().trim();
        this.objPolishReceipts.inv_supplier_name = JSON.parse(result.records1)[0].inv_supplier_name.toString().trim();
        this.modifyPolishReceipts.inv_supplier_name = JSON.parse(result.records1)[0].inv_supplier_name.toString().trim();
        this.objPolishReceipts.issue_type = JSON.parse(result.records2)[0].issue_type;
        this.modifyPolishReceipts.issue_type = JSON.parse(result.records2)[0].issue_type;
        this.getPendingPolishIssueNos(0, true, result.records2);
        this.objPolishReceipts.inv_supplier_gstn_no = JSON.parse(result.records1)[0].supplier_gstn_no;
        this.modifyPolishReceipts.inv_supplier_gstn_no = JSON.parse(result.records1)[0].supplier_gstn_no;
        console.log(result, 'fetch');
        this.polishReceiptIssueNosList = JSON.parse(result.records2)
        this.modifyPolishReceiptIssueNosList = JSON.parse(result.records2)
        this.objPolishReceipts.total_invoice_amount = JSON.parse(result.records1)[0].inv_amount;
        this.modifyPolishReceipts.total_invoice_amount = JSON.parse(result.records1)[0].inv_amount;
        let date = new Date(this.objPolishReceipts.invoice_date);
        this.objPolishReceipts.invoice_date = this._datePipe.transform(date, 'dd/MM/yyyy');
        let modifyDate1 = new Date(this.modifyPolishReceipts.invoice_date);
        this.modifyPolishReceipts.invoice_date = this._datePipe.transform(modifyDate1, 'dd/MM/yyyy');
        let date1 = new Date(this.objPolishReceipts.polish_order_date);
        this.objPolishReceipts.polish_order_date = this._datePipe.transform(date1, 'dd/MM/yyyy');
        let modifyDate = new Date(this.modifyPolishReceipts.polish_order_date);
        this.modifyPolishReceipts.polish_order_date = this._datePipe.transform(modifyDate, 'dd/MM/yyyy');
      }
    });
  }

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Receipt No': this.dataSource.data[i].receipt_no,
          'Receipt Date': this._datePipe.transform(this.dataSource.data[i].receipt_date, 'dd/MM/yyyy'),
          "Supplier Name": this.dataSource.data[i].supplier_name,
          "Quantity": this.dataSource.data[i].quantity,
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Stone/Polish Order Receipt",
        datetime
      );
    } else
      this._confirmationDialog.openAlertDialog("No record found, Load the data first", "Stone/Polish Order Receipt");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.receipt_no);
        tempObj.push(this._datePipe.transform(e.receipt_date, 'dd/MM/yyyy'));
        tempObj.push(e.supplier_name);
        tempObj.push(e.quantity);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Receipt No', 'Receipt Date', 'Supplier Name', 'Quantity']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Stone/Polish Order Receipt' + '.pdf');
    } else
      this._confirmationDialog.openAlertDialog("No record found, Load the data first", "Stone/Polish Order Receipt");
  }

  private resetScreen(): void {
    if (this.objAction.isEditing) {
      this.objPolishReceipts = JSON.parse(JSON.stringify(this.modifyPolishReceipts));
      this.modifyPolishReceipts = JSON.parse(JSON.stringify(this.modifyPolishReceipts));
      this.polishReceiptDetailsList = JSON.parse(JSON.stringify(this.modifyPolishReceiptDetailsList));
      this.modifyPolishReceiptDetailsList = JSON.parse(JSON.stringify(this.modifyPolishReceiptDetailsList));
      this.polishReceiptIssueNosList = JSON.parse(JSON.stringify(this.modifyPolishReceiptIssueNosList));
      this.modifyPolishReceiptIssueNosList = JSON.parse(JSON.stringify(this.modifyPolishReceiptIssueNosList));
    } else {
      this.objPolishReceipts = JSON.parse(JSON.stringify(this.unChangedPolishReceipts));
      this.polishReceiptIssueNosList = [];
      this.polishReceiptDetailsList = [];
    }
  }

  private openAlertDialog(value: string, componentName: string, focus: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        document.getElementById(focus).focus();
      _dialogRef = null;
    });
  }

  public openSizeTableDialog(): void {
    let dialogRef = this._dialog.open(SizeAndProductComponent, {
      width: "700px",
      data: {
      },
      panelClass: "custom-dialog-container",
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    });
  }
}
