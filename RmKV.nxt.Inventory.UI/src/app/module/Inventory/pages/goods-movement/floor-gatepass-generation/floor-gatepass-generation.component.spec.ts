import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FloorGatepassGenerationComponent } from './floor-gatepass-generation.component';

describe('FloorGatepassGenerationComponent', () => {
  let component: FloorGatepassGenerationComponent;
  let fixture: ComponentFixture<FloorGatepassGenerationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FloorGatepassGenerationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloorGatepassGenerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
