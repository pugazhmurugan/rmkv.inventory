import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterToGodownAcknowledgementComponent } from './counter-to-godown-acknowledgement.component';

describe('CounterToGodownAcknowledgementComponent', () => {
  let component: CounterToGodownAcknowledgementComponent;
  let fixture: ComponentFixture<CounterToGodownAcknowledgementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterToGodownAcknowledgementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterToGodownAcknowledgementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
