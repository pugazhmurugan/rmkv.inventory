import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterToGodownComponent } from './counter-to-godown.component';

describe('CounterToGodownComponent', () => {
  let component: CounterToGodownComponent;
  let fixture: ComponentFixture<CounterToGodownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterToGodownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterToGodownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
