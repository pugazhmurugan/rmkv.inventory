import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { EditMode, Rights } from 'src/app/common/models/common-model';
import { EditableGridComponent } from 'src/app/common/pages/editable-grid/editable-grid.component';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { IndexedDBCommonFN } from 'src/app/common/shared/IndexedDB/indexed_db_common';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ByNoModel } from 'src/app/module/Inventory/model/common.model';
import { GodownToCounterService } from 'src/app/module/Inventory/services/goods-movement/internal-transfers/godown-to-counter/godown-to-counter.service';
declare var jsPDF: any;

@Component({
  selector: 'app-godown-to-counter',
  templateUrl: './godown-to-counter.component.html',
  styleUrls: ['./godown-to-counter.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class GodownToCounterComponent implements OnInit {


  componentVisibility: boolean = true;
  public commonIndexed: IndexedDBCommonFN = new IndexedDBCommonFN(this.dbService, this._localStorage);

  @ViewChild(EditableGridComponent, null) byno_data;
  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Transfer_No", "Transfer_Date", "From", "To", "Transfer_Pcs", "Selling_Price", "Action"];
  objGodownToCounter: any = {
    Transfer_No: '',
    Transfer_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    From_Section: this._localStorage.getCompanySectionName(),
    From_Warehouse: this._localStorage.getwareHouseName(),
    To_Location: '',
    Remarks: ''
  }

  objLoad = {
    Transfer_No: '',
    Transfer_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Section: this._localStorage.getCompanySectionName(),
    From_Warehouse: this._localStorage.getwareHouseName(),
    To_Location: '',
    Remarks: '',
    From_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),// this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    To_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),//this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Status: 'ALL'
  }
  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  objEditableGrid = {
    tabledata: [],
    obj: {
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0
      , uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: 0.00, hsncode: '', gst: 0, unique_byno_prod_serial: null
    },
    flags: { isProductCode: true, isProductName: true, isUOM: true, isSellingPrice: true, isDisable: false },
    schema_name: 'GC'
  }
  objGodown = [{
    byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
    uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
    isDelete: false, valid: false
  }]
  rights: Rights = {
    Add: true,
    Update: true,
    Delete: true,
    Approve: true,
    View: true
  };

  showTable: boolean = false;
  locationList: any;
  model1: any = new Date();
  model2: any = new Date();
  section_id = +this._localStorage.getWhSectionId();

  public dateValidation: DateValidation = new DateValidation();
  sectionList: any;
  constructor(public _localStorage: LocalStorage,
    public _router: Router, private dbService: NgxIndexedDBService,
    public _datePipe: DatePipe,
    public _godowntocounter: GodownToCounterService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    private _matDialog: MatDialog, public _excelService: ExcelService,
    public _decimalPipe: DecimalPipe, public _commonService: CommonService) {
    this.showTable = false;
    this.model1.setDate(this.model2.getDate() - 2);
    this.objLoad.From_Date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');

  }

  ngOnInit() {
    this.showTable = true;
    this.getToLocation();
    this.getRights();
    this.getSectionList();
  }
  private getRights(): void {
    var rights = this._localStorage.getMenuRights("/GodowntoCounter");
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "Delete" && rights[x].Status == true) {
        this.rights.Delete = true;
      }
      if (rights[x].Right_Name == "Approve" && rights[x].Status == true) {
        this.rights.Approve = true;
      }
      if (rights[x].Right_Name == "View" && rights[x].Status == true) {
        this.rights.View = true;
      }
    }
  }
  public onClear(): void {
    this.byno_data.objEditableGrid.tabledata = [];
    this.byno_data.objEditableGrid.tabledata.push({
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
      uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
      isDelete: false, valid: false
    });
    this.objAction.isEditing = false;
    this.commonIndexed.clearCommonIndexedData(this.objEditableGrid.schema_name);
    this.getTransferNo();
  }

  resetScreen(exitFlag) {
    if (!this.objEditableGrid.flags.isDisable) {
      if (JSON.stringify(this.byno_data.objEditableGrid.tabledata) != JSON.stringify(this.objGodown)) {
        this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag)
      } else if (exitFlag)
        this.onListClick();
    } else
      this.onListClick();
  }


  validateFromDate() {
    let date = this.dateValidation.validateDate(this.objLoad.From_Date, this.model1, this.minDate, this.maxDate);
    this.objLoad.From_Date = date[0];
    this.model1 = date[1];
  }
  validateToDate() {
    let date = this.dateValidation.validateDate(this.objLoad.To_Date, this.model2, this._datePipe.transform(this.model1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoad.To_Date = date[0];
    this.model2 = date[1];
  }

  private openConfirmationDialog(message: string, exitFlag): void {
    debugger;
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Godown To Counter";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.onClear();
    });
  }
  
  public newClick(): void {
    if (this.rights.Add) {
      this.objEditableGrid.flags.isDisable = false;
      this.objEditableGrid.tabledata = [];
      this.componentVisibility = !this.componentVisibility;
      this.objAction.isEditing = false;
      //this.objGodownToCounter.To_Location = '';
      this.getTransferNo();
    }
  }

  public onListClick(): void {
    this.onClear();
    this.componentVisibility = !this.componentVisibility;
  }
  private beforeSaveValidate(): boolean {
    if (!this.objGodownToCounter.To_Location.toString().trim()) {
      document.getElementById('location').focus();
      this._confirmationDialogComponent.openAlertDialog("Select to location", "Godown To Counter");
      this.objGodownToCounter.To_Location = "";
      return false;
    } if (this.byno_data.objEditableGrid.tabledata[0].product_code == '' && this.byno_data.objEditableGrid.tabledata[0].byno == '') {
      let input = this.byno_data.byno.toArray();
      input[0].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Byno", "Godown To Counter");
      return false;
    } else
      return true;
  }


  private beforeloadValidate(): boolean {
    if (!this.objLoad.To_Location.toString().trim()) {
      document.getElementById('location').focus();
      this._confirmationDialogComponent.openAlertDialog("Select to location", "Godown To Counter");
      this.objLoad.To_Location = "";
      return false;
    } if (!this.objLoad.From_Date.toString().trim()) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter from date", "Godown To Counter");
      return false;
    } else if (this.objLoad.From_Date.length != 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid from date", "Godown To Counter");
      return false;
    } else if (!this.objLoad.To_Date.toString().trim()) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter to date", "Godown To Counter");
      return false;
    } else if (this.objLoad.To_Date.length != 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid to date", "Godown To Counter");
      return false;
    } else if (this.model2 < this.model1) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("To date can't be less than from date", "Godown To Counter");
      return false;
    } else
      return true;
  }
  public addGoods(): void {
    if (this.beforeSaveValidate()) {
      debugger
      for (let i = 0; i < this.objEditableGrid.tabledata.length; i++) {
        if (this.objEditableGrid.tabledata[i].product_code == '') {
          this.objEditableGrid.tabledata.splice(i, 1);
        }
      }
      let objDetails = [];
      for (let i = 0; i < this.byno_data.objEditableGrid.tabledata.length; i++) {
        objDetails.push(Object.assign({
          serial_no: i + 1,
          byno: this.byno_data.objEditableGrid.tabledata[i].byno,
          inv_byno: this.byno_data.objEditableGrid.tabledata[i].inv_byno,
          byno_serial: this.byno_data.objEditableGrid.tabledata[i].byno_serial,
          byno_prod_serial: this.byno_data.objEditableGrid.tabledata[i].byno_prod_serial,
          uom_id: this.byno_data.objEditableGrid.tabledata[i].uom_id,
          uom_descrition: this.byno_data.objEditableGrid.tabledata[i].uom_descrition,
          transfer_qty: this.byno_data.objEditableGrid.tabledata[i].prod_qty,
          transfer_quantity: this.byno_data.objEditableGrid.tabledata[i].prod_qty,
          transfer_pcs: this.byno_data.objEditableGrid.tabledata[i].prod_pcs,
          selling_price: this.byno_data.objEditableGrid.tabledata[i].selling_price,
          hsncode: this.byno_data.objEditableGrid.tabledata[i].hsncode,
          gst: this.byno_data.objEditableGrid.tabledata[i].gst,
          unique_byno_prod_serial: this.byno_data.objEditableGrid.tabledata[i].unique_byno_prod_serial,
        }));
      }
      let objData = {
        GoodCounter: JSON.stringify([{
          transfer_date: this.objGodownToCounter.Transfer_Date,
          transfer_id: +this.objGodownToCounter.Transfer_No,
          wh_section_id: +this._localStorage.getWhSectionId(),
          destination_id: +this.objGodownToCounter.To_Location,
          remarks: this.objGodownToCounter.Remarks.toString().trim(),
          details: objDetails,
          entered_by: +this._localStorage.intGlobalUserId(),
          company_section_id: +this._localStorage.getCompanySectionId()
        }]),
      }
      this._godowntocounter.addGodownToCounter(objData).subscribe((result: any) => {
        if (result) {
          this.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New Transfer No Is :" + " " + result + "", result);
        }
      });
    }
  }
  public getSectionList(): void {
    debugger;
    let obj = {
      Section: JSON.stringify([{
        sales_location_id: +this._localStorage.getGlobalSalesLocationId(),
        warehouse_id: +this._localStorage.getGlobalWarehouseId()
      }])
    }
    this._commonService.getGroupSection(obj).subscribe((result: any) => {
      debugger;
      if (result) {
        this.sectionList = JSON.parse(result);
      }
    });
  }

  generateReport(data, flag?) {
    debugger;
    let objLoad = [{
      transfer_date: flag == false ? this._datePipe.transform(data.transfer_date, 'dd/MM/yyyy') : this.objGodownToCounter.Transfer_Date,
      company_section_id: +this._localStorage.getCompanySectionId(),
      warehouse_id: +this._localStorage.getGlobalWarehouseId(),
      transfer_id: flag == false ? data.transfer_id : +data,
      transfer_type: 'G',
      wh_section_id: this.sectionList.filter(x => +x.company_section_id == +this._localStorage.getCompanySectionId())[0].wh_section_id,
    }];
    let objData = {
      GodownToCounterReport: JSON.stringify(objLoad)
    };
    
    this._godowntocounter.getGodownToCounterReport(objData).subscribe((result: any) => {
      if (result.size != 0) {
        let dataType = result.type;
        console.log(result);
        const file = new Blob([result], { type: dataType });
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      } else
        this._confirmationDialogComponent.openAlertDialog('No record found', 'Godown To Counter');
    });
  }

  openAlertDialog(meassge: any, data?: any) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = meassge;
    _dialogRef.componentInstance.componentName = "Godown To Counter";
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.openPrintConfirmationDialog(data);
      }
      _dialogRef = null;
    });
  }

  public openPrintConfirmationDialog(data: any): any {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    _dialogRef.componentInstance.confirmMessage = "Do you want to print this record ?"
    _dialogRef.componentInstance.componentName = "Godown To Counter";
    return _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onClear();
        this.generateReport(data);
      }
      else {
        this.onClear();
        this.getTransferNo();
      }
      _dialogRef = null;
    });
  }

  getGoods(): void {
    debugger;
    if (this.beforeloadValidate()) {
      this.matTableConfig([]);
      let objData = {
        GoodCounter: JSON.stringify([{
          from_date: this.objLoad.From_Date,
          to_date: this.objLoad.To_Date,
          wh_section_id: this._localStorage.getWhSectionId(),
          source_id: this._localStorage.getGlobalWarehouseId(),
          destination_id: this.objLoad.To_Location,
          status: this.objLoad.Status,
          company_section_id: +this._localStorage.getCompanySectionId()
        }]),
      }
      debugger;
      this._godowntocounter.getGodownToCounter(objData).subscribe((result: any) => {
        if (result) {
          this.matTableConfig(result);
        } else
          this._confirmationDialogComponent.openAlertDialog('No records found', 'Godown To Counter');
      });
    }
  }
  fetchGoods(transfer_id): void {
    let objData = {
      GoodCounter: JSON.stringify([{
        transfer_id: transfer_id,
        wh_section_id: this._localStorage.getWhSectionId(),
        source_id: this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._godowntocounter.fetchGodownToCounter(objData).subscribe((result: ByNoModel[]) => {
      if (result) {
        this.objAction.isEditing = true;
        this.objEditableGrid.flags.isDisable = false;
        this.objEditableGrid.tabledata = [];
        this.objEditableGrid.tabledata = result;
        for (let i = 0; i < this.objEditableGrid.tabledata.length; i++) {
          this.objEditableGrid.tabledata[i].valid = true;
          this.objEditableGrid.tabledata[i].selling_price = this.objEditableGrid.tabledata[i].selling_price + '.00';
        }
        this.componentVisibility = !this.componentVisibility;
        this.objGodownToCounter.Transfer_No = transfer_id;
        this.objGodownToCounter.To_Location = this.objLoad.To_Location;
      }
    });
  }

  viewGoods(transfer_id): void {
    let objData = {
      GoodCounter: JSON.stringify([{
        transfer_id: transfer_id,
        wh_section_id: this._localStorage.getWhSectionId(),
        source_id: this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._godowntocounter.fetchGodownToCounter(objData).subscribe((result: ByNoModel[]) => {
      if (result) {
        this.objEditableGrid.tabledata = [];
        this.objEditableGrid.tabledata = result;
        for (let i = 0; i < this.objEditableGrid.tabledata.length; i++) {
          this.objEditableGrid.tabledata[i].valid = true;
          this.objEditableGrid.tabledata[i].selling_price = this.objEditableGrid.tabledata[i].selling_price + '.00';
        }
        this.objEditableGrid.flags.isDisable = true;
        this.componentVisibility = !this.componentVisibility;
        this.objGodownToCounter.Transfer_No = transfer_id;
        this.objGodownToCounter.To_Location = this.objLoad.To_Location;
      }
    });
  }
  public openDeleteConfirmationDialog(data: any): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = 'Do you want cancel this record ?';
    dialogRef.componentInstance.componentName = 'Godown To Counter';
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.delete(data);
      dialogRef = null;
    });
  }

  public delete(data: any) {
    let objData = {
      GoodCounter: JSON.stringify([{
        transfer_date: this._datePipe.transform(data.transfer_date, 'dd/MM/yyyy'),
        transfer_id: data.transfer_id,
        cancel: false,
        wh_section_id: this._localStorage.getWhSectionId(),
        source_id: this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        entered_by: +this._localStorage.intGlobalUserId(),
        cancelled_reason: ''
      }]),
    }
    this._godowntocounter.cancelGodownToCounter(objData).subscribe((result: any) => {
      if (result) {
        this._confirmationDialogComponent.openAlertDialog("Cancel successfully", "Godown To Counter");
        this.getGoods();
      }
    });
  }
  public openApproveConfirmationDialog(data: any): any {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    _dialogRef.componentInstance.confirmMessage = "Do you want to approve this record ?"
    _dialogRef.componentInstance.componentName = "Godown To Counter";
    return _dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.approve(data);
      _dialogRef = null;
    });
  }

  private approve(data: any): void {
    let objData = {
      GoodCounter: JSON.stringify([{
        transfer_date: this._datePipe.transform(data.transfer_date, 'dd/MM/yyyy'),
        transfer_id: data.transfer_id,
        approve: true,
        wh_section_id: this._localStorage.getWhSectionId(),
        source_id: this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        entered_by: +this._localStorage.intGlobalUserId(),
      }]),
    }
    this._godowntocounter.approveGodownToCounter(objData).subscribe((result: any) => {
      if (result !== null) {
        this._confirmationDialogComponent.openAlertDialog("Added to approved list", "Godown To Counter");
        this.getGoods();
      }
    });
  }

  private getTransferNo(): void {
    let objData = {
      GoodCounter: JSON.stringify([{
        group_section_id: this._localStorage.getGlobalGroupSectionId(),
        source_id: this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        wh_section_id: +this._localStorage.getWhSectionId()
      }]),
    }
    this._godowntocounter.getTransferNo(objData).subscribe((result: any) => {
      if (result) {
        this.objGodownToCounter.Transfer_No = result;
      }
    });
  }

  public matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }
  private getToLocation(): void {
    let objData = {
      GoodCounter: JSON.stringify([{
        sales_location_id: this._localStorage.getGlobalSalesLocationId()
      }]),
    }
    this._godowntocounter.getToLocation(objData).subscribe((result: any) => {
      if (result) {
        this.locationList = result;
        result.length == 1 ? this.objGodownToCounter.To_Location = this.locationList[0].warehouse_id : '';
        result.length == 1 ? this.objLoad.To_Location = this.locationList[0].warehouse_id : '';
      } else
        this._confirmationDialogComponent.openAlertDialog('No records found', 'Godown To Counter ');
    });
  }
  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Transfer No': this.dataSource.data[i].transfer_id,
          'Transfer Date': this._datePipe.transform(this.dataSource.data[i].transfer_date, 'dd/MM/yyyy'),
          "From": this.dataSource.data[i].source_name,
          "To": this.dataSource.data[i].destination_name,
          "Transfer Pcs": this.dataSource.data[i].transfer_pcs,
          "Selling Price": this._decimalPipe.transform(this.dataSource.data[i].selling_price, '1.2-2'),
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Godown_To_Counter",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Godown To Counter");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.transfer_id);
        tempObj.push(this._datePipe.transform(e.transfer_date, 'dd/MM/yyyy'));
        tempObj.push(e.source_name);
        tempObj.push(e.destination_name);
        tempObj.push(e.transfer_pcs);
        tempObj.push(this._decimalPipe.transform(e.selling_price, '1.2-2'));
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Transfer No', 'Transfer Date', 'From', 'To', 'Transfer Pcs', 'Selling Price']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Godown_To_Counter' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Godown To Counter");
  }

  public getProductPcs(): number {
    if (this.dataSource.data !== undefined)
      return this.dataSource.data.map(t => +t.transfer_pcs).reduce((acc, value) => acc + value, 0);
  }

  public getSellingPrice(): any {
    let price = '0.00'
    if (this.dataSource.data !== undefined) {
      price = this.dataSource.data.map(t => +t.selling_price).reduce((acc, value) => acc + value, 0);
      return this._decimalPipe.transform(price, '1.2-2');
    }
  }
}
