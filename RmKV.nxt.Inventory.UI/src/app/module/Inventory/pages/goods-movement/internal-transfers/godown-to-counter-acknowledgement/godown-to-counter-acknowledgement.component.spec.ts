import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GodownToCounterAcknowledgementComponent } from './godown-to-counter-acknowledgement.component';

describe('GodownToCounterAcknowledgementComponent', () => {
  let component: GodownToCounterAcknowledgementComponent;
  let fixture: ComponentFixture<GodownToCounterAcknowledgementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GodownToCounterAcknowledgementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GodownToCounterAcknowledgementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
