import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { EditableGridComponent } from 'src/app/common/pages/editable-grid/editable-grid.component';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { IndexedDBCommonFN } from 'src/app/common/shared/IndexedDB/indexed_db_common';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { EditMode, ForCheckBynoData, Rights } from 'src/app/module/Inventory/model/common.model';
import { CounterToGodownAcknowledgementService } from 'src/app/module/Inventory/services/goods-movement/internal-transfers/counter-to-godown-acknowledgment/counter-to-godown-acknowledgement.service';
import { CounterToGodownService } from 'src/app/module/Inventory/services/goods-movement/internal-transfers/counter-to-godown/counter-to-godown.service';
import { GodownToCounterService } from 'src/app/module/Inventory/services/goods-movement/internal-transfers/godown-to-counter/godown-to-counter.service';
declare var jsPDF: any;

@Component({
  selector: 'app-counter-to-godown-acknowledgement',
  templateUrl: './counter-to-godown-acknowledgement.component.html',
  styleUrls: ['./counter-to-godown-acknowledgement.component.scss'],
  providers: [DatePipe,DecimalPipe]
})
export class CounterToGodownAcknowledgementComponent implements OnInit {
  @ViewChild(EditableGridComponent, null) byno_data;
  public commonIndexed: IndexedDBCommonFN = new IndexedDBCommonFN(this.dbService, this._localStorage);

  componentVisibility:boolean=true;
  displayedColumns = ["Serial_No", "transfer_id","transfer_date", "ack_date", "To_Section", "From_Section","Ack_Pcs","Ack_Qty"];
  @ViewChild(MatSort, null) sort: MatSort;

  Date:string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }
  
  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  showTable: boolean = false;

  objEditableGrid = {
    tabledata: [],
    obj: {
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0
      , uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: 0.00, hsncode: '', gst: 0, unique_byno_prod_serial: null
    },
    flags: {byno:false, isProductCode: true, isProductName: true, isUOM: true, isSellingPrice: true, isDisable: true ,isAction:false},
    schema_name: 'invoice'
  }
  objGodown = [{
    byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
    uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
    isDelete: false, valid: false
  }]
  
  sectionList: any = [];
  locationList: any = [];
  loadGtoCList: any[] = [];
  date1: any = new Date();
  dataSource: any = new MatTableDataSource([]);
  objLoad: any = {
    section: this._localStorage.getCompanySectionName(),
    from_warehouse: this._localStorage.getwareHouseName(),
    to_location: 0,
    from_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    to_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  objCounterToGodown: any = {
    from_section: this._localStorage.getCompanySectionName(),
    from_warehouse: this._localStorage.getwareHouseName(),
    to_location: 0,
    transfer_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    transfer_ack_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    transfer_id: '',
    byno: '',
    acknowledged_pieces: 0,
    total_pieces: 0,
    remarks: ''
  }

  modifyCounterToGodown: any = {
    from_section: this._localStorage.getCompanySectionName(),
    from_warehouse: this._localStorage.getwareHouseName(),
    to_location: 0,
    transfer_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    transfer_ack_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    transfer_id: '',
    byno: '',
    acknowledged_pieces: 0,
    total_pieces: 0,
    remarks: ''
  }

  unChangedCounterToGodown: any = {
    from_section: this._localStorage.getCompanySectionName(),
    from_warehouse: this._localStorage.getwareHouseName(),
    to_location: 0,
    transfer_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    transfer_ack_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    transfer_id: '',
    byno: '',
    acknowledged_pieces: 0,
    total_pieces: 0,
    remarks: ''
  }

  byNoList: any = [{
    byno: '',
    received_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    product_code: '',
    product_name: '',
    uom_descrition: '',
    prod_qty: '',
    prod_pcs: '',
    selling_price: '',
    gst: ''
  }];

  unChangedByNoList: any = [{
    byno: '',
    received_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    product_code: '',
    product_name: '',
    uom_descrition: '',
    prod_qty: '',
    prod_pcs: '',
    selling_price: '',
    gst: ''
  }];

  modifyByNoList: any = [{
    byno: '',
    received_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    product_code: '',
    product_name: '',
    uom_descrition: '',
    prod_qty: '',
    prod_pcs: '',
    selling_price: '',
    gst: ''
  }];

  getAckQtyTotal: any = {
    ack_qty: 0,
  }

  addtempArray: any = [{
    byno: '',
    received_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    product_code: '',
    product_name: '',
    uom_descrition: '',
    prod_qty: '',
    prod_pcs: '',
    selling_price: '',
    gst: ''
  }];

  ForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  rights: Rights = {
    Add: false,
    Update: false,
    Delete: false,
    Approve: false,
    View: false
  };

  Inv_Byno: string = "";
  Byno_Serial: string = "";
  Byno_Prod_Serial: string = "";
  keyUpEvent: any
  constructor(
    public _minMaxDate: MinMaxDate,
    public _router: Router,
    private dbService: NgxIndexedDBService,
    public _localStorage: LocalStorage,
    private _datePipe: DatePipe,
    public _commonService: CommonService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _counterToGodownAckService: CounterToGodownAcknowledgementService,
    public _godownToCounterService: GodownToCounterService,
    public _matDialog: MatDialog,
    public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    private _excelService: ExcelService,
    public _decimalPipe: DecimalPipe,
    public _keyPressEvents : KeyPressEvents
     ) { 
      this.showTable = false;
      this.objLoad.from_date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
      this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 2)));
    }

  ngOnInit() {
    this.getToLocation();
    this.getRights();
  }

  private getRights(): void {
    var rights = this._localStorage.getMenuRights("/CountertoGodownAcknowledgement");
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "Delete" && rights[x].Status == true) {
        this.rights.Delete = true;
      }
      if (rights[x].Right_Name == "Approve" && rights[x].Status == true) {
        this.rights.Approve = true;
      }
      if (rights[x].Right_Name == "View" && rights[x].Status == true) {
        this.rights.View = true;
      }
    }
  }

  public validateFromDate(): void {
    let date = this.dateValidation.validateDate(this.objLoad.from_date, this.fromDate1, this.minDate, this.maxDate);
    this.objLoad.from_date = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.objLoad.to_date, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoad.to_date = date[0];
    this.toDate1 = date[1];
  }

  public validateDate(): void {
    let date = this.dateValidation.validateDate(this.objCounterToGodown.transfer_date, this.date1, this.minDate, this.maxDate);
    this.objCounterToGodown.transfer_date = date[0];
    this.date1 = date[1];
  }

  newClick(): void {
    // if (this.rights.Add) {
    this.componentVisibility = !this.componentVisibility;
    this.resetScreen();
    this.showTable = true;
    // }
  }

  private onListClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.matTableConfig([]);
    this.getAckQtyTotal = {
      ack_qty: 0,
    }
  }

  private getToLocation(): void {
    debugger;
    let objData = {
      GoodCounter: JSON.stringify([{
        sales_location_id: this._localStorage.getGlobalSalesLocationId()
      }]),
    }
    this._godownToCounterService.getToLocation(objData).subscribe((result: any) => {
      if (result) {
        this.locationList = result;
        this.objCounterToGodown.to_location = this.locationList[0].warehouse_id;
        this.unChangedCounterToGodown.to_location = this.locationList[0].warehouse_id;
        this.objLoad.to_location = this.locationList[0].warehouse_id;
      } 
    });
  }

  public onClearByNoDetails(): void {
    // if (!this.objCounterToGodown.transfer_id.toString().trim()) {
      this.byNoList = [];
      this.objCounterToGodown.byno = '';
      this.objCounterToGodown.acknowledged_pieces = 0;
      this.objCounterToGodown.total_pieces = 0;
    }
  // }

  public onKeyPressTransferNo(keyUpEvent: any): void {
    if (this.objCounterToGodown.transfer_id === '' && keyUpEvent.keyCode === 13) {
      document.getElementById("transferId").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter transfer no', 'Counter to Godown Acknowledgement');
    } else if (keyUpEvent.keyCode === 13) {
      this.checkGtoCTransferNoDetails();
      // this.loadTransferNoBasedByNoDetails();
    }
  }

  private loadTransferNoBasedByNoDetails(): void {
    debugger;
    let objData = {
      LoadByNoDetails: JSON.stringify([{
        transfer_date: this.objCounterToGodown.transfer_date,
        transfer_id: +this.objCounterToGodown.transfer_id,
        wh_section_id: +this._localStorage.getWhSectionId(),
        source_id: this._localStorage.getGlobalWarehouseId(),
        destination_id: +this.objCounterToGodown.to_location,
        company_section_id: +this._localStorage.getCompanySectionId(),
      }]),
    }
    this._counterToGodownAckService.loadCtoGTransferNoBasedDetails(objData).subscribe((result: any) => {
      if (JSON.parse(result) !== null) {
        this.byNoList = JSON.parse(result);
        this.objCounterToGodown.total_pieces = +(this.byNoList.length);
        console.log(JSON.parse(result), 'byno')
      } else
        this._confirmationDialogComponent.openAlertDialog('No record found', 'Counter to Godown Acknowledgement');
    });
  }

  public setTotalCalculation(tableRecords: any): void {
    debugger;
    this.getAckQtyTotal = {
      ack_qty: 0,
    }
    for (let i = 0; i < tableRecords.length; i++) {
      this.getAckQtyTotal.ack_qty += +(tableRecords[i].transfer_qty);
    }
  }

  public checkInvalidByNoData(): boolean {
    debugger;
    let index = this.byNoList.findIndex(x => x.byno !== this.objCounterToGodown.byno.toString().trim().toUpperCase())
    if (index !== -1)
      return true;
    else
      return false;
  }

  public checkToLocationIsEmpty(): boolean {
    if (this.objCounterToGodown.to_location === 0) {
      document.getElementById("location").focus();
      this._confirmationDialogComponent.openAlertDialog('Select to location', 'Counter to Godown Acknowledgement');
      return false;
    } else if (this.objCounterToGodown.transfer_date.toString() === "") {
      document.getElementById("date").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter transfer date', 'Counter to Godown Acknowledgement');
      return false;
    } else if (this.objCounterToGodown.transfer_date.toString().length !== 10) {
      document.getElementById("date").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter valid transfer date', 'Counter to Godown Acknowledgement');
      this.objCounterToGodown.transfer_date = "";
      return false;
    } else return true;
  }

  public clearByNoList(): void {
    this.byNoList.length = [];
  }

  public onClearCtoGList(): void {
    this.matTableConfig([]);
    this.getAckQtyTotal = {
      ack_qty: 0,
    }
  }

  public onClearView(): void {
    this.objAction.isView = true;
    this.componentVisibility = !this.componentVisibility;
  }

  private emptyFromToDate(): boolean {
    if (this.objLoad.to_location === 0) {
      document.getElementById("toSection").focus();
      this._confirmationDialogComponent.openAlertDialog('Select to location', 'Counter to Godown Acknowledgement');
      return false;
    } else if (this.objLoad.from_date.toString() === "") {
      document.getElementById("fromDate").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter from date', 'Counter to Godown Acknowledgement');
      return false;
    } else if (this.objLoad.from_date.toString().length !== 10) {
      document.getElementById("fromDate").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter valid from date', 'Counter to Godown Acknowledgement');
      this.objLoad.from_date = "";
      return false;
    } else if (this.objLoad.to_date.toString() === "") {
      document.getElementById("toDate").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter to date', 'Counter to Godown Acknowledgement');
      return false;
    } else if (this.objLoad.to_date.toString().length !== 10) {
      document.getElementById("toDate").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter valid to date', 'Counter to Godown Acknowledgement');
      this.objLoad.to_date = "";
      return false;
    } else {
      return true;
    }
  }

  public matTableConfig(tableRecords?: any) {
    if (tableRecords) {
      this.dataSource = new MatTableDataSource(tableRecords);
      this.dataSource.sort = this.sort;
    }
    else
      this.dataSource = new MatTableDataSource([]);
  }

  public loadCtoGDetails(): void {
    debugger;
    if (this.emptyFromToDate()) {
      let LoadAcknowledgement = {
        LoadAcknowledgement: JSON.stringify([{
          from_date: this.objLoad.from_date,
          to_date: this.objLoad.to_date,
          wh_section_id: +this._localStorage.getWhSectionId(),
          source_id: this._localStorage.getGlobalWarehouseId(),
          destination_id: +this.objLoad.to_location,
          company_section_id: +this._localStorage.getCompanySectionId(),
        }])
      }
      this._counterToGodownAckService.loadCtoGAcknowledgement(LoadAcknowledgement).subscribe((result: any) => {
        debugger;
        if (JSON.parse(result) !== null) {
          this.loadGtoCList = JSON.parse(result);
          this.matTableConfig(JSON.parse(result));
          this.setTotalCalculation(JSON.parse(result));
        }
        else {
          this._confirmationDialogComponent.openAlertDialog("No record found", "Counter to Godown Acknowledgement");
        }
      });
    }
  }

  public fetchCountertoGodownDetails(index: number): void {
    debugger;
    let obj: any = [{
      transfer_date: this._datePipe.transform(this.dataSource.data[index].transfer_date, 'dd/MM/yyyy'),
      ack_date: this._datePipe.transform(this.dataSource.data[index].ack_date, 'dd/MM/yyyy'),
      transfer_id: this.dataSource.data[index].transfer_id,
      wh_section_id: +this._localStorage.getWhSectionId(),
      source_id: +this._localStorage.getGlobalWarehouseId(),
      company_section_id: +this._localStorage.getCompanySectionId(),
    }]
    let objFetch = {
      FetchGtoCAck: JSON.stringify(obj)
    }
    this._counterToGodownAckService.fetchCtoGAcknowledgementDetails(objFetch).subscribe((result: any) => {
      if (JSON.parse(result)) {
        debugger;
        this.objCounterToGodown = JSON.parse(JSON.stringify(result[0]));
        this.modifyCounterToGodown = JSON.parse(JSON.stringify(result[0]));
        this.objAction.isView = true;
        this.componentVisibility = !this.componentVisibility;
      }
    });
  }

  private beforeSaveValidate(): boolean {
    if (this.objCounterToGodown.transfer_date.toString() === "") {
      document.getElementById("date").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter transfer date', 'Counter to Godown Acknowledgement');
      return false;
    } else if (this.objCounterToGodown.transfer_date.toString().length !== 10) {
      document.getElementById("date").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter valid transfer date', 'Counter to Godown Acknowledgement');
      this.objCounterToGodown.transfer_date = "";
      return false;
    } else if (this.objCounterToGodown.transfer_id === 0 || this.objCounterToGodown.transfer_id === '') {
      document.getElementById("transferId").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter transfer no', 'Counter to Godown Acknowledgement');
      return false;
    } else if (this.byNoList.length === 0) {
      this._confirmationDialogComponent.openAlertDialog('No record to save', 'Counter to Godown Acknowledgement');
      return false;
    } else if (!this.selectAtleastOneByNoAcknowledge()) {
      document.getElementById("byNo").focus();
      this._confirmationDialogComponent.openAlertDialog("Any one byno must be acknowledged", "Counter to Godown Acknowledgement");
      return false;
    }
    else {
      return true;
    }
  }

  public checkTransferNoEmpty() : boolean {
    if (this.objCounterToGodown.transfer_id === '' || this.objCounterToGodown.transfer_id === 0 ) {
      document.getElementById("transferId").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter transfer no', 'Counter to Godown Acknowledgement');
      return false;
    }else if (this.objCounterToGodown.transfer_id !== '' && this.byNoList.length === 0) {
      document.getElementById("transferId").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter valid transfer no', 'Counter to Godown Acknowledgement');
      this.objCounterToGodown.transfer_id = '';
      return false;
    }else return true;
  }

  private selectAtleastOneByNoAcknowledge(): boolean {
    debugger;
    let index = this.byNoList.findIndex(x => x.received_byno !== undefined);
    if (index !== -1)
      return true;
    else
      return false;
  }

  public checkGtoCTransferNoDetails(): void {
    debugger;
    let obj: any = [{
      wh_section_id: +this._localStorage.getWhSectionId(),
      transfer_id: +this.objCounterToGodown.transfer_id,
      company_section_id: +this._localStorage.getCompanySectionId(),
      from_warehouse_id:this.objCounterToGodown.to_location
    }]
    let CheckTransfer = {
      CheckCtoGTransferNo: JSON.stringify(obj)
    }
    this._counterToGodownAckService.checkCtoGAckTransferNoDetails(CheckTransfer).subscribe((result: any) => {
      debugger;
      if (JSON.parse(result) === true) {
        this.loadTransferNoBasedByNoDetails();
      }
      else {
        this._confirmationDialogComponent.openAlertDialog("Invalid Transfer No", "Counter to Godown Acknowledgement");
      }
    });
  }


  public onSaveCToGAcknowledgementDetails(): void {
    debugger;
    if (this.beforeSaveValidate()) {
      let details = [];
      if (this.byNoList.length === 0) {
        this._confirmationDialogComponent.openAlertDialog('No record to save', 'Counter to Godown Acknowledgement');
        return;
      } else {
        for (let i = 0; i < this.byNoList.length; i++) {
          if (this.byNoList[i].received_byno !== undefined) {
            details.push({
              inv_byno: this.byNoList[i].inv_byno,
              byno_serial: this.byNoList[i].byno_serial,
              byno_prod_serial: this.byNoList[i].byno_prod_serial
            });
          }
        }
      }
      let obj = [{
        transfer_date: this.objCounterToGodown.transfer_date,
        transfer_id: +this.objCounterToGodown.transfer_id,
        wh_section_id: +this._localStorage.getWhSectionId(),
        source_id: this._localStorage.getGlobalWarehouseId(),
        destination_id: +this.objCounterToGodown.to_location,
        remarks: this.objCounterToGodown.remarks.toString().trim(),
        entered_by: +this._localStorage.intGlobalUserId(),
        details: details,
        company_section_id: +this._localStorage.getCompanySectionId(),
      }]
      let objSave = {
        AddAcknowledgement: JSON.stringify(obj)
      }
      this._counterToGodownAckService.addCtoGAcknowledgementDetails(objSave).subscribe(
        (result: any) => {
          if (result) {
            this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" :
              "New Counter to Godown Acknowledgement has been created", "Counter to Godown Acknowledgement");
            this.componentVisibility = !this.componentVisibility;
            this.loadCtoGDetails();
          }
        });
    }
  }

  private trimStringFields(): void {
    this.objCounterToGodown.transfer_id = this.objCounterToGodown.transfer_id.toString().trim();
    this.modifyCounterToGodown.transfer_id = this.modifyCounterToGodown.transfer_id.toString().trim();
    this.unChangedCounterToGodown.transfer_id = this.unChangedCounterToGodown.transfer_id.toString().trim();

    this.objCounterToGodown.byno = this.objCounterToGodown.byno.toString().trim().toUpperCase();
    this.modifyCounterToGodown.byno = this.modifyCounterToGodown.byno.toString().trim().toUpperCase();
    this.unChangedCounterToGodown.byno = this.unChangedCounterToGodown.byno.toString().trim().toUpperCase();

    this.objCounterToGodown.remarks = this.objCounterToGodown.remarks.toString().trim();
    this.modifyCounterToGodown.remarks = this.modifyCounterToGodown.remarks.toString().trim();
    this.unChangedCounterToGodown.remarks = this.unChangedCounterToGodown.remarks.toString().trim();
  }

  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    this.trimStringFields();
    if (JSON.stringify(this.objCounterToGodown) !==
      (!this.objAction.isEditing ? JSON.stringify(this.unChangedCounterToGodown) :
        JSON.stringify(this.modifyCounterToGodown)) || JSON.stringify(this.byNoList) !==
      (!this.objAction.isEditing ? JSON.stringify(this.unChangedByNoList) :
        JSON.stringify(this.modifyByNoList))) {
      return true;
    } else {
      return false;
    }
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Counter to Godown Acknowledgement";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  private resetScreen(): void {
    debugger;
    if (this.objAction.isEditing === true) {
      this.objCounterToGodown = JSON.parse(JSON.stringify(this.modifyCounterToGodown));
      this.modifyCounterToGodown = JSON.parse(JSON.stringify(this.modifyCounterToGodown));
      this.byNoList = JSON.parse(JSON.stringify(this.modifyByNoList));
      this.modifyByNoList = JSON.parse(JSON.stringify(this.modifyByNoList));
    } else {
      this.objCounterToGodown = JSON.parse(JSON.stringify(this.unChangedCounterToGodown));
      this.objCounterToGodown.to_location = this.objLoad.to_location;
      this.byNoList = JSON.parse(JSON.stringify(this.unChangedByNoList));
      this.objCounterToGodown.transfer_date = this._datePipe.transform(new Date, 'dd/MM/yyyy');
      this.date1 = new Date()
    }
  }

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Transfer No': this.dataSource.data[i].transfer_id,
          'Transfer Date': this._datePipe.transform(this.dataSource.data[i].transfer_date, 'dd/MM/yyyy'),
          "Ack Date": this.dataSource.data[i].ack_date,
          "From Section": this.dataSource.data[i].source_name,
          "To Location": this.dataSource.data[i].destination_name,
          "Ack Pcs": this.dataSource.data[i].transfer_pcs,
          "Ack Qty": this.dataSource.data[i].transfer_qty
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Counter to Godown Acknowledgement",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Counter to Godown Acknowledgement");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.transfer_id);
        tempObj.push(this._datePipe.transform(e.transfer_date, 'dd/MM/yyyy'));
        tempObj.push(this._datePipe.transform(e.ack_date, 'dd/MM/yyyy'));
        tempObj.push(e.source_name);
        tempObj.push(e.destination_name);
        tempObj.push(e.transfer_pcs);
        tempObj.push(e.transfer_qty);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Transfer No', 'Transfer Date', 'Ack Date', 'From Section', 'To Location', 'Ack Pcs', 'Ack Qty']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Counter to Godown Acknowledgement' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Counter to Godown Acknowledgement");
  }

  public openReceivedByNo(data: any): any {
    debugger;
    for (let i = 0; i < this.byNoList.length; i++) {
      if (this.byNoList[i].inv_byno === data.Inv_Byno &&
        this.byNoList[i].byno_serial === data.Byno_Serial &&
        this.byNoList[i].byno_prod_serial === data.Byno_Prod_Serial) {
        this.objCounterToGodown.byno = this.byNoList[i].byno;
        if (this.byNoList[i].received_byno === this.objCounterToGodown.byno.toString().trim().toUpperCase()) {
          document.getElementById('byNo').focus();
          this._confirmationDialogComponent.openAlertDialog('Already Acknowledged', 'Counter to Godown Acknowledgement');
          this.objCounterToGodown.byno = '';
          return false;
        } else if (this.byNoList[i].inv_byno === data.Inv_Byno &&
          this.byNoList[i].byno_serial === data.Byno_Serial &&
          this.byNoList[i].byno_prod_serial === data.Byno_Prod_Serial) {
          this.byNoList[i].received_byno = this.byNoList[i].byno;
          this.objCounterToGodown.byno = this.byNoList[i].byno;
          if (this.byNoList[i].received_byno === this.objCounterToGodown.byno.toString().trim().toUpperCase()) {
            this.objCounterToGodown.acknowledged_pieces = this.objCounterToGodown.acknowledged_pieces + 1;
            this.objCounterToGodown.byno = '';
          }
        }
      } else {
        let index = this.byNoList.findIndex(x => x.inv_byno === data.Inv_Byno && x.byno_serial === data.Byno_Serial && x.byno_prod_serial === data.Byno_Prod_Serial);
        if (index == -1) {
          document.getElementById('byNo').focus();
          this._confirmationDialogComponent.openAlertDialog('Invalid Byno', 'Counter to Godown Acknowledgement');
          this.objCounterToGodown.byno = '';
          return;
        }

      }
    }
  }

  public checkValidScan(): boolean {
    debugger;
        let index = this.byNoList.findIndex(x => x.byno === this.objCounterToGodown.byno.toString().trim().toUpperCase());
        if (index !== -1)
          return true;
        else
          return false;
      }

  public onKeyPress(keyUpEvent: any) {
    debugger
    if (keyUpEvent.keyCode === 13 && this.objCounterToGodown.byno.toString().trim().toUpperCase() === '') {
      document.getElementById("byNo").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter byno', 'Counter to Godown Acknowledgement');
    }
    else if (keyUpEvent.keyCode === 13 &&
      this.objCounterToGodown.byno.toString().trim().toUpperCase().length == 16) {
      this.byNoWithoutSlash();
    }
    else if (keyUpEvent.keyCode === 13 &&
      this.objCounterToGodown.byno.toString().trim().toUpperCase().length == 28) {
      this.OldByNoWithoutSlash();
    }
    else if (keyUpEvent.keyCode === 13 &&
      this.objCounterToGodown.byno.toString().trim().toUpperCase().length == 30) {
      this.byNoWithoutSlash();
    }
    else if (
      keyUpEvent.keyCode === 13 &&
      this.objCounterToGodown.byno.toString().trim().toUpperCase().includes("/")
    ) {
      debugger
      this.splitByNumber();
    } else if (
      this.objCounterToGodown.byno.toString().trim().toUpperCase().length >= 28 &&
      keyUpEvent.keyCode === 13
    ) {
      this.autoScannerCall();
    }
    else {
      if (
        keyUpEvent.keyCode === 13 &&
        !this.objCounterToGodown.byno.toString().trim().toUpperCase().includes("/") &&
        this.objCounterToGodown.byno.toString().trim().toUpperCase().length > 16
      ) {
        this._confirmationDialogComponent.openAlertDialog(
          "Invalid Byno",
          "Counter to Godown Acknowledgement"
        );
      } else if (keyUpEvent.keyCode === 13 && !this.checkValid()) {
        this._confirmationDialogComponent.openAlertDialog("Invalid Byno", "Counter to Godown Acknowledgement");
        this.objCounterToGodown.byno = '';
      }
    }
  }

  private checkValid(): boolean {
    debugger;
    let index = this.byNoList.findIndex(x => x.byno === this.objCounterToGodown.byno.toString().trim().toUpperCase());
    if (index !== -1)
      return true;
    else
      return false;
  }

  private byNoWithoutSlash() {
    this.objCounterToGodown.byno = this.objCounterToGodown.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objCounterToGodown.byno.toString().trim().toUpperCase().substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objCounterToGodown.byno.toString().trim().toUpperCase().substr(9, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objCounterToGodown.byno.toString().trim().toUpperCase().substr(13, 3);
    this.openReceivedByNo(this.ForCheckBynoData);
  }

  private OldByNoWithoutSlash() {
    this.objCounterToGodown.byno = this.objCounterToGodown.byno.toString().trim().toUpperCase();
    this.ForCheckBynoData.Inv_Byno = this.objCounterToGodown.byno.toString().trim().toUpperCase().substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objCounterToGodown.byno.toString().trim().toUpperCase().substr(8, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objCounterToGodown.byno.toString().trim().toUpperCase().substr(11, 3);
    console.log(this.ForCheckBynoData)
    this.openReceivedByNo(this.ForCheckBynoData);
  }

  private autoScannerCall() {
    this.ForCheckBynoData.Inv_Byno = this.objCounterToGodown.byno.toString().trim().toUpperCase().substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = this.objCounterToGodown.byno.toString().trim().toUpperCase().substr(8, 4);
    this.ForCheckBynoData.Byno_Prod_Serial = this.objCounterToGodown.byno.toString().trim().toUpperCase().substr(11, 4);
    this.openReceivedByNo(this.ForCheckBynoData);
  }

  private splitByNumber() {
    this.objCounterToGodown.byno = this.objCounterToGodown.byno.toString().trim().toUpperCase();
    var x = this.objCounterToGodown.byno.toString().trim().toUpperCase().split("/");
    this.Inv_Byno = x[0];
    this.Byno_Serial = x[1];
    this.Byno_Prod_Serial = x[2];
    if (this.Byno_Serial.length == 2) {
      this.Byno_Serial = "00".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 2) {
      this.Byno_Prod_Serial = "00".concat(this.Byno_Prod_Serial);
    }
    if (this.Byno_Serial.length == 1) {
      this.Byno_Serial = "000".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 1) {
      this.Byno_Prod_Serial = "000".concat(this.Byno_Prod_Serial);
    }
    this.ForCheckBynoData.Inv_Byno = this.Inv_Byno.toString().trim();
    this.ForCheckBynoData.Byno_Serial = this.Byno_Serial.toString().trim();
    this.ForCheckBynoData.Byno_Prod_Serial = this.Byno_Prod_Serial.toString().trim();
    this.openReceivedByNo(this.ForCheckBynoData);
  }
}
