import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GodownToCounterComponent } from './godown-to-counter.component';

describe('GodownToCounterComponent', () => {
  let component: GodownToCounterComponent;
  let fixture: ComponentFixture<GodownToCounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GodownToCounterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GodownToCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
