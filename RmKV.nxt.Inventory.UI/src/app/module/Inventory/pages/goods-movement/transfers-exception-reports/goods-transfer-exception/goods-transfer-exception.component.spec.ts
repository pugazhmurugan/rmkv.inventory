import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsTransferExceptionComponent } from './goods-transfer-exception.component';

describe('GoodsTransferExceptionComponent', () => {
  let component: GoodsTransferExceptionComponent;
  let fixture: ComponentFixture<GoodsTransferExceptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsTransferExceptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsTransferExceptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
