import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GodownToCounterExceptionComponent } from './godown-to-counter-exception.component';

describe('GodownToCounterExceptionComponent', () => {
  let component: GodownToCounterExceptionComponent;
  let fixture: ComponentFixture<GodownToCounterExceptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GodownToCounterExceptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GodownToCounterExceptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
