import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { CounterToGodownService } from 'src/app/module/Inventory/services/goods-movement/internal-transfers/counter-to-godown/counter-to-godown.service';
import { TransfersExceptionReportsService } from 'src/app/module/Inventory/services/goods-movement/transfers-exception-reports/transfers-exception-reports.service';

@Component({
  selector: 'app-counter-to-godown-exception',
  templateUrl: './counter-to-godown-exception.component.html',
  styleUrls: ['./counter-to-godown-exception.component.scss'],
  providers: [DatePipe]
})
export class CounterToGodownExceptionComponent implements OnInit {
  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd-MM-yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd-MM-yyyy'),
    company_section_id: '',
    wh_section_id: '',
    transfer_id: '',
    detailView: false,
    transferNo: false
  }
  sectionList: any[] = [];

  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  constructor(
    public _minMaxDate: MinMaxDate,
    public _router: Router,
    public _localStorage: LocalStorage,
    private _datePipe: DatePipe,
    private _commonService: CommonService,
    private _keyPressEvents: KeyPressEvents,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _transsfersexceptionservice: TransfersExceptionReportsService,
  ) {

  }

  ngOnInit() {
    this.getSectionList();
  }

  public getSectionList(): void {
    debugger;
    let obj = {
      Section: JSON.stringify([{
        sales_location_id: +this._localStorage.getGlobalSalesLocationId(),
        warehouse_id: +this._localStorage.getGlobalWarehouseId()
      }])
    }
    this._commonService.getGroupSection(obj).subscribe((result: any) => {
      debugger;
      if (result) {
        this.sectionList = JSON.parse(result);
      }
    });
  }
  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }
  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }

  public getCounterToGodownExceptionReport(): void {
    if (this.beforeViewValidate()) {
      let objLoad = [{
        from_date: this.load.FromDate,
        to_date: this.load.ToDate,
        company_section_id: +this._localStorage.getCompanySectionId(),
        wh_section_id: +this._localStorage.getWhSectionId(),
        transfer_id: +this.load.transfer_id
      }];
      let objData: any;
      if (this.load.detailView || this.load.transferNo) {
        objData = {
          CounterToGodownException_List: JSON.stringify(objLoad),
          Type: true
        };
      } else if (!this.load.detailView && !this.load.transferNo) {
        objData = {
          CounterToGodownException_List: JSON.stringify(objLoad),
          Type: false
        }
      }

      this._transsfersexceptionservice.getCounterToGodownExceptionReport(objData).subscribe((result: any) => {
        debugger;
        if (result.size != 0) {
          let dataType = result.type;
          const file = new Blob([result], { type: dataType });
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL);
        } else
          this._confirmationDialogComponent.openAlertDialog('No record found', 'Godown To Counter Report');
      });
    }
  }
  public beforeViewValidate(): boolean {
    if (this.load.company_section_id === '') {
      document.getElementById('Section').focus();
      this._confirmationDialogComponent.openAlertDialog('Select Section', 'Godown To Counter Exception');
      return false;
    } else return true;
  }

  public onlyAllowNumbers(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, false);
    }
  }
 
  public resetTransferNo(): void {
    this.load.transfer_id = '';
  }
}
