import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterToGodownExceptionComponent } from './counter-to-godown-exception.component';

describe('CounterToGodownExceptionComponent', () => {
  let component: CounterToGodownExceptionComponent;
  let fixture: ComponentFixture<CounterToGodownExceptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterToGodownExceptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterToGodownExceptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
