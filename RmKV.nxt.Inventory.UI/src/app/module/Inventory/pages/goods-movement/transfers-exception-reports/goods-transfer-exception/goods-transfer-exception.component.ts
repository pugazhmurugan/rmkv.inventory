import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { SalesLocation } from 'src/app/module/Inventory/model/common.model';
import { TransfersExceptionReportsService } from 'src/app/module/Inventory/services/goods-movement/transfers-exception-reports/transfers-exception-reports.service';

@Component({
  selector: 'app-goods-transfer-exception',
  templateUrl: './goods-transfer-exception.component.html',
  styleUrls: ['./goods-transfer-exception.component.scss'],
  providers: [DatePipe]
})
export class GoodsTransferExceptionComponent implements OnInit {
  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  objGoodsTransferException: any = {
    to_location_id : 0,
    Check: false
  }

  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  transferLocationList: SalesLocation[] = [];

  constructor(public _minMaxDate: MinMaxDate,public _router: Router,
    public _localStorage: LocalStorage, private _datePipe: DatePipe,
    public _commonService: CommonService, public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _goodsTransferExceptionService: TransfersExceptionReportsService) { }

  ngOnInit() {
    this.getAllWorkingLocation();
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }
  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }
  private getAllWorkingLocation(): void {
    let objAllWorkingLocation = {
      SalesLocation: JSON.stringify([{

      }])
    };
    this._commonService.getAllWorkingLocation(objAllWorkingLocation).subscribe((result: any) => {
      this.transferLocationList = result;
      this.transferLocationList.splice(0, 1);
      this.objGoodsTransferException.sales_location_id = 0;
      for (let i = 0; i < this.transferLocationList.length; i++) {
        if (this._localStorage.getGlobalSalesLocationId() === this.transferLocationList[i].Sales_Location_ID) {
          this.transferLocationList.splice(i, 1);
        }
      }
    });
  }
  private beforeViewGoodsTransferExceptionValidate(): boolean {
    if (this.objGoodsTransferException.to_location_id === 0) {
      document.getElementById("transferLocation").focus();
      this._confirmationDialogComponent.openAlertDialog('Select transfer location', 'Goods Transfer Exception Report');
      return false;
    }else return true;
  }
  public getGoodTransferExceptionReport() {
    debugger;
    this.beforeViewGoodsTransferExceptionValidate();
    let objLoad = [{
      from_date: this.load.FromDate,
      to_date: this.load.ToDate,
      to_loction_id: +this.objGoodsTransferException.to_location_id,
      transfer_id: 0,
      company_section_id : +this._localStorage.getCompanySectionId(),
      wh_section_id : +this._localStorage.getWhSectionId()
    }];
    let objData = {
      Goods_Transfer_Exception_Report: JSON.stringify(objLoad),
      Type: this.objGoodsTransferException.Check
    };
    this._goodsTransferExceptionService.getGoodsTransferException(objData).subscribe((result: any) => {
      if (result.size != 0) {
        let dataType = result.type;
        console.log(result);
        const file = new Blob([result], { type: dataType });
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      } else
        this._confirmationDialogComponent.openAlertDialog('No record found', 'Goods Transfer Report');
    });
  }

}
