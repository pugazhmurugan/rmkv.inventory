import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsTransferReturnableComponent } from './goods-transfer-returnable.component';

describe('GoodsTransferReturnableComponent', () => {
  let component: GoodsTransferReturnableComponent;
  let fixture: ComponentFixture<GoodsTransferReturnableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsTransferReturnableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsTransferReturnableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
