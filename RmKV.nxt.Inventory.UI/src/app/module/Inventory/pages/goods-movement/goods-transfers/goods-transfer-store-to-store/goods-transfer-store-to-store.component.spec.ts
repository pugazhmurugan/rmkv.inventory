import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsTransferStoreToStoreComponent } from './goods-transfer-store-to-store.component';

describe('GoodsTransferStoreToStoreComponent', () => {
  let component: GoodsTransferStoreToStoreComponent;
  let fixture: ComponentFixture<GoodsTransferStoreToStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsTransferStoreToStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsTransferStoreToStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
