import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { EditMode } from 'src/app/common/models/common-model';
import { EditableGridComponent } from 'src/app/common/pages/editable-grid/editable-grid.component';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { IndexedDBCommonFN } from 'src/app/common/shared/IndexedDB/indexed_db_common';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ByNoModel } from 'src/app/module/Inventory/model/common.model';
import { GoodsTransferService } from 'src/app/module/Inventory/services/goods-movement/goods-transfers/goods-transfer.service';

@Component({
  selector: 'app-goods-transfer-other-state',
  templateUrl: './goods-transfer-other-state.component.html',
  styleUrls: ['./goods-transfer-other-state.component.scss'],
  providers: [DatePipe]
})
export class GoodsTransferOtherStateComponent implements OnInit {
  componentVisibility: boolean = true;
  displayedColumns = ["Serial_No", "Transfer_No", "Transfer_Date", "Warehouse_From", "Warehouse_To", "Transfer_qty", "Cost_price", "Selling_Price", "Action"];

  @ViewChild(EditableGridComponent, null) byno_data;

  objLoad = {
    Section: this._localStorage.getCompanySectionName(),
    From_Warehouse: this._localStorage.getwareHouseName(),
    To_Location: '',
    From_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    To_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Status: 'ALL'
  }
  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  objGoodsTransfer: any = {
    Transfer_No: '',
    Transfer_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    From_Sales_Location: this._localStorage.getSalesLocationName(),
    To_Sales_Location: '',
    From_Section: this._localStorage.getCompanySectionName(),
    From_Warehouse: this._localStorage.getwareHouseName(),
    To_Section: this._localStorage.getCompanySectionName(),
    Remarks: ''
  }
  locationList: any;

  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  showTable: boolean = false;
  model1: any = new Date();
  model2: any = new Date();
  objEditableGrid = {
    tabledata: [],
    obj: {
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0
      , uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: 0.00, hsncode: '', gst: 0, unique_byno_prod_serial: null
    },
    flags: { isProductCode: false, isProductName: true, isUOM: true, isSellingPrice: true, isDisable: false },
    schema_name: 'GTOState'
  }
  objGodown = [{
    byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
    uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
    isDelete: false, valid: false
  }]
  public commonIndexed: IndexedDBCommonFN = new IndexedDBCommonFN(this.dbService, this._localStorage);
  dataSource: any = new MatTableDataSource([]);

  constructor(public _minMaxDate: MinMaxDate, public _router: Router, private dbService: NgxIndexedDBService,
    public _localStorage: LocalStorage, private _datePipe: DatePipe, public _CommonService: CommonService,
    public _GoodsTransfer: GoodsTransferService,
    public _confirmationDialogComponent: ConfirmationDialogComponent, public _matDialog: MatDialog) {
    this.showTable = false;
    this.model1.setDate(this.model2.getDate() - 2);
    this.objLoad.From_Date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
    this.getToLocation();
  }

  ngOnInit() {
    this.showTable = true;
    this.getToLocation();
  }

  validateFromDate() {
    let date = this.dateValidation.validateDate(this.objLoad.From_Date, this.model1, this.minDate, this.maxDate);
    this.objLoad.From_Date = date[0];
    this.model1 = date[1];
  }
  validateToDate() {
    let date = this.dateValidation.validateDate(this.objLoad.To_Date, this.model2, this._datePipe.transform(this.model1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoad.To_Date = date[0];
    this.model2 = date[1];
  }

  public newClick(): void {
    this.objEditableGrid.flags.isDisable = false;
    this.objEditableGrid.tabledata = [];
    this.componentVisibility = !this.componentVisibility;
    this.objAction.isEditing = false;
    this.getTransferNo();
  }

  resetScreen(exitFlag) {
    debugger
    if (!this.objEditableGrid.flags.isDisable) {
      if (JSON.stringify(this.byno_data.objEditableGrid.tabledata) != JSON.stringify(this.objGodown) || this.objGoodsTransfer.To_Sales_Location != '') {
        this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag)
      } else if (exitFlag)
        this.onListClick();
    } else
      this.onListClick();
  }

  public onListClick(): void {
    this.componentVisibility = !this.componentVisibility;
  }

  private openConfirmationDialog(message: string, exitFlag): void {
    debugger;
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Goods Transfer (With in OtherState)";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.onClear();
    });
  }
  public onClear(): void {
    this.byno_data.objEditableGrid.tabledata = [];
    this.byno_data.objEditableGrid.tabledata.push({
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
      uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
      isDelete: false, valid: false
    });
    this.objAction.isEditing = false;
    this.objGoodsTransfer.To_Sales_Location = '';
    this.commonIndexed.clearCommonIndexedData(this.objEditableGrid.schema_name);
    //this.getTransferNo();
  }

  private getTransferNo(): void {
    let objData = {
      GoodsTransfer: JSON.stringify([{
        group_section_id: this._localStorage.getGlobalGroupSectionId(),
        source_id: this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        wh_section_id: +this._localStorage.getWhSectionId()
      }]),
    }
    this._GoodsTransfer.getTransferNoWithincity(objData).subscribe((result: any) => {
      if (result) {
        this.objGoodsTransfer.Transfer_No = result;
      }
    });
  }
  private getToLocation(): void {
    let objData = {
      GoodsTransfer: JSON.stringify([{
        sales_location_id: this._localStorage.getGlobalSalesLocationId()
      }]),
    }
    this._GoodsTransfer.getOtherStateToLocation(objData).subscribe((result: any) => {
      if (result) {
        this.locationList = result;
        result.length == 1 ? this.objGoodsTransfer.To_Sales_Location = this.locationList[0].warehouse_id : '';
        result.length == 1 ? this.objLoad.To_Location = this.locationList[0].warehouse_id : '';
      } else
        this._confirmationDialogComponent.openAlertDialog('No records found', 'Goods Transfer (With in OtherState) ');
    });
  }

  private beforeSaveValidate(): boolean {
    if (!this.objGoodsTransfer.To_Sales_Location.toString().trim()) {
      document.getElementById('salesLocation').focus();
      this._confirmationDialogComponent.openAlertDialog("Select to sales location", "Goods Transfer (With in OtherState)");
      this.objGoodsTransfer.To_Location = "";
      return false;
    } if (this.byno_data.objEditableGrid.tabledata[0].product_code == '' && this.byno_data.objEditableGrid.tabledata[0].byno == '') {
      let input = this.byno_data.byno.toArray();
      input[0].nativeElement.focus();
      this._confirmationDialogComponent.openAlertDialog("Enter Byno", "Goods Transfer (With in OtherState)");
      return false;
    } else
      return true;
  }
  public AddGoodsTransfer(): void {
    if (this.beforeSaveValidate()) {
      debugger
      for (let i = 0; i < this.objEditableGrid.tabledata.length; i++) {
        if (this.objEditableGrid.tabledata[i].product_code == '') {
          this.objEditableGrid.tabledata.splice(i, 1);
        }
      }
      let objDetails = [];
      for (let i = 0; i < this.byno_data.objEditableGrid.tabledata.length; i++) {
        objDetails.push(Object.assign({
          serial_no: i + 1,
          byno: this.byno_data.objEditableGrid.tabledata[i].byno,
          inv_byno: this.byno_data.objEditableGrid.tabledata[i].inv_byno,
          byno_serial: this.byno_data.objEditableGrid.tabledata[i].byno_serial,
          byno_prod_serial: this.byno_data.objEditableGrid.tabledata[i].byno_prod_serial,
          uom_id: this.byno_data.objEditableGrid.tabledata[i].uom_id,
          uom_descrition: this.byno_data.objEditableGrid.tabledata[i].uom_descrition,
          transfer_qty: this.byno_data.objEditableGrid.tabledata[i].prod_qty,
          transfer_quantity: this.byno_data.objEditableGrid.tabledata[i].prod_qty,
          transfer_pcs: this.byno_data.objEditableGrid.tabledata[i].prod_pcs,
          selling_price: this.byno_data.objEditableGrid.tabledata[i].selling_price,
          hsncode: this.byno_data.objEditableGrid.tabledata[i].hsncode,
          gst: this.byno_data.objEditableGrid.tabledata[i].gst,
          unique_byno_prod_serial: this.byno_data.objEditableGrid.tabledata[i].unique_byno_prod_serial,
        }));
      }
      let objData = {
        GoodsTransfer: JSON.stringify([{
          transfer_date: this.objGoodsTransfer.Transfer_Date,
          transfer_id: +this.objGoodsTransfer.Transfer_No,
          wh_section_id: +this._localStorage.getWhSectionId(),
          destination_wh_id: +this.objGoodsTransfer.To_Sales_Location,
          remarks: this.objGoodsTransfer.Remarks,
          details: objDetails,
          entered_by: +this._localStorage.intGlobalUserId(),
          company_section_id: +this._localStorage.getCompanySectionId()
        }]),
      }
      this._GoodsTransfer.addWithincity(objData).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New Transfer No Is :" + " " + result + "", "Goods Transfer (With in OtherState)");
          this.componentVisibility = !this.componentVisibility;
          this.objLoad.To_Location = this.objGoodsTransfer.To_Location;
          this.onClear();
        }
      });
    }
  }

  private beforeloadValidate(): boolean {
    if (!this.objLoad.To_Location.toString().trim()) {
      document.getElementById('location').focus();
      this._confirmationDialogComponent.openAlertDialog("Select to location", "Goods Transfer (With in OtherState)");
      this.objLoad.To_Location = "";
      return false;
    } if (!this.objLoad.From_Date.toString().trim()) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter from date", "Goods Transfer (With in OtherState)");
      return false;
    } else if (this.objLoad.From_Date.length != 10) {
      document.getElementById('fromDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid from date", "Goods Transfer (With in OtherState)");
      return false;
    } else if (!this.objLoad.To_Date.toString().trim()) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter to date", "Goods Transfer (With in OtherState)");
      return false;
    } else if (this.objLoad.To_Date.length != 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid to date", "Goods Transfer (With in OtherState)");
      return false;
    } else if (this.model2 < this.model1) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("To date can't be less than from date", "Goods Transfer (With in OtherState)");
      return false;
    } else
      return true;
  }


  public matTableConfig(tableRecords: any[]): void {
    if (tableRecords)
      this.dataSource = new MatTableDataSource(tableRecords);
    else
      this.dataSource = new MatTableDataSource([]);
  }
  getGoodsTransfer(): void {
    if (this.beforeloadValidate()) {
      this.matTableConfig([]);
      let objData = {
        GoodsTransfer: JSON.stringify([{
          from_date: this.objLoad.From_Date,
          to_date: this.objLoad.To_Date,
          wh_section_id: this._localStorage.getWhSectionId(),
          source_id: this._localStorage.getGlobalWarehouseId(),
          destination_id: this.objLoad.To_Location,
          status: this.objLoad.Status,
          company_section_id: +this._localStorage.getCompanySectionId()
        }]),
      }
      this._GoodsTransfer.getWithincity(objData).subscribe((result: any) => {
        if (result) {
          this.matTableConfig(result);
        } else
          this._confirmationDialogComponent.openAlertDialog('No records found', 'Goods Transfer (With in OtherState)');
      });
    }
  }
  fetchGoodsTransfer(transfer_id): void {
    let objData = {
      GoodsTransfer: JSON.stringify([{
        transfer_id: transfer_id,
        wh_section_id: this._localStorage.getWhSectionId(),
        source_id: this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._GoodsTransfer.fetchOtherState(objData).subscribe((result: ByNoModel[]) => {
      if (result) {
        this.objAction.isEditing = true;
        this.objEditableGrid.flags.isDisable = false;
        this.objEditableGrid.tabledata = [];
        this.objEditableGrid.tabledata = result;
        for (let i = 0; i < this.objEditableGrid.tabledata.length; i++) {
          this.objEditableGrid.tabledata[i].valid = true;
          this.objEditableGrid.tabledata[i].selling_price = this.objEditableGrid.tabledata[i].selling_price + '.00';
        }
        this.componentVisibility = !this.componentVisibility;
        this.objGoodsTransfer.Transfer_No = transfer_id;
        this.objGoodsTransfer.To_Sales_Location = this.objLoad.To_Location;
      }
    });
  }
  viewGoodsTransfer(transfer_id): void {
    let objData = {
      GoodsTransfer: JSON.stringify([{
        transfer_id: transfer_id,
        wh_section_id: this._localStorage.getWhSectionId(),
        source_id: this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId()
      }]),
    }
    this._GoodsTransfer.fetchOtherState(objData).subscribe((result: ByNoModel[]) => {
      if (result) {
        this.objEditableGrid.tabledata = [];
        this.objEditableGrid.tabledata = result;
        for (let i = 0; i < this.objEditableGrid.tabledata.length; i++) {
          this.objEditableGrid.tabledata[i].valid = true;
          this.objEditableGrid.tabledata[i].selling_price = this.objEditableGrid.tabledata[i].selling_price + '.00';
        }
        this.objEditableGrid.flags.isDisable = true;
        this.componentVisibility = !this.componentVisibility;
        this.objGoodsTransfer.Transfer_No = transfer_id;
        this.objGoodsTransfer.To_Sales_Location = this.objLoad.To_Location;
      }
    });
  }
  public openDeleteConfirmationDialog(data: any): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = 'Do you want cancel this record ?';
    dialogRef.componentInstance.componentName = 'Goods Transfer (With in OtherState)';
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.delete(data);
      dialogRef = null;
    });
  }

  public delete(data: any) {
    let objData = {
      GoodsTransfer: JSON.stringify([{
        transfer_date: this._datePipe.transform(data.transfer_date, 'dd/MM/yyyy'),
        transfer_id: data.transfer_id,
        cancel: false,
        wh_section_id: this._localStorage.getWhSectionId(),
        source_id: this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        entered_by: +this._localStorage.intGlobalUserId(),
        cancelled_reason: ''
      }]),
    }
    this._GoodsTransfer.cancelOtherState(objData).subscribe((result: any) => {
      if (result) {
        this._confirmationDialogComponent.openAlertDialog("Cancel successfully", "Goods Transfer (With in OtherState)");
        this.getGoodsTransfer();
      }
    });
  }
  public openApproveConfirmationDialog(data: any): any {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    _dialogRef.componentInstance.confirmMessage = "Do you want to approve this record ?"
    _dialogRef.componentInstance.componentName = "Goods Transfer (With in OtherState)";
    return _dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.approve(data);
      _dialogRef = null;
    });
  }

  private approve(data: any): void {
    let objData = {
      GoodsTransfer: JSON.stringify([{
        transfer_date: this._datePipe.transform(data.transfer_date, 'dd/MM/yyyy'),
        transfer_id: data.transfer_id,
        approve: true,
        wh_section_id: this._localStorage.getWhSectionId(),
        source_id: this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        entered_by: +this._localStorage.intGlobalUserId(),
      }]),
    }
    this._GoodsTransfer.approveOtherState(objData).subscribe((result: any) => {
      if (result !== null) {
        this._confirmationDialogComponent.openAlertDialog("Added to approved list", "Goods Transfer (With in OtherState)");
        this.getGoodsTransfer();
      }
    });
  }

}
