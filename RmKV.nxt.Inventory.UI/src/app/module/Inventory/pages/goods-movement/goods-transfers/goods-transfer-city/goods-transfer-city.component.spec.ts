import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsTransferCityComponent } from './goods-transfer-city.component';

describe('GoodsTransferCityComponent', () => {
  let component: GoodsTransferCityComponent;
  let fixture: ComponentFixture<GoodsTransferCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsTransferCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsTransferCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
