import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsTransferOtherStateComponent } from './goods-transfer-other-state.component';

describe('GoodsTransferOtherStateComponent', () => {
  let component: GoodsTransferOtherStateComponent;
  let fixture: ComponentFixture<GoodsTransferOtherStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsTransferOtherStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsTransferOtherStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
