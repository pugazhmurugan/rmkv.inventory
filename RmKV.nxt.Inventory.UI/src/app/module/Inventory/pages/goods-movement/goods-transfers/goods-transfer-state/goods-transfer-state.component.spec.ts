import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsTransferStateComponent } from './goods-transfer-state.component';

describe('GoodsTransferStateComponent', () => {
  let component: GoodsTransferStateComponent;
  let fixture: ComponentFixture<GoodsTransferStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsTransferStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsTransferStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
