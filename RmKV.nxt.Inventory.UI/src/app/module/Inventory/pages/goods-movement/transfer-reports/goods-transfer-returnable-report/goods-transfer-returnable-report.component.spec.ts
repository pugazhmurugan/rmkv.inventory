import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsTransferReturnableReportComponent } from './goods-transfer-returnable-report.component';

describe('GoodsTransferReturnableReportComponent', () => {
  let component: GoodsTransferReturnableReportComponent;
  let fixture: ComponentFixture<GoodsTransferReturnableReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsTransferReturnableReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsTransferReturnableReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
