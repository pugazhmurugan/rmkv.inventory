import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsTransferReportsComponent } from './goods-transfer-reports.component';

describe('GoodsTransferReportsComponent', () => {
  let component: GoodsTransferReportsComponent;
  let fixture: ComponentFixture<GoodsTransferReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsTransferReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsTransferReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
