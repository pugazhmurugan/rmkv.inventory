import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GodownCounterTransferComponent } from './godown-counter-transfer.component';

describe('GodownCounterTransferComponent', () => {
  let component: GodownCounterTransferComponent;
  let fixture: ComponentFixture<GodownCounterTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GodownCounterTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GodownCounterTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
