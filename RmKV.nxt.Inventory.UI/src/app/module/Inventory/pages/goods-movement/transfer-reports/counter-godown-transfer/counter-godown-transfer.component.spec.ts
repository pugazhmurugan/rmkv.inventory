import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterGodownTransferComponent } from './counter-godown-transfer.component';

describe('CounterGodownTransferComponent', () => {
  let component: CounterGodownTransferComponent;
  let fixture: ComponentFixture<CounterGodownTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterGodownTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterGodownTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
