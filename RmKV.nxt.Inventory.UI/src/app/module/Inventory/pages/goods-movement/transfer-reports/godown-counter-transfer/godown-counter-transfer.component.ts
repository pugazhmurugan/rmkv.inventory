import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { GodownToCounterService } from 'src/app/module/Inventory/services/goods-movement/internal-transfers/godown-to-counter/godown-to-counter.service';

@Component({
  selector: 'app-godown-counter-transfer',
  templateUrl: './godown-counter-transfer.component.html',
  styleUrls: ['./godown-counter-transfer.component.scss'],
  providers: [DatePipe]
})
export class GodownCounterTransferComponent implements OnInit {
  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    warehouse_id: '',
    company_section_id: '',
    transfer_id: '',
  }
  warehouseList: any[] = [];
  sectionList: any[] = [];
  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  constructor(public _minMaxDate: MinMaxDate, public _router: Router,
    public _localStorage: LocalStorage, private _datePipe: DatePipe, public _commonService: CommonService,
    public _confirmationDialogComponent: ConfirmationDialogComponent, public _GodownToCounterService: GodownToCounterService,
    public _keyPressEvents: KeyPressEvents) { }

  ngOnInit() {
    this.getWarehouseList();


  }
  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }
  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }
  private getWarehouseList(): void {
    this.warehouseList = this._localStorage.getWarehouse();

  }
  public getSectionList(): void {
    debugger;
    let obj = {
      Section: JSON.stringify([{
        sales_location_id: this._localStorage.getGlobalSalesLocationId(),
        warehouse_id: +this.load.warehouse_id
      }])
    }
    this._commonService.getGroupSection(obj).subscribe((result: any) => {
      debugger;
      if (result) {
       
        this.sectionList = JSON.parse(result);
        if (this.sectionList.length !== 1) {
          this.load.company_section_id = ''
        }
        else {
          this.load.company_section_id = this.sectionList[0].company_section_id;
        }
      }
    });
  }
  public CheckGodownToCounter(): void {
    debugger;
    if (this.beforeViewValidate()) {
      let obj = {
        GodownToCounterReport: JSON.stringify([{
          transfer_date: this.load.FromDate,
          company_section_id: +this.load.company_section_id,
          warehouse_id: +this.load.warehouse_id,
          transfer_id: +this.load.transfer_id,
          transfer_type: 'G',
          wh_section_id: this.sectionList.filter(x => +x.company_section_id == +this.load.company_section_id)[0].wh_section_id,
        }])
      }
      this._GodownToCounterService.checkGodownToCounterReport(obj).subscribe((result: any) => {
        debugger;
        if (result == 1) {
          this.getGodownToCounterReport();
        }
      });
    }
  }
  public getGodownToCounterReport() {
    debugger;
    let objLoad = [{
      transfer_date: this.load.FromDate,
      company_section_id: +this.load.company_section_id,
      warehouse_id: +this.load.warehouse_id,
      transfer_id: +this.load.transfer_id,
      transfer_type: 'G',
      wh_section_id: this.sectionList.filter(x => +x.company_section_id == +this.load.company_section_id)[0].wh_section_id,
    }];
    let objData = {
      GodownToCounterReport: JSON.stringify(objLoad),
      Type: true
    };
    
    console.log(objData, "Godown To Counter");
    this._GodownToCounterService.getGodownToCounterReport(objData).subscribe((result: any) => {
      if (result.size != 0) {
        let dataType = result.type;
        console.log(result);
        const file = new Blob([result], { type: dataType });
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      } else
        this._confirmationDialogComponent.openAlertDialog('No record found', 'Godown To Counter Report');
    });
  }

  private beforeViewValidate(): Boolean {
    if (this.load.warehouse_id === '') {
      document.getElementById('Warehouse').focus();
      this._confirmationDialogComponent.openAlertDialog('Select warehouse', 'Godown To Counter');
      return false;
    } else if (this.load.company_section_id === '') {
      document.getElementById('section').focus();
      this._confirmationDialogComponent.openAlertDialog('Select section', 'Godown To Counter');
      return false;
    } else if (this.load.transfer_id.toString().trim() === '' || this.load.transfer_id.toString().trim() === '0') {
      document.getElementById('transferNo').focus();
      this._confirmationDialogComponent.openAlertDialog('Enter transfer no', 'Godown To Counter Report');
      return false;
    } else return true;
  }
}
