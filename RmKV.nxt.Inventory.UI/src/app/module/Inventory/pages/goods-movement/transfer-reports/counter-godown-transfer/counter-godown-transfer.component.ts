import { DatePipe } from '@angular/common';
import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { CounterToGodownService } from 'src/app/module/Inventory/services/goods-movement/internal-transfers/counter-to-godown/counter-to-godown.service';

@Component({
  selector: 'app-counter-godown-transfer',
  templateUrl: './counter-godown-transfer.component.html',
  styleUrls: ['./counter-godown-transfer.component.scss'],
  providers: [DatePipe]
})
export class CounterGodownTransferComponent implements OnInit {

  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    company_section_id: 0,
    transfer_id: 0
  }

  sectionsList: any = [];

  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  constructor(
    public _minMaxDate: MinMaxDate, public _router: Router,
    public _localStorage: LocalStorage, private _datePipe: DatePipe,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _countertogodown: CounterToGodownService,
    private _commonService: CommonService
  ) {
    this.getSections();
  }

  ngOnInit() {
  }

  private getSections(): void {
    debugger;
    let objSections = {
      GetAllCompanySection: JSON.stringify([{
        warehouse_id: this._localStorage.getGlobalWarehouseId(),
        sales_location_id: this._localStorage.getGlobalSalesLocationId()
      }])
    }
    this._commonService.getWarehouseBasedCompanySection(objSections).subscribe((result: string) => {
      if (result && JSON.parse(result))
        this.sectionsList = JSON.parse(result);
      this.load.company_section_id = this.sectionsList[0].company_section_id;
     
    });
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }
  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }

  private beforeloadValidate(): boolean {
    if (!this.load.FromDate.toString().trim()) {
      document.getElementById('matDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter transfer date", "Counter - Godown Transfer");
      return false;
    } if (this.load.FromDate.toString().trim().length !== 10) {
      document.getElementById('matDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Invalid transfer date", "Counter - Godown Transfer");
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.load.company_section_id) !== -1) {
      document.getElementById('section_id').focus();
      this._confirmationDialogComponent.openAlertDialog("Select section", "Counter - Godown Transfer");
      return false;
    } if ([null, 'null', undefined, 'undefined', NaN, 'NaN', 0, '0', ''].indexOf(this.load.transfer_id) !== -1) {
      document.getElementById('transferid').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter transfer number", "Counter - Godown Transfer");
      return false;
    } else
      return true;
  }

  public checkTransferNo() {
    if (this.beforeloadValidate()) {
      let objFormR = {
        GoodCounter: JSON.stringify([{
          transfer_date: this.load.FromDate,
          company_section_id: +this.load.company_section_id,
          warehouse_id : this._localStorage.getGlobalWarehouseId(),
          transfer_id: +this.load.transfer_id,
          wh_section_id: +this.getWhSectionId(),
          transfer_type: 'C'
        }])
      }
      this._countertogodown.checkCounterToGodownReport(objFormR).subscribe((result: any) => {
        if (result === 1) {
          this.generateReport();
          //this._confirmationDialogComponent.openAlertDialog('Valid tranfer number', 'Counter - Godown Transfer');
        }
        else
      {
          this._confirmationDialogComponent.openAlertDialog('Invalid tranfer number', 'Counter - Godown Transfer');
      }
      });
    }
  }

  private getWhSectionId(): number {
    return this.sectionsList[this.sectionsList.findIndex(x => +x.company_section_id === +this.load.company_section_id)].wh_section_id;
  }

  public generateReport(): void {
    if (this.beforeloadValidate()) {
      let objFormR = {
        GoodCounter: JSON.stringify([{
          transfer_date: this.load.FromDate,
          company_section_id: +this.load.company_section_id,
          transfer_id: +this.load.transfer_id,
          warehouse_id : this._localStorage.getGlobalWarehouseId(),
          wh_section_id: +this.getWhSectionId(),
          transfer_type: 'C'
        }])
      }
      this._countertogodown.getCounterToGodownReport(objFormR).subscribe((result: any) => {
        if (result.size != 0) {
          // console.log(result);
          let dataType = result.type;
          const file = new Blob([result], { type: dataType });
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL);
        } else
          this._confirmationDialogComponent.openAlertDialog('No records found', 'Counter - Godown Transfer')
      });
    }
  }
}
