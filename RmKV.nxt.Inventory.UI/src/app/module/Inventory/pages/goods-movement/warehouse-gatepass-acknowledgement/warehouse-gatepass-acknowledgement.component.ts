import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { EditMode } from 'src/app/common/models/common-model';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { WarehouseGatepassAcknowledgmentService } from '../../../services/goods-movement/warehouse-gatepass-acknowledgement/warehouse-gatepass-acknowledgment.service';
declare var jsPDF: any;

@Component({
  selector: 'app-warehouse-gatepass-acknowledgement',
  templateUrl: './warehouse-gatepass-acknowledgement.component.html',
  styleUrls: ['./warehouse-gatepass-acknowledgement.component.scss'],
  providers: [DatePipe]
})
export class WarehouseGatepassAcknowledgementComponent implements OnInit {
  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };
  toSectionsList: any = [];
  SectionsList: any = [];
  newToSectionList: any = [];
  loadSectionsList: any = [];

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  public valDate: DateValidation = new DateValidation();
  tempFromDate = new Date();
  tempToDate = new Date();
  objLoad = {
    From_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    To_Date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    Location: this._localStorage.getwareHouseName(),
    Section: 0
  }

  objWarehouseGatePass: any = {
    grn_no: 0,
    Inward_Gatepass_Id: '',
    sales_location_id: 0,
    Store_Location: this._localStorage.getwareHouseName(),
    Received_Date: new Date(),
    From_Warehouse: '',
    Section: 0,
    To_Section: '',
    Gatepass_No: '',
    EwaybillNo: '',
    Bale_Count: '',
    Remarks: '',
    transfer_type: '',
    transfer_flag: '',
    gatepass_title : '',
    Transfer_Details: []
  }

  unChangedWarehouseGatePass = {
    grn_no: 0,
    Inward_Gatepass_Id: '',
    sales_location_id: 0,
    Store_Location: this._localStorage.getwareHouseName(),
    Received_Date: new Date(),
    From_Warehouse: '',
    Section: 0,
    To_Section: '',
    Gatepass_No: '',
    EwaybillNo: '',
    Bale_Count: '',
    Remarks: '',
    transfer_type: '',
    transfer_flag: '',
    Transfer_Details: []
  }

  modifyWarehouseGatePass = {
    grn_no: 0,
    Inward_Gatepass_Id: '',
    sales_location_id: 0,
    Store_Location: this._localStorage.getwareHouseName(),
    Received_Date: new Date(),
    From_Warehouse: '',
    Section: 0,
    To_Section: '',
    Gatepass_No: '',
    EwaybillNo: '',
    Bale_Count: '',
    Remarks: '',
    transfer_type: '',
    transfer_flag: '',
    Transfer_Details: []
  }

  objGatepassDetails = {
    GatePass_No: 0,
    GatePass_Date: '',
    GatePass_Type: '',
    Gate_Section_Name: '',
    Account_Name: '',
    Bale_Count: 0,
    Supplier_Code: '',
    Bale_Value: 0
  }
  // componentVisibility: boolean = false;
  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "Date", "From", "Section", "GRN", "Gatepass", "Gatepass_Date", "Bale", "Action"];
  sectionsList: any = [];
  warehouseList: any;
  gatePassList: any;
  ToSectionsList: any;
  warehouseGatePassList: any;
  locationList: any;
  locationwarehouseList: any;
  transferTypeList: any[] = [];
  sourceLocationList: any = [];
  filteredLocationList: any = [];
  grnNoList: any = [];
  constructor(public _localStorage: LocalStorage,
    public _router: Router,
    private _confirmationDialogComponent: ConfirmationDialogComponent,
    public _excelService: ExcelService,
    private _warehouseGatepassAcknowledgmentService: WarehouseGatepassAcknowledgmentService,
    public _datePipe: DatePipe,
    public _matDialog: MatDialog,
    public _keyPress: KeyPressEvents,
    public _commonService: CommonService,
    private _dialogRef: MatDialogRef<ConfirmationDialogComponent>,) {
    this.tempFromDate.setDate(this.tempToDate.getDate() - 7);
    this.objLoad.From_Date = this._datePipe.transform(this.tempFromDate, 'dd/MM/yyyy');
  }
  ngOnInit() {
    this.getTransferTypeList();
    this.getSections();
    this.getSourceLocations();
    this.getToSections();
    this.getLoadSections();
  }

  public validateFromDate(): void {
    let date = this.valDate.validateDate(this.objLoad.From_Date, this.tempFromDate, this.minDate, this.maxDate);
    this.objLoad.From_Date = date[0];
    this.tempFromDate = date[1];
  }
  public validateToDate(): void {
    let date = this.valDate.validateDate(this.objLoad.To_Date, this.tempToDate, this.minDate, this.maxDate);
    this.objLoad.To_Date = date[0];
    this.tempToDate = date[1];
  }


  private getSourceLocations(): void {
    let objAllWorkingLocation = {
      SalesLocation: JSON.stringify([{

      }])
    };
    this._commonService.getAllWorkingLocation(objAllWorkingLocation).subscribe(
      (res: any) => {
        this.sourceLocationList = res;
      }
    )
    this.sourceLocationList.splice(0, 1);
    this.objWarehouseGatePass.sales_location_id = 0;
    this.objWarehouseGatePass.From_Warehouse = '';
    this.objWarehouseGatePass.Section = 0;
    this.objWarehouseGatePass.To_Section = '';  
}


  public getWarehouse(): void {
    debugger;
    this.warehouseList = [];
    this.objWarehouseGatePass.From_Warehouse = '';
    let objPass = {
      GetSameWarehouse: JSON.stringify([{
        sales_location_id: +this.objWarehouseGatePass.sales_location_id
      }])
    }
    this._warehouseGatepassAcknowledgmentService.getSameWareHouse(objPass).subscribe((result: any) => {
      if (result)
        this.warehouseList = result;
      console.log(this.warehouseList, 'ware')
      this.removeGlobalWarehouse();
    });
  }

  
  private removeGlobalWarehouse(): void {
    let index = -1;
    index = this.warehouseList.findIndex(ele => +ele.WareHouse_Id == +this._localStorage.getGlobalWarehouseId())
    console.log(index, 'Global Warehosue');
    if (index !== -1) {
      this.warehouseList.splice(index, 1);
    }
  }


  public getWarehouseGRNNoList(): void {
    debugger;
    this.grnNoList = [];
    let objPass = {
      GetGRNNo: JSON.stringify([{
        warehouse_id: +this._localStorage.getGlobalWarehouseId(),
        wh_section_id: +this._localStorage.getWhSectionId(),
        from_warehouse_id: +this.objWarehouseGatePass.From_Warehouse
      }])
    }
    this._warehouseGatepassAcknowledgmentService.getGRNNoListDetails(objPass).subscribe((result: any) => {
      if (JSON.parse(result))
        this.grnNoList = JSON.parse(result);
      console.log(this.grnNoList, 'grnno')
    });
  }

  public getWarehouseGatePassDetails(): void {
    debugger;
    this.objWarehouseGatePass.Transfer_Details = [];
    let objPass = {
      GetGatePassDetails: JSON.stringify([{
        warehouse_id: +this._localStorage.getGlobalWarehouseId(),
        wh_section_id: +this._localStorage.getWhSectionId(),
        from_warehouse_id: +this.objWarehouseGatePass.From_Warehouse,
        grn_no: +this.objWarehouseGatePass.grn_no
      }])
    }
    this._warehouseGatepassAcknowledgmentService.getWarehouseAckGatePassDetails(objPass).subscribe((result: any) => {
      if (JSON.parse(result)) {
        debugger;
        for (let i = 0; i < JSON.parse(result).length; i++) {
          this.objWarehouseGatePass.Gatepass_No = JSON.parse(result)[i].gatepass_no;
          this.objWarehouseGatePass.EwaybillNo = JSON.parse(result)[i].eway_billno;
          this.objWarehouseGatePass.gatepass_title = JSON.parse(result)[i].gatepass_title;
          this.objWarehouseGatePass.Transfer_Details = JSON.parse(result)[i].transfer_data;
        } this.objWarehouseGatePass.Bale_Count = +(this.objWarehouseGatePass.Transfer_Details.length)

      }

    });
  }

  public checkWarehouse() {
    debugger
    if (!this.objWarehouseGatePass.From_Warehouse.toString().trim()) {
      document.getElementById('fromwarehouse').focus();
      this._confirmationDialogComponent.openAlertDialog("Select from warehouse", "Warehouse Gatepass Acknowledgement");
      return false;
    } else
      return true;
  }

  // public checkFromLocation() {
  //   debugger
  //   if (!this.objWarehouseGatePass.From_Warehouse.toString().trim()) {
  //     document.getElementById('fromwarehouse').focus();
  //     this._confirmationDialogComponent.openAlertDialog("Select from warehouse", "Warehouse Gatepass Acknowledgement");
  //     return false;
  //   } else
  //     return true;
  // }



  public onEnterGatepassLookupDetails(): void {
    debugger;
    let gatepasslookup: any = {
      gatepass_no: 0,
      gatepass_title: '',
      company_section_id: 0,
      company_section_name: '',
      destination_id: 0,
      destination_name: '',
      gatepass_date: '',
      no_of_bags: 0,
      transfer_name: ''
    };
    debugger;
    gatepasslookup = this.gatePassList.find(act => act.gatepass_no === +this.objWarehouseGatePass.Gatepass_No);
    this.objGatepassDetails.GatePass_No = gatepasslookup && gatepasslookup.gatepass_no ? gatepasslookup.gatepass_no : 0;
    this.objGatepassDetails.GatePass_Type = gatepasslookup && gatepasslookup.gatepass_title ? gatepasslookup.gatepass_title :
      '';
    this.objGatepassDetails.Bale_Value = gatepasslookup && gatepasslookup.Bale_Value ? gatepasslookup.Bale_Value : 0;
    this.objGatepassDetails.Supplier_Code = gatepasslookup && gatepasslookup.Supplier_Code ? gatepasslookup.Supplier_Code :
      this.objGatepassDetails.Supplier_Code;
    this.objGatepassDetails.Account_Name = gatepasslookup && gatepasslookup.Account_Name ? gatepasslookup.Account_Name :
      this.objGatepassDetails.Account_Name;
    this.objGatepassDetails.Bale_Count = gatepasslookup && gatepasslookup.no_of_bags ? gatepasslookup.no_of_bags : 0;
    this.objGatepassDetails.Gate_Section_Name = gatepasslookup && gatepasslookup.company_section_name ? gatepasslookup.company_section_name :
      '';
    this.objGatepassDetails.GatePass_Date = gatepasslookup && gatepasslookup.gatepass_date ? gatepasslookup.gatepass_date :
      '';
  }

  beforeLoadValidate(): any {
    if (this.objLoad.From_Date.toString() === "") {
      document.getElementById("fromDate").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter from date', 'Warehouse Gatepass Acknowledgement');
      return false;
    } else if (this.objLoad.From_Date.toString().length !== 10) {
      document.getElementById("fromDate").focus();
      this.objLoad.From_Date = "";
      this._confirmationDialogComponent.openAlertDialog('Enter valid from date', 'Warehouse Gatepass Acknowledgement');
      return false;
    } else if (!this.objLoad.To_Date.toString().trim()) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter to date", "Warehouse Gatepass Acknowledgement");
      this.objLoad.To_Date = "";
      return false;
    } else if (this.objLoad.To_Date.toString().length !== 10) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter valid to date", "Warehouse Gatepass Acknowledgement");
      return false;
    } else if (this.tempToDate < this.tempFromDate) {
      document.getElementById('toDate').focus();
      this._confirmationDialogComponent.openAlertDialog("To date can't be less than from date", "Warehouse Gatepass Acknowledgement");
      return false;
    } else if (!this.objLoad.Section.toString().trim()) {
      document.getElementById('loadsection').focus();
      this._confirmationDialogComponent.openAlertDialog("Select location", "Warehouse Gatepass Acknowledgement");
      return false;
    } else {
      return true;
    }
  }

  public getWarehouseGatePass(): void {
    if (this.beforeLoadValidate()) {
      this.matTableConfig([]);
      let objSlip: any = {
        LaodInwardGatePass: JSON.stringify([{
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          from_date: this.objLoad.From_Date,
          to_date: this.objLoad.To_Date,
          company_section_id: +this.objLoad.Section,
        }]),
      }
      this._warehouseGatepassAcknowledgmentService.getInwardGatePass(objSlip).subscribe((result: any) => {
        if (result) {
          this.warehouseGatePassList = result;
          this.matTableConfig(result);
        } else {
          this._confirmationDialogComponent.openAlertDialog("No records found.", "Warehouse Gatepass Acknowledgement");
        }
      });
    }
  }

  public onChangeNewFields(): void {
    this.gatePassList = [];
    this.objWarehouseGatePass.Gatepass_No = '';
    this.objWarehouseGatePass.Transfer_Details = [];
    this.objWarehouseGatePass.Bale_Count = '';
  }

  public GetWarehouseTransferDetail(): void {
    this.objWarehouseGatePass.Transfer_Details = [];
    this.objWarehouseGatePass.Bale_Count = ''
    let objSlip: any = {
      GetInwardGatePass: JSON.stringify([{
        wh_section_id: +this.sectionsList.filter(x => +x.company_section_id == +this.objWarehouseGatePass.Section)[0].wh_section_id,
        company_section_id: +this.objWarehouseGatePass.Section,
        warehouse_id: +this.objWarehouseGatePass.From_Warehouse,
        transfer_type: this.objWarehouseGatePass.transfer_type,
        gatepass_no: +this.objWarehouseGatePass.Gatepass_No,
      }]),
    }
    this._warehouseGatepassAcknowledgmentService.getInwardGatePassDteails(objSlip).subscribe((result: any) => {
      if (JSON.parse(result)) {
        this.objWarehouseGatePass.Transfer_Details = JSON.parse(result);
        console.log(JSON.parse(result), 'transfer');
        this.modifyWarehouseGatePass.Transfer_Details = JSON.parse(result);
        this.objWarehouseGatePass.Bale_Count = +(this.objWarehouseGatePass.Transfer_Details.length)
      }
    });
  }

  public getTotal(): void {
    return this.objWarehouseGatePass.Transfer_Details.map(t => t.Transfer_Qty).reduce((acc, value) => acc + value, 0);
  }

  private getTransferTypeList(): void {
    debugger;
    let obj = {
      GetTransferType: JSON.stringify([{
        virtual_inward: 'true'
      }])
    }
    this._warehouseGatepassAcknowledgmentService.getTransferTypeDteails(obj).subscribe((result: any) => {
      if (JSON.parse(result))
        this.transferTypeList = JSON.parse(result);
      console.log(this.transferTypeList, 'type')
    });
  }

  public getSections(): void {
    debugger;
    this.objWarehouseGatePass.Section = 0;
    let objSections = {
      GetAllCompanySection: JSON.stringify([{
        warehouse_id: +this.objWarehouseGatePass.From_Warehouse,
        sales_location_id: +this.objWarehouseGatePass.sales_location_id
      }])
    }
    this._commonService.getWarehouseBasedCompanySection(objSections).subscribe((result: any) => {
      if (JSON.parse(result))
        this.sectionsList = JSON.parse(result);
      console.log(this.sectionsList, 'sectionsss');
    });
  }

  public getNewToSections(): void {
    debugger;
    this.sectionsList = [];
    let objSections = {
      GetWarehouseSections: JSON.stringify([{
        warehouse_id: +this.objWarehouseGatePass.From_Warehouse
      }])
    }
    this._commonService.getCommonWarehouseSections(objSections).subscribe((result: Sections) => {
      if (result)
        this.newToSectionList = result;
      console.log(this.newToSectionList, 'neww')
    });
  }

  private getLoadSections(): void {
    debugger;
    let objSections = {
      GetAllCompanySection: JSON.stringify([{
        warehouse_id: +this._localStorage.getGlobalWarehouseId(),
        sales_location_id: this._localStorage.getGlobalSalesLocationId()
      }])
    }
    this._commonService.getWarehouseBasedCompanySection(objSections).subscribe((result: any) => {
      if (JSON.parse(result))
        this.loadSectionsList = JSON.parse(result);
      this.objLoad.Section = this.loadSectionsList[0].company_section_id;
      console.log(this.loadSectionsList, 'loadSec');
    });
  }

  public getGetPass(): void {
    debugger;
    this.gatePassList = [];
    let objPass = {
      GetGatePassNo: JSON.stringify([{
        wh_section_id: this.sectionsList.filter(x => +x.company_section_id == +this.objWarehouseGatePass.Section)[0].wh_section_id,
        company_section_id: +this.objWarehouseGatePass.Section,
        warehouse_id: +this._localStorage.getGlobalWarehouseId(),
        transfer_type: this.objWarehouseGatePass.transfer_type,
      }])
    }
    this._warehouseGatepassAcknowledgmentService.getInwardGatePassNoList(objPass).subscribe((result: any) => {
      if (JSON.parse(result))
        this.gatePassList = JSON.parse(result);
      console.log(this.gatePassList, 'sycvhg')
    });
  }

  private getToSections(): void {
    let objSections = {
      GetGRNSections: JSON.stringify([{
        warehouse_id: this._localStorage.getGlobalWarehouseId()
      }])
    }
    this._commonService.getGRNSections(objSections).subscribe((result: Sections) => {
      if (result)
        this.ToSectionsList = result;
    });
  }



  checkSectionAndWare() {
    if (!this.objWarehouseGatePass.From_Warehouse.toString().trim()) {
      document.getElementById('fromwarehouse').focus();
      this._confirmationDialogComponent.openAlertDialog("Select from warehouse", "Warehouse Gatepass Acknowledgement");
      return false;
    } else if (!this.objWarehouseGatePass.Section.toString().trim()) {
      document.getElementById('section').focus();
      this._confirmationDialogComponent.openAlertDialog("Select section", "Warehouse Gatepass Acknowledgement");
      return false;
    } else
      return true;
  }

  public matTableConfig(tableRecords?: any) {
    if (tableRecords) {
      for (let i = 0; i < tableRecords.length; i++) {
        if (this._datePipe.transform(tableRecords[i].Received_Date, 'dd/MM/yyyy') === this.maxDate)
          tableRecords[i].current_date = true;
      }
      this.dataSource = new MatTableDataSource(tableRecords);
    }
    else
      this.dataSource = new MatTableDataSource([]);
  }

  private checkAnyChangesMade(): boolean {
    if (JSON.stringify(this.objWarehouseGatePass) !== (!this.objAction.isEditing ? JSON.stringify(this.unChangedWarehouseGatePass) : JSON.stringify(this.modifyWarehouseGatePass))) {
      return true;
    } else {
      return false;
    }
  }
  public onClear(exitFlag: boolean): void {
    if (!this.objAction.isView) {
      if (this.checkAnyChangesMade())
        this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
      else if (exitFlag)
        this.onListClick();
    } else {
      this.onListClick();
    }
  }
  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Warehouse Gatepass Acknowledgement";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }
  public newClick(): void {
    this.objAction.isEditing = false;
    this.objAction.isView = false;
    // this.componentVisibility = !this.componentVisibility;
    this.resetScreen();
  }

  private resetScreen(): void {
    if (this.objAction.isEditing === true) {
      this.objWarehouseGatePass = JSON.parse(JSON.stringify(this.modifyWarehouseGatePass));
      this.modifyWarehouseGatePass = JSON.parse(JSON.stringify(this.modifyWarehouseGatePass));
    } else {
      this.objAction = JSON.parse(JSON.stringify(this.unChangedAction));
      this.objWarehouseGatePass = JSON.parse(JSON.stringify(this.unChangedWarehouseGatePass));
      this.modifyWarehouseGatePass = JSON.parse(JSON.stringify(this.unChangedWarehouseGatePass));
    }
  }
  public onListClick(): void {
    this.resetScreen();
    // this.componentVisibility = !this.componentVisibility;
  }

  public saveWarehouseGatePass() {
    if (this.beforeSaveValidation()) {
      let objWarehouseGatePass: any = {
        SaveWarehouseGatePass: JSON.stringify([{
          // inward_gatepass_id: this.objWarehouseGatePass.Inward_Gatepass_Id,
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          wh_section_id: +this._localStorage.getWhSectionId(),
          from_warehouse_id: +this.objWarehouseGatePass.From_Warehouse,
          entered_by: this._localStorage.intGlobalUserId(),
          grn_no: +this.objWarehouseGatePass.Transfer_Details[0].grn_no,
          gatepass_no: +this.objWarehouseGatePass.Gatepass_No
        }]),
      }
      this._warehouseGatepassAcknowledgmentService.addWarehouseAckGatePass(objWarehouseGatePass).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" : "New Warehouse Gatepass Acknowledgement record has been created", "Inward Slips");
          this.resetScreen();
        }
      });
    }
  }

  private beforeSaveValidation() {
    debugger;
    if (this.objWarehouseGatePass.sales_location_id === 0) {
      document.getElementById('sourceLocation').focus();
      this._confirmationDialogComponent.openAlertDialog("Select from location", "Warehouse Gatepass Acknowledgement");
      return false;
    } else if (!this.objWarehouseGatePass.From_Warehouse.toString().trim()) {
      document.getElementById('fromwarehouse').focus();
      this._confirmationDialogComponent.openAlertDialog("Select from warehouse", "Warehouse Gatepass Acknowledgement");
      return false;
    } else if (this.objWarehouseGatePass.grn_no === 0) {
      document.getElementById('grnNo').focus();
      this._confirmationDialogComponent.openAlertDialog("Select grn no", "Warehouse Gatepass Acknowledgement");
      return false;
    } else if (!this.objWarehouseGatePass.Bale_Count.toString().trim() || +this.objWarehouseGatePass.Bale_Count <= 0) {
      document.getElementById('Bale').focus();
      this._confirmationDialogComponent.openAlertDialog("Enter bale count", "Warehouse Gatepass Acknowledgement");
      return false;
    } else
      return true;
  }

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Received Date': this._datePipe.transform(this.dataSource.data[i].Received_Date, 'dd/MM/yyyy'),
          'Source Location': this.dataSource.data[i].Source_Warehouse_Name,
          'Section': this.dataSource.data[i].Source_Wh_Section_Name,
          'GRN No': this.dataSource.data[i].Source_GRN_No,
          "Gatepass No": this.dataSource.data[i].Gatepass_No,
          'Gatepass Date': this._datePipe.transform(this.dataSource.data[i].Gatepass_Date, 'dd/MM/yyyy'),
          'Bale Count': this.dataSource.data[i].Bale_Count,
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Warehouse Gatepass Acknowledgement",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Warehouse Gatepass Acknowledgement");
  }
  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.warehouseGatePassList.forEach(e => {
        var tempObj = [];
        tempObj.push(this._datePipe.transform(e.Received_Date, 'dd/MM/yyyy'));
        tempObj.push(e.Source_Warehouse_Name);
        tempObj.push(e.Source_Wh_Section_Name);
        tempObj.push(e.Source_GRN_No);
        tempObj.push(e.Gatepass_No);
        tempObj.push(this._datePipe.transform(e.Gatepass_Date, 'dd/MM/yyyy'));
        tempObj.push(e.Bale_Count);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Received Date', 'Source Location', 'Section', 'GRN No', 'Gatepass No', 'Gatepass Date', 'Bale Count']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Warehouse Gatepass Acknowledgement' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Warehouse Gatepass Acknowledgement");
  }

  //   private getSections(): void {
  //     debugger;
  //     let objSections = {
  //       GetAllCompanySection: JSON.stringify([{
  //         warehouse_id: +this.objWarehouseGatePass.From_Warehouse,
  //         sales_location_id : this._localStorage.getGlobalSalesLocationId()
  //       }])
  //     }
  //     this._warehouseGatepassAcknowledgmentService.getWarehouseBasedCompanySection(objSections).subscribe((result : any) => {
  //       if (JSON.parse(result))
  //         this.sectionsList = JSON.parse(result);
  //         this.toSectionsList = JSON.parse(result);
  //         for(let i =0; i < this.toSectionsList; i++ ){
  //           if(this.objWarehouseGatePass.Section === this.toSectionsList[i].company_section_id){
  //             this.toSectionsList.splice(0,1)
  //           }
  //         }
  //     });
  //   }

  //   checkWarehouse() {
  //     debugger
  //     if (!this.objWarehouseGatePass.From_Warehouse.toString().trim()) {
  //       document.getElementById('fromwarehouse').focus();
  //       this._confirmationDialogComponent.openAlertDialog("Select from warehouse", "Warehoue Gatepass Acknowledgment");
  //       return false;
  //     } else
  //       return true;
  //   }

  //   public getNewToSections(): void {
  //     debugger;
  //     this.sectionsList = [];
  //     let objSections = {
  //       GetGRNSections: JSON.stringify([{
  //         warehouse_id: +this.objWarehouseGatePass.From_Warehouse
  //       }])
  //     }
  //     this._commonService.getGRNSections(objSections).subscribe((result: any) => {
  //       if (result)
  //         this.newToSectionList = result;
  //         console.log(this.newToSectionList,'newToSectionList')
  //     });
  //   }

  //   private getTransferType(): void {
  //     let objData = {
  //       DCEway: JSON.stringify([{
  //         virtual: this._localStorage.getVirtual()
  //       }]),
  //     }
  //     debugger;
  //     this._warehouseGatepassAcknowledgmentService.getType(objData).subscribe((result: any) => {
  //       if (result) {
  //         this.transferTypeList = JSON.parse(JSON.stringify(result));
  //         console.log(result,"transferTypeList")
  //       }
  //     });
  //   }

}

export class Sections {
  Wh_Section_ID: number;
  Group_Section_ID: number;
  Group_Section_Name: string;
}