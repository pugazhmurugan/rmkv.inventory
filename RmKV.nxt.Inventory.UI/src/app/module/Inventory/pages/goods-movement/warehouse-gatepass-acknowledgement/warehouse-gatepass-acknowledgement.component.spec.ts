import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseGatepassAcknowledgementComponent } from './warehouse-gatepass-acknowledgement.component';

describe('WarehouseGatepassAcknowledgementComponent', () => {
  let component: WarehouseGatepassAcknowledgementComponent;
  let fixture: ComponentFixture<WarehouseGatepassAcknowledgementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseGatepassAcknowledgementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseGatepassAcknowledgementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
