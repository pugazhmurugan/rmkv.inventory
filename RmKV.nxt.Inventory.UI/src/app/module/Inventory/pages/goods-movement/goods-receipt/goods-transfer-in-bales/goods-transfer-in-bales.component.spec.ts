import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsTransferInBalesComponent } from './goods-transfer-in-bales.component';

describe('GoodsTransferInBalesComponent', () => {
  let component: GoodsTransferInBalesComponent;
  let fixture: ComponentFixture<GoodsTransferInBalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsTransferInBalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsTransferInBalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
