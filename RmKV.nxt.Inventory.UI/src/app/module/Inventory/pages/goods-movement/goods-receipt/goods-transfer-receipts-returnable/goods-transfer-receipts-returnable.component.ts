import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-goods-transfer-receipts-returnable',
  templateUrl: './goods-transfer-receipts-returnable.component.html',
  styleUrls: ['./goods-transfer-receipts-returnable.component.scss'],
  providers: [DatePipe]
})
export class GoodsTransferReceiptsReturnableComponent implements OnInit {

  
  componentVisibility: boolean = true;
  displayedColumns = ["Serial_No", "Transfer_No", "Transfer_Date", "Ack_Date", "From_Section", "To_Section", "Ack_Pcs", "Ack_Qty", "Action"];

  Date: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  objLoad: any = {
    section: this._localStorage.getCompanySectionName(),
    from_warehouse: this._localStorage.getwareHouseName(),
    to_section: 0,
    from_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    to_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  objGodownToCounter: any = {
    from_section: this._localStorage.getCompanySectionName(),
    from_warehouse: this._localStorage.getwareHouseName(),
    to_location: 0,
    transfer_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    transfer_ack_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    transfer_id: '',
    byno: '',
    acknowledged_pieces: 0,
    total_pieces: 0
  }

  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  date1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  constructor(
    public _minMaxDate: MinMaxDate,
    public _router: Router,
    public _localStorage: LocalStorage,
    private _datePipe: DatePipe,
    public _commonService: CommonService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
  ) {   
    this.objLoad.from_date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 2)));
  }


  ngOnInit() {
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }

  public validateDate(): void {
    let date = this.dateValidation.validateDate(this.objGodownToCounter.transfer_date, this.date1, this.minDate, this.maxDate);
    this.objGodownToCounter.transfer_date = date[0];
    this.date1 = date[1];
  }

  newClick(): void {
    this.componentVisibility = !this.componentVisibility;
  }

  onClear(): void {
    this.componentVisibility = !this.componentVisibility;
  }
  
}
