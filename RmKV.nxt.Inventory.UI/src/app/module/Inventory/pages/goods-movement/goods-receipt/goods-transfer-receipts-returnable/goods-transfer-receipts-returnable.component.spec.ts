import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsTransferReceiptsReturnableComponent } from './goods-transfer-receipts-returnable.component';

describe('GoodsTransferReceiptsReturnableComponent', () => {
  let component: GoodsTransferReceiptsReturnableComponent;
  let fixture: ComponentFixture<GoodsTransferReceiptsReturnableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsTransferReceiptsReturnableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsTransferReceiptsReturnableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
