import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MAT_HAMMER_OPTIONS } from '@angular/material';
import { Router } from '@angular/router';
import { Rights, SalesLocation } from 'src/app/common/models/common-model';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { GoodsTransferBalesService } from 'src/app/module/Inventory/services/goods-movement/good-receipts/goods-transfer-bales/goods-transfer-bales.service';
declare var jsPDF: any;

@Component({
  selector: 'app-goods-transfer-in-bales',
  templateUrl: './goods-transfer-in-bales.component.html',
  styleUrls: ['./goods-transfer-in-bales.component.scss'],
  providers: [DatePipe]
})
export class GoodsTransferInBalesComponent implements OnInit {
  workingLocationList: SalesLocation[] = [];
  getBalesList: any[] = [{
    remote_transfer_id: 0,
    remote_transfer_date: '',
    source_warehouse_id: 0,
    source_warehouse_name: '',
    is_selected: false
  }];
  getSectionGRNList: any[] = [];
  objLoad: any = {
    section: this._localStorage.getCompanySectionName(),
    sales_location_id: 0,
    grn_date: '',
    section_grn_no: ''
  }

  unChangedLoad: any = {
    section: this._localStorage.getCompanySectionName(),
    sales_location_id: 0,
    grn_date: '',
    section_grn_no: ''
  }
  
  rights: Rights = {
    Add: false,
    Update: false,
    Delete: false,
    Approve: false,
    View: false
  };

  dataSource: any = new MatTableDataSource([]);
  displayedColumns = ["Serial_No", "remote_transfer_id", "remote_transfer_date", "Source_From"];
  @ViewChild(MatSort, null) sort: MatSort;

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    public _commonService: CommonService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _goodsTransferBalesService: GoodsTransferBalesService,
    public _datePipe: DatePipe,
    public _excelService: ExcelService) { }

  ngOnInit() {
    this.getAllWorkingLocation();
    this.getGoodsBalesSectionGRN();
    this.getRights();
  }

  private getRights(): void {
    var rights = this._localStorage.getMenuRights("/GoodsTransferinBales");
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "Delete" && rights[x].Status == true) {
        this.rights.Delete = true;
      }
      if (rights[x].Right_Name == "Approve" && rights[x].Status == true) {
        this.rights.Approve = true;
      }
      if (rights[x].Right_Name == "View" && rights[x].Status == true) {
        this.rights.View = true;
      }
    }
  }

  private getAllWorkingLocation(): void {
    let objAllWorkingLocation = {
      SalesLocation: JSON.stringify([{

      }])
    };
    this._commonService.getAllWorkingLocation(objAllWorkingLocation).subscribe((result: any) => {
      this.workingLocationList = result;
      this.workingLocationList.splice(0, 1);
      for (let i = 0; i < this.workingLocationList.length; i++) {
        if (this._localStorage.getGlobalSalesLocationId() === this.workingLocationList[i].Sales_Location_ID) {
          this.workingLocationList.splice(i, 1);
        } this.objLoad.sales_location_id = this.workingLocationList[0].Sales_Location_ID;
      }
    });
  }

  public getGoodsBalesSectionGRN(): void {
    debugger;
    let objData = {
      BalesSectionGRN: JSON.stringify([{
        wh_section_id: +this._localStorage.getWhSectionId(),
        warehouse_id: +this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
      }]),
    }
    this._goodsTransferBalesService.getGoodsBalesSectionGRN(objData).subscribe((result: any) => {
      if (JSON.parse(result) !== null) {
        this.getSectionGRNList = JSON.parse(result);
        console.log(this.getSectionGRNList,'section')
        for(let i = 0; i< this.getSectionGRNList.length; i++){
          this.objLoad.section_grn_no = this.getSectionGRNList[i].section_grn_no; 
          this.objLoad.grn_date = this._datePipe.transform(this.getSectionGRNList[i].grn_date,'dd/MM/yyyy');
          this.objLoad.grn_no = this.getSectionGRNList[i].grn_no;
        }
      }
    });
  }

  // public onChangeSectionGRNDetails(): void {
  //   if (this.objLoad.section_grn_no !== 0) {
  //     for (let i = 0; i < this.getSectionGRNList.length; i++) {
  //         this.objLoad.section_grn_no = this.getSectionGRNList[i].section_grn_no;
  //         this.objLoad.grn_no = this.getSectionGRNList[i].grn_no;
  //         this.objLoad.grn_date = this.getSectionGRNList[i].grn_date;
  //         break;
  //       }
  //     }
  //       else {
  //         this.objLoad.grn_no = 0;
  //         this.objLoad.grn_date = "";
  //       }
  //     }

  public validateBeforeLoad(): boolean {
    if (this.objLoad.sales_location_id === -1) {
      document.getElementById("target").focus();
      this._confirmationDialogComponent.openAlertDialog('Select sales location', 'Goods Transfer in Bales');
      return false;
    } else if (this.objLoad.section_grn_no.toString() === '') {
      document.getElementById("section").focus();
      this._confirmationDialogComponent.openAlertDialog('Select section grn', 'Goods Transfer in Bales');
      return false;
    } else return true;
  }

  public loadGoodsTransferBalesDetails(): void {
    debugger;
    this.matTableConfig([]);
    if (this.validateBeforeLoad()) {
      let objData = {
        LoadGoodsBales: JSON.stringify([{
          wh_section_id: +this._localStorage.getWhSectionId(),
          warehouse_id: +this._localStorage.getGlobalWarehouseId(),
          company_section_id: +this._localStorage.getCompanySectionId(),
          grn_no: +this.objLoad.grn_no,
        }]),
      }
      this._goodsTransferBalesService.loadGoodsTransferBales(objData).subscribe((result: any) => {
        if (JSON.parse(result) !== null) {
          this.getBalesList = JSON.parse(result);
          this.matTableConfig(JSON.parse(result));
          console.log( this.getBalesList,'load')
        } else
          this._confirmationDialogComponent.openAlertDialog('No record found', 'Goods Transfer in Bales');
      });
    }
  }

  public matTableConfig(tableRecords?: any) : any {
    if (tableRecords) {
      this.dataSource = new MatTableDataSource(tableRecords);
      this.dataSource.sort = this.sort;
    }
    else
      this.dataSource = new MatTableDataSource([]);
  }

  public validateBeforeSave(): boolean {
    if (this.objLoad.sales_location_id === -1) {
      document.getElementById("target").focus();
      this._confirmationDialogComponent.openAlertDialog('Select sales location', 'Goods Transfer in Bales');
      return false;
    } else if (this.objLoad.section_grn_no.toString() === '') {
      document.getElementById("section").focus();
      this._confirmationDialogComponent.openAlertDialog('Select section grn', 'Goods Transfer in Bales');
      return false;
    } else return true;
  }

  public saveGoodsTransferBalesDetails(): void {
    debugger;
    if (this.validateBeforeSave()) {
      let tempArray = [];
      for (let i = 0; i < this.getBalesList.length; i++) {
        if(this.getBalesList.length === 1 && this.getBalesList[i].remote_transfer_id === 0){
          this._confirmationDialogComponent.openAlertDialog('No record to save', 'Goods Transfer in Bales');
          return;
        }else {
          tempArray.push({
            warehouse_id: +this.getBalesList[i].source_warehouse_id,
            grn_no: +this.objLoad.grn_no,
            serial_no: i+1,
            company_section_id: +this._localStorage.getCompanySectionId(),
            remote_wh_section_id: +this.getBalesList[i].remote_wh_section_id,
            remote_transfer_id: +this.getBalesList[i].remote_transfer_id,
          });
        }
      }
      let objData = {
        AddGoodsBales: JSON.stringify([{
          company_section_id: +this._localStorage.getCompanySectionId(),
          goods_transfer_details: tempArray,
          entered_by: this._localStorage.intGlobalUserId()
        }]),
      }
      this._goodsTransferBalesService.addGoodsTransferBales(objData).subscribe((result: any) => {
        if (result) {
          this._confirmationDialogComponent.openAlertDialog("All records has been saved", "Goods Transfer in Bales")
          this.resetScreen();
        }
      });
    }
  }

  private resetScreen(): void {
    debugger;
    this.matTableConfig([]);
  }

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Transfer No': this.dataSource.data[i].transfer_id,
          'Transfer Date': this._datePipe.transform(this.dataSource.data[i].transfer_date, 'dd/MM/yyyy'),
          "Source From": this.dataSource.data[i].source_warehouse_name,
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Goods Transfer in Bales",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Goods Transfer in Bales");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.transfer_id);
        tempObj.push(this._datePipe.transform(e.transfer_date, 'dd/MM/yyyy'));
        tempObj.push(e.source_warehouse_name);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Transfer No', 'Transfer Date', 'Source From']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Goods Transfer in Bales' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Goods Transfer in Bales");
  }


}
