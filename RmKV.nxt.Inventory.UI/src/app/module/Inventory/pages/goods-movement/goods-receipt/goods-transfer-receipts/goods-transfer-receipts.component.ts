import { DatePipe, DecimalPipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog, MatDialogRef, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { CommonService } from 'src/app/common/services/common/common.service';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { ExcelService } from 'src/app/common/shared/directives/excel-service';
import { KeyPressEvents } from 'src/app/common/shared/directives/key.press.events';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { EditMode, ForCheckBynoData, Rights, SalesLocation } from 'src/app/module/Inventory/model/common.model';
import { GoodsTransferReceiptsService } from 'src/app/module/Inventory/services/goods-movement/good-receipts/goods-transfer-receipts/goods-transfer-receipts.service';
import { GodownToCounterService } from 'src/app/module/Inventory/services/goods-movement/internal-transfers/godown-to-counter/godown-to-counter.service';
import { InvoiceDetailsService } from 'src/app/module/Inventory/services/purchases/invoice-prod-details/invoice-details.service';
declare var jsPDF: any;

@Component({
  selector: 'app-goods-transfer-receipts',
  templateUrl: './goods-transfer-receipts.component.html',
  styleUrls: ['./goods-transfer-receipts.component.scss'],
  providers: [DatePipe, DecimalPipe]
})
export class GoodsTransferReceiptsComponent implements OnInit {


  componentVisibility: boolean = true;
  displayedColumns = ["Serial_No", "transfer_id", "transfer_date", "ack_date", "From_Section", "To_Section", "Ack_Pcs", "Ack_Qty", "Action"];
  @ViewChild(MatSort, null) sort: MatSort;
  transferNoList: any[] = [];
  Date: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');
  workingLocationList: SalesLocation[] = [];
  warehouseList: any = [];

  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  sectionList: any = [];
  locationList: any = [];
  loadGtoCList: any[] = [];
  date1: any = new Date();
  dataSource: any = new MatTableDataSource([]);
  objLoad: any = {
    section: this._localStorage.getCompanySectionName(),
    from_warehouse: this._localStorage.getwareHouseName(),
    to_location: 0,
    from_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    to_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  objGoodsTransferReceipts: any = {
    sales_location_id: 0,
    gatepass_no: 0,
    receipt_id: 0,
    transfer_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    receipt_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    from_section: this._localStorage.getCompanySectionName(),
    from_warehouse: 0,
    to_location: 0,
    transfer_id: 0,
    byno: '',
    acknowledged_pieces: 0,
    total_pieces: 0,
    remarks: ''
  }

  modifyGoodsTransferReceipts: any = {
    sales_location_id: 0,
    gatepass_no: 0,
    receipt_id: 0,
    transfer_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    receipt_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    from_section: this._localStorage.getCompanySectionName(),
    from_warehouse: 0,
    to_location: 0,
    transfer_id: 0,
    byno: '',
    acknowledged_pieces: 0,
    total_pieces: 0,
    remarks: ''
  }

  unChangedGoodsTransferReceipts: any = {
    sales_location_id: 0,
    gatepass_no: 0,
    receipt_id: 0,
    transfer_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    receipt_date: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    from_section: this._localStorage.getCompanySectionName(),
    from_warehouse: 0,
    to_location: 0,
    transfer_id: 0,
    byno: '',
    acknowledged_pieces: 0,
    total_pieces: 0,
    remarks: ''
  }

  byNoList: any = [{
    byno: '',
    received_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    product_code: '',
    product_name: '',
    uom_descrition: '',
    prod_qty: '',
    prod_pcs: '',
    selling_price: '',
    gst: '',
    received_quantity: 0,
    //tagdata : [{
    counter_id: 0
    // }]
  }];

  unChangedByNoList: any = [{
    byno: '',
    received_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    product_code: '',
    product_name: '',
    uom_descrition: '',
    prod_qty: '',
    prod_pcs: '',
    selling_price: '',
    gst: '',
    received_quantity: 0,
    // tagdata : [{
    counter_id: 0
    // }]
  }];

  modifyByNoList: any = [{
    byno: '',
    received_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    product_code: '',
    product_name: '',
    uom_descrition: '',
    prod_qty: '',
    prod_pcs: '',
    selling_price: '',
    gst: '',
    received_quantity: 0,
    // tagdata : [{
    counter_id: 0
    // }]
  }];

  getAckQtyTotal: any = {
    ack_qty: 0,
  }

  addtempArray: any = [{
    byno: '',
    received_byno: '',
    byno_serial: '',
    byno_prod_serial: '',
    product_code: '',
    product_name: '',
    uom_descrition: '',
    prod_qty: '',
    prod_pcs: '',
    selling_price: '',
    gst: '',
    received_quantity: 0,
    // tagdata : [{
    counter_id: 0
    // }]
  }];

  ForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  objAction: EditMode = {
    isEditing: false,
    isView: false
  };

  unChangedAction: EditMode = {
    isEditing: false,
    isView: false
  };

  rights: Rights = {
    Add: false,
    Update: false,
    Delete: false,
    Approve: false,
    View: false
  };

  Inv_Byno: string = "";
  Byno_Serial: string = "";
  Byno_Prod_Serial: string = "";
  keyUpEvent: any
  counterList: any;
  @ViewChildren('counter') counter: ElementRef | any;
  constructor(
    public _minMaxDate: MinMaxDate,
    public _router: Router,
    private dbService: NgxIndexedDBService,
    public _localStorage: LocalStorage,
    private _datePipe: DatePipe,
    public _commonService: CommonService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _goodsTransferReceiptsService: GoodsTransferReceiptsService,
    public _godownToCounterService: GodownToCounterService,
    public _matDialog: MatDialog,
    public _dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    private _excelService: ExcelService,
    public _decimalPipe: DecimalPipe,
    public _keyPressEvents: KeyPressEvents,
    public _invoiceProdDetails: InvoiceDetailsService
  ) {
    this.objLoad.from_date = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 2)));
  }


  ngOnInit() {
    this.getAllWorkingLocation();
    this.getToLocation();
    this.getRights();
    this.getCounterDetails();
    // this.getTransferNoDetails();
  }
  public getCounterDetails(): void {
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
      }])
    }
    this._invoiceProdDetails.getCounterDetails(objData).subscribe((result: any) => {
      if (result) {
        this.counterList = result;
      }
    });
  }
  public validateFromDate(): void {
    let date = this.dateValidation.validateDate(this.objLoad.from_date, this.fromDate1, this.minDate, this.maxDate);
    this.objLoad.from_date = date[0];
    this.fromDate1 = date[1];
  }

  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.objLoad.to_date, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.objLoad.to_date = date[0];
    this.toDate1 = date[1];
  }

  public validateDate(): void {
    let date = this.dateValidation.validateDate(this.objGoodsTransferReceipts.transfer_date, this.date1, this.minDate, this.maxDate);
    this.objGoodsTransferReceipts.transfer_date = date[0];
    this.date1 = date[1];
  }

  newClick(): void {
    //  if (this.rights.Add) {
    this.componentVisibility = !this.componentVisibility;
    this.resetScreen();
    // }
  }

  private onListClick(): void {
    this.componentVisibility = !this.componentVisibility;
    this.matTableConfig([]);
    this.getAckQtyTotal = {
      ack_qty: 0,
    }
  }

  private getRights(): void {
    var rights = this._localStorage.getMenuRights("/GoodsTransferReceipts");
    for (var x in rights) {
      if (rights[x].Right_Name == "Add" && rights[x].Status == true) {
        this.rights.Add = true;
      }
      if (rights[x].Right_Name == "Update" && rights[x].Status == true) {
        this.rights.Update = true;
      }
      if (rights[x].Right_Name == "Delete" && rights[x].Status == true) {
        this.rights.Delete = true;
      }
      if (rights[x].Right_Name == "Approve" && rights[x].Status == true) {
        this.rights.Approve = true;
      }
      if (rights[x].Right_Name == "View" && rights[x].Status == true) {
        this.rights.View = true;
      }
    }
  }

  public checkSalesLocationIsEmpty(): boolean {
    if (this.objGoodsTransferReceipts.sales_location_id === 0) {
      document.getElementById("target").focus();
      this._confirmationDialogComponent.openAlertDialog('Select sales location', 'Goods Transfer Receipts');
      return false;
    } else return true;
  }

  public checkWarehouseIsEmpty(): boolean {
    if (this.objGoodsTransferReceipts.from_warehouse === 0) {
      document.getElementById("warehouse").focus();
      this._confirmationDialogComponent.openAlertDialog('Select warehouse name', 'Goods Transfer Receipts');
      return false;
    } else return true;
  }

  private getAllWorkingLocation(): void {
    let objAllWorkingLocation = {
      SalesLocation: JSON.stringify([{

      }])
    };
    this._commonService.getAllWorkingLocation(objAllWorkingLocation).subscribe((result: any) => {
      this.workingLocationList = result;
      this.workingLocationList.splice(0, 1);
      this.objGoodsTransferReceipts.sales_location_id = 0;
      for (let i = 0; i < this.workingLocationList.length; i++) {
        if (this._localStorage.getGlobalSalesLocationId() === this.workingLocationList[i].Sales_Location_ID) {
          this.workingLocationList.splice(i, 1);
        }
      }
    });
  }

  private getToLocation(): void {
    debugger;
    let objData = {
      GoodCounter: JSON.stringify([{
        sales_location_id: this._localStorage.getGlobalSalesLocationId()
      }]),
    }
    this._godownToCounterService.getToLocation(objData).subscribe((result: any) => {
      if (result) {
        this.locationList = result;
        this.objGoodsTransferReceipts.to_location = this.locationList[0].warehouse_id;
        this.unChangedGoodsTransferReceipts.to_location = this.locationList[0].warehouse_id;
        this.objLoad.to_location = this.locationList[0].warehouse_id;
      }
    });
  }

  public getWarehouse(): void {
    debugger;
    this.objGoodsTransferReceipts.from_warehouse = 0;
    this.objGoodsTransferReceipts.transfer_id = 0;
    this.objGoodsTransferReceipts.gatepass_no = 0;
    let obj = {
      GetAllWarehouse: JSON.stringify([{
        sales_location_id: +this.objGoodsTransferReceipts.sales_location_id,
        virtual: false
      }])
    }
    this._goodsTransferReceiptsService.getAllWarehouseListDetails(obj).subscribe((result: any) => {
      debugger;
      if (result) {
        this.warehouseList = JSON.parse(result);
        this.objGoodsTransferReceipts.from_warehouse = 0;
        console.log(JSON.parse(result), 'warehouse');
      }
    });
  }

  public getTransferNoDetails(): void {
    debugger;
    let objData = {
      LoadTransferNo: JSON.stringify([{
        wh_section_id: +this._localStorage.getWhSectionId(),
        from_warehouse_id: +this.objGoodsTransferReceipts.from_warehouse,
        company_section_id: +this._localStorage.getCompanySectionId()
        // wh_section_id: 1,
        // from_warehouse_id: 2,
        // company_section_id: 6
      }]),
    }
    this._goodsTransferReceiptsService.loadGoodsReceiptsTransferNo(objData).subscribe((result: any) => {
      if (JSON.parse(result)) {
        this.transferNoList = JSON.parse(result);
        this.objGoodsTransferReceipts.transfer_id = 0;
        this.objGoodsTransferReceipts.gatepass_no = 0;
      }
    });
  }

  public checkGoodsReceiptsTransferNoDetails(): void {
    debugger;
    let obj: any = [{
      transfer_id: +this.objGoodsTransferReceipts.transfer_id,
      gatepass_no: +this.objGoodsTransferReceipts.gatepass_no,
      company_section_id: +this._localStorage.getCompanySectionId(),
    }]
    let CheckTransfer = {
      CheckReceiptsTransferNo: JSON.stringify(obj)
    }
    this._goodsTransferReceiptsService.checkGoodsReceiptsTransferNoDetails(CheckTransfer).subscribe((result: any) => {
      debugger;
      if (JSON.parse(result)) {
        this.onChangeTransferNoDetails();
      }
    });
  }

  public onChangeTransferNoDetails(): void {
    debugger;
    if (this.objGoodsTransferReceipts.transfer_id !== 0) {
      for (let i = 0; i < this.transferNoList.length; i++) {
        this.objGoodsTransferReceipts.transfer_id = this.transferNoList[i].remote_transfer_id;
        this.objGoodsTransferReceipts.gatepass_no = this.transferNoList[i].gatepass_no;
        break;
      }
    }
    else {
      this.objGoodsTransferReceipts.gatepass_no = 0;
    }
  }

  private beforeLoadProductValidate(): boolean {
    if (this.objGoodsTransferReceipts.sales_location_id === 0) {
      document.getElementById("target").focus();
      this._confirmationDialogComponent.openAlertDialog('Select sales location', 'Goods Transfer Receipts');
      return false;
    } else if (this.objGoodsTransferReceipts.from_warehouse === 0) {
      document.getElementById("warehouse").focus();
      this._confirmationDialogComponent.openAlertDialog('Select warehouse name', 'Goods Transfer Receipts');
      return false;
    } else if (this.objGoodsTransferReceipts.transfer_date.toString() === "") {
      document.getElementById("date").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter transfer date', 'Goods Transfer Receipts');
      return false;
    } else if (this.objGoodsTransferReceipts.transfer_date.toString().length !== 10) {
      document.getElementById("date").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter valid transfer date', 'Goods Transfer Receipts');
      this.objGoodsTransferReceipts.transfer_date = "";
      return false;
    } else if (this.objGoodsTransferReceipts.transfer_id === 0) {
      document.getElementById("transferId").focus();
      this._confirmationDialogComponent.openAlertDialog('Select transfer no', 'Goods Transfer Receipts');
      return false;
    } else if (this.objGoodsTransferReceipts.gatepass_no === 0) {
      document.getElementById("grnNo").focus();
      this._confirmationDialogComponent.openAlertDialog('Grn no is must', 'Goods Transfer Receipts');
      return false;
    } else return true;
  }

  public loadGoodsReceiptsProductList(): void {
    debugger;
    this.objGoodsTransferReceipts.total_pieces = '';
    this.objGoodsTransferReceipts.acknowledged_pieces = '';
    if (this.beforeLoadProductValidate()) {
      let objData = {
        LoadProductList: JSON.stringify([{
          gatepass_no: +this.objGoodsTransferReceipts.gatepass_no,
          from_warehouse_id: +this.objGoodsTransferReceipts.from_warehouse,
          // from_warehouse_id: 2,
          company_section_id: +this._localStorage.getCompanySectionId(),
        }]),
      }
      this._goodsTransferReceiptsService.loadGoodsReceiptsProductDetails(objData).subscribe((result: any) => {
        if (JSON.parse(result) !== null) {
          this.byNoList = JSON.parse(result);
          this.objGoodsTransferReceipts.total_pieces = +(this.byNoList.length);
          this.byNoList.tagsdata = JSON.parse(result.tagsdata);
          console.log(this.byNoList, 'byno')
        } else
          this._confirmationDialogComponent.openAlertDialog('No record found', 'Goods Transfer Receipts');
      });
    }
  }

  public onClearByNoDetails(): void {
    this.byNoList = [];
    this.objGoodsTransferReceipts.byno = '';
    this.objGoodsTransferReceipts.acknowledged_pieces = 0;
    this.objGoodsTransferReceipts.total_pieces = 0;
  }

  public setTotalCalculation(tableRecords: any): void {
    debugger;
    this.getAckQtyTotal = {
      ack_qty: 0,
    }
    for (let i = 0; i < tableRecords.length; i++) {
      this.getAckQtyTotal.ack_qty += +(tableRecords[i].transfer_qty);
    }
  }

  public checkInvalidByNoData(): boolean {
    debugger;
    let index = this.byNoList.findIndex(x => x.byno !== this.objGoodsTransferReceipts.byno)
    if (index !== -1)
      return true;
    else
      return false;
  }

  public clearByNoList(): void {
    this.byNoList.length = [];
  }

  public onClearReceiptsList(): void {
    this.matTableConfig([]);
    this.getAckQtyTotal = {
      ack_qty: 0,
    }
  }

  public onClearView(): void {
    this.objAction.isView = true;
    this.componentVisibility = !this.componentVisibility;
  }

  private beforeLoadValidate(): boolean {
    if (this.objLoad.to_location === 0) {
      document.getElementById("toSection").focus();
      this._confirmationDialogComponent.openAlertDialog('Select to location', 'Goods Transfer Receipts');
      return false;
    } else if (this.objLoad.from_date.toString() === "") {
      document.getElementById("fromDate").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter from date', 'Goods Transfer Receipts');
      return false;
    } else if (this.objLoad.from_date.toString().length !== 10) {
      document.getElementById("fromDate").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter valid from date', 'Goods Transfer Receipts');
      this.objLoad.from_date = "";
      return false;
    } else if (this.objLoad.to_date.toString() === "") {
      document.getElementById("toDate").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter to date', 'Goods Transfer Receipts');
      return false;
    } else if (this.objLoad.to_date.toString().length !== 10) {
      document.getElementById("toDate").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter valid to date', 'Goods Transfer Receipts');
      this.objLoad.to_date = "";
      return false;
    } else {
      return true;
    }
  }


  public loadGoodsReceiptsDetails(): void {
    debugger;
    if (this.beforeLoadValidate()) {
      let LoadAcknowledgement = {
        LoadGoodsReceipts: JSON.stringify([{
          from_date: this.objLoad.from_date,
          to_date: this.objLoad.to_date,
          wh_section_id: +this._localStorage.getWhSectionId(),
          source_id: this._localStorage.getGlobalWarehouseId(),
          destination_id: +this.objLoad.to_location,
          company_section_id: +this._localStorage.getCompanySectionId(),
        }])
      }
      this._goodsTransferReceiptsService.loadGoodsTransferReceipts(LoadAcknowledgement).subscribe((result: any) => {
        debugger;
        if (JSON.parse(result) !== null) {
          this.loadGtoCList = JSON.parse(result);
          this.matTableConfig(JSON.parse(result));
          this.setTotalCalculation(JSON.parse(result));
        }
        else {
          this._confirmationDialogComponent.openAlertDialog("No record found", "Goods Transfer Receipts");
        }
      });
    }
  }

  public fetchGoodsReceiptsDetails(index: number): void {
    debugger;
    let obj: any = [{
      transfer_date: this._datePipe.transform(this.dataSource.data[index].transfer_date, 'dd/MM/yyyy'),
      ack_date: this._datePipe.transform(this.dataSource.data[index].ack_date, 'dd/MM/yyyy'),
      transfer_id: this.dataSource.data[index].transfer_id,
      wh_section_id: +this._localStorage.getWhSectionId(),
      source_id: +this._localStorage.getGlobalWarehouseId(),
      company_section_id: +this._localStorage.getCompanySectionId(),
    }]
    let objFetch = {
      FetchGoodsReceipts: JSON.stringify(obj)
    }
    this._goodsTransferReceiptsService.fetchGoodsTransferReceiptsDetails(objFetch).subscribe((result: any) => {
      if (JSON.parse(result)) {
        debugger;
        this.objGoodsTransferReceipts = JSON.parse(JSON.stringify(result[0]));
        this.modifyGoodsTransferReceipts = JSON.parse(JSON.stringify(result[0]));
        this.objAction.isView = true;
        this.componentVisibility = !this.componentVisibility;
      }
    });
  }

  public matTableConfig(tableRecords?: any) {
    if (tableRecords) {
      this.dataSource = new MatTableDataSource(tableRecords);
      this.dataSource.sort = this.sort;
    }
    else
      this.dataSource = new MatTableDataSource([]);
  }

  private beforeSaveValidate(): boolean {
    if (this.objGoodsTransferReceipts.sales_location_id === 0) {
      document.getElementById("target").focus();
      this._confirmationDialogComponent.openAlertDialog('Select sales location', 'Goods Transfer Receipts');
      return false;
    } else if (this.objGoodsTransferReceipts.from_warehouse === 0) {
      document.getElementById("warehouse").focus();
      this._confirmationDialogComponent.openAlertDialog('Select warehouse name', 'Goods Transfer Receipts');
      return false;
    } else if (this.objGoodsTransferReceipts.transfer_date.toString() === "") {
      document.getElementById("date").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter transfer date', 'Goods Transfer Receipts');
      return false;
    } else if (this.objGoodsTransferReceipts.transfer_date.toString().length !== 10) {
      document.getElementById("date").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter valid transfer date', 'Goods Transfer Receipts');
      this.objGoodsTransferReceipts.transfer_date = "";
      return false;
    } else if (this.objGoodsTransferReceipts.transfer_id === 0) {
      document.getElementById("transferId").focus();
      this._confirmationDialogComponent.openAlertDialog('Select transfer no', 'Goods Transfer Receipts');
      return false;
    } else if (this.objGoodsTransferReceipts.gatepass_no === 0) {
      document.getElementById("grnNo").focus();
      this._confirmationDialogComponent.openAlertDialog('Grn no is must', 'Goods Transfer Receipts');
      return false;
    } else if (this.byNoList.length === 0) {
      this._confirmationDialogComponent.openAlertDialog('No record to save', 'Goods Transfer Receipts');
      return false;
    } else if (this.byNoList.length === 1 && this.byNoList[0].byno === '') {
      this._confirmationDialogComponent.openAlertDialog('No record to save', 'Goods Transfer Receipts');
      return false;
    } else if (!this.selectAtleastOneByNoAcknowledge()) {
      document.getElementById("byNo").focus();
      this._confirmationDialogComponent.openAlertDialog("Any one byno must be acknowledged", "Goods Transfer Receipts");
      return false;
    }
    else {
      return true;
    }
  }

  private selectAtleastOneByNoAcknowledge(): boolean {
    debugger;
    let index = this.byNoList.findIndex(x => x.received_byno !== undefined);
    if (index !== -1)
      return true;
    else
      return false;
  }

  public onSaveGoodsReceiptsDetails(): void {
    debugger;
    //if (this.beforeSaveValidate()) {
      let details = [];
      for (let i = 0; i < this.byNoList.length; i++) {
        // for (let j = 0; i < this.byNoList.tagsdata.length; j++) {
        if (this.byNoList[i].counter_id !== 0) {
          if (this.byNoList[i].received_byno !== undefined) {
            details.push({
              serial_no: i + 1,
              inv_byno: this.byNoList[i].inv_byno,
              byno_serial: this.byNoList[i].byno_serial,
              byno_prod_serial: this.byNoList[i].byno_prod_serial,
              received_qty: +this.byNoList[i].prod_qty,
              received_pcs: +this.byNoList[i].prod_pcs,
              received_quantity: +this.byNoList[i].prod_qty,
              counter_id: +this.byNoList[i].counter_id,
            });
          }
        } else {
          let input = this.counter.toArray();
          input[i].nativeElement.focus();
          this._confirmationDialogComponent.openAlertDialog("Select counter", "Goods Transfer Receipts");
          return;
        }
      }
      // }
      let obj = [{
        receipt_date: this.objGoodsTransferReceipts.receipt_date,
        receipt_id: +this.objGoodsTransferReceipts.receipt_id,
        transfer_date: this.objGoodsTransferReceipts.transfer_date,
        transfer_id: +this.objGoodsTransferReceipts.transfer_id,
        wh_section_id: +this._localStorage.getWhSectionId(),
        grn_no: +this.objGoodsTransferReceipts.gatepass_no,
        source_wh_section_id: +this._localStorage.getGlobalWarehouseId(),
        company_section_id: +this._localStorage.getCompanySectionId(),
        remarks: this.objGoodsTransferReceipts.remarks.toString().trim(),
        entered_by: +this._localStorage.intGlobalUserId(),
        details: details,
      }]
      let objSave = {
        AddGoodsReceipts: JSON.stringify(obj)
      }
      this._goodsTransferReceiptsService.addGoodsTransferReceiptsDetails(objSave).subscribe(
        (result: any) => {
          if (result) {
            this._confirmationDialogComponent.openAlertDialog(this.objAction.isEditing ? "Changes have been saved" :
              "New Goods Transfer Receipt has been created", "Goods Transfer Receipts");
            this.componentVisibility = !this.componentVisibility;
            this.loadGoodsReceiptsDetails();
          }
        });
    //}
  }

  private trimStringFields(): void {
    this.objGoodsTransferReceipts.transfer_id = this.objGoodsTransferReceipts.transfer_id.toString().trim();
    this.modifyGoodsTransferReceipts.transfer_id = this.modifyGoodsTransferReceipts.transfer_id.toString().trim();
    this.unChangedGoodsTransferReceipts.transfer_id = this.unChangedGoodsTransferReceipts.transfer_id.toString().trim();

    this.objGoodsTransferReceipts.byno = this.objGoodsTransferReceipts.byno.toString().trim();
    this.modifyGoodsTransferReceipts.byno = this.modifyGoodsTransferReceipts.byno.toString().trim();
    this.unChangedGoodsTransferReceipts.byno = this.unChangedGoodsTransferReceipts.byno.toString().trim();

    this.objGoodsTransferReceipts.remarks = this.objGoodsTransferReceipts.remarks.toString().trim();
    this.modifyGoodsTransferReceipts.remarks = this.modifyGoodsTransferReceipts.remarks.toString().trim();
    this.unChangedGoodsTransferReceipts.remarks = this.unChangedGoodsTransferReceipts.remarks.toString().trim();
  }

  public onClear(exitFlag: boolean): void {
    if (this.checkAnyChangesMade())
      this.openConfirmationDialog(exitFlag ? "Changes will be lost. Are you sure?" : "Do you want to clear all the fields?", exitFlag);
    else if (exitFlag)
      this.onListClick();
  }

  private checkAnyChangesMade(): boolean {
    debugger;
    this.trimStringFields();
    if (JSON.stringify(this.objGoodsTransferReceipts) !==
      (!this.objAction.isEditing ? JSON.stringify(this.unChangedGoodsTransferReceipts) :
        JSON.stringify(this.modifyGoodsTransferReceipts)) || JSON.stringify(this.byNoList) !==
      (!this.objAction.isEditing ? JSON.stringify(this.unChangedByNoList) :
        JSON.stringify(this.modifyByNoList))) {
      return true;
    } else {
      return false;
    }
  }

  private openConfirmationDialog(message: string, exitFlag?: boolean): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = message;
    dialogRef.componentInstance.componentName = "Goods Transfer Receipts";
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        exitFlag ? this.onListClick() : this.resetScreen();
      dialogRef = null;
    });
  }

  private resetScreen(): void {
    debugger;
    if (this.objAction.isEditing === true) {
      this.objGoodsTransferReceipts = JSON.parse(JSON.stringify(this.modifyGoodsTransferReceipts));
      this.modifyGoodsTransferReceipts = JSON.parse(JSON.stringify(this.modifyGoodsTransferReceipts));
      this.byNoList = JSON.parse(JSON.stringify(this.modifyByNoList));
      this.modifyByNoList = JSON.parse(JSON.stringify(this.modifyByNoList));
    } else {
      this.objGoodsTransferReceipts = JSON.parse(JSON.stringify(this.unChangedGoodsTransferReceipts));
      this.objGoodsTransferReceipts.to_location = this.objLoad.to_location;
      this.byNoList = JSON.parse(JSON.stringify(this.unChangedByNoList));
      this.objGoodsTransferReceipts.transfer_date = this._datePipe.transform(new Date, 'dd/MM/yyyy');
      this.date1 = new Date()
    }
  }

  public exportToExcel(): void {
    if (this.dataSource.data.length != 0) {
      const datetime = this._datePipe.transform(new Date(), "dd-MM-yyyy hh:mm:a");
      const customerArr = [];
      for (let i = 0; i < this.dataSource.data.length; i++) {
        customerArr[i] = Object.assign({
          'Transfer No': this.dataSource.data[i].transfer_id,
          'Transfer Date': this._datePipe.transform(this.dataSource.data[i].transfer_date, 'dd/MM/yyyy'),
          "Ack Date": this.dataSource.data[i].ack_date,
          "From Section": this.dataSource.data[i].source_name,
          "To Location": this.dataSource.data[i].destination_name,
          "Ack Pcs": this.dataSource.data[i].transfer_pcs,
          "Ack Qty": this.dataSource.data[i].transfer_qty
        });
      }
      this._excelService.exportAsExcelFile(
        customerArr,
        "Goods Transfer Receipts",
        datetime
      );
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Goods Transfer Receipts");
  }

  public exportToPdf(): void {
    if (this.dataSource.data.length != 0) {
      var prepare = [];
      this.dataSource.data.forEach(e => {
        var tempObj = [];
        tempObj.push(e.transfer_id);
        tempObj.push(this._datePipe.transform(e.transfer_date, 'dd/MM/yyyy'));
        tempObj.push(this._datePipe.transform(e.ack_date, 'dd/MM/yyyy'));
        tempObj.push(e.source_name);
        tempObj.push(e.destination_name);
        tempObj.push(e.transfer_pcs);
        tempObj.push(e.transfer_qty);
        prepare.push(tempObj);
      });
      const doc = new jsPDF();
      doc.autoTable({
        head: [['Transfer No', 'Transfer Date', 'Ack Date', 'From Section', 'To Location', 'Ack Pcs', 'Ack Qty']],
        body: prepare,
        didParseCell: function (table) {
          if (table.section === 'head') {
            table.cell.styles.textColor = '#FFFFFF';
            table.cell.styles.fillColor = '#d32f2f';
          }
        }
      });
      doc.save('Goods Transfer Receipts' + '.pdf');
    } else
      this._confirmationDialogComponent.openAlertDialog("No record found, Load the data first", "Goods Transfer Receipts");
  }

  public openReceivedByNo(data: any): any {
    debugger;
    for (let i = 0; i < this.byNoList.length; i++) {
      if (this.byNoList[i].inv_byno === data.Inv_Byno &&
        this.byNoList[i].byno_serial === data.Byno_Serial &&
        this.byNoList[i].byno_prod_serial === data.Byno_Prod_Serial) {
        this.objGoodsTransferReceipts.byno = this.byNoList[i].byno;
        if (this.byNoList[i].received_byno === this.objGoodsTransferReceipts.byno) {
          document.getElementById('byNo').focus();
          this._confirmationDialogComponent.openAlertDialog('Already Acknowledged', 'Goods Transfer Receipts');
          this.objGoodsTransferReceipts.byno = '';
          return false;
        } else if (this.byNoList[i].inv_byno === data.Inv_Byno &&
          this.byNoList[i].byno_serial === data.Byno_Serial &&
          this.byNoList[i].byno_prod_serial === data.Byno_Prod_Serial) {
          this.byNoList[i].received_byno = this.byNoList[i].byno;
          this.objGoodsTransferReceipts.byno = this.byNoList[i].byno;
          if (this.byNoList[i].received_byno === this.objGoodsTransferReceipts.byno) {
            this.objGoodsTransferReceipts.acknowledged_pieces = this.objGoodsTransferReceipts.acknowledged_pieces + 1;
            this.objGoodsTransferReceipts.byno = '';
          }
        }
      } else {
        let index = this.byNoList.findIndex(x => x.inv_byno === data.Inv_Byno && x.byno_serial === data.Byno_Serial && x.byno_prod_serial === data.Byno_Prod_Serial);
        if (index == -1) {
          document.getElementById('byNo').focus();
          this._confirmationDialogComponent.openAlertDialog('Invalid Byno', 'Goods Transfer Receipts');
          this.objGoodsTransferReceipts.byno = '';
          return;
        }

      }
    }
  }

  public checkValidScan(): boolean {
    debugger;
    let index = this.byNoList.findIndex(x => x.byno === this.objGoodsTransferReceipts.byno);
    if (index !== -1)
      return true;
    else
      return false;
  }

  public onKeyPress(keyUpEvent: any) {
    debugger
    if (keyUpEvent.keyCode === 13 && this.objGoodsTransferReceipts.byno === '') {
      document.getElementById("byNo").focus();
      this._confirmationDialogComponent.openAlertDialog('Enter byno', 'Goods Transfer Receipts');
    }
    else if (keyUpEvent.keyCode === 13 &&
      this.objGoodsTransferReceipts.byno.length == 16) {
      this.byNoWithoutSlash();
    }
    else if (keyUpEvent.keyCode === 13 &&
      this.objGoodsTransferReceipts.byno.length == 28) {
      this.OldByNoWithoutSlash();
    }
    else if (keyUpEvent.keyCode === 13 &&
      this.objGoodsTransferReceipts.byno.length == 30) {
      this.byNoWithoutSlash();
    }
    else if (
      keyUpEvent.keyCode === 13 &&
      this.objGoodsTransferReceipts.byno.includes("/")
    ) {
      debugger
      this.splitByNumber();
    } else if (
      this.objGoodsTransferReceipts.byno.length >= 28 &&
      keyUpEvent.keyCode === 13
    ) {
      this.autoScannerCall();
    }
    else {
      if (
        keyUpEvent.keyCode === 13 &&
        !this.objGoodsTransferReceipts.byno.includes("/") &&
        this.objGoodsTransferReceipts.byno.length > 16
      ) {
        this._confirmationDialogComponent.openAlertDialog(
          "Invalid Byno",
          "Goods Transfer Receipts"
        );
      } else if (keyUpEvent.keyCode === 13 && !this.checkValid()) {
        this._confirmationDialogComponent.openAlertDialog("Invalid Byno", "Goods Transfer Receipts");
        this.objGoodsTransferReceipts.byno = '';
      }
    }
  }

  private checkValid(): boolean {
    debugger;
    let index = this.byNoList.findIndex(x => x.byno === this.objGoodsTransferReceipts.byno);
    if (index !== -1)
      return true;
    else
      return false;
  }

  private byNoWithoutSlash() {
    this.objGoodsTransferReceipts.byno = this.objGoodsTransferReceipts.byno.toString().trim();
    this.ForCheckBynoData.Inv_Byno = this.objGoodsTransferReceipts.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objGoodsTransferReceipts.byno.substr(9, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objGoodsTransferReceipts.byno.substr(13, 3);
    this.openReceivedByNo(this.ForCheckBynoData);
  }

  private OldByNoWithoutSlash() {
    this.objGoodsTransferReceipts.byno = this.objGoodsTransferReceipts.byno.toString().trim();
    this.ForCheckBynoData.Inv_Byno = this.objGoodsTransferReceipts.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objGoodsTransferReceipts.byno.substr(8, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objGoodsTransferReceipts.byno.substr(11, 3);
    console.log(this.ForCheckBynoData)
    this.openReceivedByNo(this.ForCheckBynoData);
  }

  private autoScannerCall() {
    this.ForCheckBynoData.Inv_Byno = this.objGoodsTransferReceipts.byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = this.objGoodsTransferReceipts.byno.substr(8, 4);
    this.ForCheckBynoData.Byno_Prod_Serial = this.objGoodsTransferReceipts.byno.substr(11, 4);
    this.openReceivedByNo(this.ForCheckBynoData);
  }

  private splitByNumber() {
    this.objGoodsTransferReceipts.byno = this.objGoodsTransferReceipts.byno.toString().trim();
    var x = this.objGoodsTransferReceipts.byno.split("/");
    this.Inv_Byno = x[0];
    this.Byno_Serial = x[1];
    this.Byno_Prod_Serial = x[2];
    if (this.Byno_Serial.length == 2) {
      this.Byno_Serial = "00".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 2) {
      this.Byno_Prod_Serial = "00".concat(this.Byno_Prod_Serial);
    }
    if (this.Byno_Serial.length == 1) {
      this.Byno_Serial = "000".concat(this.Byno_Serial);
    }
    if (this.Byno_Prod_Serial.length == 1) {
      this.Byno_Prod_Serial = "000".concat(this.Byno_Prod_Serial);
    }
    this.ForCheckBynoData.Inv_Byno = this.Inv_Byno.toString().trim();
    this.ForCheckBynoData.Byno_Serial = this.Byno_Serial.toString().trim();
    this.ForCheckBynoData.Byno_Prod_Serial = this.Byno_Prod_Serial.toString().trim();
    this.openReceivedByNo(this.ForCheckBynoData);
  }


}
