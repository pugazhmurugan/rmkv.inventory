import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesReturnOtherLocationComponent } from './sales-return-other-location.component';

describe('SalesReturnOtherLocationComponent', () => {
  let component: SalesReturnOtherLocationComponent;
  let fixture: ComponentFixture<SalesReturnOtherLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesReturnOtherLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesReturnOtherLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
