import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SrtReportComponent } from './srt-report.component';

describe('SrtReportComponent', () => {
  let component: SrtReportComponent;
  let fixture: ComponentFixture<SrtReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SrtReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SrtReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
