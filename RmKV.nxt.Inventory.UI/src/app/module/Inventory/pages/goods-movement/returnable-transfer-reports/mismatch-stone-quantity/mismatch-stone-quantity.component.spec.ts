import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MismatchStoneQuantityComponent } from './mismatch-stone-quantity.component';

describe('MismatchStoneQuantityComponent', () => {
  let component: MismatchStoneQuantityComponent;
  let fixture: ComponentFixture<MismatchStoneQuantityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MismatchStoneQuantityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MismatchStoneQuantityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
