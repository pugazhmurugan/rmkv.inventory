import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingProductionIssueListComponent } from './pending-production-issue-list.component';

describe('PendingProductionIssueListComponent', () => {
  let component: PendingProductionIssueListComponent;
  let fixture: ComponentFixture<PendingProductionIssueListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingProductionIssueListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingProductionIssueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
