import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionReceiptListComponent } from './production-receipt-list.component';

describe('ProductionReceiptListComponent', () => {
  let component: ProductionReceiptListComponent;
  let fixture: ComponentFixture<ProductionReceiptListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionReceiptListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionReceiptListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
