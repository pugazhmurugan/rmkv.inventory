import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';

import { SaveWorksOrderIssue, WorksOrderLoad } from 'src/app/module/Inventory/model/goodsMovement/returnableGoodsTransfer/work-order-issue.model';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { ReturnableGoodsTransferReportsService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer-reports/returnable-goods-transfer-reports.service';


@Component({
  selector: 'app-work-order-receipt-list',
  templateUrl: './work-order-receipt-list.component.html',
  styleUrls: ['./work-order-receipt-list.component.scss'],
  providers: [DatePipe]
})
export class WorkOrderReceiptListComponent implements OnInit {

  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  objWorkOrderIssue: SaveWorksOrderIssue = {
    wo_issue_date: new Date(),
    supplier_code: "",
    supplier_name: "",
    transfer_type: "",
    remarks: "",
    wo_issue_id: 0,
  }

  objModifyWorkOrderIssue: SaveWorksOrderIssue = {
    wo_issue_date: new Date(),
    supplier_code: "",
    supplier_name: "",
    transfer_type: "",
    remarks: "",
    wo_issue_id: 0,
  }

  UnchangedWorkOrderIssue: SaveWorksOrderIssue = {
    wo_issue_date: new Date(),
    supplier_code: "",
    supplier_name: "",
    transfer_type: "",
    remarks: "",
    wo_issue_id: 0,
  }

  newSupplierLookupList: any = [];
  newSupplierLookupLists: any = [];
  focusFlag: boolean = false;
  allsuppliers: boolean = false;

  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  constructor(public _minMaxDate: MinMaxDate,public _router: Router,
    public _localStorage: LocalStorage, 
    private _confirmationDialog: ConfirmationDialogComponent,
    private _workorderreceiptlistservice: ReturnableGoodsTransferReportsService,
    private _matDialog: MatDialog, private _datePipe: DatePipe,) { }

  ngOnInit() {
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }
  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }



  public validateSupplierCode(): boolean {
    let index = this.newSupplierLookupLists.findIndex(element => element.account_code === this.objWorkOrderIssue.supplier_code.toString().trim());
    if (index === -1)
      return false;
    else return true;
  }


  public openNewSupplierLookups(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierCode()) {
      this.openNewSupplierLookupDialogs();
    }
  }

  private openNewSupplierLookupDialogs(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "650px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.objWorkOrderIssue.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      if (result) {
        debugger;
        this.objWorkOrderIssue.supplier_code = result.account_code.toString().trim();
        this.objWorkOrderIssue.supplier_name = result.account_name.toString().trim();

      } else {
        this.objWorkOrderIssue.supplier_code = '';
        this.objWorkOrderIssue.supplier_name = '';
      }
    });
  }

  public checkValidSupplierName(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.objWorkOrderIssue.supplier_code.toString().trim() !== '' && this.objWorkOrderIssue.supplier_name.toString().trim() === '') {
      this.openAlertDialog("Invalid supplier code", "WorkOrder Issue", focus);
    }
  }

  public onEnterNewSupplierLookupDetail(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: "",
      email: "",
      address1: "",
      address2: "",
      address3: "",
      gstn_no: ""
    };

    supplierLookupList = this.newSupplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.objWorkOrderIssue.supplier_code.toString().trim().toLowerCase());
    this.objWorkOrderIssue.supplier_code = supplierLookupList && supplierLookupList.account_code ? supplierLookupList.account_code : this.objWorkOrderIssue.supplier_code.toString().trim();
    this.objWorkOrderIssue.supplier_name = supplierLookupList && supplierLookupList.account_name ? supplierLookupList.account_name : '';

  }
  
  openAlertDialog(value: string, componentName?: string, focus?: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        document.getElementById(focus).focus();
        _dialogRef = null;
      }
      _dialogRef = null;
    });
  } 

  public onChangeOfAllSuppliers(): void {
    this.objWorkOrderIssue.wo_issue_id = 0;
    this.objWorkOrderIssue.supplier_code = '';
    this.objWorkOrderIssue.supplier_name = '';
    this.objWorkOrderIssue.transfer_type = '';
    this.objWorkOrderIssue.remarks = '';
    this.objWorkOrderIssue.wo_issue_date =new Date();
  }


  public generateReport(): void {
    if (this.beforeGenerateReportValidate()) {
      let objReceiptList = {
        WorkOrderIssueReportView: JSON.stringify([{
          from_date: this.load.FromDate,
          to_date: this.load.ToDate,
          supplier_code : this.objWorkOrderIssue.supplier_code,
          supplier_name : this.objWorkOrderIssue.supplier_name
      }])
      }
      this._workorderreceiptlistservice.getWorkOrderReceiptListReport(objReceiptList).subscribe((result: any) => {
        if (result.size != 0) {
          let dataType = result.type;
          const file = new Blob([result], { type: dataType });
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL);
        } else
          this._confirmationDialog.openAlertDialog('No records found', 'Work Order Issue List')
      });
    }
  }

  private beforeGenerateReportValidate(): boolean {
    if (this.allsuppliers === false && this.objWorkOrderIssue.supplier_code === '') {
      document.getElementById('supplierCode').focus();
      this._confirmationDialog.openAlertDialog('Enter supplier code', 'Work Order Issue List');
      return false;
    } if (this.allsuppliers === false && this.objWorkOrderIssue.supplier_name === '') {
      document.getElementById('supplierCode').focus();
      this._confirmationDialog.openAlertDialog('Invalid supplier code', 'Work Order Issue List');
      this.onChangeOfAllSuppliers();
      return false;
    } else return true;
  }


}
