import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolishPendingIssueReportComponent } from './polish-pending-issue-report.component';

describe('PolishPendingIssueReportComponent', () => {
  let component: PolishPendingIssueReportComponent;
  let fixture: ComponentFixture<PolishPendingIssueReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolishPendingIssueReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolishPendingIssueReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
