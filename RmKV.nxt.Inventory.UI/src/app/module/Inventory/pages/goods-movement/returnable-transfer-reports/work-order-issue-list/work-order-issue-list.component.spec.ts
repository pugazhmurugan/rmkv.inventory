import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkOrderIssueListComponent } from './work-order-issue-list.component';

describe('WorkOrderIssueListComponent', () => {
  let component: WorkOrderIssueListComponent;
  let fixture: ComponentFixture<WorkOrderIssueListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkOrderIssueListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkOrderIssueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
