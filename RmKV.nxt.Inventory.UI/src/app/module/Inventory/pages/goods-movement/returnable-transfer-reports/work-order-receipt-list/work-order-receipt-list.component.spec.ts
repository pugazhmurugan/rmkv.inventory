import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkOrderReceiptListComponent } from './work-order-receipt-list.component';

describe('WorkOrderReceiptListComponent', () => {
  let component: WorkOrderReceiptListComponent;
  let fixture: ComponentFixture<WorkOrderReceiptListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkOrderReceiptListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkOrderReceiptListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
