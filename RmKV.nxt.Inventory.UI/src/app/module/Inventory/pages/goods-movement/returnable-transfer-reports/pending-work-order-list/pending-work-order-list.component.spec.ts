import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingWorkOrderListComponent } from './pending-work-order-list.component';

describe('PendingWorkOrderListComponent', () => {
  let component: PendingWorkOrderListComponent;
  let fixture: ComponentFixture<PendingWorkOrderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingWorkOrderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingWorkOrderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
