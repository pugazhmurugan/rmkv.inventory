import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-pending-work-order-issue',
  templateUrl: './pending-work-order-issue.component.html',
  styleUrls: ['./pending-work-order-issue.component.scss']
})
export class PendingWorkOrderIssueComponent implements OnInit {

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
