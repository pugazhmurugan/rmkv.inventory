import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/common/shared/local-storage';

@Component({
  selector: 'app-pending-work-order-list',
  templateUrl: './pending-work-order-list.component.html',
  styleUrls: ['./pending-work-order-list.component.scss']
})
export class PendingWorkOrderListComponent implements OnInit {

  constructor(public _router: Router,
    public _localStorage: LocalStorage,) { }

  ngOnInit() {
  }

}
