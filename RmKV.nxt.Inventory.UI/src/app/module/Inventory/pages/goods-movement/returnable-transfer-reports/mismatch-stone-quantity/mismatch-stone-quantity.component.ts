import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { ReturnableGoodsTransferReportsService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer-reports/returnable-goods-transfer-reports.service';

@Component({
  selector: 'app-mismatch-stone-quantity',
  templateUrl: './mismatch-stone-quantity.component.html',
  styleUrls: ['./mismatch-stone-quantity.component.scss'],
  providers: [DatePipe]
})
export class MismatchStoneQuantityComponent implements OnInit {
  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy')
  }

  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  constructor(public _minMaxDate: MinMaxDate, public _router: Router,
    public _mismatchstonequantityreportservice: ReturnableGoodsTransferReportsService,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    public _localStorage: LocalStorage, private _datePipe: DatePipe,) { }

  ngOnInit() {
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }
  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  }


  public generateReport(): void {
    debugger;
    let objForm = {
      SingleProduct: JSON.stringify([{
        from_date: this.load.FromDate,
        to_date: this.load.ToDate,
        company_section_id: +this._localStorage.getCompanySectionId(),
        // warehouse_id: +this._localStorage.getGlobalWarehouseId(),

        // wh_section_id: this.sectionsList.filter(x => +x.company_section_id == +this._localStorage.getCompanySectionId())[0].wh_section_id,

      }])
    }
    this._mismatchstonequantityreportservice.getMismatchStoneQuantityReport(objForm).subscribe((result: any) => {
      if (result.size != 0) {
        let dataType = result.type;
        const file = new Blob([result], { type: dataType });
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      } else
        this._confirmationDialogComponent.openAlertDialog('No records found', 'Price Change History')
    });
  }


}
