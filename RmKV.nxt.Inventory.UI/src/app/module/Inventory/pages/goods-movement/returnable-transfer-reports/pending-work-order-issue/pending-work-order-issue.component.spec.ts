import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingWorkOrderIssueComponent } from './pending-work-order-issue.component';

describe('PendingWorkOrderIssueComponent', () => {
  let component: PendingWorkOrderIssueComponent;
  let fixture: ComponentFixture<PendingWorkOrderIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingWorkOrderIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingWorkOrderIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
