import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AccountsLookupService } from 'src/app/common/services/accounts-lookup/accounts-lookup.service';
import { AccountsLookupComponent } from 'src/app/common/shared/accounts-lookup/accounts-lookup.component';
import { ConfirmationDialogComponent } from 'src/app/common/shared/confirmation-dialog/confirmation-dialog.component';
import { DateValidation } from 'src/app/common/shared/Datepicker/DatePickerValidation';
import { MinMaxDate } from 'src/app/common/shared/directives/max-min-date';
import { LocalStorage } from 'src/app/common/shared/local-storage';
import { EwaybillService } from 'src/app/module/Inventory/services/ewaybill/ewaybill.service';
import { ReturnableGoodsTransferReportsService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer-reports/returnable-goods-transfer-reports.service';
import { HsnMasterService } from 'src/app/module/Inventory/services/masters/hsn-master/hsn-master.service';

@Component({
  selector: 'app-polish-pending-issue-report',
  templateUrl: './polish-pending-issue-report.component.html',
  styleUrls: ['./polish-pending-issue-report.component.scss'],
  providers: [DatePipe]
})
export class PolishPendingIssueReportComponent implements OnInit {

  load: any = {
    FromDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    ToDate: this._datePipe.transform(new Date(), 'dd/MM/yyyy'),
    supplier_code: '',
    supplier_name: '',
    allSupplierName: false,
    issue_type: 'Stone',
  }
  values = [];

  supplierLookupList: any = [];
  focusFlag: boolean = false;
  fromDate1: any = new Date();
  toDate1: any = new Date();
  receiptDate1: any = new Date();
  public dateValidation: DateValidation = new DateValidation();

  minimumDate = new Date(+this._localStorage.getMinDate().split("/")[2], +this._localStorage.getMinDate().split("/")[1], +this._localStorage.getMinDate().split("/")[0]);
  maximumDate = new Date();
  minDate: string = this._localStorage.getMinDate();
  maxDate: string = this._datePipe.transform(new Date(), 'dd/MM/yyyy');

  constructor(public _minMaxDate: MinMaxDate,public _router: Router,private _services : EwaybillService,
    public _localStorage: LocalStorage, private _datePipe: DatePipe,
    private _confirmationDialogComponent : ConfirmationDialogComponent,
    private _accountsLookupService: AccountsLookupService,
    public _matDialog: MatDialog, private _returnableGoodsTransferReportService:ReturnableGoodsTransferReportsService) { }

  ngOnInit() {
    this.getSupplierLookupList();
    this.load.FromDate = this._datePipe.transform(new Date().setDate((new Date().getDate() - 2)), 'dd/MM/yyyy');
    this.fromDate1 = new Date(new Date().setDate((new Date().getDate() - 2)));
  }

  public validateFromDate(): void {
    debugger;
    let date = this.dateValidation.validateDate(this.load.FromDate, this.fromDate1, this.minDate, this.maxDate);
    this.load.FromDate = date[0];
    this.fromDate1 = date[1];
  }
  public validateToDate(): void {
    let date = this.dateValidation.validateDate(this.load.ToDate, this.toDate1, this._datePipe.transform(this.fromDate1, 'dd/MM/yyyy'), this.maxDate);
    this.load.ToDate = date[0];
    this.toDate1 = date[1];
  } 

  private getSupplierLookupList(): void {
    let objAccounts = {
      GetLookup: JSON.stringify([{
        field_value: 1
      }])
    }
    this._accountsLookupService.getAccountsLookup(objAccounts).subscribe((result: any) => {
      debugger;
      if (result) {
        this.supplierLookupList = JSON.parse(result);
        for (let i = 0; i < this.supplierLookupList.length; i++) {
          this.supplierLookupList.account_code = this.supplierLookupList.account_code.toString().trim();
          this.supplierLookupList.account_name = this.supplierLookupList.account_name.toString().trim();
        }
      }
    });
  }


  public openSupplierLookup(event: KeyboardEvent | any): void {
    debugger;
    if (event.keyCode === 13 && !this.validateSupplierName()) {
      this.openSupplierLookupDialog();
    }
  }

  private openSupplierLookupDialog(): void {
    this.focusFlag = true;
    const dialogRef = this._matDialog.open(AccountsLookupComponent, {
      width: "550px",
      panelClass: "custom-dialog-container",
      data: {
        searchString: this.load.supplier_code,
        For: 1
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      this.focusFlag = false;
      debugger;
      if (result) {
        this.load.supplier_code = result.account_code.toString().trim();
        this.load.supplier_name = result.account_name.toString().trim();
      } else {
        this.load.supplier_code = "";
        this.load.supplier_name = "";
      }
    });
  }

  public onEnterSupplierLookupDetails(): void {
    debugger;
    let supplierLookupList: any = {
      account_code: "",
      account_name: ""
    };

    supplierLookupList = this.supplierLookupList.find(act => act.account_code.toLowerCase().trim() === this.load.supplier_code.toString().trim().toLowerCase());
    this.load.supplier_code = supplierLookupList && supplierLookupList.account_code.toString().trim() ? supplierLookupList.account_code.toString().trim() : this.load.supplier_code.toString().trim();
    this.load.supplier_name = supplierLookupList && supplierLookupList.account_name.toString().trim() ? supplierLookupList.account_name.toString().trim() : '';
  }
  public checkValidSupplierName(event: KeyboardEvent, focus: string): any {
    if (event.keyCode !== 13 && !this.focusFlag && this.load.supplier_code !== '' && this.load.supplier_name === '') {
      this.openAlertDialog('Invalid supplier code', 'Pending Production Issue', focus);
    }
  }

  public validateSupplierName(): boolean {
    let index = this.supplierLookupList.findIndex(element => element.account_code.toString().trim() === this.load.supplier_code);
    if (index === -1)
      return false;
    else return true;
  }

  private openAlertDialog(value: string, componentName: string, focus: string) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        document.getElementById(focus).focus();
      _dialogRef = null;
    });
  }

  // public checkValidSupplierName(event: KeyboardEvent,i: number, focus: string): any {
  //   if (event.keyCode !== 13 && !this.focusFlag && this.getProductionMaterialList[i].material_name.toString().trim() !== ''
  //     && this.getProductionMaterialList[i].material_id === 0) {
  //     this.openAlertDialog("Invalid material name", "Production Material Requirements", focus,i);
  //   }
  // }

  public clearSupplierDetails() : void {
    if(this.load.allSupplierName === true){
      this.load.supplier_name = '';
      this.load.supplier_code = '';
    }
  }

  public getPolishPendingIssueReport() {
    if(this.beforeViewValidate()){
    let objLoad = [{
      from_date: this.load.FromDate,
      to_date: this.load.ToDate,
      company_section_id: +this._localStorage.getCompanySectionId(),
      wh_section_id: +this._localStorage.getWhSectionId(),
      supplier_code: this.load.supplier_code.toString().trim(),
      issue_type: this.load.issue_type,
    }];
    let objData = {
      PolishPendingIssueReport: JSON.stringify(objLoad)
    };
    this._returnableGoodsTransferReportService.getPolishPendingIssueReport(objData).subscribe((result: any) => {
      if (result.size != 0) {
        let dataType = result.type;
        console.log(result);
        const file = new Blob([result], { type: dataType });
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      } else
        this._confirmationDialogComponent.openAlertDialog('No record found', 'Polish Pending Issue');
    });
  }
  }

  private beforeViewValidate(): boolean {
    debugger;
    if (this.load.supplier_code === '' && this.load.allSupplierName === false) {
      document.getElementById('supplierCode').focus();
      this._confirmationDialogComponent.openAlertDialog('Enter supplier code', 'Polish Pending Issue');
      return false;
    } else return true;
  }

}
