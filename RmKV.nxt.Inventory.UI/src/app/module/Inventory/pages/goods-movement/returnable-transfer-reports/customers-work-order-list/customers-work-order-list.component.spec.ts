import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersWorkOrderListComponent } from './customers-work-order-list.component';

describe('CustomersWorkOrderListComponent', () => {
  let component: CustomersWorkOrderListComponent;
  let fixture: ComponentFixture<CustomersWorkOrderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersWorkOrderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersWorkOrderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
