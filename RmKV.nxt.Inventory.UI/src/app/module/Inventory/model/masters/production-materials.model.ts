export class ProdMaterials {
    material_id: number; 
    material_name: string;
    material_uom: number;
    active: boolean;
}