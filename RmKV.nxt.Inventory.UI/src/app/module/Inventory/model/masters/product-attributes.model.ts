export class ProductAttributes {
    Group_Section_Id?:number[];
    Attribute_Id: number;
    Attribute_Name: string;
    Active?: boolean;
}

export class ProductAttributeValues {
    Attribute_Value_Id: number;
    Attribute_Value: string;
    Active?: boolean;
}
