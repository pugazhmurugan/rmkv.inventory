export class ProductGroup {
    product_group_id: number;
    product_group_name: string;
    product_group_desc: string;
    product_group_uom: number;
    unique_byno_prod_serial: boolean;
    // lsp: boolean;
    group_section_id: number[];
    category_id: number;
    entered_by: number;
}