export class ProductCategory {
    Category_Id:number;
    Group_Section_Name: string;
    Category_Name:string;
    Group_Section_Id: number;
    Active:boolean;
}

export class ProductCategoryList {
    Category_Id:number;
    Category_Name:string;
    Active:boolean;
}