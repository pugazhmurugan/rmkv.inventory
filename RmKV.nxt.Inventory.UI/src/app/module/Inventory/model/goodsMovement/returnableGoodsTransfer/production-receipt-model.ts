export class ProductionReceiptLoad {
    from_date: Date | string;
    to_date: Date | string;
    status: string;
    supplier_code: string;
    supplier_name: string;
}

export class ProductionReceipts {
    production_receipt_no: number;
    inv_supplier_code: string;
    inv_supplier_name: string;
    inv_supplier_gstn_no: string;
    invoice_date: Date | string;
    production_receipt_date: Date | string;
    invoice_no: string;
    taxable_value: number | string;
    other_amount: number | string;
    additions: number | string;
    deductions: number | string;
    cgst_total: number | string;
    sgst_total: number | string;
    igst_total: number | string;
    round_off_total: number | string;
    invoice_amount: number | string;
    remarks: string;
}

export class ProductionReceiptDetails {
    issue_no: number | string;
    product_id: number;
    product_name: string;
    product_size: string;
    estimated_qty: number;
    received_so_far: number;
    received_qty: number;
    conversion_charges: number | string;
    hsn_code: string;
    cgst: number | string;
    cgst_amount: number | string;
    sgst: number | string;
    sgst_amount: number | string;
    igst: number | string;
    igst_amount: number | string;
    total_charges: number | string;
    new_inv_byno: string;
    new_inv_byno_serial: string;
    new_inv_byno_prod_serial: string;
    production_products: any[];
}

export class ProductionMaterials {
    material_id: number;
    material_name: string;
    material_qty: number;
}

export class ProductionReceiptFlags {
    isIgst: boolean;
    isComposite: boolean;
}