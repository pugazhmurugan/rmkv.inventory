export class WorksOrderLoad {
    fromDate: Date | string;
    toDate: Date | string;
    supplier_code: string;
    supplier_name: string;
    transfer_type: string;
}

export class SaveWorksOrderIssue {
    wo_issue_date: Date | string;
    supplier_code: string;
    supplier_name: string;
    transfer_type: string;
    remarks: string;
    wo_issue_id: number;
}
export class WorkOrderRecepit {
    byno: string;
    cgst: number | string;
    charges: number | string;
    checked: Boolean;
    description: string;
    hsn_code: number | string;
    igst: number | string;
    image_path: string
    saree_value: string;
    serial_no: number;
    sgst: number | string;
    stone_qty: number;
    stone_type: string;
    transfer_type: string;
    wh_section_id: number;
    wo_issue_date: string;
    wo_issue_id: number;
    work_order_no: number;
    work_order_serial_no: number;
    work_order_transfer_type: string;
    work_order_wh_section_id: number;
    customer_charges: number | string;
    sgst_amount?: number | string;
    cgst_amount?: number | string;
    igst_amount?: number | string;
}