export class PolishReceipt {
    inv_no : string;
    receipt_no : number;
    polish_order_date: string | Date;
    inv_supplier_code: string;
    inv_supplier_name: string;
    inv_supplier_gstn_no: string;
    invoice_no: string;
    invoice_date: string | Date;
    // invoice_amount: string | number;
    issue_type: string;
    taxable_amount :  string | number;
    other_amount :  string | number;
    sgst_amount :  string | number;
    sgst :  string | number;
    sgst_total : string | number
    igst_amount :  string | number;
    igst_total : string | number
    igst :  string | number;
    cgst_amount :  string | number;
    cgst :  string | number;
    cgst_total : string | number
    round_off :  string | number;
    total_invoice_amount :  string | number;
    additions : number | string;
    deductions : number | string;
    remarks : string;
    file_path1 : string | any;
    file_path2 : string | any;
    file_path3 : string | any;
    file_path4 : string | any;
}

export class PolishReceiptGridList {
    wh_section_id?: number;
    issue_no: number | string;
    byno: string;
    byno_prod_serial: number | string;
    byno_serial: number | string;
    charges: number | string;
    checked: boolean;
    description: string;
    image_path: string;
    inv_byno: string;
    issue_date: Date | string;
    issued_qty: number | string;
    returned: boolean;
    serial_no: number;
    stone_qty: number;
    stone_type: string;
    transfer_type: string;
    conversion_charges : number | string;
    hsn_code : string;
    igst : number | string;
    igst_amount : number | string;
    cgst : number | string;
    cgst_amount : number | string;
    sgst : number | string;
    sgst_amount : number | string;
    total_charges : number | string;
}