export class InvoicesLoad {
    company_section_name: string;
    from_inv_entry_date: Date | string;
    to_inv_entry_date: Date | string;
    status: string | number;
    supplier_code: string;
    supplier_name: string;
    all_suppliers: boolean;
    inv_byno: string;
    is_mismatched_invoices: boolean;
}

export class Invoices {
    inv_byno: string;
    supplier_code: string;
    supplier_name: string;
    supplier_address: string;
    supplier_gstn_no: string;
    supplier_email: string;
    entered_gstn_no: string;
    supplier_invoice_no: string;
    supplier_invoice_date: Date | string;
    invoice_amount: number | string;
    invoice_actual_amount: number;
    grn_no: number | string;
    po_id: number;
    purchase_order_no: string;
    invoice_entry_date: Date | string;
    master_discount_percent: number;
    master_discount_amount: number | string;
    credit_days: number;
    special_discount_percent: number;
    special_discount_amount: number | string;
    tcs_percent: number | string;
    tcs_percent_amount: number | string;
    tcs_amount: number | string;
    actual_tcs_amount: number | string;
    irn: string;
    product_total: number | string;
    actual_product_total: number | string;
    discount_total: number | string;
    actual_discount_total: number | string;
    freight_charges: number | string;
    cgst_total: number | string;
    actual_cgst_total: number | string;
    sgst_total: number | string;
    actual_sgst_total: number | string;
    igst_total: number | string;
    actual_igst_total: number | string;
    round_off_total: number | string;
    actual_round_off_total: number | string;
    grand_total: number | string;
    actual_grand_total: number | string;
    additions: number | string;
    deductions: number | string;
    merchandiser_name: string;
    qty_total: number;
    undeducted_product_total: number;
    actual_undeducted_product_total: number;
    include_freight_in_tax: boolean;
    include_additions_in_tax: boolean;
    include_deductions_in_tax: boolean;

    // actual_discount_amount : number | string;
    // actual_assessable_value : number | string;
    // actual_assessable_value : number | string;
    // actual_assessable_value : number | string;
    // actual_assessable_value : number | string;
}

export class Flags {
    isRegistered?: boolean;
    isIgst: boolean;
    isComposite: boolean;
    isDiscountAmount: boolean;
    isSupplierDesc: boolean;
    isMrp: boolean;
    isDryWash: boolean;
    isAddMargin: boolean;
    // isAllSupplier: boolean;
}

export class InvoiceDetails {
    inv_byno: string;
    byno_serial: number;
    product_group_id: number | string;
    product_group_name: string;
    product_group_uom: string | number;
    product_qty: number | string;
    actual_product_qty: number | string;
    no_of_pcs: number;
    actual_no_of_pcs: number;
    cost_price: number | string;
    actual_cost_price: number | string;
    discount_amount: number | string;
    discount_percent: number | string;
    discount_percent_amt: number | string;
    price_code: string;
    add_margin: string;
    supplier_desc: string;
    dry_wash: boolean;
    mrp: number | string;
    hsn_code: string;
    sgst?: number | string;
    actual_sgst?: number | string;
    sgst_amount?: number | string;
    actual_sgst_amount?: number | string;
    cgst?: number | string;
    actual_cgst?: number | string;
    cgst_amount?: number | string;
    actual_cgst_amount?: number | string;
    igst?: number | string;
    actual_igst?: number | string;
    igst_amount?: number | string;
    actual_igst_amount?: number | string;
    tax_percentage: number;
    round_off: number | string;
    actual_round_off: number | string;
    total_taxable_value: number | string;
    actual_total_taxable_value: number | string;
    productGroupFocus: boolean;
    profit_percentage: number;
    selling_price: number;
    // columns: any[];
}

export class SupplierDescription {
    supplier_description_id: number;
    supplier_group_id: number;
    supplier_description: string;
}

export class InvoicePoNos {
    po_id: number;
    po_no: string;
    merchandiser_name: string;
}

export class PendingInvoiceNos {
    invoice_no: string;
    invoice_date: Date | string;
    grn_no: number | string;
    warehouse_id: number;
    company_section_id: number;
    serial_no: number;
    supplier_code: string;
    invoice_amount: number | string;
}