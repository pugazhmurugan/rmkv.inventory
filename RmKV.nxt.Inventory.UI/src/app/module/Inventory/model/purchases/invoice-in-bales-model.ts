export class InvoiceInBales {
    company_section_id: number;
    company_section_name: string;
    section_name: string;
    section_grn_no: number;
    grn_no: number;
    supplier_code: string;
    supplier_name: string;
    grn_date: Date | string;
    no_of_invoices: number;
    current_invoices: number;
}