export class InvoiceRegister {
    register_id:number;
    supplier_code: string;
    supplier_name: string;
    supplier_address: string;
    gst_no: string;
    supplier_email:string;
    gstn_no:string;
    supplier_gstn_no:string;
    ewaybillno:string;
    invoice_no:string;
    invoice_date:string | Date;
    sub_total:number;
    other_amount:number;
    taxable_amount:number;
    cgst_amount:number;
    sgst_amount:number;
    igst_amount:number;
    cess:number;
    round_off:number;
    invoice_amount:number;
    warehouse_id: number;
    company_section_id: number;
    entered_by: number;
}

export class Invoices {
    supplier_code: string;
    supplier_name: string;
    supplier_address: string;
    supplier_gstn_no: string;
    supplier_email:string;
}