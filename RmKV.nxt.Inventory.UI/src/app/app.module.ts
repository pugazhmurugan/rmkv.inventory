import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { CommonRoute } from './common/common.routing';
import { CommonViewModule } from './common/common.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthendicationGuard } from './common/services/authendication/guard/authendication-guard.guard';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS, MatNativeDateModule } from '@angular/material/core';
import { HttpInterceptorService, HttpLoader } from './common/services/app-interceptor/http-client-interceptor/http-interceptor.service';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';
import { httpFactory } from './common/services/app-interceptor/http-interceptor/intercept-http-factory';
import { AppDateAdapter, APP_DATE_FORMATS } from './common/shared/directives/date-picker';
import { Http, XHRBackend, RequestOptions, HttpModule } from "@angular/http";
import { ReactiveFormsModule } from '@angular/forms';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { DBConfig, NgxIndexedDBModule } from 'ngx-indexed-db';
import { InventoryModule } from './module/Inventory/inventory.module';
import { InventoryRoute } from './module/Inventory/inventory.routing';

const httpClientLoaderService = [HttpInterceptorService, HttpLoader];
const httpLoaderService = [httpFactory, HttpLoader];

const dbConfig: DBConfig = {
  name: 'InvoiceData',
  version: 1,
  objectStoresMeta: [
    {
      store: 'invoice',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'by_no', options: { unique: false } },
      ]
    },
    {
      store: 'GC',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'by_no', options: { unique: false } },
      ]
    },
    {
      store: 'CG',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'by_no', options: { unique: false } },
      ]
    },
    {
      store: 'GTWCity',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'by_no', options: { unique: false } },
      ]
    },
    {
      store: 'GTWState',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'by_no', options: { unique: false } },
      ]
    },
    {
      store: 'GTOState',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'by_no', options: { unique: false } },
      ]
    },
    {
      store: 'GTStore',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'by_no', options: { unique: false } },
      ]
    },
    {
      store: 'GTReturnable',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'by_no', options: { unique: false } },
      ]
    },
    {
      store: 'purchaseReturn',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'by_no', options: { unique: false } },
      ]
    },
    {
      store: 'multipleProduct',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'by_no', options: { unique: false } },
      ]
    }, 

    {
      store: 'ProductChangebyno',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'by_no', options: { unique: false } },
      ]
    },

    {
      store: 'ProductChangeStyleCode',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'by_no', options: { unique: false } },
      ]
    }
  ],
};

@NgModule({

  declarations: [
    AppComponent
  ],

  imports: [
    NgxIndexedDBModule.forRoot(dbConfig),
    BrowserModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    InventoryModule,
    CommonViewModule,
    InventoryRoute,
    CommonRoute,
    PerfectScrollbarModule,
    CommonModule
  ],

  providers: [
    AuthendicationGuard,
    httpClientLoaderService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] },
    { provide: MAT_DATE_LOCALE, useValue: "en-IN" },
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    {
      provide: Http,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions, HttpLoader]
    },
    {
      provide: APP_BASE_HREF,
      useValue: environment.baseUrl
    }
  ],

  bootstrap: [AppComponent],

  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})


export class AppModule { }
