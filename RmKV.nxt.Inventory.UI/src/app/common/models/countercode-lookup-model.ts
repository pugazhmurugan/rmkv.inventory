export class CounterCode {
    counter_code: string;
    counter_id: number;
    counter_name: string;
}
