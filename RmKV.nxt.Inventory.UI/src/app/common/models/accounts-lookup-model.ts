export class AccountsLookUp {
    account_code:string;
    account_name:string;
    gstn_no: string;
    address1: string;
    address2: string;
    address3: string;
    email: string;
    agent_address1: string;
    agent_address2: string;
    agent_address3: string;
    agent_email: string;   
}