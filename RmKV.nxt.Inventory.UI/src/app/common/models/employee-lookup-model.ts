export class EmployeeLookup {
    Employee_Id: number;
    Employee_Code: string;
    Employee_Name: string;
    Date_Left?:string | Date;
    Designation_Name?: string;
    Department_Name?: string;
    Section_Name?: string;
    Company_Name?: string;
    Sales_Location_Name?: string;
    Level1?: string;
    Level2?: string;
    Section_Id?: number;
    Salesman_Code?: number;
    KRA_Template_Id?: number;
    Incentive_Type_Name?: string;
    Incentive_Type_Id? : number;
    Non_Sales_Designation_Name?: string;
    Non_Sales_Section_Name?: string;
    Weekly_Day_Off?: string;
}

export class EmployeeLookupDialog {
    searchString: string;
    Sales_Location_Id?: number;
    Sales_Location_Name?: string;
    Section_Name?: string;
    Company_Name?: string;
    Designation_Name?: string;
    For?: string;
    Non_Sales_Desgination_Name?: string;
    Non_Sales_Section_Name?: string;
    Level1?: string;
    Resigned: boolean;
}

export class EmployeeLookupFil {
    For: string;
    Resigned: boolean;
    Sales_Location_Id: number;
}