import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { LocalStorage } from '../../shared/local-storage';
import { throwError, BehaviorSubject, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Modules, Menus, RemoveToken } from '../../models/common-model';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  constructor(private _httpClient: HttpClient, private _localStorage: LocalStorage) {
    this.currentUserSubject = new BehaviorSubject<any>(_localStorage.getToken());
    this.currentUser = this.currentUserSubject.asObservable();
  }

  getMenus(objMenu: Menus) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Menus/GetMenus", objMenu)
      .pipe(catchError(this.handleError));
  }

  getCompany(objCompanyFil: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetCompanies", objCompanyFil);
  }

  getWorkingLocation() {
    let data = {
      Sales_Location_Id: +this._localStorage.getGlobalSalesLocationId(),
      User_Id: +this._localStorage.intGlobalUserId()
    }
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetSalesLocations", data)
      .pipe(catchError(this.handleError));
  }

  checkLockPassword(objUserProfile: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/CheckLockPassword", objUserProfile)
      .pipe(catchError(this.handleError));
  }

  getAllWorkingLocation(objAllWorkingLocation: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetAllSalesLocations", objAllWorkingLocation);
  }

  getInventoryDocumentRootPathAndUrl() {
    return this._httpClient.get(this._localStorage.getApiUrl() + "Shared/GetGRNDocumentRootPathAndUrl/" + this._localStorage.getGlobalSalesLocationId())
      .pipe(catchError(this.handleError));
  }

  getHRConfiguration() {
    return this._httpClient.get(this._localStorage.getApiUrl() + 'Shared/GetHRConfiguration/' + this._localStorage.getGlobalSalesLocationId())
      .pipe(catchError(this.handleError));
  }

  getUserProfile(objUserProfile: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetUserProfile", objUserProfile)
      .pipe(catchError(this.handleError));
  }

  getGRNSections(objSections: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetGRNSectionsByWarehouseId", objSections)
      .pipe(catchError(this.handleError));
  }

  getCommonWarehouseSections(objSections: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetCommonSectionsDetails", objSections)
      .pipe(catchError(this.handleError));
  }

  uploadTempFile(formData: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/UploadGRNTempFile", formData)
      .pipe(catchError(this.handleError));
  }

  getLRList(formData: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetLRLookup", formData)
      .pipe(catchError(this.handleError));
  }

  getStates() {
    return this._httpClient.get(this._localStorage.getApiUrl() + "Shared/GetStates")
      .pipe(catchError(this.handleError));
  }

  getGroupSection(objgroupsection: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetSection", objgroupsection)
      .pipe(catchError(this.handleError));
  }

  getGroupSections() {
    return this._httpClient.get(this._localStorage.getApiUrl() + "Shared/GetGroupSections")
      .pipe(catchError(this.handleError));
  }

  getByNoDetails(data: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetByNoDetails", data)
      .pipe(catchError(this.handleError));
  }

  checkBynoByScreen(data: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetByNoDetails", data)
      .pipe(catchError(this.handleError));
  }

  getWarehouse(objWarehouse: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetWarehouse", objWarehouse)
      .pipe(catchError(this.handleError));
  }

  getGlobalData(objWarehouse: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetGlobalParams", objWarehouse)
      .pipe(catchError(this.handleError));
  }

  getWarehouseBasedCompanySection(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetCompanySectionDetails", obj)
      .pipe(catchError(this.handleError));
  }

  getDocumentPath(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetDocumentPath", obj)
      .pipe(catchError(this.handleError));
  }

  getDocumentPathView(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + "Shared/GetDocumentPathView", obj)
      .pipe(catchError(this.handleError));
  }

  public getMaterialLookup(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Shared/GetMaterialLookup', obj)
      .pipe(catchError(this.handleError));
  }

  public getMaterialProductLookup(obj: any) {
    return this._httpClient.post(this._localStorage.getApiUrl() + 'Shared/GetMaterialProductLookup', obj)
      .pipe(catchError(this.handleError));
  }

  logout() {
    localStorage.removeItem('userInfo');
  }

  handleError(error: Response) {
    return throwError(error);
  }

  handleError1(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.log("Client Side Error =" + error.message);
    } else {
      console.log("Server Side Error=" + error.message);
    }
    return throwError(error);
  }

}
