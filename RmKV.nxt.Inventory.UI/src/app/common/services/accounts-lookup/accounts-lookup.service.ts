import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from '../../shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class AccountsLookupService {

  constructor(private _http: HttpClient, private _localStorage: LocalStorage) { }

  getAccountsLookup(objAccounts : any) {
    return this._http.post(this._localStorage.getApiUrl() + "Shared/GetGRNAccountsLookup", objAccounts)
      .pipe(catchError(this.handleError));
  }
  gePendindSupplierLookup(objAccounts : any) {
    return this._http.post(this._localStorage.getApiUrl() + "WorkOrderReceipt/GetWorkOrderReceiptPendindSupplierList", objAccounts)
      .pipe(catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.log("Client Side Error =" + error.message);
    } else {
      console.log("Server Side Error=" + error.message);
    }
    return throwError(error);
  }}
