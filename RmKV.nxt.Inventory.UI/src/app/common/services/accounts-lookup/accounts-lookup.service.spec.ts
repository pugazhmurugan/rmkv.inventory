import { TestBed } from '@angular/core/testing';

import { AccountsLookupService } from './accounts-lookup.service';

describe('AccountsLookupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccountsLookupService = TestBed.get(AccountsLookupService);
    expect(service).toBeTruthy();
  });
});
