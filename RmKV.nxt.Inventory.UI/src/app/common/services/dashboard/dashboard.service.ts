import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { LocalStorage } from '../../shared/local-storage';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private _http: HttpClient, private _localStorage: LocalStorage) { }
  
  public getGRNDashboard(objGet: any) {
    debugger;
    return this._http.post(this._localStorage.getApiUrl() + "Shared/GetDashboardDetails" , objGet)
      .pipe(catchError(this.handleError));
  }
  
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.log("Client Side Error =" + error.message);
    } else {
      console.log("Server Side Error=" + error.message);
    }
    return throwError(error);
  }
}
