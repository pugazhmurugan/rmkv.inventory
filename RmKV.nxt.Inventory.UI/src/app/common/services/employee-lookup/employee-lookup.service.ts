import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from '../../shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class EmployeeLookupService {

  constructor(private _http: HttpClient, private _localStorage: LocalStorage) { }

  getEmployeeLookupfor(employeeLookupFil: any) {
    debugger;
    return this._http.post(this._localStorage.getApiUrl() + "Shared/GetEmployeeLookup" ,  employeeLookupFil)
      .pipe(catchError(this.handleError));
  }
  getResignedEmployee(employeeLookupFil: any) {
    debugger;
    return this._http.post(this._localStorage.getApiUrl() + "Shared/GetResignedEmployeeLookup" ,  employeeLookupFil)
      .pipe(catchError(this.handleError));
  }
  // getAllEmployeesForEmployeeLookup() {
  //   return this._http.get(this._localStorage.getApiUrl() + "EmployeeLookup/GetEmployeeLookup?Sales_Location_Id=" +
  //     this._localStorage.getGlobalSalesLocationId() + "&For=All")
  //     .pipe(catchError(this.handleError));
  // }

    handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        console.log("Client Side Error =" + error.message);
      } else {
        console.log("Server Side Error=" + error.message);
      }
      return throwError(error);
    }
}
