import { TestBed } from '@angular/core/testing';

import { EmployeeLookupService } from './employee-lookup.service';

describe('EmployeeLookupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmployeeLookupService = TestBed.get(EmployeeLookupService);
    expect(service).toBeTruthy();
  });
});
