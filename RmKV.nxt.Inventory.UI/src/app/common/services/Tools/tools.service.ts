import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorage } from '../../shared/local-storage';

@Injectable({
  providedIn: 'root'
})
export class ToolsService {

  constructor(private _http: HttpClient, private _localStorage: LocalStorage) { }

  getProductChangeLookup(employeeLookupFil: any) {
    debugger;
    return this._http.post(this._localStorage.getApiUrl() + "ProductChange/GetProductChangeLookup", employeeLookupFil)
      .pipe(catchError(this.handleError));
  }
  getStyleChangeLookup(employeeLookupFil: any) {
    debugger;
    return this._http.post(this._localStorage.getApiUrl() + "ProductChange/GetStyleChangeLookup", employeeLookupFil)
      .pipe(catchError(this.handleError));
  }
  getCounterChangeLookup(employeeLookupFil: any) {
    debugger;
    return this._http.post(this._localStorage.getApiUrl() + "ProductChange/GetCounterChangeLookup", employeeLookupFil)
      .pipe(catchError(this.handleError));
  }
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.log("Client Side Error =" + error.message);
    } else {
      console.log("Server Side Error=" + error.message);
    }
    return throwError(error);
  }
}