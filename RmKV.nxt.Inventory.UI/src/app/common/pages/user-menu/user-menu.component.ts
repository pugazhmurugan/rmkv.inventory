import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LocalStorage } from '../../shared/local-storage';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { LockScreenComponent } from '../../shared/lock-screen/lock-screen.component';
import { MatDialog } from '@angular/material';
import { CommonService } from '../../services/common/common.service';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UserMenuComponent implements OnInit {

  userName: string = "";
  designationName: string = "Designation";
  employeePhotoName: string = '../assets/images/users/user.jpg';
  public userImage = '../assets/images/users/user.jpg';
  
  constructor(public _localStorage: LocalStorage, public _router: Router, public _dialog: MatDialog,
    private _activatedRoute: ActivatedRoute, public _commonService: CommonService,) {
      console.log(this._localStorage.getLockStatus(),'Lock')
      if(this._localStorage.getLockStatus() == 'true'){
        this.openLockScreen();
      }
     }

  ngOnInit() {
    let userProfile = JSON.parse(this._activatedRoute.snapshot.data["userProfile"])[0];
    if (!userProfile)
      userProfile = this._localStorage.getUserProfile();
    if (userProfile) {
      this._localStorage.setUserProfile(userProfile);
      this.setUserProfile(userProfile);
    } else this.userProfile();
  }

  private userProfile(): void {
    let objUserProfile = {
      UserProfile: this._localStorage.intGlobalUserId()
    };
    this._commonService.getUserProfile(objUserProfile).subscribe((result: any) => {
      this._localStorage.setUserProfile(JSON.parse(result)[0]);
      this.setUserProfile(JSON.parse(result));
    });
  }

  private setUserProfile(userProfile: any): void {
    this.userName = userProfile.employee_name ? userProfile.employee_name : this._localStorage.getLoggedUserName();
    this.designationName = userProfile.designation_name ? userProfile.designation_name : 'Designation';
    this.employeePhotoName = userProfile.employee_photo_name ? userProfile.employee_photo_name : '../assets/images/users/user.jpg';
  }

  public redirectToLauncher(menuPath: string): void {
    let urlParameters = btoa("Sales_Location_ID=" + this._localStorage.getGlobalSalesLocationId() +
      "&Company_ID=" + this._localStorage.getGlobalCompanyId() +
      "&Module_ID=" + this._localStorage.getModuleId() +
      "&User_ID=" + this._localStorage.intGlobalUserId() +
      "&Menu_Path=" + menuPath +
      "&User_Name=" + this._localStorage.getLoggedUserName() +
      "&Section_ID=" + this._localStorage.getGlobalSectionId() +
      "&Warehouse_ID=" + this._localStorage.getGlobalWarehouseId() +
      "&Group_Section_ID=" + this._localStorage.getGlobalGroupSectionId()
    )

    window.location.href = JSON.parse(this._localStorage.getEnvironment()).launcherUiUrl + "params=" + urlParameters +
      "&AuthToken=" + this._localStorage.getAuthToken();
  }

  public mailClick(): void {
    this._router.navigate(["/Inventory/Mail"]);
  }

  public chatClick(): void {
    this._router.navigate(["/Inventory/Chats"]);
  }

  public openLockScreen(): void {
    let dialogRef = this._dialog.open(LockScreenComponent, {
      width: "250px",
      data: {},
      panelClass: "custom-dialog-container1"
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
