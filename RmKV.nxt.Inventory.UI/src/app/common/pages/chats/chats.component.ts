import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { LocalStorage } from '../../shared/local-storage';
import { Router } from '@angular/router';
import { ChatService } from './chats.service';
import { Chat } from '../../models/common-model';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss'],
  providers: [ ChatService ]
})
export class ChatsComponent implements OnInit {

  @ViewChild('sidenav',null) sidenav: any;
  public userImage = 'assets/images/users/user.jpg';
  public chats: Array<Chat>;
  public talks: Array<Chat>;
  public sidenavOpen:boolean = true;
  public currentChat:Chat;
  public newMessage:string;

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router, private chatService:ChatService) { }

    ngOnInit() {
      this.chats = this.chatService.getChats(); 
      if(window.innerWidth <= 768){
        this.sidenavOpen = false;
      }    
    } 
  
    @HostListener('window:resize')
    public onWindowResize():void {
      (window.innerWidth <= 768) ? this.sidenavOpen = false : this.sidenavOpen = true;
    }
  
    public getChat(obj){
      if(this.talks){
         this.talks.length = 2;
      }   
      this.talks = this.chatService.getTalk();
      this.talks.push(obj);
      this.currentChat = obj;      
      this.talks.forEach(talk => {
        if(!talk.my){
          talk.image = obj.image;
        }
      });
      if(window.innerWidth <= 768){
        this.sidenav.close();
      }     
    }
  
    public sendMessage($event) {       
      if (($event.which === 1 || $event.which === 13) && this.newMessage.trim() != '') {
        if(this.talks){ 
          this.talks.push(
              new Chat(
                'assets/images/users/user.jpg', 
                'Emilio Verdines', 
                'online', 
                this.newMessage,
                new Date(), 
                true)
          )
          this.newMessage = '';
          let chatContainer = document.querySelector('.chat-content');
          if(chatContainer){
            setTimeout(() => {
              var nodes = chatContainer.querySelectorAll('.mat-list-item');
              let newChatTextHeight = nodes[nodes.length- 1];
              chatContainer.scrollTop = chatContainer.scrollHeight + newChatTextHeight.clientHeight;
            }); 
          }
        }
      }
    }
  
    public ngOnDestroy(){
      if(this.talks)
        this.talks.length = 2;
    }
  
  }