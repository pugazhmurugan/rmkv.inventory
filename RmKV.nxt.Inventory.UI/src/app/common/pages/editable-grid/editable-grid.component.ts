import { DecimalPipe } from '@angular/common';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild, ViewChildren, ɵConsole } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { NgxIndexedDBService, ObjectStoreMeta } from 'ngx-indexed-db';
import { ForCheckBynoData, ByNoModel } from 'src/app/module/Inventory/model/common.model';
import { CommonService } from '../../services/common/common.service';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { GridKeyEvents } from '../../shared/directives/grid-key-events';
import { KeyPressEvents } from '../../shared/directives/key.press.events';
import { LocalStorage } from '../../shared/local-storage';

@Component({
  selector: 'app-editable-grid',
  templateUrl: './editable-grid.component.html',
  styleUrls: ['./editable-grid.component.scss'],
  providers: [DecimalPipe]
})
export class EditableGridComponent implements OnInit {
  inputEls: any = [];
  isBynoLength: number = 0;
  Inv_Byno: string = "";
  Byno_Serial: string = "";
  Byno_Prod_Serial: string = "";

  ForCheckBynoData: ForCheckBynoData = {
    Inv_Byno: "",
    Byno_Serial: "",
    Byno_Prod_Serial: ""
  };

  ByNoObj: ByNoModel = {
    byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
    uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
    isDelete: false, valid: false,counter_id : 0
  }

  @Input() value: any;
  objEditableGrid = {
    tabledata: [],
    counterdata : [],
    flags: { isProductCode: false, isProductName: false, isUOM: false, isProdPcs: false, isProdQty: false, isSellingPrice: false, 
      isHSN: false, isGST: false, isDisable: false, isAction: true, isProductSupplier : false,isCounter : false, isStyleCode : false, isCounterEdit : true },
    schema_name: ''
  }

  columns: any[] = [
    { display: "sno", editable: false },
    { display: "byno", editable: true },
    { display: "productCode", editable: false },
    { display: "productName", editable: false },
    { display: "productSupplierName", editable: false },
    { display: "uom", editable: false },
    { display: "sellingPrice", editable: false },
    { display: "qty", editable: false },
    { display: "pieces", editable: false },
    { display: "counter", editable: false },
    { display: "hsn", editable: false },
    { display: "gst", editable: false },
    { display: "stylecode", editable: false },
    { display: "action", editable: true },
  ]

  @ViewChildren("byno") byno: ElementRef | any;
  @ViewChildren("productCode") productCode: ElementRef | any;
  @ViewChildren("productName") productName: ElementRef | any;
  @ViewChildren("uom") uom: ElementRef | any;
  @ViewChildren("sellingPrice") sellingPrice: ElementRef | any;
  @ViewChildren("qty") qty: ElementRef | any;
  @ViewChildren("pieces") pieces: ElementRef | any;
  @ViewChildren("hsn") hsn: ElementRef | any;
  @ViewChildren("gst") gst: ElementRef | any;
  @ViewChildren("action") action: ElementRef | any;


  @Output() OutData = new EventEmitter<string>();
  already: boolean;
  focusFlag: boolean = false;
  constructor(public _localStorage: LocalStorage, private _commonServices: CommonService,
    private _confirmationDialogComponent: ConfirmationDialogComponent,
    public _router: Router, private dbService: NgxIndexedDBService, public _gridKeyEvents: GridKeyEvents,
    public _keyPressEvents: KeyPressEvents, private _matDialog: MatDialog, public _decimalPipe: DecimalPipe) {
  }
  //***************************************************************Index DB**********************************************************************************//

  private addIndexedData(schema_name: string): void {
    this.dbService.getAll(schema_name).subscribe((data) => {
      console.log(data, 'all');
      if (data.length !== 0) {
        let array = data.filter(x => +x.user_id === +this._localStorage.intGlobalUserId() && +x.company_section_id === +this._localStorage.getCompanySectionId());
        console.log(array, 'all_filter');
        if (array.length !== 0) {
          let id = array[0].id;
          this.dbService
            .update(schema_name, {
              id: id,
              user_id: this._localStorage.intGlobalUserId(),
              company_section_id: +this._localStorage.getCompanySectionId(),
              data: JSON.stringify(this.objEditableGrid.tabledata.filter(x => x.valid == true))
            })
            .subscribe((storeData) => {
              console.log('storeData: ', storeData);
            });
        }
        else {
          this.dbService
            .add(schema_name, {
              user_id: this._localStorage.intGlobalUserId(),
              company_section_id: +this._localStorage.getCompanySectionId(),
              data: JSON.stringify(this.objEditableGrid.tabledata.filter(x => x.valid == true))
            })
            .subscribe((key) => {
              console.log('key: ', key);
            });
        }

      }
      else {
        this.dbService
          .add(schema_name, {
            user_id: this._localStorage.intGlobalUserId(),
            company_section_id: +this._localStorage.getCompanySectionId(),
            data: JSON.stringify(this.objEditableGrid.tabledata.filter(x => x.valid == true))
          })
          .subscribe((key) => {
            console.log('key: ', key);
          });
      }
    })
  }

  private deleteIndexedData(schema_name: string): void {

    this.dbService.getAll(schema_name).subscribe((data) => {
      if (data.length !== 0) {
        let array = data.filter(x => +x.user_id === +this._localStorage.intGlobalUserId() && +x.company_section_id === +this._localStorage.getCompanySectionId());
        console.log(array, 'all_filter');
        if (array.length !== 0) {
          let id = array[0].id;
          this.dbService
            .update(schema_name, {
              id: id,
              user_id: this._localStorage.intGlobalUserId(),
              company_section_id: +this._localStorage.getCompanySectionId(),
              data: JSON.stringify(this.objEditableGrid.tabledata.filter(x => x.valid == true))
            })
            .subscribe((storeData) => {
              console.log('storeData: ', storeData);
            });
        }
        else {
          this.dbService
            .add(schema_name, {
              user_id: this._localStorage.intGlobalUserId(),
              company_section_id: +this._localStorage.getCompanySectionId(),
              data: JSON.stringify(this.objEditableGrid.tabledata)
            })
            .subscribe((key) => {
              console.log('key: ', key);
            });
        }
      }
      else {
        this.dbService
          .add(schema_name, {
            user_id: this._localStorage.intGlobalUserId(),
            company_section_id: +this._localStorage.getCompanySectionId(),
            data: JSON.stringify(this.objEditableGrid.tabledata)
          })
          .subscribe((key) => {
            console.log('key: ', key);
          });
      }
    })
  }
  private initiateDB(schema_name: string): void {
    const storeSchema: ObjectStoreMeta = {
      store: schema_name,
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
        { name: 'data', keypath: 'data', options: { unique: false } }
      ]
    };

    this.dbService.createObjectStore(storeSchema);
    this.clearobj();
    this.objEditableGrid.tabledata.push(this.ByNoObj);
    this.dbService.count(schema_name).subscribe((peopleCount) => {
      console.log(peopleCount, schema_name, this._localStorage.intGlobalUserId());
      if (peopleCount > 0) {

        this.dbService.getAll(schema_name).subscribe((data) => {
          if (data.length !== 0) {
            let array = data.filter(x => +x.user_id === +this._localStorage.intGlobalUserId() && +x.company_section_id === +this._localStorage.getCompanySectionId());
            console.log(array, 'all_filter');
            if (array.length !== 0) {
              let id = array[0].id;
              this.objEditableGrid.tabledata = [];
              this.objEditableGrid.tabledata = JSON.parse(array[0].data);
              if (!this.objEditableGrid.flags.isDisable) {
                this.clearobj();
                this.objEditableGrid.tabledata.push(this.ByNoObj);
              }
              console.log(this.objEditableGrid.tabledata, 'table')
            }
            else {
              this.objEditableGrid.tabledata = [];
              if (!this.objEditableGrid.flags.isDisable) {
                this.clearobj();
                this.objEditableGrid.tabledata.push(this.ByNoObj);
              }
            }
          }
          else {
            this.objEditableGrid.tabledata = [];
            if (!this.objEditableGrid.flags.isDisable) {
              this.clearobj();
              this.objEditableGrid.tabledata.push(this.ByNoObj);
            }
          }
        })
      }
    });
  }

  //*************************************************************** Validations **********************************************************************************//
  private checkExistByno(i: number) {
    this.already = false;
    let index = 0;
    let byno = '';
    for (let j = 0; (j < this.objEditableGrid.tabledata.length && j !== i); j++) {
      if (this.objEditableGrid.tabledata[j].byno === this.objEditableGrid.tabledata[i].byno) {
        this.already = true;
        index = j + 1;
        byno = this.objEditableGrid.tabledata[i].byno;
        break;
      }
    }
    if (this.already) {
      // this.inputEls = this.byno.toArray();
      // this.inputEls[i].nativeElement.focus();
      this.openAlertDialogByno(i, "This Byno " + " " + byno + " Already Available In Row No" + " " + index + ""
      );
    }
  }
  openAlertDialogByno(i, value) {
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = "Byno";
    _dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let input = this.byno.toArray();
        input[i].nativeElement.focus();
      }
      _dialogRef = null;
    });
  }


  checkDuplicateByno(i: number) {
    var flag = 0;
    for (var start in this.objEditableGrid.tabledata) {
      if (
        i != +start &&
        this.objEditableGrid.tabledata[i].byno === this.objEditableGrid.tabledata[+start].byno
        && this.objEditableGrid.tabledata[i].unique_byno_prod_serial === true
      ) {
        this.clearInvalidBynoRow(i);
        this.openAlertDialog(i, "Byno Already Exist");
        flag = 1;
      }
    }
    if (flag === 0) {
      // setTimeout(() => {
      //   this.inputEls = this.qty.toArray();
      //   this.inputEls[i].nativeElement.focus();
      // }, 100);
      this.add(i);
    }
  }

  private clearInvalidBynoRow(i) {
    this.objEditableGrid.tabledata[i].byno = "";
    this.objEditableGrid.tabledata[i].product_code = "";
    this.objEditableGrid.tabledata[i].product_name = "";
    this.objEditableGrid.tabledata[i].uom_descrition = "";
    this.objEditableGrid.tabledata[i].selling_price = "0.00";
    this.objEditableGrid.tabledata[i].prod_qty = 0;
    this.objEditableGrid.tabledata[i].pieces = "";
    this.objEditableGrid.tabledata[i].hsncode = "";
    this.objEditableGrid.tabledata[i].Remarks = "";
    this.objEditableGrid.tabledata[i].gst = 0;
  }

  openAlertDialog(index: number, message: string) {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    dialogRef.componentInstance.alertMessage = message;
    dialogRef.componentInstance.componentName = "Byno";

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.inputEls = this.byno.toArray();
        this.inputEls[index].nativeElement.focus();
      }
      dialogRef = null;
    });
  }
  keypressValidate(Uomid, event: KeyboardEvent | any, maxValue: number, key1: string, key2: string) {
    if (Uomid == 2) {
      this.onlyAllowDecimal(event, maxValue, key1, key2)
    } else {
      this._keyPressEvents.onlyAllowNumbers(event);
    }
  }
  public onlyAllowDecimal(event: KeyboardEvent | any, maxValue: number, key1: string, key2: string): boolean {
    debugger;
    if (event.target.selectionStart === 0 && event.target.selectionEnd === event.target.value.length) {
      const pattern = /^[0-9]*$/;
      if (pattern.test(event.key)) {
        this[key1][key2] = "";
        event.target.value = "";
      }
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    } else {
      return this._keyPressEvents.onlyAllowDecimal(event, maxValue, true);
    }
  }

  // Validateions

  public disablePieces(i: number): boolean {
    if (this.objEditableGrid.tabledata[i].valid == false) {
      return true;
    }
    else if (this.objEditableGrid.tabledata[i].unique_byno_prod_serial === false &&
      this.objEditableGrid.tabledata[i].uom_id == 2) {
      return false;
    }
    else return true;
  }
  //***************************************************************KeyEvents**********************************************************************************//

  public onKeyFocusGlobal(event: KeyboardEvent | any, rowIndex: number, colIndex: number): void {

    if (this.objEditableGrid.tabledata[rowIndex].unique_byno_prod_serial === false && this.objEditableGrid.tabledata[rowIndex].uom_id !== 2) {
      this.columns[6].editable = true;
      this.columns[7].editable = false;
    }
    else if (this.objEditableGrid.tabledata[rowIndex].unique_byno_prod_serial === false && this.objEditableGrid.tabledata[rowIndex].uom_id === 2) {
      this.columns[6].editable = true;
      this.columns[7].editable = true;
    }

    //********* Up Arrow in Qty Field ***********//
    if (event.keyCode == 38 && colIndex == 6) {
      debugger;
      let index = -1;
      for (let i = 0; i < rowIndex; i++) {
        if (this.objEditableGrid.tabledata[i].unique_byno_prod_serial === false) {
          index = i;
        }
      }
      if (index !== -1) {
        rowIndex = index + 1;
        this.columns[6].editable = true;
      }
    }

    //********* Down Arrow in Qty Field ***********//
    if (event.keyCode == 40 && colIndex == 6) {
      debugger;
      let index = -1;
      for (let i = rowIndex; ((i + 1) < (this.objEditableGrid.tabledata.length - 1)); i++) {
        if (this.objEditableGrid.tabledata[i + 1].unique_byno_prod_serial === false) {
          index = i + 1;
          break;
        }
      }
      if (index !== -1) {
        rowIndex = index - 1;
        this.columns[6].editable = true;
      }
    }

    //********* Up Arrow in Pieces Field ***********//
    if (event.keyCode == 38 && colIndex == 7) {
      debugger;
      let index = -1;
      for (let i = 0; i < rowIndex; i++) {
        if (this.objEditableGrid.tabledata[i].unique_byno_prod_serial === false && this.objEditableGrid.tabledata[i].uom_id === 2) {
          index = i;
        }
      }
      if (index !== -1) {
        rowIndex = index + 1;
        this.columns[7].editable = true;
      }
    }

    //********* Down Arrow in Qty Field ***********//
    if (event.keyCode == 40 && colIndex == 7) {
      debugger;
      let index = -1;
      for (let i = rowIndex; ((i + 1) < (this.objEditableGrid.tabledata.length - 1)); i++) {
        if (this.objEditableGrid.tabledata[i + 1].unique_byno_prod_serial === false && this.objEditableGrid.tabledata[i + 1].uom_id === 2) {
          index = i + 1;
          break;
        }
      }
      if (index !== -1) {
        rowIndex = index - 1;
        this.columns[7].editable = true;
      }
    }

    //********* Proceed to Key Functions ***********//

    switch (event.keyCode) {
      case 13: // Enter Key
        let response = this._gridKeyEvents.onEnterKeyFocus(rowIndex, colIndex, this.columns, this, this.objEditableGrid.tabledata);
        if (response)
          this.add(rowIndex);
        break;

      case 37: // Arrow Left
        this._gridKeyEvents.focusLeft(rowIndex, colIndex, this.columns, this, this.objEditableGrid.tabledata);
        break;

      case 9: // Tab key
        this.onKeyPress(event, rowIndex);
        break;

      case 38: // Arrow Up
        this._gridKeyEvents.focusUp(rowIndex, colIndex, this.columns, this);
        break;

      case 39: // Arrow Right
        this._gridKeyEvents.focusRight(rowIndex, colIndex, this.columns, this, this.objEditableGrid.tabledata);
        break;

      case 40: // Arrow Down
        this._gridKeyEvents.focusDown(rowIndex, colIndex, this.columns, this, this.objEditableGrid.tabledata);
        break;
    }
  }

  public onPrdKeyPress(keyUpEvent: any, i: number) {
    if (keyUpEvent.keyCode == 9 || keyUpEvent.keyCode == 13) {
      if (this.objEditableGrid.tabledata[i].unique_byno_prod_serial === false &&
        this.objEditableGrid.tabledata[i].uom_id == 2) {
        setTimeout(() => {
          this.inputEls = this.pieces.toArray();
          this.inputEls[i + 1].nativeElement.focus();
        }, 10);
      } else {
        setTimeout(() => {
          this.inputEls = this.byno.toArray();
          this.inputEls[i + 1].nativeElement.focus();
        }, 10);
      }
    }
  }
  public onPiecesKeyPress(keyUpEvent: any, i: number) {
    if (keyUpEvent.keyCode == 9 || keyUpEvent.keyCode == 13) {
      setTimeout(() => {
        this.inputEls = this.byno.toArray();
        this.inputEls[i + 1].nativeElement.focus();
      }, 10);
    }
  }
  public openDeleteConfirmationDialog(i: number): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = 'Do you want remove this record?';
    dialogRef.componentInstance.componentName = 'Byno';
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.objEditableGrid.tabledata.splice(i, 1);
        setTimeout(() => {
          this.inputEls = this.byno.toArray();
          this.inputEls[i].nativeElement.focus();
        }, 100);
        this.addIndexedData(this.objEditableGrid.schema_name);
      }
      dialogRef = null;
    });
  }
  public onKeyPress(keyUpEvent: any, i: number) {
    this.focusFlag = keyUpEvent.keyCode == 13 ? true : false;
    if (keyUpEvent.keyCode == 8) {
      this.clearCurrentRow(i, false);
      setTimeout(() => {
        this.inputEls = this.byno.toArray();
        this.inputEls[i].nativeElement.focus();
      }, 10);
    } else if (keyUpEvent.keyCode == 46) {
      console.log(this.objEditableGrid.tabledata[i],"delete")
      if (this.objEditableGrid.tabledata[i].valid) {
        this.objEditableGrid.tabledata[i].byno = this.objEditableGrid.tabledata[i].inv_byno + "/" + Number(this.objEditableGrid.tabledata[i].byno_serial) + "/" + Number(this.objEditableGrid.tabledata[i].byno_prod_serial);
        this.openDeleteConfirmationDialog(i);
      } else if (this.objEditableGrid.tabledata[i + 1].valid || this.objEditableGrid.tabledata[i + 1]) {
        this.objEditableGrid.tabledata.splice(i, 1);
        setTimeout(() => {
          this.inputEls = this.byno.toArray();
          this.inputEls[i].nativeElement.focus();
        }, 100);
        this.addIndexedData(this.objEditableGrid.schema_name);
      }
    } else if (keyUpEvent.keyCode == 45) {
      if (this.objEditableGrid.tabledata[i].valid) {
        let data = this.objEditableGrid.tabledata;
        this.objEditableGrid.tabledata = [];
        if (i == 0) {
          this.clearobj();
          this.objEditableGrid.tabledata.push(this.ByNoObj);
          this.objEditableGrid.tabledata[0].isAdd = false;
          this.objEditableGrid.tabledata[0].isDelete = true;
          for (let j = 0; j < data.length; j++) {
            this.objEditableGrid.tabledata.push(data[j]);
          }
        } else {
          debugger;
          for (let j = 0; j < i; j++) {
            this.objEditableGrid.tabledata.push(data[j]);
          }
          this.clearobj();
          this.objEditableGrid.tabledata.push(this.ByNoObj);
          this.objEditableGrid.tabledata[this.objEditableGrid.tabledata.length - 1].isAdd = false;
          this.objEditableGrid.tabledata[this.objEditableGrid.tabledata.length - 1].isDelete = true;
          for (let j = i; (j >= i && j <= (data.length - 1)); j++) {
            this.objEditableGrid.tabledata.push(data[j]);
          }
        }
        setTimeout(() => {
          this.inputEls = this.byno.toArray();
          this.inputEls[i].nativeElement.focus();
        }, 100);
        this.addIndexedData(this.objEditableGrid.schema_name);
      }
    } else if (keyUpEvent.keyCode == 46) {
      if (this.objEditableGrid.tabledata[i].valid) {
        this.objEditableGrid.tabledata.splice(i, 1);
        this.addIndexedData(this.objEditableGrid.schema_name);
      }
      setTimeout(() => {
        this.inputEls = this.byno.toArray();
        this.inputEls[i + 1].nativeElement.focus();
      }, 10);
    } else if (keyUpEvent.keyCode === 13 &&
      this.objEditableGrid.tabledata[i].byno.length == 16) {
      this.byNoWithoutSlash(i);
    } else if (keyUpEvent.keyCode === 13 &&
      this.objEditableGrid.tabledata[i].byno.length == 28) {
      this.OldByNoWithoutSlash(i);
    } else if (keyUpEvent.keyCode === 13 &&
      this.objEditableGrid.tabledata[i].byno.length == 30) {
      this.byNoWithoutSlash(i);
    } else if (keyUpEvent.keyCode === 13 && this.objEditableGrid.tabledata[i].byno.includes("/")) {
      this.splitByNumber(i);
    } else if (this.objEditableGrid.tabledata[i].byno.length >= 28 && keyUpEvent.keyCode === 13) {
      this.autoScannerCall(i);
    } else {
      if (keyUpEvent.keyCode === 13 && !this.objEditableGrid.tabledata[i].byno.toString().trim()) {
        this.openAlertDialogByno(i, "Enter Byno");
      } else if (keyUpEvent.keyCode === 13 && !this.objEditableGrid.tabledata[i].byno.includes("/") && this.objEditableGrid.tabledata[i].byno.length > 16) {
        this._confirmationDialogComponent.openAlertDialog("Invalid Byno", "Byno");
      } else if (keyUpEvent.keyCode === 13) {
        this.objEditableGrid.tabledata[i].byno != "" ? this._confirmationDialogComponent.openAlertDialog("Invalid Byno", "Byno") : "";
        this.clearInvalidBynoRow(i);
      }
    }
  }
  public onFocusOut(keyUpEvent: any, i: number) {
    if (this.focusFlag == false && this.objEditableGrid.tabledata[i].product_code == '' && this.objEditableGrid.tabledata[i].byno.length !== 30 && this.objEditableGrid.tabledata[i].byno.length !== 28) {
      if (keyUpEvent.keyCode == 8) {
        // this.clearbynoData(i);
        this.clearCurrentRow(i, false);
        setTimeout(() => {
          this.inputEls = this.byno.toArray();
          this.inputEls[i].nativeElement.focus();
        }, 10);
      } else if (keyUpEvent.keyCode == 46) {
        this.clearCurrentRow(i, true);
        setTimeout(() => {
          this.inputEls = this.byno.toArray();
          this.inputEls[i].nativeElement.focus();
        }, 10);
      } else if (this.objEditableGrid.tabledata[i].byno.length == 16) {
        this.byNoWithoutSlash(i);
      } else if (this.objEditableGrid.tabledata[i].byno.length == 28) {
        this.OldByNoWithoutSlash(i);
      } else if (this.objEditableGrid.tabledata[i].byno.length == 30) {
        this.byNoWithoutSlash(i);
      } else if (this.objEditableGrid.tabledata[i].byno.includes("/")) {
        this.splitByNumber(i);
      } else if (this.objEditableGrid.tabledata[i].byno.length >= 28) {
        this.autoScannerCall(i);
      } else if (this.objEditableGrid.tabledata[i].byno !== '' && this.objEditableGrid.tabledata[i].product_code == '') {
        this.openAlertDialogByno(i, "Invalid Byno");
      } else {
        if (this.objEditableGrid.tabledata.length - 1 !== i && !this.objEditableGrid.tabledata[i].byno.toString().trim()) {
          this.openAlertDialogByno(i, "Enter Byno");
        } else if (!this.objEditableGrid.tabledata[i].byno.includes("/") && this.objEditableGrid.tabledata[i].byno.length > 16) {
          this.openAlertDialogByno(i, "Invalid Byno");
        }
      }
    } if (this.focusFlag == true) {
      this.focusFlag = false;
    }
  }
  //***************************************************************Functions**********************************************************************************//

  private byNoWithoutSlash(i: number) {
    this.objEditableGrid.tabledata[i].byno = this.objEditableGrid.tabledata[i].byno.toString().trim();
    this.ForCheckBynoData.Inv_Byno = this.objEditableGrid.tabledata[i].byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = this.objEditableGrid.tabledata[i].byno.substr(8, 4);
    this.ForCheckBynoData.Byno_Prod_Serial = this.objEditableGrid.tabledata[i].byno.substr(12, 4);
    this.getByNoDetails(this.ForCheckBynoData, i);
  }

  private OldByNoWithoutSlash(i: number) {
    this.objEditableGrid.tabledata[i].byno = this.objEditableGrid.tabledata[i].byno.toString().trim();
    this.ForCheckBynoData.Inv_Byno = this.objEditableGrid.tabledata[i].byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = "0" + this.objEditableGrid.tabledata[i].byno.substr(8, 3);
    this.ForCheckBynoData.Byno_Prod_Serial = "0" + this.objEditableGrid.tabledata[i].byno.substr(11, 3);
    console.log(this.ForCheckBynoData)
    this.getByNoDetails(this.ForCheckBynoData, i);
  }

  private autoScannerCall(i: number) {
    this.objEditableGrid.tabledata[i].byno = this.objEditableGrid.tabledata[i].byno.toString().trim();
    this.ForCheckBynoData.Inv_Byno = this.objEditableGrid.tabledata[i].byno.substr(0, 8);
    this.ForCheckBynoData.Byno_Serial = this.objEditableGrid.tabledata[i].byno.substr(8, 4);
    this.ForCheckBynoData.Byno_Prod_Serial = this.objEditableGrid.tabledata[i].byno.substr(11, 4);
    this.getByNoDetails(this.ForCheckBynoData, i);
  }

  private splitByNumber(i: number) {
    let already = false;
    let indexNum = 0;
    let byno = '';
    for (let index = 0; index < this.objEditableGrid.tabledata.length; index++) {
      if (
        index !== i &&
        this.objEditableGrid.tabledata[index].byno === this.objEditableGrid.tabledata[i].byno
        && this.objEditableGrid.tabledata[index].unique_byno_prod_serial === true
      ) {
        indexNum = index + 1;
        byno = this.objEditableGrid.tabledata[i].byno;
        already = true;
        break;
      }
    }
    if (already) {
      this.clearInvalidBynoRow(i);
      this._confirmationDialogComponent.openAlertDialog(
        "This Byno " + " " + byno + " Already Available In Row No" + " " + indexNum + "",
        "Byno"
      );
    } else {
      if (this.objEditableGrid.tabledata[i].byno.length >= 8) {
        if (this.objEditableGrid.tabledata[i].byno.includes("/")) {
          let slashOccurance = 0;
          for (let j = 0; j < this.objEditableGrid.tabledata[i].byno.length; j++) {
            if (this.objEditableGrid.tabledata[i].byno[j] === "/") {
              slashOccurance++;
            }
          }
          if (slashOccurance < 2) {
            this.clearInvalidBynoRow(i);
            this._confirmationDialogComponent.openAlertDialog(
              "Invalid Byno",
              "Byno"
            );
          } else {
            this.objEditableGrid.tabledata[i].byno = this.objEditableGrid.tabledata[i].byno.toString().trim();
            var x = this.objEditableGrid.tabledata[i].byno.split("/");
            this.Inv_Byno = x[0];
            this.Byno_Serial = x[1];
            this.Byno_Prod_Serial = x[2];
            if (this.Byno_Serial.length == 2) {
              this.Byno_Serial = "00".concat(this.Byno_Serial);
            }
            if (this.Byno_Prod_Serial.length == 2) {
              this.Byno_Prod_Serial = "00".concat(this.Byno_Prod_Serial);
            }
            if (this.Byno_Serial.length == 1) {
              this.Byno_Serial = "000".concat(this.Byno_Serial);
            }
            if (this.Byno_Prod_Serial.length == 1) {
              this.Byno_Prod_Serial = "000".concat(this.Byno_Prod_Serial);
            }
            this.ForCheckBynoData.Inv_Byno = this.Inv_Byno.toString().trim();
            this.ForCheckBynoData.Byno_Serial = this.Byno_Serial.toString().trim();
            this.ForCheckBynoData.Byno_Prod_Serial = this.Byno_Prod_Serial.toString().trim();
            this.getByNoDetails(this.ForCheckBynoData, i);
          }
        } else {
          this._confirmationDialogComponent.openAlertDialog(
            "Byno Must Have '/' Forward Slash",
            "Byno"
          );
        }
      } else {
        this.clearInvalidBynoRow(i);
        this._confirmationDialogComponent.openAlertDialog(
          "Invalid Byno",
          "Byno"
        );
      }
    }
  }

  public getByNoDetails(element: any, i: number): void {
    let obj = {
      ByNoData: JSON.stringify([{
        inv_byno: element.Inv_Byno,
        byno_serial: element.Byno_Serial,
        byno_prod_serial: element.Byno_Prod_Serial,
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._commonServices.getByNoDetails(obj).subscribe((result: any) => {
      if (result !== 'null' && JSON.parse(result).length > 0) {
        let data = JSON.parse(result);
        if (data[0].byno !== null) {
          this.objEditableGrid.tabledata[i] = data[0];
          this.objEditableGrid.tabledata[i].selling_price = this.objEditableGrid.tabledata[i].selling_price + '.00'; //this._decimalPipe.transform(data[0].selling_price, '1.2-2');
          this.objEditableGrid.counterdata = data[0].counterdata;
          this.objEditableGrid.tabledata[i].counter_id = data[0].counter_id;
          console.log(this.objEditableGrid, 'Product');
          if (data[0].unique_byno_prod_serial == true) {
            this.checkExistByno(i);
            if (this.already == true) {
              this.already = false;
              this.clearobj();
              this.objEditableGrid.tabledata[i] = this.ByNoObj;
            } else {
              this.addIndexedData(this.objEditableGrid.schema_name);
              this.objEditableGrid.tabledata[i].isAdd = false;
              this.objEditableGrid.tabledata[i].valid = true;
              this.ForCheckBynoData.Inv_Byno = "";
              this.ForCheckBynoData.Byno_Serial = "";
              this.ForCheckBynoData.Byno_Prod_Serial = "";
              this.isBynoLength = this.objEditableGrid.tabledata.length;
              if (data[0].unique_byno_prod_serial === true) {
                this.objEditableGrid.tabledata[i].prod_pcs = 1;
              }
              this.addemptyData(i);
            }
          } else {
            this.addIndexedData(this.objEditableGrid.schema_name);
            this.objEditableGrid.tabledata[i].isAdd = false;
            this.objEditableGrid.tabledata[i].valid = true;
            // this.checkDuplicateByno(i);
            this.ForCheckBynoData.Inv_Byno = "";
            this.ForCheckBynoData.Byno_Serial = "";
            this.ForCheckBynoData.Byno_Prod_Serial = "";
            this.isBynoLength = this.objEditableGrid.tabledata.length;
            if (data[0].unique_byno_prod_serial === true) {
              this.objEditableGrid.tabledata[i].prod_pcs = 1;
            }
            this.addemptyData(i);
          }
        } else {
          this.openAlertDialogByno(i, "Invalid ByNo");
          this.clearCurrentRow(i, true);
          if (this.objEditableGrid.tabledata[i + 1]) {
            this.objEditableGrid.tabledata[i].isAdd = false;
            this.objEditableGrid.tabledata[i].isDelete = true;
          }
        }
      } else {
        this.openAlertDialogByno(i, "Invalid ByNo");
        this.clearCurrentRow(i, true);
        if (this.objEditableGrid.tabledata[i + 1]) {
          this.objEditableGrid.tabledata[i].isAdd = false;
          this.objEditableGrid.tabledata[i].isDelete = true;
        }
      }
    })
  }
  //***************************************************************Others**********************************************************************************//

  private clearobj(): void {
    this.ByNoObj = {
      byno: '', inv_byno: '', byno_serial: '', byno_prod_serial: '', product_code: '', product_name: '', uom_id: 0,
      uom_descrition: '', prod_qty: 0, prod_pcs: 0, selling_price: '0.00', hsncode: '', gst: 0, unique_byno_prod_serial: null, isAdd: true,
      isDelete: false, valid: false, counter_id : 0
    }
  }


  clearCurrentRow(i: number, clear: boolean) {
    var byno = this.objEditableGrid.tabledata[i].byno;
    this.clearobj();
    this.objEditableGrid.tabledata[i] = this.ByNoObj;
    if (clear == false) {
      this.objEditableGrid.tabledata[i].byno = byno;
    }
    // this.objEditableGrid.tabledata[i].isAdd = false;
    // this.objEditableGrid.tabledata[i].isDelete = true;
    this.addIndexedData(this.objEditableGrid.schema_name);
  }

  public addemptyData(i: number): void {
    this.clearobj();
    if (!this.objEditableGrid.tabledata[i + 1]) {
      this.objEditableGrid.tabledata.push(this.ByNoObj);
    }
    this.objEditableGrid.tabledata[i].isAdd = false;
    this.objEditableGrid.tabledata[i].isDelete = true;
    if (this.objEditableGrid.tabledata[i].unique_byno_prod_serial == false) {
      setTimeout(() => {
        this.inputEls = this.qty.toArray();
        this.inputEls[i].nativeElement.focus();
      }, 100);
    }
    else if (!this.objEditableGrid.tabledata[i + 1]) {
      setTimeout(() => {
        this.inputEls = this.byno.toArray();
        this.inputEls[i].nativeElement.focus();
      }, 100);
    }
    else {
      setTimeout(() => {
        this.inputEls = this.byno.toArray();
        this.inputEls[i + 1].nativeElement.focus();
      }, 100);
    }

  }

  public add(i: number) {
    debugger
    if (this.objEditableGrid.tabledata[i].valid == true) {
      this.objEditableGrid.tabledata[i].isAdd = false;
      this.objEditableGrid.tabledata[i].isDelete = true;
      this.clearobj();
      if (!this.objEditableGrid.tabledata[i + 1]) {
        this.objEditableGrid.tabledata.push(this.ByNoObj);
        setTimeout(() => {
          this.inputEls = this.byno.toArray();
          this.inputEls[i + 1].nativeElement.focus();
        }, 100);
      }
      else {
        setTimeout(() => {
          this.inputEls = this.byno.toArray();
          this.inputEls[i + 1].nativeElement.focus();
        }, 100);
      }
    }
    else {
      if (!this.objEditableGrid.tabledata[i].byno.toString().trim()) {
        this.inputEls = this.byno.toArray();
        this.inputEls[i].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog(
          "Enter Byno",
          "Byno"
        );
      } else if (
        !this.objEditableGrid.tabledata[i].byno.includes("/") ||
        this.objEditableGrid.tabledata[i].byno.length > 16
      ) {
        this.inputEls = this.byno.toArray();
        this.inputEls[i].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog(
          "Invalid Byno",
          "Byno"
        );
      } else if (
        this.objEditableGrid.tabledata[i].byno.toString().trim() !== "" &&
        this.objEditableGrid.tabledata[i].product_code == ''
      ) {
        this.inputEls = this.byno.toArray();
        this.inputEls[i].nativeElement.focus();
        this._confirmationDialogComponent.openAlertDialog(
          "Invalid Byno",
          "Byno"
        );
      }
      // setTimeout(() => {
      //   this.inputEls = this.byno.toArray();
      //   this.inputEls[i].nativeElement.focus();
      // }, 100);
    }
  }

  public openConfirmationDialog(i: number): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = 'Do you want remove this record?';
    dialogRef.componentInstance.componentName = 'Byno';
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.delete(i);
      dialogRef = null;
    });
  }

  public delete(i: number) {
    this.deleteIndexedData(this.objEditableGrid.schema_name);
    this.objEditableGrid.tabledata.splice(i, 1);
    this.isBynoLength = this.objEditableGrid.tabledata.length;
    setTimeout(() => {
      this.inputEls = this.byno.toArray();
      this.inputEls[i].nativeElement.focus();
    }, 100);
  }


  public gettrue(i): boolean {
    if ((i + 1) < this.objEditableGrid.tabledata.length) {
      return true;
    }
    else {
      return false;
    }
  }

  public getfalse(i): boolean {
    if ((i + 1) === this.objEditableGrid.tabledata.length) {
      return true;
    }
    else {
      return false;
    }
  }

  ngOnInit() {
    setTimeout(() => {
      this.inputEls = this.byno.toArray();
      this.inputEls[this.inputEls.length - 1].nativeElement.focus();
    }, 100);
    console.log(this.value, 'InputobjEditableGrid');
    // this.objEditableGrid = this.value;
    console.log(this.value, 'input')
    if (this.value.tabledata.length > 0) {
      this.objEditableGrid.tabledata = [];
      for (let i = 0; i < this.value.tabledata.length; i++) {
        if (this.value.tabledata[i].valid == true) {
          this.objEditableGrid.tabledata.push(this.value.tabledata[i]);
          this.objEditableGrid.tabledata[i].isAdd = false;
          this.objEditableGrid.tabledata[i].isDelete = true;
        }
      }
      this.objEditableGrid.flags = this.value.flags;
      this.objEditableGrid.schema_name = this.value.schema_name;
      if (!this.objEditableGrid.flags.isDisable) {
        this.clearobj();
        this.objEditableGrid.tabledata.push(this.ByNoObj);
      }
    }
    else {
      this.objEditableGrid = this.value;
      this.initiateDB(this.objEditableGrid.schema_name);
    }
  }
  public getProductPcs(): number {
    return this.objEditableGrid.tabledata.map(t => +t.prod_pcs).reduce((acc, value) => acc + value, 0);
  }
  public getProductQty(): number {
    return this.objEditableGrid.tabledata.map(t => +t.prod_qty).reduce((acc, value) => acc + value, 0);
  }
  public getSellingPrice(): any {
    // for (let i = 0; this.objEditableGrid.tabledata.length; i++) {
    //   this.objEditableGrid.tabledata[i].selling_price = +this.objEditableGrid.tabledata[i].selling_price// this._decimalPipe.transform(this.objEditableGrid.tabledata[i].selling_price, '1.0-0')
    // }
    let price = this.objEditableGrid.tabledata.map(t => +t.selling_price).reduce((acc, value) => acc + value, 0);
    return this._decimalPipe.transform(price, '1.2-2');
  }
  getEditData(EditData: string) {
    this.OutData.emit(EditData);
  }

}
