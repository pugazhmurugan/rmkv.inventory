import { Component, OnInit } from '@angular/core';
import { LocalStorage } from '../../shared/local-storage';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { KeyPressEvents } from '../../shared/directives/key.press.events';
import { MessagesService } from '../messages/messages.service';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { MessagesComponent } from '../messages/messages.component';

@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.scss'],
  providers: [MessagesService, MessagesComponent]
})
export class MailComponent implements OnInit {

  objUser: any = {
    User_ID: '',
    User_Name: '',
    Message: '',
    Recicever_Details: []
  }

  tag: any[] = [];

  constructor(
    public _localStorage: LocalStorage,
    public _router: Router,
    public _matDialog: MatDialog,
    public _keyPressEvents: KeyPressEvents, private _confirmationDialog: ConfirmationDialogComponent
    , private _messagesService: MessagesService, public _message: MessagesComponent) { }

  ngOnInit() {
  }

  public onKeyPressSearch(keyUpEvent): void {
    if (keyUpEvent.keyCode === 13) {
      // this.openSerchTagLookUp();
    }
  }

  // public openSerchTagLookUp(): void {
  //   const dialogRef = this._matDialog.open(EmployeeAllUserLookUpComponent, {
  //     data: {
  //       searchString: this.objUser.User_Name,
  //     },
  //     panelClass: "custom-dialog-container",
  //   });
  //   dialogRef.afterClosed().subscribe((result: any) => {
  //     if (result) {
  //       let string = "";
  //       for (let i = 0; i < result.length; i++) {
  //         this.objUser.Recicever_Details.push(Object.assign({ receiver_id: result[i].user_id }));
  //         if (i == 0)
  //           string = string + result[i].user_name.toString()
  //         else
  //           string = string + "," + result[i].user_name.toString()
  //       }
  //       this.objUser.User_Name = string;
  //     }
  //   });
  // }

  public saveMessages(): void {
    if (this.beforeSendValidate()) {
      let objGet = {
        Notification: JSON.stringify([{
          sender_id: this._localStorage.intGlobalUserId(),
          message: this.objUser.Message.toString().trim(),
          receiver_details: this.objUser.Recicever_Details
        }])
      }
      this._messagesService.saveMessages(objGet).subscribe((result: any) => {
        if (result) {
          this._confirmationDialog.openAlertDialog('Mail send sucessfully', 'Mail');
          this.objUser.User_Name = '';
          this.objUser.Message = '';
        }
        debugger
        this._message.getMessages();
      });
    }
  }

  beforeSendValidate() {
    if (this.objUser.User_Name == '') {
      document.getElementById('to').focus();
      this._confirmationDialog.openAlertDialog('Enter to', 'Mail');
      return false;
    } else if (!this.objUser.Message.toString().trim()) {
      document.getElementById('textarea').focus();
      this._confirmationDialog.openAlertDialog('Enter messages', 'Mail');
      return false;
    } else return true;
  }

  public onlyAllowEnterKey(event: any): boolean {
    debugger;
    if (event.keyCode === 13)
      return true;
    else return false;
  }

}
