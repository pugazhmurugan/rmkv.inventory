import { Component, OnInit } from '@angular/core';
import { LocalStorage } from '../../shared/local-storage';
import { DatePipe } from '@angular/common';
import { DashboardService } from '../../services/dashboard/dashboard.service';
import { Rights } from '../../models/common-model';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [DatePipe]
})
export class DashboardComponent implements OnInit {

  public colorScheme = {
    domain: ['#999']
  };
  public colorScheme3 = {
    domain: ['#2F3E9E', '#D22E2E', '#378D3B', '#0096A6', '#F47B00']
  };
  public colorScheme1 = {
    domain: ['#2F3E9E', '#D22E2E', '#378D3B', '#0096A6', '#F47B00', '#606060']
  };
  public colorScheme4 = {
    domain: ['#D22E2E', '#0096A6', '#378D3B', '#F47B00', '#606060']
  };

  public autoScale = true;
  public showXAxis = true;
  public showYAxis = true;
  public gradient = false;
  public showXAxisLabel = false;
  public xAxisLabel = 'Year';
  public showYAxisLabel = false;
  public yAxisLabel = 'Profit';
  public roundDomains = true;
  public showLabels = true;
  public explodeSlices = true;
  public doughnut = false;
  public showLegend: boolean = true;
  public legendTitle = '';

  public totalBalesData: any[] = [];
  public infoBoardData: any[] = [];
  public inwardBalesData: any[] = [];
  public outwardBalesData: any[] = [];
  public goodTransferData: any[] = [];
  public balesStockData: any[] = [];

  rights: Rights = {
    Add: false,
    Update: false,
    Delete: false,
    View: false
  };

  constructor(
    private _localStorage: LocalStorage,
    private _dashboardService: DashboardService,
    public _datePipe: DatePipe,
  ) {
    this.getGRNDashboard();
  }

  ngOnInit() {
    // this.getRights();
    // this.getGRNDashboard(); 
  }

  private getRights() {
    var rights = this._localStorage.getMenuRights("/Dashboard");
    for (var x in rights) {
      if (rights[x].Right_Name == "View" && rights[x].Status == true) {
        this.rights.View = true;
      }
    }
  }

  private getGRNDashboard(): void {
    let objGet = {
      warehouse_id: +this._localStorage.getGlobalWarehouseId()
    }
    this._dashboardService.getGRNDashboard(objGet).subscribe((result: any) => {
      if (result.RETURN_VALUE === 1) {
        if (result.inwardbales !== null)
          this.inwardBalesData = JSON.parse(result.inwardbales);
        if (result.outwardbales !== null)
          this.outwardBalesData = JSON.parse(result.outwardbales);
        if (result.transfers !== null)
          this.goodTransferData = JSON.parse(result.transfers);
        if (result.stock !== null)
          this.balesStockData = JSON.parse(result.stock);
        if (result.counts !== null)
          this.totalBalesData = JSON.parse(result.counts);
        if (result.events !== null)
          this.infoBoardData = JSON.parse(result.events);
      }
    });
  }
}

export class GRNKeys {
  name: string;
  value: string;
}


