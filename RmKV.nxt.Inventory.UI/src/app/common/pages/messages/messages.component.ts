import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MessagesService } from './messages.service';
import { LocalStorage } from '../../shared/local-storage';
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MessagesService]
})
export class MessagesComponent implements OnInit {

  @ViewChild(MatMenuTrigger, null) trigger: MatMenuTrigger;
  public selectedTab: number = 1;
  public messages: any;
  public files: Array<Object>;
  public meetings: Array<Object>;
  public userImage = '../assets/img/profile/adam.jpg';
  badge: any;

  constructor(private _messagesService: MessagesService, public _localStorage: LocalStorage) {
    // this.getMessages();
  }

  ngOnInit() {
  }

  public getMessages(): void {
    let objGet = {
      Notification: JSON.stringify([{
        receiver_id: this._localStorage.intGlobalUserId()
      }])
    }
    this._messagesService.getMessages(objGet).subscribe((result: any) => {
      if (result) {
        debugger;
        let tempObj = JSON.parse(result);
        this.messages = tempObj;
        this.badge = this.messages.length;
      }
    });
  }

  public saveMessages(): void {
    let objGet = {
      Notification: JSON.stringify([{
        notification_id: this.messages[0].notification_id
      }])
    }
    this._messagesService.ViewMessages(objGet).subscribe((result: any) => {
      if (result == 1) {
        this.badge = '';
      }
    });
  }

  openMessagesMenu() {
    this.trigger.openMenu();
    this.selectedTab = 0;
  }

  onMouseLeave() {
    this.trigger.closeMenu();
  }

  stopClickPropagate(event: any) {
    event.stopPropagation();
    event.preventDefault();
  }

}