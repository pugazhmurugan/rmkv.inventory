import { Injectable } from '@angular/core'
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { LocalStorage } from '../../shared/local-storage';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class MessagesService {
    constructor(private _http: HttpClient, private _localStorage: LocalStorage) { }

    private messages = [
        {
            name: 'ashley',
            text: 'After you get up and running, you can place Font Awesome icons just about...',
            time: '1 min ago'
        },
        {
            name: 'michael',
            text: 'You asked, Font Awesome delivers with 40 shiny new icons in version 4.2.',
            time: '2 hrs ago'
        },
        {
            name: 'julia',
            text: 'Want to request new icons? Here\'s how. Need vectors or want to use on the...',
            time: '10 hrs ago'
        },
        {
            name: 'bruno',
            text: 'Explore your passions and discover new ones by getting involved. Stretch your...',
            time: '1 day ago'
        },
        {
            name: 'tereza',
            text: 'Get to know who we are - from the inside out. From our history and culture, to the...',
            time: '1 day ago'
        },
        {
            name: 'adam',
            text: 'Need some support to reach your goals? Apply for scholarships across...',
            time: '2 days ago'
        },
        {
            name: 'michael',
            text: 'Wrap the dropdown\'s trigger and the dropdown menu within .dropdown, or...',
            time: '1 week ago'
        }
    ];

    getMessages(notification: any) {
        return this._http.post(this._localStorage.getApiUrl() + "Shared/FetchNotification", notification)
            .pipe(catchError(this.handleError));
    }

    saveMessages(notification: any) {
        return this._http.post(this._localStorage.getApiUrl() + "Shared/SaveNotification", notification)
            .pipe(catchError(this.handleError));
    }

    ViewMessages(notification: any) {
        return this._http.post(this._localStorage.getApiUrl() + "Shared/ViewNotification", notification)
            .pipe(catchError(this.handleError));
    }

    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.log("Client Side Error =" + error.message);
        } else {
            console.log("Server Side Error=" + error.message);
        }
        return throwError(error);
    }

}
