import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductGroupLookupComponent } from './product-group-lookup.component';

describe('ProductGroupLookupComponent', () => {
  let component: ProductGroupLookupComponent;
  let fixture: ComponentFixture<ProductGroupLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductGroupLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductGroupLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
