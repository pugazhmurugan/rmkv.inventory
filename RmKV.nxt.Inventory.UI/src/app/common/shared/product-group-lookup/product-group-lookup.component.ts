import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductGroupAttributesService } from 'src/app/module/Inventory/services/masters/product-group-attributes/product-group-attributes.service';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-product-group-lookup',
  templateUrl: './product-group-lookup.component.html',
  styleUrls: ['./product-group-lookup.component.scss']
})
export class ProductGroupLookupComponent implements OnInit {

  productGroupList: ProductGroupLookup[] = [];
  filteredProductGroupList: ProductGroupLookup[] = [];
  tempProductGroupListList: ProductGroupLookup[] = [];

  selectedProducts: ProductGroupLookup = {
    product_group_id : 0,
    product_group_name : "",
    product_group_desc : "",
    category_id : 0,
    category_name : "",
    product_group_uom : 0,
    unique_byno_prod_serial : 0,
    lsp : false
  };

  selectedList: any = [];
  searchString: string = "";
  selectedRowIndex: number = 0;
  constructor( public _dialogRef: MatDialogRef<ProductGroupLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: searchDialog,
    public _matDialog: MatDialog,
    private _productGroupAttributesService: ProductGroupAttributesService,
    private _keyPressEvents: KeyPressEvents,
    public _localStorage: LocalStorage
    ) { 
      this._dialogRef.disableClose = true;
    }

  ngOnInit() {
    this.getProductGroupList();
  }

  private getProductGroupList(): void {
    debugger;
    let objData: any = [{
      group_section_id : +this._localStorage.getGlobalGroupSectionId()
    }];
    let objDetails: any = {
      GetProductGroupLookup: JSON.stringify(objData)
    }
    this._productGroupAttributesService.GetProductGroupLookupDetails(objDetails).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0){
      debugger;
      this.productGroupList = JSON.parse(result);
      this.tempProductGroupListList = JSON.parse(result);
      this.filteredProductGroupList = JSON.parse(result);
      this.filterProduct(this._data);
      }
    });
  }

  onKeyDown(e: any) {
    debugger;
    e.preventDefault();
    var listElm = document.querySelector('tr');
    var selectedElm = document.activeElement,
      // map actions to event's key
      action = { ArrowUp: "previous", Up: "previous", ArrowDown: "next", Down: "next" }

    selectedElm = selectedElm[action[e.key] + "ElementSibling"];

    // loop when reached the top or bottom edge
    if (!selectedElm)
      selectedElm = listElm.children[e.key == 'ArrowUp' || e.key == 'Up' ? listElm.children.length - 1 : 0];
    selectedElm;
    // selectedElm.focus();
  }

  private filterProduct(dialogValue: searchDialog): void {
    dialogValue.searchString = !dialogValue.searchString ? "" : dialogValue.searchString.toLowerCase().trim();
    if (dialogValue.searchString) {
      this.tempProductGroupListList = this.tempProductGroupListList.filter((item: any) =>
        item.product_group_name.toLowerCase().startsWith(dialogValue.searchString));
    } if (dialogValue.searchString === "") {
      this.tempProductGroupListList = this.productGroupList;
    }
    this.filteredProductGroupList = this.tempProductGroupListList;
  }

  public searchGroup(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredTemp = [];
      if (searchString === "") { 
        filteredTemp = this.productGroupList;
      } else {
        filteredTemp = this.productGroupList.filter(item =>
          item.product_group_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
      }
      if (filteredTemp.length > 0)
        this.selectedRowIndex = 0;
      this.filteredProductGroupList = filteredTemp;
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      this.selectedProducts = this.filteredProductGroupList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(this.filteredProductGroupList[this.selectedRowIndex]);
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.filteredProductGroupList);
    if (this.filteredProductGroupList)
      this.selectedProducts = this.filteredProductGroupList[this.selectedRowIndex];
  }

  public onDoubleClick(selectedProductRecord: any): void {
    this.selectedProducts = selectedProductRecord;
    this._dialogRef.close(this.selectedProducts);
  }

  public onClick(selectedAttributeRecord: any, event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.selectedProducts = selectedAttributeRecord;
      this._dialogRef.close(this.selectedProducts);
    }
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }
  
}

export class ProductGroupLookup {
  product_group_id : number;
  product_group_name : string;
  product_group_desc : string;
  category_id : number;
  category_name : string;
  product_group_uom : number;
  unique_byno_prod_serial : number;
  lsp :boolean
 }

export class searchDialog {
  searchString: string;
  group_section_id : number;
}