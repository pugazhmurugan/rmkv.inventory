import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductGroupAttributesService } from 'src/app/module/Inventory/services/masters/product-group-attributes/product-group-attributes.service';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-attribute-lookup',
  templateUrl: './attribute-lookup.component.html',
  styleUrls: ['./attribute-lookup.component.scss']
})
export class AttributeLookupComponent implements OnInit {

  attributeList: AttributeLookup[] = [];
  filteredAttributeList: AttributeLookup[] = [];
  tempAttributeList: AttributeLookup[] = [];

  selectedAttributes: AttributeLookup = {
    attribute_id : 0,
    attribute_name : "",
    // attribute_value_id : 0,
    // attribute_value : ""
  };

  selectedList: any = [];
  searchString: string = "";
  selectedRowIndex: number = 0;

  constructor(
    public _dialogRef: MatDialogRef<AttributeLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: searchDialog,
    public _matDialog: MatDialog,
    private _productGroupAttributesService: ProductGroupAttributesService,
    private _keyPressEvents: KeyPressEvents,
    public _localStorage: LocalStorage
  ) {
    this._dialogRef.disableClose = true;
   }

  ngOnInit() {
    this.getAttributesList();
  }

  private getAttributesList(): void {
    debugger;
    let objData: any = [{
      group_section_id : +this._localStorage.getGlobalGroupSectionId()
    }];
    let objDetails: any = {
      GetAttributeLookup: JSON.stringify(objData)
    }
    this._productGroupAttributesService.getAttributesLookup(objDetails).subscribe((result: any) => {
      if (JSON.parse(result) && JSON.parse(result).length > 0){
      debugger;
      this.attributeList = JSON.parse(result);
      this.tempAttributeList = JSON.parse(result);
      this.filteredAttributeList = JSON.parse(result);
      this.filterAttribute(this._data);
      }
    });
  }

  onKeyDown(e: any) {
    debugger;
    e.preventDefault();
    var listElm = document.querySelector('tr');
    var selectedElm = document.activeElement,
      // map actions to event's key
      action = { ArrowUp: "previous", Up: "previous", ArrowDown: "next", Down: "next" }

    selectedElm = selectedElm[action[e.key] + "ElementSibling"];

    // loop when reached the top or bottom edge
    if (!selectedElm)
      selectedElm = listElm.children[e.key == 'ArrowUp' || e.key == 'Up' ? listElm.children.length - 1 : 0];
    selectedElm;
    // selectedElm.focus();
  }

  private filterAttribute(dialogValue: searchDialog): void {
    dialogValue.searchString = !dialogValue.searchString ? "" : dialogValue.searchString.toLowerCase().trim();
    if (dialogValue.searchString) {
      this.tempAttributeList = this.tempAttributeList.filter((item: any) =>
        item.attribute_name.toLowerCase().startsWith(dialogValue.searchString));
        // item.template_name.toLowerCase().startsWith(dialogValue.searchString));
    } if (dialogValue.searchString === "") {
      this.tempAttributeList = this.attributeList;
    }
    this.filteredAttributeList = this.tempAttributeList;
  }

  public searchAttribute(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredTemp = [];
      if (searchString === "") { 
        filteredTemp = this.attributeList;
      } else {
        filteredTemp = this.attributeList.filter(item =>
          item.attribute_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
          // || item.template_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
      }
      if (filteredTemp.length > 0)
        this.selectedRowIndex = 0;
      this.filteredAttributeList = filteredTemp;
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      this.selectedAttributes = this.filteredAttributeList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(this.filteredAttributeList[this.selectedRowIndex]);
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.filteredAttributeList);
    if (this.filteredAttributeList)
      this.selectedAttributes = this.filteredAttributeList[this.selectedRowIndex];
  }

  public onDoubleClick(selectedTemplateRecord: any): void {
    this.selectedAttributes = selectedTemplateRecord;
    this._dialogRef.close(this.selectedAttributes);
  }

  public onClick(selectedAttributeRecord: any, event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.selectedAttributes = selectedAttributeRecord;
      this._dialogRef.close(this.selectedAttributes);
    }
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

}

export class AttributeLookup {
  attribute_id : number;
  attribute_name : string;
//   attribute_value_id : number;
//   attribute_value : string;
 }

export class searchDialog {
  searchString: string;
  group_section_id : number;
}