import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { InvoiceDetailsService } from 'src/app/module/Inventory/services/purchases/invoice-prod-details/invoice-details.service';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';
import { ProductLookupComponent } from '../product-lookup/product-lookup.component';

@Component({
  selector: 'app-invoice-attributes-lookup',
  templateUrl: './invoice-attributes-lookup.component.html',
  styleUrls: ['./invoice-attributes-lookup.component.scss']
})
export class InvoiceAttributesLookupComponent implements OnInit {


  objLookup = {
    product_group_id: 0,
    product_group_desc: '',
    product_uom: '',
    apply_check: false
  }
  attribute_value: number = 0;
  tempProductList: any;
  ProductList: any = [{
    product_code: '',
    product_name: '',
    product_attributes: [{
      attribute_name: '',
      value_data: [{
        attribute_value_id: '',
        attribute_value: ''
      }]
    }]
  }];
  selectedRowIndex: number = 0;
  selectedProduct: any = [];
  filterProductList: any;
  attributesList: any = [];
  productDetails: any;
  maxrow: any;
  tempAttributesData: any[] = [];
  type_id: number = 0;
  type_value_id: number = 0;
  brand_id: number = 0;
  brand_value_id: number = 0;
  TypewiseProductList: any;
  productCode: string = '';
  apply_check: boolean = false;
  attributeValue: any;
  constructor(public _dialogRef: MatDialogRef<ProductLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    public _keyPressEvents: KeyPressEvents,
    public _productLookupService: InvoiceDetailsService,
    public _localStorage: LocalStorage, public _confirmationDialogComponent: ConfirmationDialogComponent,) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.objLookup.product_group_desc = this._data.product_group_desc;
    this.objLookup.product_uom = this._data.product_uom;
    this.objLookup.product_group_id = this._data.product_group_id;
    this.productCode = this._data.searchString;
    this.attributeValue = this._data.attributeValue;
    console.log(this.attributeValue, 'result');
    this.getInvoiceProdAttributes();
  }
  assignAttributeValue() {
    for (let i = 0; i < this.attributeValue.length; i++) {
      this.attributesList[i].selectedvalue = this.attributeValue[i].attribute_value;
    }
  }
  public GetProductDetailsByBrand(): void {
    debugger;
    // this.type_id = this.attributesList[0].attribute_id;
    // this.type_value_id = this.attributesList[0].attribute_value_id;
    // this.brand_id = this.attributesList[1].attribute_id;
    // this.brand_value_id = this.attributesList[1].attribute_value_id;

    this.type_id = this.attributesList[0].attribute_id;
    let index = this.attributesList[0].value_data.findIndex(e => e.attribute_value == this.attributesList[0].selectedvalue);
    this.type_value_id = this.attributesList[0].value_data[index].attribute_value_id;

    this.brand_id = this.attributesList[1].attribute_id;
    let index1 = this.attributesList[1].value_data.findIndex(e => e.attribute_value == this.attributesList[1].selectedvalue);
    this.brand_value_id = this.attributesList[1].value_data[index1].attribute_value_id;



    if (this.brand_id && this.type_id) {
      let objData = {
        InvoiceProdDetails: JSON.stringify([{
          company_section_id: +this._localStorage.getCompanySectionId(),
          type_id: +this.type_id,
          type_value_id: +this.type_value_id,
          brand_id: +this.brand_id,
          brand_value_id: +this.brand_value_id
        }])
      }
      this._productLookupService.getProductDetailsByBrandType(objData).subscribe((result: any) => {
        if (result) {
          this.TypewiseProductList = result;
          this.productCode = this.TypewiseProductList[0].product_code;
          console.log(result, 'efv')
        }
      });
    }
  }
  private getInvoiceProdAttributes(): void {
    debugger;
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        product_group_id: +this.objLookup.product_group_id
      }])
    }
    this._productLookupService.getInvoiceProdAttributes(objData).subscribe((result: any) => {
      if (result) {
        for (let i = 0; i < result.length; i++) {
          this.attributesList.push({
            attribute_id: result[i].attribute_id,
            attribute_name: result[i].attribute_name,
            value_data: result[i].value_data,
            selectedvalue: []// result[i].value_data[0].attribute_value
          })
        }
        this.assignAttributeValue();
        if (this._data.viewFlag)
          this.GetProductDetailsByBrand();
      }
    });
  }

  private getProductLookupDetails(i): void {
    debugger;
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        product_group_id: +this.objLookup.product_group_id,
        product_code: this.filterProductList[i].product_code
      }])
    }
    this._productLookupService.getProductLookupDetails(objData).subscribe((result: any) => {
      if (result) {
        //this.productDetails = result;
        this.selectedProduct = result;
        this.selectedProduct.push(Object.assign({ checkbox: this.objLookup.apply_check }));
        this._dialogRef.close(this.selectedProduct);
      }
    });
  }

  onKeyDown(e: any) {
    debugger;
    e.preventDefault();
    var listElm = document.querySelector('tr');
    var selectedElm = document.activeElement,
      // map actions to event's key
      action = { ArrowUp: "previous", Up: "previous", ArrowDown: "next", Down: "next" }

    selectedElm = selectedElm[action[e.key] + "ElementSibling"];

    // loop when reached the top or bottom edge
    if (!selectedElm)
      selectedElm = listElm.children[e.key == 'ArrowUp' || e.key == 'Up' ? listElm.children.length - 1 : 0];
    selectedElm;
    // selectedElm.focus();
  }


  private filterProduct(dialogValue: any): void {
    debugger
    dialogValue.searchString = !dialogValue.searchString ? "" : dialogValue.searchString.toLowerCase().trim();
    if (dialogValue.searchString) {
      this.tempProductList = this.tempProductList.filter((item: any) =>
        item.product_code.toLowerCase().includes(dialogValue.searchString) ||
        item.product_name.toLowerCase().includes(dialogValue.searchString));
    } if (dialogValue.searchString === "") {
      this.tempProductList = this.ProductList;
    }
    this.filterProductList = this.tempProductList;
  }

  public searchProduct(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredProducts = [];
      if (searchString === "") {
        filteredProducts = this.ProductList;
      } else {
        filteredProducts = this.ProductList.filter(item =>
          item.product_code.toLowerCase().includes(searchString.trim().toLowerCase()) ||
          item.product_name.toLowerCase().includes(searchString.trim().toLowerCase()));
      }
      if (filteredProducts.length > 0)
        this.selectedRowIndex = 0;
      this.filterProductList = filteredProducts;
    }
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.tempProductList);
    if (this.tempProductList)
      this.selectedProduct = this.tempProductList[this.selectedRowIndex];
    this.scrollTo(this.selectedRowIndex);
  }
  private scrollTo(index: number) {
    let elmnt = document.getElementById("row" + index);
    elmnt.scrollIntoView(false);
    window.scrollTo(0, 0); // only if it's innerhtml
  }


  public onDoubleClick(i: number): void {
    debugger
    this.tempAttributesData = this.tempAttributesData.sort((a, b) => (a.attribute_id > b.attribute_id) ? 1 : -1)
    if (this.attributesList.length <= this.tempAttributesData.length) {
      this._dialogRef.disableClose = false;
      this._dialogRef.close(this.tempAttributesData);
    }
    //  else {
    //   this._confirmationDialogComponent.openAlertDialog('Select all attributes', 'Invoice Product Details')
    // }
  }

  public onClick(selectedProductRecord: any, event: KeyboardEvent | any): void {
    debugger
    if (event.keyCode === 13) {
      this.tempAttributesData = this.tempAttributesData.sort((a, b) => (a.attribute_id > b.attribute_id) ? 1 : -1)
      if (this.attributesList.length <= this.tempAttributesData.length) {
        this._dialogRef.disableClose = false;
        this._dialogRef.close(this.tempAttributesData);
      } else {
        this._confirmationDialogComponent.openAlertDialog('Select all attributes', 'Invoice Product Details')
      }
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    debugger
    if (event.keyCode === 13) {
      this.tempAttributesData = this.tempAttributesData.sort((a, b) => (a.attribute_id > b.attribute_id) ? 1 : -1)
      if (this.attributesList.length <= this.tempAttributesData.length) {
        this._dialogRef.disableClose = false;
        this._dialogRef.close(this.tempAttributesData);
      }
      // else {
      //   this.openAlertDialog('Select all attributes', 'Invoice Product Details')
      // }
    }
  }

  public dialogOK(): void {
    let selectedRow = [];
    this.tempAttributesData = this.tempAttributesData.sort((a, b) => (a.attribute_id > b.attribute_id) ? 1 : -1);
    if (this._data.viewFlag == true) {
      this.tempAttributesData = [];
      for (let j = 0; j < this.attributesList.length; j++) {
        if (this.attributesList[j].selectedvalue == '') {
          this._confirmationDialogComponent.openAlertDialog("Select" + " " + this.attributesList[j].attribute_name + "", "Invoice Product Details");
          return
        } else {
          for (let k = 0; k < this.attributesList[j].value_data.length; k++) {
            if (this.attributesList[j].value_data[k].attribute_value == this.attributesList[j].selectedvalue) {
              this.tempAttributesData.push(this.attributesList[j].value_data[k]);
            }
          }
        }
      }
    } else {
      for (let j = 0; j < this.attributesList.length; j++) {
        let index = this.tempAttributesData.findIndex(e => +e.attribute_id == + this.attributesList[j].attribute_id);
        if (index == -1) {
          this._confirmationDialogComponent.openAlertDialog("Select" + " " + this.attributesList[j].attribute_name + "", "Invoice Product Details");
          return
        }
      }
    }
    this.tempAttributesData = this.tempAttributesData.sort((a, b) => (a.attribute_id > b.attribute_id) ? 1 : -1);
    if (this.productCode !== '') {
      if (this.attributesList.length <= this.tempAttributesData.length) {
        this._dialogRef.disableClose = false;
        selectedRow.push(this.tempAttributesData);
        selectedRow.push(Object.assign({ productDetails: this.TypewiseProductList }));
        selectedRow.push(Object.assign({ applyCheck: this.apply_check }));
        this._dialogRef.close(selectedRow);
        console.log(selectedRow, 'selectedRow')
      } else {
        this._confirmationDialogComponent.openAlertDialog('Select all attributes', 'Invoice Product Details')
      }
    } else {
      this._confirmationDialogComponent.openAlertDialog('Select product code', 'Invoice Product Details')
    }
  }
  openAlertDialog(value: string, componentName?: string) {
    //  this._matDialog.closeAll();
    let _dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 0 }
    });
    _dialogRef.componentInstance.alertMessage = value;
    _dialogRef.componentInstance.componentName = componentName;
    _dialogRef.afterClosed().subscribe(result => {
      if (result)
        _dialogRef = null;
    });
  }
  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

  filterAttributesValue(data: any, j) {
    if (data.selectedvalue !== '') {
      for (let i = 0; i < this.attributesList[j].value_data.length; i++) {
        if (this.attributesList[j].value_data[i].attribute_id == data.attribute_id && this.attributesList[j].value_data[i].attribute_value == data.selectedvalue) {
          if (this.tempAttributesData.length !== 0) {
            let index = this.tempAttributesData.findIndex(e => +e.attribute_id == +data.attribute_id);
            if (index == -1) {
              this.tempAttributesData.push(this.attributesList[j].value_data[i]);
            } else {
              this.tempAttributesData.splice(index, 1);
              this.tempAttributesData.push(this.attributesList[j].value_data[i]);
            }
          } else {
            this.tempAttributesData.push(this.attributesList[j].value_data[i]);
          }
        }
      }
    }
    this.GetProductDetailsByBrandType(data, j);
  }
  public GetProductDetailsByBrandType(data, j): void {
    debugger;
    for (let i = 0; i < this.attributesList[j].value_data.length; i++) {
      if (this.attributesList[j].value_data[i].attribute_id == data.attribute_id && this.attributesList[j].value_data[i].attribute_value == data.selectedvalue) {
        if (j == 0) {
          this.type_id = this.attributesList[j].attribute_id
          let index = this.attributesList[j].value_data.findIndex(e => e.attribute_value == data.selectedvalue);
          this.type_value_id = this.attributesList[j].value_data[index].attribute_value_id;
        } else if (j == 1) {
          this.brand_id = this.attributesList[j].attribute_id;
          let index = this.attributesList[j].value_data.findIndex(e => e.attribute_value == data.selectedvalue);
          this.brand_value_id = this.attributesList[j].value_data[index].attribute_value_id;

        }
      }
    }
    // if (this.type_id == 0) {
    //   this._confirmationDialogComponent.openAlertDialog("Select type", "Invoice Product Details");
    //   return;
    // }
    if (this.brand_id && this.type_id) {
      if (j == 1) {
        let objData = {
          InvoiceProdDetails: JSON.stringify([{
            company_section_id: +this._localStorage.getCompanySectionId(),
            type_id: +this.type_id,
            type_value_id: +this.type_value_id,
            brand_id: +this.brand_id,
            brand_value_id: +this.brand_value_id
          }])
        }
        this._productLookupService.getProductDetailsByBrandType(objData).subscribe((result: any) => {
          if (result) {
            this.TypewiseProductList = result;
            this.productCode = this.TypewiseProductList[0].product_code;
            console.log(result, 'efv')
          }
        });
      }
    }
  }
  onChangeAttributes(data: any, j) {
    debugger
    let brand = this.tempAttributesData.findIndex(e => +e.attribute_id == 11);
    if (brand == -1) {
      this._confirmationDialogComponent.openAlertDialog("Select brand" + "", "Invoice Product Details");
      return;
    }
    // let type = this.tempAttributesData.findIndex(e => +e.attribute_id == 12);
    // if (type == -1) {
    //   this._confirmationDialogComponent.openAlertDialog("Select type" + "", "Invoice Product Details");
    //   return;
    // }
    let attribute_id = this.attributesList[j - 1].attribute_id;
    let attribute_name = this.attributesList[j - 1].attribute_name;
    let index = this.tempAttributesData.findIndex(e => +e.attribute_id == +attribute_id);
    if (index == -1)
      this._confirmationDialogComponent.openAlertDialog("Select" + " " + attribute_name + "", "Invoice Product Details");
  }
}
