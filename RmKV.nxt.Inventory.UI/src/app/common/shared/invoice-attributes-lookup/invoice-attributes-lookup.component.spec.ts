import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceAttributesLookupComponent } from './invoice-attributes-lookup.component';

describe('InvoiceAttributesLookupComponent', () => {
  let component: InvoiceAttributesLookupComponent;
  let fixture: ComponentFixture<InvoiceAttributesLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceAttributesLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceAttributesLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
