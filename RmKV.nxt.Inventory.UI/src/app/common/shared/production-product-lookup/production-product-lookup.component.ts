import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductionProductsService } from 'src/app/module/Inventory/services/masters/Production-Product/production-products.service';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-production-product-lookup',
  templateUrl: './production-product-lookup.component.html',
  styleUrls: ['./production-product-lookup.component.scss']
})
export class ProductionProductLookupComponent implements OnInit {

  ProductNameList: ProductNameLookup[] = [];
  filteredProductNameList: ProductNameLookup[] = [];
  tempProductNameList: ProductNameLookup[] = [];

  selectedProducts: ProductNameLookup = {
    prod_id: 0,
    prod_name: '',
    prod_size: 0,
    special_product: false,
    active: true,
  };

  selectedList: any = [];
  searchString: string = "";
  selectedRowIndex: number = 0;

  constructor(
    public _dialogRef: MatDialogRef<ProductionProductLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: searchDialog,
    public _matDialog: MatDialog,
    public _productionProductsService: ProductionProductsService,
    private _keyPressEvents: KeyPressEvents,
    public _localStorage: LocalStorage
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getProductNameList();
  }

  private getProductNameList(): void {
    this._productionProductsService.getProductionProductLookup().subscribe((result: any) => {
      if (result && result.length > 0) {
        this.ProductNameList = result;
        this.tempProductNameList = result;
        this.filteredProductNameList = result;
        this.filterProduct(this._data);
      }
    });
  }

  onKeyDown(e: any) {
    debugger;
    e.preventDefault();
    var listElm = document.querySelector('tr');
    var selectedElm = document.activeElement,
      action = { ArrowUp: "previous", Up: "previous", ArrowDown: "next", Down: "next" }
    selectedElm = selectedElm[action[e.key] + "ElementSibling"];
    if (!selectedElm)
      selectedElm = listElm.children[e.key == 'ArrowUp' || e.key == 'Up' ? listElm.children.length - 1 : 0];
    selectedElm;
  }

  private filterProduct(dialogValue: searchDialog): void {
    dialogValue.searchString = !dialogValue.searchString ? "" : dialogValue.searchString.toLowerCase().trim();
    if (dialogValue.searchString) {
      this.tempProductNameList = this.tempProductNameList.filter((item: any) =>
        item.prod_name.toLowerCase().startsWith(dialogValue.searchString));
    } if (dialogValue.searchString === "") {
      this.tempProductNameList = this.ProductNameList;
    }
    this.filteredProductNameList = this.tempProductNameList;
  }

  public searchProduct(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredTemp = [];
      if (searchString === "") {
        filteredTemp = this.ProductNameList;
      } else {
        filteredTemp = this.ProductNameList.filter(item =>
          item.prod_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
      }
      if (filteredTemp.length > 0)
        this.selectedRowIndex = 0;
      this.filteredProductNameList = filteredTemp;
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      this.selectedProducts = this.filteredProductNameList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(this.filteredProductNameList[this.selectedRowIndex]);
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.filteredProductNameList);
    if (this.filteredProductNameList)
      this.selectedProducts = this.filteredProductNameList[this.selectedRowIndex];
  }

  public onDoubleClick(selectedProductRecord: any): void {
    this.selectedProducts = selectedProductRecord;
    this._dialogRef.close(this.selectedProducts);
  }

  public onClick(selectedAttributeRecord: any, event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.selectedProducts = selectedAttributeRecord;
      this._dialogRef.close(this.selectedProducts);
    }
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

}

export class ProductNameLookup {
  prod_id: number;
  prod_name: string;
  prod_size: number;
  special_product: boolean;
  active: boolean;
}

export class searchDialog {
  searchString: string;
}