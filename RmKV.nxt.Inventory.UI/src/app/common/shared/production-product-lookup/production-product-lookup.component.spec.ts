import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionProductLookupComponent } from './production-product-lookup.component';

describe('ProductionProductLookupComponent', () => {
  let component: ProductionProductLookupComponent;
  let fixture: ComponentFixture<ProductionProductLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionProductLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionProductLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
