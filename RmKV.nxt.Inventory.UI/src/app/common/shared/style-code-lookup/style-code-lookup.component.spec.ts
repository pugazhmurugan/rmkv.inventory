import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleCodeLookupComponent } from './style-code-lookup.component';

describe('StyleCodeLookupComponent', () => {
  let component: StyleCodeLookupComponent;
  let fixture: ComponentFixture<StyleCodeLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleCodeLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleCodeLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
