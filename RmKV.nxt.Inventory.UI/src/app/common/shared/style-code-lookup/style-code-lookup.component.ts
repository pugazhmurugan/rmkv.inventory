import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToolsService } from '../../services/Tools/tools.service';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-style-code-lookup',
  templateUrl: './style-code-lookup.component.html',
  styleUrls: ['./style-code-lookup.component.scss']
})
export class StyleCodeLookupComponent implements OnInit {

  StyleCodeList: any[] = [];
  filteredStyleCodeList: any[] = [];
  tempStyleCodeList: any[] = [];

  selectedStyleCode: any = {
    // StyleCode_id: 0,
    // StyleCode_name: "",
    // StyleCode_value_id : 0,
    // StyleCode_value : ""
  };

  selectedList: any = [];
  searchString: string = "";
  selectedRowIndex: number = 0;

  constructor(
    public _dialogRef: MatDialogRef<StyleCodeLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    private _keyPressEvents: KeyPressEvents,
    public _localStorage: LocalStorage,
    public _StyleCodeService: ToolsService
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getStyleCodeList();
  }

  private getStyleCodeList(): void {
    let objdata = {
      ProductMaster: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._StyleCodeService.getStyleChangeLookup(objdata).subscribe((result: any[]) => {
      console.log(result, "list")
      this.StyleCodeList = result;
      this.tempStyleCodeList = result;
      this.filteredStyleCodeList = result;
      this.filterStyleCode(this._data);
    });
  }

  onKeyDown(e: any) {
    debugger;
    e.preventDefault();
    var listElm = document.querySelector('tr');
    var selectedElm = document.activeElement,
      // map actions to event's key
      action = { ArrowUp: "previous", Up: "previous", ArrowDown: "next", Down: "next" }

    selectedElm = selectedElm[action[e.key] + "ElementSibling"];

    // loop when reached the top or bottom edge
    if (!selectedElm)
      selectedElm = listElm.children[e.key == 'ArrowUp' || e.key == 'Up' ? listElm.children.length - 1 : 0];
    selectedElm;
    // selectedElm.focus();
  }

  private filterStyleCode(dialogValue: any): void {
    dialogValue.searchString = !dialogValue.searchString ? "" : dialogValue.searchString.toLowerCase().trim();
    if (dialogValue.searchString) {
      this.tempStyleCodeList = this.tempStyleCodeList.filter((item: any) =>
        item.StyleCode_name.toLowerCase().startsWith(dialogValue.searchString));
      // item.template_name.toLowerCase().startsWith(dialogValue.searchString));
    } if (dialogValue.searchString === "") {
      this.tempStyleCodeList = this.StyleCodeList;
    }
    this.filteredStyleCodeList = this.tempStyleCodeList;
  }

  public searchStyleCode(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredTemp = [];
      if (searchString === "") {
        filteredTemp = this.StyleCodeList;
      } else {
        filteredTemp = this.StyleCodeList.filter(item =>
          item.StyleCode_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
        // || item.template_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
      }
      if (filteredTemp.length > 0)
        this.selectedRowIndex = 0;
      this.filteredStyleCodeList = filteredTemp;
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      this.selectedStyleCode = this.filteredStyleCodeList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(this.filteredStyleCodeList[this.selectedRowIndex]);
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.filteredStyleCodeList);
    if (this.filteredStyleCodeList)
      this.selectedStyleCode = this.filteredStyleCodeList[this.selectedRowIndex];
  }

  public onDoubleClick(selectedTemplateRecord: any): void {
    this.selectedStyleCode = selectedTemplateRecord;
    this._dialogRef.close(this.selectedStyleCode);
  }

  public onClick(selectedStyleCodeRecord: any, event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.selectedStyleCode = selectedStyleCodeRecord;
      this._dialogRef.close(this.selectedStyleCode);
    }
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

}

