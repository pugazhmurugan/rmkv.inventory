import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductGroupAttributesService } from 'src/app/module/Inventory/services/masters/product-group-attributes/product-group-attributes.service';
import { ProductionMaterialsService } from 'src/app/module/Inventory/services/masters/production-materials/production-materials.service';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-material-lookup',
  templateUrl: './material-lookup.component.html',
  styleUrls: ['./material-lookup.component.scss']
})
export class MaterialLookupComponent implements OnInit {

  productionMaterialList: MaterialNameLookup[] = [];
  filteredProductionMaterialList: MaterialNameLookup[] = [];
  tempProductionMaterialList: MaterialNameLookup[] = [];

  selectedProducts: MaterialNameLookup = {
    material_id : 0,
    material_name : "",
    uom_description : "",
  };

  selectedList: any = [];
  searchString: string = "";
  selectedRowIndex: number = 0;
  constructor( public _dialogRef: MatDialogRef<MaterialLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: searchDialog,
    public _matDialog: MatDialog,
    private _productionMaterialService: ProductionMaterialsService,
    private _keyPressEvents: KeyPressEvents,
    public _localStorage: LocalStorage
    ) { 
      this._dialogRef.disableClose = true;
    }

  ngOnInit() {
    this.getMaterialNameList();
  }

  private getMaterialNameList(): void {
    debugger;
    // let objDetails: any = {
    //   GetProductionMaterialLookup: JSON.stringify
    // }
    this._productionMaterialService.getProductionMaterialDetails().subscribe((result: any) => {
      if (result){
      debugger;
      this.productionMaterialList = result.filter(x => x.active === true);
      this.tempProductionMaterialList = result.filter(x => x.active === true);
      this.filteredProductionMaterialList = result.filter(x => x.active === true);
      this.filterProduct(this._data);
      }
    });
  }

  onKeyDown(e: any) {
    debugger;
    e.preventDefault();
    var listElm = document.querySelector('tr');
    var selectedElm = document.activeElement,
      // map actions to event's key
      action = { ArrowUp: "previous", Up: "previous", ArrowDown: "next", Down: "next" }

    selectedElm = selectedElm[action[e.key] + "ElementSibling"];

    // loop when reached the top or bottom edge
    if (!selectedElm)
      selectedElm = listElm.children[e.key == 'ArrowUp' || e.key == 'Up' ? listElm.children.length - 1 : 0];
    selectedElm;
    // selectedElm.focus();
  }

  private filterProduct(dialogValue: searchDialog): void {
    dialogValue.searchString = !dialogValue.searchString ? "" : dialogValue.searchString.toLowerCase().trim();
    if (dialogValue.searchString) {
      this.tempProductionMaterialList = this.tempProductionMaterialList.filter((item: any) =>
        item.material_name.toLowerCase().startsWith(dialogValue.searchString));
    } if (dialogValue.searchString === "") {
      this.tempProductionMaterialList = this.productionMaterialList;
    }
    this.filteredProductionMaterialList = this.tempProductionMaterialList;
  }

  public searchGroup(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredTemp = [];
      if (searchString === "") { 
        filteredTemp = this.productionMaterialList;
      } else {
        filteredTemp = this.productionMaterialList.filter(item =>
          item.material_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
      }
      if (filteredTemp.length > 0)
        this.selectedRowIndex = 0;
      this.filteredProductionMaterialList = filteredTemp;
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      this.selectedProducts = this.filteredProductionMaterialList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(this.filteredProductionMaterialList[this.selectedRowIndex]);
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.filteredProductionMaterialList);
    if (this.filteredProductionMaterialList)
      this.selectedProducts = this.filteredProductionMaterialList[this.selectedRowIndex];
  }

  public onDoubleClick(selectedProductRecord: any): void {
    this.selectedProducts = selectedProductRecord;
    this._dialogRef.close(this.selectedProducts);
  }

  public onClick(selectedAttributeRecord: any, event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.selectedProducts = selectedAttributeRecord;
      this._dialogRef.close(this.selectedProducts);
    }
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }
  
}

export class MaterialNameLookup {
  material_id : number;
  material_name : string;
  uom_description : string;
 }

export class searchDialog {
  searchString: string;
}
