import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialLookupComponent } from './material-lookup.component';

describe('ProductionMaterialLookupComponent', () => {
  let component: MaterialLookupComponent;
  let fixture: ComponentFixture<MaterialLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
