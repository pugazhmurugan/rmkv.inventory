import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { KeyPressEvents } from '../directives/key.press.events';

@Component({
  selector: 'app-invoice-prod-details-confirmation-dialog',
  templateUrl: './invoice-prod-details-confirmation-dialog.component.html',
  styleUrls: ['./invoice-prod-details-confirmation-dialog.component.scss']
})
export class InvoiceProdDetailsConfirmationDialogComponent implements OnInit {

  Row: number | any;

  constructor(public _dialogRef: MatDialogRef<InvoiceProdDetailsConfirmationDialogComponent>,
    public _matDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _confirmationDialog: ConfirmationDialogComponent,
    public _confirmDialogRef: MatDialogRef<ConfirmationDialogComponent>,
    public _keyPressEvents: KeyPressEvents
  ) {
    _dialogRef.disableClose = true;
  }

  ngOnInit() {
    console.log(+this._data.row);
  }
  public addReason(): any {
    if (this.beforesavevalidation()) {
      // localStorage.setItem('Reason', );
      this._dialogRef.close(this.Row);
    }
  }

  onCheckRow() {
    debugger
    if (+this._data.row < +this.Row) {
      document.getElementById('remarks').focus();
      this._confirmationDialog.openAlertDialog("Row count can't be greater than total row count", "Invoivce Product Details");
      this.Row = "";
      return false;
    } else
      return true;
  }
  private beforesavevalidation(): boolean {
    if ([null, undefined, 0].indexOf(this.Row) !== -1 || !this.Row.toString().trim()) {
      document.getElementById("remarks").focus();
      this._confirmationDialog.openAlertDialog('Enter Row', 'Invoivce Product Details');
      return false;
    } else {
      return true;
    }
  }


  public dialogClosed() {
    this._dialogRef.close(null);
  }
}
