import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceProdDetailsConfirmationDialogComponent } from './invoice-prod-details-confirmation-dialog.component';

describe('InvoiceProdDetailsConfirmationDialogComponent', () => {
  let component: InvoiceProdDetailsConfirmationDialogComponent;
  let fixture: ComponentFixture<InvoiceProdDetailsConfirmationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceProdDetailsConfirmationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceProdDetailsConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
