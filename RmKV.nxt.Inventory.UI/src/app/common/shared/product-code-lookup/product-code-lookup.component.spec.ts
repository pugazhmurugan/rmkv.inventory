import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCodeLookupComponent } from './product-code-lookup.component';

describe('ProductCodeLookupComponent', () => {
  let component: ProductCodeLookupComponent;
  let fixture: ComponentFixture<ProductCodeLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductCodeLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCodeLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
