import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductCode } from '../../models/product-code-lookup-model';
import { ToolsService } from '../../services/Tools/tools.service';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-product-code-lookup',
  templateUrl: './product-code-lookup.component.html',
  styleUrls: ['./product-code-lookup.component.scss']
})
export class ProductCodeLookupComponent implements OnInit {

  ProductCodeList: ProductCode[] = [];
  filteredProductCodeList: any[] = [];
  tempProductCodeList: any[] = [];

  selectedProductCode: any = {};

  selectedList: any = [];
  searchString: string = "";
  selectedRowIndex: number = 0;

  constructor(
    public _dialogRef: MatDialogRef<ProductCodeLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    private _keyPressEvents: KeyPressEvents,
    public _localStorage: LocalStorage,
    public _ProductCodeService: ToolsService
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getProductCodeList();
  }

  private getProductCodeList(): void {
    let objdata = {
      ProductMaster: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._ProductCodeService.getProductChangeLookup(objdata).subscribe((result: any[]) => {
      console.log(result, "list")
      this.ProductCodeList = result;
      this.tempProductCodeList = result;
      this.filteredProductCodeList = result;
      this.filterProductCode(this._data);
    });
  }

  onKeyDown(e: any) {
    debugger;
    e.preventDefault();
    var listElm = document.querySelector('tr');
    var selectedElm = document.activeElement,
      // map actions to event's key
      action = { ArrowUp: "previous", Up: "previous", ArrowDown: "next", Down: "next" }

    selectedElm = selectedElm[action[e.key] + "ElementSibling"];

    // loop when reached the top or bottom edge
    if (!selectedElm)
      selectedElm = listElm.children[e.key == 'ArrowUp' || e.key == 'Up' ? listElm.children.length - 1 : 0];
    selectedElm;
    // selectedElm.focus();
  }

  private filterProductCode(dialogValue: any): void {
    dialogValue.searchString = !dialogValue.searchString ? "" : dialogValue.searchString.toLowerCase().trim();
    if (dialogValue.searchString) {
      this.tempProductCodeList = this.tempProductCodeList.filter((item: any) =>
        item.product_code.toLowerCase().startsWith(dialogValue.searchString));
      // item.template_name.toLowerCase().startsWith(dialogValue.searchString));
    } if (dialogValue.searchString === "") {
      this.tempProductCodeList = this.ProductCodeList;
    }
    this.filteredProductCodeList = this.tempProductCodeList;
  }

  public searchProductCode(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredTemp = [];
      if (searchString === "") {
        filteredTemp = this.ProductCodeList;
      } else {
        filteredTemp = this.ProductCodeList.filter(item =>
          item.product_code.toLowerCase().startsWith(searchString.trim().toLowerCase()));
        // || item.template_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
      }
      if (filteredTemp.length > 0)
        this.selectedRowIndex = 0;
      this.filteredProductCodeList = filteredTemp;
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      this.selectedProductCode = this.filteredProductCodeList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(this.filteredProductCodeList[this.selectedRowIndex]);
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.filteredProductCodeList);
    if (this.filteredProductCodeList)
      this.selectedProductCode = this.filteredProductCodeList[this.selectedRowIndex];
  }

  public onDoubleClick(selectedTemplateRecord: any): void {
    this.selectedProductCode = selectedTemplateRecord;
    this._dialogRef.close(this.selectedProductCode);
  }

  public onClick(selectedProductCodeRecord: any, event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.selectedProductCode = selectedProductCodeRecord;
      this._dialogRef.close(this.selectedProductCode);
    }
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

}

