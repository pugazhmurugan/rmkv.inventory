import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterCodeLookupComponent } from './counter-code-lookup.component';

describe('CounterCodeLookupComponent', () => {
  let component: CounterCodeLookupComponent;
  let fixture: ComponentFixture<CounterCodeLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterCodeLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterCodeLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
