import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CounterCode } from '../../models/countercode-lookup-model';
import { ToolsService } from '../../services/Tools/tools.service';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-counter-code-lookup',
  templateUrl: './counter-code-lookup.component.html',
  styleUrls: ['./counter-code-lookup.component.scss']
})
export class CounterCodeLookupComponent implements OnInit {

  
  CounterCodeList: CounterCode[] = [];
  filteredCounterCodeList: any[] = [];
  tempCounterCodeList: any[] = [];

  selectedCounterCode: any = {
    // CounterCode_id: 0,
    // CounterCode_name: "",
    // // CounterCode_value_id : 0,
    // CounterCode_value : ""
  };

  selectedList: any = [];
  searchString: string = "";
  selectedRowIndex: number = 0;

  constructor(
    public _dialogRef: MatDialogRef<CounterCodeLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    private _keyPressEvents: KeyPressEvents,
    public _localStorage: LocalStorage,
    public _CounterCodeService: ToolsService
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getCounterCodeList();
  }

  private getCounterCodeList(): void {
    let objdata = {
      ProductMaster: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._CounterCodeService.getCounterChangeLookup(objdata).subscribe((result: any[]) => {
      console.log(result, "list")
      this.CounterCodeList = result;
      this.tempCounterCodeList = result;
      this.filteredCounterCodeList = result;
      this.filterCounterCode(this._data);
    });
  }

  onKeyDown(e: any) {
    debugger;
    e.preventDefault();
    var listElm = document.querySelector('tr');
    var selectedElm = document.activeElement,
      // map actions to event's key
      action = { ArrowUp: "previous", Up: "previous", ArrowDown: "next", Down: "next" }

    selectedElm = selectedElm[action[e.key] + "ElementSibling"];

    // loop when reached the top or bottom edge
    if (!selectedElm)
      selectedElm = listElm.children[e.key == 'ArrowUp' || e.key == 'Up' ? listElm.children.length - 1 : 0];
    selectedElm;
    // selectedElm.focus();
  }

  private filterCounterCode(dialogValue: any): void {
    dialogValue.searchString = !dialogValue.searchString ? "" : dialogValue.searchString.toLowerCase().trim();
    if (dialogValue.searchString) {
      this.tempCounterCodeList = this.tempCounterCodeList.filter((item: any) =>
        item.CounterCode_name.toLowerCase().startsWith(dialogValue.searchString));
      // item.template_name.toLowerCase().startsWith(dialogValue.searchString));
    } if (dialogValue.searchString === "") {
      this.tempCounterCodeList = this.CounterCodeList;
    }
    this.filteredCounterCodeList = this.tempCounterCodeList;
  }

  public searchCounterCode(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredTemp = [];
      if (searchString === "") {
        filteredTemp = this.CounterCodeList;
      } else {
        filteredTemp = this.CounterCodeList.filter(item =>
          item.counter_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
        // || item.template_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
      }
      if (filteredTemp.length > 0)
        this.selectedRowIndex = 0;
      this.filteredCounterCodeList = filteredTemp;
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      this.selectedCounterCode = this.filteredCounterCodeList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(this.filteredCounterCodeList[this.selectedRowIndex]);
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.filteredCounterCodeList);
    if (this.filteredCounterCodeList)
      this.selectedCounterCode = this.filteredCounterCodeList[this.selectedRowIndex];
  }

  public onDoubleClick(selectedTemplateRecord: any): void {
    this.selectedCounterCode = selectedTemplateRecord;
    this._dialogRef.close(this.selectedCounterCode);
  }

  public onClick(selectedCounterCodeRecord: any, event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.selectedCounterCode = selectedCounterCodeRecord;
      this._dialogRef.close(this.selectedCounterCode);
    }
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

}

