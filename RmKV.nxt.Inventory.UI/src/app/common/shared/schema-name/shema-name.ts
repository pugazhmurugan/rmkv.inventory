import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})

export class SchemaName {
  public data = {
    name: 'InvoiceData',
    version: 1,
    objectStoresMeta: [
      {
        store: 'invoice',
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
          { name: 'user_id', keypath: 'user_id', options: { unique: false } },
          { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
          { name: 'data', keypath: 'by_no', options: { unique: false } },
        ]
      },
      {
        store: 'GC',
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
          { name: 'user_id', keypath: 'user_id', options: { unique: false } },
          { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
          { name: 'data', keypath: 'by_no', options: { unique: false } },
        ]
      },
      {
        store: 'CG',
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
          { name: 'user_id', keypath: 'user_id', options: { unique: false } },
          { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
          { name: 'data', keypath: 'by_no', options: { unique: false } },
        ]
      },
      {
        store: 'GTWCity',
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
          { name: 'user_id', keypath: 'user_id', options: { unique: false } },
          { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
          { name: 'data', keypath: 'by_no', options: { unique: false } },
        ]
      },
      {
        store: 'GTWState',
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
          { name: 'user_id', keypath: 'user_id', options: { unique: false } },
          { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
          { name: 'data', keypath: 'by_no', options: { unique: false } },
        ]
      },
      {
        store: 'GTOState',
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
          { name: 'user_id', keypath: 'user_id', options: { unique: false } },
          { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
          { name: 'data', keypath: 'by_no', options: { unique: false } },
        ]
      },
      {
        store: 'GTStore',
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
          { name: 'user_id', keypath: 'user_id', options: { unique: false } },
          { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
          { name: 'data', keypath: 'by_no', options: { unique: false } },
        ]
      },
      {
        store: 'GTReturnable',
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
          { name: 'user_id', keypath: 'user_id', options: { unique: false } },
          { name: 'company_section_id', keypath: 'company_section_id', options: { unique: false } },
          { name: 'data', keypath: 'by_no', options: { unique: false } },
        ]
      }         
    ],
  }
}