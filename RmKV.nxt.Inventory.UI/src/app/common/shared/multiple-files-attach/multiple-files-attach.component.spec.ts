import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleFilesAttachComponent } from './multiple-files-attach.component';

describe('MultipleFilesAttachComponent', () => {
  let component: MultipleFilesAttachComponent;
  let fixture: ComponentFixture<MultipleFilesAttachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleFilesAttachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleFilesAttachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
