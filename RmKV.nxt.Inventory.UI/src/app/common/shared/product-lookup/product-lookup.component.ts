import { HostListener } from '@angular/core';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { InvoiceDetailsService } from 'src/app/module/Inventory/services/purchases/invoice-prod-details/invoice-details.service';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-product-lookup',
  templateUrl: './product-lookup.component.html',
  styleUrls: ['./product-lookup.component.scss']
})
export class ProductLookupComponent implements OnInit {

  objLookup = {
    product_group_id: 0,
    product_group_desc: '',
    product_uom: '',
    apply_check: false
  }
  atributeValue = '';
  tempProductList: any;
  ProductList: any = [{
    product_code: '',
    product_name: '',
    product_attributes: [{
      attribute_name: '',
      value_data: [{
        attribute_value_id: '',
        attribute_value: ''
      }]
    }]
  }];
  selectedRowIndex: number = 0;
  selectedProduct: any = [];
  filterProductList: any;
  attributesList = [];
  productDetails: any;
  maxrow: any;
  tempAttributesList: any;
  constructor(public _dialogRef: MatDialogRef<ProductLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    public _keyPressEvents: KeyPressEvents,
    public _productLookupService: InvoiceDetailsService,
    public _localStorage: LocalStorage) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.objLookup.product_group_desc = this._data.product_group_desc;
    this.objLookup.product_uom = this._data.product_uom;
    this.objLookup.product_group_id = this._data.product_group_id;
    this._data.searchString = this._data.searchString;
    console.log(this._data)
    this.getInvoiceProdAttributes();
    this.getProductLookup();
  }


  private getInvoiceProdAttributes(): void {
    debugger;
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        product_group_id: +this.objLookup.product_group_id
      }])
    }
    this._productLookupService.getInvoiceProdAttributes(objData).subscribe((result: any) => {
      if (result) {
        console.log(result, 'value')
        this.attributesList = result;
        this.tempAttributesList = result;
        console.log(this.attributesList, 'test')
      }
    });
  }
  private getProductLookup(): void {
    debugger;
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        product_group_id: +this.objLookup.product_group_id
      }])
    }
    this._productLookupService.getProductLookupList(objData).subscribe((result: any) => {
      if (result) {
        console.log(JSON.parse(result.records), 'ressjhbkjui');
        this.ProductList = JSON.parse(result.records);
        console.log(this.ProductList, 'Test');
        this.tempProductList = JSON.parse(result.records);
        this.filterProductList = JSON.parse(result.records);
        this.filterProduct(this._data);
      }
    });
  }
  private getProductLookupDetails(i): void {
    debugger;
    let objData = {
      InvoiceProdDetails: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        product_group_id: +this.objLookup.product_group_id,
        product_code: this.filterProductList[i].product_code
      }])
    }
    this._productLookupService.getProductLookupDetails(objData).subscribe((result: any) => {
      if (result) {
        //this.productDetails = result;
        this.selectedProduct = result;
        this.selectedProduct.push(Object.assign({ checkbox: this.objLookup.apply_check }));
        this._dialogRef.close(this.selectedProduct);
      }
    });
  }

  onKeyDown(e: any) {
    debugger;
    e.preventDefault();
    var listElm = document.querySelector('tr');
    var selectedElm = document.activeElement,
      // map actions to event's key
      action = { ArrowUp: "previous", Up: "previous", ArrowDown: "next", Down: "next" }

    selectedElm = selectedElm[action[e.key] + "ElementSibling"];

    // loop when reached the top or bottom edge
    if (!selectedElm)
      selectedElm = listElm.children[e.key == 'ArrowUp' || e.key == 'Up' ? listElm.children.length - 1 : 0];
    selectedElm;
    // selectedElm.focus();
  }


  private filterProduct(dialogValue: any): void {
    debugger
    dialogValue.searchString = !dialogValue.searchString ? "" : dialogValue.searchString.toLowerCase().trim();
    if (dialogValue.searchString) {
      this.tempProductList = this.tempProductList.filter((item: any) =>
        item.product_code.toLowerCase().includes(dialogValue.searchString) ||
        item.product_name.toLowerCase().includes(dialogValue.searchString));
    } if (dialogValue.searchString === "") {
      this.tempProductList = this.ProductList;
    }
    this.filterProductList = this.tempProductList;
  }

  public searchProduct(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredProducts = [];
      if (searchString === "") {
        filteredProducts = this.ProductList;
      } else {
        filteredProducts = this.ProductList.filter(item =>
          item.product_code.toLowerCase().includes(searchString.trim().toLowerCase()) ||
          item.product_name.toLowerCase().includes(searchString.trim().toLowerCase()));
      }
      if (filteredProducts.length > 0)
        this.selectedRowIndex = 0;
      this.filterProductList = filteredProducts;
    }
  }



  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.tempProductList);
    if (this.tempProductList)
      this.selectedProduct = this.tempProductList[this.selectedRowIndex];
    this.scrollTo(this.selectedRowIndex);
  }
  private scrollTo(index: number) {
    let elmnt = document.getElementById("row" + index);
    elmnt.scrollIntoView(false);
    window.scrollTo(0, 0); // only if it's innerhtml
  }


  public onDoubleClick(i: number): void {
    debugger
    this.getProductLookupDetails(i);
    // this.selectedProduct = [];
    // this.selectedProduct.push(this.productDetails);
    // this.selectedProduct.push(Object.assign({ checkbox: this.objLookup.apply_check }));
    // // this.selectedProduct = [];
    // // this.selectedProduct.push(selectedProductRecord);
    // // this.selectedProduct.push(Object.assign({ checkbox: this.objLookup.apply_check }));
    // this._dialogRef.close(this.selectedProduct);
  }

  public onClick(selectedProductRecord: any, event: KeyboardEvent | any): void {
    debugger
    if (event.keyCode === 13) {
      this.getProductLookupDetails(this.selectedRowIndex);
      // this.selectedProduct = [];
      // this.selectedProduct.push(this.productDetails);
      // this.selectedProduct.push(Object.assign({ checkbox: this.objLookup.apply_check }));
      // // this.selectedProduct = [];
      // // this.selectedProduct.push(selectedProductRecord);
      // // this.selectedProduct.push(Object.assign({ checkbox: this.objLookup.apply_check }));
      // this._dialogRef.close(this.selectedProduct);
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    debugger
    if (event.keyCode === 13) {
      this.getProductLookupDetails(this.selectedRowIndex);
      //  this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this.getProductLookupDetails(this.selectedRowIndex);
    // let selectedRow = [];
    // selectedRow.push(this.productDetails);
    // selectedRow.push(Object.assign({ checkbox: this.objLookup.apply_check }));
    // // selectedRow.push(this.tempProductList[this.selectedRowIndex]);
    // // selectedRow.push(Object.assign({ checkbox: this.objLookup.apply_check }));
    // this._dialogRef.close(selectedRow);
  }
  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }
  //   filterAttributesValue(data: any) {
  //     debugger
  //     if (data.target.value !== '') {
  //       this.filterProductList = [];
  //       this._data.searchString = '';
  //       for (let i = 0; i < this.ProductList.length; i++) {
  //         if (this.ProductList[i].product_attributes !== '') {
  //           let array = JSON.parse(this.ProductList[i].product_attributes);
  //           if (array[0].attribute_value_id == +data.target.value) {
  //             this.filterProductList.push(this.ProductList[i]);
  //           }
  //         }
  //       }
  //     } else {
  //       this.filterProductList = this.ProductList;
  //     }
  //   }
  // }
  filterAttributesValue(data: any) {
    debugger
    if (data.target.value !== '') {
      // this.filterProductList = [];
      this._data.searchString = '';
      for (let i = 0; i < this.filterProductList.length; i++) {
        if (this.filterProductList[i].product_attributes !== '') {
          let array = JSON.parse(this.filterProductList[i].product_attributes);
          if (array[0].attribute_value_id == +data.target.value) {
            this.filterProductList = this.filterProductList[i];
          } else {
            this.filterProductList = [];
          }
        }
      }
    } else {
      this.filterProductList = this.ProductList;
      this.attributesList = [];
      this.attributesList = this.tempAttributesList;
    }
  }
}