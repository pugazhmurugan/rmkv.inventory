import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductionMaterialsService } from 'src/app/module/Inventory/services/masters/production-materials/production-materials.service';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-production-material-lookup',
  templateUrl: './production-material-lookup.component.html',
  styleUrls: ['./production-material-lookup.component.scss']
})
export class ProductionMaterialLookupComponent implements OnInit {

  materialNameList: MaterialNameLookup[] = [];
  filteredMaterialNameList: MaterialNameLookup[] = [];
  tempMaterialNameList: MaterialNameLookup[] = [];

  selectedMaterial: MaterialNameLookup = {
    material_id: 0,
    material_name: "",
    material_uom: 0,
    active: true
  };

  selectedList: any = [];
  searchString: string = "";
  selectedRowIndex: number = 0;
  
  constructor(
    public _dialogRef: MatDialogRef<ProductionMaterialLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: searchDialog,
    public _matDialog: MatDialog,
    private _prodMaterialService: ProductionMaterialsService,
    private _keyPressEvents: KeyPressEvents,
    public _localStorage: LocalStorage
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getMaterialNameList();
  }

  private getMaterialNameList(): void {
    this._prodMaterialService.getProductionMaterialDetails().subscribe((result: any) => {
      if (result && result.length > 0) {
        this.materialNameList = result;
        this.tempMaterialNameList = result;
        this.filteredMaterialNameList = result;
        this.filterMaterial(this._data);
      }
    });
  }

  onKeyDown(e: any) {
    debugger;
    e.preventDefault();
    var listElm = document.querySelector('tr');
    var selectedElm = document.activeElement,
      action = { ArrowUp: "previous", Up: "previous", ArrowDown: "next", Down: "next" }
    selectedElm = selectedElm[action[e.key] + "ElementSibling"];
    if (!selectedElm)
      selectedElm = listElm.children[e.key == 'ArrowUp' || e.key == 'Up' ? listElm.children.length - 1 : 0];
    selectedElm;
  }

  private filterMaterial(dialogValue: searchDialog): void {
    dialogValue.searchString = !dialogValue.searchString ? "" : dialogValue.searchString.toLowerCase().trim();
    if (dialogValue.searchString) {
      this.tempMaterialNameList = this.tempMaterialNameList.filter((item: any) =>
        item.material_name.toLowerCase().startsWith(dialogValue.searchString));
    } if (dialogValue.searchString === "") {
      this.tempMaterialNameList = this.materialNameList;
    }
    this.filteredMaterialNameList = this.tempMaterialNameList;
  }

  public searchMaterial(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredTemp = [];
      if (searchString === "") {
        filteredTemp = this.materialNameList;
      } else {
        filteredTemp = this.materialNameList.filter(item =>
          item.material_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
      }
      if (filteredTemp.length > 0)
        this.selectedRowIndex = 0;
      this.filteredMaterialNameList = filteredTemp;
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      this.selectedMaterial = this.filteredMaterialNameList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(this.filteredMaterialNameList[this.selectedRowIndex]);
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.filteredMaterialNameList);
    if (this.filteredMaterialNameList)
      this.selectedMaterial = this.filteredMaterialNameList[this.selectedRowIndex];
  }

  public onDoubleClick(selectedProductRecord: any): void {
    this.selectedMaterial = selectedProductRecord;
    this._dialogRef.close(this.selectedMaterial);
  }

  public onClick(selectedAttributeRecord: any, event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.selectedMaterial = selectedAttributeRecord;
      this._dialogRef.close(this.selectedMaterial);
    }
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

}

export class MaterialNameLookup {
  material_id: number;
  material_name: string;
  material_uom: number;
  active: boolean;
}

export class searchDialog {
  searchString: string;
}