import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionMaterialLookupComponent } from './production-material-lookup.component';

describe('ProductionMaterialLookupComponent', () => {
  let component: ProductionMaterialLookupComponent;
  let fixture: ComponentFixture<ProductionMaterialLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionMaterialLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionMaterialLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
