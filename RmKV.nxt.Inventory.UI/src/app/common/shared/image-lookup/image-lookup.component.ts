import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-image-lookup',
  templateUrl: './image-lookup.component.html',
  styleUrls: ['./image-lookup.component.scss']
})
export class ImageLookupComponent implements OnInit {

  constructor(public _dialogRef: MatDialogRef<ImageLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,) { this._dialogRef.disableClose = true; }

  ngOnInit() {
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

}
