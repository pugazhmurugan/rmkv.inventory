import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageLookupComponent } from './image-lookup.component';

describe('ImageLookupComponent', () => {
  let component: ImageLookupComponent;
  let fixture: ComponentFixture<ImageLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
