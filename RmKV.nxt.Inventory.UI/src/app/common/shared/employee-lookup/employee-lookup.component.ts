import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { EmployeeLookup, EmployeeLookupDialog, EmployeeLookupFil } from '../../models/employee-lookup-model';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { EmployeeLookupService } from '../../services/employee-lookup/employee-lookup.service';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';
import { SalesLocation } from '../../models/common-model';

@Component({
  selector: 'app-employee-lookup',
  templateUrl: './employee-lookup.component.html',
  styleUrls: ['./employee-lookup.component.scss']
})
export class EmployeeLookupComponent implements OnInit {

  employeeList: EmployeeLookup[];
  filterdEmployeeList: EmployeeLookup[];
  temporaryEmployeeList: EmployeeLookup[] = [];

  selectedEmployee: EmployeeLookup = {
    Employee_Id: 0,
    Employee_Code: "",
    Employee_Name: "",
    Designation_Name: "",
    Department_Name: "",
    Section_Name: "",
    Company_Name: "",
    Sales_Location_Name: "",
    Level1: "",
    Level2: "",
    Salesman_Code: 0
  };

  employeeLookupFil: EmployeeLookupFil = {
    For: "all",
    Resigned: false,
    Sales_Location_Id: this._localStorage.getGlobalSalesLocationId()
  }

  selectedRowIndex: number = 0;
  workingLocationList: SalesLocation[] = [];

  constructor(
    public _dialogRef: MatDialogRef<EmployeeLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: EmployeeLookupDialog,
    public _matDialog: MatDialog,
    private _employeeLookupService: EmployeeLookupService,
    private _keyPressEvents: KeyPressEvents,
    public _localStorage: LocalStorage
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getWorkingLocation();
    this.employeeLookupForWhat();
  }

  private employeeLookupForWhat(): void {
    debugger
    if (!this._data.For || this._data.For.toString().toLowerCase() === "all") {
      this.getEmployeeList('All');
    }
    if (this._data.For) {
      switch (this._data.For.toString().toLowerCase()) {

        case "sales":
          this.getEmployeeList("Sales");
          break;

        case "non sales":
          this.getEmployeeList("Non Sales");
          break;

        case "kra":
          this.getEmployeeList("KRA");
          break;

        case "sales+kra":
          this.getEmployeeList("Sales+KRA");
          break;

        case "sales exception":
          this.getEmployeeList("Sales Exception");
          break;

        case "advance":
          this.getEmployeeList("ADVANCE");
          break;

        case "relieving":
          this.getEmployeeList("Relieving");
          break;

        case "advance report":
          this.getEmployeeList("ADVANCEREPORT");
          break;

        case "resignation":
          this.getEmployeeList("Resignation");
          break;
          
        case "employee queries":
          this.getEmployeeList("Employee Queries");
          break;

        case "sales and nonsales":
            this.getEmployeeList("SALES&NONSALES");
            break;
      }
    }
  }

  private getWorkingLocation(): void {
    this.workingLocationList = this._localStorage.getWorkingLocation();
  }

  public getEmployeeList(_for: string): void {
    debugger;
    if (_for != undefined) {
      this.employeeLookupFil.For = _for;
      this.employeeLookupFil.Sales_Location_Id = this._data.Sales_Location_Id ? +this._data.Sales_Location_Id : +this._localStorage.getGlobalSalesLocationId();
      if (_for == 'Resignation') {
        this.employeeLookupFil.Resigned = this._data.Resigned
      }
    }
    this._employeeLookupService.getEmployeeLookupfor(this.employeeLookupFil).subscribe((result: any) => {
      debugger;
      this.employeeList = result;
      this.setSalesLocationName();
      this.temporaryEmployeeList = result;
      this.filterdEmployeeList = result;
      this.filterEmployee(this._data);
    });
  }

  private setSalesLocationName(): void {
    this._data.Sales_Location_Name = this.workingLocationList[this.workingLocationList.findIndex((item: any) =>
      item.Sales_Location_ID === (this._data.Sales_Location_Id ? +this._data.Sales_Location_Id
        : +this._localStorage.getGlobalSalesLocationId()))].Sales_Location_Name;
  }

  private filterEmployee(dialogValue: EmployeeLookupDialog): void {
    dialogValue.searchString = !dialogValue.searchString ? "" : dialogValue.searchString.toLowerCase().trim();
    this.filterBasedOnAll();
    if (dialogValue.searchString) {
      this.temporaryEmployeeList = this.temporaryEmployeeList.filter((item: any) =>
        item.Employee_Name.toLowerCase().startsWith(dialogValue.searchString) ||
        item.Employee_Code.toLowerCase().startsWith(dialogValue.searchString));
    } if (dialogValue.searchString === "" && dialogValue.Sales_Location_Name === "" && dialogValue.Section_Name === "") {
      this.temporaryEmployeeList = this.employeeList;
    }
    this.filterdEmployeeList = this.temporaryEmployeeList;
  }

  private filterBasedOnAll(): void {
    this.filterBasedOnWorkingLocationAndSection();
    this.filterBasedOnCompanyAndDesignation();
    this.filterBasedOnNonSales();
    this.filterBasedOnOthers();
  }

  private filterBasedOnWorkingLocationAndSection(): void {
    this.temporaryEmployeeList = this.employeeList;
    if (this.checkValidation(this._data.Sales_Location_Name)) {
      this.temporaryEmployeeList = this.temporaryEmployeeList.filter((item: any) =>
        item.Sales_Location_Name.toString().trim().toLowerCase().startsWith(this._data.Sales_Location_Name.toString().trim().toLowerCase()));
    } if (this.checkValidation(this._data.Section_Name)) {
      this.temporaryEmployeeList = this.temporaryEmployeeList.filter((item: any) =>
        item.Section_Name.toString().trim().toLowerCase().startsWith(this._data.Section_Name.toString().trim().toLowerCase()));
    }
  }

  private filterBasedOnCompanyAndDesignation(): void {
    if (this.checkValidation(this._data.Company_Name)) {
      this.temporaryEmployeeList = this.temporaryEmployeeList.filter((item: any) =>
        item.Company_Name.toString().trim().toLowerCase().startsWith(this._data.Company_Name.toString().trim().toLowerCase()));
    } if (this.checkValidation(this._data.Designation_Name)) {
      this.temporaryEmployeeList = this.temporaryEmployeeList.filter((item: any) =>
        item.Designation_Name.toString().trim().toLowerCase().startsWith(this._data.Designation_Name.toString().trim().toLowerCase()));
    }
  }

  private filterBasedOnNonSales(): void {
    if (this.checkValidationForNonSales(this._data.Non_Sales_Desgination_Name)) {
      this.temporaryEmployeeList = this.temporaryEmployeeList.filter((item: any) =>
        item.Non_Sales_Desgination_Name.toString().trim().toLowerCase().startsWith(this._data.Non_Sales_Desgination_Name.toString().trim().toLowerCase()));
    } if (this.checkValidationForNonSales(this._data.Non_Sales_Section_Name)) {
      this.temporaryEmployeeList = this.temporaryEmployeeList.filter((item: any) =>
        item.Non_Sales_Section_Name.toString().trim().toLowerCase().startsWith(this._data.Non_Sales_Section_Name.toString().trim().toLowerCase()));
    } if (this.checkValidationForNonSales(this._data.Level1)) {
      this.temporaryEmployeeList = this.temporaryEmployeeList.filter((item: any) =>
        item.Level1.toString().trim().toLowerCase().startsWith(this._data.Level1.toString().trim().toLowerCase()));
    }
  }

  private filterBasedOnOthers(): void {
    if ((this._data.Sales_Location_Name === "" || this._data.Sales_Location_Name === undefined) &&
      (this._data.Section_Name === "" || this._data.Section_Name === undefined) &&
      (this._data.Non_Sales_Section_Name === "" || this._data.Non_Sales_Section_Name === undefined ||
        this._data.Non_Sales_Section_Name === "Others") &&
      (this._data.Non_Sales_Desgination_Name === "" || this._data.Non_Sales_Desgination_Name === undefined ||
        this._data.Non_Sales_Desgination_Name === "Others") &&
      (this._data.Level1 === "" || this._data.Level1 === undefined || this._data.Level1 === "Others") &&
      (this._data.Company_Name === "" || this._data.Company_Name === undefined) &&
      (this._data.Designation_Name === "" || this._data.Designation_Name === undefined)) {
      this.temporaryEmployeeList = this.employeeList;
    }
  }

  private checkValidation(value: string): boolean {
    return value ? true : false;
  }

  private checkValidationForNonSales(value: string): boolean {
    return value && value !== "Others" ? true : false;
  }

  public searchEmployee(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      this.filterBasedOnAll();
      let filteredEmployees = [];
      if (searchString === "") {
        filteredEmployees = this.employeeList;
      } else {
        filteredEmployees = this.employeeList.filter(item =>
          item.Employee_Code.toLowerCase().startsWith(searchString.trim().toLowerCase())
          || item.Employee_Name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
      }
      if (filteredEmployees.length > 0)
        this.selectedRowIndex = 0;
      this.filterdEmployeeList = filteredEmployees;
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      this.selectedEmployee = this.filterdEmployeeList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(this.filterdEmployeeList[this.selectedRowIndex]);
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.filterdEmployeeList);
    if (this.filterdEmployeeList)
      this.selectedEmployee = this.filterdEmployeeList[this.selectedRowIndex];
    this.scrollTo(this.selectedRowIndex);
  }

  private scrollTo(index: number) {
    let elmnt = document.getElementById("row" + index);
    elmnt.scrollIntoView(false);
    window.scrollTo(0, 0); // only if it's innerhtml
  }

  public onDoubleClick(selectedEmployeeRecord: any): void {
    this.selectedEmployee = selectedEmployeeRecord;
    this._dialogRef.close(this.selectedEmployee);
  }

  public onClick(selectedEmployeeRecord: any, event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.selectedEmployee = selectedEmployeeRecord;
      this._dialogRef.close(this.selectedEmployee);
    }

  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }
}


