import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingSupplierLookupComponent } from './pending-supplier-lookup.component';

describe('PendingSupplierLookupComponent', () => {
  let component: PendingSupplierLookupComponent;
  let fixture: ComponentFixture<PendingSupplierLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingSupplierLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingSupplierLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
