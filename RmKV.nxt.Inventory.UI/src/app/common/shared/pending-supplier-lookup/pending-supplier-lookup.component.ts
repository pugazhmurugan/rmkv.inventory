import { HostListener, Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { AccountsLookUp } from '../../models/accounts-lookup-model';
import { AccountsLookupService } from '../../services/accounts-lookup/accounts-lookup.service';
import { AccountsLookupComponent } from '../accounts-lookup/accounts-lookup.component';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-pending-supplier-lookup',
  templateUrl: './pending-supplier-lookup.component.html',
  styleUrls: ['./pending-supplier-lookup.component.scss']
})
export class PendingSupplierLookupComponent implements OnInit {
  accountsList: AccountsLookUp[] = [];
  fillterAccountsList: any[] = [];
  selectedUnifrom: any[] = [];
  selectedRowIndex: number = 0;
  gentMasterList: any[] = [];
  fieldValue: number = -1;
  locationId: number;
  constructor(
    public _dialogRef: MatDialogRef<AccountsLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    private _keyPressEvents: KeyPressEvents,
    public _localStorage: LocalStorage,
    public _agentMasterService: AccountsLookupService,
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadAccountsDetails();
  }

  private loadAccountsDetails(): void {
    debugger;
    let objAccounts = {
      WorkOrderIssue: JSON.stringify([{
        wh_section_id: this._localStorage.getWhSectionId(),
        transfer_type: this._data.data,
        company_section_id: +this._localStorage.getCompanySectionId()
      }])
    }
    this._agentMasterService.gePendindSupplierLookup(objAccounts).subscribe((result: any) => {
      if (result) {
      debugger;
        this.accountsList = result;
        this.fillterAccountsList = result;
        this.gentMasterList = result;
        this.filterUnifrom(this._data.searchString);
      }
    });
  }


  // private getUniformList(): void {
  //   debugger;
  //   this._uniformService.getUniformList().subscribe((result: any) => {
  //     this.accountsList = result;
  //     this.fillterAccountsList = result;
  //     this.filterUnifrom(this._data);
  //   });
  // }

  private filterUnifrom(searchString: any): void {
    searchString = !searchString ? "" : searchString.toLowerCase().trim();
    let filteredValue = [];
    if (searchString) {
      filteredValue = this.accountsList.filter((item: any) =>
        item.account_code.toLowerCase().startsWith(searchString)
        || item.account_name.toLowerCase().startsWith(searchString)
      );
    } if (searchString === "") {
      filteredValue = this.accountsList;
    }
    this.fillterAccountsList = filteredValue;
  }

  public searchaccounts(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredValue = [];
      if (searchString === "") {
        filteredValue = this.accountsList;
      } else {
        filteredValue = this.accountsList.filter(item =>
          item.account_code.toLowerCase().startsWith(searchString.trim().toLowerCase())
          || item.account_name.toLowerCase().startsWith(searchString.trim().toLowerCase())
        );
      }
      if (filteredValue.length > 0)
        this.selectedRowIndex = 0;
      this.fillterAccountsList = filteredValue;
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      this.selectedUnifrom = this.fillterAccountsList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(this.fillterAccountsList[this.selectedRowIndex]);
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.fillterAccountsList);
    if (this.fillterAccountsList)
      this.selectedUnifrom = this.fillterAccountsList[this.selectedRowIndex];
    this.scrollTo(this.selectedRowIndex);
  }

  private scrollTo(index: number) {
    let elmnt = document.getElementById("row" + index);
    elmnt.scrollIntoView(false);
    window.scrollTo(0, 0); // only if it's innerhtml
  }

  public onDoubleClick(selectedTrainerRecord: any): void {
    this.selectedUnifrom = selectedTrainerRecord;
    this._dialogRef.close(this.selectedUnifrom);
  }

  public onClick(selectedTrainerRecord: any, event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.selectedUnifrom = selectedTrainerRecord;
      this._dialogRef.close(this.selectedUnifrom);
    }

  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }
}
