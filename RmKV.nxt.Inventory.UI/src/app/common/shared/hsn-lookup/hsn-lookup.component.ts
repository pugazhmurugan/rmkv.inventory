import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HsnMasterService } from 'src/app/module/Inventory/services/masters/hsn-master/hsn-master.service';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-hsn-lookup',
  templateUrl: './hsn-lookup.component.html',
  styleUrls: ['./hsn-lookup.component.scss']
})
export class HsnLookupComponent implements OnInit {

  HSNList: HSNLookup[] = [];
  filteredHSNList: HSNLookup[] = [];
  tempHSNList: HSNLookup[] = [];

  selectedHSN: HSNLookup = {
    HSN_id: 0,
    HSN_name: "",
    // HSN_value_id : 0,
    // HSN_value : ""
  };

  selectedList: any = [];
  searchString: string = "";
  selectedRowIndex: number = 0;

  constructor(
    public _dialogRef: MatDialogRef<HsnLookupComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: searchDialog,
    public _matDialog: MatDialog,
    private _hsnMasterService: HsnMasterService,
    private _keyPressEvents: KeyPressEvents,
    public _localStorage: LocalStorage
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.getHSNList();
  }

  private getHSNList(): void {
    this._hsnMasterService.getHSNMaster().subscribe((result: any[]) => {
      console.log(result, "list")
      this.HSNList = result;
      this.tempHSNList = result;
      this.filteredHSNList = result;
      this.filterHSN(this._data);
    });
  }

  onKeyDown(e: any) {
    debugger;
    e.preventDefault();
    var listElm = document.querySelector('tr');
    var selectedElm = document.activeElement,
      // map actions to event's key
      action = { ArrowUp: "previous", Up: "previous", ArrowDown: "next", Down: "next" }

    selectedElm = selectedElm[action[e.key] + "ElementSibling"];

    // loop when reached the top or bottom edge
    if (!selectedElm)
      selectedElm = listElm.children[e.key == 'ArrowUp' || e.key == 'Up' ? listElm.children.length - 1 : 0];
    selectedElm;
    // selectedElm.focus();
  }

  private filterHSN(dialogValue: searchDialog): void {
    dialogValue.searchString = !dialogValue.searchString ? "" : dialogValue.searchString.toLowerCase().trim();
    if (dialogValue.searchString) {
      this.tempHSNList = this.tempHSNList.filter((item: any) =>
        item.HSN_name.toLowerCase().startsWith(dialogValue.searchString));
      // item.template_name.toLowerCase().startsWith(dialogValue.searchString));
    } if (dialogValue.searchString === "") {
      this.tempHSNList = this.HSNList;
    }
    this.filteredHSNList = this.tempHSNList;
  }

  public searchHSN(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredTemp = [];
      if (searchString === "") {
        filteredTemp = this.HSNList;
      } else {
        filteredTemp = this.HSNList.filter(item =>
          item.HSN_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
        // || item.template_name.toLowerCase().startsWith(searchString.trim().toLowerCase()));
      }
      if (filteredTemp.length > 0)
        this.selectedRowIndex = 0;
      this.filteredHSNList = filteredTemp;
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      this.selectedHSN = this.filteredHSNList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(this.filteredHSNList[this.selectedRowIndex]);
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.filteredHSNList);
    if (this.filteredHSNList)
      this.selectedHSN = this.filteredHSNList[this.selectedRowIndex];
  }

  public onDoubleClick(selectedTemplateRecord: any): void {
    this.selectedHSN = selectedTemplateRecord;
    this._dialogRef.close(this.selectedHSN);
  }

  public onClick(selectedHSNRecord: any, event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.selectedHSN = selectedHSNRecord;
      this._dialogRef.close(this.selectedHSN);
    }
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }

}

export class HSNLookup {
  HSN_id: number;
  HSN_name: string;
  //   HSN_value_id : number;
  //   HSN_value : string;
}

export class searchDialog {
  searchString: string;
  group_section_id: number;
}