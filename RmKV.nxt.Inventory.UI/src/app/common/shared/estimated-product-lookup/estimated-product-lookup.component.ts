import { DatePipe } from '@angular/common';
import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { IssueDetailComponent } from 'src/app/module/Inventory/pages/goods-movement/returnable-goods-transfer/issue-detail/issue-detail.component';
import { ProductionReceiptService } from 'src/app/module/Inventory/services/goods-movement/returnable-goods-transfer/production-receipt/production-receipt.service';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { KeyPressEvents } from '../directives/key.press.events';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-estimated-product-lookup',
  templateUrl: './estimated-product-lookup.component.html',
  styleUrls: ['./estimated-product-lookup.component.scss'],
  providers: [DatePipe]
})
export class EstimatedProductLookupComponent implements OnInit {

  objIssueNos: any = {
    issue_no: '',
    issue_date: '',
    transfer_type: '',
    wh_section_id: this._localStorage.getWhSectionId()
  }
  selectedRowIndex: number = 0;
  productsList: any[] = [];
  selectedProduct: any[] = [];
  filterProductList: any[] = [];
  constructor(
    public _router: Router,
    public _datePipe: DatePipe,
    public _dialogRef: MatDialogRef<IssueDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _matDialog: MatDialog,
    private _keyPressEvents: KeyPressEvents,
    public _confirmationDialogComponent: ConfirmationDialogComponent,
    private _productionReceiptService: ProductionReceiptService,
    public _localStorage: LocalStorage
  ) {
    this._dialogRef.disableClose = true;
    debugger;
    this.objIssueNos = JSON.parse(JSON.stringify(this._data.objIssueNo));
    this.loadProductLookupList();
  }

  ngOnInit() {
  }

  private loadProductLookupList(): void {
    let objLoad: any = {
      ProductionReceipt: JSON.stringify([{
        company_section_id: +this._localStorage.getCompanySectionId(),
        wh_section_id: +this.objIssueNos.wh_section_id,
        issue_no: +this.objIssueNos.issue_no
      }])
    }
    this._productionReceiptService.getProductionReceiptProductLookup(objLoad).subscribe((result: any[]) => {
      if (result) {
        this.productsList = JSON.parse(JSON.stringify(result));
        this.filterProductList = JSON.parse(JSON.stringify(result));
        this.filterProducts(this._data.searchString);
      }
    });
  }

  private filterProducts(searchString: any): void {
    searchString = !searchString ? "" : searchString.toLowerCase().trim();
    let filteredValue = [];
    if (searchString) {
      filteredValue = this.productsList.filter((item: any) =>
        item.product_name.toLowerCase().startsWith(searchString)
      );
    } if (searchString === "") {
      filteredValue = this.productsList;
    }
    this.filterProductList = filteredValue;
  }

  public searchProducts(searchString: string, event?: KeyboardEvent | any): void {
    if (event.key !== "ArrowDown" && event.key !== "ArrowUp") {
      let filteredValue = [];
      if (searchString === "") {
        filteredValue = this.productsList;
      } else {
        filteredValue = this.productsList.filter(item =>
          item.product_name.toLowerCase().startsWith(searchString.trim().toLowerCase())
        );
      }
      if (filteredValue.length > 0)
        this.selectedRowIndex = 0;
      this.filterProductList = filteredValue;
    }
  }

  public determineEnterKey(event: KeyboardEvent | any): void {
    if (event.keyCode === 13) {
      this.selectedProduct = this.filterProductList[this.selectedRowIndex];
      this.dialogOK();
    }
  }

  public dialogOK(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(this.filterProductList[this.selectedRowIndex]);
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent | any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (document.getElementById("row" + this.selectedRowIndex)) {
        document.getElementById("row" + this.selectedRowIndex).focus();
      }
    }
    this.selectedRowIndex = this._keyPressEvents.arrowKeyUpAndDown(event, this.selectedRowIndex, this.filterProductList);
    if (this.filterProductList)
      this.selectedProduct = this.filterProductList[this.selectedRowIndex];
    this.scrollTo(this.selectedRowIndex);
  }

  private scrollTo(index: number) {
    let elmnt = document.getElementById("row" + index);
    elmnt.scrollIntoView(false);
    window.scrollTo(0, 0); // only if it's innerhtml
  }

  public onDoubleClick(selectedRecord: any): void {
    this.selectedProduct = selectedRecord;
    this._dialogRef.close(this.selectedProduct);
  }

  public onClick(selectedRecord: any, event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.selectedProduct = selectedRecord;
      this._dialogRef.close(this.selectedProduct);
    }
  }

  public dialogClose(): void {
    this._dialogRef.disableClose = false;
    this._dialogRef.close(null);
  }
}
