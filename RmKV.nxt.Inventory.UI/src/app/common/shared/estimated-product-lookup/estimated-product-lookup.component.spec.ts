import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstimatedProductLookupComponent } from './estimated-product-lookup.component';

describe('EstimatedProductLookupComponent', () => {
  let component: EstimatedProductLookupComponent;
  let fixture: ComponentFixture<EstimatedProductLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstimatedProductLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstimatedProductLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
