import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CommonService } from '../../services/common/common.service';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { LocalStorage } from '../local-storage';

@Component({
  selector: 'app-single-file-attach',
  templateUrl: './single-file-attach.component.html',
  styleUrls: ['./single-file-attach.component.scss']
})
export class SingleFileAttachComponent implements OnInit {

  objAttachView = {
    file_path: ''
  }

  attachment: any = [
    {
      Comment: "",
      Progress: 0,
      File: [],
      File_Path: "File"
    }
  ]
  unchangedattachment: any = [
    {
      Comment: "",
      Progress: 0,
      File: [],
      File_Path: "File"
    }
  ]

  // selectedFiles: File | any = [];
  // removedFiles: File | any = [];
  selectedDocument: any = [];
  //  fileIndex: any[] = [];
  // removedfilesIndex: number[] = [];

  document: any;
  tempFile: any;
  oldDocument: any;

  @ViewChild('file', null) fileInput: ElementRef | any;
  @ViewChild('docInput', null) docInput: ElementRef | any;

  constructor(
    public _dialogRef: MatDialogRef<SingleFileAttachComponent>,
    private _confirmationDialog: ConfirmationDialogComponent,
    private _commonService: CommonService,
    public _matDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public _data: any,
    public _localStorage: LocalStorage,
  ) {
    this._dialogRef.disableClose = true;
  }

  ngOnInit() {
    if (this._data.objAttach)
      this.objAttachView = JSON.parse(this._data.objAttach);
    if (this.objAttachView.file_path) {
      this.attachment[0].Progress = 100;
      this.oldDocument = this.objAttachView.file_path;
    }
  }

  public uploadDocument(event): void {
    debugger;
    let fileToUpload = event.target.files[0] as any;
    let formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    formData.append('fileRootPath', this._localStorage.getInventoryDocumentPath());
    this._commonService.uploadTempFile(formData).subscribe(
      (result: any) => {
        debugger;
        this.tempFile = result.document_Path;
        this.attachment[0].Progress = 100;
        if (event.target.length != 0) {
          this.selectedDocument = event.target.files[0];
          this.document = event.target.files;
        } else {
          this.fileInput.nativeElement.files = this.document;
          this.selectedDocument = this.document[0];
        }
        this.objAttachView.file_path = "";
      });
  }


  public onClear(): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = "Are you sure? You wish to cancel";
    dialogRef.componentInstance.componentName = "Attach File";

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        // this.selectedFiles = '';
        this._dialogRef.close();
      }
    });
  }

  private onConfirmation(): void {
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage =
      "Are you sure? You wish to save";
    dialogRef.componentInstance.componentName = "Attach File";

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        let objAttach = {
          isFileAttach: 1,
          // myFiles: this.selectedFiles
        }
        this._dialogRef.close(objAttach);
      } else {
        this._dialogRef.close(null);
      }
    });
  }

  public openRemoveTempFileConfirmationDialog(i: number): void {
    debugger;
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = "Are you sure, want to remove this file?";
    dialogRef.componentInstance.componentName = "Attach File";
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        debugger;
        this.removeTempDocument();
      }
    });
  }

  public openRemoveFileConfirmationDialog(i: number): void {
    debugger;
    let dialogRef = this._matDialog.open(ConfirmationDialogComponent, {
      panelClass: "custom-dialog-container",
      data: { confirmationDialog: 1 }
    });
    dialogRef.componentInstance.confirmMessage = "Are you sure, want to remove this file?";
    dialogRef.componentInstance.componentName = "Attach File";
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        debugger;
        this.removeUploadedDocuments(i);
      }
    });
  }

  private removeUploadedDocuments(index: number): void {
    debugger;
    if (index == 0) {
      // this.removedFiles.push({
      //   name: this.oldDocument
      // });
      // this.removedfilesIndex.push(index);
      this.oldDocument = null;
      this.objAttachView.file_path = '';
      this.document = null;
      this.tempFile = null;
      this.attachment[0].Progress = 0;
    }
  }

  // public addFileAttachment(): void {
  //   debugger;
  //   if (this.selectedFiles.length < 1)
  //     this._confirmationDialog.openAlertDialog("Choose a file", "Attach File");
  //   else
  //     this.onUploadDocuments();
  // }

  public onUploadDocuments(): void {
    debugger;
    let docSize: number = 0;
    if (docSize > +this._localStorage.getInventoryDocumentSize()) {
      this._confirmationDialog.openAlertDialog("Maximum size for all files can't be greater than 15MB", 'Attach File');
      return;
    }
    let objAttach: any = {
      isFileAttach: 1,
      selectedFiles: this.selectedDocument,
      objAttachView: this.objAttachView,
      // removedFiles: this.removedFiles,
      // removedFileIndex: this.removedfilesIndex
    };
    this._dialogRef.close(objAttach);
  }

  public removeTempDocument(): void {
    let formData = new FormData();
    // formData.append('fileRootPath', this._localStorage.getInventoryDocumentPath());
    formData.append('fileRootPath', '\\\\192.9.202.39\\\\Rmkv_Nxt_Doc\\\\Invoices');
    formData.append('oldDocument', this.tempFile);
    this._commonService.uploadTempFile(formData).subscribe((result: any) => {
      debugger;
      if (!result.document_Path) {
        this._confirmationDialog.openAlertDialog('File has been removed', 'Attach File');
        this.attachment[0].Progress = 0;
        this.document = null;
        this.tempFile = null;
        this.docInput.nativeElement.value = '';
      }
    });
  }


}
