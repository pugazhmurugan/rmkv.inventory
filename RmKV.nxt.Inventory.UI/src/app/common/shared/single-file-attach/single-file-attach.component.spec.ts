import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleFileAttachComponent } from './single-file-attach.component';

describe('SingleFileAttachComponent', () => {
  let component: SingleFileAttachComponent;
  let fixture: ComponentFixture<SingleFileAttachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleFileAttachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleFileAttachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
