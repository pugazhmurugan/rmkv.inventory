import { NgxIndexedDBService } from 'ngx-indexed-db';
import { LocalStorage } from '../local-storage';

export class IndexedDBCommonFN {
    constructor(private dbService: NgxIndexedDBService,public _localStorage: LocalStorage){
        
    }
    public clearCommonIndexedData(schema_name: string): void {
        this.dbService.getByIndex(schema_name, 'user_id', this._localStorage.intGlobalUserId()).subscribe((data) => {
          console.log(data);
          if (data !== undefined) {
            this.dbService
              .update(schema_name, {
                id: data.id,
                user_id: this._localStorage.intGlobalUserId(),
                data: JSON.stringify([])
              })
              .subscribe((storeData) => {
                console.log('storeData: ', storeData);
              });
          }
          else {
            this.dbService
              .add(schema_name, {
                user_id: this._localStorage.intGlobalUserId(),
                data: JSON.stringify([])
              })
              .subscribe((key) => {
                console.log('key: ', key);
              });
          }
        });
      }
}