import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { ErrorComponent } from './pages/error/error.component';
import { ServerDownComponent } from './pages/server-down/server-down.component';
import { RedirectToComponent } from './pages/redirect-to/redirect-to.component';

const CommonRoutes: Routes = [
    { path: "redirect", component: RedirectToComponent },
    { path: "", redirectTo: "redirect", pathMatch: "full" },
    { path: "Maintanance", component: ServerDownComponent },
    { path: "Error", component: ErrorComponent },
    { path: "**", redirectTo: "Error", pathMatch: "full" },
];

@NgModule({
    imports: [RouterModule.forRoot(CommonRoutes)],
    exports: [RouterModule]
})

export class CommonRoute { }