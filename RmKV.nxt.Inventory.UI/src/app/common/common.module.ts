import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { HeaderComponent } from './pages/header/header.component';
import { ErrorComponent } from './pages/error/error.component';
import { ServerDownComponent } from './pages/server-down/server-down.component';
import { AngularMaterialModule } from '../angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmationDialogComponent } from './shared/confirmation-dialog/confirmation-dialog.component';
import { LockScreenComponent } from './shared/lock-screen/lock-screen.component';
import { LocalStorage } from './shared/local-storage';
import { ChildMenuItemsComponent } from './pages/child-menu-items/child-menu-items.component';
import { CommonService } from './services/common/common.service';
import { CommonRoute } from './common.routing';
import { EmployeeLookupComponent } from './shared/employee-lookup/employee-lookup.component';
import { RedirectToComponent } from './pages/redirect-to/redirect-to.component';
import { MenusResolveGuard, UserProfileResolveGuard } from './pages/resolve-guards/menus-resolve-guard/menus-resolve-guard';
import { OnlyNumber } from './shared/directives/only-number';
import { FocusOnInitDirective } from './shared/directives/focus-oninit';
import { ExcelService } from './shared/directives/excel-service';
import { ConfirmationDeleteComponent } from './shared/confirmation-delete/confirmation-delete.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { TodoService } from './pages/dashboard/todo.service';
import { FullScreenComponent } from './pages/full-screen/full-screen.component';
import { MessagesComponent } from './pages/messages/messages.component';
import { UserMenuComponent } from './pages/user-menu/user-menu.component';
import { MailComponent } from './pages/mail/mail.component';
import { ChatsComponent } from './pages/chats/chats.component';
import { AttributeLookupComponent } from './shared/attribute-lookup/attribute-lookup.component';
import { ProductGroupLookupComponent } from './shared/product-group-lookup/product-group-lookup.component';
import { HsnLookupComponent } from './shared/hsn-lookup/hsn-lookup.component';
import { SingleFileAttachComponent } from './shared/single-file-attach/single-file-attach.component';
import { AccountsLookupComponent } from './shared/accounts-lookup/accounts-lookup.component';
import { ImageLookupComponent } from './shared/image-lookup/image-lookup.component';
import { ReasonComponent } from './shared/reason/reason.component';
import { ProductLookupComponent } from './shared/product-lookup/product-lookup.component';
import { InvoiceProdDetailsConfirmationDialogComponent } from './shared/invoice-prod-details-confirmation-dialog/invoice-prod-details-confirmation-dialog.component';
import { StyleCodeLookupComponent } from './shared/style-code-lookup/style-code-lookup.component';
import { CounterCodeLookupComponent } from './shared/counter-code-lookup/counter-code-lookup.component';
import { ProductCodeLookupComponent } from './shared/product-code-lookup/product-code-lookup.component';
import { MultipleFilesAttachComponent } from './shared/multiple-files-attach/multiple-files-attach.component';
import { InvoiceAttributesLookupComponent } from './shared/invoice-attributes-lookup/invoice-attributes-lookup.component';
import { PendingSupplierLookupComponent } from './shared/pending-supplier-lookup/pending-supplier-lookup.component';
import { ProductionMaterialLookupComponent } from './shared/production-material-lookup/production-material-lookup.component';
import { ProductionProductLookupComponent } from './shared/production-product-lookup/production-product-lookup.component';
@NgModule({
    imports: [
        AngularMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        CommonRoute,
        NgxChartsModule,
        PerfectScrollbarModule,
    ],
    declarations: [
        RedirectToComponent,
        HeaderComponent,
        ErrorComponent,
        ServerDownComponent,
        ConfirmationDialogComponent,
        LockScreenComponent,
        ChildMenuItemsComponent,
        EmployeeLookupComponent,
        OnlyNumber,
        FocusOnInitDirective,
        ConfirmationDeleteComponent,
        DashboardComponent,
        FullScreenComponent,
        MessagesComponent,
        UserMenuComponent,
        MailComponent,
        ChatsComponent,
        AttributeLookupComponent,
        ProductGroupLookupComponent,
        HsnLookupComponent,
        SingleFileAttachComponent,
        AccountsLookupComponent,
        ImageLookupComponent,
        ReasonComponent,
        ProductLookupComponent,
        InvoiceProdDetailsConfirmationDialogComponent,
        StyleCodeLookupComponent,
        CounterCodeLookupComponent,
        ProductCodeLookupComponent,
        MultipleFilesAttachComponent,
        InvoiceAttributesLookupComponent,
        PendingSupplierLookupComponent,
        ProductionMaterialLookupComponent,
        ProductionProductLookupComponent,
    ],
    exports: [],
    providers: [
        MenusResolveGuard,
        UserProfileResolveGuard,
        LocalStorage,
        CommonService,
        ConfirmationDialogComponent,
        ExcelService,
        TodoService,
    ],
    entryComponents: [
        ConfirmationDialogComponent,
        EmployeeLookupComponent,
        LockScreenComponent,
        ReasonComponent,
        InvoiceProdDetailsConfirmationDialogComponent,
        CounterCodeLookupComponent,
        ProductCodeLookupComponent,
        StyleCodeLookupComponent,
        InvoiceAttributesLookupComponent,
        PendingSupplierLookupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class CommonViewModule { }
