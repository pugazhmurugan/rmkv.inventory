export const environment = {
  production: false,
  // apiUrl: "http://192.9.202.100:8098/",
  apiUrl: "http://localhost:8098/",
  launcherUiUrl: "http://192.9.202.100/rmkv.nxt/redirect?",
  baseUrl: "/",
};
